<?php
use Tracy\Debugger;

/**
 * Webmex - http://www.webmex.cz.
 */
version_compare(PHP_VERSION, '5.3', '<') and exit('Webmex requires PHP 5.3 or newer.');

$pathinfo = pathinfo(__FILE__);
$dirname = $pathinfo['dirname'];

define('SQC', true);
define('WEBMEX', true);
define('DOCROOT', $dirname . '/');
define('APPDIR', 'core');
define('APPROOT', DOCROOT . APPDIR . '/');

// Display errors and exceptions

require_once(APPROOT . 'include/nette/nette.phar');
$debugMode = (@$_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest')
	? Debugger::PRODUCTION
	: (!empty($_COOKIE['debug'])? Debugger::DEVELOPMENT: Debugger::DETECT);
Debugger::enable($debugMode, DOCROOT . 'etc/log' /*, 'errors@webmex.cz'*/);

//Debugger::enable(Debugger::DEVELOPMENT, DOCROOT . 'etc/log' /*, 'errors@webmex.cz'*/);

// Setup core
require_once(APPROOT . 'include/core.php');

Core::$profiler = false;  // display profiler at the bottom

Core::setup();

// Run application
Core::run();