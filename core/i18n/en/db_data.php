<?php

defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */
$db_extras = <<<END
INSERT INTO `page` VALUES(1,0,1,0,1,"welcome","Welcome","","",1,1,"","","",NULL,NULL,3,"list","",1,0,null,null,1,0,1,null,null,null,null,null);
INSERT INTO `page` VALUES(2,0,1,0,5,"basket","Basket","","",0,0,"","","",NULL,NULL,3,"list","",0,0,null,null,1,0,1,null,null,null,null,null);
INSERT INTO `page` VALUES(3,0,1,0,5,"order","Order","","",0,0,"","","",NULL,NULL,2,"list","",0,0,null,null,1,0,1,null,null,null,null,null);
INSERT INTO `page` VALUES(4,0,1,0,5,"order-sent","Order sent","","",0,0,"","","",NULL,NULL,2,"list","",0,0,null,null,1,0,1,null,null,null,null,null);
INSERT INTO `page` VALUES(5,0,1,0,1,"terms","Terms","","",0,0,"","","",NULL,NULL,3,"list","",0,0,null,null,1,0,1,null,null,null,null,null);
INSERT INTO `page` VALUES(6,0,1,0,2,"category","Category","","",1,0,"","","",NULL,NULL,3,"list","",0,0,null,null,1,0,1,null,null,null,null,null);
INSERT INTO `page` VALUES(7,0,1,0,5,"search","Search results","","",1,0,"","","",NULL,NULL,3,"list","",0,0,null,null,1,0,1,null,null,null,null,null);
INSERT INTO `page` VALUES(8,0,1,0,5,"404","Page not found","","",1,0,"","","",NULL,NULL,3,"list","",0,0,null,null,1,0,1,null,null,null,null,null);
INSERT INTO `page` VALUES(9,0,1,0,4,"sitemap","Sitemap","","",1,0,"","","",NULL,NULL,3,"list","",0,0,null,null,1,0,1,null,null,null,null,null);
INSERT INTO `config` VALUES("page_index","1");
INSERT INTO `config` VALUES("page_basket","2");
INSERT INTO `config` VALUES("page_order","3");
INSERT INTO `config` VALUES("page_order_finish","4");
INSERT INTO `config` VALUES("page_terms","5");
INSERT INTO `config` VALUES("page_search","7");
INSERT INTO `config` VALUES("page_404","8");
INSERT INTO `config` VALUES("page_sitemap","9");

INSERT INTO `email_message` VALUES ("1","Your order #{id}","<p>Hello,<br>
we received your order number #{id}.</p>","new_order", "order");

INSERT INTO `email_message` VALUES ("2","Your customer account","<p>Hello,<br>
Your new customer account:</p>","new_account", "account");

INSERT INTO `email_message` VALUES ("3","Password reset","<p>Hello,<br>
if yout want to reset your password, click the following link:</p>","reset_password", "password_reset");

INSERT INTO `email_message` VALUES ("4","Your new password","<p>Hello,<br>
this is your new password to the customer account:</p>","new_password", "account");

INSERT INTO `email_message` VALUES ("5","Order comment  #{id}","<p>Hello,<br>
a comment was added to your order #{id}:</p>","order_comment", "order_comment");

INSERT INTO `email_message` VALUES ("6","Low stock {name}","<p>Hello,<br>
following item has a low stock:</p>
<p>{sku} - {name}, stock: {stock} pcs</p>","low_stock", "default");

INSERT INTO `email_message` VALUES ("7","Discount voucher for your next purchase","<p>Hello,<br>
this is your discount voucher for your next purchase at {store_name}:</p>","voucher", "voucher");

INSERT INTO `email_message` VALUES ("8","Your order is being processed","<p>Hello,<br>
Your order #{id} is being processed and will be sent as soon as possible.</p>","order_status_in_process", "order");

INSERT INTO `email_message` VALUES ("9","Your order is processed","<p>Hello,<br>
Your order #{id} is processed and sent to your email address.</p>","order_status_finished", "order");

INSERT INTO `email_message` VALUES ("10","Your order has been canceled","<p>Hello,<br>
Your order #{id} is canceled.</p>","order_status_canceled", "order");

INSERT INTO `email_message` VALUES ("11","Your order is ready for shipment","<p>Hello,<br>
Your order #{id} is ready for shipment.</p>","order_status_to_send", "order");

INSERT INTO `email_message` VALUES ("12","Your order {id} is confirmed","<p>Hello,<br />
Your order {id} has been confirmed.</p>","order_confirmed", "order");

INSERT INTO `email_message` VALUES ("13","Your order {id} je potvrzena","<p>Dobrý den,<br />
Vaše objednávka {id} byla potvrzena obchodníkem.</p>","order_status_confirmed", "order");

INSERT INTO `email_message` VALUES ("14","Your order {id} has been modified","<p>Dobrý den,<br />
Vaše objednávka {id} byla upravena.</p>","order_status_updated", "order");

INSERT INTO `email_message` VALUES ("15","Your order {id} waiting for payment","<p>Dobrý den,<br />
Vaše objednávka {id} čeká na uhrazení platby.</p>","order_status_waiting_for_payment", "order");

INSERT INTO `email_message` VALUES ("16","Your order {id} has been payed","<p>Dobrý den,<br />
Vaše objednávka {id} byla zaplacena.</p>","order_status_paid", "order");

INSERT INTO `email_message` VALUES ("17","Your order {id} čeká na vyzvednutí","<p>Dobrý den,<br />
Vaše objednávka {id} čeká na vyzvednutí.</p>","order_status_waiting_for_pickup", "order");

INSERT INTO `email_message` VALUES ("18","Your order {id} byla odeslána","<p>Dobrý den,<br />
Vaše objednávka {id} byla odeslána.</p>","order_status_sent", "order");

INSERT INTO `availability` VALUES(1,0,"In stock");
INSERT INTO `country` VALUES(1,"Czech republic",0);
INSERT INTO `delivery` VALUES(1,"Czech Post","cpost","0.0",0,null,null,null,null,1);
INSERT INTO `payment` VALUES(1,"Cash on delivery","","0.0",null,0,null,null,null,null,null,1);
INSERT INTO `delivery_payments` VALUES(1,1,"10%",3000);
INSERT INTO `block` VALUES(1,"Categories","",0,"left","pages",'{"type":"2","order_by":"position"}',null);
INSERT INTO `customer_group` VALUES ("1","Visitors","1.0");
INSERT INTO `customer_group` VALUES ("2","Registered customers","1.0");

INSERT INTO `vat` VALUES ("1","Standard","20");
INSERT INTO `vat` VALUES ("2","Reduced","14");

INSERT INTO `config` VALUES ('sharelinks','<!-- AddThis Button BEGIN -->
<div class="addthis_toolbox addthis_default_style ">
<a class="addthis_button_facebook_like" fb:like:layout="button_count"></a>
<a class="addthis_button_tweet"></a>
<a class="addthis_button_pinterest_pinit" pi:pinit:layout="horizontal"></a>
<a class="addthis_counter addthis_pill_style"></a>
</div>
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=xa-5253253243715f1c"></script>
<!-- AddThis Button END -->');
INSERT INTO `label` VALUES ("1","HOT","DA0400");
END;
