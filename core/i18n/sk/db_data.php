<?php

defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */
$db_extras = <<<END
INSERT INTO `page` VALUES(1,0,1,0,1,"vitajte","Vitajte","","",1,1,"","","",NULL,NULL,3,"list","",1,0,null,null,1,0,1,null,null,null,null,null);
INSERT INTO `page` VALUES(2,0,1,0,5,"kosik","Váš košík","","",0,0,"","","",NULL,NULL,3,"list","",0,0,null,null,1,0,1,null,null,null,null,null);
INSERT INTO `page` VALUES(3,0,1,0,5,"objednavka","Objednávka","","",0,0,"","","",NULL,NULL,2,"list","",0,0,null,null,1,0,1,null,null,null,null,null);
INSERT INTO `page` VALUES(4,0,1,0,5,"objednavka-odoslana","Objednávka odoslaná","","",0,0,"","","",NULL,NULL,2,"list","",0,0,null,null,1,0,1,null,null,null,null,null);
INSERT INTO `page` VALUES(5,0,1,0,1,"obchodne-podmienky","Obchodné podmienky","","",0,0,"","","",NULL,NULL,3,"list","",0,0,null,null,1,0,1,null,null,null,null,null);
INSERT INTO `page` VALUES(6,0,1,0,2,"kategorie","Kategórie","","",1,0,"","","",NULL,NULL,3,"list","",0,0,null,null,1,0,1,null,null,null,null,null);
INSERT INTO `page` VALUES(7,0,1,0,5,"vyhladavanie","Výsledky vyhľadávania","","",1,0,"","","",NULL,NULL,3,"list","",0,0,null,null,1,0,1,null,null,null,null,null);
INSERT INTO `page` VALUES(8,0,1,0,5,"404","Stránka nenájdená","","",1,0,"","","",NULL,NULL,3,"list","",0,0,null,null,1,0,1,null,null,null,null,null);
INSERT INTO `page` VALUES(9,0,1,0,4,"mapa-stranok","Mapa stránok","","",1,0,"","","",NULL,NULL,3,"list","",0,0,null,null,1,0,1,null,null,null,null,null);
INSERT INTO `config` VALUES("page_index","1");
INSERT INTO `config` VALUES("page_basket","2");
INSERT INTO `config` VALUES("page_order","3");
INSERT INTO `config` VALUES("page_order_finish","4");
INSERT INTO `config` VALUES("page_terms","5");
INSERT INTO `config` VALUES("page_search","7");
INSERT INTO `config` VALUES("page_404","8");
INSERT INTO `config` VALUES("page_sitemap","9");

INSERT INTO `email_message` VALUES ("1","Vaša objednávka #{id}","<p>Dobrý deň,<br>
potvrdzujeme prijatie Vašej objednávky číslo #{id}.</p>","new_order", "order");

INSERT INTO `email_message` VALUES ("2","Váše konto","<p>Dobrý deň,<br>
bolo Vám vytvorené konto pre ďalšie nákupy:</p>","new_account", "account");

INSERT INTO `email_message` VALUES ("3","Reset hesla do zákazníckeho  konta","<p>Dobrý deň,<br>
pokiaľ chcete resetovat Vaše heslo, kliknite na nasledujúci odkaz:</p>","reset_password", "password_reset");

INSERT INTO `email_message` VALUES ("4","Vaše nové heslo","<p>Dobrý deň,<br>
bolo Vámvygenerované nové heslo pre prístup do zákazníckeho  konta:</p>","new_password", "account");

INSERT INTO `email_message` VALUES ("5","K objednávke  #{id} bol pridaný komentár","<p>Dobrý deň,<br>
k objednávke  #{id} bol pridaný komentár administrátormi obchodu:</p>","order_comment", "order_comment");

INSERT INTO `email_message` VALUES ("6","Nízky stav skladu {name}","<p>Dobrý deň,<br>
nasledujúci tovar má nízky stav skladových zásob:</p>","low_stock", "default");

INSERT INTO `email_message` VALUES ("7","Zľavový kupón pre Váš ďalší nákup","<p>Dobrý deň,<br>
bol Vám vytvorený zľavový kupón pre Váš ďalší nákup v e-shope {store_name}:</p>","voucher","voucher");

INSERT INTO `email_message` VALUES ("8","Vaša objednávka sa spracováva","<p>Dobrý deň,<br>
Vaša objednávka číslo {id} je teraz spracovaná a bude odoslaná v najkratšom možnom termíne.</p>","order_status_in_process","order");

INSERT INTO `email_message` VALUES ("9","Vaša objednávka je vybavená","<p>Dobrý deň,<br>
Vaša objednávka číslo {id} je vybavená a odoslaná na Vašu adresu.</p>","order_status_finished","order");

INSERT INTO `email_message` VALUES ("10","Vaša objednávka bola stornovaná","<p>Dobrý deň,<br>
Vaša objednávka číslo {id} je zrušená.</p>","order_status_canceled","order");

INSERT INTO `email_message` VALUES ("11","Vaša objednávka je pripravená na odoslanie","<p>Dobrý deň,<br>
Vaša objednávka číslo {id} je pripravená na odoslanie.</p>","order_status_to_send","order");

INSERT INTO `email_message` VALUES ("12","Vaša objednávka {id} je potvrdená","<p>Dobrý deň,<br>
Vaša objednávka {id} bola potvrdená obchodníkom.</p>","order_confirmed","order");

INSERT INTO `email_message` VALUES ("13","Vaša objednávka {id} je potvrdená","<p>Dobrý deň,<br />
Vaša objednávka {id} bola potvrzena obchodníkem.</p>","order_status_confirmed", "order");

INSERT INTO `email_message` VALUES ("14","Vaša objednávka {id} bola upravena","<p>Dobrý deň,<br />
Vaša objednávka {id} bola upravena.</p>","order_status_updated", "order");

INSERT INTO `email_message` VALUES ("15","Vaša objednávka {id} čaká na platbu","<p>Dobrý deň,<br />
Vaša objednávka {id} čaká na platbu.</p>","order_status_waiting_for_payment", "order");

INSERT INTO `email_message` VALUES ("16","Vaša objednávka {id} bola zaplatena","<p>Dobrý deň,<br />
Vaša objednávka {id} bola zaplatena.</p>","order_status_paid", "order");

INSERT INTO `email_message` VALUES ("17","Vaša objednávka {id} čaká na vyzdvihnutie","<p>Dobrý deň,<br />
Vaša objednávka {id} čaká na vyzdvihnutie.</p>","order_status_waiting_for_pickup", "order");

INSERT INTO `email_message` VALUES ("18","Vaša objednávka {id} bola odoslaná","<p>Dobrý deň,<br />
Vaša objednávka {id} bola odoslaná.</p>","order_status_sent", "order");

INSERT INTO `availability` VALUES(1,0,"Skladom");
INSERT INTO `country` VALUES(1,"Slovensko",0);
INSERT INTO `delivery` VALUES(1,"Slovenská pošta","cpost","0.0",0,null,null,null,null,1);
INSERT INTO `payment` VALUES(1,"Dobierka","","0.0",null,0,null,null,null,null,null,1);
INSERT INTO `delivery_payments` VALUES(1,1,"10%",3000);
INSERT INTO `block` VALUES(1,"Kategórie","",0,"left","pages",'{"type":"2","order_by":"position"}',null);
INSERT INTO `customer_group` VALUES ("1","Návštevnici","1.0");
INSERT INTO `customer_group` VALUES ("2","Registrovaní","1.0");

INSERT INTO `vat` VALUES ("1","Základná","20");
INSERT INTO `vat` VALUES ("2","Znížená","14");

INSERT INTO `config` VALUES ('sharelinks', '<!-- AddThis Button BEGIN -->
<div class="addthis_toolbox addthis_default_style ">
<a class="addthis_button_facebook_like" fb:like:layout="button_count"></a>
<a class="addthis_button_tweet"></a>
<a class="addthis_button_pinterest_pinit" pi:pinit:layout="horizontal"></a>
<a class="addthis_counter addthis_pill_style"></a>
</div>
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=xa-5253253243715f1c"></script>
<!-- AddThis Button END -->');
INSERT INTO `label` VALUES ("1","Akcia","DA0400");
END;
