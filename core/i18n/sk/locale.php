<?php

defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */
Core::$locale = array(
	'language' => 'sk',
	'date' => 'j. n. Y',
	'datetime' => 'j. n. Y H:i',
	'currency' => '€',
	'price_format' => 3,
	'vat' => 20,
	'stop_words' => array(
		'aby', 'ale', 'anebo', 'ani', 'áno', 'asi', 'avšak', 'až', 'bez', 'bude', 'budem', 'budeme', 'budeš', 'byl', 'byla',
		'boli', 'bolo', 'byť', 'článok', 'článku', 'članky', 'com', 'čo', 'ďalšie', 'dnes', 'do', 'email', 'jak', 'aké', 'ako',
		'je', 'jeho', 'jej', 'jejich', 'jen', 'ešte', 'iné', 'som', 'si', 'sme', 'sú', 'ste',
		'kam', 'kde', 'kdo', 'keď', 'ktorá', 'ktoré', 'má', 'máte', 'mezi', 'mě', 'mít', 'mně', 'mnou', 'můj',
		'môže', 'na', 'nad', 'nám', 'napište', 'nás', 'naši', 'nebo', 'nejsou', 'není', 'net', 'než', 'nic', 'nové',
		'nový', 'od', 'ode', 'org', 'pak', 'po', 'pod', 'podle', 'pokud', 'pouze', 'práve', 'pred', 'pres', 'pri', 'pro', 'proč',
		'proto', 'protože', 'prví', 'ptá', 'se', 'sice', 'spol', 'strana', 'své', 'svoj', 'tak', 'také',
		'takže', 'tam', 'tato', 'tedy', 'téma', 'ten', 'tedy', 'tento', 'teto', 'ti', 'tím', 'tímto', 'tipy', 'to', 'tohle', 'toho',
		'tohoto', 'tom', 'tomto', 'tomuto', 'tuto', 'tvůj', 'tyto', 'vám', 'vás', 'váš', 'vaše', 've', 'vedle', 'vy',
		'viac', 'však', 'všetok', 'zda', 'zde', 'späť', 'že',
		'cm', 'mm', 'km', 'hz', 'ghz', 'mhz', 'kg'
	),
	'word_radix_fn' => 'radix_sk'
);

function radix_sk($str)
{
	$radix = null;

	if (preg_match('/(ovy|ova|ami|ych|ich)$/', $str)) {
		$radix = substr($str, 0, -3);
	} else if (preg_match('/(am|em|im|om|um|ym)$/', $str)) {
		$radix = substr($str, 0, -2);
	} else if (preg_match('/[aeiouy]$/', $str)) {
		$radix = substr($str, 0, -1);
	}

	return $radix ? $radix : $str;
}