<form action="http://www.webmex.cz/order" method="post" target="_blank">
	<h3>Premium funkcie</h3>
	<p>Používate obmedzenú verziu e-shopu, ktorá je dostupná zadarmo a niektoré funkcie nie sú dostupné.
		Ak chcete využiť premiovych funkcií tohto systému, prejdite na premium verziu.</p>
	<p>Premium verze e-shopu obsahuje tyto funkce:</p>
	<ul>
		<li>Atribúty tovaru (veľkosť, farba, a iné) vrátane vlastných kódov, cien a obrázkov</li>
		<li>Prihlasovanie zákazníkov - sledovanie objednávok, správa údajov</li>
		<li>Cenovej hladiny pre registrovaných zákazníkov</li>
		<li>Prepojenie s účtovným programom Pohoda</li>
		<li>Faktúry z objednávok</li>
		<li>Importy tovaru napr. od dodávateľov vo formáte XML, CSV alebo XLS/li>
		<li>Hromadné zľavy a zľavy s dátumom platnosti</li>
		<li>Zľavové kupóny</li>
	</ul>
	<p><a href="http://www.webmex.cz/#2-premium" target="_blank">Viac o premium funkciách &raquo;</a></p>

	<input type="hidden" name="version" value="<?php echo Core::version ?>" />
	<input type="hidden" name="instid" value="<?php echo Core::config('instid') ?>" />
	<input type="hidden" name="name" value="<?php echo Core::config('store_name') ?>" />
	<input type="hidden" name="host" value="<?php echo $_SERVER['HTTP_HOST'] ?>" />
	<input type="hidden" name="url" value="<?php echo Core::$url ?>" />

	<div class="buttons">
		<button class="button">Chcem premium e-shop &raquo;</button>
	</div>
</form>