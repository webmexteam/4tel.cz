<?php defined('WEBMEX') or die('No direct access.'); ?>

<div><?php echo $content?></div>

<table cellspacing="0" border="0" cellpadding="0" width="100%" style="margin-top:25px">

	<tr>
		<td colspan="2" style="padding:0 0 15px 0">
			
			<table align="right" cellspacing="0" border="0" cellpadding="0" width="100%" style="border:1px solid #e0e0e0">
				<tr>
					<td style="background:#f0f0f0;padding:5px" colspan="2"><?php echo __('your_customer_account')?></td>
				</tr>
				<tr>
					<td style="padding:5px;">
						<?php echo __('email')?>: <b><?php echo $email?></b><br>
						<?php echo __('password_reset_url')?>: <a href="<?php echo $link?>"><?php echo $link?></a>
					</td>
				</tr>
				<tr>
					<td style="padding:5px;font-size:10px;color:#777">
						<?php echo __('password_reset_url_info')?>
					</td>
				</tr>
			</table>
			
		</td>
	</tr>
	
</table>