<?php defined('WEBMEX') or die('No direct access.'); ?>

<div><?php echo $content ?></div>

<table cellspacing="0" border="0" cellpadding="0" width="100%" style="margin-top:25px">
    <tr>
	<td colspan="2">
	    <h1 style="font-size:16px;font-weight:normal;margin:0 0 10px 0"><?php echo __('order_recapitulation') ?> <?php echo $id ?></h1>
	</td>
    </tr>

    <?php if ($payment_info && $payment): ?>
        <tr>
    	<td colspan="2" style="padding-bottom:15px">

    	    <table cellspacing="0" border="0" cellpadding="0" width="100%" style="border:1px solid #e0e0e0">
    		<tr>
    		    <td style="background:#f0f0f0;padding:5px" colspan="2"><?php echo __('payment') ?>: <b><?php echo $payment->payment['name'] ?></b></td>
    		</tr>
    		<tr>
    		    <td style="padding:5px">
			    <?php echo $payment_info ?>
    		    </td>
    		</tr>


		    <?php if (Core::config('account_num') && strlen(Core::config('account_num')) > 0): ?>
			<tr>
			    <td style="padding:5px">
				<?php echo __('account_num') ?>: <?php echo Core::config('account_num') ?>
			    </td>
			</tr>
		    <?php endif; ?>

    	    </table>

    	</td>
        </tr>
    <?php endif; ?>

    <tr>
	<td colspan="2" style="padding:0 0 15px 0">

	    <table align="right" cellspacing="0" border="0" cellpadding="0" width="100%" style="border:1px solid #e0e0e0">
		<tr>
		    <td style="background:#f0f0f0;padding:5px" colspan="2"><?php echo __('your_order') ?></td>
		</tr>
		<tr>
		    <td style="padding:5px;" width="50%" valign="top">
			<?php echo __('status') ?>: <b><?php echo __('status_' . Core::$def['order_status'][$order_status]) ?></b><br>
			<?php echo __('received') ?>: <?php echo fdate($received, true) ?>

			<?php if ($track_num): ?>
    			<br><?php echo __('track_num') ?>:

			    <?php if ($delivery && ($link = $delivery->getTrackLink($track_num))): ?>
				<a href="<?php echo $link ?>"> <b><?php echo $track_num ?></b></a>

			    <?php else: ?>
				<b><?php echo $track_num ?></b>
			    <?php endif; ?>

			<?php endif; ?>
		    </td>
		    <td style="padding:5px;" valign="top">
			<?php echo __('email') ?>: <?php echo $email ?><br>
			<?php echo __('phone') ?>: <?php echo $phone ?>
		    </td>
		</tr>
	    </table>

	</td>
    </tr>

    <tr>
	<td width="50%" valign="top">

	    <table cellspacing="0" border="0" cellpadding="0" width="98%" style="border:1px solid #e0e0e0">
		<tr>
		    <td style="background:#f0f0f0;padding:5px"><?php echo __('billing_address') ?></td>
		</tr>
		<tr>
		    <td style="padding:5px;">
			<?php echo $first_name . ' ' . $last_name ?><br>
			<?php echo ($company ? $company . '<br>' : '') ?>
			<?php echo $street ?><br>
			<?php echo $city . ' ' . $zip ?><br>
			<?php echo $country ?>
			<?php echo ($company_id ? '<br>' . __('company_id') . ': ' . $company_id : '') ?>
			<?php echo ($company_vat ? '<br>' . __('company_vat') . ': ' . $company_vat : '') ?>
		    </td>
		</tr>
	    </table>

	</td>
	<td width="50%" valign="top">

	    <table align="right" cellspacing="0" border="0" cellpadding="0" width="98%" style="border:1px solid #e0e0e0">
		<tr>
		    <td style="background:#f0f0f0;padding:5px"><?php echo __('shipping_address') ?></td>
		</tr>
		<tr>
		    <td style="padding:5px;">
			<?php if (!$ship_street): ?>
    			<em><?php echo __('shipping_as_billing') ?></em>

			<?php else: ?>
			    <?php echo $ship_first_name . ' ' . $ship_last_name ?><br>
			    <?php echo ($ship_company ? $ship_company . '<br>' : '') ?>
			    <?php echo $ship_street ?><br>
			    <?php echo $ship_city . ' ' . $ship_zip ?><br>
			    <?php echo $ship_country ?>
			<?php endif; ?>
		    </td>
		</tr>
	    </table>

	</td>
    </tr>

    <tr>
	<td colspan="2" style="padding:15px 0">

	    <table align="right" cellspacing="0" border="0" cellpadding="0" width="100%" style="border:1px solid #e0e0e0">
		<tr>
		    <td style="background:#f0f0f0;padding:5px"><?php echo __('order_items') ?></td>
		</tr>
		<tr>
		    <td>

			<table cellspacing="0" border="0" cellpadding="0" width="100%">
			    <?php $total_price = 0; ?>
			    <?php foreach ($products as $product): ?>
    			    <tr>
    				<td style="padding:5px" valign="top"><?php echo $product['sku'] ?></td>
    				<td style="padding:5px" valign="top"><?php echo $product['name'] ?></td>
				<td style="padding:5px" valign="top"><?php echo $product['quantity'] ?> ks</td>
    				<td style="padding:5px;white-space:nowrap" valign="top" align="right"><?php echo price(sprintf('%.2f', $product['price']) * $product['quantity'], $order['currency']) ?></td>
    				<td style="padding:5px;white-space:nowrap" align="right" valign="top">
    				    <b>
					    <?php $vat = sprintf('%.2f', $product['price']) * 0.21 ?>

					    <?php $product_price = (sprintf('%.2f', $product['price']) + $vat) * $product['quantity']; ?>
					    <?php echo price($product_price, $order['currency']); ?>
    				    </b>
    				</td>

				    <?php $total_price += $product_price; ?>
    			    </tr>
			    <?php endforeach; ?>

			</table>

		    </td>
		</tr>
	    </table>

	</td>
    </tr>

    <tr>
	<td colspan="2" style="padding-bottom:20px">

	    <table align="right" cellspacing="0" border="0" cellpadding="0">
		<?php if (isset($voucher)): ?>
    		<tr>
    		    <td style="padding-bottom:5px"><?php echo __('voucher') ?> (<?php echo $voucher['code'] ?>):</td>
    		    <td align="right" style="padding-left:20px;padding-bottom:5px;white-space:nowrap"><?php echo '-' . price($voucher_discount, $order['currency']) ?></td>
    		</tr>
		<?php endif; ?>

		<?php if ((int) $vat_payer): ?>
    		<tr>
    		    <td style="padding-bottom:5px"><?php echo __('total_excl_vat') ?>:</td>
    		    <td align="right" style="padding-left:20px;padding-bottom:5px;white-space:nowrap"><?php echo price($total_excl_vat, $order['currency']) ?></td>
    		</tr>
		<?php endif; ?>
		<tr>
		    <td>DPH:</td>
		    <?php $vat = (sprintf('%.2f', $total_excl_vat))*0.21; ?>
		    <td align="right" style="padding-left:20px;white-space:nowrap">
			    <?php
				echo number_format($vat, 2, ',', ' ');
			    ?> Kč
		    </td>
		</tr>
		<tr>
		    <td>Zaokrouhleno:</td>
		    <?php $total_rounded = number_format(round($total_price) - round($total_price, 2), 2, ',' ,' '); ?>
		    <td align="right" style="padding-left:20px;white-space:nowrap">
			    <?php
			    if ((round($total_price) - $total_price) > 0) {
				echo '+' . $total_rounded;
			    } else {
				echo $total_rounded;
			    }
			    ?> Kč
		    </td>
		</tr>
		<tr>
		    <td><?php echo __('total_to_pay') ?>:</td>
		    <td align="right" style="padding-left:20px;white-space:nowrap"><b><?php echo price(round($total_price), $order['currency']) ?></b></td>
		</tr>
	    </table>

	</td>
    </tr>
    <tr>
	<td colspan="2">

	    <table cellspacing="0" border="0" cellpadding="0" width="100%">
		<tr>
		    <td><?php echo __('comment') ?>:</td>
		</tr>
		<tr>
		    <td>
			<?php if ($customer_comment): ?>
			    <?php echo Email::html($customer_comment) ?>

			<?php else: ?>
    			<em><?php echo __('no_comment') ?></em>
			<?php endif; ?>
		    </td>
		</tr>
	    </table>

	</td>
    </tr>
</table>