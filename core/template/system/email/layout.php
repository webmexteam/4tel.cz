{syntax double}
<?php defined('WEBMEX') or die('No direct access.'); ?>
<html>
	<head>
		<meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
		<style type="text/css">
		*, html, body { font-family: Verdana, sans-serif; font-size: 12px; color: #444; line-height: 17px; }
		</style>
	</head>
	<body marginheight="0" topmargin="0" marginwidth="0" bgcolor="#f0f0f0" leftmargin="0" width="100%">
	
		<table cellspacing="0" border="0" cellpadding="0" width="100%">
			<tr>
				<td valign="top" style="padding:20px">
					<!-- Main content --> 
					
					<table align="center" cellspacing="0" border="0" cellpadding="0" width="600" style="background:#fff;border:1px solid #ddd;">
						<tr>
							<td style="font-size:16px;padding:20px">
								<a href="{store_url}" style="font-size:16px;text-decoration:none">{store_name}</a>
								
								<!-- <a href="{store_url}"><img src="http://.../logo.gif" alt="{store_name}" border="0" /></a> -->
							</td>
						</tr>
						
						<tr>
							<td style="padding:10px 20px">
								<?php echo $content?>
							</td>
						</tr>
						
						<tr>
							<td style="background:#f4f4f4;padding:20px;font-size:11px;color:#777">
								<a href="<?php echo Core::$url?>" style="font-size:11px;color:#777"><?php echo preg_replace('#^http://|/$#', '', Core::$url)?></a>
							</td>
						</tr>
					</table>
					
					<!-- END Main content -->
				</td>
			</tr>
		</table>
		
	</body>
</html>