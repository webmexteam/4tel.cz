<?php if($analytics = Core::config('analytics')): ?>
	<script type="text/javascript">
		ga('ecommerce:addTransaction', { 
			'id': '<?php echo $order['id']?>', // Transaction ID. Required 
			'affiliation': '<?php echo Core::config('store_name')?>', // Affiliation or store name 
			'revenue': '<?php echo $order['total_price']?>', // Grand Total 
			'shipping': '<?php echo $order['delivery_payment']?>', // Shipping 
			'tax': '' // Tax 
		});
		
		<?php foreach($order->order_products() as $product): ?>
		// Příkaz addItem by měl být volán pro každou položku v nákupním košíku 
		ga('ecommerce:addItem', { 
			'id': '<?php echo $order['id']?>', // Transaction ID. Required 
			'name': '<?php echo $product['name']?>', // Product name. Required 
			'sku': '<?php echo ($product['sku'] ? $product['sku'] : $product['id'])?>', // SKU/code 
			'category': '', // Category or variation 
			'price': '<?php echo $product['price']?>', // Unit price 
			'quantity': '<?php echo $product['quantity']?>' // Quantity 
		});
		<?php endforeach; ?>
	
		ga('ecommerce:send'); // Odesílání údajů o položkách a transakcích do Google Analytics
	</script>
<?php endif; ?>



<?php
# Heureka conversion script
if ($heureka_conversions = Core::config("heureka_conversions")) {
	echo '<!-- Heureka conversion script -->';
	echo '
		<script type="text/javascript">
		var _hrq = _hrq || [];
		_hrq.push([\'setKey\', \'' . $heureka_conversions . '\']);
		_hrq.push([\'setOrderId\', \'' . $order['id'] . '\']);
	';

	foreach($order->order_products() as $product) {
		echo '
			_hrq.push([\'addProduct\', \'' . $product['name'] . '\', \'' . $product['price'] . '\', \'' . $product['quantity'] . '\']);
		';
	}

	echo '
		_hrq.push([\'trackOrder\']);

		(function() {
		var ho = document.createElement(\'script\'); ho.type = \'text/javascript\'; ho.async = true;
		ho.src = (\'https:\' == document.location.protocol ? \'https://ssl\' : \'http://www\') + \'.heureka.cz/direct/js/cache/1-roi-async.js\';
		var s = document.getElementsByTagName(\'script\')[0]; s.parentNode.insertBefore(ho, s);
		})();
		</script>
	';
}
?>



<?php if($zbozi_conversions = Core::config('zbozi_conversions')): ?>
<iframe src="<?php echo $zbozi_conversions?>&uniqueId=<?php echo $order['id']?>&price=<?php echo $order['total_incl_vat']?>" frameborder="0" marginwidth="0" marginheight="0" scrolling="no" style="position:absolute; top:-3000px; left:-3000px; width:1px; height:1px; overflow:hidden;"></iframe>
<?php endif; ?>



<?php if($sklik_conversions = Core::config('sklik_conversions')): ?>
<!-- Měřicí kód Sklik.cz -->
<iframe width="119" height="22" frameborder="0" scrolling="no" src="<?php echo $sklik_conversions?><?php echo $order['total_incl_vat']?>"></iframe>
<?php endif; ?>



<?php 
	if($heureka = Core::config('heureka_overeno')){
		if (empty($GLOBALS['heureka_overeno_sent'])) {  // prevent multiple send (bug by View with overwrited template)
			$products = array(
				'itemId' => array(),
			);
			
			foreach($order->order_products() as $product){
				if($product['product_id']){
					$products['itemId'][] = $product['name'];
				}
			}

			$requestUrl = 'http://www.heureka.cz/direct/dotaznik/objednavka.php' . 
				'?id=' . $heureka . 
				'&email=' . $order['email'] . 
				'&' . http_build_query($products) . 
				'&orderid=' . $order['id'];
			$result = @file_get_contents($requestUrl);

			$GLOBALS['heureka_overeno_sent'] = TRUE;
		}
	}
?>



<?php if(FALSE && $google_conversions = Core::config('google_conversions')): ?>
	<!-- Google Code for Konverze Prodej Conversion Page -->
	<?php
	$ids = array(); 
	$prices = array(); 
	foreach ($order->order_products() as $product) {
		if ($product['product_id']) {
			$ids[] = $product['product_id'];
			$prices[] = price_vat($product['price'], $product['vat'])->price;
		}
	}

	$ecommProdId = '[' . join(', ', $ids) . ']';
	$ecommPageType = 'cart';
	$ecommTotalValue = '[' . join(', ', $prices) . ']';
	?>

	<script type="text/javascript">
	var google_tag_params = {
		ecomm_prodid: <?php echo $ecommProdId ?>,
		ecomm_pagetype: '<?php echo $ecommPageType ?>',
		ecomm_totalvalue: <?php echo $ecommTotalValue ?> 
	};
	</script>


	<!-- Google Code for Conversion Page --> 
	<script type="text/javascript"> 
	/* <![CDATA[ */ 
	var google_conversion_id = <?php echo $google_conversions ?>; 
	var google_custom_params = window.google_tag_params;
	var google_remarketing_only = true;

	var google_conversion_language = "cs"; 
	var google_conversion_format = "3"; 
	var google_conversion_color = "ffffff"; 
	var google_conversion_value = <?php echo (float) str_replace(' ', '', str_replace(',', '.', $order['total_incl_vat']))?>; 
	/* ]]> */ 
	</script> 
	<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js"></script> 
	<noscript> 
		<div style="display:inline;width:1px;height:1px;overflow:hidden;"> 
			<img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/<?php echo $google_conversions ?>/?value=<?php echo (float) str_replace(' ', '', str_replace(',', '.', $order['total_incl_vat']))?>&amp;guid=ON&amp;script=0"/>
		</div>
	</noscript>
	<!-- eof Google Code for Konverze Digital24.cz Conversion Page --> 
<?php endif; ?>