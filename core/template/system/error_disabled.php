<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="cs" lang="cs">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>System Error</title>
		
		<meta name="robots" content="noindex,nofollow" />
		
		<meta http-equiv="pragma" content="no-cache" />
		
		<style type="text/css">
			body {
				font-family: Arial, Tahoma;
				font-size: 12px;
				background: #f4f4f4;
			}
			#container {
				width: 700px;
				margin: 10px auto;
				background: #fff;
				border: 1px solid #ccc;
				padding: 10px;
			}
			#container h2 {
				font-size: 110%;
				margin: 0;
				padding: 5px;
				background: #f0f0f0;
			}
			#container p {
				margin: 15px 0;
			}
			#container p.back {
				background: #f0f0f0;
				padding: 10px;
				margin: 0;
			}
			#container p.back .storename {
				margin-right: 30px;
			}
		</style>
		
	</head>
	<body>
		
		<div id="container">
			<?php if(User::has('all')): ?>
			<h2><?php echo $message?></h2>
			
			<pre><?php echo $file?> [line <?php echo $line?>]</pre>
			
			<?php else: ?>
			<h2><?php echo (Core::$lang ? __('error_occured') : 'An error occured')?></h2>
			<?php endif; ?>
			
			<p><?php echo (Core::$lang ? __('error_occured_text') : 'Page can not be displayed. Please try again later. If the problem persists, contact the administrator of this site.')?></p>
			
			<?php if(Core::config('store_name')): ?>
			<p class="back">
				<a href="<?php echo url(Core::$is_admin ? 'admin' : '')?>" class="storename"><strong><?php echo Core::config('store_name')?></strong></a>
			</p>
			<?php endif; ?>
		</div>
		
		<?php if(Core::$debug_report): ?>
		<img src="<?php echo $report?>" alt="" style="visibility:hidden;" />
		<?php endif; ?>
	</body>
</html>