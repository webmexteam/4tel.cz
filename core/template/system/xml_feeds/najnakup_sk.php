<?php defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */

 echo '<?xml version="1.0" encoding="utf-8"?>'?>

<SHOP>
	<?php
	$offset = 0;
	$_products = null;
	$availablity_id = (int) Core::config('xml_feed_availability_id');

	while(
		($_products = Core::$db->product()->where('status', 1)->limit(100, $offset)) &&
		($availablity_id ? $_products->where('availability_id  = '.$availablity_id) : true) &&
		count($_products)
	):

	foreach($_products as $product):
	$manufacturer = Core::$controller->_getManufacturer($product);

	?>

    <?php
        $displayName = $product['name_feed']?: $product['name'];
        $displayDesc = $product['description_feed']?: $product['description_short'];
    ?>
	<SHOPITEM>
		<CODE><?php echo $product['id']?></CODE>
		<NAME><![CDATA[<?php echo trim($manufacturer.' '.strip_tags(trim($displayName.' '.$product['nameext'])))?>]]></NAME>

		<DESCRIPTION><![CDATA[<?php echo strip_tags($displayDesc)?>]]></DESCRIPTION>
		<PRODUCT_URL><?php echo url($product, null, true)?></PRODUCT_URL>
		<?php if($img = imgsrc($product, 3)): ?>
		<IMAGE_URL><?php echo preg_replace('/\s/', '%20', $site.$img)?></IMAGE_URL>
		<?php endif; ?>

		<PRICE><?php echo price_vat($product['price'] + $product['fees'], $product['vat'])->price?></PRICE>

		<AVAILABILITY><?php echo $product->availability['name']?></AVAILABILITY>

		<?php if($page = $product->product_pages()->fetch()): ?>
		<CATEGORY><![CDATA[<?php echo Core::$controller->_getCategoryPath($page, ' > ')?>]]></CATEGORY>
		<?php endif; ?>

		<?php if($manufacturer): ?>
		<MANUFACTURER><![CDATA[<?php echo $manufacturer?>]]></MANUFACTURER>
		<?php endif; ?>

		<?php if($product['ean13']): ?>
		<EAN><?php echo $product['ean13']?></EAN>
		<?php endif; ?>
	</SHOPITEM>

	<?php endforeach; Core::$controller->_flush(); $offset += 100; endwhile; ?>

</SHOP>

<?php Core::$controller->_flush(); ?>