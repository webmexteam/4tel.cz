<?php defined('SQC') or die('No direct access.');

/*
 * Copyright (c) 2010-2011 Daniel Regéci. All rights reserved.
 * License: http://www.superqc.com/license
 */

 echo '<?xml version="1.0" encoding="utf-8"?>'?>

<SHOP>
	<?php
	$offset = 0;
	$_products = null;
	$availablity_id = (int) Core::config('xml_feed_availability_id');

	while(
		($_products = Core::$db->product()->where('status', 1)->where('price != 1')->limit(500, $offset)) &&
		($availablity_id ? $_products->where('availability_id  = '.$availablity_id) : true) &&
		count($_products)
	):

	foreach($_products as $product):
		$manufacturer = Core::$controller->_getManufacturer($product);
	?>

	<?php if(($attributes = $product->product_attributes()->order('name ASC, id ASC')) && count($attributes)): foreach($attributes as $attribute): ?>
	<SHOPITEM>
		<ITEM_ID><?php echo $product['id']?>attr<?php echo $attribute['id']?></ITEM_ID>

		<?php $displayName = $product['name_feed']?: $product['name']; ?>
		<PRODUCTNAME><![CDATA[<?php echo strip_tags(trim($displayName.' '.$attribute['value'])) ?>]]></PRODUCTNAME>
		<?php if($product['nameext']): ?>
			<PRODUCT><![CDATA[<?php echo strip_tags(trim($displayName.' '.$attribute['value'].' '.$product['nameext']))?>]]></PRODUCT>
		<?php else: ?>
			<PRODUCT><![CDATA[<?php echo strip_tags(trim($displayName.' '.$attribute['value']))?>]]></PRODUCT>
		<?php endif; ?>

		<?php $displayDesc = $product['description_feed']?: $product['description_short']; ?>
		<DESCRIPTION><![CDATA[<?php echo strip_tags($displayDesc)?>]]></DESCRIPTION>
		<URL><?php echo url($product, array('attr' => $attribute['id']), true)?></URL>
		<?php if(($attribute['file_id'] && ($img = imgsrc(Core::$db->product_files[$attribute['file_id']], 3))) || ($img = imgsrc($product, 3))): ?>
		<IMGURL><?php echo preg_replace('/\s/', '%20', $site.$img)?></IMGURL>
		<?php endif; ?>

		<PRICE_VAT>
			<?php
			if (FALSE && (float)$product['units_in_pack']) {
				echo price_vat(
					($product['price'] + estPrice($attribute['price'], $product['price'])) / $product['units_in_pack'],
					$product->vat['rate']
				)->price;

			} else {
				echo price_vat(
					$product['price'] + estPrice($attribute['price'], $product['price']),
					$product['vat']
				)->price;
			}
			?>
		</PRICE_VAT>

		<?php if($manufacturer): ?>
		<MANUFACTURER><![CDATA[<?php echo $manufacturer?>]]></MANUFACTURER>
		<?php endif; ?>
		<?php if(($product['default_page'] && ($page = Core::$db->page[(int) $product['default_page']])) || (($page = $product->product_pages()->fetch()) && ($page = $page->page))): ?>
		<CATEGORYTEXT><![CDATA[<?php echo Core::$controller->_getCategoryPath($page, ' | ')?>]]></CATEGORYTEXT>
		<?php endif; ?>
		<?php if(($ean = $attribute['ean13']) || ($ean = $product['ean13'])): ?>
		<EAN><?php echo $ean?></EAN>
		<?php endif; ?>
		<?php if(($sku = $attribute['sku']) || ($sku = $product['sku'])): ?>
		<PRODUCTNO><?php echo $sku?></PRODUCTNO>
		<?php endif; ?>
		<PARAM>
			<PARAM_NAME><![CDATA[<?php echo $attribute['name']?>]]></PARAM_NAME>
			<VAL><![CDATA[<?php echo $attribute['value']?>]]></VAL>
		</PARAM>
		<?php if(($av = $attribute->availability['days']) !== null || ($av = $product->availability['days']) !== null): ?>
		<DELIVERY_DATE><?php echo $av?></DELIVERY_DATE>
		<?php endif; ?>
		<ITEMGROUP_ID>GROUP<?php echo $product['id']?></ITEMGROUP_ID>
	</SHOPITEM>

	<?php endforeach; else: ?>

	<SHOPITEM>
		<ITEM_ID><?php echo $product['id']?></ITEM_ID>

		<?php $displayName = $product['name_feed']?: $product['name']; ?>
		<PRODUCTNAME><![CDATA[<?php echo strip_tags(trim($displayName))?>]]></PRODUCTNAME>
		<?php if($product['nameext']): ?>
			<PRODUCT><![CDATA[<?php echo strip_tags(trim($displayName.' '.$product['nameext']))?>]]></PRODUCT>
		<?php else: ?>
			<PRODUCT><![CDATA[<?php echo strip_tags(trim($displayName))?>]]></PRODUCT>
		<?php endif; ?>

		<?php $displayDesc = $product['description_feed']?: $product['description_short']; ?>
		<DESCRIPTION><![CDATA[<?php echo strip_tags($displayDesc)?>]]></DESCRIPTION>
		<URL><?php echo url($product, null, true)?></URL>
		<?php if($product->availability['days'] !== null): ?>
		<DELIVERY_DATE><?php echo $product->availability['days']?></DELIVERY_DATE>
		<?php endif; ?>

		<?php if($img = imgsrc($product, 3)): ?>
		<IMGURL><?php echo preg_replace('/\s/', '%20', $site.$img)?></IMGURL>
		<?php endif; ?>

		<PRICE_VAT>
			<?php
			if (FALSE && (float)$product['units_in_pack']) {
				echo price_vat(
					$product['price'] / $product['units_in_pack'],
					$product->vat['rate']
				)->price;

			} else {
				echo price_vat(
					$product['price'],
					$product['vat']
				)->price;
			}
			?>
		</PRICE_VAT>

		<?php if($product['default_page'] && ($page = Core::$db->page[(int) $product['default_page']])): ?>
		<CATEGORYTEXT><![CDATA[<?php echo Core::$controller->_getCategoryPath($page, ' | ')?>]]></CATEGORYTEXT>
		<?php endif; ?>

		<?php if($manufacturer): ?>
		<MANUFACTURER><![CDATA[<?php echo $manufacturer?>]]></MANUFACTURER>
		<?php endif; ?>

		<?php if($sku = $product['sku']): ?>
		<PRODUCTNO><?php echo $sku?></PRODUCTNO>
		<?php endif; ?>

		<?php if($product['ean13']): ?>
		<EAN><?php echo $product['ean13']?></EAN>
		<?php endif; ?>
	</SHOPITEM>
	<?php endif; ?>

	<?php endforeach; Core::$controller->_flush(); $offset += 500; endwhile; ?>
</SHOP>

<?php Core::$controller->_flush(); ?>