<?php

defined ( 'SQC' ) or die ( 'No direct access.' );

/*
 * Copyright (c) 2010-2011 Daniel Regéci. All rights reserved.
 * License: http://www.superqc.com/license
 */

echo '<?xml version="1.0" encoding="utf-8"?>'?>
<item_list>
	<?php
	$offset = 0;
	$_products = null;
	$availablity_id = ( int ) Core::config ( 'xml_feed_availability_id' );
	
	while ( ($_products = Core::$db->product ()->where ( 'status', 1 )->where ( 'price != 1' )->limit ( 500, $offset )) && ($availablity_id ? $_products->where ( 'availability_id  = ' . $availablity_id ) : true) && count ( $_products ) ) :
		foreach ( $_products as $product ) :
			if($product["stock"]>0) {
			?>
<item id='<?=$product["id"]?>'> <stock_quantity><?=$product["stock"];?></stock_quantity> </item>	
<?php } endforeach; Core::$controller->_flush(); $offset += 500; endwhile; ?>
</item_list>