<?php defined('SQC') or die('No direct access.');

/*
 * Copyright (c) 2013 Tomas Nikl, http://www.tomasnikl.eu
 */

echo '<?xml version="1.0"?> <rss version="2.0" xmlns:g="http://base.google.com/ns/1.0">'?>

<channel>
	<title><?php echo Core::config('store_name')?></title>
	<link><?php echo url(null)?></link>
	<description></description>
	<?php
	$offset = 0;
	$_products = null;
	$availablity_id = (int) Core::config('xml_feed_availability_id');

	while(
		($_products = Core::$db->product()->where('status', 1)->where('availability_id != ?', 7)->where('availability_id != ?', 8)->where('availability_id != ?', 9)->where('availability_id != ?', 10)->limit(500, $offset)) &&
		($availablity_id ? $_products->where('availability_id  = '.$availablity_id) : true) &&
		count($_products)
	):

	foreach($_products as $product): ?>

    <?php
        $displayName = $product['name_feed']?: $product['name'];
        $displayDesc = $product['description_feed']?: $product['description_short'];
    ?>

	<item>
		<g:id><?php echo $product['id']?></g:id>
		<title><![CDATA[<?php echo strip_tags(trim($displayName))?>]]></title>
		<description><![CDATA[<?php echo strip_tags($displayDesc)?>]]></description>
		<?php if(($product['default_page'] && ($page = Core::$db->page[(int) $product['default_page']])) || (($page = $product->product_pages()->fetch()) && ($page = $page->page))): ?>
		<g:product_type><?php echo Core::$controller->_getCategoryPath($page, ' &gt; ')?></g:product_type>
		<?php endif; ?>
		<link><![CDATA[<?php echo url($product, null, true)?>]]></link>
		<?php $files = Core::$db->product_files()->where('product_id', $product['id'])->order('position ASC'); ?>
		<?php $x=0;foreach($files as $i => $file):?>
			<?php if(!$file['is_image']) continue;?>
			<?php if($x > 10) break;?>
			<?php if($x==0):?>
				<g:image_link><![CDATA[<?php echo preg_replace('/\s/', '%20', $site.imgsrc($file, 0))?>]]></g:image_link>
			<?php else:?>
				<g:additional_image_link><![CDATA[<?php echo preg_replace('/\s/', '%20', $site.imgsrc($file, 0))?>]]></g:additional_image_link>
			<?php endif?>
		<?php $x++;endforeach?>
		<g:condition>new</g:condition>
		<?php if($product->availability['days'] !== null): ?>
		<g:availability><?php echo ((int) $product->availability['days'] == 0) ? 'in stock' : 'out of stock';?></g:availability>
		<?php endif; ?>
		<g:price><?php echo price_vat($product['price'] + estPrice($attribute['price'], $product['price']) + $product['fees'], $product['vat'])->price ?></g:price>
		<?php if($manufacturer = Core::$controller->_getManufacturer($product)): ?>
		<g:brand><![CDATA[<?php echo $manufacturer?>]]></g:brand>
		<?php endif; ?>

		<?php if($product['ean13']): ?>
		<g:gtin><![CDATA[<?php echo $product['ean13']?>]]></g:gtin>
		<?php else: ?>
		<g:identifier_exists>FALSE</g:identifier_exists>
		<?php endif; ?>

		<?php if($product['sku']): ?>
		<g:mpn><![CDATA[<?php echo $product['sku']?>]]></g:mpn>
		<?php endif; ?>
	</item>
	<?php endforeach; Core::$controller->_flush(); $offset += 500; endwhile; ?>
</channel></rss>


<?php Core::$controller->_flush(); ?>