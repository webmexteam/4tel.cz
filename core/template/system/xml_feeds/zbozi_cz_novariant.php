<?php defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */

 echo '<?xml version="1.0" encoding="utf-8"?>'?>

<SHOP>
	<?php
	$offset = 0;
	$_products = null;
	$availablity_id = (int) Core::config('xml_feed_availability_id');

	while(
		($_products = Core::$db->product()->where('status', 1)->where('price != 1')->limit(100, $offset)) &&
		($availablity_id ? $_products->where('availability_id  = '.$availablity_id) : true) &&
		count($_products)
	):

	foreach($_products as $product):
		$manufacturer = Core::$controller->_getManufacturer($product);
		$categories = array();

		foreach($product->product_pages() as $page){
			$categories[] = Core::$controller->_getCategoryPath($page, ' | ');
		}

        $displayName = $product['name_feed']?: $product['name'];
        $displayDesc = $product['description_feed']?: $product['description_short'];

	?>

	<?php if(($attributes = $product->product_attributes()->order('name ASC, id ASC')) && count($attributes)): foreach($attributes as $attribute): ?>
	<SHOPITEM>
		<?php if($product['nameext']): ?>
		<PRODUCTNAME><![CDATA[<?php echo strip_tags($displayName)?>]]></PRODUCTNAME>
		<PRODUCTNAMEEXT><![CDATA[<?php echo strip_tags($product['nameext'].' '.$attribute['value'])?>]]></PRODUCTNAMEEXT>

		<?php else: ?>
		<PRODUCT><![CDATA[<?php echo strip_tags(trim($displayName).' '.$attribute['value'])?>]]></PRODUCT>
		<?php endif; ?>

		<DESCRIPTION><![CDATA[<?php echo strip_tags($displayDesc)?>]]></DESCRIPTION>
		<URL><?php echo url($product, array('attr' => $attribute['id']), true)?></URL>
		<UNFEATURED>0<?php /*echo ($product['paid_listing'] == 1 ? '0' : '1')*/ ?></UNFEATURED>

		<?php if (-1 < $product->availability["days"]): ?>

			<?php if ($product["stock"]): ?>

				<DELIVERY_DATE><?php echo $product->availability["days"]?></DELIVERY_DATE>

			<?php else: ?>

				<?php if (-1 < (int) Core::config("stock_out_availability")): ?>
					<DELIVERY_DATE><?php echo $outOfStockDeliveryDays ?></DELIVERY_DATE>

				<?php else: ?>
					<DEVLIVERY_DATE>-1</DELIVERY_DATE>

				<?php endif; ?>

			<?php endif; ?>

		<?php else: ?>

			<DELIVERY_DATE>-1</DELIVERY_DATE>

		<?php endif; ?>

		<?php if(($attribute['file_id'] && ($img = imgsrc(Core::$db->product_files[$attribute['file_id']], 3))) || ($img = imgsrc($product, 3))): ?>
		<IMGURL><?php echo preg_replace('/\s/', '%20', $site.$img)?></IMGURL>
		<?php endif; ?>

		<PRICE_VAT>
			<?php
			if (FALSE && (float)$product['units_in_pack']) {
				echo price_vat(
					($product['price'] + estPrice($attribute['price'], $product['price'])) / $product['units_in_pack'],
					$product->vat['rate']
				)->price;

			} else {
				echo price_vat(
					$product['price'] + estPrice($attribute['price'], $product['price']),
					$product['vat']
				)->price;
			}
			?>
		</PRICE_VAT>

		<?php if($fees = $product['fees']): ?>
		<DUES><?php echo price_vat($fees, $product['vat'])->price?></DUES>
		<?php endif; ?>

		<?php foreach($categories as $category): ?>
		<CATEGORYTEXT><![CDATA[<?php echo $category?>]]></CATEGORYTEXT>
		<?php endforeach; ?>

		<?php if($manufacturer): ?>
		<MANUFACTURER><![CDATA[<?php echo $manufacturer?>]]></MANUFACTURER>
		<?php endif; ?>

		<?php if(($ean = $attribute['ean13']) || ($ean = $product['ean13'])): ?>
		<EAN><?php echo $ean?></EAN>
		<?php endif; ?>
	</SHOPITEM>

	<?php endforeach; else: ?>

	<SHOPITEM>
		<?php if($product['nameext']): ?>
		<PRODUCTNAME><![CDATA[<?php echo strip_tags($displayName)?>]]></PRODUCTNAME>
		<PRODUCTNAMEEXT><![CDATA[<?php echo strip_tags($product['nameext'])?>]]></PRODUCTNAMEEXT>

		<?php else: ?>
		<PRODUCT><![CDATA[<?php echo strip_tags($displayName)?>]]></PRODUCT>
		<?php endif; ?>

		<DESCRIPTION><![CDATA[<?php echo strip_tags($displayDesc)?>]]></DESCRIPTION>
		<URL><?php echo url($product, null, true)?></URL>
		<UNFEATURED>0<?php /*echo ($product['paid_listing'] == 1 ? '0' : '1')*/ ?></UNFEATURED>

		<?php if (-1 < $product->availability["days"]): ?>

			<?php if ($product["stock"]): ?>

				<DELIVERY_DATE><?php echo $product->availability["days"]?></DELIVERY_DATE>

			<?php else: ?>

				<?php if (-1 < (int) Core::config("stock_out_availability")): ?>
					<DELIVERY_DATE><?php echo $outOfStockDeliveryDays ?></DELIVERY_DATE>

				<?php else: ?>
					<DEVLIVERY_DATE>-1</DELIVERY_DATE>

				<?php endif; ?>

			<?php endif; ?>

		<?php else: ?>

			<DELIVERY_DATE>-1</DELIVERY_DATE>

		<?php endif; ?>

		<?php if($img = imgsrc($product, 3)): ?>
		<IMGURL><?php echo preg_replace('/\s/', '%20', $site.$img)?></IMGURL>
		<?php endif; ?>

		<PRICE_VAT>
			<?php
			if (FALSE && (float)$product['units_in_pack']) {
				echo price_vat(
					$product['price'] / $product['units_in_pack'],
					$product->vat['rate']
				)->price;

			} else {
				echo price_vat(
					$product['price'],
					$product['vat']
				)->price;
			}
			?>
		</PRICE_VAT>

		<?php if($fees = $product['fees']): ?>
		<DUES><?php echo price_vat($fees, $product['vat'])->price?></DUES>
		<?php endif; ?>

		<?php foreach($categories as $category): ?>
		<CATEGORYTEXT><![CDATA[<?php echo $category?>]]></CATEGORYTEXT>
		<?php endforeach; ?>

		<?php if($manufacturer): ?>
		<MANUFACTURER><![CDATA[<?php echo $manufacturer?>]]></MANUFACTURER>
		<?php endif; ?>

		<?php if($product['ean13']): ?>
		<EAN><?php echo $product['ean13']?></EAN>
		<?php endif; ?>
	</SHOPITEM>
	<?php endif; ?>

	<?php endforeach; Core::$controller->_flush(); $offset += 100; endwhile; ?>
</SHOP>

<?php Core::$controller->_flush(); ?>
