<?php if($analytics = Core::config('analytics')): ?>
	<script>
	(function(i,s,o,g,r,a,m){ i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	ga('create', '<?php echo $analytics?>', '<?php echo $_SERVER["SERVER_NAME"] ?>');
	ga('send', 'pageview');
	ga('require', 'ecommerce', 'ecommerce.js'); // Načtení pluginu elektronického obchodu
	</script>
<?php endif; ?>



<?php if ($google_conversions = Core::config('google_conversions') && (Core::$current_product || $_products || Core::$current_page == Core::config('page_basket')) ): ?>
	<!-- Google Code for Konverze Prodej Conversion Page -->
	<?php
	// 1) Get data
	$ecommProdId = NULL;
	$ecommPageType = NULL;
	$ecommTotalValue = NULL;

	// Product detail
	if(Core::$current_product) {
		$product = $product?: Core::$db->product[Core::$current_product];

		$ecommProdId = $product['id'];
		$ecommPageType = 'product';
		$ecommTotalValue = price_vat($product['price'] + $product['fees'], $product['vat'])->price;
	
	// Category
	} elseif ($_products) {
		$ids = array(); 
		$prices = array();
		foreach ($_products as $product) {
			$ids[] = $product['id'];
			$prices[] = price_vat($product['price'] + $product['fees'], $product['vat'])->price;
		}

		$ecommProdId = '[' . join(', ', $ids) . ']';
		$ecommPageType = 'category';
		$ecommTotalValue = '[' . join(', ', $prices) . ']';

	// Basket
	} elseif (Core::$current_page == Core::config('page_basket')) {
		$ids = array(); 
		$prices = array(); 
		foreach (Basket::products() as $product) {
			$ids[] = $product['product']['id'];
			$prices[] = price_vat($product['product']['price'] + $product['product']['fees'], $product['product']['vat'])->price;
		}

		$ecommProdId = '[' . join(', ', $ids) . ']';
		$ecommPageType = 'cart';
		$ecommTotalValue = '[' . join(', ', $prices) . ']';
	}
	?>

	<?php 
	// 2) Render
	if ($ecommProdId && $ecommPageType && $ecommTotalValue): 
	?>
		<script type="text/javascript">
		var google_tag_params = {
			ecomm_prodid: <?php echo $ecommProdId ?>,
			ecomm_pagetype: '<?php echo $ecommPageType ?>',
			ecomm_totalvalue: <?php echo $ecommTotalValue ?> 
		};
		</script>

		<script type="text/javascript">
		/* <![CDATA[ */
		var google_conversion_id = <?php echo $google_conversions; ?>;
		var google_custom_params = window.google_tag_params;
		var google_remarketing_only = true;
		/* ]]> */
		</script>
		<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js"></script>
		<noscript>
			<div style="display:inline;">
			<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/<?php echo $google_conversions; ?>/?value=0&amp;guid=ON&amp;script=0"/>
			</div>
		</noscript> 
	<?php 
	endif; 
	?>
<?php endif; ?>