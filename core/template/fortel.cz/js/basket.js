$(function() {
	$(".quantity input").change(function() {
		$('.basket form button[name=update_qty]').trigger('click');
	});
	
	$(".qty_plus").click(function() {
		var input = $(this).parents(".quantity").find("input");
		var new_val = parseInt(input.val())+1;
		input.val(new_val);
		input.change();
                //$('.basket form').submit();
                $('.basket form button[name=update_qty]').trigger('click');
		return false;
	});
	$(".qty_minus").click(function() {
		var input = $(this).parents(".quantity").find("input");
		var val = parseInt(input.val());
		if(val > 1)
		{
			input.val(val-1);
			input.change();
		}
                //$('.basket form').submit();
                $('.basket form button[name=update_qty]').trigger('click');
		return false;
	});
});