/* dostupnost */
jQuery(document).ready(function($) {

	// Update table when attribute change
	jQuery('select[name="attributes[]"]').change(function(e){
		jQuery.ajax({
			url: '/webmex_delivery_info/table/24/' + jQuery(this).val(),
			method: 'GET',
			success: function(response){
				jQuery('#avtable .cont').html(response);
			}
		});
	});
    
    
    $('.allImgs a').click(function() {
        $('.detailImg li').css('display', 'block');
        $(this).parent().remove();
        
        return false;
    });
    
});

$(function() {
	$("#qty_plus").click(function() {
        
        // Stav skladu pro produkt
        var stock = $('.produkt-dostupnost-panel .stock').attr('data-product-stock');
        
		var new_val = parseInt($("input[name='qty']").val())+1;
        
        if($('.info').find('.stock_warning')) {
            $('.stock_warning').remove();
        }
        
        if(stock == undefined || stock >= new_val) {        
            $("input[name='qty']").val(new_val);
            prepocitejCeny(new_val);
        } else {
            $('.info').append('<div class="stock_warning" style="width: 100%; color: #ff0000; float: left;">Skladem máme pouze '+stock+' ks!</div>');
        }
		return false;
	});
	$("#qty_minus").click(function() {
		var val = parseInt($("input[name='qty']").val());
        
        if($('.info').find('.stock_warning')) {
            $('.stock_warning').remove();
        }
        
		if(val > 1)
		{
			$("input[name='qty']").val(val-1);
			prepocitejCeny(val-1);
		}
		return false;
	});
	$('select[name="attributes[]"]').change(function(){
		prepocitejCeny2();
	});
	prepocitejCeny2();
	
	$(".pridani_do_kosiku input[name='qty']").change(function() {
		prepocitejCeny($(this).val());
	});
	
	function prepocitejCeny(new_val) {
		//$(".info .pred").text(fprice($(".info .pred").data('pred')*new_val)+" "+_currency);
		$(".info .cena .hlavni").text(fprice($(".info .cena .hlavni").attr('data-cena')*new_val)+" "+_currency);
        $(".info .cena .unvat").text(fprice($(".info .cena .unvat").attr('data-cena')*new_val)+" "+_currency);
		var cenaeur = fprice((parseInt($(".info .cena .hlavni").attr('data-cena')) * parseInt($("input[name='qty']").val())) / $(".product_detail .info .cena small").attr("data-rate"));
		$(".product_detail .info .cena small").text(cenaeur + ' ' + $(".product_detail .info .cena small").attr("data-symbol"));
		//$(".info .usetrite").text(fprice($(".info .usetrite").data('usetrite')*new_val)+" "+_currency);
	}

	function prepocitejCeny2() {
		var $attributes = $('select[name="attributes[]"]');
		var $selectedAttribute = $attributes.find(":selected");
		var optid = $attributes.val();

		if (optid) {
			$(".info .cena .hlavni").text(fprice((parseInt($selectedAttribute.attr("data-cena")) + _product_price)*parseInt($("input[name='qty']").val()))+" "+_currency);
			$(".info .cena .hlavni").attr('data-cena', parseInt($selectedAttribute.attr("data-cena")) + _product_price);
			var customdostupnost = $selectedAttribute.attr("data-dostupnost");
			if (customdostupnost == '')
				customdostupnost = $(".info .cena .hlavni").attr('data-dostupnost');
			$(".produkt-dostupnost-panel .titulek").text(customdostupnost);
			var cenaeur = fprice((parseInt($(".info .cena .hlavni").attr('data-cena')) * parseInt($("input[name='qty']").val())) / $(".product_detail .info .cena small").attr("data-rate"));
			$(".product_detail .info .cena small").text(cenaeur + ' ' + $(".product_detail .info .cena small").attr("data-symbol"));
			
			if ($selectedAttribute.attr("data-file") != '') {
				$(".product_detail .fotky div:eq(0) figure a.fancybox").attr("href", $selectedAttribute.attr("data-file"));
				$(".product_detail .fotky div:eq(0) figure a.fancybox img").attr("src", $selectedAttribute.attr("data-file"));
			}

		} else {
			if (optid == '') {
				$(".info .cena .hlavni").text(fprice(_product_price*parseInt($("input[name='qty']").val()))+" "+_currency);
				$(".info .cena .hlavni").attr('data-cena', _product_price);
				$(".produkt-dostupnost-panel .titulek").text($(".info .cena .hlavni").attr('data-dostupnost'));
				var cenaeur = fprice((parseInt($(".info .cena .hlavni").attr('data-cena')) * parseInt($("input[name='qty']").val())) / $(".product_detail .info .cena small").attr("data-rate"));
				$(".product_detail .info .cena small").text(cenaeur + ' ' + $(".product_detail .info .cena small").attr("data-symbol"));
			}
		}

		var attrStock = product_attr_stock[$selectedAttribute.val()];
		var $stock = $('.detail .info .stock');
        if($stock.html() != undefined) {
    		if (attrStock !== undefined && attrStock !== null) {
                $stock.html( $stock.html().replace(/\d+/, attrStock) );
            } else {
                $stock.html( $stock.html().replace(/\d+/, $stock.data('product-stock')) )
            }
        }
	}
});