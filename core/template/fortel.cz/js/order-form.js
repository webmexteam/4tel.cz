$(function(){
	$(".deliveries input, .payments input").change(function() {
        
		$(this).closest(".panel").find(".active").removeClass("active");
		$(this).closest(".list-item").addClass('active');
		
		var delivery_price = 0;
        var unvat_delivery_price = 0;
        
		if ($("input[name='delivery']:checked").length) {
			delivery_price = parseInt($("input[name='delivery']:checked").data("price"));
            unvat_delivery_price = parseInt($("input[name='delivery']:checked").siblings("input[name='unvat_delivery']").val());
		}
        
		var payment_price = 0;
		var unvat_payment_price = 0;
        
		if ($("input[name='payment']:checked").length) {
			payment_price = parseInt($("input[name='payment']:checked").data("price"));
            unvat_payment_price = parseInt($("input[name='payment']:checked").siblings("input[name='unvat_payment']").val());
		}
		
		var dp_price = delivery_price+payment_price;
        var dp_price_unvat = unvat_delivery_price+unvat_payment_price;
        
		for (key in delivery_payments) {
			var dp = delivery_payments[key];
			if (dp.free_over && (_subtotal > dp.free_over)) {
				dp_price = 0;
                dp_price_unvat = 0;
			}
		}
		
		var $vat_incld_obj = $(".basketPanelCena-celkem .vat_incld");
		var $vat_excld_obj = $(".basketPanelCena-celkem .vat_excld");
        var $vat_obj = $(".basketPanelCena-celkem .vat");
        
        var vat = fprice((dp_price+$vat_incld_obj.data("price"))-(dp_price_unvat+$vat_excld_obj.data("price")));
        
		$vat_incld_obj.text( fprice(dp_price+$vat_incld_obj.data("price"))+" "+_currency );
		$vat_excld_obj.text( fprice(dp_price_unvat+$vat_excld_obj.data("price"))+" "+_currency );
        $vat_obj.text( fprice(vat)+" "+_currency );
		

		/*
		 * Delivery right info
		 */
		$(".payment_delivery").css("display", "block");
		$(".delivery .price").html('<i>vyberte způsob dopravy</i>');
		$(".delivery .name").text('');
		if ($("input[name='delivery']:checked").length) {
			$(".delivery .name").text( $("input[name='delivery']:checked").closest(".row.clearfix").find(".j-delivery-name-source").text() );
			
			if (delivery_price > 0) {
				$(".delivery .price").text( $("input[name='delivery']:checked").closest(".row.clearfix").find(".j-delivery-price").text() );
			} else {
				$(".delivery .price").text('0,00 Kč');
			}
		}
		

		/*
		 * Payment right info
		 */
		var $payment = $("input[name='payment']:checked");
		$(".payment .price").html('<i>vyberte způsob platby</i>');
		$(".payment .name").text('');
		if ($payment.length) {
			$(".payment .name").text( $payment.closest(".row.clearfix").find(".j-payment-name-source").text() );
			
			if (payment_price > 0) {
				$(".payment .price").text( $payment.closest(".row.clearfix").find(".j-payment-price").text() );
			} else {
				$(".payment .price").text('0,00 Kč');
			}
		}
	});
});