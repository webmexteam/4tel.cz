<?php defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */
?>


<h2>Hlavička</h2>
<div class="my-box">
	<div class="title-wrap">
		<h3>Obsah</h3>
	</div>
	<div class="content-wrap">
		<div class="row-fluid">
			<div class="span12">
				<?php echo forminput('text', 'opt[kvalitnidestniky.cz][header_1_body]', $options['kvalitnidestniky.cz']['header_1_body'], array('label' => '1. text v hlavičce'))?>
			</div>
		</div>
	</div>
</div>

<h2>Patička</h2>
<div class="my-box">
	<div class="title-wrap">
		<h3>Menu</h3>
	</div>
	<div class="content-wrap">
		<div class="row-fluid">
			<div class="span2">
				<?php echo forminput('text', 'opt[kvalitnidestniky.cz][footer_menu_1_title]', $options['kvalitnidestniky.cz']['footer_menu_1_title'], array('label' => 'Nadpis 1. menu v patičce'))?>
			</div>
			<div class="span2">
				<?php echo forminput('text', 'opt[kvalitnidestniky.cz][footer_menu_2_title]', $options['kvalitnidestniky.cz']['footer_menu_2_title'], array('label' => 'Nadpis 2. menu v patičce'))?>
			</div>
			<div class="span2">
				<?php echo forminput('text', 'opt[kvalitnidestniky.cz][footer_menu_3_title]', $options['kvalitnidestniky.cz']['footer_menu_3_title'], array('label' => 'Nadpis 3. menu v patičce'))?>
			</div>
			<div class="span2">
				<?php echo forminput('text', 'opt[kvalitnidestniky.cz][footer_menu_4_title]', $options['kvalitnidestniky.cz']['footer_menu_4_title'], array('label' => 'Nadpis 4. menu v patičce'))?>
			</div>
		</div>
	</div>

	<div class="title-wrap">
		<h3>Obsah</h3>
	</div>
	<div class="content-wrap">
		
			<div class="row-fluid">
				<div class="span12">
					<h4>Kontakt v patičce <a href="#" class="show-editor-tercovci" style="color:green">(zobrazit editor)</a></h4>
					<div class="editor-box-tercovci">
						<?php echo forminput('textarea', 'opt[kvalitnidestniky.cz][footer_menu_4_body]', $options['kvalitnidestniky.cz']['footer_menu_4_body'], array('cls' => 'editor full', 'label' => 'Obsah'))?>
					</div>
				</div>
			</div>
		
			<div class="row-fluid">
				<div class="span12">
					<h4 style="padding:3px 0;">Obsah v patičce <a href="#" class="show-editor-tercovci" style="color:green">(zobrazit editor)</a></h4>
					<div class="editor-box-tercovci">
						<?php echo forminput('textarea', 'opt[kvalitnidestniky.cz][footer_logos_body]', $options['kvalitnidestniky.cz']['footer_logos_body'], array('cls' => 'editor full', 'label' => 'Obsah'))?>
					</div>
				</div>
			</div>
	</div>
</div>

<script>
$(function(){
	$('.editor-box-tercovci').hide();
	$('.show-editor-tercovci').click(function(){
		if($(this).hasClass('active')){
			$(this).parent().parent().find('.editor-box-tercovci').hide();
			$(this).text('(zobrazit editor)').removeClass('active');
		}else{
			$(this).parent().parent().find('.editor-box-tercovci').show();
			$(this).text('(skrýt editor)').addClass('active');
		}
		return false;
	});
});
</script>