<?php

defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */
$db_tables = array(
	'availability' => array(
		'id' => array(
			'type' => 'integer',
			'primary' => true,
			'autoincrement' => true
		),
		'days' => array(
			'type' => 'integer',
		),
		'name' => array(
			'type' => 'varchar',
			'not_null' => true
		),
		'hex_color' => array(
			'type' => 'varchar',
			'not_null' => true
		)
	),
	'basket' => array(
		'id' => array(
			'type' => 'integer',
			'primary' => true,
			'autoincrement' => true
		),
		'last_update' => array(
			'type' => 'integer'
		),
		'key' => array(
			'type' => 'varchar',
			'not_null' => true,
			'index' => true
		),
		'voucher_id' => array(
			'type' => 'integer',
		)
	),
	'basket_products' => array(
		'id' => array(
			'type' => 'integer',
			'primary' => true,
			'autoincrement' => true
		),
		'basket_id' => array(
			'type' => 'integer',
			'not_null' => true
		),
		'product_id' => array(
			'type' => 'integer',
			'not_null' => true
		),
		'attributes' => array(
			'type' => 'varchar'
		),
		'qty' => array(
			'type' => 'integer',
			'not_null' => true,
			'default' => 1
		),
		'free_sample' => array(
			'type' => 'integer',
			'not_null' => false,
			'default' => 0
		),
	),
	'block' => array(
		'id' => array(
			'type' => 'integer',
			'primary' => true,
			'autoincrement' => true
		),
		'title' => array(
			'type' => 'varchar'
		),
		'text' => array(
			'type' => 'text'
		),
		'position' => array(
			'type' => 'integer'
		),
		'column' => array(
			'type' => 'varchar'
		),
		'driver' => array(
			'type' => 'varchar'
		),
		'config' => array(
			'type' => 'text'
		),
		'page_id' => array(
			'type' => 'integer'
		),
		'show_title' => array(
			'type' => 'integer',
			'not_null' => true,
			'default' => 0
		)
	),
	'banner' => array(
		'id' => array(
			'type' => 'integer',
			'primary' => true,
			'autoincrement' => true
		),
		'name' => array(
			'type' => 'varchar'
		),
		'type' => array(
			'type' => 'integer',
			'default' => 0,
			'not_null' => true,
		),
		'text' => array(
			'type' => 'text',
		),
		'image' => array(
			'type' => 'varchar'
		),
		'image_link' => array(
			'type' => 'text'
		),
		'link_new_window' => array(
			'type' => 'integer',
			'default' => 0,
			'not_null' => true,
		),
		'flash_width' => array(
			'type' => 'integer',
		),
		'flash_height' => array(
			'type' => 'integer',
		),
		'status' => array(
			'type' => 'integer',
			'default' => 0,
			'not_null' => true,
		),
	),
	'config' => array(
		'name' => array(
			'type' => 'varchar',
			'primary' => true
		),
		'value' => array(
			'type' => 'text'
		)
	),
	'country' => array(
		'id' => array(
			'type' => 'integer',
			'primary' => true,
			'autoincrement' => true
		),
		'name' => array(
			'type' => 'varchar',
			'not_null' => true,
			'unique' => true
		),
		'position' => array(
			'type' => 'integer'
		)
	),
	'customer' => array(
		'id' => array(
			'type' => 'integer',
			'primary' => true,
			'autoincrement' => true
		),
		'email' => array(
			'type' => 'varchar',
			'not_null' => true,
			'unique' => true
		),
		'password' => array(
			'type' => 'varchar'
		),
		'last_login' => array(
			'type' => 'integer'
		),
		'first_name' => array(
			'type' => 'varchar'
		),
		'last_name' => array(
			'type' => 'varchar'
		),
		'company' => array(
			'type' => 'varchar'
		),
		'street' => array(
			'type' => 'varchar'
		),
		'zip' => array(
			'type' => 'varchar'
		),
		'city' => array(
			'type' => 'varchar'
		),
		'country' => array(
			'type' => 'varchar'
		),
		'phone' => array(
			'type' => 'varchar'
		),
		'company_id' => array(
			'type' => 'varchar'
		),
		'company_vat' => array(
			'type' => 'varchar'
		),
		'ship_first_name' => array(
			'type' => 'varchar'
		),
		'ship_last_name' => array(
			'type' => 'varchar'
		),
		'ship_street' => array(
			'type' => 'varchar'
		),
		'ship_zip' => array(
			'type' => 'varchar'
		),
		'ship_country' => array(
			'type' => 'varchar'
		),
		'ship_city' => array(
			'type' => 'varchar'
		),
		'ship_company' => array(
			'type' => 'varchar'
		),
		'discount' => array(
			'type' => 'float'
		),
		'customer_group_id' => array(
			'type' => 'integer',
			'index' => true
		),
		'active' => array(
			'type' => 'integer',
			'length' => 1,
			'not_null' => true,
			'default' => 0
		),
		'type' => array(
			'type' => 'varchar',
			'length' => 20,
			'not_null' => true,
			'default' => 'website'
		)
	),
	'customer_group' => array(
		'id' => array(
			'type' => 'integer',
			'primary' => true,
			'autoincrement' => true
		),
		'name' => array(
			'type' => 'varchar',
			'not_null' => true
		),
		'coefficient' => array(
			'type' => 'float_long',
			'not_null' => true,
			'default' => 1
		)
	),
	'delivery' => array(
		'id' => array(
			'type' => 'integer',
			'primary' => true,
			'autoincrement' => true
		),
		'name' => array(
			'type' => 'varchar',
			'not_null' => true
		),
		'shortcut' => array(
			'type' => 'varchar'
		),
		'color' => array(
			'type' => 'varchar'
		),
		'driver' => array(
			'type' => 'varchar'
		),
		'price' => array(
			'type' => 'varchar'
		),
		'customer_group_id' => array(
			'type' => 'integer',
			'not_null' => true,
			'default' => 0
		),
		'weight_min' => array(
			'type' => 'float'
		),
		'weight_max' => array(
			'type' => 'float'
		),
		'price_min' => array(
			'type' => 'float'
		),
		'price_max' => array(
			'type' => 'float'
		),
		'ord' => array(
			'type' => 'integer'
		)
	),
	'delivery_payments' => array(
		'delivery_id' => array(
			'type' => 'integer',
			'primary' => true
		),
		'payment_id' => array(
			'type' => 'integer',
			'primary' => true
		),
		'price' => array(
			'type' => 'float'
		),
		'free_over' => array(
			'type' => 'float'
		)
	),
	'email_message' => array(
		'id' => array(
			'type' => 'integer',
			'primary' => true,
			'autoincrement' => true
		),
		'subject' => array(
			'type' => 'varchar'
		),
		'text' => array(
			'type' => 'text'
		),
		'key' => array(
			'type' => 'varchar',
			'not_null' => true,
			'unique' => true
		),
		'template' => array(
			'type' => 'varchar'
		)
	),
	'feature' => array(
		'id' => array(
			'type' => 'integer',
			'primary' => true,
			'autoincrement' => true
		),
		'name' => array(
			'type' => 'varchar',
			'not_null' => true
		),
		'position' => array(
			'type' => 'integer'
		),
		'unit' => array(
			'type' => 'varchar'
		),
		'type' => array(
			'type' => 'varchar',
			'not_null' => true,
			'default' => 'string'
		),
		'featureset_id' => array(
			'type' => 'integer',
			'index' => true
		),
		'value' => array(
			'type' => 'text'
		),
		'filter' => array(
			'type' => 'integer',
			'length' => 1,
			'not_null' => true,
			'default' => 0
		),
	),
	'featureset' => array(
		'id' => array(
			'type' => 'integer',
			'primary' => true,
			'autoincrement' => true
		),
		'name' => array(
			'type' => 'varchar'
		)
	),
	'label' => array(
		'id' => array(
			'type' => 'integer',
			'primary' => true,
			'autoincrement' => true
		),
		'name' => array(
			'type' => 'varchar',
		),
		'color' => array(
			'type' => 'varchar',
		)
	),
	'newsletter_recipient' => array(
		'id' => array(
			'type' => 'integer',
			'primary' => true,
			'autoincrement' => true
		),
		'email' => array(
			'type' => 'varchar',
			'unique' => true
		),
		'date' => array(
			'type' => 'integer'
		)
	),
	'notes' => array(
		'id' => array(
			'type' => 'integer',
			'primary' => true,
			'autoincrement' => true
		),
		'color' => array(
			'type' => 'varchar'
		),
		'message' => array(
			'type' => 'text',
		),
		'date' => array(
			'type' => 'integer'
		)
	),
	'order' => array(
		'id' => array(
			'type' => 'integer',
			'primary' => true,
			'autoincrement' => true
		),
		'received' => array(
			'type' => 'integer'
		),
		'customer_id' => array(
			'type' => 'integer',
			'index' => true
		),
		'status' => array(
			'type' => 'integer',
			'length' => 2,
			'not_null' => true,
			'default' => 1
		),
		'ip' => array(
			'type' => 'varchar'
		),
		'phone' => array(
			'type' => 'varchar'
		),
		'email' => array(
			'type' => 'varchar'
		),
		'first_name' => array(
			'type' => 'varchar'
		),
		'last_name' => array(
			'type' => 'varchar'
		),
		'street' => array(
			'type' => 'varchar'
		),
		'zip' => array(
			'type' => 'varchar'
		),
		'city' => array(
			'type' => 'varchar'
		),
		'country' => array(
			'type' => 'varchar'
		),
		'company' => array(
			'type' => 'varchar'
		),
		'company_id' => array(
			'type' => 'varchar'
		),
		'company_vat' => array(
			'type' => 'varchar'
		),
		'ship_first_name' => array(
			'type' => 'varchar'
		),
		'ship_last_name' => array(
			'type' => 'varchar'
		),
		'ship_street' => array(
			'type' => 'varchar'
		),
		'ship_zip' => array(
			'type' => 'varchar'
		),
		'ship_city' => array(
			'type' => 'varchar'
		),
		'ship_country' => array(
			'type' => 'varchar'
		),
		'ship_company' => array(
			'type' => 'varchar'
		),
		'last_change' => array(
			'type' => 'integer'
		),
		'last_change_user' => array(
			'type' => 'integer'
		),
		'track_num' => array(
			'type' => 'varchar'
		),
		'comment' => array(
			'type' => 'text'
		),
		'delivery_id' => array(
			'type' => 'integer'
		),
		'payment_id' => array(
			'type' => 'integer'
		),
		'payment_realized' => array(
			'type' => 'integer'
		),
		'payment_status' => array(
			'type' => 'varchar'
		),
		'status_log' => array(
			'type' => 'text'
		),
		'delivery_payment' => array(
			'type' => 'float'
		),
		'total_price' => array(
			'type' => 'float'
		),
		'total_incl_vat' => array(
			'type' => 'float'
		),
		'stock_recalc' => array(
			'type' => 'integer',
			'length' => 2,
			'not_null' => true,
			'default' => 0
		),
		'sync_time' => array(
			'type' => 'integer'
		),
		'payment_session' => array(
			'type' => 'varchar'
		),
		'encrypted_signature' => array(
			'type' => 'varchar'
		),
		'payment_description' => array(
			'type' => 'varchar'
		),
		'vat_payer' => array(
			'type' => 'integer',
			'length' => 2,
			'not_null' => true,
			'default' => 0
		),
		'voucher_id' => array(
			'type' => 'integer',
		),
		'confirmed' => array(
			'type' => 'integer',
		),
		'currency' => array(
			'type' => 'varchar',
			'length' => 24
		),
		'send_email_to_customer' => array(
			'type' => 'integer',
			'default' => 0,
			'not_null' => true,
		)
	),
	'order_comments' => array(
		'id' => array(
			'type' => 'integer',
			'primary' => true,
			'autoincrement' => true
		),
		'order_id' => array(
			'type' => 'integer',
			'not_null' => true
		),
		'user_id' => array(
			'type' => 'integer',
			'not_null' => true
		),
		'time' => array(
			'type' => 'integer'
		),
		'comment' => array(
			'type' => 'text'
		),
		'is_private' => array(
			'type' => 'integer',
			'length' => 1,
			'not_null' => true,
			'default' => 1
		)
	),
	'order_products' => array(
		'id' => array(
			'type' => 'integer',
			'primary' => true,
			'autoincrement' => true
		),
		'order_id' => array(
			'type' => 'integer',
			'not_null' => true
		),
		'product_id' => array(
			'type' => 'integer'
		),
		'price' => array(
			'type' => 'float'
		),
		'quantity' => array(
			'type' => 'integer'
		),
		'vat' => array(
			'type' => 'float'
		),
		'sku' => array(
			'type' => 'varchar'
		),
		'name' => array(
			'type' => 'varchar'
		),
		'ean13' => array(
			'type' => 'varchar'
		),
		'type' => array(
			'type' => 'integer',
			'default' => 0,
			'not_null' => true
		)
	),
	'page' => array(
		'id' => array(
			'type' => 'integer',
			'primary' => true,
			'autoincrement' => true
		),
		'parent_page' => array(
			'type' => 'integer'
		),
		'status' => array(
			'type' => 'integer',
			'length' => 1,
			'not_null' => true,
			'default' => 0
		),
		'position' => array(
			'type' => 'integer',
			'index' => true
		),
		'menu' => array(
			'type' => 'integer'
		),
		'sef_url' => array(
			'type' => 'varchar'
		),
		'name' => array(
			'type' => 'varchar'
		),
		'description_short' => array(
			'type' => 'text'
		),
		'description' => array(
			'type' => 'text'
		),
		'products' => array(
			'type' => 'integer',
			'length' => 1,
			'not_null' => true,
			'default' => 0
		),
		'rss' => array(
			'type' => 'integer',
			'length' => 1,
			'not_null' => true,
			'default' => 0
		),
		'title' => array(
			'type' => 'varchar'
		),
		'meta_description' => array(
			'type' => 'varchar'
		),
		'meta_keywords' => array(
			'type' => 'varchar'
		),
		'heureka_category' => array(
			'type' => 'varchar'
		),
		'last_change' => array(
			'type' => 'integer'
		),
		'last_change_user' => array(
			'type' => 'integer'
		),
		'layout_columns' => array(
			'type' => 'integer',
			'not_null' => true,
			'default' => 3
		),
		'subpages' => array(
			'type' => 'varchar',
			'not_null' => true,
			'default' => 'list'
		),
		'pubdate' => array(
			'type' => 'integer'
		),
		'product_columns' => array(
			'type' => 'integer',
			'not_null' => true,
			'default' => 0
		),
		'authorization' => array(
			'type' => 'integer',
			'length' => 1,
			'not_null' => true,
			'default' => 0
		),
		'external_url' => array(
			'type' => 'varchar'
		),
		'content_file' => array(
			'type' => 'varchar'
		),
		'menu_image' => array(
			'type' => 'varchar'
		),
		'subpages_position' => array(
			'type' => 'integer',
			'not_null' => true,
			'default' => 1
		),
		'inherit_products' => array(
			'type' => 'integer',
			'length' => 1,
			'not_null' => true,
			'default' => 0
		),
		'enable_filter' => array(
			'type' => 'integer',
			'length' => 1,
			'not_null' => true,
			'default' => 1
		),
		'discount' => array(
			'type' => 'float',
			'index' => true
		),
		'discount_date' => array(
			'type' => 'integer'
		),
		'sync_id' => array(
			'type' => 'integer'
		),
		'seo_title' => array(
			'type' => 'varchar'
		),
		'featureset_id' => array(
			'type' => 'integer'
		)
	),
	'page_files' => array(
		'id' => array(
			'type' => 'integer',
			'primary' => true,
			'autoincrement' => true
		),
		'page_id' => array(
			'type' => 'integer',
			'not_null' => true,
			'index' => true
		),
		'filename' => array(
			'type' => 'varchar',
			'not_null' => true
		),
		'description' => array(
			'type' => 'varchar'
		),
		'position' => array(
			'type' => 'integer'
		),
		'size' => array(
			'type' => 'integer'
		),
		'is_image' => array(
			'type' => 'integer',
			'length' => 1,
			'not_null' => true
		),
		'align' => array(
			'type' => 'integer'
		)
	),
	'payment' => array(
		'id' => array(
			'type' => 'integer',
			'primary' => true,
			'autoincrement' => true
		),
		'name' => array(
			'type' => 'varchar',
			'not_null' => true
		),
		'driver' => array(
			'type' => 'varchar'
		),
		'price' => array(
			'type' => 'varchar'
		),
		'config' => array(
			'type' => 'text'
		),
		'customer_group_id' => array(
			'type' => 'integer',
			'not_null' => true,
			'default' => 0
		),
		'weight_min' => array(
			'type' => 'float'
		),
		'weight_max' => array(
			'type' => 'float'
		),
		'price_min' => array(
			'type' => 'float'
		),
		'price_max' => array(
			'type' => 'float'
		),
		'info' => array(
			'type' => 'text'
		),
		'ord' => array(
			'type' => 'integer'
		)
	),
	'product' => array(
		'id' => array(
			'type' => 'integer',
			'primary' => true,
			'autoincrement' => true
		),
		'name' => array(
			'type' => 'varchar',
			'not_null' => true
		),
		'nameext' => array(
			'type' => 'varchar'
		),
		'status' => array(
			'type' => 'integer',
			'length' => 1,
			'not_null' => true,
			'default' => 0
		),
		'hide_product_on_zero_stock' => array(
			'type'     => 'integer',
			'length'   => 1,
			'not_null' => true,
			'default'  => 0
		),
		'position' => array(
			'type' => 'integer',
			'index' => true
		),
		'description_short' => array(
			'type' => 'text'
		),
		'description' => array(
			'type' => 'text'
		),
		'price' => array(
			'type' => 'float'
		),
		'price_old' => array(
			'type' => 'float'
		),
		'stock' => array(
			'type' => 'integer'
		),
		'stock_out_availability' => array(
			'type' => 'integer',
			'not_null' => true,
			'default' => 0,
		),
		'last_change' => array(
			'type' => 'integer'
		),
		'last_change_user' => array(
			'type' => 'integer'
		),
		'sku' => array(
			'type' => 'varchar'
		),
		'unit' => array(
			'type' => 'varchar',
			'length' => 10
		),
		'ean13' => array(
			'type' => 'varchar'
		),
		'productno' => array(
			'type' => 'varchar'
		),
		'availability_id' => array(
			'type' => 'integer'
		),
		'promote' => array(
			'type' => 'integer',
			'length' => 1,
			'not_null' => true,
			'default' => 0,
			'index' => true
		),
		'layout_columns' => array(
			'type' => 'integer',
			'not_null' => true,
			'default' => 3
		),
		'sef_url' => array(
			'type' => 'varchar'
		),
		'title' => array(
			'type' => 'varchar'
		),
		'meta_description' => array(
			'type' => 'varchar'
		),
		'meta_keywords' => array(
			'type' => 'varchar'
		),
		'heureka_name' => array(
			'type' => 'varchar'
		),
		'guarantee' => array(
			'type' => 'varchar'
		),
		'recycling_fee' => array(
			'type' => 'float'
		),
		'copyright_fee' => array(
			'type' => 'float'
		),
		'discount' => array(
			'type' => 'float'
		),
		'discount_date' => array(
			'type' => 'integer'
		),
		'paid_listing' => array(
			'type' => 'integer',
			'length' => 1,
			'not_null' => true,
			'default' => 0
		),
		'show_sku' => array(
			'type' => 'integer',
			'length' => 1,
			'not_null' => true,
			'default' => 1
		),
		'show_ean13' => array(
			'type' => 'integer',
			'length' => 1,
			'not_null' => true,
			'default' => 0
		),
		'list_attributes' => array(
			'type' => 'integer',
			'length' => 1,
			'not_null' => true,
			'default' => 0
		),
		'sold_qty' => array(
			'type' => 'integer',
			'index' => true
		),
		'lock_data' => array(
			'type' => 'integer',
			'length' => 1,
			'not_null' => true,
			'default' => 0
		),
		'lock_price' => array(
			'type' => 'integer',
			'length' => 1,
			'not_null' => true,
			'default' => 0
		),
		'lock_stock' => array(
			'type' => 'integer',
			'length' => 1,
			'not_null' => true,
			'default' => 0
		),
		'featureset_id' => array(
			'type' => 'integer'
		),
		'default_page' => array(
			'type' => 'integer'
		),
		'vat_id' => array(
			'type' => 'integer'
		),
		'weight' => array(
			'type' => 'float'
		),
		'cost' => array(
			'type' => 'float',
		),
		'margin' => array(
			'type' => 'varchar',
		),
		'sales' => array(
			'type' => 'integer',
			'not_null' => true,
			'default' => 0
		),
		'ignore_discounts' => array(
			'type' => 'integer',
			'length' => 1,
			'not_null' => true,
			'default' => 0
		),
		'import_unique' => array(
			'type' => 'varchar',
			'length' => 48,
			'index' => true
		),
		'import_schema' => array(
			'type' => 'varchar',
			'length' => 48,
			'index' => true
		),
		'import_uid' => array(
			'type' => 'varchar',
			'length' => 13,
			'index' => true
		),
		'free_sample' => array(
			'type' => 'integer',
			'not_null' => false,
			'default' => 0,
		),
	),
	'product_attributes' => array(
		'id' => array(
			'type' => 'integer',
			'primary' => true,
			'autoincrement' => true
		),
		'product_id' => array(
			'type' => 'integer',
			'not_null' => true,
			'index' => true
		),
		'name' => array(
			'type' => 'varchar'
		),
		'value' => array(
			'type' => 'varchar'
		),
		'price' => array(
			'type' => 'varchar'
		),
		'weight' => array(
			'type' => 'float',
			'not_null' => true
		),
		'is_default' => array(
			'type' => 'integer',
			'length' => 1,
			'not_null' => true,
			'default' => 0
		),
		'sku' => array(
			'type' => 'varchar',
			'index' => true
		),
		'ean13' => array(
			'type' => 'varchar'
		),
		'stock' => array(
			'type' => 'integer'
		),
		'availability_id' => array(
			'type' => 'integer'
		),
		'file_id' => array(
			'type' => 'integer'
		)
	),
	'product_attribute_template' => array(
		'id' => array(
			'type' => 'integer',
			'primary' => true,
			'autoincrement' => true
		),
		'name' => array(
			'type' => 'varchar',
			'not_null' => true,
			'default' => ''
		)
	),
	'product_attribute_template_items' => array(
		'id' => array(
			'type' => 'integer',
			'primary' => true,
			'autoincrement' => true
		),
		'product_attribute_template_id' => array(
			'type' => 'integer',
			'index' => true
		),
		'name' => array(
			'type' => 'varchar',
			'not_null' => true,
			'default' => ''
		),
		'value' => array(
			'type' => 'varchar'
		),
		'price' => array(
			'type' => 'varchar',
			'length' => 24
		)
	),
	'product_discounts' => array(
		'id' => array(
			'type' => 'integer',
			'primary' => true,
			'autoincrement' => true
		),
		'product_id' => array(
			'type' => 'integer',
			'not_null' => true,
			'index' => true
		),
		'quantity' => array(
			'type' => 'integer',
			'not_null' => true
		),
		'value' => array(
			'type' => 'float',
			'not_null' => true,
			'default' => 0
		)
	),
	'product_files' => array(
		'id' => array(
			'type' => 'integer',
			'primary' => true,
			'autoincrement' => true
		),
		'product_id' => array(
			'type' => 'integer',
			'not_null' => true,
			'index' => true
		),
		'filename' => array(
			'type' => 'varchar',
			'not_null' => true
		),
		'description' => array(
			'type' => 'varchar'
		),
		'position' => array(
			'type' => 'integer'
		),
		'size' => array(
			'type' => 'integer'
		),
		'is_image' => array(
			'type' => 'integer',
			'length' => 1,
			'not_null' => true
		),
		'align' => array(
			'type' => 'integer'
		)
	),
	'product_pages' => array(
		'product_id' => array(
			'type' => 'integer',
			'primary' => true
		),
		'page_id' => array(
			'type' => 'integer',
			'primary' => true
		)
	),
	'product_labels' => array(
		'product_id' => array(
			'type' => 'integer',
			'primary' => true
		),
		'label_id' => array(
			'type' => 'integer',
			'primary' => true
		)
	),
	'product_related' => array(
		'product_id' => array(
			'type' => 'integer',
			'primary' => true
		),
		'related_id' => array(
			'type' => 'integer',
			'primary' => true
		)
	),
	'user' => array(
		'id' => array(
			'type' => 'integer',
			'primary' => true,
			'autoincrement' => true
		),
		'username' => array(
			'type' => 'varchar',
			'not_null' => true
		),
		'email' => array(
			'type' => 'varchar',
			'not_null' => true
		),
		'password' => array(
			'type' => 'varchar',
			'not_null' => true
		),
		'name' => array(
			'type' => 'varchar'
		),
		'last_login' => array(
			'type' => 'integer'
		),
		'enable_login' => array(
			'type' => 'integer',
			'length' => 1,
			'not_null' => true,
			'default' => 0
		),
		'bad_login_count' => array(
			'type' => 'integer',
			'length' => 3,
			'not_null' => true,
			'default' => 0,
		),
		'security_blocked' => array(
			'type' => 'integer',
			'length' => 1,
			'not_null' => true,
			'default' => 0,
		),
	),
	'user_permissions' => array(
		'user_id' => array(
			'type' => 'integer',
			'primary' => true
		),
		'permission_id' => array(
			'type' => 'integer',
			'primary' => true
		)
	),
	'invoice' => array(
		'id' => array(
			'type' => 'integer',
			'primary' => true,
			'autoincrement' => true
		),
		'type' => array(
			'type' => 'varchar',
			'length' => 24,
			'not_null' => true,
			'default' => 'invoice'
		),
		'order_id' => array(
			'type' => 'integer',
			'index' => true
		),
		'status' => array(
			'type' => 'integer',
			'length' => 2,
			'not_null' => true,
			'default' => 1
		),
		'invoice_num' => array(
			'type' => 'varchar',
			'not_null' => true
		),
		'customer_name' => array(
			'type' => 'varchar'
		),
		'company_id' => array(
			'type' => 'varchar',
			'length' => 24
		),
		'company_vat' => array(
			'type' => 'varchar',
			'length' => 24
		),
		'address' => array(
			'type' => 'text'
		),
		'payment' => array(
			'type' => 'varchar'
		),
		'delivery' => array(
			'type' => 'varchar'
		),
		'symbol' => array(
			'type' => 'varchar'
		),
		'date_issue' => array(
			'type' => 'integer',
			'not_null' => true
		),
		'date_due' => array(
			'type' => 'integer'
		),
		'date_vat' => array(
			'type' => 'integer'
		),
		'vat' => array(
			'type' => 'float'
		),
		'date_create' => array(
			'type' => 'integer',
			'not_null' => true
		),
		'last_change' => array(
			'type' => 'integer'
		),
		'last_change_user' => array(
			'type' => 'integer'
		),
		'note' => array(
			'type' => 'text',
			'not_null' => true,
		),
		'customer_id' => array(
			'type' => 'integer'
		),
		'vat_payer' => array(
			'type' => 'integer',
			'length' => 2,
			'not_null' => true,
			'default' => 0
		),
		'voucher_id' => array(
			'type' => 'integer',
		),
		'shipping_address' => array(
			'type' => 'text'
		)
	),
	'invoice_items' => array(
		'id' => array(
			'type' => 'integer',
			'primary' => true,
			'autoincrement' => true
		),
		'invoice_id' => array(
			'type' => 'integer',
			'not_null' => true,
			'index' => true
		),
		'sku' => array(
			'type' => 'varchar'
		),
		'name' => array(
			'type' => 'varchar'
		),
		'quantity' => array(
			'type' => 'integer'
		),
		'price' => array(
			'type' => 'float'
		),
		'vat' => array(
			'type' => 'float'
		)
	),
	'module' => array(
		'id' => array(
			'type' => 'integer',
			'primary' => true,
			'autoincrement' => true
		),
		'name' => array(
			'type' => 'varchar',
			'unique' => true
		),
		'version' => array(
			'type' => 'varchar'
		),
		'status' => array(
			'type' => 'integer',
			'length' => 2,
			'not_null' => true,
			'default' => 0
		),
		'storage' => array(
			'type' => 'text'
		)
	),
	'redirect' => array(
		'id' => array(
			'type' => 'integer',
			'primary' => true,
			'autoincrement' => true
		),
		'url' => array(
			'type' => 'varchar',
			'not_null' => true,
			'index' => true
		),
		'object_type' => array(
			'type' => 'varchar',
			'not_null' => true
		),
		'object_id' => array(
			'type' => 'integer',
			'not_null' => true
		)
	),
	'registry' => array(
		'name' => array(
			'type' => 'varchar',
			'length' => 32,
			'primary' => true
		),
		'value' => array(
			'type' => 'text'
		)
	),
	'search_word' => array(
		'id' => array(
			'type' => 'integer',
			'primary' => true,
			'autoincrement' => true
		),
		'word' => array(
			'type' => 'varchar',
			'not_null' => true,
			'unique' => true
		)
	),
	'search_index' => array(
		'product_id' => array(
			'type' => 'integer',
			'primary' => true
		),
		'search_word_id' => array(
			'type' => 'integer',
			'primary' => true
		),
		'weight' => array(
			'type' => 'float',
			'not_null' => true
		)
	),
	'static_file' => array(
		'id' => array(
			'type' => 'integer',
			'primary' => true,
			'autoincrement' => true
		),
		'localname' => array(
			'type' => 'varchar',
			'not_null' => true,
			'unique' => true
		),
		'url' => array(
			'type' => 'varchar',
		),
		'mimetype' => array(
			'type' => 'varchar',
		),
		'content' => array(
			'type' => 'blob',
		),
	),
	'vat' => array(
		'id' => array(
			'type' => 'integer',
			'primary' => true,
			'autoincrement' => true
		),
		'name' => array(
			'type' => 'varchar'
		),
		'rate' => array(
			'type' => 'float'
		)
	),
	'voucher' => array(
		'id' => array(
			'type' => 'integer',
			'primary' => true,
			'autoincrement' => true
		),
		'code' => array(
			'type' => 'varchar',
			'not_null' => true,
		),
		'value' => array(
			'type' => 'varchar',
			'not_null' => true,
		),
		'name' => array(
			'type' => 'varchar'
		),
		'max_quantity' => array(
			'type' => 'integer'
		),
		'quantity_per_customer' => array(
			'type' => 'integer'
		),
		'valid_from' => array(
			'type' => 'integer'
		),
		'valid_to' => array(
			'type' => 'integer'
		),
		'status' => array(
			'type' => 'integer',
			'length' => 1,
			'not_null' => true,
			'default' => 1
		),
		'last_change' => array(
			'type' => 'integer'
		),
		'last_change_user' => array(
			'type' => 'integer'
		),
		'used' => array(
			'type' => 'integer',
			'not_null' => true,
			'default' => 0
		),
		'generator_id' => array(
			'type' => 'integer',
		),
		'min_price' => array(
			'type' => 'float',
		)
	),
	'voucher_orders' => array(
		'voucher_id' => array(
			'type' => 'integer',
			'primary' => true,
			'not_null' => true
		),
		'order_id' => array(
			'type' => 'integer',
			'primary' => true,
			'not_null' => true
		),
		'date' => array(
			'type' => 'integer',
			'not_null' => true
		)
	),
	'voucher_generator' => array(
		'id' => array(
			'type' => 'integer',
			'primary' => true,
			'autoincrement' => true
		),
		'value' => array(
			'type' => 'varchar',
			'not_null' => true,
		),
		'type' => array(
			'type' => 'integer',
			'default' => 1,
			'not_null' => true
		),
		'event' => array(
			'type' => 'varchar',
		),
		'name' => array(
			'type' => 'varchar'
		),
		'max_quantity' => array(
			'type' => 'integer'
		),
		'customer_group_id' => array(
			'type' => 'float'
		),
		'min_price' => array(
			'type' => 'float'
		),
		'valid_from' => array(
			'type' => 'integer'
		),
		'valid_to' => array(
			'type' => 'integer'
		),
		'status' => array(
			'type' => 'integer',
			'length' => 1,
			'not_null' => true,
			'default' => 1
		),
		'last_change' => array(
			'type' => 'integer'
		),
		'last_change_user' => array(
			'type' => 'integer'
		),
		'used' => array(
			'type' => 'integer',
			'not_null' => true,
			'default' => 0
		)
	),
  'rotator' => array(
    	'id' => array(
				'type' => 'integer',
				'primary' => true,
				'autoincrement' => true
			),
			'name' => array(
				'type' => 'varchar',
				'not_null' => true,
				'default' => ''
			),
			'width' => array(
				'type' => 'integer',
				'not_null' => true,
				'default' => '0'
			),
			'height' => array(
				'type' => 'integer',
				'not_null' => true,
				'default' => '0'
			),
			'interval' => array(
				'type' => 'integer',
				'not_null' => true,
				'default' => '0'
			),
			'last_change' => array(
				'type' => 'integer',
			),
			'last_change_user' => array(
				'type' => 'integer',
			)  
  )
);

$db_extras = <<<END
INSERT INTO `config` VALUES('dbversion','1.0.0');
INSERT INTO `config` VALUES('template','stonegallery');
INSERT INTO `config` VALUES('template_style','default');
INSERT INTO `config` VALUES('template_theme','default');
INSERT INTO `config` VALUES('title','%s');
INSERT INTO `config` VALUES('description','');
INSERT INTO `config` VALUES('keywords','');
INSERT INTO `config` VALUES('limit_records_admin','15');
INSERT INTO `config` VALUES('limit_records','10');
INSERT INTO `config` VALUES('default_order','position ASC');
INSERT INTO `config` VALUES('vat_mode','exclude');
INSERT INTO `config` VALUES('display_stock','1');
INSERT INTO `config` VALUES('suspend_no_stock','0');
INSERT INTO `config` VALUES('stock_notify','5');
INSERT INTO `config` VALUES('stock_auto','1');
INSERT INTO `config` VALUES('first_login','1');
INSERT INTO `config` VALUES('maintenance','0');
INSERT INTO `config` VALUES('watermark_apply_on_original','0');
INSERT INTO `config` VALUES('watermark_min_size','250');
INSERT INTO `config` VALUES('watermark_type','text');
INSERT INTO `config` VALUES('watermark_filepath','files/watermark.png');
INSERT INTO `config` VALUES('watermark_text','');
INSERT INTO `config` VALUES('watermark_alignment','c');
INSERT INTO `config` VALUES('watermark_font_color','255, 255, 255');
INSERT INTO `config` VALUES('watermark_font_size','30');
INSERT INTO `config` VALUES('watermark_opacity','50');
INSERT INTO `config` VALUES('watermark_png24','0');
INSERT INTO `config` VALUES('watermark_text_shadow','1');
INSERT INTO `config` VALUES('hide_no_stock_products','0');
INSERT INTO `config` VALUES('account_num','');
INSERT INTO `config` VALUES('ga_username','');
INSERT INTO `config` VALUES('ga_password','');
INSERT INTO `config` VALUES('ga_profile','');
INSERT INTO `config` VALUES('google_conversions','');
INSERT INTO `config` VALUES('phone','');

INSERT INTO `config` VALUES("conversion_rate_script",'<script type="text/javascript">
var _hrq = _hrq || [];
_hrq.push(["setKey", "%s"]);
_hrq.push(["setOrderId", "%s"]);

%s

_hrq.push(["trackOrder"]);

(function() {
var ho = document.createElement("script"); ho.type = "text/javascript"; ho.async = true;
ho.src = ("https:" == document.location.protocol ? "https://ssl" : "http://www") + ".heureka.cz/direct/js/cache/1-roi-async.js";
var s = document.getElementsByTagName("script")[0]; s.parentNode.insertBefore(ho, s);
})();
</script>');

INSERT INTO `config` VALUES("invoice_issuer","Firma, s.r.o.
Ulice 41
Praha 102 01

IČ:   123 456 78
DIČ:  CZ123 456 78

Účet číslo:      123456789/2700
Bankovní ústav:  UniCredit Bank");

INSERT INTO `config` VALUES ("invoice_footer","Vystavil: , {date}");
INSERT INTO `config` VALUES ("invoice_due","14");
INSERT INTO `config` VALUES ("invoice_num","20100001");
INSERT INTO `config` VALUES ('analytics','');
INSERT INTO `config` VALUES ('heureka_overeno','');
INSERT INTO `config` VALUES ('hide_webmex_link','0');
INSERT INTO `config` VALUES ('footer','Copyright &copy; {year}');
INSERT INTO `config` VALUES ('enable_customer_login','1');
INSERT INTO `config` VALUES ('customer_group_default','1');
INSERT INTO `config` VALUES ('customer_group_registered','2');
INSERT INTO `config` VALUES ('pohoda','{"ico":"","stock_internet_only":"1","stock_enable_insert":"1","stock_pictures":"1","stock_pictures_root":"files\/","stock_price_only":"0"}');
INSERT INTO `config` VALUES ('html_header','');
INSERT INTO `config` VALUES ('stock_out_availability','0');
INSERT INTO `config` VALUES ('last_lcheck','0');
INSERT INTO `config` VALUES ('invoice_symbol','order_id');
INSERT INTO `config` VALUES ('hide_prices_to_visitors','0');
INSERT INTO `config` VALUES ('heureka_conversions','');
INSERT INTO `config` VALUES ('sklik_conversions','');
INSERT INTO `config` VALUES ('vat_rate_default','1');
INSERT INTO `config` VALUES ('vat_payer','1');
INSERT INTO `config` VALUES ('vat_delivery','1');
INSERT INTO `config` VALUES ('invoice_price_format','1');
INSERT INTO `config` VALUES ('order_round_decimals','2');
INSERT INTO `config` VALUES ('customer_confirmation','0');
INSERT INTO `config` VALUES ('enable_vouchers','0');
INSERT INTO `config` VALUES ('attributes_prices_change','0');
INSERT INTO `config` VALUES ('allow_order_payment_change','0');
INSERT INTO `config` VALUES ('last_cache_clean','0');
INSERT INTO `config` VALUES ('send_order_status','1');
INSERT INTO `config` VALUES ('sitemap_products','0');
INSERT INTO `config` VALUES ('upload_driver','auto');
INSERT INTO `config` VALUES ('confirm_orders','0');
INSERT INTO `config` VALUES ('url_full_path','0');
INSERT INTO `config` VALUES ('meta_robots','index,follow');
INSERT INTO `config` VALUES ('meta_author','');
INSERT INTO `config` VALUES ('image_driver','gd');
INSERT INTO `config` VALUES ('xml_feeds','static');
INSERT INTO `config` VALUES ('price_vat_round',-1);
INSERT INTO `config` VALUES ('xml_feeds_ttl',60);
INSERT INTO `config` VALUES ('phone_required',1);
INSERT INTO `config` VALUES ('xml_feed_availability_id',0);
INSERT INTO `config` VALUES ('upload_unique_names',1);
INSERT INTO `config` VALUES ('jpeg_quality',100);
INSERT INTO `config` VALUES ('ajaxbasket',0);
INSERT INTO `config` VALUES ('stock_out_set_status',0);
INSERT INTO `config` VALUES ('url_rewrite',0);
INSERT INTO `config` VALUES ('zbozi_conversions','');
INSERT INTO `config` VALUES ('default_unit','ks');
INSERT INTO `config` VALUES ('business_conditions_pdf','ks');
INSERT INTO `config` VALUES ('business_conditions_txt','ks');
END;
