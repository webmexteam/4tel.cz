<?php

defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */
class Controller_Paypal extends Controller
{

	public function __construct()
	{
		parent::__construct();

		$this->content = tpl('default_1columns.latte');

		if (Core::$uri != 'paypal/check') {
			Paypal::setup();
		}
	}

	public function process_payment($hash)
	{
		if ($hash) {
			if ($parts = explode('_', $hash)) {
				if (($order = Core::$db->order[(int) $parts[0]]) && md5($order['id'] . $order['received'] . $order['email']) == $parts[1]) {

					if (!(int) $order['payment_realized']) {
						Paypal::pay($order);
					} else {
						redirect('paypal/fail');
					}
				}
			}
		}
	}

	public function callback()
	{
		if (Paypal::callback() !== true) {
			redirect('paypal/error');
		} else {
			redirect('paypal/success');
		}
	}

	public function success($channel = null)
	{
		$order_id = 0;

		if (!isSet($_SESSION['paypal']) || empty($_SESSION['paypal']['token'])) {
			redirect('paypal/error');
		}

		$order_id = $_SESSION['paypal']['order_id'];

		if (!$order_id || !($order = Core::$db->order[$order_id])) {
			redirect('paypal/error');
		}

		$_SESSION['order_id'] = $order['id'];

		$this->content->content = tpl('paypal/success.latte', array(
			'order' => $order
				));
	}

	public function fail()
	{
		$order_id = 0;
		$order = $link = null;

		$order_id = $_SESSION['paypal']['order_id'];

		if (!$order_id || !($order = Core::$db->order[$order_id])) {
			redirect('paypal/error');
		}

		if ($order) {
			$link = url('paypal/process_payment/' . $order['id'] . '_' . md5($order['id'] . $order['received'] . $order['email']), null, true);
			$_SESSION['order_id'] = $order['id'];
		}

		$this->content->content = tpl('paypal/fail.latte', array(
			'order' => $order,
			'link' => $link
				));
	}

	public function error()
	{
		$order_id = 0;
		$order = $link = null;

		$order_id = $_SESSION['paypal']['order_id'];

		if ($order_id) {
			$order = Core::$db->order[$order_id];
		}

		if ($order) {
			$link = url('paypal/process_payment/' . $order['id'] . '_' . md5($order['id'] . $order['received'] . $order['email']), null, true);
			$_SESSION['order_id'] = $order['id'];
		}

		$this->content->content = tpl('paypal/error.latte', array(
			'order' => $order,
			'link' => $link
				));
	}

}