<?php
defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */
class Controller_Remote extends Controller
{

	public function __construct() {
		$this->tpl = null;
	}

	private $response = array(
		'status' => 500,
		'message' => NULL
		);

	//private $remoteUrl = 'http://localhost/webmex/web/admin/remote/check/';
	private $remoteUrl = 'http://webmex.cz/admin/remote/check/';

	public function postRemoteKey() {
		$this->tpl = null;
		if(!isset($_POST))
			exit;

		if(!$this->checkRemoteAccess($_POST, true)) {
			$this->response['message'] = 'Ověřování pomocí remote key selhalo.';
			$this->sendResponse();
		}

		if(isset($_POST['remote_key'])) {
			$exists = Core::$db->config()->where('name', 'remote_key')->fetch();
			if($exists) {
				Core::$db->config()->where('name', 'remote_key')->update(array('value' => $_POST['remote_key']));
			}else{
				Core::$db->config(array(
					'name' => 'remote_key',
					'value' => $_POST['remote_key']
				));
			}
			$this->response['status'] = 200;
			$this->response['message'] = 'Remote key byl uložen.';
			$this->sendResponse();
		}
		$this->response['message'] = 'Neočekávané vstupni parametry.';
		$this->sendResponse();
	}

	public function postLicenseDate() {
		$this->tpl = null;
		if(!isset($_POST))
			exit;

		if(!$this->checkRemoteAccess($_POST, true)) {
			$this->response['message'] = 'Ověřování pomocí remote key selhalo.';
			$this->sendResponse();
		}

		if(isset($_POST['date'])) {
			$exists = Core::$db->config()->where('name', 'webmex_license_expiration')->fetch();
			if($exists) {
				Core::$db->config()->where('name', 'webmex_license_expiration')->update(array('value' => trim(date('Y-m-d', strtotime($_POST['date'])))));
			}else{
				Core::$db->config(array(
					'name' => 'webmex_license_expiration',
					'value' => trim(date('Y-m-d', strtotime($_POST['date'])))
				));
			}
			$this->response['status'] = 200;
			$this->response['message'] = 'Datum expirace bylo změněno.';
			$this->sendResponse();
		}
		$this->response['message'] = 'Neočekávané vstupni parametry.';
		$this->sendResponse();
	}

	public function postLicense() {
		$this->tpl = null;
		if(!isset($_POST))
			exit;

		if(!$this->checkRemoteAccess($_POST)) {
			$this->response['message'] = 'Ověřování pomocí remote key selhalo.';
			$this->sendResponse();
		}

		if(isset($_POST['license_id']) && isset($_POST['license'])) {
			Core::$db->config()->where('name', 'instid')->update(array('value' => $_POST['license_id']));
			Core::$db->config()->where('name', 'license_key')->update(array('value' => $_POST['license']));
			$this->response['status'] = 200;
			$this->response['message'] = 'Licence byla uložena.';
		}else{
			$this->response['message'] = 'Neočekávané vstupni parametry.';
		}

		$this->sendResponse();
	}

	public function postAdmin() {
		$this->tpl = null;
		if(!isset($_POST))
			exit;

		if(!$this->checkRemoteAccess($_POST)) {
			$this->response['message'] = 'Ověřování pomocí remote key selhalo.';
			$this->sendResponse();
		}

		if(isset($_POST['password'])) {
			$data = array(
				'username' => 'webmex',
				'email' => 'info@webmex.cz',
				'password' => $_POST['password'],
				'name' => 'Webmex PLUS s.r.o.',
				'enable_login' => 1
			);

			$user = Core::$db->user()->where('username', 'webmex')->fetch();

			if($user) {
				$user->update($data);
			}else{
				$userId = Core::$db->user($data);
				Core::$db->user_permissions(array(
					'user_id' => $userId,
					'permission_id' => 10000
				));
			}

			$this->response['status'] = 200;
			$this->response['message'] = 'Uživatel webmex byl vytvořen.';
		}else{
			$this->response['message'] = 'Neočekávané vstupni parametry.';
		}

		$this->sendResponse();
	}

	public function postMaintance() {
		$this->tpl = null;
		if(!isset($_POST))
			exit;

		if(!$this->checkRemoteAccess($_POST)) {
			$this->response['message'] = 'Ověřování pomocí remote key selhalo.';
			$this->sendResponse();
		}

		if(isset($_POST['maintance']) && (int) $_POST['maintance'] == 1) {
			Core::$db->config()->where('name', 'maintenance')->update(array(
				'value' => 1
			));

			$this->response['status'] = 200;
			$this->response['message'] = 'Režim údržby byl zapnut.';
			$this->sendResponse();
		}
	}

	private function checkRemoteAccess($post, $first = false) {
		if(!isset($post['id']) || !isset($post['key'])) {
			return false;
		}

		if(!$first) {
			$remote_key = Core::$db->config()->where('name', 'remote_key')->fetch();
			if(!$remote_key)
				return false;

			if($remote_key['value'] != $post['key'])
				return false;
		}

		$fields = array(
			'id' => $post['id'],
			'key' => $post['key'],
		);

		$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL,$this->remoteUrl);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, "id=".$fields['id']."&key=".$fields['key']);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		$response = curl_exec($ch);
		curl_close ($ch);
		$r = json_decode($response);
		return $r->status;
		if($r->status == 200) {
			return true;
		}else{
			return false;
		}
	}


	public function sendResponse() {
		echo trim(json_encode($this->response));
		exit;
	}

}
?>