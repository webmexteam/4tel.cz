<?php

defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */
class Controller_Default extends Controller
{

	protected $features_filter;

	public function __construct()
	{
		parent::__construct();

		if (isSet($_GET['preview']) && !empty($_POST['css'])) {
			$_SESSION['design_mode'] = array(
				'css' => $_POST['css']
			);
		} else if (!isSet($_GET['preview'])) {
			unset($_SESSION['design_mode']);
		}

		if (isSet($_POST['login']) && !empty($_POST['customer_email']) && !empty($_POST['customer_password'])) {
			// customer login
			if (($result = Customer::login($_POST['customer_email'], $_POST['customer_password'], isSet($_POST['customer_remember']))) !== true) {
				View::$global_data['customer_login_error'] = __($result);
			} else {
				redirect($_SERVER['HTTP_REFERER']);
			}
		}

		if (isSet($_POST['reset_password']) && !empty($_POST['customer_email_reset'])) {
			$result = $this->reset_password($_POST['customer_email_reset']);

			if (!$result) {
				View::$global_data['customer_login_error'] = __('user_not_found');
			}
		}

		$this->content = tpl('default_3columns.latte');

		if (isSet($_POST['newsletter']) && !isSet($_POST['submit_order'])) {
			if (!Core::$db->newsletter_recipient()->where('email', $_POST['newsletter'])->fetch()) {
				Core::$db->newsletter_recipient(array(
					'email' => $_POST['newsletter'],
					'date' => time()
				));
			}
			redirect(TRUE);
		}
	}

	public function index()
	{
		$this->a(PAGE_INDEX);
	}

	// pages
	public function a($id, $sef_url = null)
	{
		$page = Core::$db->page[(int) $id];
		$products = $basket_products = $product_tpl = $file_content = $producers = $features = null;

		$is_basket = (Core::config('page_basket') == $page['id']);
		$is_order_step1 = (Core::config('page_order_step1') == $page['id']);
		$is_order_step2 = (Core::config('page_order_step2') == $page['id']);
		$is_order_finish = (Core::config('page_order_finish') == $page['id']);
		$is_search = (Core::config('page_search') == $page['id']);
		$is_sitemap = (Core::config('page_sitemap') == $page['id']);
        $has_newsletter = false;

		$canonical = null;

		/**
		 * Inicializace zakaznika pro filtr ceny Od Do
		 */
		$customer_discount = 0;
		$coefficient = 1;
		$group_id = Core::config('customer_group_default');

		if(Customer::$logged_in) {
			$group_id = Core::config('customer_group_registered');

			if ((int) Customer::get('customer_group_id')) {
   				$coefficient = Core::$db->customer_group[(int) Customer::get('customer_group_id')]['coefficient'];
    		}else{
    			$coefficient = Core::$db->customer_group[(int) $group_id]['coefficient'];
    		}

    		$customer_discount = (float) Customer::get('discount');
		}else{
			$coefficient = Core::$db->customer_group[(int) $group_id]['coefficient'];
		}

		if (!empty($_GET)) {
			$d = $_GET;

			unset($d['page'], $d['uri'], $d['q']);

			if (!empty($d)) {
				$canonical = url(true, false, true);
			}
		}

		View::$global_data['canonical'] = $canonical;

		if ($page && $page['status'] == 1 && ($sef_url === null || $page['sef_url'] == $sef_url)) {

			if ($page['external_url']) {
				redirect(ltrim($page['external_url'], '/'));
			}

			setMeta($page);

			Core::$current_page = $page;

			$expanded = array();

			if (Core::$current_page && !Core::$current_page['parent_page']) {
				$expanded[] = Core::$current_page['id'];
			} else if (Core::$current_page) {
				$parent_id = Core::$current_page['parent_page'];
				$_page = null;

				$expanded[] = Core::$current_page['id'];

				while ($parent_id) {
					$_page = Core::$db->page[$parent_id];

					if (!$_page) {
						break;
					}

					$expanded[] = $parent_id;

					$parent_id = $_page['parent_page'];
				}
			}

			Core::$active_pages = $expanded;

			if ((int) $page['authorization'] && !Customer::$logged_in) {
				$this->content->content = tpl('page_login.latte', array(
					'page' => $page
						));
				return;
			}

			if (!empty($page['content_file']) && file_exists(DOCROOT . 'etc/pages/' . $page['content_file'])) {
				$file_content = $this->getPageContent(DOCROOT . 'etc/pages/' . $page['content_file']);
			}

			if ($is_basket) {
				$this->_basket();

				$basket_products = Basket::products();
				$basket_free_samples = Basket::freeSamples();
			}

			if ($is_order_step1 && empty($_GET['payment'])) {

				$basket_products = Basket::products();
				$basket_free_samples = Basket::freeSamples();

				if (!$basket_products && !$basket_free_samples) {
					redirect(PAGE_BASKET);
				}

				$this->_order_step1();

			} else if ($is_order_step2 && empty($_GET['payment'])) {

                if (Customer::$logged_in) {
                    $cust = Core::$db->customer()->where('id', Customer::get('id'))->fetch();
                    if(!empty($cust['email'])) {
                        $has_newsletter = (bool) Core::$db->newsletter_recipient()->where('email', $cust['email'])->fetch();
                    }
                }

				$basket_products = Basket::products();
				$basket_free_samples = Basket::freeSamples();

				if (!$basket_products && !$basket_free_samples) {
					redirect(PAGE_BASKET);
				}

				$this->_order_step2();

			} else if ($is_order_finish && !empty($_GET['finish'])) {

				$basket_products = Basket::products();
				$basket_free_samples = Basket::freeSamples();
				$order = Core::$db->order[(int) $_SESSION['order_id']];

				if ($order && $order['received'] == $_GET['finish']) {
					View::$global_data['order'] = $order;
				} else {
					redirect(PAGE_ORDER);
				}
			}

			$order_by = 'default';
			$order_dir = 'ASC';

			if ((int) $page['enable_filter']) {

				foreach ($_GET as $k => $v) {
					if ($k == 'dir-asc' && $v == 1) {
						$order_dir = 'ASC';
					} else if ($k == 'dir-desc' && $v == 1) {
						$order_dir = 'DESC';
					}
				}

				if (isSet($_GET['order'])) {
					$order_by = $_GET['order'];
					if (!in_array($order_by, array('name', 'price', 'default', 'sales', 'last_change'))) {
						$order_by = 'default';
					}
				}
			}

			View::$global_data['_view'] = 'pictures';
			View::$global_data['_order'] = $order_by;
			View::$global_data['_order_dir'] = $order_dir;

			if ($page['products'] == 1 && !$is_search) {

				if ((int) $page['inherit_products']) {
					$this->getSubpages($page['id'], $ids);

					if(Core::config('hide_no_stock_products')) {
						$products = Core::$db->product()->where('stock > 0 AND hide_product_on_zero_stock = 1 OR hide_product_on_zero_stock != 1')->where('id IN (' . Core::$db->product_pages()->where('page_id', $ids)->select('product_id') . ') OR id IN (' . $page->product_pages()->select('product_id') . ')');
					}else{
						$products = Core::$db->product()->where('id IN (' . Core::$db->product_pages()->where('page_id', $ids)->select('product_id') . ') OR id IN (' . $page->product_pages()->select('product_id') . ')');
					}

					$_products_ids = clone $products;
					$_products_ids = $_products_ids->select('id');

					$producers = Core::$db->page()->where('menu', 3)->where('status', 1)->where('id', Core::$db->product_pages()->where('product_id', $_products_ids)->select('page_id'))->order('position ASC');
				} else {
					if(Core::config('hide_no_stock_products')) {
						$products = Core::$db->product()->where('stock > 0 AND hide_product_on_zero_stock = 1 OR hide_product_on_zero_stock != 1')->where('id', $page->product_pages()->select('product_id'));
					}else{
						$products = Core::$db->product()->where('id', $page->product_pages()->select('product_id'));
					}
				}

				$products->where('status', 1);

				$min_max_prices = array(
					'min' => 0,
					'max' => 0,
					'min_selected' => 0,
					'max_selected' => 0
				);
				$coefficient = 1;

				if ((int) $page['enable_filter']) {
					$_min_price = clone $products;
					$_max_price = clone $products;

					$_min_price->select('(
							MIN(price - price / 100 *
							(
								COALESCE((product.discount + '.$customer_discount.'), 0)
								+ COALESCE((SELECT SUM(discount) FROM page WHERE id IN (SELECT page_id FROM product_pages WHERE product_id = product.id)), 0)
							)) * '.$coefficient.'
							) AS min_price');
					$_min_price = $_min_price->order('min_price ASC')->limit(1)->fetch()->row;
					$min_max_prices['min'] = (float) $_min_price['min_price'];
					$min_max_prices['min_selected'] = $min_max_prices['min'];

					$_max_price->select('(
							MAX(price - price / 100 *
							(
								COALESCE((product.discount + '.$customer_discount.'), 0)
								+ COALESCE((SELECT SUM(discount) FROM page WHERE id IN (SELECT page_id FROM product_pages WHERE product_id = product.id)), 0)
							)) * '.$coefficient.'
							) AS max_price');
					$_max_price = $_max_price->order('max_price ASC')->limit(1)->fetch()->row;
					$min_max_prices['max'] = (float) $_max_price['max_price'];
					$min_max_prices['max_selected'] = $min_max_prices['max'];
				}

				if ((int) $page['enable_filter'] && !empty($_GET['price_from']) && (float) $_GET['price_from'] > 0 && !empty($_GET['price_to']) && (float) $_GET['price_to'] > 0) {
					$products->where("(
						(price - price / 100 *
						(
							COALESCE((product.discount + ".$customer_discount."), 0)
							+ COALESCE((SELECT SUM(discount) FROM page WHERE id IN ( SELECT page_id FROM product_pages WHERE product_id = product.id)), 0)
						)) * ".$coefficient."
						) BETWEEN ".(float) $_GET['price_from']." AND ".(float) $_GET['price_to']."");
					$min_max_prices['min_selected'] = (float) $_GET['price_from'];
					$min_max_prices['max_selected'] = (float) $_GET['price_to'];
				}

				if ((int) $page['enable_filter']) {
					if($min_max_prices['min_selected'] < $min_max_prices['min']) {
						$min_max_prices['min_selected'] = $min_max_prices['min'];
					}

					if($min_max_prices['max_selected'] > $min_max_prices['max']) {
						$min_max_prices['max_selected'] = $min_max_prices['max'];
					}
				}


				if ((int) $page['enable_filter']) {
					$_products = clone $products;
				}

				if ((int) $page['enable_filter'] && !empty($_GET['p'])) {
					$products->where('id', Core::$db->product_pages()->where('page_id', $_GET['p'])->select('product_id'));
				}

				if ($page['enable_filter'] && !empty($_GET['f']) && ($featureset = $page->featureset) && $featureset['id']) {
					$tbl_name = 'features_' . $featureset['id'];

					$_ids = Core::$db->$tbl_name();

					foreach ($_GET['f'] as $fkey => $fdata) {
						// multiselect
						if (is_array($fdata)) {
							$is_in = array();
							$filter_this = true;
							foreach ($fdata as $fid => $fvalue) {
								if ($fvalue !== '') {
									$is_in[] = "'".$fvalue."'";
									//$_ids->where('f' . $fkey, $fvalue);
								} else {
									$filter_this = false;
								}
							}
							if ($filter_this) {
								$_ids->where('f' . $fkey . ' IN (' . join(',', (array) $is_in) . ')');
							}

						// simple select
						} else {
							if ($fdata !== '') {
								$_ids->where('f' . $fkey, $fdata);
							}
						}
					}

					if ($_ids->getWhere()) {
						$products->where('id', $_ids->select('product_id'));
					}
				}
/*
				if ($page['enable_filter'] && !empty($_GET['c']) && ($featureset = $page->featureset) && $featureset['id']) {
					$tbl_name = 'features_' . $featureset['id'];

					$_ids = Core::$db->$tbl_name();

					foreach ($_GET['c'] as $fkey => $fdata) {

                    }
                }
*/
				if ($page['enable_filter'] && isSet($_products)) {
					$product_ids = $page_ids = array();

					foreach ($_products->select('id') as $priduct_id) {
						$product_ids[] = $priduct_id['id'];
					}

					$producers = Core::$db->page()->where('menu', 3)->where('status', 1)->where('id', Core::$db->product_pages()->where('product_id', $product_ids)->select('page_id'))->order('position ASC');
				}

				if (Core::$is_premium && $page['enable_filter'] && $page->featureset['id']) {
					foreach ($page->featureset->feature()->order('position ASC') as $feature) {
						if ((int) $feature['filter']) {
							$features[$feature['id']] = array(
								'name' => $feature['name'],
								'options' => $this->getFeatureOptions($page->featureset, $feature, $product_ids)
							);
						}
					}

				}



				Event::run('Controller_Default.page::products', $products);

				if (!(int) $page['enable_filter']) {
					$products->order('position ASC, promote DESC');
				} else {

					$orderby = $order_by . ' ' . $order_dir;

					if ($order_by == 'default') {
              $orderby = 'position ' . $order_dir;

              $deforder = Core::config('default_order');

              if (!is_null($deforder) && preg_match('#^(.+)(ASC|DESC)$#', $deforder, $m)) {
                  $odir = 'ASC';
                  if ($m[2] == 'ASC' && $order_dir == 'DESC') $odir = 'DESC';
                  else if ($m[2] == 'DESC' && $order_dir == 'ASC') $odir = 'DESC';

                  if ($m[1] == 'position ') $orderby = 'position ' . $odir;
                  else if ($m[1] == 'price ') $orderby = 'price ' . $odir;
                  else if ($m[1] == 'name ') $orderby = 'name ' . $odir;
                  else if ($m[1] == 'position price ') $orderby = 'position ' . $odir . ', price ' . $odir;
              }
					}

					$products->order($orderby . ', promote DESC');
				}

				$products->limit(pgLimit(), pgOffset());

				$products_count = Core::$db->product;

				if ($products->getWhere()) {
					$products_count->where($products->getWhere());
				}

				$products_count = $products_count->select('COUNT(id) as total_count')->fetch();
				$products_count = (int) $products_count['total_count'];
			}

			if ($is_search && !empty($_GET['q'])) {
				$q = trim($_GET['q']);

				$products = Search::findProducts($q, $order_by, $order_dir);

				if ($products) {
					if ($page['enable_filter']) {
						$_products = clone $products;
					}

					if ($page['enable_filter'] && !empty($_GET['p'])) {
						$products->where('id', Core::$db->product_pages()->where('page_id', $_GET['p'])->select('product_id'));
					}

					if ($page['enable_filter'] && isSet($_products)) {
						$page_ids = array();

						foreach (Core::$db->product_pages()->where('product_id', $_products->select('id'))->select('page_id') as $pid) {
							$page_ids[] = $pid['page_id'];
						}

						$producers = Core::$db->page()->where('menu', 3)->where('status', 1)->where('id', $page_ids);
					}

					Event::run('Controller_Default.page::products', $products);

					$products->limit(pgLimit(), pgOffset());

					$products_count = Core::$db->product;

					if ($products->getWhere()) {
						$products_count->where($products->getWhere());
					}

					$products_count = $products_count->select('COUNT(id) as total_count')->fetch();
					$products_count = (int) $products_count['total_count'];
				} else {
					$products = array();
					$products_count = 0;
				}
			}

            // Ccalculate customer discount by category
            if (Customer::$logged_in) {
                foreach($products as $key => $product) {
                    $pageDiscount = Core::$db->product_pages()->where('product_id', $product['id'])->where('page_id != 109')->fetch();
                    if($pageDiscount) {
                        $breadcrumbIDs = loadBreadcrumbIds($pageDiscount['page_id']);
                        if(count($breadcrumbIDs) > 0) {
                            foreach($breadcrumbIDs as $bId) {
                                $discount = Core::$db->customer_discount_pages()->where('customer_id', Customer::get('id'))->where('page_id', $bId)->fetch();

                                if($discount && $discount['discount'] > 0) {
                                    $products[$key]['discount_price'] = $product['price'];
                                    if($discount && $discount['discount'] > 0) {
                                        $discountPrice = $product['price'] - (($product['price'] / 100) * $discount['discount']);
                                        $products[$key]['discount_price'] = $discountPrice;
                                    }

                                    break;
                                }
                            }
                        }
                    }

                }
            }


			// set layout (columns)
			$this->content = tpl('default_' . ($page['layout_columns'] ? $page['layout_columns'] : 3) . 'columns.latte');

			$subpages = Core::$db->page()->where('parent_page', $page['id'])->where('status', 1)->order('position ASC');

			if ($page['subpages'] == 'news') {
				$subpages->order('pubdate DESC');
			}

			$page_action = $page_action_top = null;

			if ($is_basket) {
				$page_action = tpl('basket.latte', array(
					'basket_products' => $basket_products,
					'basket_free_samples' => $basket_free_samples,
				));
			} else if ($is_order_finish) {
				$page_action = tpl('order_finish.latte', array(
					'basket_products' => $basket_products,
					'basket_free_samples' => $basket_free_samples,
				));
			} else if ($is_order_step1 && !empty($_GET['payment'])) {
				$page_action = $this->_order_payment($_GET['id'], $_GET['hash']);
			} else if ($is_order_step1) {
				$page_action = tpl('order_step1.latte', array(
					'basket_products' => $basket_products,
					'basket_free_samples' => $basket_free_samples,
				));
			} else if ($is_order_step2) {
				$page_action = tpl('order_step2.latte', array(
					'basket_products' => $basket_products,
					'basket_free_samples' => $basket_free_samples,
                    'has_newsletter' => $has_newsletter,
				));
			} else if ($is_sitemap) {
				$page_action = tpl('sitemap.latte');
			} else if ($is_search) {
				$page_action_top = tpl('search_results.latte', array('products' => $products, 'total_products' => $products_count));
			}

			// set content
			$this->content->content = tpl('page.latte', array(
				'page' => $page,
				'page_file_content' => $file_content,
				'products' => $products,
				'products_count' => $products_count,
				'subpages' => $subpages,
				'is_basket' => $is_basket,
				'is_order_step1' => $is_order_step1,
				'is_order_step2' => $is_order_step2,
				'is_order_finish' => $is_order_finish,
				'is_search' => $is_search,
				'is_sitemap' => $is_sitemap,
				'basket_products' => $basket_products,
				'producers' => $producers,
				'features' => $features,
				'page_action_top' => $page_action_top,
				'page_action' => $page_action,
				'min_max_prices' => $min_max_prices
			));
		} else {
			Core::show_404();
		}
	}

	private function getFeatureOptions($featureset, $feature, $product_ids)
	{
		$values = array();

		if ($featureset && $feature) {
			$tbl_name = 'features_' . $featureset['id'];

			if (!$this->features_filter) {
				$this->features_filter = Core::$db->$tbl_name()->where('product_id', $product_ids);
			}

			$i = 1;
			foreach ($this->features_filter as $row) {
				$text = $v = $row['f' . $feature['id']];

				if ($feature['type'] == 'yesno') {
					$text = (int) $text ? __('yes') : __('no');
					$text = $text->__toString();
				} else if ($feature['type'] == 'number') {
					$text = (float) $text;
					$v = (float) $v;
				}

				if (!$text) {
					continue;
				}

				if ($feature['unit']) {
					$text .= ' ' . $feature['unit'];
				}

				if (!isSet($values[$text])) {
					$values[$text] = array(
						'count' => 0,
						'value' => $v,
						'id' => $row['id']
					);
				}

				$values[$text]['count']++;
				$i++;
			}
		}

		uasort($values, function($a, $b){
			return strnatcmp($a['value'], $b['value']);
		});
		return $values;
	}

	// products
	public function p($id, $sef_url = null)
	{
		$enable_buy = true;
		$attr_stock = false;
		$product = Core::$db->product[(int) $id];

		Event::run('Controller_Default::product', $product);

		if ($product && $product['status'] == 1 && ($sef_url === null || $product['sef_url'] == $sef_url)) {
			setMeta($product);

			$attributes = array();
			$attrs_with_stock = array();
			$attrs_skus = array();
			$attrs_eans = array();
			$has_default = false;

			if ($product['default_page']) {
				$page = $product->product_pages()->where('page_id', $product['default_page'])->fetch()->page;
			} else {
				$page = $product->product_pages()->fetch()->page;
			}

			Core::$current_page = $page;
			Core::$current_product = $product;

			$expanded = array();

			if (Core::$current_page && !Core::$current_page['parent_page']) {
				$expanded[] = Core::$current_page['id'];
			} else if (Core::$current_page) {
				$parent_id = Core::$current_page['parent_page'];
				$_page = null;

				$expanded[] = Core::$current_page['id'];

				while ($parent_id) {
					$_page = Core::$db->page[$parent_id];

					if (!$_page) {
						break;
					}

					$expanded[] = $parent_id;

					$parent_id = $_page['parent_page'];
				}
			}

			Core::$active_pages = $expanded;

			foreach ($product->product_attributes()->order('name ASC, id ASC') as $attr) {
				$price = estPrice($attr['price'], $product['price']);
				$sku = $attr['sku'];
				$ean = $attr['ean13'];
				$stock = ($attr['stock'] !== '' && $attr['stock'] !== null) ? $attr['stock'] : null;

				if (strrpos($sku, '*') !== false) {
					$sku = preg_replace('/\*/', $product['sku'], $sku);

					if (!in_array($attr['name'], $attrs_skus)) {
						$attrs_skus[] = $attr['name'];
					}
				}

				if (strrpos($ean, '*') !== false) {
					$ean = preg_replace('/\*/', $product['ean13'], $ean);

					if (!in_array($attr['name'], $attrs_eans)) {
						$attrs_eans[] = $attr['name'];
					}
				}

				if ($stock !== null) {
					$attr_stock = true;
				}

				if (!in_array($attr['name'], $attrs_with_stock) && ($stock !== null || $attr->availability['id'])) {
					$attrs_with_stock[] = $attr['name'];
				}

				$attr_enable = !((int) Core::config('suspend_no_stock') && ($stock !== null && $stock <= 0));

				if (!$has_default && $attr_enable && $attr['is_default']) {
					$has_default = true;
				}

				$attributes[$attr['name']][] = array(
					'id'           => $attr['id'],
					'value'        => $attr['value'],
					'price'        => $price,
					'weight'       => $attr['weight'],
					'default'      => ($attr_enable && $attr['is_default']),
					'sku'          => $sku,
					'ean13'        => $ean,
					'stock'        => $stock,
					'availability' => $attr->availability,
					'file_id'      => $attr['file_id'],
					'enable'       => $attr_enable
				);
			}

			if ($attributes && !$has_default) {
				$key = key($attributes);
				$attributes[$key][0]['default'] = 1;
			}

			if (!empty($_GET['attr'])) {
				foreach ($attributes as $gname => $group) {
					foreach ($group as $attr_i => $attr) {
						if ($attr['id'] == (int) $_GET['attr']) {
							$attributes[$gname][$attr_i]['default'] = 1;
						} else {
							$attributes[$gname][$attr_i]['default'] = 0;
						}
					}
				}
			}

			if ((int) Core::config('suspend_no_stock') && $product['stock'] !== null && $product['stock'] !== '' && $product['stock'] <= 0) {
				$enable_buy = false;
			}

			if ($enable_buy && $attributes) {
				$_enable = false;

				foreach ($attributes as $attr) {
					foreach ($attr as $option) {
						if ($option['enable']) {
							$_enable = true;
						}
					}
				}

				if (!$_enable) {
					$enable_buy = false;
				}
			}

			if ($product['price'] === null) {
				$enable_buy = false;
			}

			$discounts = $qtys = array();
			$price = $product['price'];

			if ($_discounts = $product->product_discounts()->order('quantity ASC')) {

				foreach ($_discounts as $i => $discount) {
					$qtys[$i] = $discount;
				}

				foreach ($qtys as $i => $discount) {
					if ($qtys[$i + 1]) {
						$to = ($qtys[$i + 1]['quantity'] - 1);

						if ($discount['quantity'] == $to) {
							$range = __('discount_qty_exact', $discount['quantity']);
						} else {
							$range = __('discount_qty_range', $discount['quantity'], $to);
						}
					} else {
						$range = __('discount_qty_more', $discount['quantity']);
					}

					$discounts[] = array(
						'range' => $range,
						'discount' => (float) $discount['value'],
						'price' => $price - ((float) $discount['value'] / 100 * $price)
					);
				}
			}

			$features = array();
			if ($product['featureset_id']) {
				$featureset = Core::$db->featureset[(int) $product['featureset_id']];
				$tbl_name = 'features_' . $product['featureset_id'];

				if ($row = Core::$db->$tbl_name()->where('product_id', $product['id'])->limit(1)->fetch()) {
					foreach ($featureset->feature()->order('position ASC, id ASC') as $feature) {
						if ($row['f' . $feature['id']] !== null) {
							$value = $row['f' . $feature['id']];

							if ($feature['type'] == 'yesno') {
								$value = $value == 1 ? __('yes') : __('no');
							}

							if ($feature['type'] == 'text') {
								if (substr(ltrim($value), 0, 1) != '<') {
									$value = nl2br($value);
								}
							}

							if ($feature['type'] == 'number') {
								$value = (float) $value;
							}

							$features[] = array(
								'id' => $feature['id'],
								'name' => $feature['name'],
								'unit' => $feature['unit'],
								'type' => $feature['type'],
								'value' => $value,
								'_value' => $row['f' . $feature['id']]
							);
						}
					}
				}
			}

			$related_products = array();
			foreach ($product->product_related()->limit(6) as $related) {
				$related_product = Core::$db->product[(int) $related['related_id']];
				if ($related_product['status']) {
					$related_products[] = $related_product;
				}
			}

			// set layout (columns)
			$this->content = tpl('default_' . ($product['layout_columns'] ? $product['layout_columns'] : 3) . 'columns.latte');

			// Set pages & manufacturers
			$categories = array();
			$manufacturers = array();
			foreach($product->product_pages() as $page) {
				if($page->page['menu'] == 3) {
					$manufacturers[] = $page;
				}else{
					$categories[] = $page;
				}
			}

            // Calculate page dicount for customer
            $page_discount = 0;
            if (Customer::$logged_in) {
                $page = Core::$db->product_pages()->where('product_id', $product['id'])->where('page_id != 109')->fetch();
                if($page) {
                    $breadcrumbIDs = loadBreadcrumbIds($page['page_id']);
                    if(count($breadcrumbIDs) > 0) {
                        foreach($breadcrumbIDs as $bId) {
                            $discount = Core::$db->customer_discount_pages()->where('customer_id', Customer::get('id'))->where('page_id', $bId)->fetch();

                            if($discount && $discount['discount'] > 0) {
                                if($discount && $discount['discount'] > 0) {
                                    $page_discount = (($product['price'] / 100) * $discount['discount']);
                                }

                                break;
                            }
                        }
                    }
                }
            }

			// set content
			$this->content->content = tpl('product.latte', array(
				'product' => $product,
				'attributes' => $attributes,
				'features' => $features,
				'discounts' => $discounts,
                'page_discount' => $page_discount,
				'enable_buy' => $enable_buy,
				'attributes_show_sku' => (count($attrs_skus) <= 1),
				'attributes_show_ean13' => (count($attrs_eans) <= 1),
				'attributes_have_stock' => $attr_stock,
				'attributes_with_stock' => $attrs_with_stock,
				'display_stock' => (int) Core::config('display_stock'),
				'related_products' => $related_products,
				'categories' => $categories,
				'manufacturers' => $manufacturers,
			));
		} else {
			Core::show_404();
		}
	}

	public function _basket()
	{
		if (!SHOW_PRICES) {
			return;
		}

		if (!empty($_GET['delete'])) {
			Basket::remove((int) $_GET['delete']);

            if(!empty($_GET['fast']) && $_GET['fast'] == 1) {
                redirect(PAGE_INDEX);
            }
		}

		if (!empty($_GET['removevoucher'])) {
			Basket::removeVoucher();
		}

		if (!empty($_GET['buy'])) {
			$product = Core::$db->product[(int) $_GET['buy']];

			if (Core::$is_premium && $product && count($product->product_attributes())) {
				redirect($product);
			}


            if(isset($_GET['qty']) && (int) $_GET['qty'] > 0) {
                $qty = (int) $_GET['qty'];
            } else {
                $qty = 1;
            }

			$result = Basket::add((int) $_GET['buy'], null, $qty);

			if ($result !== true && is_numeric($result)) {
				View::$global_data['basket_error'] = __($result > 0 ? 'max_quantity' : 'basket_no_stock', $result);
			}
			redirect($product);
		}

		if (!empty($_GET['free_sample'])) {
			$product = Core::$db->product[(int) $_GET['free_sample']];

			$result = Basket::addFreeSample((int) $_GET['free_sample']);

			if ($result !== true && is_numeric($result)) {
				View::$global_data['basket_error'] = __($result > 0 ? 'max_quantity' : 'basket_no_stock', $result);
			}
			redirect($product);
		}

		if (isSet($_POST['use_voucher']) && !empty($_POST['voucher'])) {
			if ($voucher = Core::$db->voucher()->where('code', $_POST['voucher'])->fetch()) {

				if ($voucher->model->isValid()) {
					if ($voucher['min_price'] === null || $voucher['min_price'] <= Basket::total()->total) {
						Basket::useVoucher($voucher);
					} else {
						View::$global_data['basket_error'] = __('msg_voucher_min_price', (string) price($voucher['min_price']));
					}
				} else {
					View::$global_data['basket_error'] = __('msg_voucher_expired');
				}
			} else {
				View::$global_data['basket_error'] = __('msg_voucher_invalid_code');
			}
		}

		if (isSet($_POST['checkout'])) {
			redirect(PAGE_ORDER);
		}

		if (!empty($_POST['product_id']) && is_numeric($_POST['product_id'])) {
			$product = Core::$db->product[ $_POST['product_id'] ];
			$result = Basket::add((int) $_POST['product_id'], !empty($_POST['attributes']) ? $_POST['attributes'] : null, !empty($_POST['qty']) && is_numeric($_POST['qty']) ? (int) $_POST['qty'] : 1);

			if ($result !== true && is_numeric($result)) {
				View::$global_data['basket_error'] = __($result > 0 ? 'max_quantity' : 'basket_no_stock', $result);
			}

			if (!empty($_POST['redirect_page'])){
			    $_SESSION['flash'] = 1;
			    header('Location: http://' . $_POST['redirect_page'].'');
			    exit;
			}
		}

		if (isSet($_POST['update_qty']) && !empty($_POST['qty']) && is_array($_POST['qty'])) {
			$result = Basket::update($_POST['qty']);

			if ($result !== true && is_numeric($result)) {
				View::$global_data['basket_error'] = __($result > 0 ? 'max_quantity' : 'basket_no_stock', $result);
			}
		}

	}

	public function _order_step1()
	{
		$customer = null;

		if (Customer::$logged_in) {
			View::$global_data['order'] = Customer::get();
			$customer = Core::$db->customer[Customer::get('id')];
		}

		$basket_total = Basket::total();

		$deliveries_payments = $this->getDeliveriesPayments($customer, $basket_total->_total_before_discount, $basket_total->weight);

		$deliveries = $deliveries_payments['deliveries'];
		$payments = $deliveries_payments['payments'];

		View::$global_data['deliveries'] = $deliveries;
		View::$global_data['payments'] = $payments;


		if (isSet($_POST['submit_order'])) {
			$errors = array();

			// simple spam protection
			if (strlen($_SERVER['HTTP_USER_AGENT']) < 25) {
				// if HTTP_USER_AGENT is too short, it is propably a bot

				$errors[] = array('Spam detected.', __('order_cannot_be_send'));
			}

			if(!isset($_POST['delivery']) || !isset($_POST['payment'])) {
				$errors[] = array('required', __('select_delivery_payment'));
			}

			if (empty($errors)) {
				$this->_saveOrderStep1();
			} else {
				View::$global_data['order_errors'] = $errors;
			}

			View::$global_data['order'] = array_merge((array) View::$global_data['order'], $_POST);
		}
	}

	public function _order_step2()
	{
		$customer = null;

		if (Customer::$logged_in) {
			View::$global_data['order'] = Customer::get();
			$customer = Core::$db->customer[Customer::get('id')];
		}

		$basket_total = Basket::total();

		if(!isset($_SESSION['order']) && !isset($_SESSION['order']['delivery']) || !isset($_SESSION['order']['payment'])) {
			redirect(url(PAGE_ORDER, array(), true));
		}

		$d = Core::$db->delivery[(int) $_SESSION['order']['delivery']];
		$d['name'] = $_SESSION['order']['delivery_name'];

		View::$global_data['delivery'] = $d;
		View::$global_data['payment'] = Core::$db->payment[(int) $_SESSION['order']['payment']];
		View::$global_data['dp_price'] = $_SESSION['order']['dp_price'];
		View::$global_data['total_price'] = $_SESSION['order']['total_price'];

		$phone_required = (Core::config('phone_required') === null || (int) Core::config('phone_required'));
		View::$global_data['phone_required'] = $phone_required;



		if (isSet($_POST['submit_order'])) {
			$required = array('first_name', 'last_name', 'street', 'city', 'zip', 'terms');
			$errors = array();

			if ($phone_required) {
				$required[] = 'phone';
			}

			if (!Customer::$logged_in) {
				$required[] = 'email';
				//$required[] = 'souhlas-osobni-udaje';
			}

			foreach ($required as $name) {
				if (empty($_POST[$name])) {
					$errors[] = array('required', __($name));
				} else if ($name == 'email' && !valid_email($_POST[$name])) {
					$errors[] = array('invalid_email', __($name));
				} else if ($name == 'phone') {
					$_POST[$name] = preg_replace('/[^\d\+]/', '', trim($_POST[$name]));

					if (!preg_match('/^((\+|00)[\d]{2,3})?[\d]{7,14}?$/', $_POST[$name])) {
						$errors[] = array('invalid_phone', __($name));
					}
				}
			}

			// simple spam protection
			if (strlen($_SERVER['HTTP_USER_AGENT']) < 25) {
				// if HTTP_USER_AGENT is too short, it is propably a bot

				$errors[] = array('Antispam protection.', __('order_cannot_be_send'));
			}

			if (empty($errors)) {
				$this->_saveOrder();
			} else {
				View::$global_data['order_errors'] = $errors;
			}

			View::$global_data['order'] = array_merge((array) View::$global_data['order'], $_POST);
		}
	}

	public function _saveOrderStep1()
	{
		$data = $_POST;
		$error = false;
		$dp_price = null;

		$delivery = Core::$db->delivery[(int) $data['delivery']];
		$payment = Core::$db->payment[(int) $data['payment']];

		if ($delivery && $payment) {
			$_SESSION['order']['delivery'] = (int) $delivery['id'];
			$_SESSION['order']['payment'] = (int) $payment['id'];

			$_SESSION['order']['delivery_name'] = $data['delivery_name'];
		} else {
			$error = true;
		}

		if(isset($data['dp_price']))
			$_SESSION['order']['dp_price'] = $data['dp_price'];

		if(isset($data['total_price']))
			$_SESSION['order']['total_price'] = $data['total_price'];

		if ($error) {
			View::$global_data['order_errors'] = array(array('Bad delivery & payment combination.', __('order_cannot_be_send')));
		} else {
			redirect(url(PAGE_ORDER2, array(), true));
		}
	}

	public function _saveOrder()
	{
		$data = $_POST;
		$error = false;
		$dp_price = null;

		$data['received'] = time();
		$data['ip'] = $_SERVER['REMOTE_ADDR'];

		if (Customer::$logged_in) {
			$data['customer_id'] = Customer::get('id');
			$data['email'] = Customer::get('email');
		}

		$group_ids = array(0, (int) Core::config('customer_group_default'));

		if (Customer::$logged_in) {
			$group_ids[] = (int) Core::config('customer_group_registered');

			if (($gid = (int) Customer::get('customer_group_id'))) {
				$group_ids[] = $gid;
			}
		}

		$delivery = Core::$db->delivery[(int) $_SESSION['order']['delivery']];
		$payment = Core::$db->payment[(int) $_SESSION['order']['payment']];
		$delivery['name'] = $_SESSION['order']['delivery_name'];

		$subtotal = Basket::total();

		if ($delivery && $payment) {
			$dp_price = estPrice($delivery['price'], $subtotal->subtotal) + estPrice($payment['price'], $subtotal->subtotal);

			if (!in_array($delivery['customer_group_id'], $group_ids) || !in_array($payment['customer_group_id'], $group_ids)) {
				$error = 'Delivery or payment not available for this customer group.';
			} else {
				if ($dp = Core::$db->delivery_payments()->where('delivery_id', $delivery['id'])->where('payment_id', $payment['id'])->fetch()) {
					$dp_price += estPrice($dp['price'], $subtotal->subtotal);

					if ($dp['free_over'] && parseFloat($dp['free_over']) <= $subtotal->total) {
						$dp_price = 0;
					}

					$data['delivery_id'] = $delivery['id'];
					$data['payment_id'] = $payment['id'];
					$data['delivery_payment'] = $dp_price;
				} else {
					// disallowed D&P combination
					$error = 'Bad delivery-payment combination.';
				}
			}
		} else {
			$error = 'Missing delivery or payment.';
		}

		if ($error) {
			View::$global_data['order_errors'] = array(array($error, __('order_cannot_be_send')));
		} else {

			$_total = $subtotal->_total_before_discount + price_vat($dp_price, VAT_DELIVERY)->price;

			if ($voucher = Basket::voucher()) {
				$data['voucher_id'] = $voucher['id'];

				$discount = estPrice($voucher['value'], $_total);
				$_total -= $discount;

				if ($_total < 0) {
					$_total = 0;
				}
			}

			$data['total_price'] = $subtotal->subtotal;
			$data['total_incl_vat'] = $subtotal->total;
			$total_excl_vat = round($subtotal->subtotal + $dp_price, (int) Core::config('order_round_decimals'));

			$data = prepare_data('order', $data);

			$order_id = Core::$db->order($data);

			$data['id'] = $order_id;

			if (isset($voucher) && $voucher) {
				$voucher->update(array('used' => $voucher['used'] + 1));

				Core::$db->voucher_orders(array(
					'order_id' => $order_id,
					'voucher_id' => $voucher['id'],
					'date' => time()
				));
			}

			$_SESSION['order_id'] = (int) $order_id;

			foreach (Basket::products() as $product) {
				$name = trim($product['product']['name'] . ' ' . $product['product']['nameext']) . ($product['attributes'] ? ' [' . $product['attributes'] . ']' : '');

				$oproduct_id = Core::$db->order_products(array(
					'order_id' => $order_id,
					'product_id' => $product['product']['id'],
					'price' => $product['price'],
					'vat' => $product['product']['vat'],
					'quantity' => $product['qty'],
					'sku' => $product['sku'],
					'ean13' => $product['ean13'],
					'name' => $name
						));

				$_update = array();

				$_product = Core::$db->order_products[(int) $oproduct_id];

				// Update sales in product table
				$real_product = Core::$db->product[(int) $product['product']['id']];
				$real_product->update(array('sales' => ($real_product['sales'] + $product['qty'])));

				if ((int) Core::config('stock_auto')) {
					$product['product']->model->setStock((int) $product['qty'] * -1, $product['attributes_ids']);
				}

				$_update['sold_qty'] = $product['product']['sold_qty'] + $product['qty'];

				if (!empty($_update)) {
					$product['product']->update($_update);
				}
			}
			foreach (Basket::freeSamples() as $product) {
				$name = trim($product['product']['name'] . ' ' . $product['product']['nameext'])
					. ' - VZOREK ZDARMA';

				$oproduct_id = Core::$db->order_products(array(
					'order_id' => $order_id,
					'product_id' => $product['product']['id'],
					'price' => $product['price'],
					'vat' => $product['product']['vat'],
					'quantity' => $product['qty'],
					'sku' => $product['sku'],
					'ean13' => $product['ean13'],
					'name' => $name
				));
			}

			Core::$db->order_products(array(
				'order_id' => $order_id,
				'price' => $dp_price,
				'vat' => VAT_DELIVERY,
				'quantity' => 1,
				'type' => 1,
				'name' => __('delivery_payment') . ' [' . $delivery['name'] . ']'
			));

			Basket::destroy();

			$order = Core::$db->order[(int) $order_id];

			$business_conditions = array();
			if(Core::config('business_conditions_pdf')) {
				$business_conditions[] = DOCROOT.'etc/obchodni_podminky.pdf';
			}
			if(Core::config('business_conditions_txt')) {
				$business_conditions[] = DOCROOT.'etc/obchodni_podminky.txt';
			}
			if(Core::config('next_email_att')) {
				$business_conditions[] = DOCROOT.'etc/objednavka_priloha.pdf';
			}

			Email::event('new_order', $order['email'], null, array('order' => $order), $business_conditions);
			Email::event('new_order', Core::config('email_notify'), $order['email'], array('order' => $order), $business_conditions);

			$customer_id = 0;
			$customer_data = $data;

			unset($customer_data['id']);

			if ($customer = Core::$db->customer()->where('email', $customer_data['email'])->fetch()) {
				$customer_id = $customer['id'];
			}

			//create customer account
			if (!$customer && isSet($_POST['create_account'])) {
				$password = strtoupper(random());
				$customer_data['password'] = sha1(sha1($password));

				$customer_data['active'] = (int) Core::config('customer_confirmation') ? 0 : 1;

				$customer_id = Core::$db->customer(prepare_data('customer', $customer_data));

				if ($customer_id) {
					$customer_data['id'] = $customer_id;

					if (!(int) Core::config('customer_confirmation')) {
						$customer_data['password'] = $password;

						Email::event('new_account', $customer_data['email'], null, $customer_data);
					} else {
						sendmail(Core::config('email_notify'), array(__('customer_registered'), __('customer_registered_text')), $customer_data);
					}
				}
			}

			if (isSet($_POST['newsletter'])) {
				if (!Core::$db->newsletter_recipient()->where('email', $customer_data['email'])->fetch()) {
					Core::$db->newsletter_recipient(array(
						'email' => $customer_data['email'],
						'date' => time()
					));
				}
			}

			if ($customer_id) {
				Core::$db->order()->where('id', $order_id)->update(array(
					'customer_id' => $customer_id
				));
			}

			$group_id = (int) Core::config('customer_group_default');

			if (Customer::$logged_in) {
				$group_id = (int) Core::config('customer_group_registered');

				if (($gid = (int) Customer::get('customer_group_id'))) {
					$group_id = $gid;
				}
			}

			if ($customer_id) {
				$_customer = Core::$db->customer[$customer_id];
			} else {
				$_customer = $order['email'];
			}

			Event::run('Controller_Default::saveOrder', $order, $customer, $payment, $delivery);

			Voucher::generate('order_submit', $customer, $order);

			if (!(int) Core::config('confirm_orders') && $payment && $payment['driver']) {
				$driverclass = 'Payment_' . ucfirst($payment['driver']);
				if ($inst = new $driverclass($payment)) {
					$order = Core::$db->order[(int) $order_id];

					$inst->process($order);
				}
			}

			unset($_SESSION['order']);

			redirect(url(PAGE_ORDER_FINISH, array('finish' => $data['received']), true));
		}
	}

	public function _order_payment($order_id, $hash = null)
	{
		if ($order_id && $hash && ($order = Core::$db->order[(int) $order_id])) {
			if ($hash == $order->model->hash()) {

				if ((int) $order['payment_realized'] || $order['status'] != 1) {
					$this->content = __('payment_change_disabled');
					return;
				}

				$total = $subtotal = $weight = 0;

				foreach ($order->order_products() as $product) {
					if ($product['type'] == 0) {
						$total += price_vat($product['price'], $product['vat'])->price * $product['quantity'];
						$subtotal += price_unvat($product['price'], $product['vat'])->price * $product['quantity'];
					}

					if (($_product = $product['product']) && $_product['weight']) {
						$weight += $_product['weight'] * $product['quantity'];
					}
				}

				$order_total = $total;

				if ($voucher = $order->voucher) {
					$discount = estPrice($voucher['value'], $total);

					$total = $total - $discount;
				}

				if ($total < 0) {
					$total = 0;
				}

				$deliveries_payments = $this->getDeliveriesPayments($order->customer, $order_total, $weight);

				$group_ids = $deliveries_payments['group_ids'];

				if (!empty($_POST)) {
					$data = array();

					$delivery = Core::$db->delivery[(int) $_POST['delivery']];
					$payment = Core::$db->payment[(int) $_POST['payment']];

					if ($delivery && $payment) {
						$dp_price = estPrice($delivery['price'], $subtotal) + estPrice($payment['price'], $subtotal);

						if (!in_array($delivery['customer_group_id'], $group_ids) || !in_array($payment['customer_group_id'], $group_ids)) {
							$error = 'Payment or delivery are nto available for this customer group.';
						} else {
							if ($dp = Core::$db->delivery_payments()->where('delivery_id', $delivery['id'])->where('payment_id', $payment['id'])->fetch()) {
								$dp_price += estPrice($dp['price'], $subtotal);

								if ($dp['free_over'] && parseFloat($dp['free_over']) <= $order_total) {
									$dp_price = 0;
								}

								$data['delivery_id'] = $delivery['id'];
								$data['payment_id'] = $payment['id'];
								$data['delivery_payment'] = $dp_price;
							} else {
								// disallowed D&P combination
								$error = 'Disallowed delivery & payment combination.';
							}
						}
					} else {
						$error = 'Payment or delivery not selected.';
					}

					if ($error) {
						View::$global_data['order_errors'] = array(array($error, __('order_cannot_be_send')));
					} else {
						$_total = $order_total + price_vat($dp_price, VAT_DELIVERY)->price;

						if ($voucher) {
							$discount = estPrice($voucher['value'], $_total);

							$_total -= $discount;

							if ($_total < 0) {
								$_total = 0;
							}
						}

						$data['total_price'] = $subtotal;
						$data['total_incl_vat'] = round($_total, (int) Core::config('order_round_decimals'));

						$order->update($data);

						Core::$db->order_products()->where('order_id', $order['id'])->where('type', 1)->delete();

						Core::$db->order_products(array(
							'order_id' => $order['id'],
							'price' => $dp_price,
							'vat' => VAT_DELIVERY,
							'quantity' => 1,
							'type' => 1,
							'name' => __('delivery_payment') . ' [' . $delivery['name'] . ', ' . $payment['name'] . ']'
						));

						if ($payment && $payment['driver']) {
							$driverclass = 'Payment_' . ucfirst($payment['driver']);
							if ($inst = new $driverclass($payment)) {
								$order = Core::$db->order[(int) $order['id']];

								$inst->process($order);
							}
						}

						$_SESSION['order_id'] = (int) $order['id'];

						redirect(url(PAGE_ORDER_FINISH, array('finish' => $order['received']), true));
					}
				}

				return tpl('order_payment.latte', array(
							'deliveries' => $deliveries_payments['deliveries'],
							'payments' => $deliveries_payments['payments'],
							'order' => $order,
							'subtotal' => $subtotal,
							'order_total' => $order_total,
							'total' => $total,
							'voucher' => $voucher,
							'discount' => $discount
						));
			} else {
				redirect('');
			}
		} else {
			redirect('');
		}
	}

	private function getDeliveriesPayments($customer = null, $total_price = null, $total_weight = null)
	{
		$group_ids = array(0, (int) Core::config('customer_group_default'));

		if ($customer) {
			$group_ids[] = (int) Core::config('customer_group_registered');

			if (($gid = (int) $customer['customer_group_id'])) {
				$group_ids[] = $gid;
			}
		}

		$deliveries = array();
		$payments = array();

		foreach (Core::$db->delivery()->where('customer_group_id', $group_ids)->order('ord') as $del) {
			$st = true;

			if ($del['weight_min'] !== null && $del['weight_min'] > $total_weight) {
				$st = false;
			}

			if ($del['weight_max'] !== null && $del['weight_max'] < $total_weight) {
				$st = false;
			}

			if ($del['price_min'] !== null && $del['price_min'] > $total_price) {
				$st = false;
			}

			if ($del['price_max'] !== null && $del['price_max'] < $total_price) {
				$st = false;
			}

			if ($st) {
				$deliveries[] = $del;
			}
		}

		foreach (Core::$db->payment()->where('customer_group_id', $group_ids)->order('ord') as $pay) {
			$st = true;

			if ($pay['weight_min'] !== null && $pay['weight_min'] > $total_weight) {
				$st = false;
			}

			if ($pay['weight_max'] !== null && $pay['weight_max'] < $total_weight) {
				$st = false;
			}

			if ($pay['price_min'] !== null && $pay['price_min'] > $total_price) {
				$st = false;
			}

			if ($pay['price_max'] !== null && $pay['price_max'] < $total_price) {
				$st = false;
			}

			if ($st) {
				$payments[] = $pay;
			}
		}

		return array(
			'payments' => $payments,
			'deliveries' => $deliveries,
			'group_ids' => $group_ids
		);
	}

	public function rebuy($id = null) {
		if(!Customer::$logged_in) {
			redirect('');
		}


		if($id) {
			$last_order = Core::$db->order[(int) $id];
		}else{
			$last_order = Core::$db->order()->where('customer_id', Customer::get('id'))->order('id DESC')->limit(1)->fetch();
		}
		if(!$last_order) {
			redirect('');
		}

		$order_products = Core::$db->order_products()->where('order_id', $last_order['id']);

		foreach ($order_products as $product) {

			$exists_product = Core::$db->product[(int) $product['product_id']];
			$generovat_nazev = str_replace($exists_product['name'], '', $product['name']);


			Basket::add((int) $product['product_id'], null, $product['quantity'], $generovat_nazev);
		}

		redirect(PAGE_BASKET);
	}

	public function denied()
	{
		Core::show_404();
	}

	public function _show_404()
	{
		header('HTTP/1.1 404 File Not Found');

		$this->a(PAGE_404);
	}

	public function maintenance()
	{
		header('HTTP/1.1 503 Service Unavailable');

		$this->tpl = tpl('maintenance.latte');
	}

	public function ajaxbasket()
	{
		$this->tpl = null;
		$response = array('status' => false, 'msg' => '');

		if (!empty($_POST)) {
			if (!empty($_POST['product_id'])) {
				$product = Core::$db->product[(int) $_POST['product_id']];

				if (Core::$is_premium && $product && empty($_POST['attributes']) && count($product->product_attributes())) {
					$response['msg'] = 'Product has attributes that must be selected';
					$response['has_attributes'] = true;
					$response['redirect'] = url($product, null, true);
				} else {
					$attrs = !empty($_POST['attributes']) ? (array) $_POST['attributes'] : null;

					$result = Basket::add((int) $_POST['product_id'], $attrs, max((int) $_POST['qty'], 1));

					if ($result !== true && is_numeric($result)) {
						$response['msg'] = (string) __($result > 0 ? 'max_quantity' : 'basket_no_stock', $result);
					} else if ($result === true) {
						$response['status'] = true;
						$response['msg'] = (string) __('ajaxbasket_product_added');
					}
				}
			}
		}

		$response['products'] = array();

		if ($products = Basket::products()) {
			foreach ($products as $product) {
				$response['products'][] = array(
					'id' => $product['id'],
					'qty' => $product['qty'],
					'name' => $product['product']['name'],
					'url' => url($product['product'], null, true),
					'price' => $product['price'],
					'sku' => $product['sku'],
					'ean13' => $product['ean13'],
					'stock' => $product['stock'],
					'attributes' => $product['attributes'],
					'attributes_ids' => $product['attributes_ids'],
					'weight' => $product['weight']
				);
			}
		}

		$response['items'] = count($response['products']);
		$response['total'] = Basket::total()->total;

		$response['page_basket'] = url(PAGE_BASKET, null, true);

		echo json_encode($response);
	}

	public function ajaxbasket_block($block_id)
	{
		$this->tpl = null;

		if ($block = Core::$db->block[(int) $block_id]) {
			$basket_products = Basket::products();
			$total = Basket::total();

			$block_inst = new Block_Basket($block);

			echo $this->block['text'] . tpl('blocks/basket.latte', array('block' => $block_inst, 'basket_products' => $basket_products, 'total' => $total));
		}
	}

	private function getPageContent($file)
	{
		$info = pathinfo($file);

		if ($info['extension'] == 'texy') {
			$hash = md5($file);
			$cache = DOCROOT . 'etc/tmp/texy_' . $hash . '.cache.php';
			$fmtime = filemtime($file);

			if (file_exists($cache) && abs($fmtime - filemtime($cache)) < 3) {
				ob_start();
				include $cache;
				return ob_get_clean();
			}

			if (!class_exists('Texy')) {
				require_once DOCROOT . 'core/vendor/texy/texy.min.php';
			}

			$texy = new Texy();
			//$texy->headingModule->top = 2;
			$texy->imageModule->root = View::$global_data['tplbase'] . 'images/';

			if ($result = $texy->process(file_get_contents($file))) {
				file_put_contents($cache, "<?php defined('WEBMEX') OR die('No direct access.'); /* generated " . date('c') . " */ ?> \n" . $result);
				@touch($cache, filemtime($file));
			}

			return $result;
		}

		ob_start();
		include $file;
		return ob_get_clean();
	}

	private function getSubpages($page_id, & $ids = array())
	{
		foreach (Core::$db->page()->where('parent_page', $page_id)->where('status', 1) as $page) {
			$ids[] = $page['id'];

			$this->getSubpages($page['id'], $ids);
		}
	}

}