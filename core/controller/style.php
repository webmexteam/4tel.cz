<?php

defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */
class Controller_Style extends Controller
{
	protected $cache_offset = 86400;
	protected $design_mode = false;



	public function __construct()
	{
		parent::__construct();

		$this->design_mode = !empty($_SESSION['design_mode']) && User::$logged_in;

		header('Content-Type: text/css; charset=UTF-8');

		if (!$this->design_mode) {
			header('Expires: ' . gmdate('D, d M Y H:i:s', time() + $this->cache_offset) . ' GMT');
			header('Cache-control: cache');
			header('Pragma: cache');
		}

		$this->tpl = null;
	}



	public function less_css()
	{
		// Prepare
		$lessPath = substr($_SERVER['REQUEST_URI'], strlen(Core::$base));
		$lessPath = substr($lessPath, 0, strlen($lessPath) - 4);
		$fullLessPath = DOCROOT . "$lessPath";

		$compiledFilename = preg_replace('#[^a-zA-Z0-9\.]#', '-', $lessPath);
		$compiledFilename = preg_replace('#-{2,}#', '-', $compiledFilename);
		$compiledFilename = "$compiledFilename.css";
		$compiledPath = DOCROOT . 'etc/tmp/' . $compiledFilename;

		// Check
		if (!file_exists($fullLessPath)) {
			Core::show_404();
			exit;
		}

		// Render
		if (!file_exists($compiledPath) 
			|| (filectime($compiledPath) <= filectime($fullLessPath))
			|| $_SERVER['REMOTE_ADDR'] == '127.0.0.1'
		) {
			$compiledString = $this->compileLess($fullLessPath);
			file_put_contents($compiledPath, $compiledString);
		}

		echo file_get_contents($compiledPath);
		return;
	}



	private function compileLess($file)
	{
		require_once(APPROOT . 'vendor/lessphp/lessc.inc.php');

		try {
			$less = new lessc($file);

			$base = rtrim(preg_replace('/index\.php/', '', Core::$url), '/') . '/';
			$filebase = $base . substr(dirname($file), strlen(DOCROOT)) . '/';

			// global variables
			$variables = array(
				'base' => $base,
				'filebase' => $filebase,
			);

			$output = $less->parse(null, $variables);  // @ - bug in LessCSS

		} catch (Exception $e) {
			die($e->getMessage());
		}

		return $output;
	}



	/********************* OLD STYLE METHODS *********************/



	public function admin($style_file = null)
	{
		if ($style_file === null) {
			$style_file = Core::findFile('admin/template/default/css/style.less.css');
		} else {
			if (!($style_file = Core::findFile('admin/template/default/css/' . $style_file))) {
				$args = func_get_args();
				$path = join('/', $args);

				if ($path && file_exists(DOCROOT . $path)) {
					$style_file = $path;
				} else {
					Core::show_404();
					exit;
				}
			}
		}

		if (!preg_match('/\.css$/', $style_file)) {
			Core::show_404();
			exit;
		}

		$base = rtrim(preg_replace('/index\.php/', '', Core::$url), '/') . '/';
		$tplbase = $base . substr(dirname($style_file), strlen(DOCROOT), -3);

		$hash = md5(Core::$url . $style_file . Core::version);
		$cache = DOCROOT . 'etc/tmp/admin_' . $hash . '.css';

		if (!Core::$debug && $_SERVER['REMOTE_ADDR'] != '127.0.0.1' && file_exists($cache)) {
			include($cache);
			exit;
		}

		require_once(APPROOT . 'vendor/lessphp/lessc.inc.php');

		try {
			$less = new lessc($style_file);

			// global variables
			$variables = array(
				'base' => "\"".$base."\"",
				'tplbase' => "\"".$tplbase."\""
			);

			$output = $less->parse(null, $variables);
		} catch (Exception $e) {
			die($e->getMessage());
		}

		@file_put_contents($cache, $output);

		echo $output;
	}



	public function index()
	{
		$template = Core::config('template');
		$style = Core::config('template_style');
		$theme = Core::config('template_theme');

		if ($this->design_mode) {
			if (!empty($_SESSION['webmex_design_template'])) {
				$template = $_SESSION['webmex_design_template'];
			}

			if (!empty($_SESSION['webmex_design_style'])) {
				$style = $_SESSION['webmex_design_style'];
			}

			if (!empty($_SESSION['webmex_design_theme'])) {
				$theme = $_SESSION['webmex_design_theme'];
			}
		}

		$style_file = Core::findFile('template/' . $template . '/css/style/' . $style . '.less.css');
		$theme_file = Core::findFile('template/' . $template . '/css/theme/' . $style . '_' . $theme . '.less.css');

		if (!$theme_file) {
			$theme_file = DOCROOT . 'etc/theme/' . $template . '_' . $style . '_' . $theme . '.less.css';
		}

		$base = rtrim(preg_replace('/index\.php/', '', Core::$url), '/') . '/';
		$tplbase = $base . substr(dirname($style_file), strlen(DOCROOT), -9);

		$hash = md5(Core::$url . $template . $style . $theme . Core::version);
		$cache = DOCROOT . 'etc/tmp/' . $hash . '.css';

		if (!Core::$debug && $_SERVER['REMOTE_ADDR'] != '127.0.0.1' && file_exists($cache) && filemtime($cache) >= filemtime($theme_file) && !$this->design_mode) {
			include($cache);
			exit;
		}

		require_once(APPROOT . 'vendor/lessphp/lessc.inc.php');

		try {
			$less = new lessc($style_file);

			// global variables
			$variables = array(
				'base' => "\"".$base."\"",
				'tplbase' => "\"".$tplbase."\"",
				'tplfolder' => "\"".Core::config('template')."\""
			);

			$theme_content = null;

			if (file_exists($theme_file)) {
				$theme_content = file_get_contents($theme_file) . "\n";

				if ($this->design_mode) {
					foreach ($_SESSION['design_mode']['css'] as $k => $v) {

						if (preg_match('/image$/', $k)) {
							if (!$v) {
								$v = 'none';
							} else {
								$v = 'url("' . (preg_match('/^https?\:/', $v) ? '' : '__base__') . $v . '")';
							}
						}

						$theme_content .= '@' . $k . ': ' . $v . ";\n";
					}
				}

				$theme_content = preg_replace('/__base__/mui', $base, $theme_content);

				$less->import_content($theme_content);
			}

			$output = $less->parse(null, $variables);

			foreach (View::$css_files['front'] as $cssfile) {
				if (substr($cssfile, -4) == '.css') {
					$cssfile_content = file_get_contents($cssfile);
					$output .= $less->parse($theme_content . "\n" . $cssfile_content, $variables);
				}
			}
		} catch (Exception $e) {
			die($e->getMessage());
		}

		if (file_exists(DOCROOT . 'etc/style.css')) {
			$output .= file_get_contents(DOCROOT . 'etc/style.css');
		}

		if(file_exists(DOCROOT.'etc/style.less')){

			try{
				$less = new lessc(DOCROOT.'etc/style.less');

				// global variables
				$variables = array(
					'base' => $base,
					'tplbase' => $tplbase
				);

				$output .= $less->parse(null, $variables);
				
			}catch(Exception $e){
				die($e->getMessage());
			}
		}

		if ($this->design_mode) {
			echo $output;
			exit;
		}

		@file_put_contents($cache, $output);

		echo $output;
	}



	private function minify($str)
	{
		$result = preg_replace('!/\*[^*]*\*+([^/][^*]*\*+)*/!', '', $result);

		return str_replace(array("\r\n", "\r", "\n", "\t", '  ', '    ', '    '), ' ', $result);
	}

}