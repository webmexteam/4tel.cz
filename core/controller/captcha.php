<?php

defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */
class Controller_Captcha extends Controller
{

	protected $safe_chars = 'ABDEFGHJKLMNPRSTWXYZ23456789';

	public function __construct()
	{
		$this->tpl = null;
	}

	public function index()
	{
		redirect('/');
	}

	public function img($hash)
	{
		if (!empty($_SESSION['captcha'][$hash])) {
			$opt = $_SESSION['captcha'][$hash][0];

			if (!$opt) {
				die('Invalid session data');
			}

			if (!$_SESSION['captcha'][$hash][2]) {
				$_SESSION['captcha'][$hash][2] = 0;
			}

			$_SESSION['captcha'][$hash][2]++;

			if ($_SESSION['captcha'][$hash][2] <= 5) {
				$code = $this->generateCode($opt['length']);
				$_SESSION['captcha'][$hash][1] = md5($code);
			}

			$img = imagecreatetruecolor($opt['width'], $opt['height']);
			imagefill($img, 0, 0, imagecolorallocate($img, $opt['bgcolor'][0], $opt['bgcolor'][1], $opt['bgcolor'][2]));

			$font = APPROOT . 'vendor/fonts/sans.ttf';
			$txtwidth = strlen($code) * $opt['fontsize'] * 0.75;
			$margin = ($opt['width'] - $txtwidth) / 2;

			foreach (str_split($code) as $i => $char) {
				$r = mt_rand(-25, 25);
				$c = imagecolorresolvealpha($img, mt_rand(0, 150), mt_rand(50, 200), mt_rand(0, 150), 50);

				imagettftext($img, $opt['fontsize'], $r, ($i * ($opt['fontsize'] * 0.70)) + $margin, ($opt['height'] - $opt['fontsize']) / 2 + $opt['fontsize'], $c, $font, $char);
			}

			$fs = $opt['fontsize'] * 1.8;

			// foreach (str_split($this->generateCode(5)) as $i => $char) {
			// 	$r = mt_rand(-70, 70);
			// 	$c = imagecolorresolvealpha($img, mt_rand(0, 70), mt_rand(50, 120), mt_rand(0, 70), 95);

			// 	imagettftext($img, $fs, $r, ($i * ($fs * 0.75)) - 10, ($opt['height'] - $fs) / 2 + $fs, $c, $font, $char);
			// }

			if (!$code) {
				imagettftext($img, $opt['fontsize'] * 0.4, 0, 6, ($opt['height'] - $opt['fontsize'] * 0.4) / 2 + $opt['fontsize'] * 0.4, 0, $font, 'Too much reloads');
			}

			while(ob_get_level()) ob_end_clean();  // hotfix for unwanted white-spaces
			
			header('Content-Type: image/gif');
			header('Cache-Control: no-cache');
			header('Expires: ' . gmdate('D, d M Y H:i:s', time()) . ' GMT');
			imagegif($img);
		} else {
			die('Invalid hash');
		}
	}

	public function generateCode($len = 4)
	{
		$str = '';
		$chars = str_split($this->safe_chars);

		foreach ((array) array_rand($chars, $len) as $i) {
			$str .= $chars[$i];
		}

		return $str;
	}

}