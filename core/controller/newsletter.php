<?php

defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */
class Controller_Newsletter extends Controller
{

	public function index()
	{
		$this->content = tpl('default_3columns.latte');

		if (!empty($_POST['newsletter_email']) && valid_email($_POST['newsletter_email'])) {
			if (!Core::$db->newsletter_recipient()->where('email', $_POST['newsletter_email'])->fetch()) {
				Core::$db->newsletter_recipient(array(
					'email' => $_POST['newsletter_email'],
					'date' => time()
				));

				Voucher::generate('newsletter_signup', $_POST['newsletter_email']);

				$content = __('email_registered');
			} else {
				$content = __('email_already_registered');
			}
		} else {
			$content = __('enter_your_email');
		}

		$this->content->content = tpl('newsletter.latte', array('content' => $content));
	}

}