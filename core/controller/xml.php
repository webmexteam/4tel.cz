<?php

defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */
class Controller_Xml extends Controller
{

	public $fp, $file;
	public $cache = true;

	public function __construct()
	{
		@header('Content-Type: application/xml; charset=UTF-8');
	}

	public function export($format)
	{
		$this->tpl = null;

		@set_time_limit(240);

		$static_file = DOCROOT . 'etc/feed/' . $format . '.xml';

		if (is_readable($static_file) && filesize($static_file) && filemtime($static_file) >= strtotime('-48 hours')) {
			$fp = fopen($static_file, 'r');

			fpassthru($fp);

			exit;
		} else if (false && Core::config('xml_feeds') == 'static') {
			$this->staticfile($format);
		} else {
			$this->cache = false;

			$f = Core::findFile('template/system/xml_feeds/' . $format . '.php');

			if (!empty($_GET['k']) && $_GET['k'] == Core::config('access_key') && file_exists($f)) {

				extract(View::$global_data);

				require_once $f;

				exit;
			}
		}

		Core::show_404();
	}

	public function create($format)
	{
		$this->tpl = null;

		@set_time_limit(240);

		$f = Core::findFile('template/system/xml_feeds/' . $format . '.php');

		if (!empty($_GET['k']) && $_GET['k'] == Core::config('access_key') && file_exists($f)) {
			extract(View::$global_data);

			if (!is_dir(DOCROOT . 'etc/feed')) {
				FS::mkdir(DOCROOT . 'etc/feed', true);
			}

			$file = 'etc/feed/' . $format . '.xml';
			$this->file = DOCROOT . $file;

			ob_start();

			require_once $f;

			$this->_flush();

			if ($this->fp) {
				fclose($this->fp);
				FS::writable(DOCROOT . $file);
			}

			return DOCROOT . $file;
		}

		return false;
	}

	public function _getCategories($product, $path_sep = '/', $cat_sep = '|')
	{
		$str = '';

		foreach ($product->product_pages() as $page) {
			$str .= $cat_sep . $this->_getCategoryPath($page, $path_sep);
		}

		return trim($str, ' ' . $cat_sep . $path_sep);
	}

	public function _getCategoryPath($page, $path_sep = '/')
	{
		if (!$page['id']) {
			$page = $page->page;
		}

		$path = $page['name'];

		while ($page && (int) $page['parent_page']) {
			$parent = Core::$db->page[(int) $page['parent_page']];

			if ($parent) {
				$path = $parent['name'] . $path_sep . $path;
				$page = $parent;
			} else {
				$page = null;
			}
		}

		return $path;
	}

	public function _getManufacturer($product)
	{
		foreach ($product->product_pages() as $page) {
			$page = $page->page;

			if ($page['menu'] == 3 && !(int) $page['parent_page']) {
				return $page['name'];
			}
		}

		return null;
	}

	public function _flush()
	{
		if ($this->cache && !$this->fp) {
			$this->fp = fopen($this->file, 'w');
		}

		if (!$this->fp || !$this->cache) {
			flush();
			return;
		}

		$c = trim(preg_replace('/[\s\t\n]{2,}/miu', '', ob_get_contents()));

		fwrite($this->fp, $c);

		ob_clean();
	}

}
