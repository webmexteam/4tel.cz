<?php

defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */
class Controller_Customer extends Controller
{

	protected $customer;

	public function __construct()
	{
		parent::__construct();

		if (!Core::$is_premium || !(int) Core::config('enable_customer_login')) {
			redirect(null);
		}

		if (isSet($_POST['login']) && !empty($_POST['customer_email']) && !empty($_POST['customer_password'])) {
			// customer login
			if (($result = Customer::login($_POST['customer_email'], $_POST['customer_password'], isSet($_POST['customer_remember']))) !== true) {
				View::$global_data['customer_login_error'] = __($result);
			} else {
				redirect('customer');
			}
		}

		if (isSet($_POST['reset_password']) && !empty($_POST['customer_email_reset'])) {
			$result = $this->reset_password($_POST['customer_email_reset']);

			if (!$result) {
				View::$global_data['customer_login_error'] = __('user_not_found');
			} else {
				View::$global_data['customer_login_msg'] = __('customer_reset_code_sent');
			}
		}

		if (!Customer::$logged_in && !preg_match('/^customer\/(login|reset|fblogin|register|lost_password)/i', Core::$uri)) {
			redirect('customer/login');
		}

		setMeta(array('title' => __('customer')->__toString()));

		$this->content = tpl('default_2columns.latte');

		$this->customer = Core::$db->customer[(int) Customer::get('id')];
	}

	public function fblogin() {
		$this->tpl = null;
		if(Customer::fblogin()){
			echo json_encode(array('success'=>1,'redirect'=>url('customer')));
			exit;
		}
		echo json_encode(array('success'=>1,'redirect'=>url('customer/login?error=webuser_exists')));
		exit;

		redirect('customer');
	}

	public function index()
	{
		$limit = 5;

		$orders = $this->customer->order()->limit($limit, pgOffset($limit))->order('received DESC');

		$total_count = $this->customer->order();

		if ($orders->getWhere()) {
			$total_count->where($orders->getWhere());
		}

		$total_count = $total_count->group("COUNT(*)");
		$total_count = (int) $total_count[0];

		$this->content->content = tpl('customer_account.latte', array(
            'customer' => $this->customer,
			'active_tab' => 'orders',
			'content' => tpl('customer_orders.latte', array(
				'orders' => $orders,
				'limit' => $limit,
				'total_count' => $total_count
			))
				));
	}

	public function register()
	{
		$data = array();

		if (isSet($_POST['save'])) {
			$data = $_POST;
			$errors = array();

			$required = array('first_name', 'last_name', 'street', 'city', 'zip', 'phone');

			if (!(int) Core::config('customer_confirmation')) {
				$required[] = 'password1';
				$required[] = 'password2';
			}

			foreach ($required as $name) {
				if (empty($data[$name])) {
					$errors[] = array('required', __($name));
				} else if ($name == 'email' && !valid_email($data[$name])) {
					$errors[] = array('invalid_email', __($name));
				} else if ($name == 'phone') {
					$data[$name] = preg_replace('/[^\d\+]/', '', trim($data[$name]));

					if (!preg_match('/^((\+|00)[\d]{2,3})?[\d]{7,14}?$/', $data[$name])) {
						$errors[] = array('invalid_phone', __($name));
					}
				}
			}

			if (isSet($data['shipping_as_billing'])) {
				$data['ship_first_name'] = '';
				$data['ship_last_name'] = '';
				$data['ship_street'] = '';
				$data['ship_city'] = '';
				$data['ship_zip'] = '';
				$data['ship_country'] = '';
				$data['ship_company'] = '';
			}

			if (!empty($data['password1']) && !empty($data['password2'])) {
				if ($data['password1'] == $data['password2']) {
					$data['password'] = sha1(sha1(trim($data['password1'])));
				} else {
					$errors[] = array('new_passwords_dont_match', __('password'));
				}
			}

			if ($customer = Core::$db->customer()->where('email', $data['email'])->fetch()) {
				$errors[] = array('email_already_registered', __('email'));
			}

			if (empty($errors)) {
				$data['active'] = (int) Core::config('customer_confirmation') ? 0 : 1;

				$customer_id = Core::$db->customer(prepare_data('customer', $data));

				$has_newsletter = (bool) Core::$db->newsletter_recipient()->where('email', $data['email'])->fetch();

				if (isSet($data['newsletter']) && !$has_newsletter) {
					Core::$db->newsletter_recipient(array(
						'email' => $data['email'],
						'date' => time()
					));
				} else if ($has_newsletter) {
					Core::$db->newsletter_recipient()->where('email', $data['email'])->delete();
				}

				$customer_data = $data;
				$customer_data['id'] = $customer_id;

				if (!(int) Core::config('customer_confirmation')) {
					$customer_data['password'] = $data['password1'];

					Email::event('new_account', $customer_data['email'], null, $customer_data);

					Voucher::generate('customer_register', $customer_data);

					Customer::login($data['email'], $data['password1']);
				} else {
					$_SESSION['customer_confirm'] = true;

					sendmail(Core::config('email_notify'), array(__('customer_registered'), __('customer_registered_text')), $customer_data);
				}

				redirect('customer');
			} else {
				View::$global_data['customer_errors'] = $errors;
			}
		}

		$this->content->content = tpl('customer_register.latte', array(
			'register' => true,
			'customer' => $data
				));
	}

	public function lost_password()
	{
		$this->content = tpl('default_3columns.latte');

		$this->content->content = tpl('customer_lost_password.latte', array(
				));
	}

	public function order($id)
	{
		$order = Core::$db->order[(int) $id];

		if ($order && $order['customer_id'] == $this->customer['id']) {
			$this->content->content = tpl('customer_account.latte', array(
                'customer' => $this->customer,
				'active_tab' => 'orders',
				'content' => tpl('customer_order.latte', array(
					'order' => $order
				))
					));
		} else {
			redirect('customer');
		}
	}

	public function invoice($id)
	{
		$this->tpl = null;

		if ($id == 0) {
			redirect('customer');
		}

		$invoice = Core::$db->invoice[(int) $id];

		if ($id > 0 && !$invoice || $invoice->order['customer_id'] != Customer::get('id')) {
			redirect('customer');
		}

		$inv = new Invoice();

		$inv->vat = $invoice['vat'];
		$inv->invoice_num = $invoice['invoice_num'];
		$inv->payment = $invoice['payment'];
		$inv->delivery = $invoice['delivery'];
		$inv->symbol = $invoice['symbol'];
		$inv->date_issue = $invoice['date_issue'];
		$inv->date_due = $invoice['date_due'];
		$inv->date_vat = $invoice['date_vat'];
		$inv->date_create = $invoice['date_create'];

		$inv->note = $invoice['note'];
		$inv->status = $invoice['status'];
		$inv->voucher_id = $invoice['voucher_id'];
		$inv->voucher_id = $invoice['voucher_id'];

		$inv->type = $invoice['type'];
		$inv->vat_payer = $invoice['vat_payer'];
		$inv->shipping_address = $invoice['shipping_address'];

		if (!$invoice['customer_name']) {
			$inv->customer = $invoice['customer']; // for backward compatibility (version 1.2)
		} else {
			$inv->customer = $invoice['customer_name'] . "\n" . $invoice['address']
					. "\n\n" . __('company_id') . ': ' . $invoice['company_id']
					. "     " . __('company_vat') . ': ' . $invoice['company_vat'];
		}

		$inv->issuer = Core::config('invoice_issuer');
		$inv->footer = Core::config('invoice_footer');

		foreach ($invoice->invoice_items() as $item) {
			$inv->add($item['sku'], $item['name'], $item['quantity'], $item['price'], $item['vat']);
		}

		$inv->save($invoice['invoice_num'] . '.pdf', 'D');

		exit;
	}

	public function settings()
	{
		$has_newsletter = (bool) Core::$db->newsletter_recipient()->where('email', $this->customer['email'])->fetch();

		if (isSet($_POST['save'])) {
			$data = $_POST;
			$errors = array();

			$required = array('first_name', 'last_name', 'street', 'city', 'zip', 'phone');

			foreach ($required as $name) {
				if (empty($data[$name])) {
					$errors[] = array('required', __($name));
				} else if ($name == 'email' && !valid_email($data[$name])) {
					$errors[] = array('invalid_email', __($name));
				} else if ($name == 'phone') {
					$data[$name] = preg_replace('/[^\d\+]/', '', trim($data[$name]));

					if (!preg_match('/^((\+|00)[\d]{2,3})?[\d]{7,14}?$/', $data[$name])) {
						$errors[] = array('invalid_phone', __($name));
					}
				}
			}

			if (isSet($data['shipping_as_billing'])) {
				$data['ship_first_name'] = '';
				$data['ship_last_name'] = '';
				$data['ship_street'] = '';
				$data['ship_city'] = '';
				$data['ship_zip'] = '';
				$data['ship_country'] = '';
				$data['ship_company'] = '';
			}

			if (!empty($data['password1']) && !empty($data['password2'])) {
				if (!empty($data['old_password']) && $this->customer['password'] == sha1(sha1(trim($data['old_password'])))) {
					if ($data['password1'] == $data['password2']) {
						$data['password'] = sha1(sha1(trim($data['password1'])));
					} else {
						$errors[] = array('new_passwords_dont_match', '');
					}
				} else {
					$errors[] = array('wrong_password', '');
				}
			}


			if (empty($errors)) {
				$data['active'] = $this->customer['active'];

				$this->customer->update(prepare_data('customer', $data));

				if (isSet($data['newsletter']) && !$has_newsletter) {
					Core::$db->newsletter_recipient(array(
						'email' => $this->customer['email'],
						'date' => time()
					));
				} else if ($has_newsletter) {
					Core::$db->newsletter_recipient()->where('email', $this->customer['email'])->delete();
				}

				redirect('customer/settings');
			} else {
				View::$global_data['customer_errors'] = $errors;
			}
		}

		$this->content->content = tpl('customer_account.latte', array(
            'customer' => $this->customer,
			'active_tab' => 'settings',
			'content' => tpl('customer_settings.latte', array(
				'customer' => $this->customer,
				'has_newsletter' => $has_newsletter
			))
				));
	}

	public function login()
	{
		$this->content = tpl('default_2columns.latte');

		if (!empty($_SESSION['reset_password'])) {
			View::$global_data['customer_login_msg'] = __('customer_new_password_sent');
			unset($_SESSION['reset_password']);
		}

		$this->content->content = tpl('customer_login.latte');
	}

	public function logout()
	{
		$this->tpl = null;

		Customer::logout();
		redirect(null);
	}

	public function reset($customer_id, $time, $hash)
	{
		$this->tpl = null;

		$customer = Core::$db->customer[(int) $customer_id];

		if ($customer && md5($customer['password'] . $time) == $hash && $time >= strtotime('-24 hours')) {
			$new_pass = strtoupper(random());

			$customer->update(array(
				'password' => sha1(sha1($new_pass))
			));

			$customer['password'] = $new_pass;

			Email::event('new_password', $customer['email'], null, $customer);

			$_SESSION['reset_password'] = true;

			redirect('customer');
		}

		echo __('invalid_reset_request');
	}

	private function reset_password($email)
	{
		if (valid_email($email)) {
			$customer = Core::$db->customer()->where('email', $email)->fetch();

			if ($customer) {
				$time = time();

				$customer['link'] = url('customer/reset/' . $customer['id'] . '/' . $time . '/' . md5($customer['password'] . $time), null, true);

				Email::event('reset_password', $customer['email'], null, $customer);

				return true;
			}
		}

		return false;
	}

}