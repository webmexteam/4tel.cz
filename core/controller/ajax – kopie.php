<?php

defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 * @author Tomas Nikl <tomasnikl.cz@gmail.com>
 */
class Controller_Ajax2 extends Controller
{

	public function __construct() {
		$this->tpl = null;
	}

	public function recalculate_basket() {

		// Rozparsujeme $_POST data
		$post = array();
		parse_str($_POST['data'], $post);

		// Aktualizujeme polozky v kosiku
		$product_data = array();
		foreach($post['qty'] as $_id => $_qty) {
			if((int) $_qty <= 0){
				$_qty = 1;
			}
			$product_data[$_id] = $_qty;
		}

		Basket::update($product_data);

		$total = Basket::total();
		$data = array();

		foreach(Basket::products() as $product){

			ob_start();
				echo price_vat(array($product['product'], ($product['price'] * $product['qty'])));
				$product_total_price = ob_get_contents();
			ob_clean();

			$data['products'][$product['id']] = array(
				'id'       => $product['id'],
				'price'    => price_vat(array($product['product'], $product['price'])),
				'quantity' => $product['qty'],
				'total'    => $product_total_price,
				'qty'	   => $product_data[$product['id']]
			);
		}

		if((int) Core::config('vat_payer')){
			ob_start();
				echo price($total->subtotal);
				$subtotal = ob_get_contents();
			ob_clean();

			$data['subtotal'] = $subtotal;
		}

		if(Basket::voucher()){
			ob_start();
				echo '-'.price($total->discount);
				$voucher = ob_get_contents();
			ob_clean();

			$data['voucher'] = $voucher;
		}

		ob_start();
			echo price($total->total);
			$total_price = ob_get_contents();
		ob_clean();

		$data['total'] = $total_price;

		echo json_encode($data);
	}

	public function recalculate_filter() 
	{
		if(!isset($_GET['page_id']) || !$page = Core::$db->page[(int) $_GET['page_id']])
			return false;

		$this->getSubpages($page['id'], $ids);

		$p = Core::$db->product_pages()->where('page_id', $ids)->select('product_id') . ') OR id IN (' . $page->product_pages()->select('product_id');

		// $pas = Core::$db->page()->select('id, name')->where('menu', 3)->where('page.id IN (
		// 	SELECT page_id FROM product_pages WHERE product_id IN (
		// 		SELECT product.id FROM product WHERE product.id IN (
		// 			SELECT product_id FROM product_pages WHERE page_id IN ('.join(',', (array) $ids).')
		// 		)
		// 	)
		// )');

		if(isset($_GET['price_from']) && (float) $_GET['price_from'] >= 0 && isset($_GET['price_to']) && (float) $_GET['price_to'] > 0) {
			$price_sql = "
				AND (
					(product.price - product.price / 100 * 
						(
							COALESCE(product.discount, 0) 
							+ COALESCE((SELECT SUM(discount) FROM page WHERE product.id IN ( SELECT page_id FROM product_pages WHERE product_id = product.id)), 0)
						)
					) * 1
				) BETWEEN ".(float) $_GET['price_from']." AND ".(float) $_GET['price_to']."";
		}else{
			$price_sql = "";
		}

		$feature_sql = "";
		if(isset($_GET['f']) && $featureset = $page['featureset_id']) {
			$feature_where = "";
			$i = 1;

			$current_fid = "";
			foreach ($_GET['f'] as $fid => $fvalues) {
				if(isset($fvalues[0]) && $fvalues[0] == "")
					continue;

				$x = 1;
				foreach ($fvalues as $key => $fvalue) {
					if($current_fid == $fid){
						$feature_where .= " OR ";
					}else if($i==1){
						$feature_where .= "WHERE ";
					}else{
						$feature_where .=" AND ";
					}
					if($x == 1){
						$feature_where .= "(";
					}
					$feature_where .= "f".$fid." = '".$fvalue."'";
					if($x == count($fvalues)) {
						$feature_where .= ")";
					}

					$current_fid = $fid;

					$x++;
				}
				$i++;
			}
			if($feature_where != ""){
				$feature_sql .= "
					AND page.id IN (
						SELECT page_id FROM product_pages WHERE product_id IN (
							SELECT product_id FROM features_".$featureset." ".$feature_where."
						)
					)
				" ;

				$feature_sql .= "
					AND product.id IN (
						SELECT product_id FROM features_".$featureset." ".$feature_where."
					)
				";
			}
		}


		$_producers = Core::$db_conn->query('
			SELECT page.id, COUNT(product.id) count FROM page
			LEFT JOIN product ON product.id IN (
				SELECT product_id FROM product_pages WHERE page_id = page.id
			)
			WHERE (menu = 3) AND (page.id IN (
				SELECT page_id FROM product_pages WHERE product_id IN (
					SELECT product.id FROM product WHERE product.id IN (
						SELECT product_id FROM product_pages WHERE page_id IN (39,40)
					)
				)
			))
			'.$price_sql.'
			'.$feature_sql.'
			GROUP BY page.id
		');

		$producers = array();
		foreach ($_producers as $producer) {
			$producers[] = array(
				'id' => $producer['id'],
				'count' => $producer['count']
			);
		}

		if ((int) $page['inherit_products']) {
			$this->getSubpages($page['id'], $ids);

			if(Core::config('hide_no_stock_products')) {
				$products = Core::$db->product()->where('stock > 0 AND hide_product_on_zero_stock = 1 OR hide_product_on_zero_stock != 1')->where('id IN (' . Core::$db->product_pages()->where('page_id', $ids)->select('product_id') . ') OR id IN (' . $page->product_pages()->select('product_id') . ')');
			}else{
				$products = Core::$db->product()->where('id IN (' . Core::$db->product_pages()->where('page_id', $ids)->select('product_id') . ') OR id IN (' . $page->product_pages()->select('product_id') . ')');
			}
		} else {
			if(Core::config('hide_no_stock_products')) {
				$products = Core::$db->product()->where('stock > 0 AND hide_product_on_zero_stock = 1 OR hide_product_on_zero_stock != 1')->where('id', $page->product_pages()->select('product_id'));
			}else{
				$products = Core::$db->product()->where('id', $page->product_pages()->select('product_id'));
			}
		}

		if (!empty($_GET['price_from']) && (float) $_GET['price_from'] > 0 && !empty($_GET['price_to']) && (float) $_GET['price_to'] > 0) {
			$products->where("(
				(price - price / 100 * 
				(
					COALESCE(discount, 0) 
					+ COALESCE((SELECT SUM(discount) FROM page WHERE id IN ( SELECT page_id FROM product_pages WHERE product_id = product.id)), 0)
				)) * 1
				) BETWEEN ".(float) $_GET['price_from']." AND ".(float) $_GET['price_to']."");
		}

		if (!empty($_GET['f']) && ($featureset = $page->featureset) && $featureset['id']) {
			$tbl_name = 'features_' . $featureset['id'];

			$_ids = Core::$db->$tbl_name();

			foreach ($_GET['f'] as $fkey => $fdata) {
				if(is_array($fdata)) {
					$is_in = array();
					$filter_this = true;
					foreach ($fdata as $fid => $fvalue) {
						if ($fvalue !== '') {
							$is_in[] = "'".$fvalue."'";
							//$_ids->where('f' . $fkey, $fvalue);
						}else{
							$filter_this = false;
						}
					}
					if($filter_this) {
						$_ids->where('f' . $fkey . ' IN (' . join(',', (array) $is_in) . ')');
					}
				}
			}

			if ($_ids->getWhere()) {
				$products->where('id', $_ids->select('product_id'));
			}
		}

		$producers[] = array(
				'id' => 0,
				'count' => $products->count()
			);


		// Features
		$features = array();

		$feature_sql = "";
		if(isset($_GET['f']) && $featureset = $page['featureset_id']) {
			$feature_where = "";
			$i = 1;

			$current_fid = "";
			foreach ($_GET['f'] as $fid => $fvalues) {
				if(isset($fvalues[0]) && $fvalues[0] == "")
					continue;

				$x = 1;
				foreach ($fvalues as $key => $fvalue) {
					if($current_fid == $fid){
						$feature_where .= " OR ";
					}else if($i==1){
						$feature_where .= "WHERE ";
					}else{
						$feature_where .=" AND ";
					}
					if($x == 1){
						$feature_where .= "(";
					}
					if(!is_numeric($fvalue)) {
						$fvalue = "'".$fvalue."'";
					}
					$feature_where .= "f".$fid." = ".$fvalue;
					if($x == count($fvalues)) {
						$feature_where .= ")";
					}

					$current_fid = $fid;

					$x++;
				}
				$i++;
			}
			if($feature_where != ""){
				$feature_sql .= "
					AND page.id IN (
						SELECT page_id FROM product_pages WHERE product_id IN (
							SELECT product_id FROM features_".$featureset." ".$feature_where."
						)
					)
				" ;
			}
		}

		$fsql = Core::$db_conn->query('
			SELECT features_1.* FROM features_1
			LEFT JOIN product ON product.id = features_1.product_id
			'.$feature_where.'
			AND product.status = 1
		');

		$fes = array();
		foreach ($fsql as $_feature) {
			foreach (Core::$db_inst->tableFields('features_1') as $col_name => $col_data) {
				if(substr($col_name, 0, 1) == "f") {
					$feature_id = (int) substr($col_name, 1);
					$feature_val = $_feature[$col_name];

					if($col_data['type'] == "varchar") {
						$val = "'".$feature_val."'";
					}else{
						$val = $feature_val;
					}

					if($feature_where) {
						$new_feature_where = $feature_where . " AND (f".$feature_id." = ".$val.") ";
					}else{
						$new_feature_where = "WHERE (f".$feature_id." = ".$val.") ";
					}

					$count_sql = Core::$db_conn->query('
						SELECT count(product.id) count FROM product WHERE id IN (
							SELECT features_1.product_id FROM features_1
							'.$new_feature_where.'
							AND product.status = 1
						)
					');

					$count = $count_sql->fetch();

					if(substr($feature_val, -3) == ".00") {
						$feature_val = (int) $feature_val;
					}

					$fes[] = array(
						'feature_id' => $feature_id,
						'id' => $_feature['id'],
						'value' => $feature_val,
						'count' => $count['count']
					);
				}
			}	
		}

		echo json_encode(array(
				'producers' => $producers,
				'features' => $fes
			));
	}

	private function getSubpages($page_id, & $ids = array())
	{
		foreach (Core::$db->page()->where('parent_page', $page_id)->where('status', 1) as $page) {
			$ids[] = $page['id'];

			$this->getSubpages($page['id'], $ids);
		}
	}

}