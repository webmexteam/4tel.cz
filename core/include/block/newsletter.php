<?php

defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */
Block::$drivers[] = 'newsletter';

class Block_Newsletter extends Block
{

	protected $msg;

	public function getContent()
	{
		return $this->block['text'] . tpl('blocks/newsletter.latte', array('block' => $this));
	}

}