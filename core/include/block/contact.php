<?php

defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */
Block::$drivers[] = 'contact';

class Block_Contact extends Block
{

	private $sent = false;
	private $error = null;
	public $config = array(
		'email' => '',
		'captcha' => 0
	);

	public function __construct($block = null)
	{
		parent::__construct($block);

		if (isSet($_POST['contact-text']) && !empty($_POST['block_id']) && $_POST['block_id'] == $this->block['id']) {
			$send = true;

			if ((int) $this->config['captcha'] && !empty($_GET['js'])) {
				if (!Captcha::validate('contact' . $this->block['id'], $_POST['captcha'])) {
					$send = false;
					$this->error = __('captcha_invalid_code');
				}
			}

			if(!empty($_POST['contact-email'])) {
				$send = false;
				$this->error = __('captcha_invalid_code');
			}

			if ((int) $this->config['captcha'] && $_POST['bcc_' . $this->block['id']] != $this->block['id']) {
				$send = false;
			}

			if (empty($_POST['contact-contact'])) {
				$send = false;
				$this->error = __('fill_in_your_contact');
			}

			if ($send) {
				$this->sent = $this->send($_POST['contact-contact'], $_POST['contact-text']);
			}
		}
	}

	public function getContent()
	{
		$id = $this->block['id'];
		$values = array();

		if (!$this->sent && !empty($_POST['contact-contact'])) {
			$values['contact-contact'] = $_POST['contact-contact'];
		}

		if (!$this->sent && !empty($_POST['contact-text'])) {
			$values['contact-text'] = $_POST['contact-text'];
		}

		return $this->block['text'] . tpl('blocks/contact.latte', array('block' => $this, 'values' => $values, 'sent' => $this->sent, 'error' => $this->error));
	}

	public function getConfigForm()
	{
		return tpl('blocks/config_contact.latte', array('block' => $this));
	}

	private function send($contact, $msg)
	{
		if ($this->config['email'] && trim($msg)) {
			$mailer = mailerConnect();
			$mailer->From = Core::config('email_sender');
			$mailer->FromName = Core::config('store_name');
			$mailer->AddAddress($this->config['email']);

			if (valid_email($contact)) {
				$mailer->AddReplyTo($contact);
			}

			$subject = __('customer_message');
			$text = __('customer_message_text', Core::config('store_name'), $contact) . $msg;

			$text .= "\n\nURL: " . url(true, null, true);

			$mailer->Subject = $subject;
			$mailer->Body = $text;

			return $mailer->send();
		}

		return false;
	}

}