<?php

defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */
class Menu
{

	public static $items = array();
	public static $active_item;

	public static function add($config, $key, $parent_path = '')
	{
		$parts = explode('/', trim($parent_path, ' /'));

		if (count($parts) == 0 || empty($parts[0])) {
			$item = & self::$items;
		} else {
			$item = & self::$items['_' . $parts[0]];

			foreach (array_splice($parts, 1) as $part) {
				$item = & $item['_' . $part];
			}
		}

		$config['key'] = $key;

		$item['_' . $key] = $config;

		uasort($item, array('self', 'sortItems'));

		if (!empty($config['menu'])) {
			foreach ($config['menu'] as $subkey => $subitem) {
				self::add($subitem, $subkey, $parent_path . '/' . $key);
			}
		}
	}

	public static function remove($path)
	{
		$parts = explode('/', trim($path, ' /'));

		if (!empty($parts[0])) {

			if (count($parts) == 1) {
				unset(self::$items['_' . $parts[0]]);
				return;
			}

			$item = & self::$items['_' . $parts[0]];

			foreach (array_splice($parts, 1, count($parts) - 2) as $part) {
				$item = & $item['_' . $part];
			}

			unset($item['_' . array_pop($parts)]);
		}
	}

	public static function setAttr($path, $name, $value)
	{
		$parts = explode('/', trim($path, ' /'));

		if (count($parts) == 0 || empty($parts[0])) {
			$item = & self::$items;
		} else {
			$item = & self::$items['_' . $parts[0]];

			foreach (array_splice($parts, 1) as $part) {
				$item = & $item['_' . $part];
			}
		}

		$item[$name] = $value;
	}

	public static function findItem($path, &$item)
	{
		$parts = explode('/', trim($path, ' /'));

		if (count($parts) == 0 || empty($parts[0])) {
			$item = & self::$items;
		} else {
			$item = & self::$items['_' . $parts[0]];

			foreach (array_splice($parts, 1) as $part) {
				$item = & $item['_' . $part];
			}
		}
	}

	private static function sortItems($a, $b)
	{
		if (!is_array($a) || !isSet($a['position'])) {
			return -1;
		}

		if (!is_array($b) || !isSet($b['position'])) {
			return 1;
		}

		if ($a['position'] == $b['position']) {
			return 0;
		}

		return ($a['position'] < $b['position']) ? -1 : 1;
	}

	public static function countSubitems($item)
	{
		$total = 0;

		foreach ($item as $k => $v) {
			if (substr($k, 0, 1) == '_') {
				$total++;
			}
		}

		return $total;
	}

}