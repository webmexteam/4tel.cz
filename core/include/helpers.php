<?php

defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */
function notEmpty($val)
{
	return ($val !== '' && $val !== null);
}

function isBucket($obj)
{
	return (is_object($obj) && $obj instanceof WebmexAPI_Bucket);
}

function prepare_data($table, $data, $toggle = true)
{
	$d = array();

	foreach (Core::$db_inst->tableFields($table) as $name => $attr) {
		if (array_key_exists($name, $data)) {
			if (!$attr['notnull'] && ($data[$name] === '' || $data[$name] === null)) {
				$d[$name] = null;
			} else if (preg_match('/int(eger)?/i', $attr['type']) && ($attr['notnull'] || $data[$name] !== null)) {
				$d[$name] = (int) (string) $data[$name];
			} else if ($attr['type'] == 'float') {
				$d[$name] = parseFloat($data[$name]);
			} else {
				$d[$name] = $data[$name];
			}
		} else if ($toggle && preg_match('/int(eger)?/i', $attr['type']) && $attr['length'] == 1) {
			$d[$name] = 0;
		}
	}
	return $d;
}

function tpl($path, $data = array(), $template = null)
{
	return new View($path, $data, $template);
}

function __($name)
{
	$args = func_get_args();

	return new LangString($name, $args);
}

function url($url, $params = array(), $incl_http = false, $all_params = false)
{
	if ($all_params) {
		$get = (array) $_GET;

		$params = array_merge($get, $params);
	}

	unset($params['uri']);

	if ($url === true) {
		$url = Core::$orig_uri;
	} else if (is_object($url)) {
		return url($url->model->getURL(), $params, $incl_http);
	} else if (is_numeric($url)) {
		// page
		return url(Core::$db->page[$url], $params, $incl_http);
	}

	if ($url == Core::$orig_uri && Core::$is_admin && $all_params !== null) {
		if (!empty($_GET['sort']) && !isSet($params['sort'])) {
			$params['sort'] = $_GET['sort'];
		}

		if (!empty($_GET['dir']) && !isSet($params['dir'])) {
			$params['dir'] = $_GET['dir'];
		}

		if (!empty($_GET['page']) && !isSet($params['page'])) {
			$params['page'] = $_GET['page'];
		}

		if (!empty($_GET['search']) && !isSet($params['search'])) {
			$params['search'] = $_GET['search'];
		}

		if (isSet($_GET['search']) && !empty($_GET['where']) && !isSet($params['where'])) {
			$params['where'] = $_GET['where'];
		}

		if (isSet($_GET['search']) && !empty($_GET['category']) && !isSet($params['category'])) {
			$params['category'] = $_GET['category'];
		}
	}

	if (!empty($_GET['preview']) && !isSet($params['preview'])) {
		$params['preview'] = $_GET['preview'];
	}

	if ($url == Core::$orig_uri && !empty($_GET['q']) && !isSet($params['q'])) {
		$params['q'] = $_GET['q'];
	}

	$base = ($incl_http ? Core::$url : Core::$base);
	$use_htaccess = Core::$use_htaccess || (int) Core::config('url_rewrite');

	Event::run('helpers::url', $url, $params, $base, $use_htaccess);

	return $base
			. ($use_htaccess ? '' : 'index.php' . (Core::$fix_path ? '?uri=' : '') . '/')
			. $url
			. (!empty($params) ? ((!Core::$use_htaccess && Core::$fix_path) ? '&' : '?') . http_build_query($params === true ? $_GET : $params, '', '&') : '');
}

function parseFloat($num)
{
	return (float) preg_replace(array(
				'/\s/',
				'/\,(\d{1,2})$/',
				'/\,/'
					), array(
				'',
				'.$1',
				''
					), $num);
}

function fdate($int, $time = false)
{
	return date($time ? Core::$locale['datetime'] : Core::$locale['date'], (int) $int);
}

function save($old_price, $new_price, $percents = FALSE) {
	if($percents) {
		return round(((float) $old_price - (float) $new_price) / (float) $old_price * 100);
	}else{
		return (float) ((float) $old_price - (float) $new_price);
	}
}

function fprice($num, $format = null)
{
	$num = (float) $num;

	if (!$format) {
		$format = Core::config('price_format');
	}

	return call_user_func_array('number_format', array_merge(array((float) $num), Core::$def['price_formats'][$format]));
}

function fhours($seconds)
{
	$hours = floor($seconds / 3600);
	$minutes = floor(($seconds - ($hours * 3600)) / 60);
	$seconds = floor($seconds - ($hours * 3600) - ($minutes * 60));

	if ($hours < 10) {
		$hours = '0' . $hours;
	}

	if ($minutes < 10) {
		$minutes = '0' . $minutes;
	}

	if ($seconds < 10) {
		$seconds = '0' . $seconds;
	}

	return $hours . ':' . $minutes . ':' . $seconds;
}

// VAT functions, added 2011-01-04
function price($num, $currency = null)
{
	return new Price_Vat(round((float) $num, 2), $currency);
}

function price_vat($num, $vat = null, $currency = null)
{
	if (is_array($num)) {
		$vat = $num[0]['vat'];
		$num = $num[1];
	}

	if (is_object($num) && ($num instanceof Db_Row || $num instanceof Odb_Row)) {
		$vat = $num['vat'];
		$num = $num['price'];
	}

	$price = price($num, $currency);

	if ($vat && (int) Core::config('vat_payer') && Core::config('vat_mode') == 'exclude') {
		$price->price = round($price->price * ($vat / 100 + 1), 2);
	}

	$price->price = round($price->price, Core::config('price_vat_round') == -1 ? 2 : (int) Core::config('price_vat_round'));

	return $price;
}

function price_unvat($num, $vat = null, $currency = null)
{
	if (is_array($num)) {
		$vat = $num[0]['vat'];
		$num = $num[1];
	}

	if (is_object($num) && ($num instanceof Db_Row || $num instanceof Odb_Row)) {
		$vat = $num['vat'];
		$num = $num['price'];
	}

	$price = (float) $num;


	if ($vat && (int) Core::config('vat_payer') && Core::config('vat_mode') == 'include') {
        $price = $price / (($vat + 100) / 100.0);
		// err = $price = $price - ((round($vat / (100 + $vat), 4)) * $price);
	}

        return new Price_Vat($price, $currency);
}

class Price_Vat
{

	public $price, $currency;

	function __construct($price, $currency = null)
	{
		$this->price = (float) $price;
		$this->currency = $currency ? $currency : Core::config('currency');
	}

	function __toString()
	{
		return fprice($this->price) . '&nbsp;' . $this->currency;
	}

}

/////////

function estPrice($str, $total = null)
{
	if ($total !== null && preg_match('/^([\d\-\+\.\,]+)\%$/', $str, $matches)) {
		return parseFloat($matches[1]) / 100 * $total;
	}

	return parseFloat($str);
}

function redirect($url, $method = '302')
{
	$codes = array(
		'refresh' => 'Refresh',
		'300' => 'Multiple Choices',
		'301' => 'Moved Permanently',
		'302' => 'Found',
		'303' => 'See Other',
		'304' => 'Not Modified',
		'305' => 'Use Proxy',
		'307' => 'Temporary Redirect'
	);

	if ($url === true) {
		$url = !empty($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '';
	}

	if (strpos($url, '://') === false) {
		// HTTP headers expect absolute URLs
		$url = url($url);
	}

	Core::$remove_flash = false;

	header('HTTP/1.1 ' . $method . ' ' . $codes[$method]);
	header('Location: ' . $url);

	exit;
}

function mailerConnect()
{
	if (!class_exists('PHPMailer', false)) {
		require_once APPROOT . 'vendor/phpmailer/class.phpmailer.php';
	}

	$cfg = Core::$def['email'];

	$mailer = new PHPMailer();
	$mailer->CharSet = $cfg['charset'];

	if ($cfg['smtp_server']) {
		$mailer->Mailer = 'smtp';
		$mailer->Host = $cfg['smtp_server'];
		$mailer->Port = $cfg['smtp_port'];

		if ($cfg['smtp_user']) {
			$mailer->set('SMTPAuth', true);
			$mailer->Username = $cfg['smtp_user'];
			$mailer->Password = $cfg['smtp_password'];
		}
	}

	return $mailer;
}

function sendmail($to, $email_message_key, $replacements = array(), $reply_to = null)
{
	if (is_string($email_message_key)) {
		$email = Core::$db->email_message()->where('`key`', $email_message_key)->fetch();

		$subject = $email['subject'];
		$text = trim($email['text']);
	} else if (is_array($email_message_key)) {
		$subject = $email_message_key[0];
		$text = trim($email_message_key[1]);
	}

	if ($text) {
		$mailer = mailerConnect();
		$mailer->From = Core::config('email_sender');
		$mailer->FromName = Core::config('store_name');

		$toEmails = preg_split('/,|;\s*/', $to);
		foreach ($toEmails as $toEmail) {
			$mailer->AddAddress($toEmail);
		}

		if ($reply_to) {
			$mailer->AddReplyTo($reply_to);
		}

		$replacements['store_name'] = Core::config('store_name');
		$replacements['store_url'] = url(null, null, true);
		$replacements['admin_url'] = url('admin', null, true);

		foreach ($replacements as $n => $v) {
			$subject = str_replace('{' . $n . '}', $v, $subject);
			$text = str_replace('{' . $n . '}', $v, $text);
		}

		$mailer->Subject = $subject;
		$mailer->Body = $text;

		$mailer->send();
	}
}

function random($len = 6)
{
	return substr(str_repeat(sha1(uniqid()), $len), 0, $len);
}

// Front-end helpers

function banner($id, $params = array())
{
	$banner = Core::$db->banner[(int) $id];
	if(!$banner || !$banner['status'])
		return false;

	if(file_exists(APPROOT . 'template/' . Core::config('template') . '/banners/banner_' . $banner['id'] .'.php')) {
		$tpl_name = 'banners/banner_' . $banner['id'] .'.php';
	}else{
		$tpl_name = 'banners/banner_default.php';
	}

	return tpl($tpl_name, array('banner' => $banner, 'params' => $params));
}

function search_form()
{
	return tpl('search_form.php');
}

function productImg($path, $size, $number)
{
	$images = array();
	
	$result = Core::$db->product_files()->where('product_id', $path['id'])->order('position ASC, id ASC');
	
	foreach ($result as $img){
		$images[] = $img; 
	}
	
	if (!empty($images[$number])){
		$img = $images[$number];
	}else{
		return null;
	}
	
	if ($img) {
		$filename = $img['filename'];
		$description = $img['description'];

		if ($size === null) {
			$size = $img['size'];
		}
	}
	

	$pathinfo = pathinfo($filename);
	$sizepath = (int) $size > 0 ? '/_' . Core::$def['image_sizes'][$size] : '';

	return $filename ? Core::$base . $pathinfo['dirname'] . $sizepath . '/' . $pathinfo['basename'] : null;
}

function imgsrc($path, $size = null, $autosize = 2, & $description = null)
{
	$filename = null;

	if (is_object($path) && $path->table_name == 'page') {
		$img = Core::$db->page_files()->where('page_id', $path['id'])->order('position ASC')->limit(1)->fetch();

		if ($img) {
			$filename = $img['filename'];
			$size = $img['size'];
			$description = $img['description'];
		}
	} else if (is_object($path) && $path->table_name == 'product') {
		$img = Core::$db->product_files()->where('product_id', $path['id'])->order('position ASC')->limit(1)->fetch();

		if ($img) {
			$filename = $img['filename'];
			$description = $img['description'];

			if ($size === null) {
				$size = $img['size'];
			}
		}
	} else if (is_object($path) && !empty($path['filename'])) {
		$filename = $path['filename'];
		$description = $img['description'];

		if ($size === null) {
			$size = $path['size'];
		}
	}

	if ((int) $size === -1) {
		$size = $autosize;
	}

	$pathinfo = pathinfo($filename);
	$sizepath = (int) $size > 0 ? '/_' . Core::$def['image_sizes'][$size] : '';

	return $filename ? Core::$base . $pathinfo['dirname'] . $sizepath . '/' . $pathinfo['basename'] : null;
}

/**
 * Obrazek pro variantu produktu
 */
function imgsrcvariant($file_id, $path, $size = null, $autosize = 2, & $description = null)
{
	$filename = null;

	if (is_object($path) && $path->table_name == 'product') {
		$img = Core::$db->product_files()->where('id', $file_id)->order('position ASC')->limit(1)->fetch();

		if ($img) {
			$filename = $img['filename'];
			$description = $img['description'];

			if ($size === null) {
				$size = $img['size'];
			}
		}else{
			return imgsrcvariant($path, $size, $autosize, $description);
		}
	}

	if ((int) $size === -1) {
		$size = $autosize;
	}

	$pathinfo = pathinfo($filename);
	$sizepath = (int) $size > 0 ? '/_' . Core::$def['image_sizes'][$size] : '';

	return $filename ? Core::$base . $pathinfo['dirname'] . $sizepath . '/' . $pathinfo['basename'] : null;
}

function existsFiles($obj) {
	if (is_object($obj) && $obj->table_name == 'page') {
		$files = Core::$db->page_files()->where('page_id', $obj['id'])->order('position ASC');
		return ($files->count('*') > 0);

	} else if (is_object($obj) && $obj->table_name == 'product') {
		$files = Core::$db->product_files()->where('product_id', $obj['id'])->order('position ASC');
		return ($files->count('*') > 0);
	}

	return FALSE;
}

function filename($obj)
{
	if(isset($obj['description']) && $obj['description'] != '')
		return $obj['description'];

	if(!isset($obj['filename']))
		return null;

	$array = explode('/', $obj['filename']);
	$filename = $array[count($array)-1];
	return $filename;
}

function setMeta($obj)
{
	if (is_object($obj) && isSet($obj['title'])) {
		if ($title = $obj['title'] ? $obj['title'] : $obj['name']) {
			View::$meta['title'] = $title;
		}

		if ($obj['meta_keywords']) {
			View::$meta['keywords'] = $obj['meta_keywords'];
		}

		if ($obj['meta_description']) {
			View::$meta['description'] = $obj['meta_description'];
		}
	} else if (is_array($obj)) {
		if ($obj['title']) {
			View::$meta['title'] = $obj['title'];
		}

		if ($obj['keywords']) {
			View::$meta['keywords'] = $obj['keywords'];
		}

		if ($obj['description']) {
			View::$meta['description'] = $obj['description'];
		}
	}
}

function pageFullPath($page, $links = false, $sep = ' &rsaquo; ')
{
	$path = $links ? '<a href="' . url($page) . '">' . $page['name'] . '</a>' : $page['name'];

	while ((int) $page['parent_page']) {
		$parent = Core::$db->page[(int) $page['parent_page']];

		if ($parent) {
			$path = ($links ? '<a href="' . url($parent) . '">' . $parent['name'] . '</a>' : $parent['name']) . $sep . $path;
			$page = $parent;
		} else {
			return null;
		}
	}

	return $path;
}

function valid_email($email)
{
	return (bool) preg_match('/^[-_a-z0-9\'+*$^&%=~!?{}]++(?:\.[-_a-z0-9\'+*$^&%=~!?{}]+)*+@(?:(?![-.])[-a-z0-9.]+(?<![-.])\.[a-z]{2,6}|\d{1,3}(?:\.\d{1,3}){3})(?::\d++)?$/iD', (string) $email);
}

function pgLimit()
{
	$l = Core::$is_admin ? (int) Core::config('limit_records_admin') : (int) Core::config('limit_records');

	return $l ? $l : 10;
}

function pgOffset($limit = null)
{
	return ((int) Core::$controller->pagination_page - 1) * ($limit ? $limit : pgLimit());
}

function pagination_old($count, $page = null, $limit = null)
{
	if (!$limit) {
		$limit = pgLimit();
	}

	$params = $_GET;

	if ($page === null) {
		$page = Core::$controller->pagination_page;
	}

	$out = '<div class="pagination"><ul><li>' . __('pages') . ': </li>';

	$total = ceil($count / $limit);

	for ($i = 1; $i <= $total; $i++) {
		if (abs($i - $page) > 5 && $i != $total && $i != 1) {
			if ($i == 2) {
				$out .= '<li>...</li>';
			}
			if ($i == $total - 1) {
				$out .= '<li>...</li>';
			}
			continue;
		}

		if ($page == $i) {
			$out .= '<li class="active"><span>' . $i . '</span></li>';
		} else {
			$out .= '<li' . ($page == $i ? ' class="active"' : '') . '><a href="' . url(true, array_merge($params, array('page' => $i))) . '">' . $i . '</a></li>';
		}
	}

	$out .= '</ul></div>';

	return $out;
}

function dirify($s, $sep = '-', $is_file = false)
{
	$s = xliterate_utf8($s);
	$s = strtolower($s);
	$s = strip_tags($s);
	$s = preg_replace('/&[^;\s]+;/', '', $s);

	$s = preg_replace('/[\/\*\;\,]/', $sep, $s);

	if ($is_file) {
		$s = preg_replace('/[^\w\s\.\_\-]/', '', $s);
	} else {
		$s = preg_replace('/[^\w\s]/', '', $s);
	}

	$s = preg_replace('/\s+/', $sep, $s);
	$s = preg_replace('/[\`\´\'\"]/', '', $s);
	return(trim($s, ' -'));
}

// Thanks to Movable Type team
function xliterate_utf8($s)
{
	$utf8_ASCII = array(
		"\xc3\x80" => 'A', # A`
		"\xc3\xa0" => 'a', # a`
		"\xc3\x81" => 'A', # A'
		"\xc3\xa1" => 'a', # a'
		"\xc3\x82" => 'A', # A^
		"\xc3\xa2" => 'a', # a^
		"\xc4\x82" => 'A', # latin capital letter a with breve
		"\xc4\x83" => 'a', # latin small letter a with breve
		"\xc3\x86" => 'AE', # latin capital letter AE
		"\xc3\xa6" => 'ae', # latin small letter ae
		"\xc3\x85" => 'A', # latin capital letter a with ring above
		"\xc3\xa5" => 'a', # latin small letter a with ring above
		"\xc4\x80" => 'A', # latin capital letter a with macron
		"\xc4\x81" => 'a', # latin small letter a with macron
		"\xc4\x84" => 'A', # latin capital letter a with ogonek
		"\xc4\x85" => 'a', # latin small letter a with ogonek
		"\xc3\x84" => 'A', # A:
		"\xc3\xa4" => 'a', # a:
		"\xc3\x83" => 'A', # A~
		"\xc3\xa3" => 'a', # a~
		"\xc3\x88" => 'E', # E`
		"\xc3\xa8" => 'e', # e`
		"\xc3\x89" => 'E', # E'
		"\xc3\xa9" => 'e', # e'
		"\xc3\x8a" => 'E', # E^
		"\xc3\xaa" => 'e', # e^
		"\xc3\x8b" => 'E', # E:
		"\xc3\xab" => 'e', # e:
		"\xc4\x92" => 'E', # latin capital letter e with macron
		"\xc4\x93" => 'e', # latin small letter e with macron
		"\xc4\x98" => 'E', # latin capital letter e with ogonek
		"\xc4\x99" => 'e', # latin small letter e with ogonek
		"\xc4\x9a" => 'E', # latin capital letter e with caron
		"\xc4\x9b" => 'e', # latin small letter e with caron
		"\xc4\x94" => 'E', # latin capital letter e with breve
		"\xc4\x95" => 'e', # latin small letter e with breve
		"\xc4\x96" => 'E', # latin capital letter e with dot above
		"\xc4\x97" => 'e', # latin small letter e with dot above
		"\xc3\x8c" => 'I', # I`
		"\xc3\xac" => 'i', # i`
		"\xc3\x8d" => 'I', # I'
		"\xc3\xad" => 'i', # i'
		"\xc3\x8e" => 'I', # I^
		"\xc3\xae" => 'i', # i^
		"\xc3\x8f" => 'I', # I:
		"\xc3\xaf" => 'i', # i:
		"\xc4\xaa" => 'I', # latin capital letter i with macron
		"\xc4\xab" => 'i', # latin small letter i with macron
		"\xc4\xa8" => 'I', # latin capital letter i with tilde
		"\xc4\xa9" => 'i', # latin small letter i with tilde
		"\xc4\xac" => 'I', # latin capital letter i with breve
		"\xc4\xad" => 'i', # latin small letter i with breve
		"\xc4\xae" => 'I', # latin capital letter i with ogonek
		"\xc4\xaf" => 'i', # latin small letter i with ogonek
		"\xc4\xb0" => 'I', # latin capital letter with dot above
		"\xc4\xb1" => 'i', # latin small letter dotless i
		"\xc4\xb2" => 'IJ', # latin capital ligature ij
		"\xc4\xb3" => 'ij', # latin small ligature ij
		"\xc4\xb4" => 'J', # latin capital letter j with circumflex
		"\xc4\xb5" => 'j', # latin small letter j with circumflex
		"\xc4\xb6" => 'K', # latin capital letter k with cedilla
		"\xc4\xb7" => 'k', # latin small letter k with cedilla
		"\xc4\xb8" => 'k', # latin small letter kra
		"\xc5\x81" => 'L', # latin capital letter l with stroke
		"\xc5\x82" => 'l', # latin small letter l with stroke
		"\xc4\xbd" => 'L', # latin capital letter l with caron
		"\xc4\xbe" => 'l', # latin small letter l with caron
		"\xc4\xb9" => 'L', # latin capital letter l with acute
		"\xc4\xba" => 'l', # latin small letter l with acute
		"\xc4\xbb" => 'L', # latin capital letter l with cedilla
		"\xc4\xbc" => 'l', # latin small letter l with cedilla
		"\xc4\xbf" => 'l', # latin capital letter l with middle dot
		"\xc5\x80" => 'l', # latin small letter l with middle dot
		"\xc3\x92" => 'O', # O`
		"\xc3\xb2" => 'o', # o`
		"\xc3\x93" => 'O', # O'
		"\xc3\xb3" => 'o', # o'
		"\xc3\x94" => 'O', # O^
		"\xc3\xb4" => 'o', # o^
		"\xc3\x96" => 'O', # O:
		"\xc3\xb6" => 'o', # o:
		"\xc3\x95" => 'O', # O~
		"\xc3\xb5" => 'o', # o~
		"\xc3\x98" => 'O', # O/
		"\xc3\xb8" => 'o', # o/
		"\xc5\x8c" => 'O', # latin capital letter o with macron
		"\xc5\x8d" => 'o', # latin small letter o with macron
		"\xc5\x90" => 'O', # latin capital letter o with double acute
		"\xc5\x91" => 'o', # latin small letter o with double acute
		"\xc5\x8e" => 'O', # latin capital letter o with breve
		"\xc5\x8f" => 'o', # latin small letter o with breve
		"\xc5\x92" => 'OE', # latin capital ligature oe
		"\xc5\x93" => 'oe', # latin small ligature oe
		"\xc5\x94" => 'R', # latin capital letter r with acute
		"\xc5\x95" => 'r', # latin small letter r with acute
		"\xc5\x98" => 'R', # latin capital letter r with caron
		"\xc5\x99" => 'r', # latin small letter r with caron
		"\xc5\x96" => 'R', # latin capital letter r with cedilla
		"\xc5\x97" => 'r', # latin small letter r with cedilla
		"\xc3\x99" => 'U', # U`
		"\xc3\xb9" => 'u', # u`
		"\xc3\x9a" => 'U', # U'
		"\xc3\xba" => 'u', # u'
		"\xc3\x9b" => 'U', # U^
		"\xc3\xbb" => 'u', # u^
		"\xc3\x9c" => 'U', # U:
		"\xc3\xbc" => 'u', # u:
		"\xc5\xaa" => 'U', # latin capital letter u with macron
		"\xc5\xab" => 'u', # latin small letter u with macron
		"\xc5\xae" => 'U', # latin capital letter u with ring above
		"\xc5\xaf" => 'u', # latin small letter u with ring above
		"\xc5\xb0" => 'U', # latin capital letter u with double acute
		"\xc5\xb1" => 'u', # latin small letter u with double acute
		"\xc5\xac" => 'U', # latin capital letter u with breve
		"\xc5\xad" => 'u', # latin small letter u with breve
		"\xc5\xa8" => 'U', # latin capital letter u with tilde
		"\xc5\xa9" => 'u', # latin small letter u with tilde
		"\xc5\xb2" => 'U', # latin capital letter u with ogonek
		"\xc5\xb3" => 'u', # latin small letter u with ogonek
		"\xc3\x87" => 'C', # ,C
		"\xc3\xa7" => 'c', # ,c
		"\xc4\x86" => 'C', # latin capital letter c with acute
		"\xc4\x87" => 'c', # latin small letter c with acute
		"\xc4\x8c" => 'C', # latin capital letter c with caron
		"\xc4\x8d" => 'c', # latin small letter c with caron
		"\xc4\x88" => 'C', # latin capital letter c with circumflex
		"\xc4\x89" => 'c', # latin small letter c with circumflex
		"\xc4\x8a" => 'C', # latin capital letter c with dot above
		"\xc4\x8b" => 'c', # latin small letter c with dot above
		"\xc4\x8e" => 'D', # latin capital letter d with caron
		"\xc4\x8f" => 'd', # latin small letter d with caron
		"\xc4\x90" => 'D', # latin capital letter d with stroke
		"\xc4\x91" => 'd', # latin small letter d with stroke
		"\xc3\x91" => 'N', # N~
		"\xc3\xb1" => 'n', # n~
		"\xc5\x83" => 'N', # latin capital letter n with acute
		"\xc5\x84" => 'n', # latin small letter n with acute
		"\xc5\x87" => 'N', # latin capital letter n with caron
		"\xc5\x88" => 'n', # latin small letter n with caron
		"\xc5\x85" => 'N', # latin capital letter n with cedilla
		"\xc5\x86" => 'n', # latin small letter n with cedilla
		"\xc5\x89" => 'n', # latin small letter n preceded by apostrophe
		"\xc5\x8a" => 'N', # latin capital letter eng
		"\xc5\x8b" => 'n', # latin small letter eng
		"\xc3\x9f" => 'ss', # double-s
		"\xc5\x9a" => 'S', # latin capital letter s with acute
		"\xc5\x9b" => 's', # latin small letter s with acute
		"\xc5\xa0" => 'S', # latin capital letter s with caron
		"\xc5\xa1" => 's', # latin small letter s with caron
		"\xc5\x9e" => 'S', # latin capital letter s with cedilla
		"\xc5\x9f" => 's', # latin small letter s with cedilla
		"\xc5\x9c" => 'S', # latin capital letter s with circumflex
		"\xc5\x9d" => 's', # latin small letter s with circumflex
		"\xc8\x98" => 'S', # latin capital letter s with comma below
		"\xc8\x99" => 's', # latin small letter s with comma below
		"\xc5\xa4" => 'T', # latin capital letter t with caron
		"\xc5\xa5" => 't', # latin small letter t with caron
		"\xc5\xa2" => 'T', # latin capital letter t with cedilla
		"\xc5\xa3" => 't', # latin small letter t with cedilla
		"\xc5\xa6" => 'T', # latin capital letter t with stroke
		"\xc5\xa7" => 't', # latin small letter t with stroke
		"\xc8\x9a" => 'T', # latin capital letter t with comma below
		"\xc8\x9b" => 't', # latin small letter t with comma below
		"\xc6\x92" => 'f', # latin small letter f with hook
		"\xc4\x9c" => 'G', # latin capital letter g with circumflex
		"\xc4\x9d" => 'g', # latin small letter g with circumflex
		"\xc4\x9e" => 'G', # latin capital letter g with breve
		"\xc4\x9f" => 'g', # latin small letter g with breve
		"\xc4\xa0" => 'G', # latin capital letter g with dot above
		"\xc4\xa1" => 'g', # latin small letter g with dot above
		"\xc4\xa2" => 'G', # latin capital letter g with cedilla
		"\xc4\xa3" => 'g', # latin small letter g with cedilla
		"\xc4\xa4" => 'H', # latin capital letter h with circumflex
		"\xc4\xa5" => 'h', # latin small letter h with circumflex
		"\xc4\xa6" => 'H', # latin capital letter h with stroke
		"\xc4\xa7" => 'h', # latin small letter h with stroke
		"\xc5\xb4" => 'W', # latin capital letter w with circumflex
		"\xc5\xb5" => 'w', # latin small letter w with circumflex
		"\xc3\x9d" => 'Y', # latin capital letter y with acute
		"\xc3\xbd" => 'y', # latin small letter y with acute
		"\xc5\xb8" => 'Y', # latin capital letter y with diaeresis
		"\xc3\xbf" => 'y', # latin small letter y with diaeresis
		"\xc5\xb6" => 'Y', # latin capital letter y with circumflex
		"\xc5\xb7" => 'y', # latin small letter y with circumflex
		"\xc5\xbd" => 'Z', # latin capital letter z with caron
		"\xc5\xbe" => 'z', # latin small letter z with caron
		"\xc5\xbb" => 'Z', # latin capital letter z with dot above
		"\xc5\xbc" => 'z', # latin small letter z with dot above
		"\xc5\xb9" => 'Z', # latin capital letter z with acute
		"\xc5\xba" => 'z', # latin small letter z with acute
	);
	return strtr($s, $utf8_ASCII);
}

if (!function_exists('array_replace')) {

	function array_replace(array &$array, array &$array1)
	{
		$args = func_get_args();
		$count = func_num_args();

		for ($i = 0; $i < $count; ++$i) {
			if (is_array($args[$i])) {
				foreach ($args[$i] as $key => $val) {
					$array[$key] = $val;
				}
			} else {
				trigger_error(
						__FUNCTION__ . '(): Argument #' . ($i + 1) . ' is not an array', E_USER_WARNING
				);
				return NULL;
			}
		}

		return $array;
	}

}

function remoteCopy($source, $destination)
{
	$fp_source = fopen($source, 'r');
	$fp_destination = fopen($destination, 'w');

	if ($fp_source && $fp_destination) {
		while ($data = fread($fp_source, 4096)) {
			fwrite($fp_destination, $data);
		}

		fclose($fp_source);
		fclose($fp_destination);

		return true;
	}

	return false;
}

if (!function_exists('http_request')) {

	function http_request($url, $method = 'GET', $data = array(), $cookies = array())
	{
		$response = array('', '');
		$str = http_build_query($data);

		$url_info = parse_url($url);

		$fp = fsockopen($url_info['host'], isSet($url_info['port']) ? $url_info['port'] : 80, $errno, $errstr, 30);

		if ($fp) {
			$out = $method . " " . (isSet($url_info['path']) ? $url_info['path'] . (isSet($url_info['query']) ? '?' . $url_info['query'] : '') : '/') . " HTTP/1.1\r\n";
			$out .= "Host: " . $url_info['host'] . "\r\n";
			$out .= "Content-type: application/x-www-form-urlencoded\r\n";

			foreach ($cookies as $name => $cookie) {
				$out .= "Cookie: " . $name . "=" . $cookie . "\r\n";
			}

			$out .= "Content-length: " . strlen($str) . "\r\n";
			$out .= "Connection: Close\r\n\r\n" . $str;

			$header = true;
			fwrite($fp, $out);
			while (!feof($fp)) {
				$tmp = fgets($fp, 1024);

				if ($header) {
					$response[0] .= $tmp;
				} else {
					$response[1] .= $tmp;
				}

				if ($tmp == "\r\n") {
					$header = false;
				}
			}
			fclose($fp);
		}

		return $response;
	}

}

function getStyleId($mtime = true)
{
	$hash = md5(Core::config('template') . Core::config('template_style') . Core::config('template_theme') . Core::version . Core::config('last_cache_clean'));

	if ($mtime) {
		$filemtime = @filemtime(DOCROOT . 'etc/tmp/' . $hash . '.css');
	}

	return $hash . ($mtime ? $filemtime : '');
}

class LangString
{

	public $name;
	public $args;

	public function __construct($name, $args)
	{
		$this->name = $name;
		$this->args = $args;
	}

	public function __toString()
	{
		if (isset(Core::$lang[$this->name])) {
			if ($args = array_slice($this->args, 1)) {
				return vsprintf(Core::$lang[$this->name], $args);
			}

			return Core::$lang[$this->name];
		}

		return '__' . $this->name;
	}

}

function limit_words($str, $limit, $end_char = null)
{
	$limit = (int) $limit;
	$end_char = ($end_char === null) ? '&#8230;' : $end_char;

	if (trim($str) === '')
		return $str;

	if ($limit <= 0)
		return $end_char;

	preg_match('/^\s*+(?:\S++\s*+){1,' . $limit . '}/u', $str, $matches);

	return rtrim($matches[0]) . (strlen($matches[0]) === strlen($str) ? '' : $end_char);
}

function xmlentities($str)
{
	$xml = array('&#34;', '&#38;', '&#38;', '&#60;', '&#62;', '&#160;', '&#161;', '&#162;', '&#163;', '&#164;', '&#165;', '&#166;', '&#167;', '&#168;', '&#169;', '&#170;', '&#171;', '&#172;', '&#173;', '&#174;', '&#175;', '&#176;', '&#177;', '&#178;', '&#179;', '&#180;', '&#181;', '&#182;', '&#183;', '&#184;', '&#185;', '&#186;', '&#187;', '&#188;', '&#189;', '&#190;', '&#191;', '&#192;', '&#193;', '&#194;', '&#195;', '&#196;', '&#197;', '&#198;', '&#199;', '&#200;', '&#201;', '&#202;', '&#203;', '&#204;', '&#205;', '&#206;', '&#207;', '&#208;', '&#209;', '&#210;', '&#211;', '&#212;', '&#213;', '&#214;', '&#215;', '&#216;', '&#217;', '&#218;', '&#219;', '&#220;', '&#221;', '&#222;', '&#223;', '&#224;', '&#225;', '&#226;', '&#227;', '&#228;', '&#229;', '&#230;', '&#231;', '&#232;', '&#233;', '&#234;', '&#235;', '&#236;', '&#237;', '&#238;', '&#239;', '&#240;', '&#241;', '&#242;', '&#243;', '&#244;', '&#245;', '&#246;', '&#247;', '&#248;', '&#249;', '&#250;', '&#251;', '&#252;', '&#253;', '&#254;', '&#255;');

	$html = array('&quot;', '&amp;', '&amp;', '&lt;', '&gt;', '&nbsp;', '&iexcl;', '&cent;', '&pound;', '&curren;', '&yen;', '&brvbar;', '&sect;', '&uml;', '&copy;', '&ordf;', '&laquo;', '&not;', '&shy;', '&reg;', '&macr;', '&deg;', '&plusmn;', '&sup2;', '&sup3;', '&acute;', '&micro;', '&para;', '&middot;', '&cedil;', '&sup1;', '&ordm;', '&raquo;', '&frac14;', '&frac12;', '&frac34;', '&iquest;', '&Agrave;', '&Aacute;', '&Acirc;', '&Atilde;', '&Auml;', '&Aring;', '&AElig;', '&Ccedil;', '&Egrave;', '&Eacute;', '&Ecirc;', '&Euml;', '&Igrave;', '&Iacute;', '&Icirc;', '&Iuml;', '&ETH;', '&Ntilde;', '&Ograve;', '&Oacute;', '&Ocirc;', '&Otilde;', '&Ouml;', '&times;', '&Oslash;', '&Ugrave;', '&Uacute;', '&Ucirc;', '&Uuml;', '&Yacute;', '&THORN;', '&szlig;', '&agrave;', '&aacute;', '&acirc;', '&atilde;', '&auml;', '&aring;', '&aelig;', '&ccedil;', '&egrave;', '&eacute;', '&ecirc;', '&euml;', '&igrave;', '&iacute;', '&icirc;', '&iuml;', '&eth;', '&ntilde;', '&ograve;', '&oacute;', '&ocirc;', '&otilde;', '&ouml;', '&divide;', '&oslash;', '&ugrave;', '&uacute;', '&ucirc;', '&uuml;', '&yacute;', '&thorn;', '&yuml;');

	$str = preg_replace('/\&(?!\#?\w+\;)/', '&amp;', $str);

	$str = str_replace($html, $xml, $str);
	$str = str_ireplace($html, $xml, $str);

	return $str;
}

function stock_changes($order, $stock_increasing) {
	foreach($order->order_products() as $product){
		if($product['product_id'] && $product->product['stock'] !== null && $product->product['stock'] !== ''){
			if ($stock_increasing == true) {
				$new_qty = (int) $product->product['stock'] + (int) $product['quantity'];
			} else {
				$new_qty = (int) $product->product['stock'] - (int) $product['quantity'];
			}
			$product->product->update(array('stock' => $new_qty));
		}
	}
}

function heureka_conversion_rate_script($order) {

	$products = "";
	$jsAddProduct = "_hrq.push(['addProduct', '%s', '%s', '%s']);";

	# 3 variables in "%s"
	# 1. Heureka key
	# 2. Order ID
	# 3. Calling of push() for each product
	$js = Core::config("conversion_rate_script");

	foreach ($order->order_products()as $p)
		if ($p["product_id"])
			$products .= sprintf($jsAddProduct, $p["name"], html_entity_decode(price_vat($p["price"], $p["vat"], $p["currency"])), $p["quantity"]);

	return sprintf($js, Core::config("heureka_conversions"), $order["id"], $products);

}


function barDump($value, $name)
{
	return Tracy\Debugger::barDump($value, $name);
}



function getProductAvailability($product, $attribute = NULL)
{
	$availability = NULL;
	$stock = NULL;

	// Product availability
	if (!$availability) {
		$availability = $product->availability?: NULL;
		$stock = $product['stock'];
	}

	// Attribute availability?
	if ($attribute) {
		if (is_numeric($attribute)) {
			$attribute = $product->product_attributes()
				->where('id', $attribute)
				->fetch();
		}

		if ($attribute['availability_id']) {
			$availability = $attribute->availability;
		}
		$stock = $availability['stock'];
	}

	// Out fo stock?
	if (($stock === '0') || ($stock === 0)) {
		if ($product['stock_out_availability']) {
			$availability = Core::$db->availability[ $product['stock_out_availability'] ];

		} elseif ($stockOutAvailabilityId = Core::config('stock_out_availability')) {
			$availability = Core::$db->availability[ $stockOutAvailabilityId ];
		}
	}

	return $availability;
}



function getProductManufacturer($product)
{
	foreach ($product->product_pages() as $pp) {
		$page = $pp->page;
		if ($page['menu'] == 3) {
			return $page;
		}
	}

	return NULL;
}


function stringDump($value)
{
	ob_start();
	var_dump($value);
	return ob_get_clean();
}



$GLOBALS['fileLogUsed'] = FALSE;
$GLOBALS['fileLogDepth'] = 0;
function fileLog($msg = '', $filename = 'main.log', $depthIncrement = 0)
{
	if ($depthIncrement < 0) {
		$GLOBALS['fileLogDepth'] += $depthIncrement;
	}

	$filepath = DOCROOT . '/etc/log/' . $filename;

	//~~~~~~ Format message
	// Base message
	$msg = '[' . date('Y-m-d H:i:s') . '] [' . $_SERVER['REQUEST_URI'] . "] [" . $_SERVER['REMOTE_ADDR'] . "] \n" . $msg . "\n";

	// Indentation
	$indent = str_repeat("\t", $GLOBALS['fileLogDepth']);
	$msg = $indent . $msg;
	$msg = str_replace("\n", "\n$indent", $msg);
	$msg .= "\n";

	// First entry prepend with blank lines
	if (!$GLOBALS['fileLogUsed']) {
		$msg = "\n\n#####  #####  #####  #####  #####  ##### \n\n\n\n" . $msg;

		$GLOBALS['fileLogUsed'] = TRUE;
	}
	//~~~~~~/

	if ($depthIncrement > 0) {
		$GLOBALS['fileLogDepth'] += $depthIncrement;
	}
	return file_put_contents($filepath, $msg, FILE_APPEND);
}


// TODO move to model entity
$productFeatureCache = array();
/**
 * @param int|object  product ID or product row
 * @param int|string  feature ID or NAME
 * @param int|NULL
 * @return mixed
 */
function productFeature($product, $feature, $featuresetId = NULL) {
	global $productFeatureCache;

	// 1) Prepare values
	// product id
	$productId = $product;
	if (!is_numeric($product)) {
		$productId = $product['id'];
	}

	// featureset id
	if (!$featuresetId && !$productFeatureCache['featuresetId']) {
		$featureset = Core::$db->featureset()->fetch();
		$featuresetId = $featureset['id'];

	} elseif ($productFeatureCache['featuresetId']) {
		$featuresetId = $productFeatureCache['featuresetId'];
	}
	$productFeatureCache['featuresetId'] = $featuresetId;

	$tableName = 'features_' . $featuresetId;

	// feature id
	$featureId = $feature;
	if (!is_numeric($featureId) && empty($productFeatureCache['feature'][$feature])) {
		$featureRow = Core::$db->feature()->where('name', $feature)->fetch();
		if (!$featureRow) return FALSE;

		$featureId = $featureRow['id'];

		$productFeatureCache['feature'][$feature] = $featureId;

	} elseif (!empty($productFeatureCache['feature'][$feature])) {
		$featureId = $productFeatureCache['feature'][$feature];
	}

	$columnName = 'f' . $featureId;


	// 2) Get feature value from DB
	$featureDataRow = Core::$db->$tableName()
		->where('product_id', $productId)
		->fetch();

	if (!$featureDataRow)
		return FALSE;

	if (!isset($featureDataRow[$columnName]))
		return FALSE;


	// 3) Return
	return $featureDataRow[$columnName];
}

/**
 * Load breadcrumb Ids
 *
 * @param type $id current page Id
 * @return array array of Ids
 */
function loadBreadcrumbIds($id) {

    $node = Core::$db->page()->where('id', $id)->fetch();

    if ($node) {
        if(!in_array($node['id'], $breadcrumbIds)) {
            $breadcrumbIds[] = $node['id'];
        }
        $breadcrumbIds = getNodeIdByParentId($node['parent_page'], $breadcrumbIds);
    }

    return $breadcrumbIds;
}

/**
 * Get Id By parent page Id.
 *
 * @param type $parentPage
 * @param type $breadcrumbIds
 * @return type
 */
function getNodeIdByParentId($parentPage, $breadcrumbIds) {

    if($parentPage > 0) {

        $node = Core::$db->page()->where('id', $parentPage)->fetch();

        if($node > 0) {
            if(!in_array($node['id'], $breadcrumbIds)) {
                $breadcrumbIds[] = $node['id'];
            }
            $breadcrumbIds = getNodeIdByParentId($node['parent_page'], $breadcrumbIds);
        }

    }

    return $breadcrumbIds;
}

function getFilesFromPath($path) {

    $files = array();

    if ($handle = opendir($path)) {

        while (false !== ($entry = readdir($handle))) {

            if ($entry != "." && $entry != ".." && is_file($path.$entry)) {
                $files[] = $path.$entry;
            }
        }

        closedir($handle);
    }

    return $files;
}

/**
 *  Vygeneruje pocet produtku a zakazniku pro aktualni den.
 *
 *  Vrati pocet produktu a zakazniku pro aktualni den
 */
function generateHomepageCounts() {
    $today = date("Y-m-d"); // Dnesni den

    $counts = array();

    // Zjisti jestli konfiguracni zaznam s nazvem homepage_counts
    $res = Core::$db->config()->where('name', 'homepage_counts')->fetch();

    // Kdyz existuje provedu kontrolu datumu a pripadne pregenereovani hodnot
    if($res) {
        $value = $res['value'];
        $values = explode(';', $value);

        // Kdyz dnesni datum nneni stejny jako ulozeny, tak provedu pregenrovani poctu
        if(isset($values[0]) && trim($values[0]) != $today) {

            // vrati pocty pro produkty a zakazniky
            $products = generateProductsCount();
            $customers = generateCustomersCount();

            // vytvori hodnotu
            $configValue = $today.';'.$products.';'.$customers;

            Core::$db->config()->where('name', 'homepage_counts')->update(array('value' => $configValue));

            $counts = getHomepageCounts($products, $customers);

        } else {

            $counts = getHomepageCounts($values[1], $values[2]);

        }

    } else {

        // vrati pocty pro produkty a zakazniky
        $products = generateProductsCount();
        $customers = generateCustomersCount();

        // vytvori hodnotu
        $configValue = $today.';'.$products.';'.$customers;

        Core::$db->config()->insert(array('name' => 'homepage_counts', 'value' => $configValue));

        $counts = getHomepageCounts($products, $customers);
    }

    return $counts;
}

/**
 * Vygeneruje pocet produktu.
 */
function generateProductsCount() {

    // Vygeneruje pocet produktu
    return rand(800, 850);
}

/**
 * Vygeneruje pocet zakazniku.
 */
function generateCustomersCount() {

    // Vygeneruje pocet zakazniku
    $month = (int) date("m");

    $customers = 40;

    if($month > 10) {
        $customers = rand(35, 60);
    } else {
        $customers = rand(20, 30);
    }

    return $customers;
}

/**
 * Vrati pole s poctem produktu a zakazniku
 */
function getHomepageCounts($products, $customers) {

    $counts = array('products' => 850, 'customers' => 30);

    if(isset($products)) {
        $counts['products'] = $products;
    }

    if(isset($customers)) {
        $counts['customers'] = $customers;
    }

    return $counts;
}
