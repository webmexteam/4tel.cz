<?php

defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */
class Pohoda_Builder_Datapack extends Pohoda_Builder
{

	const version = '1.0';
	const item_version = '1.0';

	public $root;

	public function __construct()
	{
		parent::__construct();

		$config = Pohoda::config();

		$this->root = $this->dom->createElementNS($this->schema['dat'], 'dat:dataPack');

		$this->dom->appendChild($this->root);

		$this->root->setAttributeNS('http://www.w3.org/2000/xmlns/', 'xmlns:dat', $this->schema['dat']);
		$this->root->setAttributeNS('http://www.w3.org/2000/xmlns/', 'xmlns:ord', $this->schema['ord']);
		$this->root->setAttributeNS('http://www.w3.org/2000/xmlns/', 'xmlns:typ', $this->schema['typ']);
		$this->root->setAttributeNS('http://www.w3.org/2000/xmlns/', 'xmlns:lst', $this->schema['lst']);
		$this->root->setAttributeNS('http://www.w3.org/2000/xmlns/', 'xmlns:adb', $this->schema['adb']);
		$this->root->setAttribute('version', self::version);
		$this->root->setAttribute('application', 'WEBMEX ' . Core::version);
		$this->root->setAttribute('id', uniqid());
		$this->root->setAttribute('ico', $config['ico']);
		$this->root->setAttribute('note', '');
	}

	public function addItem($id)
	{
		$el = $this->dom->createElementNS($this->schema['dat'], 'dat:dataPackItem');
		$el->setAttribute('id', $id);
		$el->setAttribute('version', self::item_version);

		$this->root->appendChild($el);

		return $el;
	}

}