<?php

defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */
class Pohoda_Builder_Order
{

	const version = '1.6';

	protected $data;
	protected $datapack;
	protected $order;
	protected $order_header;
	protected $order_detail;
	protected $vat_rate = 'high';

	public function __construct($datapack, $data)
	{
		$this->datapack = $datapack;
		$this->data = $data;

		$item = $this->datapack->addItem('ORDER' . $this->data['id']);

		$this->order = $this->datapack->dom->createElementNS($this->datapack->schema['ord'], 'ord:order');
		$this->order->setAttribute('version', self::version);

		$item->appendChild($this->order);

		$this->header();
		$this->items();
	}

	private function push($target, $name, $value = null)
	{
		if (preg_match('/^(\w+)\:/', $name, $m)) {
			$value = preg_replace('/\&/', '&amp;', $value);

			$el = $this->datapack->dom->createElementNS($this->datapack->schema[$m[1]], $name, $value);
		} else {
			$el = $this->datapack->dom->createElement($name, $value);
		}

		$target->appendChild($el);

		return $el;
	}

	private function header()
	{
		$this->order_header = $this->push($this->order, 'ord:orderHeader');

		$this->push($this->order_header, 'ord:orderType', 'receivedOrder');

		$this->push($this->order_header, 'ord:numberOrder', $this->data['id']);
		$this->push($this->order_header, 'ord:date', date('Y-m-d', $this->data['received']));
		$this->push($this->order_header, 'ord:text', __('order_from_eshop', Core::config('store_name')));

		$this->partner();

		if ($order->payment['name']) {
			$payment = $this->push($this->order_header, 'ord:paymentType');
			$this->push($payment, 'typ:ids', $order->payment['name']);
		}

		if ($this->data['comment']) {
			$this->push($this->order_header, 'ord:note', $this->data['comment']);
		}
	}

	private function partner()
	{
		$partner = $this->push($this->order_header, 'ord:partnerIdentity');

		$extid = $this->push($partner, 'typ:extId');
		$this->push($extid, 'typ:ids', $this->data['customer_id'] ? $this->data['customer_id'] : 'Order.' . $this->data['id']);
		$this->push($extid, 'typ:exSystemName', 'WEBMEX');

		$addr = $this->push($partner, 'typ:address');

		if ($this->data['company']) {
			$this->push($addr, 'typ:company', $this->data['company']);
		} else {
			$this->push($addr, 'typ:company', $this->data['first_name'] . ' ' . $this->data['last_name']);
		}

		$this->push($addr, 'typ:name', $this->data['first_name'] . ' ' . $this->data['last_name']);
		$this->push($addr, 'typ:city', $this->data['city']);
		$this->push($addr, 'typ:street', $this->data['street']);
		$this->push($addr, 'typ:zip', $this->data['zip']);

		if ($this->data['company_id']) {
			$this->push($addr, 'typ:ico', $this->data['company_id']);
		}

		if ($this->data['company_vat']) {
			$this->push($addr, 'typ:dic', $this->data['company_vat']);
		}


		if ($this->data['ship_street']) {
			$ship = $this->push($partner, 'typ:shipToAddress');

			if ($this->data['ship_company']) {
				$this->push($ship, 'typ:company', $this->data['ship_company']);
			}

			$this->push($ship, 'typ:name', $this->data['ship_first_name'] . ' ' . $this->data['ship_last_name']);
			$this->push($ship, 'typ:city', $this->data['ship_city']);
			$this->push($ship, 'typ:street', $this->data['ship_street']);
			$this->push($ship, 'typ:zip', $this->data['ship_zip']);
		}
	}

	private function items()
	{
		$this->order_detail = $this->push($this->order, 'ord:orderDetail');

		foreach ($this->data->order_products() as $product) {
			$item = $this->push($this->order_detail, 'ord:orderItem');
			$this->push($item, 'ord:text', $product['name']);
			$this->push($item, 'ord:quantity', $product['quantity']);
			$this->push($item, 'ord:rateVAT', $this->vat_rate);

			$price = $this->push($item, 'ord:homeCurrency');
			$this->push($price, 'typ:unitPrice', $product['price']);

			if ($product['sku'] || $product['ean13']) {
				$stock = $this->push($item, 'ord:stockItem');
				$stockitem = $this->push($stock, 'typ:stockItem');

				if ($product['sku']) {
					$this->push($stockitem, 'typ:ids', $product['sku']);
				}

				if ($product['ean13']) {
					$this->push($stockitem, 'typ:EAN', $product['ean13']);
				}
			}
		}
	}

}