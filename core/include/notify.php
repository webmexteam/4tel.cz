<?php

defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */
class Notify extends Extendable
{

	protected static $messages = array();

	public static function setup()
	{
		$msgs = Registry::get('notifications');

		if (!$msgs) {
			$msgs = array();
		}

		self::$messages = $msgs;
	}

	public static function messages()
	{
		return self::$messages;
	}

	public static function count()
	{
		return count(self::$messages);
	}

	public static function push($msg, $type = 'info', $url = null)
	{
		Event::run('Notify::push', $msg, $type, $url);
		self::$messages[] = new Notify_Message($msg, null, $type, $url);
	}

	public static function payment($payment_type, $order)
	{
		Event::run('Notify::payment', $payment_type, $order);
		self::$messages[] = new Notify_Message(__('payment_received', $order['id'], $payment_type), null, 'payment', url('admin/orders/edit/' . $order['id']));
	}

	public static function find($key, $value)
	{
		foreach (self::$messages as $i => $msg) {
			if ($msg->$key == $value) {
				return $msg;
			}
		}

		return false;
	}

	public static function remove($uid)
	{
		foreach (self::$messages as $i => $msg) {
			if ($msg->uid == $uid) {
				unset(self::$messages[$i]);
			}
		}
	}

	public static function shutdown()
	{
		Registry::set('notifications', self::$messages);
	}

}

register_shutdown_function(array(Notify, 'shutdown'));