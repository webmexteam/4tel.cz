<?php

defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */
class Import_Csv
{

	protected $file;
	public $error = false;
	public $delimiter = ';';
	public $encoding = 'windows-1250';

	public function __construct($file)
	{
		$this->file = $file;

		if (!$this->open($this->file)) {
			$this->error = true;
		}
	}

	public function open($file)
	{
		if (($this->fp = fopen($file, 'r')) === false) {
			return false;
		}

		$this->detectDelimiter();

		return true;
	}

	private function detectDelimiter()
	{
		$max_lines = 15;
		$i = 0;
		$delimiters = '\;\|\,\t';

		while (($line = fgets($this->fp)) !== false) {
			$i++;

			if ($i >= $max_lines) {
				break;
			}

			preg_match_all('/[' . $delimiters . ']/', $line, $m);

			if (!$m || !$m[0]) {
				continue;
			}

			$count = array();
			foreach ($m[0] as $char) {

				if (!isset($count[$char])) {
					$count[$char] = 0;
				}

				$count[$char]++;
			}

			arsort($count);

			if ($delimiter = key($count)) {
				$this->delimiter = $delimiter;
				break;
			}
		}

		$this->reset();
	}

	public function reset()
	{
		if ($this->fp) {
			rewind($this->fp);
		} else {
			$this->open($this->file);
		}
	}

	public function findAllColumns()
	{
		$this->reset();

		$first_line = fgetcsv($this->fp, 0, $this->delimiter);

		$cols = array();

		if ($first_line) {
			foreach ($first_line as $i => $col) {
				if (trim($col)) {

					if ($this->encoding && $this->encoding != 'utf-8') {
						$col = iconv($this->encoding, 'utf-8', $col);
					}

					$cols[$i] = trim($col);
				}
			}
		}

		return $cols;
	}

	public function getColumnIndex($col)
	{
		$index = -1;

		foreach ($this->findAllColumns() as $i => $c) {
			$c = trim($c);

			if ($c == $col) {
				$index = $i;
			}
		}

		$this->reset();

		return $index;
	}

	public function countElement($el = null)
	{
		$this->reset();

		$rows = -1;

		while (($data = fgetcsv($this->fp, 0, $this->delimiter)) !== false) {
			if (count($data) > 1) {
				$rows++;
			}
		}

		return $rows;
	}

	public function getElement()
	{
		$data = fgetcsv($this->fp, 0, $this->delimiter);

		if (count($data) > 1) {

			foreach ($data as $i => $v) {
				if ($this->encoding && $this->encoding != 'utf-8') {
					$data[$i] = iconv($this->encoding, 'utf-8', $v);
				}
			}

			return $data;
		}

		return ($data === false ? false : null);
	}

	public function analyze()
	{
		if ($this->error) {
			return false;
		}

		// common tag names
		$common_cols = array(
			'name' => array('NAME', 'PRODUCT NAME', 'NAZEV', 'PRODUCT'),
			'ean13' => array('EAN', 'EAN13'),
			'sku' => array('SKU', 'CODE'),
			'price' => array('PRICE', 'PRICE_VAT', 'CENA'),
			'description' => array('DESCRIPTION', 'POPIS'),
			'files' => array('IMGURL', 'IMAGE', 'OBRAZEK'),
			'categories' => array('CATEGORYTEXT', 'CATEGORY'),
		);

		$common_uniques = array('name', 'sku', 'ean13');

		if ($elements = $this->findAllColumns()) {

			$col_matches = array();
			$variant_element = null;

			if (!empty($_SESSION['dataimport']['elements']) && is_array($_SESSION['dataimport']['elements'])) {
				foreach ($_SESSION['dataimport']['elements'] as $param => $el) {
					$col_matches[$param] = $el;
				}
			} else {
				foreach ($elements as $i => $col) {
					foreach ($common_cols as $tag => $common) {
						if (in_array(strtoupper(dirify($col)), $common)) {
							$col_matches[$tag] = $i;
							break;
						}
					}
				}
			}

			$unique = null;

			if (!empty($_SESSION['dataimport']['options']['product_unique'])) {
				$unique = $_SESSION['dataimport']['options']['product_unique'];
				$variant_element = $_SESSION['dataimport']['options']['variant_element'];
			} else {
				foreach ($common_uniques as $tag) {
					if ($col_matches[$tag]) {
						$unique = $tag;
						break;
					}
				}

				if (in_array('id', $elements) && in_array('name', $elements)) {
					$unique = 'id';
				}
			}

			$sample = null;

			$this->reset();

			$this->getElement();

			while ($product = $this->getElement()) {
				if (!$sample) {
					$sample = $product;
					break;
				}
			}

			$this->reset();

			return array(
				'filesize' => round(filesize($this->file) / 1024 / 1024, 2),
				'elements' => $elements,
				'suggest' => $col_matches,
				'product_element' => null,
				'variant_element' => $variant_element,
				'product_count' => $this->countElement(),
				'unique' => $unique,
				'sample' => trim($this->cols2Text($elements, $sample))
			);
		}

		return false;
	}

	private function cols2Text($cols, $data)
	{
		$str = '';

		if (!is_array($data)) {
			return '';
		}

		foreach ($data as $i => $v) {
			$str .= "<" . $cols[$i] . "> = " . utf8_encode($v) . "\n";
		}

		return htmlspecialchars($str);
	}

}