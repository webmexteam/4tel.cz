<?php

defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */
define('FPDF_FONTPATH', APPROOT . 'vendor/fpdf/font/');
require(APPROOT . 'vendor/fpdf/fpdf.php');

class Labels_Address
{

	public $pdf;
	protected $addresses = array();
	protected $fontsize = 10;
	protected $rows = 7;

	public function __construct()
	{
		$this->pdf = new FPDF();

		$this->pdf->SetAutoPageBreak(false);

		$this->pdf->AliasNbPages();

		$this->pdf->AddFont('Freesans', '', 'freesans.php');

		$this->pdf->AddPage('P', 'A4');
	}

	public function iconv($text)
	{
		return iconv('utf-8', 'windows-1250', $text);
	}

	public function cell($w, $h, $text, $border = 0, $ln = 0, $align = 'L', $fill = false)
	{
		$this->pdf->Cell($w, $h, $this->iconv($text), $border, $ln, $align, $fill);
	}

	public function multicell($w, $h, $text, $border = 0, $align = 'L')
	{
		$text = preg_replace('/\t/', '    ', $text);

		$this->pdf->MultiCell($w, $h, $this->iconv($text), $border, $align);
	}

	public function moveX($change)
	{
		$this->pdf->SetX($this->pdf->GetX() + $change);
	}

	public function moveY($change)
	{
		$this->pdf->SetY($this->pdf->GetY() + $change);
	}

	public function save($name = null, $dest = 'I')
	{
		$this->render();
		$this->pdf->Output($name, $dest);
	}

	public function render()
	{
		$this->pdf->SetFont('Freesans', '', $this->fontsize);

		$this->moveY($this->rows > 7 ? 5 : 10);

		$y = $this->pdf->GetY();
		$x1 = 5;
		$x2 = 117;

		$rowHeight = round(139 / $this->rows, 1);

		$i = 1;
		foreach ($this->addresses as $addr) {

			if ($i % ($this->rows * 2 + 1) === 0) {
				$i = 1;
				$this->pdf->AddPage();
			}

			$this->pdf->SetY(($i - 1) * $rowHeight + $y);

			if ($i > 0 && $i % 2 === 0) {
				$this->pdf->SetY(($i - 2) * $rowHeight + $y);
				$this->pdf->SetX($x2);
			} else {
				$this->moveX($x1);
			}

			$this->multicell(85, 5.3, join("\n", $addr), 0);

			$i++;
		}
	}

	public function addOrder($order)
	{
		$data = array();
		$prefix = '';

		if ($order['ship_street']) {
			$prefix = 'ship_';
		}

		if ($order[$prefix . 'company']) {
			$data[] = $order[$prefix . 'company'];
		}

		$data[] = $order[$prefix . 'first_name'] . ' ' . $order[$prefix . 'last_name'];
		$data[] = $order[$prefix . 'street'];
		$data[] = $order[$prefix . 'zip'] . ' ' . $order[$prefix . 'city'];
		$data[] = $order[$prefix . 'country'];

		$this->addresses[] = $data;
	}

}