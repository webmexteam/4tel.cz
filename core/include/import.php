<?php

defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */
class Import
{

	public $schema, $schema_id, $driver, $reader, $file, $tmpfile, $options, $download_error;
	public $schema_dir = 'etc/xml_import/';
	public $encoding = null;
	public static $fields = array();
	public $defaults = array(
		'datafile' => null,
		'datatype' => 'xml',
		'product_element' => null,
		'variant_element' => null,
		'product_unique' => null,
		'mode' => 0,
		'delete_products' => 0,
		'create_categories' => 1,
		'category_parent' => 0,
		'manufacturer_parent' => 0,
		'files_target' => '/import/*',
		'files_insert_only' => 0,
		'encoding' => null,
		'elements' => array()
	);
	protected $field_types = array(
		'files' => 'array',
		'manufacturers' => 'array',
		'categories' => 'array',
	);

	public function schema($schema)
	{
		if ($schema == '__quick') {
			$this->file = $_SESSION['dataimport_quick'];

			$opt = $this->analyze();

			$options = array(
				'datafile' => $this->file,
				'datatype' => $opt['datatype'],
				'product_element' => $opt['product_element'],
				'mode' => 0,
				'delete_products' => 0,
				'create_categories' => 1,
				'variant_element' => 'attribute_product_id',
				'category_parent' => 0,
				'files_target' => 'import/*',
				'product_unique' => $opt['unique'],
				'encoding' => $opt['encoding'],
				'elements' => array(),
			);

			$cols = self::$fields;
			$cols['id'] = 'id';

			foreach ($opt['elements'] as $i => $el) {
				if (isset($cols[$el])) {
					$options['elements'][$el] = $i;
				}
			}

			return $options;
		}

		if (!file_exists(DOCROOT . $this->schema_dir . $this->schema . '.php')) {
			throw new Exception('Schema "' . $this->schema . '" not found');
		}

		include_once(DOCROOT . $this->schema_dir . $this->schema . '.php');

		if (empty($import_options) && !empty($xml_import)) {
			$import_options = array();

			if (preg_match('/^fn_/', $xml_import['source'])) {
				$xml_import['source'] = 'function(' . $xml_import['source'] . ')';
			}

			if (is_bool($import_options['create_categories'])) {
				$import_options['create_categories'] = $import_options['create_categories'] ? 1 : 0;
			}

			$import_options['datafile'] = $xml_import['source'];
			$import_options['product_element'] = $xml_import['item'];
			$import_options['product_unique'] = $xml_import['unique'];

			if (!empty($xml_import['files_target_dir']))
				$import_options['files_target'] = $xml_import['files_target_dir'];
			if (!empty($xml_import['files_insert_only']))
				$import_options['files_insert_only'] = $xml_import['files_insert_only'];

			$import_options['elements'] = array();

			foreach ($xml_import['item_data'] as $k => $v) {

				if (preg_match('/^fn_/', $v)) {
					$v = 'function(' . $v . ')';
				} else if (is_numeric($v)) {
					$v = 'static(' . $v . ')';
				}

				if ($k == 'availability_id') {
					$k = 'availability';
				}

				$import_options['elements'][$k] = $v;
			}
		}

		if (empty($import_options)) {
			throw new Exception('Invalid import schema "' . $this->schema . '" - import options not defined');
		}

		$import_options = array_merge($this->defaults, $import_options);

		return $import_options;
	}

	public function __construct($driver, $file = null)
	{
		if ($file === null) {
			$this->schema = $driver;

			$this->options = $this->schema($this->schema);

			$this->driver = $this->options['datatype'];
			$this->file = $this->options['datafile'];

			if (preg_match('/^function\((.*)\)$/iu', $this->file, $m)) {
				$func = $m[1];

				if (class_exists('Dataimport_schema', false) && method_exists('Dataimport_schema', $func)) {
					$this->file = call_user_func_array(array('Dataimport_schema', $func), array($this->options));
				} else if (function_exists($func)) {
					$this->file = $func($this->options);
				} else {
					throw new Exception('Data source is a undefined function "' . $func . '"');
				}
			}
		} else {
			$this->driver = $driver;
			$this->file = $file;
		}


		if (!preg_match('/^(http|ftp)s?:\/\//', $this->file)) {
			// '/' for linux, 'C' for Windows
			$this->file = ($this->file{0} == '/' || $this->file{0} == 'C' ? $this->file : DOCROOT . $this->file);
		}

		$schema_hash = sha1($this->schema);
		$this->schema_id = 'di_' . substr($schema_hash, 0, 6) . '_' . substr($schema_hash, -6);

		if ($this->options['encoding']) {
			$this->encoding = $this->options['encoding'];
		}

		$file = $this->getFile();

		if ($this->driver == 'xml') {
			$this->reader = new Import_Xml($file);
		} else if ($this->driver == 'csv') {

			if (!$this->encoding) {
				$this->encoding = 'windows-1250';
			}

			$this->reader = new Import_Csv($file);
			$this->reader->encoding = $this->encoding;
		} else if ($this->driver == 'xls') {
			$this->reader = new Import_Xls($file);
		} else if ($this->driver == 'xlsx') {
			$this->reader = new Import_Xlsx($file);
		} else if ($this->driver == 'ods') {
			$this->reader = new Import_Ods($file);
		}
	}

	private function findNode($item, $node)
	{
		$value = $item;
		$found = false;

		if (is_array($item) && is_numeric($node)) {
			return $item[$node];
		}

		$parts = explode('/', $node);

		foreach ($parts as $part) {
			if (is_object($value) && property_exists($value, $part)) {
				$found = true;
				$value = $value->$part;
			}
		}

		if (!$found) {
			return false;
		}

		return $value;
	}

	public function getElement($element, & $subnodes = array())
	{
		if (($item = $this->reader->getElement($element)) !== false) {

			foreach ($subnodes as $name => $subnode) {
				$value = null;
				$is_array = false;

				if (preg_match('/^(static|function|expression)\(/', $subnode)) {
					continue;
				}

				if (($node = $this->findNode($item, $subnode)) !== false) {
					if ($node instanceof Traversable) {
						$value = array();

						if (count($node) == 1) {
							$value = $this->parseValue($name, stripslashes(html_entity_decode((string) $node)));
						} else {
							$is_array = true;

							foreach ($node as $k => $v) {
								$value[] = $this->parseValue($name, stripslashes(html_entity_decode((string) $v)));
							}
						}
					} else {
						$value = $this->parseValue($name, stripslashes(html_entity_decode((string) $node)));
					}
				}

				if (isset($this->field_types[$name]) && $this->field_types[$name] == 'array' && (is_object($value) || !is_array($value))) {
					$value = array($value);
				} else if (($name == 'categories' || $name == 'manufacturers') && !is_array($value[0])) {
					$value = array($value);
				}

				$subnodes[$name] = $value;
			}

			return $item;
		}

		return false;
	}

	private function parseValue($name, $value)
	{
		if (in_array($name, array('price', 'price_old', 'cost'))) {
			$value = parseFloat($value);
		} else if ($name == 'vat') {
			$value = parseFloat($value);

			if ($value < 0) {
				$value = $value * 100;
			}
		} else if (in_array($name, array('categories', 'manufacturers'))) {
			//$value = preg_split('/(\s?[\||\/|\>|\;\*]+)|(\s[\-]+)\s?/', $value);
			$cats = array();

			foreach (preg_split('/\r?\n|\|/mu', $value) as $category_path) {
				$cats[] = preg_split('/(\s?[\>|\;\*]+)|(\s[\-]+)\s?/', $category_path);
			}

			$value = $cats;
		} else if (in_array($name, array('files'))) {
			$value = preg_split('/\,|\;/', $value);

			if (count($value) == 1) {
				$value = current($value);
			}
		}

		/* if(is_string($value) && $this->encoding && strtolower($this->encoding) != 'utf-8'){
		  $value = iconv($this->encoding, 'utf-8', $value);
		  } */

		return $value;
	}

	public function download_error($errno, $errstr, $errfile, $errline)
	{
		$this->download_error = $errstr;
	}

	public function download($lifetime = 20)
	{
		$status = false;

		if (preg_match('/^(http|ftp)s?:\/\//i', $this->file)) {
			$tmpfile = DOCROOT . 'etc/tmp/dataimport_' . md5($this->file);

			$this->tmpfile = $tmpfile;

			set_error_handler(array($this, 'download_error'));

			if (file_exists($tmpfile) && filemtime($tmpfile) >= strtotime('-' . $lifetime . ' hours')) {
				$status = true;
			} else if (remoteCopy($this->file, $tmpfile)) {
				$status = true;
			} else {
				throw new Exception(($this->download_error ? $this->download_error : 'Unknown download error'));
			}
		} else if (file_exists($this->file)) {
			$status = true;
		} else {
			throw new Exception('File not found: ' . $this->file);
		}

		return $status;
	}

	public function getFile()
	{
		if ($this->download()) {
			return $this->tmpfile ? $this->tmpfile : $this->file;
		}

		return false;
	}

	public function analyze()
	{
		$result = array();

		if ($file = $this->getFile()) {
			$datatype = $this->getDatatype();

			if ($datatype == 'xml') {
				$this->reader = new Import_Xml($file);
			} else if ($datatype == 'csv') {
				if (!$this->encoding) {
					$this->encoding = 'windows-1250';
				}

				$this->reader = new Import_Csv($file);
				$this->reader->encoding = $this->encoding;
			} else if ($datatype == 'xls') {
				$this->reader = new Import_Xls($file);
			} else if ($datatype == 'xlsx') {
				$this->reader = new Import_Xlsx($file);
			} else if ($datatype == 'ods') {
				$this->reader = new Import_Ods($file);
			}

			$result = $this->reader->analyze();

			$pathinfo = pathinfo($this->file);

			$result['encoding'] = $this->encoding;
			$result['datatype'] = $datatype;
			$result['source'] = (preg_match('/^(http|ftp)s?\:\/\//i', $this->file)) ? $this->file : $pathinfo['basename'];
		}

		return $result;
	}

	public function getDatatype()
	{
		$xls_ole = pack("CCCCCCCC", 0xd0, 0xcf, 0x11, 0xe0, 0xa1, 0xb1, 0x1a, 0xe1);

		if ($file = $this->getFile()) {
			$datatype = 'csv';

			$fp = fopen($file, 'r');
			$lines = 0;

			while ($lines < 6 && ($line = fgets($fp, 4096))) {

				if (($line && preg_match('/\<\?xml/', $line)) || ($line && $lines < 1 && substr_count($line, '<') >= 2)) {
					$datatype = 'xml';
				}

				if ($lines == 0 && (preg_match('/oasis\.opendocument\.spreadsheet/i', $line) || trim($line) == 'PK')) {
					$datatype = 'ods';
					break;
				} else if ($lines == 0 && substr($line, 0, 8) == $xls_ole) {
					$datatype = 'xls';
					break;
				}

				if ($datatype == 'xml' && preg_match('/<workbook/i', $line) && preg_match('/schemas\-microsoft\-com/i', $line)) {
					$datatype = 'xlsx';
					break;
				}

				$lines++;
			}

			fclose($fp);

			return $datatype;
		}

		return false;
	}

	public function getSample(array $options = array(), & $random = false)
	{
		if ($file = $this->getFile()) {
			$datatype = $this->getDatatype();

			$options = array_merge(array(
				'product_element' => null,
				'elements' => array()
					), $options);

			if ($datatype == 'xml') {
				$this->reader = new Import_Xml($file);
			} else if ($datatype == 'csv') {
				$this->reader = new Import_Csv($file);
				$this->reader->encoding = $this->encoding;

				// flush first line
				$this->reader->getElement();
			} else if ($datatype == 'xls') {
				$this->reader = new Import_Xls($file);

				// flush first line
				$this->reader->getElement();
			} else if ($datatype == 'xlsx') {
				$this->reader = new Import_Xlsx($file);

				// flush first line
				$this->reader->getElement();
			} else if ($datatype == 'ods') {
				$this->reader = new Import_Ods($file);

				// flush first line
				$this->reader->getElement();
			}

			if ($this->reader && !$this->reader->error) {
				$item_data = $options['elements'];

				if ($random) {
					$random = is_numeric($random) ? $random : mt_rand(1, (int) $this->reader->countElement($options['product_element']));
					$i = $random;

					$this->reader->reset();

					while ($i > 0) {
						$tmp = $this->getElement($options['product_element']);
						$i--;
					}
				}

				if (in_array(datatype, array('csv', 'xls', 'xlsx', 'ods'))) {
					// flush first line
					$this->reader->getElement();
				}

				if ($product = $this->getElement($options['product_element'], $item_data)) {
					$item_data = $this->sanitizeData($product, $item_data, array(), true, $options);
				}

				return $item_data;
			}
		}

		return false;
	}

	public function sanitizeData($product, $item_data, $import_data = array(), $preview = false, $xml_import = array())
	{
		$schema_class = class_exists('Dataimport_schema', false);

		foreach ($item_data as $name => $value) {

			if (is_string($value) && preg_match('/^function\((.*)\)$/iu', $value, $m) && $m) {
				$func = $m[1];

				if ($schema_class && method_exists('Dataimport_schema', $func)) {
					$value = call_user_func_array(array('Dataimport_schema', $func), array($product, $item_data, $xml_import, $import_data));
				} else if (function_exists($func)) {
					$value = $func($product, $item_data, $xml_import, $import_data);
				} else if ($preview) {
					$value .= ' (FUNCTION NOT DEFINED)';
				} else {
					$value = null;
				}
			} else if (is_string($value) && preg_match('/^expression\((.*)\)$/iu', $value, $m) && $m) {
				$value = $this->evalExpression($m[1], array(
					'data' => $product,
					'current_data' => $item_data
						));
			} else if (is_string($value) && preg_match('/^static\((.*)\)$/iu', $value, $m) && $m) {
				$value = $m[1];
			}

			if (is_string($value)) {
				$value = trim($value);
			}

			$item_data[$name] = $value;

			if ($name == 'files' && count($value) == 1 && $value[0] == null) {
				$item_data[$name] = array();
			}

			if ($preview) {

				if ($value === null) {
					$item_data[$name] = '';
				}

				if ($name == 'description') {
					$item_data[$name] = limit_words(strip_tags((string) $value), 40);
				}

				if ($name == 'description_short') {
					$item_data[$name] = limit_words(strip_tags((string) $value), 20);
				}

				if ($name == 'availability' && is_numeric($value)) {
					if ($av = Core::$db->availability[(int) $value]) {
						$item_data[$name] = $av['name'] . ' [ID ' . $av['id'] . ']';
					}
				}

				if ($name == 'features' && isBucket($value)) {
					$item_data[$name] = $value->as_array();
				}

				if ($name == 'categories') {
					// remove page options (menu etc.)
					foreach ((array) $value as $i => $v) {
						if (is_array($v[0])) {
							$item_data[$name][$i][0] = $v[0][0];
						}
					}
				}
			} else {
				if ($name == 'categories' && is_array($value)) {
					$item_data[$name] = Core::$api->bucket($value, array(
						'parent_page' => $this->options['category_parent'],
						'map_id' => $this->schema_id
							));
				}

				if ($name == 'manufacturers' && is_array($value)) {
					$item_data[$name] = Core::$api->bucket($value, array(
						'parent_page' => $this->options['manufacturer_parent'],
						'map_id' => 'm' . $this->schema_id
							));
				}

				if ($name == 'features') {
					if (!isBucket($value)) {
						$item_data[$name] = Core::$api->bucket($value, array(
							'featureset_id' => $this->schema_id
								));
					} else if (!$value->featureset_id) {
						$value->featureset_id = $this->schema_id;
					}
				}
			}
		}

		return $item_data;
	}

	public function evalExpression($expression, $data)
	{
		$expression = preg_replace(array(
			'/\"|\'/',
			'/IF\((.*)\):/i',
			'/ELSE:/i',
			'/(echo|print|unset|isset|empty|include|require|include_once|require_once)/i',
			'/MATH\(([^\(\)]+)\)/i',
			'/MATH\(.*\)/i',
			'/\{(\$[\w\-\>\[\]]+)\}/i'
				), array(
			'',
			'".($1) ? "',
			'" : "',
			'',
			'".($1)."',
			'',
			'".$1."'
				), $expression);

		$value = null;

		if (trim($expression)) {

			extract($data);

			eval('$value = "' . $expression . '";');
		}

		return $value;
	}

	public function count()
	{
		return (int) $this->reader->countElement(@$this->options['product_element']);
	}

}

Import::$fields = array(
	'name' => __('name'),
	'nameext' => __('nameext'),
	'status' => __('status'),
	'position' => __('position'),
	'description_short' => __('description_short'),
	'description' => __('description'),
	'price' => __('price'),
	'price_old' => __('price_old'),
	'vat' => __('vat'),
	'stock' => __('stock'),
	'sku' => __('sku'),
	'ean13' => __('ean13'),
	'sef_url' => __('sef_url'),
	'title' => __('title'),
	'meta_description' => __('meta_description'),
	'meta_keywords' => __('meta_keywords'),
	'name_feed' => __('name_feed'),
	'description_feed' => __('description_feed'),
	'productno' => __('productno'),
	'availability' => __('availability'),
	'availability_days' => __('availability_days'),
	'guarantee' => __('guarantee'),
	'recycling_fee' => __('recycling_fee'),
	'copyright_fee' => __('copyright_fee'),
	'weight' => __('weight'),
	'cost' => __('cost'),
	'margin' => __('margin'),
	'categories' => __('categories'),
	'manufacturers' => __('manufacturer'),
	'files' => __('files'),
	'attributes' => __('attributes'),
	'features' => __('features'),
	'delete' => __('delete'),
	'skip' => __('skip'),
);