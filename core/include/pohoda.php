<?php

defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */
class Pohoda
{

	protected $xml;
	protected $xml_file;
	public static $config = array();

	public static function config()
	{
		if (!empty(self::$config)) {
			return self::$config;
		}

		$config = array(
			'ico' => null,
			'stock_internet_only' => true,
			'stock_price_only' => true,
			'stock_enable_insert' => false,
			'stock_pictures' => true,
			'stock_pictures_root' => 'files/'
		);

		if (!($opt = @json_decode(Core::config('pohoda'), true))) {
			$opt = array();
		}

		self::$config = array_merge($config, $opt);

		return self::$config;
	}

	public function __construct($xml_file, $is_string = false)
	{
		if ($is_string) {
			if (!($this->xml = @simplexml_load_string($xml_file))) {
				throw new Exception('Invalid XML input');
			}
		} else {
			$this->xml_file = $xml_file;

			if (!$this->xml_file || !file_exists($this->xml_file)) {
				throw new Exception('File "' . $this->xml_file . '" not found');
			}

			if (!($this->xml = @simplexml_load_file($this->xml_file))) {
				throw new Exception('Invalid XML input');
			}
		}
	}

	public function xpath($path)
	{
		return $this->xml->xpath($path);
	}

	public function val($el)
	{
		if ($v = (string) $el[0]) {
			return $v;
		}

		return null;
	}

	public function response()
	{
		$attrs = $this->xml->attributes();

		$datapack = new Pohoda_Builder_Responsepack($attrs['id'], 'ok');

		echo $datapack->build();
	}

}