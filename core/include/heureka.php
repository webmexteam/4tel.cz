<?php defined('WEBMEX') or die('No direct access.');

use Nette\Object;


/**
 * Heureka
 *
 * @author Michal Mikolas <nanuqcz@gmail.com>
 * @copyright Webmex s.r.o.
 */
class Heureka extends Object
{
	private static $cacheData = array();



	public static function getScore($shopSlug)
	{
		$cacheKey = 'getScore-' . $shopSlug;

		// Load from cache
		$score = self::loadCache($cacheKey);
		
		// Refresh cache
		if (!$score) {
			$content = file_get_contents("http://obchody.heureka.cz/$shopSlug/recenze/");
			$match = preg_match('#<div[^>]+class="score"[^>]*>\s*([0-9]+)\s*<#', $content, $matches);

			if ($match) {
				$score = $matches[1];
			} else {
				$score = NULL;
			}

			self::saveCache($cacheKey, $score);
		}

		// Return
		return $score;
	}



	public static function getCategoryTree()
	{
		// Load from cache
		$categoryTree = self::loadCache('getCategoryTree');
		
		// Refresh cache
		if (!$categoryTree) {
			$xml = simplexml_load_file('http://www.heureka.cz/direct/xml-export/shops/heureka-sekce.xml');
			
			$categoryTree = self::getCategoryTreeFromXml($xml);

			self::saveCache('getCategoryTree', $categoryTree);
		}

		// Return
		return $categoryTree;
	}



	private static function getCategoryTreeFromXml($xml) 
	{
		$categoryTree = array();

		foreach ($xml->CATEGORY as $category) {
			$childs = array();
			if (isset($category->CATEGORY)) {
				$childs = self::getCategoryTreeFromXml($category);
			}

			$categoryTree[] = array(
				'name' => (string)$category->CATEGORY_NAME,
				'fullname' => (string)$category->CATEGORY_FULLNAME,
				'childs' => $childs,
			);
		}

		return $categoryTree;
	}



	public static function getCategoryNames() 
	{
		// Load from cache
		$categoryNames = self::loadCache('getCategoryNames');
		
		// Refresh cache
		if (!$categoryNames) {
			$xml = simplexml_load_file('http://www.heureka.cz/direct/xml-export/shops/heureka-sekce.xml');
			
			$categoryNames = self::getCategoryNamesFromXml($xml);

			self::saveCache('getCategoryNames', $categoryNames);
		}

		// Return
		return $categoryNames;
	}



	private static function getCategoryNamesFromXml($xml) 
	{
		$categoryNames = array();

		foreach ($xml->CATEGORY as $category) {
			if (isset($category->CATEGORY_FULLNAME)) {
				$categoryNames[] = (string)$category->CATEGORY_FULLNAME;
			}

			if (isset($category->CATEGORY)) {
				$categoryNames = array_merge($categoryNames, self::getCategoryNamesFromXml($category));
			}
		}

		return $categoryNames;
	}



	private static function loadCache($key = NULL)
	{
		$filePath = self::getCacheFilePath();

		self::invalidateFile($filePath, 60*60*24);

		// Check
		if (!file_exists($filePath)) 
			return NULL;

		// Load
		$data = unserialize(file_get_contents($filePath));

		// Return
		if ($key !== NULL) {
			return @$data[$key];	

		} else {
			return $data;
		}
	}



	private static function saveCache($key, $value)
	{
		$filePath = self::getCacheFilePath();

		// Load
		if (!file_exists($filePath)) {
			$data = self::loadCache();
		
		} else {
			$data = array();
		}

		// Save
		$data[$key] = $value;

		return file_put_contents($filePath, serialize($data));
	}



	private static function invalidateFile($filePath, $durability)
	{
		if (file_exists($filePath) 
			&& filemtime($filePath) < (time() - $durability) ) 
		{
			unlink($filePath);
		}
	}



	private static function getCacheFilePath()
	{
		return DOCROOT . '/etc/tmp/heureka.cache.php';
	}

}