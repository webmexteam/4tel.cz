<?php

defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */
class Arr
{

	public static function overwrite($arr)
	{
		foreach (array_slice(func_get_args(), 1) as $array) {

			$array = $array instanceof WebmexAPI_Bucket ? $array->as_array() : (array) $array;

			foreach (array_intersect_key((array) $array, (array) $arr) as $key => $value) {
				if (is_array($arr[$key]) && is_array($value)) {
					$arr[$key] = self::overwrite($arr[$key], $value);
				} else {
					$arr[$key] = $value;
				}
			}
		}

		return $arr;
	}

	public static function serialize($arr)
	{
		$values = array();

		foreach ($arr as $key => $val) {
			$values[] = urlencode($key) . '=' . urlencode((string) $val);
		}

		return join('&', $values);
	}

	public static function unserialize($str)
	{
		$arr = array();

		try {
			foreach (preg_split('/\&/', $str) as $var) {
				$parts = explode('=', $var);

				$arr[urldecode($parts[0])] = urldecode($parts[1]);
			}
		} catch (Exception $e) {

		}

		return $arr;
	}

}