<?php

defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */
class Odb_Schema
{

	protected $table;

	public function __construct($table)
	{
		$this->table = $table;
	}

	public function getPrimaryKey()
	{
		return 'id';
	}

}