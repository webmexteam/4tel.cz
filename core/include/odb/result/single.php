<?php

defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */
class Odb_Result_Single extends Odb_Result implements ArrayAccess
{

	protected $row;

	public function update($data, $toString = false)
	{
		return parent::update($data, $toString, $this->row);
	}

	protected function fetchRow($offset)
	{
		if (!$this->data_fetched) {
			$this->where($this->schema->getPrimaryKey(), $offset)->limit(1);

			$this->fetchData();
		}

		$this->row = $this->current();
	}

	public function offsetExists($offset)
	{
		if (!$this->data_fetched) {
			$this->fetchRow($offset);
		}

		return (bool) $this->row;
	}

	public function offsetGet($offset)
	{
		if (!$this->data_fetched) {
			$this->fetchRow($offset);
		}

		return $this->row ? $this->row : null;
	}

	public function offsetSet($offset, $value)
	{

	}

	public function offsetUnset($offset)
	{

	}

}