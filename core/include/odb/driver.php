<?php

defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */
abstract class Odb_Driver
{

	protected $link;

	public function __construct($dsn, $user = null, $password = null)
	{

	}

	public function query($query)
	{

	}

	public function num_rows($resource)
	{

	}

	public function data_seek($resource, $rownum)
	{

	}

	public function fetch_assoc($resource)
	{

	}

	public function fetch_array($resource)
	{

	}

	public function fetch_object($resource)
	{

	}

	public function escape($value)
	{
		return $value;
	}

	public function last_insert_id()
	{

	}

	public function affected_rows()
	{

	}

}