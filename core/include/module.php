<?php

defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */
class Module extends Extendable
{

	public $status = 0;
	public $installed = false;
	public $registry = null;
	public $module_name = null;
	public $load_priority = 0;
	public $dependencies = array();
	public $has_settings = false;
	public $auto_update = true;

	public function setup()
	{

	}

	public function install($db_module)
	{

	}

	public function update()
	{
		$current_version = $this->registry['version'];
		$new_version = $this->module_info['version'];

		$versions = Update::getVersions($current_version, $new_version);

		if ($versions) {
			foreach ($versions as $version) {
				if (is_array($version)) {
					$func = 'update_' . join('_', $version);

					if (!method_exists($this, $func) && array_pop($version) == 0) {
						$func = 'update_' . join('_', $version);
					}
				} else {
					$func = 'update_' . preg_replace('/\./', '_', $version);
				}

				if (method_exists($this, $func)) {
					// TODO: log version update
					call_user_func(array($this, $func));
				}
			}
		}

		$this->registry->update(array('version' => $new_version));
	}

	public function activate()
	{
		$this->registry->update(array('status' => 1));
	}

	public function deactivate()
	{
		$this->registry->update(array('status' => 0));

		foreach (Core::$modules as $module) {
			if ($module->dependencies && isSet($module->dependencies[$this->module_name])) {
				$module->deactivate();
			}
		}
	}

	public function getLoadPriority()
	{
		return $this->load_priority ? $this->load_priority : 0;
	}

	public function getInfo($key)
	{
		if (in_array($key, array('name', 'description')) && is_array($this->module_info[$key])) {
			return !empty($this->module_info[$key][Core::$language]) ? $this->module_info[$key][Core::$language] : $this->module_info[$key]['en'];
		}

		return !empty($this->module_info[$key]) ? $this->module_info[$key] : null;
	}

	public function getSettingFrom()
	{

	}

	public function saveSettingFrom()
	{

	}

	public function getStorage()
	{
		return @unserialize($this->registry['storage']);
	}

	public function saveStorage($data)
	{
		$this->registry['storage'] = serialize($data);

		return $this->registry->update(array('storage' => $this->registry['storage']));
	}

	public function isActive()
	{
		return ($this->installed && $this->status);
	}

	public function checkCollisions()
	{
		$collisions = array();

		if ($this->dependencies) {
			foreach ($this->dependencies as $mod => $version) {
				if (!($module = Core::$modules[$mod]) || (!$module->installed || !$module->status)) {
					$collisions[] = __('module_dependency') . ': ' . ($module ? $module->getInfo('name') : $mod);
				}
			}
		}

		return $collisions;
	}

	public static function install_module($module_name)
	{
		if (Core::$modules[$module_name]) {
			$mod = Core::$modules[$module_name];

			if (!Core::$db->module()->where('name', $module_name)->fetch()) {
				$v = $mod->getInfo('version');

				$collisions = $mod->checkCollisions();

				$module_id = Core::$db->module(array(
					'name' => $module_name,
					'version' => $v,
					'status' => $collisions ? 0 : 1
						));

				if ($module_id) {
					return Core::$db->module[$module_id];
				}
			}
		}

		return false;
	}

}