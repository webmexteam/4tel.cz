<?php defined('WEBMEX') or die('No direct access.');


/**
 * AntispamHelpers
 *
 * @author  Michal Mikoláš <nanuqcz@gmail.com>
 * @copyright  Webmex plus s.r.o.
 */
class AntispamHelpers
{
	/** @var string */
	public static $key = 'AABBAABBAA';

	/** @var int */
	public static $timeMin = 5;

	/** @var int */
	public static $timeMax = 86400;  // 60 * 60 * 24 = 86400



	public static function cryptTime($time)
	{
		return strrev($time) ^ self::$key;
	}



	public static function decryptTime($time)
	{
		return strrev($time ^ self::$key);
	}



	public static function validateHidden($value) 
	{
		return ($value === '');
	}



	public static function validateCryptedTime($cryptedTime)
	{
		$time = self::decryptTime($cryptedTime);

		return self::validateTime($time);
	}



	public static function validateTime($time)
	{
		$actTime = time();

		if ($time > ($actTime - self::$timeMin)) {  // data sent too quick?
			return FALSE;
		}

		if ($time < ($actTime - self::$timeMax)) {  // data sent big time later?
			return FALSE;
		}

		return TRUE;
	}

}