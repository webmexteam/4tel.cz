<?php

defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */
class Email extends Extendable
{

	protected $template, $data = array();
	public $from, $to, $reply_to, $subject, $text, $attachment;
	public static $events = array(
		'new_order',
		'new_account',
		'reset_password',
		'new_password',
		'order_comment',
		'low_stock',
		'voucher',
		'order_status_in_process',
		'order_status_finished',
		'order_status_canceled',
		'order_status_to_send',
		'order_confirmed',
		'order_status_confirmed',
		'order_status_updated',
		'order_status_waiting_for_payment',
		'order_status_paid',
		'order_status_waiting_for_pickup',
		'order_status_sent',
	);

	public static function event($event_name, $to, $reply_to = null, $data = array(), $attachment = null)
	{

		if (is_object($data)) {
			$data = $data->as_array();
		}

		if (in_array($event_name, self::$events) && ($emails = Core::$db->email_message()->where('`key`', $event_name))) {
			foreach ($emails as $email) {

				$e = new Email($email['template']);
				$e->subject = $email['subject'];
				$e->content = $email['text'];

				if ($attachment) {
					$e->attachment = $attachment;
				}

				if ($reply_to) {
					$e->reply_to = $reply_to;
				}

				if (!empty($data['order']) && is_object($data['order'])) {
					$e->setOrder($data['order']);
					unset($data['order']);
				}

				$e->set($data);

				$e->send($to);
			}

			return true;
		}

		return false;
	}

	public static function registerEvent($event_name)
	{
		if (!in_array($event_name, self::$events)) {
			self::$events[] = $event_name;
		}
	}

	public function __construct($template = 'default')
	{
		if (!Core::findFile('template/system/email/' . $template . '.php')) {
			$template = 'default';
		}

		$this->template = $template;
	}

	public function __set($name, $value)
	{
		$this->data[$name] = $value;
	}

	public function __get($name)
	{
		return isSet($this->data[$name]) ? $this->data[$name] : null;
	}

	public function set($data)
	{
		if (is_object($data) && method_exists($data, 'as_array')) {
			$data = $data->as_array();
		}

		$this->data = array_merge($this->data, $data);
	}

	public function setOrder($order)
	{
		$this->set($order);
		$this->products = $order->order_products();
		$this->comments = $order->order_comments()->order('time DESC');

		if ($order['delivery_id']) {
			$delivery_driver = 'Delivery_' . ucfirst($order->delivery['driver']);
		}

		if ($order['payment_id']) {
			$payment_driver = 'Payment_' . ucfirst($order->payment['driver']);
		}

		$delivery = null;
		if (isSet($delivery_driver) && class_exists($delivery_driver)) {
			$delivery = new $delivery_driver($order->delivery);
		}

		$payment = null;
		if (isSet($payment_driver) && class_exists($payment_driver)) {
			$payment = new $payment_driver($order->payment);

			if ($pinfo = $payment->getPaymentInfo($order)) {
				if (trim(strip_tags($pinfo))) {
					$this->payment_info = $pinfo;
				}
			}
		} else {
			$payment = new Payment($order->payment);

			if ($pinfo = $payment->getPaymentInfo($order)) {
				if (trim(strip_tags($pinfo))) {
					$this->payment_info = $pinfo;
				}
			}
		}

		$this->delivery = $delivery;
		$this->payment = $payment;

		$this->billing_address = $order['first_name'] . ' ' . $order['last_name']
				. ($order['company'] ? '<br>' . $order['company'] : '')
				. '<br>' . $order['street']
				. '<br>' . $order['city'] . ' ' . $order['zip']
				. '<br>' . $order['country'];

		if ($order['company_id']) {
			$this->billing_address .= '<br>' . __('company_id') . ": " . $order['company_id'];
		}

		if ($order['company_vat']) {
			$this->billing_address .= '<br>' . __('company_vat') . ": " . $order['company_vat'];
		}

		$this->shipping_address = $order['ship_street'] ? (
				$order['ship_first_name'] . ' ' . $order['ship_last_name']
				. ($order['ship_company'] ? '<br>' . $order['ship_company'] : '')
				. '<br>' . $order['ship_street']
				. '<br>' . $order['ship_city'] . ' ' . $order['ship_zip']
				. '<br>' . $order['ship_country']
				) : __('shipping_as_billing');

		$this->items = '';

		foreach ($this->products as $product) {
			$this->items .= ($product['sku'] ? '' . $product['sku'] . ' - ' : '') . $product['name'] . ' -  ' . price_vat(array($product['product'], $product['price'])) . ' * ' . $product['quantity'] . ' = ' . price_vat(array($product['product'], $product['price']))* $product['quantity'] . '<br>';
		}

		$this->order_status = $order['status'];
		$this->customer_comment = $order['comment'];
		
		// total price
		$this->total_excl_vat = $order['total_excl_vat'];
		$this->total = price($order['total_incl_vat']);
		
		if ($order['voucher_id'] && ($voucher = $order->voucher) && $voucher['id']) {
			$this->voucher = $voucher;
			$this->voucher_discount = estPrice($voucher['value'], $order->model->getTotal_incl_vat(true));

			$this->total_excl_vat = $this->total_excl_vat - estPrice($voucher['value'], $order['total_excl_vat']);
		}
	}

	public function send($to = null)
	{
		if ($to) {
			$this->to = $to;
		}

		$mailer = mailerConnect();

		if ($this->from) {
			$mailer->From = $this->from;
			$mailer->FromName = Core::config('store_name');
		} else {
			$mailer->From = Core::config('email_sender');
			$mailer->FromName = Core::config('store_name');
		}

		$toEmails = preg_split('/,|;\s*/', $this->to);
		foreach ($toEmails as $toEmail) {
			$mailer->AddAddress($toEmail);
		}

		if ($this->reply_to) {
			$mailer->AddReplyTo($this->reply_to);
		}

		$html = $this->prepareEmail();

		$mailer->Subject = $this->subject;
		$mailer->MsgHTML($html);
		$mailer->AltBody = $this->getPlainText($html);

		if($this->attachment && is_array($this->attachment)) {
			foreach ($this->attachment as $key => $attachment) {
				if (file_exists($attachment)) {
					$mailer->AddAttachment($attachment);
				}
			}
		}else{
			if ($this->attachment && file_exists($this->attachment)) {
				$mailer->AddAttachment($this->attachment);
			}
		}

		return $mailer->send();
	}

	private function getPlainText($html)
	{
		if (preg_match('/<body.*\/body>/s', $html, $m) && $m) {
			$text = trim(html_entity_decode(preg_replace(array('/\&nbsp;/', '/\&times;/'), array(' ', '*'), strip_tags($m[0]))));
			$text = preg_replace('/\s+\n+/', '<br>', $text);
			$text = preg_replace('/\s{2,}/', ' ', $text);
			$text = preg_replace('/<br>/', "\n", $text);
			$text = preg_replace('/\n\s+/', "\n", $text);
		} else {
			$text = strip_tags($html);
		}

		return $text;
	}

	private function prepareEmail()
	{
		$data = array_merge(array(
			'store_name' => Core::config('store_name'),
			'store_url' => url(null, null, true),
			'admin_url' => url('admin', null, true)
				), $this->data);

		$tpl = tpl('system/email/layout.php', array(
			'content' => tpl('system/email/' . $this->template . '.php', array_merge(array(
						'content' => $this->text
							), $data))
				));

		foreach ($data as $name => $value) {
			if (is_scalar($value) || (is_object($value) && method_exists($value, '__toString'))) {
				$value = (string) $value;

				$this->subject = str_replace('{' . $name . '}', $value, (string) $this->subject);
				$tpl = str_replace('{' . $name . '}', $value, (string) $tpl);
			}
		}

		$tpl = preg_replace('/\{[\w\_\-]+\}/miu', '', $tpl);
		$tpl = preg_replace('/(href|src)\="(\/[^\"]+)"/miu', '$1="' . Core::$domain . '$2"', $tpl);

		return $tpl;
	}

	public static function html($text)
	{
		$html = strip_tags($text);

		// remove unwanted whitespaces
		$html = preg_replace('/\t/', ' ', $html);

		// split by double line break
		$p = preg_split('/(\r?\n){2,}/', $html);

		// make paragraphs
		$html = '<p>' . join('</p><p>', $p) . '</p>';

		return preg_replace('/\r?\n/', '<br>', $html);
	}

}