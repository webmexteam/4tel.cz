<?php

defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */
if (!defined('FM_ROOT')) {
	define('FM_ROOT', DOCROOT . 'files');
}

class Filemanager
{

	public static $user_uid;
	public static $extra_thumb_size = '80x80';
	public static $upload_file_name = 'Filedata';
	public static $default_preview = '/9j/4AAQSkZJRgABAgAAZABkAAD/7AARRHVja3kAAQAEAAAARgAA/+4ADkFkb2JlAGTAAAAAAf/bAIQABAMDAwMDBAMDBAYEAwQGBwUEBAUHCAYGBwYGCAoICQkJCQgKCgwMDAwMCgwMDQ0MDBERERERFBQUFBQUFBQUFAEEBQUIBwgPCgoPFA4ODhQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQU/8AAEQgAZABkAwERAAIRAQMRAf/EAHMAAQADAQEBAQAAAAAAAAAAAAADBAUCBgEIAQEAAAAAAAAAAAAAAAAAAAAAEAACAQQBAgMFBwQBBQAAAAABAgMAERIEBSETMUEUUWEiMgZxscFyIzQVgZGhYkKCwkODRREBAAAAAAAAAAAAAAAAAAAAAP/aAAwDAQACEQMRAD8A/c68JdBlLaS3UAXAP96DM7D9/wBP/wCTLD3Xvag024SyHGW8lugIsCf70GbrwNsTLCvQt4k+VvGgv7HD9qFpI5MmQZFSLXA8aCnp6rbcvbBxAGTN49KCzucX6eEzJJkF+YEW6HpQVtPVbbl7YOIAyZvHpQWdzi/TwmZJMgvzAi3Q9KCHR0TuM3xYIlrm1zc0He9xx1EWRXzQnE3FiDQNHjjto0jPggOIsLkmgsfwrd23d/StfK3W/stQbFB5/wD+t/7v+6g9BQef4z9+v/V9xoNvY/by/kb7jQZHC/uJPyfiKDS5L9lL9g+8UGbwv7iT8n4ig0uS/ZS/YPvFBT4T5ZvtX8aCbmP2g/OPuNA4f9ofzn7hQaFB51eT3Fj7YfoBYMQCaCpk2Wdzle9/O9Bbbk9xo+2X6EWLAAGgqo7RuHQ4uvUEUFibkdqaMxuwxPzWFr0EEM0kDiSJsXHnQTbG9s7K4SN8HjiBa9BDDNJA4kibFx50E2xvbOyuEjfB44gWvQRwbM2sxeFrE9CPEGg62Nyfat3WuB4KBYUDX3J9W/aawPipFxQd/wAjt93u9z4rWtYY2+ygq0CgUCgUCgUCgUCgUCgUCgUCgx3+qOEjaZJNh0MEUmw5aCcBoYTZ3jJS0gW/Xt3oLQ5nizu/x3qU9Z6f1vb629PfHPK2Nr++9BXn+peG1Yop553WCZI5RL2ZmREm6IZGCER5eXcxoIuR+o9PiE5HY2naeLR7Ik19eB2lj7w6ZMTi1/G/S3getBKec0lkeSScxa8Wo+7JBLrTxTrFGxDSHMAgC3yYZefhQdaX1Fw3ISmLV2cm7R2FZ45IkeEEAujyKquoJ6lSbUHOr9TcNu7MGnrzuZ9oMdUPBNGsqouRZGdFVlsPmBtQa1AoFAoFBy4YowRsXIIVrXsfI2oPGaf0tzXr9Lc5WWLb7UOxq7zPt7EplTZVQZER48Iz0t20AH+1BBF9D8oNLUSbcibkElOvtzjKzcYYRrmJfhBywRW9mV+tBd+pPprmeZbd14dhBoTQJHpRnYn10gZPmDQwrjKGI8XPw+w0E2/9OchyMHOh3hhm5aPV7ShmkVJNdRkGOCkrkLAgeHW3lQSchwnKck+5sS9iGbb4mbju2sjyKs8rMQcjGpK2PU4391BHufTM+y2v3JVj1ouI2OMmMeTSB5ljGSKALgBT5g0GZxm3PyXOfT8Sy6c8fGQz946Ejz4jsiIGXJE7RJItGQT49aD3lAoFAoFAoFAoFAoFBH3H7/a7TdvHLv3XC97Y2yyv5/Lb30ElAoFAoFBy4cowjOMhBxY9QDboaDwnFcdPHwmzq6+hsa/1n6bYj2ORkjde5OwNm9S1lkDG2FmbHxsKBBoxmbVPB8ZsaKx6OynLF9eTXErtEAiNmq96TPrmuXn160Hzivp1YU+mIjovGuzx88HMsUYOe5rp8EzEXFmuFVvDwFA1eN+o93U24Zkkh5DhdKbjuK2H+Dv7D3Xvoxt4xrGob2lqCgOJ2f4XmF1kkXYm4xoJONh4vZ0w86kWcu7yLLL1Iuhu3jQbPNcJoa/E6CQ67JNCTsem9BLyEGxN2grepjjUksfJ3YEH7KD5Jrc3tO8uppnj9+XgDDCiDGKHZL3WJXFlUjy9lBXg0YzNqng+M2NFY9HZTli+vJriV2iARGzVe9Jn1zXLz69aCTgeFfjZfpOaDTkgmbSlXlZcGD5GGMqkzEXFmuFVvDwFB7mgUCgUCgUCgUCgUCgUCgUCgUCgUCgUCgUCgUCgUCgUHol4zTCBDHkbdWJN6DF9OPW+mv8AD3ML+dr2oNpuM0yhQR4m3RgTegxtKBZ9pIX+XrlbzsL0Gttcdq+nkKJg6qWVhfyF6DN43Wj2ZysvVFXLH2m4FBd5DQ1k1mliTB0sel+oJtQUuN1o9mcrL1RVyx9puBQXeQ0NZNZpYkwdLHpfqCbUFbitSHYMjzDIJYBfK5vQS8ppQQxLNCuByxIHgQQaBxelBNE00y5nLEA+AAAoLf8AF6nd7mHw2+S5tf20FxWVlDqQVIuD5WoPO9xP5Lu3/T7t8vK2XjQeiZlVS7EBQLk+VqDz3HOq7yMxsDkAT7wbUG5tuqa0rMbDBh/UigyOGdV2WVjYsll95uKDR5N1XSkDGxawUe03FBncM6rssrGxZLL7zcUGjybqulIGNi1go9puKCpwjqO8hPxHEge4XoJuZdRrKhPxFwQPcAaBwzqdZkB+IOSR7iBQX+4mfayHctlj52oPLDu4G2Xb87XxoOKDs93AXy7fle+NBxQdv3bDuZY/8cr2/peg4F79PHyoO5O7cd3K/llf8aDgXv08fKg7k7tx3cr+WV/xoOVyuMb5eVvGg+vnl+pfL/a9/wDNATPL9O+X+t7/AOKB+pn59z+t70H/2Q==';

	public static function getThumbSizes()
	{
		$sizes = array();

		foreach (Core::$def['image_sizes'] as $size) {
			if (preg_match('/^\d+x\d+$/', $size)) {
				$sizes[] = $size;
			}
		}

		return $sizes;
	}

	public static function getFolders($dir = '')
	{
		$path = FM_ROOT . $dir;

		if (!is_dir($path)) {
			return null;
		}

		$files = scandir($path);
		$dirs = array();

		foreach ($files as $file) {
			if (substr($file, 0, 1) == '.' || substr($file, 0, 1) == '_') {
				continue;
			}

			if (is_dir($path . '/' . $file) && substr($file, -6) != '.video') {
				$dirs[$file] = self::getFolders($dir . '/' . $file);
			}
		}

		return $dirs;
	}

	public static function getFiles($_dir)
	{
		$dir = FM_ROOT . '/' . $_dir;

		if (!is_dir($dir)) {
			return null;
		}

		$files = scandir($dir);
		$_files = array();

		foreach ($files as $file) {
			if (substr($file, 0, 1) == '.') {
				continue;
			}

			if (is_file($dir . '/' . $file) || substr($file, -6) == '.video') {
				$_files[] = $file;
			}
		}

		return $_files;
	}

	public static function searchFiles($search_term, $_dir = '/')
	{
		$dir = FM_ROOT . $_dir;
		$rootlen = strlen(FM_ROOT);
		$dirlen = strlen($dir);

		if (!is_dir($dir)) {
			return null;
		}

		$files = array();
		foreach (glob($dir . $search_term) as $file) {
			if (!is_dir($file)) {
				$files[] = substr($file, $rootlen + 1);
			}
		}

		foreach (glob($dir . '*', GLOB_ONLYDIR) as $folder) {
			if (preg_match('/\/?_\d+x\d+|\.video$/', $folder)) {
				continue;
			}

			$_files = self::searchFiles($search_term, $_dir . substr($folder, $dirlen) . '/');

			if (is_array($_files)) {
				$files = array_merge($files, $_files);
			}
		}

		if (count($files) > 400) {
			$files = array_slice($files, 0, 400);
		}

		return $files;
	}

	public static function globistr($string = '', $mbEncoding = '')
	{
		//returns a case insensitive Version of the searchPattern for glob();
		// e.g.: globistr('./*.jpg') => './*.[jJ][pP][gG]'
		// e.g.: glob(dirname(__FILE__).'/'.globistr('*.jpg')) => '/.../*.[jJ][pP][gG]'
		// known problems: globistr('./[abc].jpg') => FALSE:'./[[aA][bB][cC]].[jJ][pP][gG]'
		//(Problem: existing Pattern[abc] will be overwritten)
		// known solution: './[abc].'.globistr('jpg') => RIGHT: './[abc].[jJ][pP][gG]'
		//(Solution: globistr() only caseSensitive Part, not everything)
		$return = "";
		if ($mbEncoding !== '') { //multiByte Version
			$string = mb_convert_case($string, MB_CASE_LOWER, $mbEncoding);
		} else { //standard Version (not multiByte,default)
			$string = strtolower($string);
		}
		$mystrlen = strlen($string);
		for ($i = 0; $i < $mystrlen; $i++) {
			if ($mbEncoding !== '') {//multiByte Version
				$myChar = mb_substr($string, $i, 1, $mbEncoding);
				//$myUpperChar = mb_strtoupper($myChar,$mbEncoding);
				$myUpperChar = mb_convert_case($myChar, MB_CASE_UPPER, $mbEncoding);
			} else {
				$myChar = substr($string, $i, 1);
				$myUpperChar = strtoupper($myChar);
			}
			if ($myUpperChar !== $myChar) { //there is a lower- and upperChar, / Char is case sentitive
				$return .= '[' . $myChar . $myUpperChar . ']'; //adding both Versions : [xX]
			} else {//only one case Version / Char is case insentitive
				$return .= $myChar; //adding '1','.','*',...
			}
		}
		return $return;
	}

	public static function getInfo($path)
	{
		$file = FM_ROOT . '/' . $path;

		if (!file_exists($file)) {
			return array();
		}

		if (substr($file, -6) == '.video' && file_exists($file . '/info.json')) {
			$info = json_decode(file_get_contents($file . '/info.json'));

			if (file_exists($file . '/video.' . $info->ext)) {
				$stat = stat($file . '/video.' . $info->ext);
			} else {
				$stat = stat($file . '/source.' . $info->source_ext);
			}

			$stat['video'] = true;
			$stat['mime'] = $info->mime;
			$stat['dimensions'] = $info->width . 'x' . $info->height;
			$stat['duration'] = $info->duration;
		} else {
			$stat = stat($file);

			$stat['mime'] = file::mime($file);

			if (self::isImage($file)) {
				$image = new Image($file);
				$stat['dimensions'] = $image->width . 'x' . $image->height;
			}
		}

		return $stat;
	}

	public static function renameFile($path, $new_name)
	{
		$sizes = self::getThumbSizes();
		$root = FM_ROOT;
		$path_full = $root . '/' . $path;

		if (!in_array(self::$extra_thumb_size, $sizes)) {
			array_unshift($sizes, self::$extra_thumb_size);
		}

		$pathinfo = pathinfo($path_full);
		$new_file = $pathinfo['dirname'] . '/' . $new_name;

		if (file_exists($path_full)) {
			if (FS::rename($path_full, $new_file)) {
				$pathinfo = pathinfo($path_full);

				foreach ($sizes as $size) {
					$thumb = $pathinfo['dirname'] . '/_' . $size . '/' . $pathinfo['basename'];

					if (file_exists($thumb)) {
						FS::rename($thumb, $pathinfo['dirname'] . '/_' . $size . '/' . $new_name);
					}
				}

				return true;
			}
		}

		return false;
	}

	public static function renameFolder($path, $new_name)
	{
		$root = FM_ROOT;

		$path_full = $root . '/' . $path;

		if (is_dir($path_full)) {
			$pathinfo = pathinfo($path_full);
			FS::rename($path_full, $pathinfo['dirname'] . '/' . $new_name);

			return true;
		}

		return false;
	}

	public static function createFolder($path)
	{
		$root = FM_ROOT;

		if (!is_dir($root . '/' . $path)) {

			self::mkDir($root . '/' . $path);
			return true;
		}

		return false;
	}

	public static function download($file)
	{
		$filename = FM_ROOT . '/' . $file;

		if (file_exists($filename)) {

			$filepath = str_replace('\\', '/', realpath($filename));
			$filesize = filesize($filepath);
			$filename = substr(strrchr('/' . $filepath, '/'), 1);
			$extension = strtolower(substr(strrchr($filepath, '.'), 1));

			$mime = array('application/octet-stream');

			if (isSet(File::$mimes[$extension])) {
				$mime = File::$mimes[$extension];
			}

			// Generate the server headers
			header('Content-Type: ' . $mime[0]);
			header('Content-Disposition: attachment; filename="' . (empty($nicename) ? $filename : $nicename) . '"');
			header('Content-Transfer-Encoding: binary');
			header('Content-Length: ' . sprintf('%d', $filesize));

			// More caching prevention
			header('Expires: 0');

			header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
			header('Pragma: public');

			if (isset($filepath)) {
				$handle = fopen($filepath, 'rb');
				fpassthru($handle);
				fclose($handle);
			} else {
				echo $data;
			}
		}
	}

	public static function delete($path)
	{
		$sizes = self::getThumbSizes();
		$root = FM_ROOT;
		$path_full = $root . '/' . $path;

		if (!in_array(self::$extra_thumb_size, $sizes)) {
			array_unshift($sizes, self::$extra_thumb_size);
		}

		$pathinfo = pathinfo($path_full);

		if (substr($path_full, -6) == '.video' && is_dir($path_full)) {
			return self::deleteFolder($path);
		}

		if (file_exists($path_full)) {
			if (FS::remove($path_full)) {

				foreach ($sizes as $size) {
					$thumb = $pathinfo['dirname'] . '/_' . $size . '/' . $pathinfo['basename'];

					if (file_exists($thumb)) {
						FS::remove($thumb);
					}
				}

				return true;
			}
		}

		return false;
	}

	public static function deleteFolder($folder)
	{
		$root = FM_ROOT;
		$path = $root . '/' . $folder;

		if (is_dir($path)) {
			$files = scandir($path);

			foreach ($files as $file) {
				if ($file == '.' || $file == '..') {
					continue;
				}

				$_path = $path . '/' . $file;

				if (is_dir($_path)) {
					self::deleteFolder($folder . '/' . $file);
				} else {
					self::delete($folder . '/' . $file);
				}
			}

			self::removeAll($path);

			return true;
		}

		return false;
	}

	public static function upload($folder, $watermark = false, $video_convert = false)
	{

		if (isSet($_FILES[self::$upload_file_name])) {
			$postfix = '';
			$path = FM_ROOT . '/' . $folder;
			$filename = $_FILES[self::$upload_file_name]['name'];

			$ext = substr(strrchr($filename, '.'), 1);
			$filename = substr($filename, 0, strrpos($filename, '.'));

			if ($video_convert && preg_match('/^video\//', file::mime($_FILES[self::$upload_file_name]['tmp_name'], $ext))) {

				if (is_dir($path . '/' . $filename . $postfix . '.video')) {
					$dirs = glob($path . '/' . $filename . '*.video');
					$postfix = '_' . (count($dirs) + 1);
				}

				$folder = $path . '/' . $filename . $postfix . '.video';
				self::mkDir($folder);

				$file = $folder . '/source.' . $ext;

				file_put_contents($folder . '/info.json', json_encode(array('ext' => $ext, 'source_ext' => $ext, 'mime' => 'awaiting conversion', 'width' => '', 'height' => '', 'duration' => '')));
				file_put_contents($folder . '/preview.jpg', base64_decode(self::$default_preview));
			} else {
				if (file_exists($path . '/' . $filename . '.' . $ext)) {
					$files = glob($path . '/' . $filename . '*.' . $ext);
					$postfix = '_' . (count($files) + 1);
				}

				$file = $path . '/' . $filename . $postfix . '.' . $ext;
			}

			move_uploaded_file($_FILES[self::$upload_file_name]['tmp_name'], $file);

			if ($watermark && self::isImage($file)) {
				$wm = Core::config('files', 'watermark');

				if (!empty($wm) && $wm['enabled']) {
					$img = new Image($path . '/' . $filename . $postfix . '.' . $ext);
					$img->watermark($wm);
					$img->save();
				}
			}
		}
	}

	public static function isImage($path)
	{
		if (preg_match('/^(gif|png|jpe?g)$/i', strtolower(substr(strrchr($path, '.'), 1)))) {
			return true;
		}

		return false;
	}

	public static function removeAll($dir)
	{
		$files = scandir($dir);
		foreach ($files as $file) {
			if ($file == '.' || $file == '..')
				continue;

			if (is_dir($dir . '/' . $file)) {
				self::removeAll($dir . '/' . $file);
			} else {
				FS::remove($dir . '/' . $file);
			}
		}

		FS::rmdir($dir);
	}

	public static function mkDir($dir)
	{
		if (!is_dir($dir)) {
			FS::mkdir($dir, true);
		}

		if (!is_dir($dir)) {
			return false;
		}

		FS::writable($dir);

		return true;
	}

	public static function chown_chmod($file)
	{

	}

	public static function getUID()
	{
		if (self::$user_uid) {
			return self::$user_uid;
		}

		self::$user_uid = Core::config('files', 'chown_uid');

		if (is_null(self::$user_uid) || !self::$user_uid) {
			$stat = stat(FM_ROOT);

			self::$user_uid = $stat['uid'];

			return $stat['uid'];
		}
	}

}