<?php

defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */
define('FPDF_FONTPATH', APPROOT . 'vendor/fpdf/font/');
require(APPROOT . 'vendor/fpdf/fpdf.php');

class WEBMEX_FPDF extends FPDF
{

	public $footer_text;
	public $date;

	public function Footer()
	{
		$this->SetY(-15);
		$this->SetFont('Freemono', '', 7);

		$text = preg_replace('/\{date\}/', fdate($this->date, true), $this->footer_text);

		$this->Cell(129, 10, iconv('utf-8', 'windows-1250', $text));
		$this->Cell(60, 10, iconv('utf-8', 'windows-1250', __('invoice_page') . ' ' . $this->PageNo() . '/{nb}'), 0, 0, 'R');
	}

}

class Invoice
{

	public $pdf;
	protected $fontsize = 9;
	protected $vat_subtotals = array();
	public $data = array(
		'invoice_num' => '',
		'vat_payer' => 0,
		'issuer' => '',
		'account_num' => '',
		'customer' => '',
		'shipping_address' => '',
		'payment' => '',
		'delivery' => '',
		'symbol' => '',
		'date_issue' => '',
		'date_due' => '',
		'date_vat' => '',
		'items' => array(),
		'footer' => '',
		'note' => '',
		'price_decimals' => 0,
		'total_round' => 0,
		'voucher_id' => 0,
		'logo' => '',
		'signature' => '',
		'type' => 'invoice',
	);

	public function __construct()
	{
		$this->pdf = new WEBMEX_FPDF();

		$this->pdf->AliasNbPages();

		$this->pdf->AddFont('Freemono', '', 'freemono.php');
		$this->pdf->AddFont('Freemono', 'B', 'freemonob.php');

		$this->pdf->AddPage();

		$this->data['price_decimals'] = (int) Core::$def['price_formats'][Core::config('price_format')][0];
		$this->data['total_round'] = (int) Core::config('order_round_decimals');

		Core::$config['price_format'] = (int) Core::config('invoice_price_format');
	}

	public function __set($key, $value)
	{
		$this->data[$key] = html_entity_decode($value);
	}

	public function add($sku, $name, $quantity, $price, $vat = null)
	{
		$this->data['items'][] = array($sku, html_entity_decode($name), $quantity, $price, $vat);
	}

	public function iconv($text)
	{
		return iconv('utf-8', 'windows-1250', $text);
	}

	public function cell($w, $h, $text, $border = 0, $ln = 0, $align = 'L', $fill = false)
	{
		$this->pdf->Cell($w, $h, $this->iconv($text), $border, $ln, $align, $fill);
	}

	public function multicell($w, $h, $text, $border = 0, $align = 'L')
	{
		$text = preg_replace('/\t/', '    ', $text);

		$this->pdf->MultiCell($w, $h, $this->iconv($text), $border, $align);
	}

	public function moveX($change)
	{
		$this->pdf->SetX($this->pdf->GetX() + $change);
	}

	public function moveY($change)
	{
		$this->pdf->SetY($this->pdf->GetY() + $change);
	}

	public function hline($ln = 2)
	{
		$this->moveY($ln);
		$this->pdf->Line($this->pdf->GetX(), $this->pdf->GetY(), $this->pdf->GetX() + 189, $this->pdf->GetY());
		$this->moveY($ln);
	}

	public function save($name = null, $dest = 'I')
	{
		$this->render();
		$this->pdf->Output($name, $dest);
	}

	private function render()
	{
		$this->pdf->footer_text = $this->data['footer'];
		$this->pdf->date = $this->data['date_create'];

		$this->pdf->SetDrawColor(180, 180, 180);

		$this->render_logo();


		$this->render_header();

		$x = $this->pdf->GetX();
		$y = $this->pdf->GetY();

		$this->render_issuer();

		$_y = $this->pdf->GetY();

		$this->pdf->SetY($y);

		$this->render_customer();

		$this->pdf->SetY($_y > $this->pdf->GetY() ? $_y : $this->pdf->GetY());

		$this->pdf->Ln(5);
		$this->hline();

		$y = $this->pdf->GetY();

		$this->render_info();

		$this->pdf->SetY($y);

		$this->render_dates();

		$this->pdf->Ln(5);

		$this->hline();

		$this->pdf->Ln(5);

		$this->render_tableheader();


		$subtotal = 0;
		$total = 0;
		
		// one row = 36 characters
		$rowChar = 36;
		
		// max rows on the first page
		$maxRowsFirst = 22;
		
		// max rows on the default page
		$maxRowsDefault = 40;
		
		// rows array
		$rows = array();
		
		$i = 1;
		$rowCounter = 0;
		
		foreach ($this->data['items'] as $item){
			$rows[$i][] = $item;
			$rowCounter += ceil(mb_strlen($item[1])/$rowChar);
			
			if (($i == 1 && $rowCounter >= $maxRowsFirst) || ($i != 1 && $rowCounter >= $maxRowsDefault)){
				$rowCounter = 0;
				$i++;
			}
		}
		
		
		foreach ($rows as $pNumber => $page) {
			foreach ($page as $item){
				$this->render_tablerow($item);
			
					$price = round((float) $item[3], $this->data['price_decimals']);

					if ($this->data['vat_payer']) {

						$total += price_vat($price * (float) $item[2], (float) $item[4])->price;
						$subtotal += price_unvat($price * (float) $item[2], (float) $item[4])->price;

					} else {
						$total += $price * (float) $item[2];
						$subtotal += $price * (float) $item[2];
					}
			}
			
			echo $pNumber . '!=' . count($rows) .'<br><br>';
			
			if (($pNumber) != count($rows)){
				$this->pdf->AddPage();
			}
		}

		

		if ($voucher = Core::$db->voucher[(int) $this->data['voucher_id']]) {
			$discount = estPrice($voucher['value'], $total);

			$this->render_tablerow(array(
				'',
				__('voucher') . ' (' . $voucher['code'] . ')',
				1,
				($discount * -1),
				0
			));

			$subtotal -= $discount;
		}

		$this->render_summary($subtotal);

		$this->render_signature();
	}

	private function render_logo()
	{
		if (!empty($this->data['logo'])) {
			$this->pdf->Image($this->data['logo'], 10, 5, null, 15);
			$this->pdf->Ln(5);
		}
	}

	private function render_signature()
	{
		if (!empty($this->data['signature'])) {
			$this->pdf->Ln(10);
			$this->pdf->Image($this->data['signature'], 10, $this->pdf->GetY(), 60);
		}
	}

	private function render_header()
	{
		$this->pdf->SetFont('Freemono', 'B', $this->fontsize + 2);

		if ($this->data['type'] == 'proforma') {
			$title = __('invoice_title_proforma');
		} else {
			$title = $this->data['vat_payer'] ? __('invoice_title_vat') : __('invoice_title');
		}

		$this->cell(0, 10, $title . ': ' . $this->data['invoice_num'], 'B', 0, 'R');

		$this->pdf->Ln(10);
	}

	private function render_issuer()
	{
		$this->pdf->SetFont('Freemono', '', $this->fontsize);
		$this->cell(0, 10, __('invoice_issuer') . ':');
		$this->pdf->Ln(10);

		$this->moveX(5);

		$this->pdf->SetFont('Freemono', '', $this->fontsize + 1);
		$this->multicell(
			97, 
			$this->fontsize / 2 + 1, 
			$this->data['issuer']
			. ($this->data['account_num']? "\n\nBankovní účet: {$this->data[account_num]}": '')
		);
	}

	private function render_customer()
	{
		$this->moveX(110);

		$this->pdf->SetFont('Freemono', '', $this->fontsize);
		$this->cell(0, 10, __('invoice_customer') . ':');
		$this->pdf->Ln(10);

		$this->pdf->Ln(($this->fontsize / 2 + 1) * 2);
		$this->moveX(115);

		$this->pdf->SetFont('Freemono', '', $this->fontsize + 1);
		$this->multicell(75, $this->fontsize / 2 + 1, $this->data['customer']);

		if (!empty($this->data['shipping_address'])) {
			$this->pdf->Ln(($this->fontsize / 2 + 1));
			$this->moveX(110);

			$this->pdf->SetFont('Freemono', 'B', $this->fontsize);
			$this->cell(0, 10, __('shipping_address') . ':');
			$this->pdf->Ln(5);

			$this->pdf->SetFont('Freemono', 'B', $this->fontsize + 1);
			$this->pdf->Ln(5);

			$this->moveX(115);
			$this->multicell(75, $this->fontsize / 2 + 1, $this->data['shipping_address']);
		}
	}

	private function render_info()
	{
		$this->pdf->SetFont('Freemono', '', $this->fontsize);

		$this->cell(50, $this->fontsize / 2 + 1, __('invoice_delivery') . ':');
		$this->cell(52, $this->fontsize / 2 + 1, $this->data['delivery']);

		$this->pdf->Ln($this->fontsize / 2 + 1);

		$this->cell(50, $this->fontsize / 2 + 1, __('invoice_payment') . ':');
		$this->cell(52, $this->fontsize / 2 + 1, $this->data['payment']);



		$this->pdf->Ln($this->fontsize / 2 + 1);
		$this->pdf->SetFont('Freemono', 'B');
		$this->cell(50, $this->fontsize / 2 + 1, __('invoice_symbol') . ':');
		$this->cell(52, $this->fontsize / 2 + 1, $this->data['symbol'] ? $this->data['symbol'] : $this->data['invoice_num']);
	}

	private function render_dates()
	{
		$this->moveX(110);

		$this->pdf->SetFont('Freemono', '', $this->fontsize);

		$this->cell(50, $this->fontsize / 2 + 1, __('invoice_issue_date') . ':');
		$this->cell(30, $this->fontsize / 2 + 1, date(__('invoice_date_format'), (int) $this->data['date_issue']), 0, 0, 'R');

		$this->pdf->Ln($this->fontsize / 2 + 1);
		$this->moveX(110);

		if ($this->data['vat_payer'] && (int) $this->data['date_vat'] && $this->data['type'] != 'proforma') {
			$this->cell(50, $this->fontsize / 2 + 1, __('invoice_vat_date') . ':');
			$this->cell(30, $this->fontsize / 2 + 1, date(__('invoice_date_format'), (int) $this->data['date_vat']), 0, 0, 'R');

			$this->pdf->Ln($this->fontsize / 2 + 1);
			$this->moveX(110);
		}

		$this->pdf->SetFont('Freemono', 'B');
		$this->cell(50, $this->fontsize / 2 + 1, __('invoice_due_date') . ':');
		$this->cell(30, $this->fontsize / 2 + 1, date(__('invoice_date_format'), (int) $this->data['date_due']), 0, 0, 'R');

		if (!$this->data['vat_payer'] || !(int) $this->data['date_vat'] || $this->data['type'] == 'proforma') {
			$this->pdf->Ln($this->fontsize / 2 + 1);
			$this->moveX(110);
		}
	}

	private function render_tableheader()
	{
		$this->pdf->SetFont('Freemono', '', $this->fontsize);

		$this->pdf->SetFillColor(220, 220, 220);

		if ($this->data['vat_payer']) {
			$this->cell(70, $this->fontsize / 2 + 1, __('invoice_name'), 0, 0, 'L', true);
			$this->cell(25, $this->fontsize / 2 + 1, __('invoice_quantity'), 0, 0, 'R', true);
			
			$this->cell(16, $this->fontsize / 2 + 1, 'Cena ks', 0, 0, 'R', true);
			
			$this->cell(20, $this->fontsize / 2 + 1, 'Bez DPH', 0, 0, 'R', true);
			$this->cell(16, $this->fontsize / 2 + 1, __('invoice_vat') . ' %', 0, 0, 'R', true);
			$this->cell(10, $this->fontsize / 2 + 1, 'DPH', 0, 0, 'R', true);
		} else {
			$this->cell(94, $this->fontsize / 2 + 1, __('invoice_name'), 0, 0, 'L', true);
			$this->cell(20, $this->fontsize / 2 + 1, __('invoice_quantity'), 0, 0, 'R', true);
			$this->cell(25, $this->fontsize / 2 + 1, __('invoice_price'), 0, 0, 'R', true);
		}

		$this->cell(32, $this->fontsize / 2 + 1, 'Cena vč. DPH', 0, 0, 'R', true);

		$this->pdf->Ln(7);
	}

	private function render_tablerow($item)
	{
		$this->pdf->Ln(0);

		$y = $this->pdf->GetY();
		$x = $this->pdf->GetX();
		$vat = null;

		$fn = Core::config('vat_mode') == 'exclude' ? 'price_vat' : 'price_unvat';

		$price = round((float) $item[3], $this->data['price_decimals']);

		$fixVat = sprintf('%.2f', $price) * 0.21;
		
		if ($this->data['vat_payer']) {
			$this->multicell(80, $this->fontsize / 1.8, $item[1]);
			$_y = $this->pdf->GetY();

			$this->pdf->SetY($y);
			$this->pdf->SetX($x+78);

			$vat = round((float) $item[4], 2);

			$this->vat_subtotals[$vat] += price_unvat($price * (float) $item[2], $vat)->price;

			$this->cell(18, $this->fontsize / 1.8, $item[2], 0, 0, 'L');
			
			//1 ks price
			$this->cell(20, $this->fontsize / 1.8, fprice(price_unvat(sprintf('%.2f', $price))->price), 0, 0, 'L');
			
			$this->cell(20, $this->fontsize / 1.8, fprice(price_unvat(sprintf('%.2f', $price) * (float) $item[2], $vat)->price), 0, 0, 'L');
			$this->cell(13, $this->fontsize / 1.8, $vat, 0, 0, 'L');
			$this->cell(20, $this->fontsize / 1.8, fprice($fixVat * (float) $item[2]), 0, 0, 'L');
		} else {
			$this->multicell(94, $this->fontsize / 1.8, $item[1]);
			$_y = $this->pdf->GetY();

			$this->pdf->SetY($y);
			$this->pdf->SetX($x + 94);

			$this->cell(20, $this->fontsize / 1.8, $item[2], 0, 0, 'R');
			$this->cell(25, $this->fontsize / 1.8, fprice($fn($price, $vat)->price), 0, 0, 'R');
		}

		$this->cell(20, $this->fontsize / 1.8, fprice((sprintf('%.2f', $price) + $fixVat) * (float) $item[2]), 0, 0, 'R');

		$this->pdf->SetY($_y);

		$this->hline(1);
	}

	private function render_summary($subtotal)
	{
		$this->pdf->Ln(5);

		$this->pdf->SetFont('Freemono', '', $this->fontsize);

		if (!empty($this->data['note'])) {
			$y = $this->pdf->GetY();
			$this->multicell(100, $this->fontsize / 2 + 1, $this->data['note']);
			$this->pdf->SetY($y);
		}

		$this->moveX(122);

		$total = $subtotal;

		if ($this->data['vat_payer']) {
			$this->cell(37, $this->fontsize / 2 + 1, __('invoice_total_excl_vat') . ':');
			$this->cell(30, $this->fontsize / 2 + 1, fprice($subtotal), 0, 0, 'R');

			$this->pdf->Ln($this->fontsize / 2 + 1);
			$this->moveX(122);

			ksort($this->vat_subtotals);

			foreach ($this->vat_subtotals as $vat => $vat_subtotal) {
				$vat_value = ($vat_subtotal * ($vat / 100 + 1)) - $vat_subtotal;

				$this->cell(37, $this->fontsize / 2 + 1, __('invoice_vat') . ' ' . fprice($vat) . '%:');
				$this->cell(30, $this->fontsize / 2 + 1, fprice($vat_value), 0, 0, 'R');
				$this->pdf->Ln($this->fontsize / 2 + 1);
				$this->moveX(122);

				$total += $vat_value;
			}
		}

		//$rounded = round($total, $this->data['total_round']);
		$rounded = round($total, 0);
		$diff = round($rounded - round($total, 2), 2);

		if ($diff != 0) {
			$this->cell(37, $this->fontsize / 2 + 1, __('invoice_rounding') . ':');
			$this->cell(30, $this->fontsize / 2 + 1, ($diff > 0 ? '+' : '') . fprice($diff), 0, 0, 'R');
			$this->pdf->Ln($this->fontsize / 2 + 1);
			$this->moveX(122);
		}

		$this->pdf->SetFont('Freemono', 'B');
		$this->cell(37, $this->fontsize / 2 + 1, __('invoice_total') . ':');
		$this->cell(30, $this->fontsize / 2 + 1, preg_replace('/\&nbsp;/', ' ', price($rounded)), 1, 0, 'R');

		if ($this->data['status'] == 2) {
			$this->pdf->SetFillColor(220, 220, 220);
			$this->pdf->Ln(10);
			$this->moveX(122);
			$this->cell(67, $this->fontsize / 2 + 1, __('invoice_dont_pay'), 0, 0, 'L', true);
		}
	}

	public static function inc()
	{
		preg_match('/^([^\d]*)?([\d]+)$/', Core::config('invoice_num'), $m);

		if ($m) {
			$in = $m[1] . ((int) $m[2] + 1);

			Core::$db->config()->where('name', 'invoice_num')->update(array('value' => $in));
		}
	}

	public static function createFromOrder($order, $invoice_num = null, $date_issue = null, $note = '')
	{
		if (!Core::$is_premium) {
			return false;
		}

		if (is_array($order) && !empty($order['id'])) {
			$order = Core::$db->order[(int) $order['id']];
		} else if (is_numeric($order)) {
			$order = Core::$db->order[(int) $order];
		}

		if (!$order) {
			return false;
		}

		$address = $order['street'] . "\n"
				. $order['zip'] . ' ' . $order['city'];

		if (!$date_issue) {
			$date_issue = time();
		}

		if (!$invoice_num) {
			$invoice_num = Core::config('invoice_num');
		}

		if ($invoice_num == Core::config('invoice_num')) {
			self::inc();
		}

		$shipping_addr = null;

		if ($order['ship_street']) {
			$shipping_addr = ($order['ship_company'] ? $order['ship_company'] . "\n" : '')
					. $order['ship_first_name'] . ' ' . $order['ship_last_name'] . "\n"
					. $order['ship_street'] . "\n"
					. $order['ship_zip'] . ' ' . $order['ship_city'] . "\n"
					. $order['ship_country'] . "\n";
		}

		$invoice_id = Core::$db->invoice(prepare_data('invoice', array(
					'invoice_num' => $invoice_num,
					'order_id' => $order['id'],
					'customer_id' => $order['customer_id'],
					'customer' => '',
					'customer_name' => $order['company'] ? $order['company'] : $order['first_name'] . ' ' . $order['last_name'],
					'company_id' => $order['company_id'],
					'company_vat' => $order['company_vat'],
					'address' => $address,
					'payment' => $order->payment['name'],
					'delivery' => $order->delivery['name'],
					'symbol' => (Core::config('invoice_symbol') == 'invoice_num' ? $invoice_num : $order['id']),
					'date_issue' => $date_issue,
					'date_due' => $date_issue + ((int) Core::config('invoice_due') * 86400),
					'date_vat' => $date_issue,
					'vat_payer' => $order['vat_payer'],
					'voucher_id' => $order['voucher_id'],
					'status' => (int) $order['payment_realized'] ? 2 : 1,
					'date_create' => time(),
					'note' => $note,
					'shipping_address' => $shipping_addr
				)));

		if ($invoice_id) {
			$products = $order->order_products();

			if (Core::$db_inst->_type == 'sqlite') {
				$products->order('rowid ASC');
			}

			foreach ($products as $product) {
				Core::$db->invoice_items(array(
					'invoice_id' => $invoice_id,
					'sku' => $product['sku'],
					'name' => $product['name'],
					'quantity' => $product['quantity'],
					'price' => $product['price'],
					'vat' => $product['vat']
				));
			}
		}

		return $invoice_id;
	}

}