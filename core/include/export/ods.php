<?php

defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */
class Export_Ods
{

	public $head;
	public $ods;
	public $line = 0;

	public function __construct()
	{
		require_once(APPROOT . 'vendor/odsphp/ods.php');

		$this->ods = newOds();
	}

	public function addHeader($columns)
	{
		if (!in_array('id', $columns)) {
			array_unshift($columns, 'id');
		}

		if (in_array('attributes', $columns)) {
			array_unshift($columns, 'attribute_product_id');
		}

		$this->createLine($columns);

		$this->head = true;
	}

	public function addProduct($product, $columns = null)
	{
		if ($columns === null) {
			$columns = array_keys($product->as_array());

			$columns[] = 'attributes';
			$columns[] = 'files';
			$columns[] = 'pages';
		}

		if (!in_array('id', $columns)) {
			array_unshift($columns, 'id');
		}

		if (in_array('attributes', $columns)) {
			array_unshift($columns, 'attribute_product_id');
		}

		if (!$this->head) {
			$head = $columns;

			if (array_search('attributes', $head) !== false) {
				unset($head[array_search('attributes', $head)]);
			}

			$this->createLine($head);

			$this->head = true;
		}

		$data = array();
		foreach ($columns as $i => $col) {

			if ($col == 'files') {
				$files = '';
				foreach ($product->product_files() as $file) {
					$files .= $file['filename'] . ',';
				}

				$data[$i] = trim($files, ',');
			} else if ($col == 'pages' || $col == 'categories') {
				$pages = '';
				foreach ($product->product_pages() as $page) {
					if ($page->page['id'] && $page->page['menu'] != 3) {
						$pages .= join(' > ', $page->page->model->getFullPath()) . "\n";
					}
				}

				$data[$i] = trim($pages);
			} else if ($col == 'manufacturers') {
				$pages = '';
				foreach ($product->product_pages() as $page) {
					if ($page->page['id'] && $page->page['menu'] == 3) {
						$pages .= join(' > ', $page->page->model->getFullPath()) . "\n";
					}
				}

				$data[$i] = trim($pages);
			} else if ($col == 'attributes') {

			} else if ($col == 'availability') {
				$data[$i] = $product->availability['name'];
			} else if ($col == 'availability_days') {
				$data[$i] = $product->availability['days'];
			} else if ($product[$col] !== null) {
				$data[$i] = $product[$col];
			} else {
				$data[$i] = '';
			}
		}

		$this->createLine($data);

		if (in_array('attributes', $columns)) {
			foreach ($product->product_attributes() as $attr) {
				$data = array();

				foreach ($columns as $i => $col) {

					if ($col == 'files') {
						$files = '';
						foreach ($product->product_files() as $file) {
							$files .= $file['filename'] . ',';
						}

						$data[$i] = trim($files, ',');
					} else if ($col == 'attribute_product_id') {
						$data[$i] = $attr['product_id'];
					} else if ($col == 'name') {
						$data[$i] = $attr['name'] . ': ' . $attr['value'];
					} else if ($col == 'attributes') {

					} else if ($attr[$col]) {
						$data[$i] = $attr[$col];
					} else {
						$data[$i] = '';
					}
				}

				$this->createLine($data);
			}
		}
	}

	public function build()
	{
		$f = DOCROOT . 'etc/tmp/' . uniqid() . '.ods';

		saveOds($this->ods, $f);

		$tmp = file_get_contents($f);

		FS::remove($f);

		return $tmp;
	}

	public function createLine($data)
	{
		$col = 0;
		foreach ($data as $i => $v) {
			$this->ods->addCell(0, $this->line, $col, xmlentities($v), (is_numeric($v) && $v !== '' && $v !== null) ? 'float' : 'string');

			$col++;
		}

		$this->line++;
	}

}