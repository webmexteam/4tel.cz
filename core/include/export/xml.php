<?php

defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */
class Export_Xml
{

	public $dom;
	public $root;

	public function __construct()
	{
		$this->dom = new DOMDocument('1.0', 'utf-8');
		$this->dom->formatOutput = true;

		$this->root = $this->dom->createElement('datapack');
		$this->dom->appendChild($this->root);
	}

	public function addProduct($product, $columns = null, $is_attribute = false)
	{
		$el = $this->dom->createElement('product');

		if ($columns === null) {
			$columns = array_keys($product->as_array());

			$columns[] = 'attributes';
			$columns[] = 'discounts';
			$columns[] = 'files';
			$columns[] = 'pages';
			$columns[] = 'features';
		}

		if (!in_array('id', $columns)) {
			array_unshift($columns, 'id');
		}

		foreach ($columns as $col) {
			if (!$is_attribute && $col == 'files') {

				foreach ($product->product_files() as $file) {
					$el_file = $this->dom->createElement('file', $file['filename']);

					$el_file->setAttribute('description', $file['description']);
					$el_file->setAttribute('position', $file['position']);
					$el_file->setAttribute('size', $file['size']);
					$el_file->setAttribute('is_image', $file['is_image']);
					$el_file->setAttribute('align', $file['align']);

					$el->appendChild($el_file);
				}
			} else if (!$is_attribute && ($col == 'pages' || $col == 'categories')) {
				foreach ($product->product_pages() as $page) {
					if ($page->page['id'] && $page->page['menu'] != 3) {
						$_el = $this->dom->createElement('page_id', $page['page_id']);

						$_el->setAttribute('categorytext', join(' / ', $page->page->model->getFullPath()));

						$el->appendChild($_el);
					}
				}
			} else if (!$is_attribute && ($col == 'manufacturers')) {
				foreach ($product->product_pages() as $page) {
					if ($page->page['id'] && $page->page['menu'] == 3) {
						$_el = $this->dom->createElement('manufacturer', $page['page_id']);

						$_el->setAttribute('categorytext', join(' / ', $page->page->model->getFullPath()));

						$el->appendChild($_el);
					}
				}
			} else if (!$is_attribute && $col == 'attributes') {

			} else if ($col == 'availability') {
				$el->appendChild($this->dom->createElement('availability', $product->availability['name']));
			} else if ($col == 'availability_days') {
				$el->appendChild($this->dom->createElement('availability_days', $product->availability['days']));
			} else if (!$is_attribute && $col == 'discounts') {
				foreach ($product->product_discounts() as $discount) {
					$el_discount = $this->dom->createElement('attribute', $discount['value']);

					$el_discount->setAttribute('quantity', $discount['quantity']);

					$el->appendChild($el_discount);
				}
			} else if (!$is_attribute && $col == 'features') {
				if ($product['featureset_id'] && ($featureset = Core::$db->featureset[$product['featureset_id']]) && $featureset['id']) {
					$tbl = 'features_' . $product['featureset_id'];

					$features = $this->dom->createElement('features');
					$features->setAttribute('set_id', $product['featureset_id']);
					$features->setAttribute('name', $featureset['name']);

					if (Core::$db_inst->tableFields($tbl) && ($row = Core::$db->$tbl()->where('product_id', $product['id'])->fetch())) {
						foreach (Core::$db->feature()->where('featureset_id', $product['featureset_id'])->order('position ASC') as $feature) {

							$_el = $this->dom->createElement('feature');
							$_el->appendChild($this->dom->createTextNode($row['f' . $feature['id']]));
							$_el->setAttribute('id', $feature['id']);
							$_el->setAttribute('name', $feature['name']);
							$_el->setAttribute('type', $feature['type']);
							$_el->setAttribute('unit', $feature['unit']);
							$_el->setAttribute('filter', $feature['filter']);
							$_el->setAttribute('position', $feature['position']);

							$features->appendChild($_el);
						}
					}

					$el->appendChild($features);
				}
			} else {
				$text = $this->dom->createTextNode($product[$col]);
				$_el = $this->dom->createElement($col);
				$_el->appendChild($text);

				$el->appendChild($_el);
			}
		}

		

		if (!$is_attribute && in_array('attributes', $columns)) {
			$el_attributes = $this->dom->createElement('attributes');
			$el_attributes->setAttribute('count', $product->product_attributes()->count());

			foreach ($product->product_attributes() as $attr) {
				$el_attribute = $this->dom->createElement('attribute');

				foreach ($attr as $key => $value) {
					$text = $this->dom->createTextNode($value);
					$_el = $this->dom->createElement($key);
					$_el->appendChild($text);
					$el_attribute->appendChild($_el);
				}
				$el_attributes->appendChild($el_attribute);
				$el->appendChild($el_attributes);
			}
		}

		$this->root->appendChild($el);

		return $el;
	}

	public function build()
	{
		return $this->dom->saveXML();
	}

}