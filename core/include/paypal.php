<?php

defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */
class Paypal
{

	public static $user;
	public static $password;
	public static $signature;
	public static $sandbox = false;
	public static $currency;
	public static $localecode;
	public static $url_callback = 'paypal/callback';
	public static $url_cancel = 'paypal/fail';
	protected static $endpoint = 'https://api-3t.paypal.com/nvp';
	protected static $paypalurl = 'https://www.paypal.com/webscr&cmd=_express-checkout&token=';

	// -------------------

	const version = '3.2';

	protected static $set_express_checkout_fields = array(
		//-- REQUIRED --//
		'METHOD' => 'SetExpressCheckout',
		'RETURNURL' => '',
		'CANCELURL' => '',
		'AMT' => '', // payment amount - MUST include decimal point followed by two further digits
		//-- OPTIONAL --//
		'CURRENCYCODE' => '',
		'MAXAMT' => '',
		'USERACTION' => 'commit', // commit | contiue
		'INVNUM' => '', // Your own unique invoice / tracking number
		// no shipping address
		'NOSHIPPING' => '0',
		// override shipping address
		'ADDROVERRIDE' => '1',
		'SHIPTONAME' => '',
		'SHIPTOSTREET' => '',
		'SHIPTOSTREET2' => '',
		'SHIPTOCITY' => '',
		'SHIPTOSTATE' => '',
		'SHIPTOZIP' => '',
		'SHIPTOCOUNTRYCODE' => '', // list of country codes here: https://www.paypal.com/en_US/ebook/PP_NVPAPI_DeveloperGuide/countrycodes_new.html#1006794

		'LOCALECODE' => '', // Defaults to 'US' - list of country codes here: https://www.paypal.com/en_US/ebook/PP_NVPAPI_DeveloperGuide/countrycodes_new.html#1006794

		'PAGESTYLE' => '', //set this to match the name of any page style you set up in the profile subtab of your paypal account
		'HDRIMG' => '', //header image displayed top left, size: 750px x 90px. defaults to business name in text
	);
	protected static $do_express_checkout_fields = array(
		//-- REQUIRED --
		'METHOD' => 'DoExpressCheckoutPayment',
		'TOKEN' => '',
		'PAYERID' => '',
		'AMT' => '', // payment amount - MUST include decimal point followed by two further digits
		'PAYMENTACTION' => 'Sale',
		//-- OPTIONAL --
		'DESC' => '',
		'CURRENCYCODE' => '', // default is USD - only required if other currency needed
		'INVNUM' => '',
		'ITEMAMT' => '', // sum cost of all items in order not including shipping or handling
		'SHIPPINGAMT' => '',
		'HANDLINGAMT' => '',
		'TAXAMT' => '',
	);

	private function setFields(array $fields, array $data)
	{
		foreach ($data as $name => $value) {
			$fields[$name] = $value;
		}

		foreach ($fields as $name => $value) {
			if (empty($value)) {
				unset($fields[$name]);
			}
		}

		return $fields;
	}

	private static function request($nvp_str)
	{
		$postdata = http_build_query(array(
					'USER' => self::$user,
					'PWD' => self::$password,
					'SIGNATURE' => self::$signature,
					'VERSION' => self::version
				)) . '&' . $nvp_str;

		$ch = curl_init(self::$endpoint);
		curl_setopt_array($ch, array(
			CURLOPT_HEADER => FALSE,
			CURLOPT_SSL_VERIFYPEER => FALSE,
			CURLOPT_SSL_VERIFYHOST => FALSE,
			CURLOPT_VERBOSE => TRUE,
			CURLOPT_RETURNTRANSFER => TRUE,
			CURLOPT_POST => TRUE
		));
		curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata);

		$response = curl_exec($ch);

		if ($err = curl_errno($ch)) {
			echo $err;
		} else {
			curl_close($ch);
		}

		return $response;
	}

	private static function set_express_checkout($order)
	{
		$data = array(
			'CURRENCYCODE' => self::$currency,
			'LOCALECODE' => self::$localecode,
			'AMT' => round((float) $order['total_incl_vat'], 2),
			'RETURNURL' => url(self::$url_callback, null, true),
			'CANCELURL' => url(self::$url_cancel, null, true),
			'DESC' => __('order') . ' #' . $order['id'],
			'INVNUM' => 'ORDER' . $order['id'],
			'NOSHIPPING' => 1
		);

		$data = self::setFields(self::$set_express_checkout_fields, $data);

		$nvp_str = http_build_query($data);

		$response = self::request($nvp_str);

		parse_str(urldecode($response), $response_array);

		if (strtoupper($response_array['ACK']) == 'SUCCESS') {
			$paypal_token = $response_array['TOKEN'];

			$_SESSION['paypal']['token'] = urldecode($paypal_token);

			redirect(self::$paypalurl . $paypal_token . '&useraction=' . $data['USERACTION']);

			return false;
		} else {
			unset($_SESSION['paypal']);

			return self::catchError($response_array);
		}
	}

	private function do_express_checkout_payment($order, $token, $payerid = null)
	{
		$data = self::setFields(self::$do_express_checkout_fields, array(
					'CURRENCYCODE' => self::$currency,
					'LOCALECODE' => self::$localecode,
					'AMT' => round((float) $order['total_incl_vat'], 2),
					'TOKEN' => $token,
					'PAYERID' => $payerid ? $payerid : $_GET['PayerID'],
					'DESC' => __('order') . ' #' . $order['id'],
					'INVNUM' => 'ORDER' . $order['id']
				));

		$nvp_qstr = http_build_query($data);

		$response = self::request($nvp_qstr);

		parse_str(urldecode($response), $nvp_response_array);

		return $nvp_response_array;
	}

	private function catchError($response_array)
	{
		if (!empty($response_array['L_ERRORCODE0'])) {
			switch ((int) $response_array['L_ERRORCODE0']) {
				// ession expired
				case 10411: return -1;
				// duplicate invoice id
				case 10412: return -2;
				// missing or invalid payerid
				case 10419:
				case 10406: return -3;
				// token is missing or is invalid
				case 10408:
				case 10410: return -4;
				// amount exceeds the maximum
				case 10414: return -5;
				// a successful transaction has already been completed for this token
				case 10415: return -6;
				// customer must return to PayPal to select new funding sources
				case 10422: return -7;
			}
		}

		return false;
	}

	public static function setup()
	{
		if (!self::$user) {
			if (!function_exists('curl_init')) {
				throw new Exception('CURL extension is required');
			}

			require_once(DOCROOT . 'etc/paypal.php');

			self::$user = $paypal['username'];
			self::$password = $paypal['password'];
			self::$signature = $paypal['signature'];
			self::$sandbox = (bool) $paypal['sandbox'];
			self::$currency = $paypal['currency_code'];
			self::$localecode = $paypal['locale_code'];

			if (self::$sandbox) {
				self::$endpoint = 'https://api-3t.sandbox.paypal.com/nvp';
				self::$paypalurl = 'https://www.sandbox.paypal.com/cgi-bin/webscr?cmd=_express-checkout&token=';
			}

			if (empty(self::$user) || empty(self::$password) || empty(self::$signature)) {
				throw new Exception('Wrong Paypal configuration');
			}
		}
	}

	public static function pay($order)
	{
		if (!$order || !$order['id']) {
			return false;
		}

		$_SESSION['paypal'] = array(
			'order_id' => $order['id'],
			'token' => ''
		);

		return self::set_express_checkout($order);
	}

	public static function callback()
	{
		if (!isSet($_SESSION['paypal']) || empty($_SESSION['paypal']['token'])) {
			return false;
		}

		if (!($order = Core::$db->order[(int) $_SESSION['paypal']['order_id']]) || $order['payment_realized']) {
			return false;
		}

		$nvp_response_array = self::do_express_checkout_payment($order, $_SESSION['paypal']['token']);

		if (strtoupper($nvp_response_array['ACK']) == 'SUCCESS') {
			if ($nvp_response_array['AMT'] != round((float) $order['total_incl_vat'], 2)) {
				// invalid amount
				return -10;
			}

			if ($nvp_response_array['CURRENCYCODE'] != self::$currency) {
				// invalid currency
				return -11;
			}

			if ($nvp_response_array['TOKEN'] != $_SESSION['paypal']['token']) {
				// invalid token
				return -12;
			}

			$order->update(array(
				'payment_session' => $nvp_response_array['TRANSACTIONID'],
				'payment_realized' => time()
			));

			Notify::payment('PayPal.com', $order);

			return true;
		} else {
			return self::catchError($response_array);
		}
	}

}