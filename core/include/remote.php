<?php

defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */
class Remote {
	public static function getExpirationDate() {
		$response = file_get_contents(Core::$remote_uri.'get-expiration-date/'.Core::$config['remote_key']);
		$response = self::decodeResponse($response);
		if($response->status == 200) {
			return $response->date;
		}else{
			return false;
		}
	}


	private static function decodeResponse($response) {
		return json_decode($response);
	}
}