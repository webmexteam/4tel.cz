<?php

defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */
class Controller extends Extendable
{

	public $tpl = 'layout.latte';
	public $content = null;
	public $pagination_page = 1;

	public function __construct()
	{
		Customer::setup();
		User::setup();

		define('SHOW_PRICES', (Core::config('hide_prices_to_visitors') && Customer::$logged_in) || !Core::config('hide_prices_to_visitors'));

		if (isSet($_GET['page'])) {
			$this->pagination_page = (int) $_GET['page'];
		}
	}

	public function render()
	{
		if (is_string($this->tpl)) {
			return tpl($this->tpl, array(
				'content' => $this->content
			));
		} else {
			return $this->tpl;
		}
	}

	public function __callX($method, $arguments)
	{

	}

}