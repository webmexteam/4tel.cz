<?php

defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */
class Captcha
{

	public static function get($name, $opt = array())
	{
		$settings = array_merge(array(
			'width' => 100,
			'height' => 35,
			'length' => 4,
			'bgcolor' => array(255, 255, 255),
			'fontsize' => 20
				), $opt);

		$hash = md5($name);

		if (!isSet($_SESSION['captcha'])) {
			$_SESSION['captcha'] = array();
		}

		$_SESSION['captcha'][$hash] = array($settings);

		$obj = new Captcha_Session;
		$obj->img = url('captcha/img/' . $hash);
		$obj->hash = $hash;
		$obj->settings = $settings;

		return $obj;
	}

	public static function validate($name, $code)
	{
		$hash = md5($name);

		if (!empty($_SESSION['captcha'][$hash]) && $code && $_SESSION['captcha'][$hash][1] == md5($code)) {
			unset($_SESSION['captcha'][$hash]);

			return true;
		}

		return false;
	}

}

class Captcha_Session
{

	public $hash, $img, $settings;

	public function __toString()
	{
		return (string) $this->img;
	}

}