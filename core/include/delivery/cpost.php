<?php

defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */
Delivery::$drivers[] = 'cpost';

class Delivery_Cpost extends Delivery
{

	public $name = 'Česká pošta';

	public function getTrackLink($track_num = null)
	{
		if (!$track_num) {
			$track_num = $this->order['track_num'];
		}

		return 'http://www.cpost.cz/cz/nastroje/sledovani-zasilky.php?barcode=' . $track_num . '&locale=CZ&go=ok';
	}

}