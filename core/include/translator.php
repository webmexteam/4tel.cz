<?php defined('WEBMEX') or die('No direct access.');

use Nette\Object, 
	Nette\Localization\ITranslator;


/**
 * Translator
 *
 * @author  Michal Mikolas <nanuqcz@mail.com>
 * @copyright  Webmex - http://www.webmex.cz.
 */
class Translator extends Object implements ITranslator
{

	public function translate($message, $count = NULL)
	{
		return __($message);
	}

}