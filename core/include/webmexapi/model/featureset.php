<?php

defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */
class WebmexAPI_Model_Featureset extends WebmexAPI_Model
{

	public $table_name = 'featureset';

	public function insert($data, $return_item = false)
	{
		$featureset_id = parent::insert($data, $return_item);

		if ($featureset_id) {
			$this->createTable($featureset_id);
		}

		return $featureset_id;
	}

	public function update($find_by, $data, $callback = null)
	{
		$data = $this->sanitize($data);

		$featureset = parent::update($find_by, $data, $callback);

		if ($featureset) {
			$this->createTable($featureset['id']);

			return $featureset;
		}

		return false;
	}

	public function createTable($featureset_id, $columns = null)
	{
		$tbl_name = 'features_' . $featureset_id;
		$cols = array(
			'product_id' => array(
				'type' => 'integer',
				'primary' => true
			)
		);

		if ($columns === null) {
			$columns = Core::$db->feature()->where('featureset_id', $featureset_id);
		}

		foreach ($columns as $feature) {

			if ($feature['type'] == 'string') {
				$s = array(
					'type' => 'varchar',
					'index' => true,
					'index_length' => 10
				);
			} else if ($feature['type'] == 'text') {
				$s = array(
					'type' => 'text'
				);
			} else if ($feature['type'] == 'number') {
				$s = array(
					'type' => 'float',
					'index' => true
				);
			} else if ($feature['type'] == 'yesno') {
				$s = array(
					'type' => 'integer',
					'length' => 1,
					'not_null' => true,
					'index' => true,
					'default' => 0
				);
			} else if ($feature['type'] == 'select') {
				$s = array(
					'type' => 'varchar',
					'index' => true,
					'index_length' => 10
				);
			}

			$cols['f' . $feature['id']] = $s;
		}

		if ($fields = Core::$db_inst->tableFields($tbl_name)) {
			// add cols
			Core::$db_inst->schema->addColumns($tbl_name, $cols);

			foreach ($fields as $col_name => $col_params) {
				if ($col_name{0} == 'f' && !isSet($cols[$col_name])) {
					Core::$db_inst->schema->dropColumn($tbl_name, $col_name);
				}
			}
		} else {
			// create table
			Core::$db_inst->schema->createTable($tbl_name, $cols);
		}
	}

}