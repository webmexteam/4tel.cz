<?php defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */
 
class WebmexAPI_Model_Page extends WebmexAPI_Model {
	
	public $table_name = 'page';
	
	public function insert($data, $return_item = false)
	{
		if(empty($data['name'])){
			throw new Exception('Required param: "name"');
		}
		
		$data = $this->sanitize($data);
		
		if(empty($data['sef_url'])){
			$data['sef_url'] = dirify($data['name']);	
		}
		
		if(! isSet($data['status'])){
			$data['status'] = 1;
		}
		
		$page_id = parent::insert($data, $return_item);
		
		return $page_id;
	}
	
	private function sanitize($data)
	{
		if(isset($data['name'])){
			$data['name'] = preg_replace('/\s{2,}/', ' ', trim($data['name']));
		}
		
		if(empty($data['parent_page'])){
			$data['parent_page'] = 0;
		}
		
		if(! isset($data['enable_filter'])){
			$data['enable_filter'] = 1;
		}
		
		if(User::$logged_in){
			$data['last_change'] = time();
			$data['last_change_user'] = User::get('id');
		}
		
		return $data;
	}
	
	public function findByPath(array $path, $parent_id = 0)
	{
		if(empty($path)){
			return false;
		}
		
		foreach($path as $page_name){
			$page_name = trim($page_name);
			
			if($page = Core::$db->page()->where('parent_page', $parent_id)->where('name', $page_name)->limit(1)->fetch()){
				$parent_id = $page['id'];
				
			}else{
				$parent_id = false;
			}
		}
		
		return $parent_id;
	}
	
	public function createPath(array $path, $parent_id = 0, $menu = 2, $allow_insert = true)
	{
		if(empty($path)){
			return false;
		}
		
		foreach($path as $page_name){
			$page_name = trim($page_name);
			
			if(! $page_name){
				continue;
			}
			
			if($page = Core::$db->page()->where('parent_page', $parent_id)->where('name', $page_name)->limit(1)->fetch()){
				$parent_id = $page['id'];
				
			}else if($allow_insert){
				$parent_id = $this->insert(array(
					'name' => $page_name,
					'menu' => $parent_id ? null : $menu,
					'parent_page' => $parent_id,
					'products' => ($menu == 2 || $menu == 3) ? 1 : 0
				));
			}
		}
		
		return $parent_id;
	}
}