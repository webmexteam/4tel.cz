<?php

defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */
class WebmexAPI_Bucket implements Iterator, ArrayAccess
{

	public $data, $options = array();
	protected $position = 0;

	public function __construct($data, array $options = array())
	{
		$this->data = $data;
		$this->options = $options;
	}

	public function __set($k, $v)
	{
		$this->options[$k] = $v;
	}

	public function __get($k)
	{
		if ($k == 'model' && is_object($this->data) && property_exists($this->data, 'model')) {
			return $this->data->model;
		}

		if (isSet($this->options[$k])) {
			return $this->options[$k];
		}

		return null;
	}

	public function __call($method, $arguments)
	{
		if (is_object($this->data)) {
			return call_user_func_array(array($this->data, $method), $arguments);
		}
	}

	public function as_array()
	{
		if (is_object($this->data) && method_exists($this->data, 'as_array')) {
			return (array) $this->data->as_array();
		}

		return (array) $this->data;
	}

	public function rewind()
	{
		$this->position = 0;
	}

	public function current()
	{
		return $this->data[$this->position];
	}

	public function key()
	{
		return $this->position;
	}

	public function next()
	{
		$this->position++;
	}

	public function valid()
	{
		return isset($this->data[$this->position]);
	}

	public function offsetSet($offset, $value)
	{
		$this->data[$offset] = $value;
	}

	public function offsetExists($offset)
	{
		return isset($this->data[$offset]);
	}

	public function offsetUnset($offset)
	{
		unset($this->data[$offset]);
	}

	public function offsetGet($offset)
	{
		return isset($this->data[$offset]) ? $this->data[$offset] : null;
	}

}