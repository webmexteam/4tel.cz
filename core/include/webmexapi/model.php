<?php

defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */
class WebmexAPI_Model
{

	public $table_name = null;

	public function find($find_by, $value = false)
	{
		if (is_object($find_by)) {
			return $find_by;
		}

		if ($value === false && is_numeric($find_by)) {
			$value = (int) $find_by;
			$find_by = 'id';
		}

		$table_name = $this->table_name;

		if ($item = Core::$db->$table_name()->where($find_by, $value)->limit(1)->fetch()) {
			return $item;
		}

		return null;
	}

	public function save($data, $unique = 'id', $return_item = true, $callback = null, $mode = 0, $return_new_data = false, & $inserted = false)
	{
		// mode = 0 insert & update
		// mode = 1 update
		// mode = 2 insert

		if ($mode < 2) {

			if (is_object($data)) {
				$upd = $this->update($data, $data->as_array(), $callback);
			} else if (is_array($data) && !empty($data[$unique])) {
				$upd = $this->update(array($unique, $data[$unique]), $data, $callback);
			}

			if ($upd && $upd->updated !== false) {

				if ($return_item) {
					return $this->find($unique, $data[$unique]);
				}

				return (int) $upd['id'];
			} else if ($mode != 1 && !$upd) {
				$inserted = true;

				return $this->insert($data, $return_item);
			}
		} else if ($mode != 1) {
			$inserted = true;

			return $this->insert($data, $return_item);
		}

		return false;
	}

	public function save_old($data, $unique = 'id', $return_item = true, $callback = null, $mode = 0, $return_new_data = false)
	{
		// mode = 0 insert & update
		// mode = 1 update
		// mode = 2 insert

		if (is_object($data)) {
			if ($mode < 2) {
				$this->update($data, $data->as_array(), $callback);

				return $this->find($unique, $data[$unique]);
			}

			return false;
		} else if (is_array($data) && !empty($data[$unique])) {
			if ($mode < 2) {
				$this->update(array($unique, $data[$unique]), $data, $callback);

				return $this->find($unique, $data[$unique]);
			}

			return false;
		} else {
			return $mode != 1 ? $this->insert($data, $return_item) : false;
		}

		return false;
	}

	public function insert($data, $return_item = false)
	{
		if(!Core::canAddNewProduct())
			return false;
		
		$table_name = $this->table_name;

		$data = prepare_data($table_name, $data);

		if ($data) {
			if ($item_id = Core::$db->$table_name($data)) {
				if ($return_item) {
					return $this->find($item_id);
				} else {
					return $item_id;
				}
			}
		}

		return false;
	}

	public function update($find_by, $data, $callback = null)
	{
		$table_name = $this->table_name;

		if (is_array($data) && !empty($data['_object']) && is_object($data['_object'])) {
			$item = $data['_object'];
		} else if (is_object($find_by)) {
			$item = $find_by;
		} else if (is_array($find_by) && count($find_by) == 2) {
			$item = $this->find($find_by[0], $find_by[1]);
		} else if (is_numeric($find_by)) {
			$item = $this->find($find_by);
		}

		if ($item) {
			$_data = prepare_data($table_name, $data, false);

			if ($_data) {

				$item = Core::$api->bucket($item);

				if ((is_string($callback) && function_exists($callback)) || (is_array($callback) && method_exists($callback[0], $callback[1]))) {
					if (call_user_func_array($callback, array(& $item, & $_data, $data)) === false) {
						$item->updated = false;
						return $item;
					}
				}

				if (!$item->update($_data)) {
					return false;
				}

				// must return OLD object
				return $item;
			}
		}

		return false;
	}

	public function delete($find_by, $value = false)
	{
		if ($item = $this->find($find_by, $value)) {
			$item->delete();
			return true;
		}

		return false;
	}

}