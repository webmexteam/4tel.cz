<?php

Extendable::extend('Customer', 'FacebookUser');

class FacebookUser {
	
	public static $api;
	public static $user;
	public static $user_data;

	public static function setup()
	{
		require_once __DIR__ .'/facebook/facebook.php';

		$storage = Core::module('facebook')->getStorage();
		
		if(! $storage['app_id'] || ! $storage['secret'] || ! (int) $storage['login']){
			return;
		}
		
		self::$api = new Facebook(array(
		  'appId'  => $storage['app_id'],
		  'secret' => $storage['secret'],
		  'cookie' => true, // enable optional cookie support
		));

		self::$user = null;
		
		$uid = null;
		$ttl = 600; // seconds
		$last_check = 0;
		
		if(! empty($_SESSION['sqc_facebook'])){
			$last_check = (int) $_SESSION['sqc_facebook']['last_check'];
		}
		
		try {
			$uid = self::$api->getUser();
			
			if($uid && $last_check < (time() - $ttl)){
				self::$user = self::$api->api('/me');
				
				if(self::$user && self::$user['email']){
					$_SESSION['sqc_facebook'] = array(
						'last_check' => time(),
						'user' => self::$user
					);
				}
			}
			
		}catch(FacebookApiException $e) {
			
		}

		if($uid && ! self::$user && ! empty($_SESSION['sqc_facebook'])){
			self::$user = $_SESSION['sqc_facebook']['user'];
		}
		
		if(self::$user){
			$customer = self::register();
			
			self::$user_data = $customer->as_array();
			
			Customer::$logged_in = true;
		}
	}
	
	private static function register()
	{
		if(self::$user){
			if($customer = Core::$db->customer()->where('email', self::$user['email'])->fetch()){
				return $customer;
				
			}else{
				$customer_id = Core::$db->customer(array(
					'email' => self::$user['email'],
					'first_name' => self::$user['first_name'],
					'last_name' => self::$user['last_name'],
					'active' => 1
				));
				
				if($customer_id){
					return Core::$db->customer[$customer_id];
				}
			}
		}
		
		return null;
	}
	
	public static function get($name = null)
	{
		return $name ? self::$user_data[$name] : self::$user_data;
	}
	
	public static function logout()
	{
		unset($_SESSION['sqc_facebook']);
		
		if(self::$user){
			redirect(self::$api->getLogoutUrl());
		}
	}
}
