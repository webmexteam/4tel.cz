<?php

defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */
class Extendable
{

	const METHOD_NOT_FOUND = -1;

	public static $imported_functions = array();

	public function __call($method, $args)
	{
		$traces = debug_backtrace();
		$object = $traces[1]['object'];
		$class_name = get_class($object);

		if (is_array(self::$imported_functions[$class_name]) && array_key_exists($method, self::$imported_functions[$class_name])) {
			$inst = self::$imported_functions[$class_name][$method];
			$inst->object = $object;

			return call_user_func_array(array($inst, $method), $args);
		}

		return self::METHOD_NOT_FOUND;
	}

	public static function __callStatic($method, $args)
	{
		if (array_key_exists($method, self::$imported_functions[__CLASS__])) {
			array_unshift($args, self);
			return call_user_func_array(array(self::$imported_functions[__CLASS__][$method], $method), $args);
		}

		return self::METHOD_NOT_FOUND;
	}

	public static function callMixins()
	{
		$traces = debug_backtrace();
		$function = $traces[1]['function'];
		$args = $traces[1]['args'];
		$class = $traces[1]['class'];
		$type = $traces[1]['type'];

		if (is_array(self::$imported_functions[$class]) && array_key_exists($function, self::$imported_functions[$class])) {
			$inst = self::$imported_functions[$class][$function];

			if ($type == '->') {
				$inst->object = $object;
			}

			return call_user_func_array(array($inst, $function), $args);
		}

		return self::METHOD_NOT_FOUND;
	}

	public static function extend($class, $extend_class)
	{
		$inst = new $extend_class();

		$functions = get_class_methods($inst);

		foreach ($functions as $key => $function_name) {
			self::$imported_functions[$class][$function_name] = &$inst;
		}
	}

}