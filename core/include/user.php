<?php

defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */
class User
{

    const has_or = -1;


    public static $logged_in = false;
    public static $data;
    protected static $permission;


    public static function setup()
    {
	if (empty($_SESSION['webmex_user']) && !empty($_COOKIE['user_remember'])) {
		$parts = explode(';', $_COOKIE['user_remember']);

		if (count($parts) == 2 && is_numeric($parts[0])) {
			$user = Core::$db->user[(int) $parts[0]];

			if ($user && sha1(Core::config('instid') . $user['id'] . $user['password']) == $parts[1]) {
				 $_SESSION['webmex_user'] = array(
				    't' => time(),
				    'login_time' => time(),
				    'user' => $user->as_array()
				);

					    $user->update(array(
				    'last_login' => time(),
				    'bad_login_count' => 0,
				));
			}
		}
	}
	
	if (!empty($_SESSION['webmex_user']) && is_array($_SESSION['webmex_user'])) {
	    self::$data = $_SESSION['webmex_user'];

	    if (empty($_POST['session_disable_extend'])) {
		$_SESSION['webmex_user']['t'] = time();
	    }

	    self::$logged_in = true;
	} else {
	    unset($_SESSION['webmex_user']);
	}
    }


    public static function login($username, $password)
    {
	$password = trim($password);

	$user = Core::$db->user('username = ?', $username)->fetch();

	if (!$user) {
	    return 'user_not_found';
	}

	if ((int) $user['enable_login'] && !$user['security_blocked'] && $user['password'] == sha1(sha1($password))) {
	    foreach ((array) $_SESSION as $k => $v) {
		unset($_SESSION[$k]);
	    }

	    $_SESSION['webmex_user'] = array(
		't' => time(),
		'login_time' => time(),
		'user' => $user->as_array()
	    );
	    
	    $v = $user['id'] . ';' . sha1(Core::config('instid') . $user['id'] . $user['password']);
	    setcookie('user_remember', $v, strtotime('+3 months'), Core::$base);

	    $user->update(array(
		'last_login' => time(),
		'bad_login_count' => 0,
	    ));

	    return true;
	} elseif ($user['security_blocked']) {
	    return 'security_blocked';
	} else {
	    $newBadLoginCount = $user['bad_login_count'] + 1;
	    $user->update(array('bad_login_count' => $newBadLoginCount));

	    if ($newBadLoginCount >= 5) {
		$user->update(array('security_blocked' => 1));
	    }

	    return 'invalid_password';
	}
    }


    public static function logout()
    {
	unset($_SESSION['webmex_user']);
	setcookie('user_remember', '', time() - 3600, Core::$base);
    }


    public static function get($name)
    {
	return self::$data['user'][$name];
    }


    public static function has($permission)
    {
	if (func_num_args() > 1) {
	    $or = false;
	    $args = func_get_args();
	    foreach ($args as $arg) {
		if ($arg == self::has_or) {
		    $or = true;
		} else if ($or && self::has($arg)) {
		    return true;
		} else if (!$or && !self::has($arg)) {
		    return false;
		}
	    }

	    return !$or;
	}

	if (self::$permission === null) {
	    self::$permission = array();

	    foreach (Core::$db->user_permissions()->where('user_id', (int) self::get('id')) as $perm) {
		self::$permission[$perm['permission_id']] = Core::$def['permissions'][$perm['permission_id']];
	    }
	}

	if ((int) self::get('id') == 1 || isSet(self::$permission[10000])) { // admin (id 1) has all permissions
	    return true;
	}

	if (is_numeric($permission) && isSet(self::$permission[$permission])) {
	    return true;
	} else if (in_array($permission, self::$permission)) {
	    return true;
	}

	return false;
    }


}
