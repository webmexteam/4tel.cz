<?php

defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */
class Customer extends Extendable
{

	public static $logged_in = false;
	public static $data;

	// Facebook
	public static $api;
	public static $user;
	public static $user_data;

	public static function setup()
	{
		if (empty($_SESSION['webmex_customer']) && !empty($_COOKIE['customer_remember'])) {
			$parts = explode(';', $_COOKIE['customer_remember']);

			if (count($parts) == 2 && is_numeric($parts[0])) {
				$customer = Core::$db->customer[(int) $parts[0]];

				if ($customer && (int) $customer['active'] && sha1(Core::config('instid') . $customer['id'] . $customer['password']) == $parts[1]) {
					$_SESSION['webmex_customer'] = array(
						't' => time(),
						'customer' => $customer->as_array()
					);

					$customer->update(array(
						'last_login' => time()
					));
				}
			}
		}

		if (!empty($_SESSION['webmex_customer']) && is_array($_SESSION['webmex_customer'])) {
			if ($_SESSION['webmex_customer']['t'] >= strtotime('-30 minutes')) {
				self::$data = $_SESSION['webmex_customer'];

				$_SESSION['webmex_customer']['t'] = time();

				self::$logged_in = true;
			} else {
				//$_SESSION['flash_login_error'] = 'msg_timeout_logout';
				//unset($_SESSION['webmex_customer']);
			}
		} else {
			//unset($_SESSION['webmex_customer']);
		}

		Extendable::callMixins();
	}

	/** Facebook register **/
	private static function register()
	{
		if(self::$user){
			if($customer = Core::$db->customer()->where('email', self::$user['email'])->fetch()){
				if($customer['type'] == 'facebook') {
					return $customer;
				}else{
					return false;
				}

			}else{
				$password = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 10);

				$customer_id = Core::$db->customer(array(
					'email' => self::$user['email'],
					'first_name' => self::$user['first_name'],
					'last_name' => self::$user['last_name'],
					'type' => 'facebook',
					'active' => 1,
					'password' => sha1(sha1(trim($password)))
				));

				if($customer_id){
					$customer = Core::$db->customer[$customer_id];

					$customer_data = $customer->as_array();
					$customer_data['id'] = $customer_id;
					$customer_data['password'] = $password;

					if (!(int) Core::config('customer_confirmation')) {
						Email::event('new_account', $customer_data['email'], null, $customer_data);
						Voucher::generate('customer_register', $customer_data);
					} else {
						$_SESSION['customer_confirm'] = true;
						sendmail(Core::config('email_notify'), array(__('customer_registered'), __('customer_registered_text')), $customer_data);
					}

					return $customer;
				}
			}
		}

		return null;
	}

	public static function fblogin() {
		// Facebook
		require_once APPDIR .'/vendor/facebook/facebook.php';

		self::$api = new Facebook(array(
		  'appId'  => '690075031032604',
		  'secret' => 'd00e006356f14868569336a78b67e53d',
		  'cookie' => true, // enable optional cookie support
		));

		if(isset($_POST['token'])) {
			self::$api->setAccessToken($_POST['token']);
		}

		self::$user = null;

		$uid = null;
		$ttl = 600; // seconds
		$last_check = 0;

		if(! empty($_SESSION['sqc_facebook'])){
			$last_check = (int) $_SESSION['sqc_facebook']['last_check'];
		}

		try {
			$uid = self::$api->getUser();

			if($uid && $last_check < (time() - $ttl)){
				self::$user = self::$api->api('/me');

				if(self::$user && self::$user['email']){
					$_SESSION['sqc_facebook'] = array(
						'last_check' => time(),
						'user' => self::$user
					);
				}
			}

		}catch(FacebookApiException $e) {

		}

		if($uid && ! self::$user && ! empty($_SESSION['sqc_facebook'])){
			self::$user = $_SESSION['sqc_facebook']['user'];
		}

		if(self::$user){
			$customer = self::register();

			if(!$customer)
				return false;

			self::$user_data = $customer->as_array();

			self::$logged_in = true;

			$_SESSION['webmex_customer'] = array(
				't' => time(),
				'customer' => self::$user_data
			);

			$customer->update(array(
				'last_login' => time()
			));

			$v = self::$user_data['id'] . ';' . sha1(Core::config('instid') . self::$user_data['id'] . self::$user_data['password']);
			setcookie('customer_remember', $v, strtotime('+3 months'), Core::$base);

			return true;
		}
	}

	public static function login($email, $password, $remember = false)
	{

		$password = trim($password);

		$customer = Core::$db->customer('email', $email)->where('active', 1)->fetch();

		if (!$customer) {
			return 'user_not_found';
		}

		if ($customer['password'] == sha1(sha1($password))) {
			$_SESSION['webmex_customer'] = array(
				't' => time(),
				'customer' => $customer->as_array()
			);

			$customer->update(array(
				'last_login' => time()
			));

			$v = $customer['id'] . ';' . sha1(Core::config('instid') . $customer['id'] . $customer['password']);
			setcookie('customer_remember', $v, strtotime('+3 months'), Core::$base);
			

			return true;
		} else {
			return 'invalid_password';
		}
	}

	public static function logout()
	{
		unset($_SESSION['webmex_customer']);
		setcookie('customer_remember', '', time() - strtotime('+3 months'), Core::$base);

		Extendable::callMixins();
	}

	public static function get($name = null)
	{
		$value = $name ? self::$data['customer'][$name] : self::$data['customer'];

		if (!$value) {
			$value = Extendable::callMixins();

			if ($value == Extendable::METHOD_NOT_FOUND) {
				$value = null;
			}
		}

		return $value;
	}

}