<?php

defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */
class AdminController extends Controller
{

	public function __construct($open_access = false)
	{
		parent::__construct();

		require_once(APPROOT . 'include/adminhelpers.php');

		if (!$open_access && !User::$logged_in && !preg_match('/^default\/(login|lost_password)/i', Core::$uri)) {
			$_SESSION['login_redirect_to'] = (Core::$is_admin ? 'admin/' : '') . Core::$uri;

			redirect('admin/default/login');
		}

		$need_update = false;

		foreach (Core::$modules as $module) {
			if ($module->installed) {
				if (Update::getVersions($module->registry['version'], $module->getInfo('version'))) {
					$need_update = true;
				}
			}
		}

		if (!$need_update) {
			$version = Core::config('version');

			if (!$version) {
				$version = '1.1.3';
			}

			$need_update = Update::getVersions($version, Core::version);
		}

		if (!preg_match('/^(default\/(login|lost_password)|update\/updatescript|update\/ajax_run_db)/i', Core::$uri) && $need_update) {
			redirect('admin/update/updatescript');
		}

		// Build admin navigation
		$this->_buildMenu();

		$this->_setScriptConstants();

		Event::run('Controller::construct');
	}

	private function _buildMenu()
	{
		if (User::has(User::has_or, 'orders', 'orders-view', 'orders-invoices')) {
			$subnav = array(
				'list' => array(
					'position' => 0,
					'text' => __('list_records'),
					'url' => url('admin/orders'),
					'cls' => 'list'
				),
				'status_pending' => array(
					'position' => 10,
					'text' => __('status_pending'),
					'url' => url('admin/orders', array('status' => 1))
				),
				'status_in_progress' => array(
					'position' => 20,
					'text' => __('status_in_progress'),
					'url' => url('admin/orders', array('status' => 2))
				)
			);

			if (User::has(User::has_or, 'orders', 'orders-invoices')) {
				$subnav['invoices'] = array(
					'position' => 30,
					'text' => __('invoices'),
					'url' => url('admin/invoices')
				);
			}

			$subnav['packages'] = array(
				'position' => 40,
				'text' => __('packages'),
				'url' => url('admin/packages')
			);

			Menu::add(array(
				'position' => 1000,
				'text' => __('orders'),
				'iconCls' => 'icon-orders',
				'url' => url('admin/orders'),
				'menu' => $subnav
					), 'orders');
		}

		if (User::has(User::has_or, 'products', 'products-edit', 'products-features', 'products-export_import', 'products-xml_feeds')) {
			$subnav = array();

			if (User::has(User::has_or, 'products', 'products-edit')) {
				$subnav['list'] = array(
					'position' => 0,
					'text' => __('list_records'),
					'url' => url('admin/products'),
					'cls' => 'list'
				);
				$subnav['add'] = array(
					'position' => 10,
					'text' => __('new_product'),
					'url' => url('admin/products/edit/0'),
					'cls' => 'add'
				);
				$subnav['attribute_templates'] = array(
					'position' => 15,
					'text' => __('attribute_templates'),
					'url' => url('admin/attribute_templates')
				);
			}

			if (User::has(User::has_or, 'products', 'products-features')) {
				$subnav['features'] = array(
					'position' => 20,
					'text' => __('features'),
					'url' => url('admin/features')
				);
			}

			if (User::has(User::has_or, 'products', 'products-export_import')) {
				$subnav['dataexport'] = array(
					'position' => 35,
					'text' => __('dataexport'),
					'url' => url('admin/dataexport'),
				);

				$subnav['dataimport'] = array(
					'position' => 40,
					'text' => __('dataimport'),
					'url' => url('admin/dataimport'),
				);
			}

			if (User::has(User::has_or, 'products', 'products-xml_feeds')) {
				$subnav['xml_feeds'] = array(
					'position' => 50,
					'text' => __('xml_feeds'),
					'url' => url('admin/products/xml_feeds')
				);
			}

			if (User::has(User::has_or, 'products', 'products-export_import')) {
				$subnav['download_price_list'] = array(
					'position' => 60,
					'text' => __('download_price_list'),
					'url' => url('admin/dataexport/pricelist'),
					'premium' => true
				);
			}

			Menu::add(array(
				'position' => 2000,
				'text' => __('products'),
				'iconCls' => 'icon-products',
				'url' => url('admin/products'),
				'menu' => $subnav
					), 'products');
		}

		if (User::has('pages')) {
			$subnav = array(
				'list' => array(
					'position' => 0,
					'text' => __('list_records'),
					'url' => url('admin/pages'),
					'cls' => 'list'
				),
				'add' => array(
					'position' => 10,
					'text' => __('new_page'),
					'url' => url('admin/pages/edit/0'),
					'cls' => 'add'
				)
			);

			Menu::add(array(
				'position' => 3000,
				'text' => __('pages'),
				'iconCls' => 'icon-pages',
				'url' => url('admin/pages'),
				'menu' => $subnav
					), 'pages');
		}

		if (User::has('customers')) {
			$subnav = array(
				'list' => array(
					'position' => 0,
					'text' => __('list_records'),
					'url' => url('admin/customers'),
					'cls' => 'list'
				),
				'add' => array(
					'position' => 10,
					'text' => __('new_customer'),
					'url' => url('admin/customers/edit/0'),
					'cls' => 'add'
				),
				'newsletter' => array(
					'position' => 20,
					'text' => __('newsletter'),
					'url' => url('admin/newsletter'),
				),
				'newsletter_send' => array(
					'position' => 25,
					'text' => __('newsletter_send'),
					'url' => url('admin/newsletter/send'),
				),
				'vouchers' => array(
					'position' => 30,
					'text' => __('vouchers'),
					'url' => url('admin/vouchers'),
				),
				'voucher_generators' => array(
					'position' => 40,
					'text' => __('voucher_generators'),
					'url' => url('admin/vouchers/generators'),
				)
			);

			Menu::add(array(
				'position' => 4000,
				'text' => __('customers'),
				'iconCls' => 'icon-customers',
				'url' => url('admin/customers'),
				'menu' => $subnav
					), 'customers');
		}

		if (User::has('blocks')) {
			$subnav = array(
				'list' => array(
					'position' => 0,
					'text' => __('list_records'),
					'url' => url('admin/blocks'),
					'cls' => 'list'
				),
				'add' => array(
					'position' => 10,
					'text' => __('new_block'),
					'url' => url('admin/blocks/edit/0'),
					'cls' => 'add'
				)
			);

			Menu::add(array(
				'position' => 5000,
				'text' => __('blocks'),
				'iconCls' => 'icon-blocks',
				'url' => url('admin/blocks'),
				'menu' => $subnav
					), 'blocks');
		}

		if (User::has('banners')) {
			$subnav = array(
				'list' => array(
					'position' => 0,
					'text' => __('list_records'),
					'url' => url('admin/banners'),
					'cls' => 'list'
				),
				'add' => array(
					'position' => 10,
					'text' => __('new_banner'),
					'url' => url('admin/banners/edit/0'),
					'cls' => 'add'
				)
			);

			Menu::add(array(
				'position' => 6000,
				'text' => __('banners'),
				'iconCls' => 'icon-banners',
				'url' => url('admin/banners'),
				'menu' => $subnav
			), 'banners');
		}

		if (Core::$modules && User::has('modules')) {
			$subnav = array(
				'list' => array(
					'position' => 0,
					'text' => __('list_records'),
					'url' => url('admin/modules'),
					'cls' => 'list'
				)
			);

			Menu::add(array(
				'position' => 7000,
				'text' => __('modules'),
				'iconCls' => 'icon-modules',
				'url' => url('admin/modules'),
				'menu' => $subnav
					), 'modules');
		}

		if (User::has(User::has_or, 'settings', 'design', 'users', 'backup', 'update', 'import')) {
			$subnav = array();

			if (User::has('settings')) {
				$subnav['settings'] = array(
					'position' => 0,
					'text' => __('settings'),
					'url' => url('admin/settings')
				);
				$subnav['delivery_payment'] = array(
					'position' => 10,
					'text' => __('delivery_payment'),
					'url' => url('admin/settings/delivery_payment')
				);
				$subnav['availabilities'] = array(
					'position' => 20,
					'text' => __('availabilities'),
					'url' => url('admin/settings/availabilities')
				);
				$subnav['countries'] = array(
					'position' => 30,
					'text' => __('countries'),
					'url' => url('admin/settings/countries')
				);
				$subnav['emails'] = array(
					'position' => 40,
					'text' => __('emails'),
					'url' => url('admin/emails')
				);
				$subnav['labels'] = array(
					'position' => 40,
					'text' => __('labels'),
					'url' => url('admin/labels')
				);
			}

			if (User::has('design')) {
				$subnav['design'] = array(
					'position' => 50,
					'text' => __('design'),
					'url' => url('admin/design')
				);
			}

			if (User::has('users')) {
				$subnav['users'] = array(
					'position' => 60,
					'text' => __('users'),
					'url' => url('admin/users')
				);
			}

			if (User::has('backup')) {
				$subnav['backup'] = array(
					'position' => 70,
					'text' => __('backup'),
					'url' => url('admin/backup')
				);
			}

			/*if (User::has('update')) {
				$subnav['update'] = array(
					'position' => 80,
					'text' => __('update'),
					'url' => url('admin/update')
				);
			}*/

			if (User::has('import')) {
				$subnav['import'] = array(
					'position' => 80,
					'text' => __('import'),
					'url' => url('admin/import')
				);
			}

			if (User::has('settings')) {
				$subnav['system_maintenance'] = array(
					'position' => 90,
					'text' => __('system_maintenance'),
					'url' => url('admin/maintenance')
				);
				$subnav['translations'] = array(
					'position' => 85,
					'text' => __('translations'),
					'url' => url('admin/translations')
				);
			}
			
			if ((int) User::get('id') == 1) {
				$subnav['superadmin'] = array(
					'position' => 100,
					'text' => __('superadmin'),
					'url' => url('admin/superadmin')
				);
			}

			Menu::add(array(
				'position' => 8000,
				'text' => __('system'),
				'iconCls' => 'icon-system',
				'url' => url('admin/settings'),
				'menu' => $subnav
					), 'system');
		}
	}

}