<?php

defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */
class Db
{

	public static $connection;
	public static $queries = array();
	public $_type;
	public $schema;
	public $table_fields = array();

	public static function logQuery($query, $time)
	{
		self::$queries[] = array('sql' => $query, 'time' => round($time, 5));
	}

	public function connect($force_connect = false)
	{
		if (!$force_connect && preg_match('/^sqlite/', Core::$def['db']['dsn']) && !file_exists(DOCROOT . 'etc/data.sqlite.php')) {
			return false;
		}

		if (empty(Core::$def['db'])) {
			return false;
		}

		self::$connection = new PDO(Core::$def['db']['dsn'], Core::$def['db']['user'], Core::$def['db']['password']);

		/**
		 * ToDo: change to PDO::ERRMODE_WARNING. problems with intallation new webmex. why?
		 */
		self::$connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

		$this->_type = self::$connection->getAttribute(PDO::ATTR_DRIVER_NAME);

		if (!$force_connect) {
			$this->tableFields('config');
		}

		if ($this->_type == 'mysql') {
			self::$connection->query('SET NAMES UTF8');
		}

		$this->schema = new Db_Schema($this);

		return self::$connection;
	}

	public function truncateTable($table_name)
	{
		$table = $this->quote_table($table_name);

		if ($this->_type == 'sqlite') {
			self::$connection->query('DELETE FROM ' . $table);
			return self::$connection->query('DELETE FROM SQLITE_SEQUENCE WHERE name = "' . $table_name . '"');
		} else {
			return self::$connection->query('TRUNCATE TABLE ' . $table);
		}
	}

	public function getNextAI($table_name)
	{
		$table = $this->quote_table($table_name);

		if ($this->_type == 'sqlite') {
			$result = self::$connection->query('SELECT seq FROM SQLITE_SEQUENCE WHERE name = "' . $table_name . '"');

			if ($row = $result->fetch()) {
				return (int) $row['seq'] + 1;
			}
		} else if ($this->_type == 'mysql') {
			$result = self::$connection->query('SHOW TABLE STATUS LIKE "' . $table_name . '"');

			if ($row = $result->fetch()) {
				return (int) $row['Auto_increment'];
			}
		}

		$result = self::$connection->query('SELECT COUNT(*) AS ai FROM ' . $table . ' ');

		if ($row = $result->fetch()) {
			return (int) $row['ai'] + 1;
		}

		return null;
	}

	public function setAI($table_name, $ai)
	{
		$table = $this->quote_table($table_name);

		if ($this->_type == 'sqlite') {
			return self::$connection->query('UPDATE SQLITE_SEQUENCE SET seq = ' . ((int) $ai - 1) . ' WHERE name = "' . $table_name . '"');
		} else if ($this->_type == 'mysql') {
			return self::$connection->query('ALTER TABLE ' . $table . ' AUTO_INCREMENT = ' . (int) $ai);
		}
	}

	public function tableFields($table_name)
	{
		$table = $this->quote_table($table_name);

		if (isSet($this->table_fields[$table])) {
			return $this->table_fields[$table];
		}

		if (!($columns = $this->cacheGet('dbcols_' . $table_name))) {
			$columns = array();

			if ($this->_type == 'sqlite') {
				try {
					$result = Db::$connection->query('pragma table_info(' . $table . ')');
				} catch (PDOException $e) {
					return false;
				}

				foreach ($result as $column) {
					$len = null;

					if (preg_match('/\(([\d\,]+)\)$/', strtolower($column['type']), $m) && $m) {
						$len = $m[1];
					}

					$columns[$column['name']] = array(
						'type' => preg_replace('/\([\d\,]+\)$/', '', strtolower($column['type'])),
						'notnull' => $column['notnull'],
						'length' => $len
					);
				}
			} else {
				try {
					$result = self::$connection->query('EXPLAIN ' . $table);
				} catch (PDOException $e) {
					return false;
				}

				foreach ($result as $column) {
					$len = null;

					if (preg_match('/\(([\d\,]+)\)$/', strtolower($column['Type']), $m) && $m) {
						$len = $m[1];
					}

					$columns[$column['Field']] = array(
						'type' => preg_replace('/\([\d\,]+\)$/', '', strtolower($column['Type'])),
						'notnull' => $column['Null'] == 'NO',
						'length' => $len
					);
				}
			}

			$this->cacheSet('dbcols_' . $table_name, $columns);
		}

		$this->table_fields[$table] = $columns;

		return $columns;
	}

	private function cacheSet($name, $data)
	{
		if (is_writable(DOCROOT . 'etc/tmp')) {
			$data = "<?php defined('WEBMEX') OR die('No direct access.'); /* generated " . date('c') . " */ ?> \n" . serialize($data);

			return @file_put_contents(DOCROOT . 'etc/tmp/' . $name . '.cache.php', $data);
		}

		return false;
	}

	private function cacheGet($name)
	{
		$ttl = 60 * 60 * 2;
		$f = DOCROOT . 'etc/tmp/' . $name . '.cache.php';

		if (file_exists($f) && filemtime($f) >= time() - $ttl) {
			ob_start();

			include $f;

			return unserialize(trim(ob_get_clean()));
		}

		return null;
	}

	public function cacheDel()
	{
		if ($f = glob(DOCROOT . 'etc/tmp/*.cache.php')) {
			foreach ($f as $file) {
				@unlink($file);
			}
		}
	}

	public function quote_identifier($value)
	{
		if ($value === '*') {
			return $value;
		} else if (is_object($value)) {
			return $this->quote_identifier((string) $value);
		} else if (is_array($value)) {
			list ($value, $alias) = $value;

			return $this->quote_identifier($value) . ' AS ' . $this->quote_identifier($alias);
		}

		if (strpos($value, '"') !== false) {
			// Quote the column in FUNC("ident") identifiers
			return preg_replace('/"(.+?)"/e', '$this->quote_identifier("$1")', $value);
		} else if (strpos($value, '.') !== false) {
			$parts = explode('.', $value);

			if ($prefix = $this->table_prefix()) {
				// Get the offset of the table name, 2nd-to-last part
				// This works for databases that can have 3 identifiers (Postgre)
				$offset = count($parts) - 2;

				// Add the table prefix to the table name
				$parts[$offset] = $prefix . $parts[$offset];
			}

			// Quote each of the parts
			return implode('.', array_map(array($this, __FUNCTION__), $parts));
		} else if (preg_match('/\w+\((.*)\)|\w+\s+\w+/i', $value)) { // function
			return $value;
		} else {
			return '`' . $value . '`';
		}
	}

	public function quote_table($value)
	{
		if (is_array($value)) {
			$table = & $value[0];
		} else {
			$table = & $value;
		}

		if (is_string($table) && strpos($table, '.') === false) {
			$table = $this->table_prefix() . $table;
		}

		return $this->quote_identifier($value);
	}

	public function quote($value)
	{
		if ($value === null) {
			return 'NULL';
		} else if ($value === true) {
			return "'1'";
		} else if ($value === false) {
			return "'0'";
		} else if (is_object($value)) {
			return $this->quote((string) $value);
		} else if (is_array($value)) {
			return '(' . implode(', ', array_map(array($this, __FUNCTION__), $value)) . ')';
		} else if (is_int($value)) {
			return (int) $value;
		} else if (is_float($value)) {
			return sprintf('%F', $value);
		}

		return $this->escape($value);
	}

	public function table_prefix()
	{
		return !empty(Core::$def['db']['table_prefix']) ? Core::$def['db']['table_prefix'] : '';
	}

	public function escape($value)
	{
		return self::$connection->quote((string) $value);
	}

}

class Db_Schema
{

	protected $db;

	public function __construct($db)
	{
		$this->db = $db;
	}

	public function createTable($name, array $columns, $return_sql = false)
	{
		if (!$return_sql && $this->db->tableFields($name)) {
			return false;
		}

		$sql = 'CREATE TABLE IF NOT EXISTS ' . $this->db->quote_table($name) . ' ';

		$schema = array();
		$primary_keys = array();
		$indexes = array();
		$index_lengths = array();
		foreach ($columns as $col_name => $col_schema) {
			$schema[] = $this->compileColumnSchema($col_name, $col_schema);

			if (!empty($col_schema['primary'])) {
				$primary_keys[] = $col_name;
			}

			if (!empty($col_schema['index'])) {
				$indexes[] = $col_name;
			}

			if (!empty($col_schema['index_length'])) {
				$index_lengths[$col_name] = $col_schema['index_length'];
			}
		}

		if (count($primary_keys) > 1) {
			foreach ($schema as $i => $q) {
				$schema[$i] = preg_replace('/ PRIMARY KEY/', '', $q);
			}

			$schema[] = 'PRIMARY KEY (' . implode(', ', array_map(array(Core::$db_inst, 'quote_identifier'), $primary_keys)) . ')';
		}

		if ($this->db->_type == 'mysql' && count($indexes)) {
			foreach ($indexes as $i => $col) {
				$indexes[$i] = Core::$db_inst->quote_identifier($col) . (isSet($index_lengths[$col]) ? '(' . $index_lengths[$col] . ')' : '');
			}

			$schema[] = 'INDEX (' . implode(', ', $indexes) . ')';
		}

		$sql .= '(' . implode(', ', $schema) . ')';

		if ($this->db->_type == 'mysql') {
			$sql .= ' ENGINE=MyISAM  DEFAULT CHARSET=utf8';
		}

		if ($return_sql) {
			return $sql;
		}

		$result = Db::$connection->query($sql);

		if ($this->db->_type == 'sqlite' && count($indexes)) {
			Db::$connection->query('CREATE INDEX IF NOT EXISTS ' . $this->db->quote_identifier('index_' . $name) . ' ON ' . $this->db->quote_table($name) . ' (' . implode(', ', array_map(array(Core::$db_inst, 'quote_identifier'), $indexes)) . ')');
		}

		return $result;
	}

	public function addColumns($name, array $columns)
	{
		$cols = $this->db->tableFields($name);

		$sql = 'ALTER TABLE ' . $this->db->quote_table($name) . ' ';

		$schema = array();
		foreach ($columns as $col_name => $col_schema) {
			if (!isSet($cols[$col_name])) {
				$schema[] = 'ADD ' . $this->compileColumnSchema($col_name, $col_schema);
			}
		}

		if (empty($schema)) {
			return true;
		}

		if ($this->db->_type == 'sqlite') {
			foreach ($schema as $query) {
				Db::$connection->query($sql . $query);
			}

			$result = count($columns);
		} else {
			$sql .= implode(', ', $schema);

			$result = Db::$connection->query($sql);
		}

		$this->db->cacheDel();

		return $result;
	}

	public function renameTable($old_name, $new_name)
	{
		$t_old = $this->db->quote_table($old_name);
		$t_new = $this->db->quote_table($new_name);

		if ($this->db->_type == 'mssql') {
			$sql = 'EXEC sp_rename ' . $t_old . ', ' . $t_new . '';
		} else if ($this->db->_type == 'mysql') {
			$sql = 'ALTER TABLE ' . $t_old . ' RENAME ' . $t_new;
		} else {
			$sql = 'ALTER TABLE ' . $t_old . ' RENAME TO ' . $t_new;
		}

		return Db::$connection->query($sql);
	}

	public function changeColumnType($table, $column, $length)
	{
		if ($this->db->_type == 'mysql') {
			echo 'MySQL1: ' . 'ALTER TABLE ' . $table . ' CHANGE ' . $column . ' ' . $column . ' ' . $length;
			echo 'MySQL2: ' . 'ALTER TABLE ' . $this->db->table_prefix() . $table . ' CHANGE ' . $column . ' ' . $column . ' ' . $length;
			$sql = 'ALTER TABLE ' . $this->db->table_prefix() . $table . ' CHANGE ' . $column . ' ' . $column . ' ' . $length;
		} else
		if ($this->db->_type == 'sqlite') {
			// Not Supported by sqlite
		}
		
		return Db::$connection->query($sql);
	}

	public function dropColumn($name, $column)
	{
		$t = $this->db->quote_table($name);

		if ($this->db->_type == 'sqlite') {
			// unsupported by SQLite
			return true;
		} else {
			$sql = 'ALTER TABLE ' . $t . ' DROP COLUMN ' . $this->db->quote_identifier($column);
		}

		return Db::$connection->query($sql);
	}

	public function dropTable($name)
	{
		return Db::$connection->query('DROP TABLE ' . $this->db->quote_table($name));
	}

	public function addIndex($table, array $indexes)
	{
		if (count($indexes)) {
			if ($this->db->_type == 'mysql') {
				return Db::$connection->query('ALTER TABLE ' . $this->db->quote_table($table) . ' ADD INDEX (' . implode(', ', array_map(array(Core::$db_inst, 'quote_identifier'), $indexes)) . ')');
			} else if ($this->db->_type == 'sqlite') {
				return Db::$connection->query('CREATE INDEX IF NOT EXISTS ' . $this->db->quote_identifier('index_' . $table) . ' ON ' . $this->db->quote_table($table) . ' (' . implode(', ', array_map(array(Core::$db_inst, 'quote_identifier'), $indexes)) . ')');
			}
		}
	}

	public function compileColumnSchema($name, $schema)
	{
		$sql = $this->db->quote_identifier($name);

		$sql .= ' ' . $this->getColumnType($schema['type']);

		if (!empty($schema['length'])) {
			$sql .= '(' . $schema['length'] . ')';
		} else if (($default_length = $this->getColumnDefaultLength($schema['type'])) !== null) {
			$sql .= '(' . $default_length . ')';
		}

		if (!empty($schema['primary'])) {
			$sql .= ' PRIMARY KEY';
		}

		if (!empty($schema['autoincrement'])) {

			if ($this->db->_type == 'mysql') {
				$sql .= ' AUTO_INCREMENT';
			} else if ($this->db->_type == 'sqlite') {
				$sql .= ' AUTOINCREMENT';
			} else if ($this->db->_type == 'pgsql') {
				$sql .= ' SERIAL';
			} else if ($this->db->_type == 'mssql') {
				$sql .= ' IDENTITY';
			}
		}

		if (!empty($schema['not_null'])) {
			$sql .= ' NOT NULL';
		}

		if (!empty($schema['unique'])) {
			$sql .= ' UNIQUE';
		}

		if (array_key_exists('default', $schema)) {
			$sql .= ' DEFAULT ' . $this->quoteDefaultValue($schema['default']);
		} else if (($default_value = $this->getColumnDefaultValue($schema['type'])) !== null) {
			$sql .= ' DEFAULT ' . $this->quoteDefaultValue($default_value);
		}

		return $sql;
	}

	public function getColumnType($type)
	{
		if ($type == 'boolean') {
			return 'integer';
		} else if ($type == 'integer') {
			return 'integer';
		} else if ($type == 'decimal') {
			return 'decimal';
		} else if ($type == 'float') {
			return 'float';
		} else if ($type == 'float_long') {
			return 'float';
		} else if ($type == 'timestamp') { // better convert timestamp to integer => use unix timestamps
			return 'integer';
		} else if ($type == 'time') {
			return 'time';
		} else if ($type == 'date') {
			return 'date';
		} else if ($type == 'blob') {
			return 'blob';
		} else if ($type == 'text') {
			return 'text';
		}

		return 'varchar';
	}

	public function getColumnDefaultLength($type)
	{
		if ($type == 'boolean') {
			return 1;
		} else if ($type == 'timestamp') {
			return 10;
		} else if ($type == 'varchar') {
			return 255;
		} else if ($type == 'float') {
			return '12,2';
		} else if ($type == 'float_long') {
			return '12,6';
		}

		return null;
	}

	public function getColumnDefaultValue($type)
	{
		if ($type == 'boolean') {
			return 0;
		}

		return null;
	}

	public function quoteDefaultValue($value)
	{
		if (in_array(strtolower($value), array('current_timestamp'))) {
			if ($this->db->_type == 'sqlite') {
				return '(' . strtoupper($value) . ')';
			}

			return strtoupper($value);
		}

		return $this->db->quote($value);
	}

}

class Db_Dump
{

	protected $db, $filename;

	public function __construct($db, $filename)
	{
		$this->db = $db;
		$this->filename = $filename;

		if (!$this->filename) {
			throw new Exception('Invalid target file');
		}

		file_put_contents($this->filename, "/*** MYSQL DUMP " . date('r') . " ***/\n\n");
	}

	private function write($str)
	{
		file_put_contents($this->filename, $str, FILE_APPEND);
	}

	public function dump()
	{
		@set_time_limit(120);

		$result = '';

		$tables = array();
		$stm = $this->db->query('SHOW TABLES');

		foreach ($stm as $table) {
			$tables[] = $table;
		}

		unset($stm);

		foreach ($tables as $table) {
			$this->dump_table($table[0]);
		}

		return true;
	}

	public function dump_table($table)
	{
		$stm = $this->db->query('SHOW CREATE TABLE `' . $table . '`');

		if (!($r = $stm->fetch())) {
			return false;
		}

		$this->write($r[1] . ";\n\n");

		$stm = $this->db->query('SELECT * FROM `' . $table . '`');
		$stm->setFetchMode(PDO::FETCH_ASSOC);

		$result = '';

		foreach ($stm as $i => $row) {
			if ($i % 10 === 0) {
				if ($i > 0) {
					$result = preg_replace('/,\n$/', ";\n\n", $result);

					$this->write($result);
					$result = '';
				}

				$result .= "INSERT INTO `" . $table . "` (";

				foreach ($row as $col => $val) {
					$result .= "`" . $col . "`, ";
				}
				$result = rtrim($result, ', ');
				$result .= ") VALUES\n";
			}

			$result .= "(";
			foreach ($row as $col => $val) {
				if ($val === null) {
					$result .= 'NULL, ';
				} else if (is_numeric($val) && !preg_match('/^0\d+/', $val)) {
					$result .= '' . $val . ', ';
				} else {
					$val = addslashes($val);
					$result .= '"' . $val . '", ';
				}
			}
			$result = rtrim($result, ', ');
			$result .= "),\n";
		}

		unset($stm);

		$result = preg_replace('/,\n$/', ";\n\n", $result);

		$this->write($result);

		return true;
	}

}