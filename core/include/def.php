<?php

defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */
Core::$def['db'] = array(
	'dsn' => 'sqlite:' . DOCROOT . 'etc/data.sqlite.php',
	'user' => null,
	'password' => null
);

Core::$def['timezone'] = 'Europe/Prague';

Core::$def['page_types'] = array(
	1 => __('main_menu'),
	2 => __('categories'),
	3 => __('producers'),
	4 => __('top_menu'),
	5 => __('hidden_pages'),
	6 => __('footer_menu_1'),
	7 => __('footer_menu_2'),
	8 => __('footer_menu_3'),
	9 => __('footer_menu_4'),
);

Core::$def['block_columns'] = array(
	'left' => __('column_left'),
	'right' => __('column_right')
);

Core::$def['banner_types'] = array(
	0 => __('banner_type_0'),
	1 => __('banner_type_1'),
	2 => __('banner_type_2')
);

Core::$def['filemenager_root'] = 'files/';
Core::$def['quick_upload_dir'] = '';

Core::$def['image_sizes'] = array(
	-1 => __('automatic'),
	0 => __('original'),
	1 => '60x60',
	2 => '120x120',
	3 => '200x200',
	4 => '250x250',
	5 => '300x300',
	6 => '400x400',
	7 => '500x500',
	7 => '700x700',
	8 => '640x480',
	9 => '1024x1024',
	10 => '1200x1200'
);

Core::$def['image_watermark'] = array(
	'apply_on_original' => false,
	'min_size' => 250,
	'type' => 'text', // image / text
	'filepath' => 'files/watermark.png',
	'text' => 'Copyright My Store',
	'alignment' => 'c', // tl, tr, bl, br, c
	'font_color' => array(255, 255, 255),
	'font_size' => 20,
	'opacity' => 50,
	'png24' => false,
	'text_shadow' => true,
);

Core::$def['price_formats'] = array(
	// decimals, dec_point, thousands_sep
	1 => array(2, '.', ''),
	2 => array(2, '.', ','),
	3 => array(2, ',', ' '),
	4 => array(0, '.', ''),
	5 => array(0, '.', ','),
	6 => array(0, ',', ' '),
);

Core::$def['email'] = array(
	'charset' => 'utf-8',
	'smtp_server' => null,
	'smtp_port' => 25,
	'smtp_user' => null,
	'smtp_password' => null
);

Core::$def['order_status'] = array(
	1 => 'pending',
	2 => 'in_progress',
	5 => 'for_shipment',
	3 => 'finished',
	4 => 'canceled',
	6 => 'confirmed',
	7 => 'updated',
	8 => 'waiting_for_payment',
	9 => 'paid',
	10 => 'waiting_for_pickup',
	11 => 'sent',
);

Core::$def['permissions'] = array(
	10000 => 'all',
	500 => 'dashboard-statistics',
	1000 => 'pages',
	2000 => 'products',
	2001 => 'products-edit',
	2002 => 'products-features',
	2003 => 'products-export_import',
	2004 => 'products-xml_feeds',
	3000 => 'orders',
	3001 => 'orders-view',
	3002 => 'orders-invoices',
	1100 => 'blocks',
	1200 => 'customers',
	4000 => 'users',
	5000 => 'backup',
	6000 => 'design',
	7000 => 'settings',
	8000 => 'update',
	9000 => 'import',
);

Core::$def['admin_template'] = 'default';

Core::$def['enable_short_links'] = false;

Core::$def['strip_request_uri'] = '';

Core::$def['db_library'] = version_compare(PHP_VERSION, '5.3', '<') ? 'odb' : 'notorm';