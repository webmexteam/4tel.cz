<?php

defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */
class Model
{

	public $row;

	public function __construct(& $row)
	{
		$this->row = $row;
	}

}