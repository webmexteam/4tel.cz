<?php

defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */
class Model_Voucher_generator extends Model
{

	public function isValid()
	{
		if ((int) $this->row->get('status')
				&& ($this->row->get('max_quantity') === null || $this->row->get('max_quantity') > $this->row->get('used'))
				&& (!$this->row->get('valid_from') || $this->row->get('valid_from') <= time())
				&& (!$this->row->get('valid_to') || $this->row->get('valid_to') >= time())) {

			return true;
		}

		return false;
	}

	public function generate($order_total, $name = '')
	{
		$code = strtoupper(random(mt_rand(8, 10)));
		$i = 0;

		while ($i < 20 && Core::$db->voucher()->where('code', $code)->fetch()) {
			$code = strtoupper(random(mt_rand(8, 10)));

			$i++;
		}

		if ($this->row->get('type') == 2) {
			// percentual
			$percent = parseFloat(preg_replace('/\s|\%/', '', $this->row->get('value')));

			$value = $percent . '%';
		} else {
			$value = round(estPrice($this->row->get('value'), $order_total), (int) Core::config('order_round_decimals'));
		}

		if ($value) {
			$voucher_id = Core::$db->voucher(array(
				'code' => $code,
				'name' => $name,
				'value' => $value,
				'max_quantity' => 1,
				'status' => 1,
				'generator_id' => $this->row->get('id')
					));

			if ($voucher_id) {
				$this->row->update(array('used' => $this->row->get('used') + 1));
				return Core::$db->voucher[$voucher_id];
			}
		}

		return null;
	}

}