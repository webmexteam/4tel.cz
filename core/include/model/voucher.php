<?php

defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */
class Model_Voucher extends Model
{

	public function isValid()
	{
		if ((int) $this->row->get('status')
				&& ($this->row->get('max_quantity') === null || $this->row->get('max_quantity') > $this->row->get('used'))
				&& (!$this->row->get('valid_from') || $this->row->get('valid_from') <= time())
				&& (!$this->row->get('valid_to') || $this->row->get('valid_to') >= time())) {

			return true;
		}

		return false;
	}

}