<?php

defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */
class Model_Order extends Model
{

	public function getTotal_incl_vat($recalc = false)
	{
		if (!$recalc && $t = (float) $this->row->get('total_incl_vat')) {
			return $t;
		}

		$total = 0;

		if ($this->row) {
			foreach ($this->row->order_products() as $product) {
				$total += price_vat($product['price'] * $product['quantity'], $product['vat'])->price;
			}
		}

		return $total;
	}

	public function getTotal_excl_vat()
	{
		$total = 0;

		if ($this->row) {
			foreach ($this->row->order_products() as $product) {
				$total += price_unvat($product['price'] * $product['quantity'], $product['vat'])->price;
			}
		}

		return $total;
	}

	public function insert(array $data)
	{
		$data['vat_payer'] = (int) Core::config('vat_payer');

		Event::run('Model_Order::insert', $data);

		return $data;
	}

	public function update(array $data, $row = null)
	{
		if ($data) {
			$total_excl_vat = $total_vat = 0;

			$voucher = null;

			if ($row) {
				if (is_object($row)) {
					$row = $row->as_array();
				}

				if ($row['voucher_id']) {
					$voucher = Core::$db->voucher[$row['voucher_id']];
				}

				$mail_data = array_merge($row, $data);

				$items = '';

				foreach (Core::$db->order_products()->where('order_id', $row['id']) as $product) {
					if ($product['product_id']) {
						$subtotal += $product['price'] * $product['quantity'];
					}

					$total_vat += price_vat($product['price'] * $product['quantity'], $product['vat'])->price;
					$total_excl_vat += price_unvat($product['price'] * $product['quantity'], $product['vat'])->price;

					$items .= ($product['sku'] ? '' . $product['sku'] . ' - ' : '') . $product['name'] . ' -  ' .
							preg_replace('/\&nbsp;/', ' ', price_vat($product['price'], $product['vat'])) . ' * ' . $product['quantity'] . ' = ' . preg_replace('/\&nbsp;/', ' ', price_vat($product['price'] * $product['quantity'], $product['vat'])) . "\n";
				}

				$mail_data['items'] = $items;
			}

			if ($voucher) {
				$discount = estPrice($voucher['value'], $total_vat);
				$total_vat -= $discount;

				if ($total_vat < 0) {
					$total_vat = 0;
				}

				$discount = estPrice($voucher['value'], $total_excl_vat);
				$total_excl_vat -= $discount;

				if ($total_excl_vat < 0) {
					$total_excl_vat = 0;
				}
			}

			$mail_data['total'] = $mail_data['total_price'] = $subtotal;
			$mail_data['total_incl_vat'] = round($total_vat, (int) Core::config('order_round_decimals'));
			$mail_data['total_excl_vat'] = round($total_excl_vat, (int) Core::config('order_round_decimals'));

			if ($row && $row['status'] != 3 && $data['status'] == 3) {

				if (!($customer = $row->customer) || !$customer['id']) {
					$customer = $row['email'];
				}

				Voucher::generate('order_finish', $customer, $row);
			}

			$order = Core::$db->order[$row['id']];

			if ((int) Core::config('confirm_orders') && $row && !$row['confirmed'] && $data['confirmed']) {

				$order['confirmed'] = time();
				$email_data = array_merge(array('order' => $order), $mail_data);

				Email::event('order_confirmed', $row['email'], null, $email_data);
			}

			if ($row && $data['status'] && $row['status'] != $data['status']) {

				if ((int) Core::config('send_order_status') && (int) $data['send_email_to_customer']) {
					$m = array(
						'2' => 'in_process',
						'3' => 'finished',
						'4' => 'canceled',
						'5' => 'to_send',
						'6' => 'confirmed',
						'7' => 'updated',
						'8' => 'waiting_for_payment',
						'9' => 'paid',
						'10' => 'waiting_for_pickup',
						'11' => 'sent',
					);

					$email_data = array_merge(array('order' => $order), $data);
					$email_data['order_status'] = $data['status'];

					Email::event('order_status_' . $m[$data['status']], $row['email'], null, $email_data);
				}
			}
		}

		Event::run('Model_Order::update', $row, $data);

		return $data;
	}

	public function hash()
	{
		return md5($this->row->get('id') . $this->row->get('email') . $this->row->get('received'));
	}

}