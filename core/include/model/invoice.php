<?php

defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */
class Model_Invoice extends Model
{

	public function getTotal_incl_vat($recalc = false)
	{
		$total = 0;

		if ($this->row) {
			foreach ($this->row->invoice_items() as $product) {
				$total += price_vat($product['price'] * $product['quantity'], $product['vat'])->price;
			}
		}

		return $total;
	}

	public function getTotal_excl_vat()
	{
		$total = 0;

		if ($this->row) {
			foreach ($this->row->invoice_items() as $product) {
				$total += price_unvat($product['price'] * $product['quantity'], $product['vat'])->price;
			}
		}

		return $total;
	}

	public function download($filename = null)
	{
		$inv = new Invoice();

		$inv->vat = $this->row['vat'];
		$inv->invoice_num = $this->row['invoice_num'];
		$inv->payment = $this->row['payment'];
		$inv->delivery = $this->row['delivery'];
		$inv->symbol = $this->row['symbol'];
		$inv->date_issue = $this->row['date_issue'];
		$inv->date_due = $this->row['date_due'];
		$inv->date_vat = $this->row['date_vat'];
		$inv->date_create = $this->row['date_create'];

		$inv->note = $this->row['note'];
		$inv->status = $this->row['status'];
		$inv->voucher_id = $this->row['voucher_id'];
		$inv->voucher_id = $this->row['voucher_id'];

		$inv->type = $this->row['type'];
		$inv->vat_payer = $this->row['vat_payer'];
		$inv->shipping_address = $this->row['shipping_address'];

		if (!$this->row['customer_name']) {
			$inv->customer = $this->row['customer']; // for backward compatibility (version 1.2)
		} else {
			$inv->customer = $this->row['customer_name'] . "\n" . $this->row['address']
					. "\n\n" . __('company_id') . ': ' . $this->row['company_id']
					. "     " . __('company_vat') . ': ' . $this->row['company_vat'];
		}

		$inv->issuer = Core::config('invoice_issuer');
		$inv->footer = Core::config('invoice_footer');

		foreach ($this->row->invoice_items() as $item) {
			$inv->add($item['sku'], $item['name'], $item['quantity'], $item['price'], $item['vat']);
		}

		if ($filename) {
			$inv->save($filename, 'F');
		} else {
			$inv->save($this->row['invoice_num'] . '.pdf', 'D');
		}
	}

}