<?php

defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */
class Model_Product_attributes extends Model
{
	public static $customer_group;

	public function getPrice()
	{
		$price = $this->row->get('price');
		$discount = 0;
		$percents = false;

		if($price === null) return null;

		if (!Core::$is_admin && !(int) Core::$db->product[(int) $this->row['product_id']]['ignore_results']) {

			// discounts form customer groups
			/*
			if (Core::$is_premium && !self::$customer_group) {
				$group_id = Core::config('customer_group_default');

				if (Customer::$logged_in) {
					$group_id = Core::config('customer_group_registered');

					if ((int) Customer::get('customer_group_id')) {
						$group_id = Customer::get('customer_group_id');
					}
				}

				self::$customer_group = Core::$db->customer_group[(int) $group_id];
			}

			if (Core::$is_premium && self::$customer_group) {
				$coef = (float) self::$customer_group['coefficient'];

				if (preg_match('/^([\d\-\+\.\,]+)\%$/', trim($price), $matches)) {
					$price = parseFloat($matches[1]) * $coef;
					$percents = true;
				} else if (preg_match('/^([\d\-\+\.\,]+)$/', trim($price), $matches)) {
					$price = parseFloat($matches[1]) * $coef;
				}
			}
			*/
			
			if(Core::$is_premium && Customer::get('discount'))
			{
				$price = $price * (1 - ((float) Customer::get('discount') / 100));
			}
			
		}

		return $price;
	}
}