<?php

defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */
class Zip
{

	protected $archive, $files = array(), $empty_dirs = array(), $driver_name, $driver;

	public function __construct($archive_filename)
	{
		$this->archive = $archive_filename;

		if (class_exists('ZipArchive')) {
			$this->driver_name = 'ZipArchive';
			$this->driver = new ZipArchive();
		} else {
			require_once(APPROOT . 'vendor/pclzip/pclzip.lib.php');

			$this->driver_name = 'PclZip';
			$this->driver = new PclZip($this->archive);
		}
	}

	public function addFile($file, $localname = null)
	{
		if (is_array($file)) {
			return array_map(array($this, 'addFile'), $file);
		} else if (strrpos($file, ',') !== false) {
			return array_map(array($this, 'addFile'), explode(',', $file));
		}

		if (in_array($file, $this->files)) {
			return false;
		}

		if (is_dir($file)) {
			return $this->addDir($file);
		}

		if (!is_readable($file)) {
			return false;
		}

		$this->files[] = array($file, $localname);

		return true;
	}

	public function addDir($dirname)
	{
		$dirname = rtrim($dirname, '/');

		if (!is_readable($dirname)) {
			return false;
		}

		$files = glob($dirname . '/*');

		if ($files === false) {
			return $this->addEmptyDir($dirname);
			;
		}

		foreach ($files as $file) {
			if (is_dir($file)) {
				$this->addDir($file);
			} else {
				$this->addFile($file);
			}
		}

		return true;
	}

	public function addEmptyDir($dirname)
	{
		$this->empty_dirs[] = $dirname;

		return true;
	}

	public function removeFile($file)
	{
		if (($key = array_search($file, $this->files)) !== false) {
			unset($this->files[$key]);
			return true;
		}

		return false;
	}

	public function removeDir($dir)
	{
		$removed = 0;

		foreach ($this->files as $i => $file) {
			if (preg_match('#^' . $dir . '#', (string) $file[0])) {
				unset($this->files[$i]);
				$removed++;
			}
		}

		return $removed;
	}

	public function create()
	{
		if ($this->driver_name == 'ZipArchive') {
			if ($this->driver->open($this->archive, ZipArchive::OVERWRITE)) {
				foreach ($this->files as $file) {
					$this->driver->addFile($file[0], $file[1]);
				}

				foreach ($this->empty_dirs as $dir) {
					$this->driver->addEmptyDir($dir);
				}

				return $this->driver->close();
			}
		} else {
			$this->driver->create(array());

			foreach ($this->files as $file) {
				if ($file[1]) {
					$remove_path = substr($file[0], 0, strlen($file[1]) * -1);

					$this->driver->add($file[0], PCLZIP_OPT_REMOVE_PATH, $remove_path);
				} else {
					$this->driver->add($file[0]);
				}
			}

			foreach ($this->empty_dirs as $dir) {
				$this->driver->add($dir . '/');
			}

			return true;
		}

		return false;
	}

	public function extract($target = null)
	{
		if (!$target) {
			$pathinfo = pathinfo($this->archive);

			$target = $pathinfo['dirname'];
		}

		$target = rtrim($target, '/');

		if ($this->driver_name == 'ZipArchive') {
			if ($this->driver->open($this->archive)) {
				$return = $this->driver->extractTo($target);
				$this->driver->close();

				return $return;
			}
		} else {
			$this->driver->extract(PCLZIP_OPT_PATH, $target, PCLZIP_OPT_SET_CHMOD, 0777);

			echo $this->driver->errorInfo(true);
		}

		return false;
	}

}