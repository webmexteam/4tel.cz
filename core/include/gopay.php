<?php

defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */
class Gopay
{

	public static $goId;
	public static $secret;
	public static $test;
	public static $gpClass = 'GopaySoap';
	public static $order_model = 'order';
	public static $url_callback = 'gopay/callback';
	public static $url_success = 'gopay/success';
	public static $url_waiting = 'gopay/waiting';
	public static $url_fail = 'gopay/fail';

	public static function setup()
	{
		if (!self::$gpClass || !self::$goId) {
			if (!class_exists('SoapClient', false)) {
				throw new Exception('SoapClient library not available');
			}

			require_once(DOCROOT . 'etc/gopay.php');

			require_once(APPROOT . 'vendor/gopay/gopay_config.php');
			require_once(APPROOT . 'vendor/gopay/gopay_helper.php');
			require_once(APPROOT . 'vendor/gopay/gopay_soap.php');
			require_once(APPROOT . 'vendor/gopay/gopay_soap.php');

			self::$goId = $gopay['goId'];
			self::$secret = $gopay['secret'];
			self::$test = (bool) $gopay['test'];

			if (self::$test) {
				GopayConfig::init(GopayConfig::TEST);
			} else {
				GopayConfig::init(GopayConfig::PROD);
			}

			if (empty(self::$goId) || empty(self::$secret)) {
				throw new Exception('Wrong Gopay configuration');
			}
		}
	}

	public static function pay($order, $channels = null, $default_channel = null, $send_customer_data = true)
	{
		if (!$order || !$order['id']) {
			return false;
		}

		$desc = $order['first_name'] . ' ' . $order['last_name'] . ', ' . fdate($order['received'], true);

		$method = 'createEshopPayment';

		$post_data = array(
			self::$goId + 0,
			$desc,
			round((float) $order['total_incl_vat'] * 100, 0),
			$order['id'],
			url(self::$url_callback, null, true),
			url(self::$url_callback, null, true),
			self::$secret,
			$channels
		);

		if ($send_customer_data) {
			$method = 'createCustomerEshopPayment';

			$country = 'CZE';

			if (preg_match('/slovensk|slovakia/iu', $order['country'])) {
				$country = 'SLO';
			} else if (preg_match('/polsk|poland/iu', $order['country'])) {
				$country = 'PLN';
			}

			$post_data = array_merge($post_data, array(
				$order['first_name'],
				$order['last_name'],
				$order['city'],
				$order['street'],
				preg_replace('/\s/', '', $order['zip']),
				$country,
				$order['email'],
				preg_replace('/\s/', '', $order['phone'])
					));
		}

		$paymentSessionId = call_user_func_array(array(self::$gpClass, $method), $post_data);

		if ($paymentSessionId > 0) {
			$order->update(array(
				'payment_session' => preg_replace('/\.[0]+$/', '', $paymentSessionId),
				'payment_description' => $desc
			));

			$encryptedSignature = GopayHelper::encrypt(
							GopayHelper::hash(GopayHelper::concatPaymentSession(
											self::$goId + 0, $paymentSessionId, self::$secret
									)), self::$secret
			);

			redirect(GopayConfig::fullIntegrationURL() . "?sessionInfo.eshopGoId=" . self::$goId . "&sessionInfo.paymentSessionId=" . $paymentSessionId . "&sessionInfo.encryptedSignature=" . $encryptedSignature . ($default_channel ? "&paymentChannel=" . strtoupper($default_channel) : ''));

			return true;
		} else {
			// error
			return $paymentSessionId;
		}

		return false;
	}

	public static function callback($data = null, $client_side = true)
	{
		if (!($result = self::validateCallback($data))) {
			return false;  // TODO: send 500 header
		}

		$status = $result[0];
		$order = $result[1];

		if ($client_side) {
			$_SESSION['gopay_order_id'] = $order['id'];

			session_write_close();
		}

		$params = array(
			$data['paymentSessionId'],
			self::$goId,
			$data['variableSymbol'],
			$order['total_incl_vat'] * 100,
			$order['first_name'] . ' ' . $order['last_name'] . ', ' . fdate($order['received'], true),
			self::$secret
		);
		$paymentInfo = call_user_func_array(
			array(self::$gpClass, 'isBuyerPaymentDone'), 
			$params
		);

		if ($status['code'] == GopayHelper::PAYMENT_DONE) {
			$order->update(array(
				'payment_realized' => time(),
				'payment_status' => 'DONE'
			));

			Notify::payment('Gopay.cz', $order);

			if ($client_side) {
				redirect(url(self::$url_success, array('order' => $order['id']), true));
			}

			return true;
		} else if ($status['code'] == GopayHelper::WAITING) {
			$order->update(array(
				'payment_status' => 'WAITING'
			));

			if ($client_side) {
				redirect(url(self::$url_waiting, null, true));
			}
		} else if ($status['code'] == GopayHelper::CANCELED) {
			$order->update(array(
				'payment_status' => 'CANCELED'
			));

			if ($client_side) {
				redirect(url(self::$url_fail, null, true));
			}
		} else if ($status['code'] == GopayHelper::TIMEOUTED) {
			$order->update(array(
				'payment_status' => 'TIMEOUTED'
			));

			if ($client_side) {
				redirect(url(self::$url_fail, null, true));
			}
		} else {
			// payment canceled or fraud
			if ($client_side) {
				redirect(url(self::$url_fail, null, true));
			} else {
				header('HTTP/1.1 500 Internal Server Error');
				exit(0);
			}
		}

		return false;
	}

	private static function validateCallback($data = null)
	{
		if ($data === null) {
			$data = $_GET;
		}

		if (empty($data)) {
			return false;
		}

		$order_model = self::$order_model;

		$paymentSessionId = $data['paymentSessionId'];
		$goId = $data['eshopGoId'];
		$variableSymbol = $data['variableSymbol'];
		$encryptedSignature = $data['encryptedSignature'];

		if (empty($paymentSessionId) || empty($goId) || empty($variableSymbol) || empty($encryptedSignature)) {
			return false;
		}

		if (!($order = Core::$db->$order_model()->where('payment_session', $paymentSessionId)->fetch())
				/**//*|| (int) $order['payment_realized']*/) {

			return false;
		}
		$order->update(array('encrypted_signature' => $encryptedSignature));

		$is_valid = GopayHelper::checkPaymentIdentity(
						$goId + 0, $paymentSessionId, $variableSymbol, $encryptedSignature, self::$goId, $order['id'], self::$secret
		);

		if (!$is_valid) {
			// fraud
			return array(-1, $order);
		}

		$result = call_user_func_array(array(self::$gpClass, 'isEshopPaymentDone'), array(
			$paymentSessionId + 0,
			self::$goId + 0,
			$order['id'],
			round((float) $order['total_incl_vat'] * 100, 0),
			$order['payment_description'],
			self::$secret
				));

		return array($result, $order);
	}

	public static function notify($data = null)
	{
		return self::callback($data, false);
	}

	public static function success($data = null)
	{
		return self::validateCallback($data);
	}

}