<?php

defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */
Payment::$drivers[] = 'gopay';

class Payment_Gopay extends Payment
{

	public $name = 'Gopay.cz';
	public $config = array(
		'channels' => array(),
		'default_channel' => null
	);

	public function __construct($payment = null)
	{
		parent::__construct($payment);
	}

	public function getConfigForm()
	{
		return tpl('payments/config_gopay.latte', array('payment' => $this));
	}

	public function getPaymentLink($order)
	{
		return url('gopay/process_payment/' . $order['id'] . '_' . md5($order['id'] . $order['received'] . $order['email']), null, true);
	}

	public function process($order)
	{
		Gopay::setup();

		if (Gopay::pay($order, $this->config['channels'], $this->config['default_channel']) !== true) {
			redirect('gopay/error');
		}
	}

	public function getPaymentInfo($order)
	{
		if ((int) Core::config('confirm_orders') && !(int) $order['confirmed']) {
			return (string) __('wait_for_order_confirmation');
		}

		return $this->payment_info ? $this->payment_info : (string) __('order_payment_link', $this->getPaymentLink($order));
	}

}