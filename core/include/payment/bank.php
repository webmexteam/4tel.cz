<?php

defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */
Payment::$drivers[] = 'bank';

class Payment_Bank extends Payment
{

	public $name;

	public function __construct($payment = null)
	{
		$this->name = __('bank_transfer');

		parent::__construct($payment);
	}

	public function getPaymentInfo($order)
	{
		if ((int) Core::config('confirm_orders') && !(int) $order['confirmed']) {
			return (string) __('wait_for_order_confirmation');
		}

		return $this->payment_info;
	}

}