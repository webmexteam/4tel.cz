<?php

defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */
define('FPDF_FONTPATH', APPROOT . 'vendor/fpdf/font/');
require(APPROOT . 'vendor/fpdf/fpdf.php');

class WEBMEX_FPDF extends FPDF
{

	public $footer_text;
	public $date_from;
	public $date_to;

	public function Footer()
	{
		$this->SetY(-15);
		$this->SetFont('Freemono', '', 7);

		$text = preg_replace('/\{date\}/', fdate($this->date, true), $this->footer_text);

		$this->Cell(129, 10, iconv('utf-8', 'windows-1250', $text));
		$this->Cell(60, 10, iconv('utf-8', 'windows-1250', Core::config('store_name') . ' - ' . __('orders') . ' ' . ($this->date_from ? (fdate($this->date_from) . ' - ' . fdate($this->date_to)) : '') . ' | ' . $this->PageNo() . '/{nb}'), 0, 0, 'R');
	}

}

class Pdf_Orders
{

	public $pdf;
	protected $orders = array();
	protected $fontsize = 10;

	public function __construct($date_from, $date_to)
	{
		$this->pdf = new WEBMEX_FPDF();

		if (is_numeric($date_from) && $date_to) {
			$this->pdf->date_from = $date_from;
			$this->pdf->date_to = $date_to;
		}

		$this->pdf->AliasNbPages();

		$this->pdf->AddFont('Freemono', '', 'freemono.php');
		$this->pdf->AddFont('Freemono', 'B', 'freemonob.php');

		$this->pdf->AddPage('P', 'A4');
	}

	public function iconv($text)
	{
		return iconv('utf-8', 'windows-1250', $text);
	}

	public function cell($w, $h, $text, $border = 0, $ln = 0, $align = 'L', $fill = false)
	{
		$this->pdf->Cell($w, $h, $this->iconv($text), $border, $ln, $align, $fill);
	}

	public function multicell($w, $h, $text, $border = 0, $align = 'L')
	{
		$text = preg_replace('/\t/', '    ', $text);

		$this->pdf->MultiCell($w, $h, $this->iconv($text), $border, $align);
	}

	public function hline($ln = 2)
	{
		$this->moveY($ln);
		$this->pdf->Line($this->pdf->GetX(), $this->pdf->GetY(), $this->pdf->GetX() + 189, $this->pdf->GetY());
		$this->moveY($ln);
	}

	public function moveX($change)
	{
		$this->pdf->SetX($this->pdf->GetX() + $change);
	}

	public function moveY($change)
	{
		$this->pdf->SetY($this->pdf->GetY() + $change);
	}

	public function save($name = null, $dest = 'I')
	{
		$this->render();
		$this->pdf->Output($name, $dest);
	}

	public function render()
	{
		$this->pdf->SetFillColor(220, 220, 220);
		$this->pdf->SetDrawColor(180, 180, 180);
		$this->pdf->SetFont('Freemono', '', $this->fontsize);

		if (!count($this->orders)) {
			$this->cell(64, $line_height, __('no_order_found'));
		}

		foreach ($this->orders as $order) {
			$this->render_order($order);
		}
	}

	public function render_order($order)
	{
		$line_height = $this->fontsize / 2 + 1;

		$products = $order->order_products();

		$lines_required = 7.5 + count($products);

		if (($this->pdf->h - $this->pdf->GetY()) <= ($lines_required * $line_height)) {
			$this->pdf->AddPage();
		}

		$this->cell(90, $line_height, __('order_number') . ': ' . $order['id'] . '; ' . fdate($order['received'], true), 0);

		if ($order['payment_realized']) {
			$this->cell(64, $line_height, __('payment_realized') . ' ' . $order['payment_session'], 0, 0, 'C', true);
		} else {
			$this->cell(64, $line_height, '');
		}

		$this->moveX(1);
		$this->cell(35, $line_height, __('status_' . Core::$def['order_status'][$order['status']]), 0, 0, 'C', true);

		$this->pdf->Ln(8);

		$qty = 0;
		foreach ($order->order_products() as $product) {
			$this->cell(35, $line_height, $product['quantity'] . ' * ' . preg_replace('/\&nbsp;/', ' ', price_unvat($product)), 0);
			$this->cell(30, $line_height, $product['sku'], 0);
			$this->multicell(125, $line_height - 2, $product['name'], 0);
			$this->pdf->Ln(1.5);

			$qty += (int) $product['quantity'];
		}

		$this->pdf->Ln(2);
		
		$this->cell(99, $line_height, 'Celkem bez DPH: ' . preg_replace('/\&nbsp;/', ' ', price((float) $order['total_excl_vat'])), 0);
		$y = $this->pdf->GetY();
		$x = $this->pdf->GetX();
		$this->pdf->SetY($y+4);
		$this->pdf->SetX($x-99);
		
		$vat = sprintf('%.2f', $order['total_excl_vat'])*0.21;
		
		$this->cell(99, $line_height, 'DPH: ' . preg_replace('/\&nbsp;/', ' ', price((float) $vat)), 0);
		$y = $this->pdf->GetY();
		$x = $this->pdf->GetX();
		$this->pdf->SetY($y+4);
		$this->pdf->SetX($x-99);

		$total_order = ($order['total_excl_vat']+(sprintf('%.2f', $order['total_excl_vat'])*0.21));
		
		$total_rounded = number_format((round($total_order) - round($total_order, 2)), 2, ',', ' ');
		
		if ((round($total_order) - $total_order) > 0) {
		    $total_rounded = '+' . $total_rounded;
		}
		  
		
		$this->cell(99, $line_height, 'Zaokrouhleno: ' . preg_replace('/\&nbsp;/', ' ', $total_rounded.' Kč'), 0);
		$y = $this->pdf->GetY();
		$x = $this->pdf->GetX();
		$this->pdf->SetY($y+4);
		$this->pdf->SetX($x-99);
		
		$this->cell(99, $line_height, 'Celkem vč. DPH: ' . preg_replace('/\&nbsp;/', ' ', price((float) round($total_order))), 0);
		
		$this->cell(90, $line_height, '', 0);
		$this->pdf->Ln($line_height + 2);

		$y = $this->pdf->GetY();
		$x = $this->pdf->GetX();

		$addr = '';
		if ($order['company']) {
			$addr .= $order['company'] . ', ';
		}
		$addr .= $order['first_name'] . ' ' . $order['last_name'] . ', ' . $order['street'] . ', ' . $order['city'] . ' ' . $order['zip'] . ', ' . $order['country'];

		$this->multicell(90, $line_height - 1, $addr, 0);
		$this->cell(0, $line_height, __('company_id') . ': ' . str_pad($order['company_id'], 12) . ' ' . __('company_vat') . ': ' . $order['company_vat'], 0);
		$this->pdf->Ln($line_height - 1);
		$this->cell(0, $line_height, $order['email'] . '; ' . $order['phone'], 0);
		$y2 = $this->pdf->GetY();

		$this->pdf->SetY($y);
		$this->pdf->SetX($x + 99);

		$addr = '';
		if ($order['ship_street']) {
			if ($order['ship_company']) {
				$addr .= $order['ship_company'] . ', ';
			}
			$addr .= $order['ship_first_name'] . ' ' . $order['ship_last_name'] . ', ' . $order['ship_street'] . ', ' . $order['ship_city'] . ' ' . $order['ship_zip'] . ', ' . $order['ship_country'];
		}

		$this->multicell(90, $line_height - 1, $addr, 0);
		$y3 = $this->pdf->GetY();

		$this->pdf->SetY(max($y2, $y3));

		$this->pdf->Ln($line_height);
		$this->hline();
	}

	public function addOrder($order)
	{
		$this->orders[] = $order;
	}

}