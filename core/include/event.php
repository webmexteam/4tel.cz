<?php

defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */
class Event
{

	protected static $events = array();
	public static $data;

	public static function add($event, $callback)
	{
		if (!isSet(self::$events[$event])) {
			self::$events[$event] = array();
		}

		self::$events[$event][] = $callback;
	}

	//-- Stupid PHP 5.2... but it works
	public static function run($event, &$arg1 = null, &$arg2 = null, &$arg3 = null, &$arg4 = null, &$arg5 = null, &$arg6 = null, &$arg7 = null)
	{
		if (empty(self::$events[$event])) {
			return;
		}

		foreach (self::$events[$event] as $callback) {
			call_user_func_array($callback, array(&$arg1, &$arg2, &$arg3, &$arg4, &$arg5, &$arg6, &$arg7));
		}
	}

}