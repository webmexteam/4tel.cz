#!/usr/bin/php
<?php
error_reporting(E_ALL & ~E_STRICT & ~E_NOTICE);

ini_set('display_errors', true);

$pathinfo = pathinfo(__FILE__);
$dirname = substr($pathinfo['dirname'], 0, -4);

define('SQC', true);
define('WEBMEX', true);
define('CRON', true);
define('DOCROOT', $dirname . '/');
define('APPDIR', 'core');
define('APPROOT', DOCROOT . APPDIR . '/');

set_include_path(DOCROOT);

require_once(APPROOT . 'include/core.php');

Core::setup(true);

if (isSet($_SERVER['TERM']) || empty($_SERVER['HTTP_HOST'])) {
	// CLI

	define('IS_CLI', true);

	require_once(APPROOT . 'include/cli.php');
	$args = parseArgs($argv);
} else {
	// http
	if (empty($_GET['k']) || $_GET['k'] != Core::config('access_key')) {
		die('Access denied. Please verify yourself by passing access key (cron.php?k=XXX).');
	}

	$args = $_GET;

	echo '<pre>';
}

if (!empty($args['job'])) {

	if ($args['job'] == 'cleancache') {
		if ($f = glob(DOCROOT . 'etc/tmp/*')) {
			foreach ($f as $file) {
				FS::remove($file);
			}
		}
	}

	if ($args['job'] == 'backup') {
		try {
			if ($file = runController('backup', 'create')) {
				echo "Backup created: " . $file . "\n";
			}
		} catch (Exception $e) {
			die("Error: " . $e->getMessage() . "\n");
		}
	}

	if ($args['job'] == 'dataimport') {
		if (empty($args['schema'])) {
			die("Error: missing --schema argument\n");
		}

		if (!file_exists(DOCROOT . 'etc/xml_import/' . $args['schema'] . '.php')) {
			die("Error: schema not found\n");
		}

		try {
			if (runController('dataimport', 'cron', array($args['schema'])) === false) {
				die('Unknown error');
			}
		} catch (Exception $e) {
			die("Error: " . $e->getMessage() . "\n");
		}
	}

	if ($args['job'] == 'xmlfeed') {
		if (empty($args['format'])) {
			die("Error: missing --format argument\n");
		}

		try {
			if ($file = runController('xml', 'create', array($args['format']), false)) {
				echo "XML feed created: " . $file . "\n";
			}
		} catch (Exception $e) {
			die("Error: " . $e->getMessage() . "\n");
		}
	}
}

echo "\n";

// Helpers

function runController($controller, $method = 'index', $arguments = array(), $is_admin = true)
{
	Core::$is_admin = $is_admin;

	$class_name = 'Controller_' . ucfirst($controller);

	if (!Core::autoload($class_name)) {
		return false;
	}

	try {
		$class = new ReflectionClass($class_name);
	} catch (ReflectionException $e) {
		return false;
	}

	Core::$controller = $controller = $class->newInstance();

	try {
		$_method = $class->getMethod($method);

		if ($method[0] === '_') {
			return false;
		}

		if ($_method->isProtected() or $_method->isPrivate()) {
			throw new ReflectionException('protected controller method');
		}
	} catch (ReflectionException $e) {
		if (call_user_func_array(array($controller, $method), $arguments) != Extendable::METHOD_NOT_FOUND) {
			$_method = null;
		} else {
			// Use __call instead
			$_method = $class->getMethod('__call');

			$arguments = array($method, $arguments);
		}
	}

	if ($_method) {
		return $_method->invokeArgs($controller, $arguments);
	}
}