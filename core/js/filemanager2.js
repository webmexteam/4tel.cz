var _fm_uploading = false;
var uploaderInst;

var root = _basepath;

FileManager = function(config){
	this.files = [];
	this.folders = [];
	this.config = {};
	this.uploader = 'flash'; // flash | basic
	
	this.viewConfig = {
		folder: '',
		search: '',
		viewType: 'thumbs'
	};
	
	this.activeFile = null;
	this.icon_path = _basepath+'core/admin/template/default/mimetypes/';
	this.media_path = _fm_root;
	
	this.init.call(this, config);
	
	return this;
}

FileManager.prototype = {

	template: new Template({
		
		leftColumn: '<div class="leftColumn">'+
						'<div class="toolbar">'+
							'<a href="#new-folder" class="ico-new-folder">'+__('new_folder')+'</a>'+
						'</div>'+
						'<div class="folders">'+
							'<ul></ul>'+
						'</div>'+
					'</div>',
					
		rightColumn: '<div class="rightColumn">'+
						'<div class="toolbar">'+
							'<a href="#upload" class="ico-upload right">'+__('upload')+'</a>'+
							'<a href="#search" class="ico-search" title="'+__('search')+'">&nbsp;</a>'+
							'<span class="sep"></span>'+
							'<a href="#view-list" class="ico-list pressed" title="'+__('list_view')+'">&nbsp;</a>'+
							'<a href="#view-thumbs" class="ico-thumbs" title="'+__('thumbs_view')+'">&nbsp;</a>'+
							'<span class="sep"></span>'+
							'<a href="#reload" class="ico-reload" title="'+__('reload')+'">&nbsp;</a>'+
							'<span class="sep"></span>'+
							'<a href="#select-all" class="ico-select-all">'+__('select_all')+'</a>'+
							'<a href="#unselect-all" class="ico-unselect-all">'+__('deselect_all')+'</a>'+
							'<a href="#delete-selected" class="ico-delete">'+__('delete_marked')+'</a>'+
						'</div>'+
						
						'<div class="files">'+
							'<ul></ul>'+
						'</div>'+
						'<iframe src="'+_base+'admin/filemanager/emptySrc/" name="iframeTarget" id="iframeTarget" width="1" heigth="1" border="0" frameborder="0" style="border:none;visibility:hidden;" />'+
					'</div>',
		
		file: '<li class="ext-{ext}">'+
					'<div class="wrap clearfix">'+
						'<input type="checkbox" name="selected_files[]" class="checkbox" value="{name}" />'+
						'<div class="img-wrap"><img src="{thumb}" alt="" style="max-width:60px;max-height:60px;" /><span class="over-icon"></span></div>'+
						'<div class="clear"></div>'+
					'</div>'+
					'<span class="name" title="{name}">{name_lines}</span>'+
				'</li>',
				
		file_table: '<li class="ext-{ext}">'+
						'<input type="checkbox" name="selected_files[]" class="checkbox" value="{name}" />'+
						'<span class="name" title="{name}">{name_lines}</span>'+
					'</li>',
				
		folder: '<li class="{cls}">'+
					'<div class="name">'+
						'<span title="{path}">{name}</span>'+
					'</div>'+
					'<ul></ul>'+
				'</li>',
				
		contextMenu: '<div class="contextMenu">'+
						'<img src="'+_basepath+'core/admin/template/default/images/filemanager-arrow.gif" alt="" class="arrow-top" />'+
						'<div class="menu-wrap">'+
							'<a href="#" class="btn-preview" unselectable="on">'+__('preview')+'</a>'+
							'<a href="#" class="btn-rename">'+__('rename')+'</a>'+
							'<a href="#" class="btn-delete">'+__('delete')+'</a>'+
							'<a href="'+_base+'admin/filemanager/downloadFile/{file_base64}/" class="btn-download" target="iframeTarget">'+__('download')+'</a>'+
						'</div>'+
						'<img src="'+_basepath+'core/admin/template/default/images/filemanager-arrow-bottom.gif" alt="" class="arrow-bottom" />'+
					'</div>',
					
		contextMenu_folder: '<div class="contextMenu">'+
						'<img src="'+_basepath+'core/admin/template/default/images/filemanager-arrow.gif" alt="" class="arrow-top" />'+
						'<div class="menu-wrap">'+
							'<a href="#" class="btn-rename">'+__('rename')+'</a>'+
							'<a href="#" class="btn-delete last">'+__('delete')+'</a>'+
						'</div>'+
						'<img src="'+_basepath+'core/admin/template/default/images/filemanager-arrow-bottom.gif" alt="" class="arrow-bottom" />'+
					'</div>',
					
		preview: '<div class="preview">'+
					'<div class="preview-img"><img src="'+_basepath+'core/admin/template/default/images/empty.gif" alt="" /></div>'+
					'<div class="preview-info">'+
						'<a href="#" title="'+__('close_preview')+'" class="preview-close"></a>'+
						'<a href="#" title="'+__('zoom_in')+'" class="preview-plus"></a>'+
						'<a href="#" title="'+__('zoom_out')+'" class="preview-minus"></a>'+
					'</div>'+
				'</div>',
				
		upload: '<div class="fileManagerUpload">'+
					'<div class="progress-bar">'+
						'<span class="pgbar-text">{msg}</span>'+
						'<div class="inner"><span class="pgbar-text">{msg}</span></div>'+
					'</div>'+
					'<div class="infoWrap"><span class="estTime">{estTime}</span><span class="info">{info}</span></div>'+
					'<form action="'+_base+'admin/filemanager/upload/" method="post" target="iframeTarget" class="uploadFileWrap" enctype="multipart/form-data">'+
						'<input type="file" name="Filedata" id="fmUploadFile" class="uploadFile" />'+
						'[if _files_watermark]<div style="padding-top:10px;clear:left;"><label><input type="checkbox" name="apply_watermark" value="1" checked="checked" /> '+__('apply_watermark')+'</label></div>[endif]'+
					'</form>'+
					'<div class="help">{help}</div>'+
				'</div>',
				
		info: 	'<div class="fileManagerInfo">'+
					'<a href="#" class="button button-disabled selectBtn">Insert image</a>'+
					'<select name="imagesize" class="imagesize"></select>'+
					'<div class="smallpreview"><img src="'+_basepath+'core/admin/template/default/mimetypes/folder.png" alt="" /></div>'+
				'</div>',
				
		info_folder:	'<div class="basicinfo basicinfo-long">'+
							'<strong>{name}</strong>'+
							'<span>{files} '+__('files')+'</span>'+
						'</div>',
		
		info_file:	'<div class="basicinfo">'+
						'<strong>{name}</strong>'+
						'<span>{mime}</span>'+
					'</div>'+
					'<div class="extinfo">'+
						'<div><span>'+__('date_created')+':</span><strong>{ctime}</strong></div>'+
						'<div><span>'+__('date_changed')+':</span><strong>{mtime}</strong></div>'+
					'</div>'+
					'<div class="extinfo">'+
						'<div><span>'+__('filesize')+':</span><strong>{size}</strong></div>'+
					'</div>',
					
		info_image:	'<div class="basicinfo">'+
						'<strong>{name}</strong>'+
						'<span>{mime}</span>'+
					'</div>'+
					'<div class="extinfo">'+
						'<div><span>'+__('date_created')+':</span><strong>{ctime}</strong></div>'+
						'<div><span>'+__('date_changed')+':</span><strong>{mtime}</strong></div>'+
					'</div>'+
					'<div class="extinfo">'+
						'<div><span>'+__('filesize')+':</span><strong>{size}</strong></div>'+
						'<div><span>'+__('dimensions')+':</span><strong>{dimensions}</strong></div>'+
					'</div>',
					
		info_video:	'<div class="basicinfo">'+
						'<strong>{name}</strong>'+
						'<span>{mime}</span>'+
					'</div>'+
					'<div class="extinfo">'+
						'<div><span>'+__('date_created')+':</span><strong>{ctime}</strong></div>'+
						'<div><span>'+__('duration')+':</span><strong>{duration}</strong></div>'+
					'</div>'+
					'<div class="extinfo">'+
						'<div><span>'+__('filesize')+':</span><strong>{size}</strong></div>'+
						'<div><span>'+__('dimensions')+':</span><strong>{dimensions}</strong></div>'+
					'</div>'
		
	}),

	init: function(config){
		var _this = this;
		
		this.config = $.extend({
			targetEl: null,
			width: 950,
			height: 550,
			scope: this,
			thumbs: _fm_thumbs,
			select: null,
			imageSize: false,
			types: ['*']
		}, config); 
		
		var prevView = $.cookie('_filemanager_view');
		
		if(prevView){
			var data = {};
			
			$.each(prevView.split('&'), function(i, part){
				var pdata = part.split('=');
				
				data[decodeURIComponent(pdata[0])] = decodeURIComponent(pdata[1].replace(/\+/g, ' '));
			});
			
			prevView = data;
		}
		
		$(this.config.targetEl).append(this.doLayout());
		
		this.el = $('#fileManager');
		
		this.updateLayout();
		
		this.bindLayout();
		
		this.bindToolbar();
		
		this.updateToolbar();
		
		if(prevView && prevView.folder){
			if(prevView.viewType){
				this.viewConfig.viewType = prevView.viewType;
			}
		
			Event.add('fileManager.onGetFolders', function(){
				var prevFolder = '/'+prevView.folder;
				
				var f = this.el.find('.folders span[title="'+prevFolder+'"]');
				
				if(f.length == 1){
					f.trigger('click');
					
				}else{
					this.getFiles();
				}
				
			}, this, true);
			
		}else{
			this.getFiles();
		}
		
		this.getFolders();
		
		this.el.find('div.files').addClass('files-loading');
		
		Event.run('fileManager.onInit', this);
		
		$(window).resize(function(){
			_this.updateLayout();
		});
	},
	
	reload: function(){
		this.getFiles();
		
		Event.run('fileManager.onReload', this);
	},
	
	close: function(){
		this.viewConfig.folder = '';
		this.dialog.close();
	},
	
	setView: function(cfg){
		var vt = this.viewConfig.viewType;
		var f = this.viewConfig.folder;
		
		if(typeof cfg.folder == 'string'){
			cfg.folder = cfg.folder.substring(1);
		}
		
		var is_search = (typeof this.viewConfig.search == 'string' && this.viewConfig.search);
		
		$.extend(this.viewConfig, cfg);
		
		if((typeof cfg.folder == 'string' && cfg.folder != f) || (is_search || (typeof cfg.search == 'string' && cfg.search))){
			this.getFiles();
			
		}else if(cfg.viewType && cfg.viewType != vt){
			this.renderFiles();
			
		}else{
			this.showFolderInfo();
		}
		
		$.cookie('_filemanager_view', $.param(this.viewConfig), { expires: 1 });
		
		Event.run('fileManager.onSetView', this, cfg);
	},
	
	doLayout: function(){
		
		var layout = $('<div />').attr('id', 'fileManager');
		
		layout.append(this.template.apply('leftColumn'));
		layout.append(this.template.apply('rightColumn'));
		
		layout.append(this.template.apply('info'));
		
		Event.run('fileManager.onDoLayout', this);
		
		return layout;
		
	},
	
	bindLayout: function(){
		var scope = this;
		
		if(this.config.select && this.config.imageSize){
			
			var options = '<optgroup label="'+__('image_dimension')+'"><option value="original">'+__('original')+'</option>';
			
			$.each(_fm_thumbs, function(i, size){
				options += '<option value="'+size+'">'+size+'</option>';
			});
			
			options += '</optgroup>';
		
			this.el.find('select.imagesize').html(options);
			
		}else{
			this.el.find('select.imagesize').hide();
		}
		
		if(this.config.select){
		
			this.el.find('.selectBtn').html(this.config.selectText || __('select_files')).bind('click', function(){
			
				var size = '';
				
				if(scope.config.imageSize){
					size = scope.el.find('select.imagesize').val();
					
					if(size == 'original'){
						size = '';
					}
				}
				
				if(scope.config.select == 'one'){
					var filename = scope.media_path+(scope.viewConfig.folder ? scope.viewConfig.folder+'/' : '')+(size ? '_'+size+'/' : '')+scope.activeFile.find('.name').attr('title');
					
					if(filename.substring(0, 1) == '/'){
						filename = filename.substring(1);
					}
					
					filename = root+filename;
					
					if(typeof scope.config.selectCallback == 'function'){
						scope.config.selectCallback.call(scope, scope, filename);
						
						scope.el.find('a[href="#unselect-all"]').trigger('click');
					}
					
				}else{
					var selected = scope.el.find('.files ul li .checkbox:checked');
					var files = [];
					
					if(scope.activeFile){
						selected.push(scope.activeFile);
					}
					
					selected.each(function(i, file){
						file = $(file);
						
						if(! file.is('li')){
							file = $(file).parents('li');
						}
						
						var filename = scope.media_path+(scope.viewConfig.folder ? scope.viewConfig.folder+'/' : '')+(size ? '_'+size+'/' : '')+file.find('.name').attr('title');
						
						if(filename.substring(0, 1) == '/'){
							filename = filename.substring(1);
						}
						
						filename = root+filename;
						
						if(! file.hasClass('disabled') && $.inArray(filename, files) == -1){
							files.push(filename);
						}
					});
					
					if(typeof scope.config.selectCallback == 'function'){
						scope.config.selectCallback.call(scope, scope, files);
						
						scope.el.find('a[href="#unselect-all"]').trigger('click');
					}
				}
				
				return false;
			}).show();
			
		}else{
			this.el.find('.selectBtn').hide();
		}
		
	},
	
	updateLayout: function(){

		var height = this.el.parent().parent().height() - 59; // 46 | 12
		
		this.el.find('.leftColumn, .rightColumn').css({
			height: height
		});
		
		this.el.find('.files ul').css({
			height: height - 39
		});
		
		this.el.find('.folders > ul').css({
			height: height - 33
		});
		
		Event.run('fileManager.onUpdateLayout', this);
	},
	
	bindToolbar: function(){
		var _this = this;
		
		if(_this.config.select == 'one'){
			this.el.find('a[href="#select-all"]').css({
				opacity: 0.5
				
			}).bind('click', function(){
				return false;
			});
			
		}else{
			this.el.find('a[href="#select-all"]').bind('click', function(){
				var lis = _this.el.find('.files ul li:not(.empty-folder)');
				
				lis.addClass('selected');
				lis.find('.checkbox').attr('checked', true);
				
				_this.updateToolbar();
				
				return false;
			});	
		}
		
		this.el.find('a[href="#unselect-all"]').bind('click', function(){
			var lis = _this.el.find('.files ul li');
			
			lis.removeClass('selected');
			lis.find('.checkbox').removeAttr('checked');
			
			_this.updateToolbar();
			
			return false;
		});
		
		this.el.find('a[href="#delete-selected"]').bind('click', function(){
			var selected = _this.el.find('.files .checkbox:checked');
			var files = [];
			
			Confirm(__('delete_files'), __('delete_files_text', selected.length), function(action){
				if(action == 'yes'){
					selected.each(function(){
						files.push($(this).val());
						
						_this.deleteFile($(this).parents('li'));
					});
					
					$.post(_base+'admin/filemanager/deleteFiles/', { folder: _this.viewConfig.folder, files: files.join(';') }, function(response){}, 'json');	
					
					_this.updateToolbar();
				}
			});
			
			return false;
		});
		
		this.el.find('a[href="#search"]').bind('click', function(){
			var input = $('<div class="inputwrap no-label"><input type="text" name="q" autocomplete="off" class="input-text" /></div>');
			var f = $('<div class="fileManagerSearch form"><span>'+__('search_help')+':</span></div>').append(input);
			
			var d = new Dialog({
				title: __('search'),
				width: 400,
				height: 70,
				content: f,
				buttons: [{
					text: __('search'),
					handler: function(){
						var q = d.el.find('input').val();
						
						if(q.length < 2){
							Alert(__('search_error'), __('search_error_min_chars'));
							return false;
						}
						
						_this.setView({
							folder: '',
							search: q
						});
						
						_this.el.find('.folders .selected').removeClass('selected');
						
						d.close();
						return false;
					}
				}]
			});
			
			d.el.find('input[name="q"]').focus();
			
			return false;
		});
		
		this.el.find('a[href="#view-list"]').bind('click', function(){
			_this.setView({
				viewType: 'list'
			});
			
			return false;
		});
		
		this.el.find('a[href="#view-thumbs"]').bind('click', function(){
			_this.setView({
				viewType: 'thumbs'
			});
			
			return false;
		});
		
		this.el.find('a[href="#reload"]').bind('click', function(){
			_this.reload();
			
			return false;
		});
		
		this.el.find('a[href="#new-folder"]').bind('click', function(){
			var selected = _this.el.find('.folders div.name.selected:first');
			var padding = parseInt(selected.css('padding-left')) + 20;
			var path = selected.find('span').attr('title');
			
			var li = $(_this.template.apply('folder', {
				name: __('new_folder'),
				path: path+'/',
			}));
			
			li.find('div.name:first').css({
				'padding-left': padding
			});
			
			selected.parent().find('ul:first').prepend(li).show();
			
			_this.bindFolders(li.find('div.name:first'));
			
			_this.renameFolder(li, false, function(name){
				$.post(_base+'admin/filemanager/createFolder/', { folder: path+'/'+name }, function(response){
				
				}, 'json');
			});
			
			return false;
		});
		
		this.el.find('a[href="#upload"]').bind('click', function(){
			if(! uploaderInst || ! uploaderInst.el){
				uploaderInst = new uploader({
					minTop: 1,
					minRight: 3,
					StateChanged: function(u){
						if(u.state == plupload.STOPPED){
							_this.getFiles();
						}
					}
				}); 
				
				uploaderInst.setTargetDir(_this.viewConfig.folder);
			}
			
			return false;
		});
		
		// center info image
		this.el.find('.fileManagerInfo .smallpreview img').bind('load', function(){
			var h = $(this).height();
			var mt = Math.round((48 - h) / 2);
			
			$(this).css({
				marginTop: mt > 3 ? mt : 0,
				opacity: 1
			}).addClass('img-loaded');
		});
		
		
		Event.run('fileManager.onBindToolbar', this);

	},
	
	isVideo: function(file){
		return file.match(/\.(flv|mp4|mov|wmv|avi|asf|asx|divx|mpe?g|mkv|movie|mpg4|mpv|ogv|ram|xvid)$/i);
	},
	
	uploaderHtml: function(html){
		var _this = this;
		var percent = 0;
		
		var form = html.find('form.uploadFileWrap');
		var iframe = this.el.find('#iframeTarget');
		
		var info = html.find('.info');
		var status = html.find('.pgbar-text');
		var progress = html.find('.progress-bar > .inner');
		var estTime = html.find('.estTime');
		var watermark = html.find('input[name="apply_watermark"]:checked').length > 0 ? true : false;
		
		html.find('input[name="apply_watermark"]').bind('click', function(){
			watermark = $(this).is(':checked');

			form.find('input[name="watermark"]').val(watermark ? 1 : 0);
		});
		
		var progressBar = function(){
			if(percent == 0 || percent >= 90){
				percent = 0;
				return;
			}
			
			percent += 2;
			
			status.html(__('uploading', percent));
			progress.width(percent+'%');
			
			setTimeout(progressBar, 500);
		}
		
		form.find('input[name="folder_path"], input[name="watermark"]').remove();
		
		form.append('<input type="hidden" name="folder_path" value="'+_this.viewConfig.folder+'" />');
		form.append('<input type="hidden" name="watermark" value="'+(watermark ? 1 : 0)+'" />');
		form.append('<input type="hidden" name="video_convert" value="0" />');
		
		iframe.bind('load', function(){
			status.html(__('finished'));
			progress.width('100%');
			info.html(__('uploaded_file'));
			html.find('#fmUploadFile').val('');
			percent = 0;
			
			_this.reload();
		});
		
		html.find('#fmUploadFile').bind('change', function(){
			var fname = $(this).val();
			
			var startUpload = function(video_convert){
				status.html(__('uploading', 0));
				progress.width('1%');
				info.html($(this).val());
				percent = 2;
				progressBar();
				form.find('input[name="video_convert"]').val(video_convert ? 1 : 0);
				form.submit();
			}
			
			if(_this.isVideo(fname)){
				Confirm(__('video_upload'), __('video_upload_convert'), function(btn){
					startUpload(btn == 'yes');
				});
				
			}else{
				startUpload(false);
			}
		});
		
		Event.run('fileManager.onUploaderShow', this);
		Event.run('fileManager.onUploaderBasicShow', this);
	},
	
	uploaderFlash: function(html){
		var _this = this;
		
		var info = html.find('.info');
		var status = html.find('.pgbar-text');
		var progress = html.find('.progress-bar > .inner');
		var estTime = html.find('.estTime');
		var watermark = html.find('input[name="apply_watermark"]:checked').length > 0 ? true : false;
		
		var totalUploadSize = 0;
		var totalFilesLoaded = 0;
		var totalFiles = 0;
		var avgSpeed = [];
		var timeDelay = [];
		
		var uploadingVideo = false;
		
		html.find('input[name="apply_watermark"]').bind('click', function(){
			watermark = $(this).is(':checked');
			
			html.find('.uploadFile').uploadifySettings('scriptData', {'watermark' : watermark ? 1 : 0});
		});
		
		var errorOccurred  = false;
		
		var uploadOnSelect = function(e, queue, file){
			totalUploadSize += file.size;
			totalFiles ++;
			
			if(_this.isVideo(file.name)){
				uploadingVideo = true;
			}
		}
		
		var uploadOnOpen = function(e, queue, file){
			info.html(file.name.substring(0, 30)+' ('+Format.fileSize(file.size)+')');
		}
		
		var uploadOnProgress = function(e, queue, file, data){
			var percent = Math.ceil(100 / totalUploadSize * data.allBytesLoaded);
			
			if(percent > 100){
				percent = 100;
			}
			
			if(data.speed > 5) avgSpeed.push(data.speed);
			
			var delay = Util.array_avg(timeDelay) || 0;
			var speed = Util.array_avg(avgSpeed);
			
			var fix = Math.ceil((totalFiles - totalFilesLoaded) * delay / 1000);
			
			var time = Format.timeFromSeconds((( totalUploadSize - data.allBytesLoaded ) / 1024 / speed) + fix);
			
			status.html(__('uploading', percent));
			estTime.html(__('est_time', time));
			progress.width(percent+'%');
		}
		
		var uploadOnError = function(e, queue, file, error){
			errorOccurred = error;
		}
		
		var uploadOnComplete = function(){
			totalFilesLoaded ++;
			var time = new Date();
			var delay = (time.getTime() - timer.getTime());
			timeDelay.push(delay);
			
			timer = new Date();
		}
		
		var uploadOnAllComplete = function(e, data){
			if(data.errors > 0 && errorOccurred){
				status.html(__('upload_error'));
				info.html('');
				
				Alert(__('upload_error'), __('upload_error_text', errorOccurred.type, errorOccurred.info));
				
			}else{
				info.html(__('uploaded_files', data.filesUploaded, Format.fileSize(data.allBytesLoaded)));
				status.html(__('finished'));
			}
			
			progress.width('100%');
			estTime.html('');
			
			html.find('.uploadFileWrap').css({
				visibility: 'visible',
				'text-indent': 0
			});
			
			_fm_uploading = false;
			
			_this.reload();
		}
		
		var startUpload = function(convert_video){
			var el = html.find('.uploadFile:first');
			
			el.uploadifySettings('scriptData', { 'video_convert' : convert_video ? 1 : 0 });
			
			el.uploadifyUpload();
		}
		
		var uploadOnSelectOnce = function(e, data){
			progress.width(0);
			info.html(__('contacting_server'));
			status.html(__('uploading', '0'));
			html.find('.uploadFileWrap').css({
				visibility: 'hidden',
				'text-indent': -600
			});
			_fm_uploading = true;
			timer = new Date();
			
			if(uploadingVideo){
				Confirm(__('video_upload'), __('video_upload_convert'), function(btn){
					startUpload(btn == 'yes');
				});
				
			}else{
				startUpload(false);
			}
		}
		
		html.find('.uploadFile:first').uploadify({ 
			'method': 'POST',
			'scriptData': { 'COMSESSION': _session_id, 'folder_path': (_this.viewConfig.folder || ''), 'watermark': watermark, 'video_convert': 0 },
			'uploader': './templates/admin/swf/uploadify.allglyphs.swf', 
			'script': _base+'admin/filemanager/upload/',
			'folder': '',
			'buttonText': escape(__('browse')),
			'multi': true,
			'auto': false,
			'onSelect': uploadOnSelect,
			'onOpen': uploadOnOpen,
			'onProgress': uploadOnProgress,
			'onError': uploadOnError,
			'onComplete': uploadOnComplete,
			'onAllComplete': uploadOnAllComplete,
			'onSelectOnce': uploadOnSelectOnce
		});
		
		Event.run('fileManager.onUploaderShow', this);
		Event.run('fileManager.onUploaderFlashShow', this);
	},
	
	updateToolbar: function(){
		var selected = this.el.find('.files ul li .checkbox:checked').length;
		var delete_all = this.el.find('a[href="#delete-selected"]');
		var unselect_all = this.el.find('a[href="#unselect-all"]');
		
		delete_all.find('span').html(selected);
		
		if(selected >= 1){
			delete_all.show();
			unselect_all.show();
			
			var btn = this.el.find('.selectBtn');
			if(btn){
				btn.removeClass('button-disabled');
				this.el.find('select.imagesize').removeAttr('disabled');
			}
			
		}else{
			delete_all.hide();
			unselect_all.hide();
			
			var btn = this.el.find('.selectBtn');
			if(btn){
				btn.addClass('button-disabled');
				this.el.find('select.imagesize').attr('disabled', true);
			}
			
		}
		
		this.el.find('a[href="#view-thumbs"], a[href="#view-list"]').removeClass('pressed');
		this.el.find('a[href="#view-'+this.viewConfig.viewType+'"]').addClass('pressed');
		
		Event.run('fileManager.onUpdateToolbar', this);
	},	
	
	getFiles: function(){
		var _this = this;
		
		_this.el.find('div.files').addClass('files-loading');
		_this.el.find('div.files ul').html('');
		
		Event.run('fileManager.beforeGetFiles', this);
		
		$.post(_base+'admin/filemanager/getFiles/', { folder: _this.viewConfig.folder, search: _this.viewConfig.search }, function(files){
			
			_this.files = files;
			
			_this.el.find('div.files').removeClass('files-loading');
			
			_this.renderFiles();
			
			Event.run('fileManager.onGetFiles', _this, files);
			
		}, 'json');
		
	},
	
	getFolders: function(){
		var _this = this;
		
		Event.run('fileManager.beforeGetFolders', this);
		
		$.post(_base+'admin/filemanager/getFolders/', { folder: _this.viewConfig.folder }, function(folders){
			
			_this.folders = folders;
			
			_this.renderFolders();
			
			_this.bindFolders();
			
			Event.run('fileManager.onGetFolders', _this, folders);
			
		}, 'json');
		
	},
	
	getFileDetail: function(file, cb){
		var scope = this;
		
		$.post(_base+'admin/filemanager/getFileDetail/', { file: file, folder: this.viewConfig.folder }, function(info){
			
			cb.call(scope, info);
			
			Event.run('fileManager.onGetFileDetail', scope, info);
			
		}, 'json');
	},
	
	renderFiles: function(){
		var _this = this;
		var tpl = this.viewConfig.viewType == 'thumbs' ? 'file' : 'file_table';
		
		if(typeof this.files != 'object'){
			return;
		}
		
		if(this.viewConfig.viewType == 'list'){
			_this.el.find('.files ul').addClass('list');
		}else{
			_this.el.find('.files ul').removeClass('list');
		}
		
		if(this.files.length == 0){
			_this.el.find('.files ul').html('<li class="empty-folder radius-small"><span class="icon icon-alert"></span>'+__((this.viewConfig.search ? 'no_search_results' : 'empty_folder'))+'</li>');
			
		}else{
			_this.el.find('.files ul').html('');
		}
		
		if(this.viewConfig.search){
			_this.el.find('.files ul').prepend('<li class="searchTitle"><span>'+__('search_results', this.files.length)+'</span>'+__('search_title', this.viewConfig.search)+'</li>');
		}
		
		var all_types = $.inArray('*', _this.config.types) >= 0;
		
		$.each(this.files, function(index, file){
			if(! file){
				return;
			}
			
			var ext = file.substring(file.lastIndexOf('.') + 1).toLowerCase();	
			var html = $(_this.template.apply(tpl, {
				name: file,
				name_lines: _this.parseName(file, _this.viewConfig.viewType == 'list' ? 150 : 17),
				thumb: _this.getFileThumb(file),
				ext: ext
			}));
			
			_this.el.find('.files ul').append(html);
			
			if(_this.config.types && !all_types && $.inArray(ext, _this.config.types) == -1){
				html.addClass('disabled').css({
					opacity: 0.5
				});
			}
		});
		
		_this.showFolderInfo();
		
		_this.bindFiles();
		
		_this.updateToolbar();
		
		Event.run('fileManager.onRenderFiles', this);
	},
	
	renderFolders: function(folders, parent, offset, path){
		var _this = this;

		var leftPadding = 20;
		
		if(! offset){
			offset = 0;
		}
		
		if(! path){
			path ='';
		}
		
		var folders = folders ? folders : this.folders;
		var el = parent ? $(parent) : this.el.find('.folders ul');
		
		if(! parent){
			folders = {
				'/': folders
			};
		}
		
		$.each(folders, function(name, subFolders){
			var _path = (name == '/' && ! parent) ? path : path+'/'+name;
				
			var e = $(_this.template.apply('folder', {
				name: name,
				path: _path,
				cls: name == '/' ? 'root' : ''
			}));
			
			e.find('div.name').css({
				'padding-left': offset * leftPadding
			});
					
			el.append(e);
			
			if(typeof subFolders == 'object'){
				_this.renderFolders(subFolders, e.find('ul'), (offset + 1), _path);
			}
		});
		
		Event.run('fileManager.onRenderFolders', this);
	},

	
	getFileThumb: function(file){
		var icon_path = this.icon_path;
		var thumb_path = this.media_path;
		var folder = this.viewConfig.folder ? this.viewConfig.folder+'/' : '';
		
		var ext = file.substring(file.lastIndexOf('.') + 1);
		
		var slashpos = file.lastIndexOf('/');
		if(slashpos >= 0){
			folder += file.substring(0, slashpos + 1);
			file = file.substring(slashpos + 1);
		}
		
		if(ext.match(/^(jpe?g|gif|png)$/i)){
			return thumb_path+folder+'_60x60/'+file;
		}
		
		if(ext == 'video'){
			return thumb_path+folder+file+'/poster.jpg';
		}
		
		switch(ext){
			case 'pdf':
				return icon_path+'pdf.png';
			break;	
			
			case 'txt':	
				return icon_path+'txt.png';
			break;	
			
			case 'php':	
				return icon_path+'php.png';
			break;
			
			case 'css':	
				return icon_path+'css.png';
			break;
			
			case 'html':	
				return icon_path+'html.png';
			break;
			
			case 'mp3':	
				return icon_path+'mp3.png';
			break;
			
			case 'exe':	
				return icon_path+'exe.png';
			break;
			
			case 'doc':
			case 'docx':
				return icon_path+'doc.png';
			break;
			
			case 'ppt':
			case 'pps':
			case 'pptx':
			case 'ppsx':
				return icon_path+'ppt.png';
			break;	
			
			case 'xls':
			case 'xlt':	
			case 'xlsx':
			case 'xltx':	
				return icon_path+'xls.png';
			break;
			
			case 'odt':
				return icon_path+'odt.png';
			break;	
			
			case 'ods':
				return icon_path+'ods.png';
			break;	
			
			case 'flv':
				return icon_path+'flv.png';
			break;	
			
			case 'mp4':
				return icon_path+'mp4.png';
			break;
		}
		
		return icon_path+'empty.png';
	},
	
	showFolderInfo: function(){
		var info = this.el.find('.fileManagerInfo');
		var img = info.find('.smallpreview img');
		
		img.attr('src', _basepath+'core/admin/template/default/mimetypes/folder.png').css({
			marginTop: 0,
			opacity: 1
		});
		
		info.find('.basicinfo, .extinfo, .loader-inline').remove();
		
		var btn = info.find('.selectBtn');
		if(btn){
			btn.addClass('button-disabled');
			this.el.find('select.imagesize').attr('disabled');
		}
		
		this.showInfoType = this.viewConfig.folder;
		
		info.append(this.template.apply('info_folder', {
			name: '/'+this.viewConfig.folder,
			files: this.files.length
		}));
	},
	
	showInfo: function(file){
		var scope = this;
		var file = $(file);
		var filename = file.find('.name').attr('title');
		var info = this.el.find('.fileManagerInfo');
		
		var img = info.find('.smallpreview img');
		
		img.attr('src', this.getFileThumb(filename));
		
		if(! img.hasClass('img-loaded')){
			img.css({
				marginTop: 0,
				opacity: 0
			});
		}
		
		info.find('.basicinfo, .extinfo, .loader-inline').remove();
		info.append('<div class="loader-inline"></div>');
		
		var btn = info.find('.selectBtn');
		if(btn){
			btn.removeClass('button-disabled');
			this.el.find('select.imagesize').removeAttr('disabled');
		}
		
		this.showInfoType = filename;
		
		this.getFileDetail(filename, function(data){
		
			if(scope.showInfoType != filename){
				return;
			}
		
			var tpl_data = {
				name: scope.viewConfig.search ? '/'+filename : filename,
				mime: data.mime,
				size: Format.fileSize(data.size),
				mtime: Format.datetime(data.mtime),
				ctime: Format.datetime(data.ctime)
			};
		
			if(data.mime.match(/(jpe?g|gif|png)$/)){
				// image
				tpl_data.dimensions = data.dimensions;
				var tpl = scope.template.apply('info_image', tpl_data);
				
			}else if(data.video){
				// image
				tpl_data.dimensions = data.dimensions;
				tpl_data.duration = data.duration;
				var tpl = scope.template.apply('info_video', tpl_data);
				
			}else{
				var tpl = scope.template.apply('info_file', tpl_data);
				
			}
			
			info.find('.basicinfo, .extinfo, .loader-inline').remove();
			info.append(tpl);
		});
		
		Event.run('fileManager.onShowInfo', this, filename);
	},
	
	showPreview: function(file){
		var scope = this;
		var orig_dimensions = null;
		var current_index = 0;
		var dimensions = this.config.thumbs.slice(0);
		var preview = $(this.template.apply('preview'));
		
		preview.find('.preview-close').bind('click', function(){
			preview.remove();
			return false;
		});
		
		preview.find('.preview-plus').bind('click', function(){
			if(current_index >= 0){
				current_index ++;
				
				if(current_index > dimensions.length - 1){
					current_index = dimensions.length - 1;
				}
				
				showImg();
			}
			return false;
		});
		
		preview.find('.preview-minus').bind('click', function(){
			if(current_index >= 0){
				current_index --;
				
				if(current_index < 0){
					current_index = 0;
				}
				
				showImg();
			}
			return false;
		});
		
		this.el.append(preview);
		
		var showImg = function(){
			var tmp_file = file;
			var folder = scope.viewConfig.folder ? scope.viewConfig.folder+'/' : '';
			
			var slashpos = tmp_file.lastIndexOf('/');
			if(slashpos >= 0){
				folder += tmp_file.substring(0, slashpos + 1);
				tmp_file = tmp_file.substring(slashpos + 1);
			}
			
			var previewPath = scope.media_path+folder+'_'+dimensions[current_index]+'/'+tmp_file;
			
			if(current_index == null || dimensions[current_index] == orig_dimensions){
				previewPath = scope.media_path+folder+tmp_file;
			}
			
			var img = preview.find('div.preview-img img');
			
			img.attr('src', previewPath);
			
			img.bind('error', function(){
				if(! this.hasOriginal){
					var src = this.src.replace(/\/_\d+x\d+/, '');
					this.src = src;
					this.hasOriginal = true;
					preview.find('.preview-plus, .preview-minus').hide();
				}
			});
		}
		
		this.getFileDetail(file, function(info){
			var previewSize = null;
			
			if(info.dimensions){
				orig_dimensions = info.dimensions;
				
				dimensions.push(orig_dimensions);
				
				dimensions.sort(function(a, b){
					var parts = a.split('x');
					var oa = parseInt(parts[0]) * parseInt(parts[1]);
					parts = b.split('x');
					var ob = parseInt(parts[0]) * parseInt(parts[1]);
					
					return oa - ob;
				});
				
				var rec = this.findMaxSize(info.dimensions);
				current_index = $.inArray(rec, dimensions);
				
				if(current_index < 0){
					current_index = null;
				}
			}
			
			showImg();
		});
		
		Event.run('fileManager.onShowPreview', this, file);
	},
	
	findMaxSize: function(file_dimensions){
		var thumbs = this.config.thumbs;
		var maxW = this.el.find('.preview-img').width() - 20;
		var maxH = this.el.find('.preview-img').height() - 20;
		var final = file_dimensions;
		var max = [0, 0, 0];
		
		var file = file_dimensions.split('x');
		var fileW = parseInt(file[0]);
		var fileH = parseInt(file[1]);
		var fileSq = fileW * fileH;
		var asp = 'w';
		var ratio = fileH / fileW;
		
		if(fileH > fileW){
			asp = 'h';
			ratio = fileW / fileH;
		}
		
		if(fileW <= maxW && fileH <= maxH){
			return file_dimensions;
		}
		
		$.each(thumbs, function(i, thumb){
			var parts = thumb.split('x');
			var w = parseInt(parts[0]);
			var h = parseInt(parts[1]);
			
			var newFileW = (asp == 'w') ? w : (w * ratio);
			var newFileH = (asp == 'h') ? h : (h * ratio);
			
			var sq = newFileW * newFileH;

			if(sq > fileSq){
				return;
			}
		
			if((newFileW > max[0] || newFileH > max[1]) && newFileW <= maxW && newFileH <= maxH && sq >= max[2]){
				max = [newFileW, newFileH, sq];
				final = thumb;
			}
			
		});
		
		return final;
	},
	
	parseName: function(name, chars){
		name = name.substring(name.lastIndexOf('/') + 1);
		var re = new RegExp('.{1,'+chars+'}', 'g');
		
		var chunks = name.match(re);
		
		for(var i in chunks){
			var part = chunks[i];
			var j = parseInt(i) + 1;
			
			if(typeof part == 'string'){
				var dotIndex = part.lastIndexOf('.');
				
				if(dotIndex >= (chars-3) && chunks[j]){
					chunks[i] = part.substring(0, dotIndex);
					chunks[j] = part.substring(dotIndex) + chunks[j];
				}
			}
		}
		
		return chunks.join('<br />');
	},
	
	bindFiles: function(){
		var _this = this;
		
		this.el.find('.files ul li').each(function(){
			var li = $(this);
			
			li.find('.img-wrap img').bind('error', function(){
				if(! this.hasOriginal){
					var src = this.src.replace(/\/_60x60/, '');
					this.src = src;
					this.hasOriginal = true;
				}
			});
			
			// disable system context menu
			li[0].oncontextmenu = function() {
				return false;
			}
			
			// show context menu on right-click
			li.bind('mousedown', function(e){
				if(e.button == 2){
					if(! $(this).hasClass('disabled')){
						_this.showContextMenu.call(_this, this, true);
					}
					return false;
				}
			});
			
			if(li.hasClass('disabled')){
				li.find('.checkbox').attr('disabled', true);
				return;
			}
			
			if(_this.config.select == 'one'){
				li.find('.checkbox').attr('disabled', true);
			}
			
			li.find('.checkbox').bind('click', function(){
				if($(this).is(':checked')){
					li.addClass('selected');
				}else{
					li.removeClass('selected');
				}
				
				_this.updateToolbar();
			});
			
			// show context menu on hover
			li.hover(function(){
				_this.showContextMenu.call(_this, this);
			}, function(){
				_this.hideContextMenu.call(_this, this);
			});
			
			
			if(_this.config.select){
				li.bind('dblclick', function(e){
					if(e.target.tagName.toLowerCase() != 'input'){
						e.preventDefault();
						e.stopPropagation();
						
						_this.el.find('.files ul li.active').removeClass('active');
						
						$(this).addClass('active');
						
						_this.activeFile = $(this);
						
						_this.el.find('.selectBtn').trigger('click');
						
						return false;
					}
				});
			}
			
			li.bind('click', function(e){
				if(e.target.tagName.toLowerCase() != 'input'){
					if($(this).hasClass('active')){
						$(this).removeClass('active');
						_this.activeFile = null;
						return;
					}
					
					_this.el.find('.files ul li.active').removeClass('active');
					
					$(this).addClass('active');
					
					_this.activeFile = $(this);
					
					_this.showInfo.call(_this, this);
				}
			});
			
			
			/* Rename by double-click */
			li.find('.name').bind('dblclick', function(){
				var name = $(this);
				var val = name.attr('title');
				var old_name = val;
				
				var ext = val.substring(val.lastIndexOf('.'));
				
				var slashpos = val.lastIndexOf('/');
				if(slashpos >= 0){
					val = val.substring(slashpos + 1);
				}
				
				name.hide();
				name.after('<input type="text" name="" value="'+val+'" class="textbox" />');
				
				var inp = li.find('.textbox');
				
				var start = 1;
				var end = 4;
				
				inp.focus();
				
				inp.bind('blur', function(){
					var val = dirify($(this).val(), true);
					
					var pos = val.lastIndexOf('.');
					var _ext = val.substring(pos);
					
					if(pos < 0 || _ext.length < 2 || _ext != ext){
						val += ext;
					}
					
					name.html(val);
					name.attr('title', val);
					name.show();
					
					$.each(_this.files, function(i, file){
						if(file == old_name){
							_this.files[i] = val;
						}
					});
					
					$.post(_base+'admin/filemanager/renameFile/', { folder: _this.viewConfig.folder, file: old_name, new_name: val }, function(response){
					
					}, 'json');
					
					$(this).remove();
					
					Event.run('fileManager.onRenameFile', _this, li);
					
				}).bind('keydown', function(e){
					if(e.which == 13){
						$(this).trigger('blur');
						
					}else if(e.which == 27){
						name.show();
						$(this).remove();
					}
				});
				
				return false;
			});
			
		});
		
		Event.run('fileManager.onBindFiles', this);
	},
	
	bindFolders: function(divs){
		var _this = this;
		var openFirst = false;
		
		if(! divs){
			divs = this.el.find('.folders ul li div');
			openFirst = true;
		}
		
		divs.each(function(){
			var folder = $(this);
			var li = folder.parent();
			
			if(! li.hasClass('root')){
				folder.unbind('mousedown').bind('mousedown', function(e){
					if(e.button == 2){
						_this.showContextMenuFolder.call(_this, li, true);
						return false;
					}
				});
			}
			
			// disable system context menu
			folder[0].oncontextmenu = function() {
				return false;
			}
			
			folder.unbind('click').bind('click', function(){
				var ul = li.find('ul:first');
				var f = folder.find('span:first').attr('title');
				
				_this.el.find('.folders div.name').removeClass('opened').removeClass('selected');
				
				if(ul.find('li').length > 0){
					ul.slideDown();
				}
				
				folder.addClass('opened').addClass('selected');
				
				_this.setView({
					folder: f,
					search: ''
				});
				
				folder.parents('li').find('> div').addClass('opened');
				
				_this.el.find('.folders ul li div:not(.opened)').parent().find('ul').slideUp();
				return false;
			});
		});
		
		if(openFirst){
			var root = this.el.find('.folders ul li:first');
		
			root.find('ul:first').show();
			root.find('> div').addClass('selected opened');
		}
		
		Event.run('fileManager.onBindFolders', this);
	},
	
	showContextMenu: function(li, show){
		li = $(li);
		var file = li.find('.name').attr('title');
		var match = li.attr('class').match(/ext-([a-zA-Z0-9]{1,6})/);
		
		if(! match){
			return;
		}
		
		var ext = match[1];
		
		if(! show){
			var _this = this;
			
			li.addClass('hovered');
			
			setTimeout(function(){
				_this.showContextMenu.call(_this, li, true);
			}, 2500);
			
			return;
		}
		
		if(li.hasClass('hovered') && li.find('input.textbox').length === 0 && li.find('div.contextMenu').length === 0){
			var menu = $(this.template.apply('contextMenu', {
				file_base64: Base64.encode(this.viewConfig.folder+'/'+file)
			}));
			
			Event.run('fileManager.beforeShowContextMenu', this, menu, li);
			
			if(! ext.match(/^(gif|png|jpe?g)$/)){
				menu.find('.btn-preview, .btn-editor').remove();
			}
			
			menu.find('a:last').addClass('last');
			
			li.append(menu);	
			
			var liPos = this.getElPos(li, li.parent());
			
			if(liPos.right < 70){
				menu.addClass('contextMenu-onLeft');
			}
			
			if(liPos.bottom < 100){
				menu.addClass('contextMenu-onTop');
			}
			
			this.bindContextMenu.call(this, menu, li);
		}
		
		Event.run('fileManager.onShowContextMenu', this, menu, li);
	},
	
	showContextMenuFolder: function(li){
		var _this = this;
		li = $(li);
		var div = li.find('div:first');
			
		this.el.find('.folders div.contextMenu').remove();
		
		var menu = $(this.template.apply('contextMenu_folder', {

		}));
		
		Event.run('fileManager.beforeShowContextMenuFolder', this, menu, li);
		
		this.el.find('.folders').append(menu);	
		
		var liPos = this.getElPos(div.find('span:first'), this.el.find('.folders'));
		
		menu.css({
			top: liPos.top + 18,
			left: liPos.left + 1
		});
		
		menu.bind('mouseleave', function(){
			_this.hideContextMenuFolder.call(_this);
		});
		
		if(liPos.bottom < 70){
			menu.addClass('contextMenu-onTop');
		}
		
		this.bindContextMenuFolder.call(this, menu, li);
		
		Event.run('fileManager.onShowContextMenuFolder', this, menu, li);

	},
	
	hideContextMenu: function(li){
		$(li).removeClass('hovered');
		$(li).find('.contextMenu').remove();
		Event.run('fileManager.onHideContextMenu', this);
	},
	
	hideContextMenuFolder: function(){
		this.el.find('.folders .contextMenu').remove();
		Event.run('fileManager.onHideContextMenuFolder', this);
	},
	
	bindContextMenu: function(menu, li){
		var _this = this;
		var file = $(li).find('.name').attr('title');
		
		$(menu).find('a.btn-rename').bind('click', function(){
			$(li).find('.name').trigger('dblclick');
			_this.hideContextMenu.call(_this, li);
			return false;
		});
		
		$(menu).find('a.btn-preview').bind('click', function(){
			_this.showPreview.call(_this, file);
			_this.hideContextMenu.call(_this, li);
			return false;
		});
		
		$(menu).find('a.btn-delete').bind('click', function(){
			Confirm(__('delete_file'), __('delete_file_text', file), function(action){
				if(action == 'yes'){
					_this.deleteFile(li);
				
					$.post(_base+'admin/filemanager/deleteFile_2/', { folder: _this.viewConfig.folder, file: file }, function(response){
						
					}, 'json');
				}
			});
			
			return false;
		});
		
		Event.run('fileManager.onBindContextMenu', this, menu, li);
	},
	
	bindContextMenuFolder: function(menu, li){
		var _this = this;
		var span = li.find('span:first');
		var folder = span.html();
		
		$(menu).find('a.btn-rename').bind('click', function(){
			_this.renameFolder(li);
			
			_this.hideContextMenuFolder.call(_this, li);
			return false;
		});
		
		$(menu).find('a.btn-delete').bind('click', function(){
			var path = span.attr('title');
			Confirm(__('delete_folder'), __('delete_folder_text', folder), function(action){
				if(action == 'yes'){
					_this.deleteFolder(li);
					
					_this.setView({
						folder: ''
					});
				
					$.post(_base+'admin/filemanager/deleteFolder/', { folder: path }, function(response){
						
					}, 'json');
				}
			});
			
			return false;
		});
		
		Event.run('fileManager.onBindContextMenuFolder', this, menu, li);
	},
	
	renameFolder: function(li, ajax, cb){
		var _this = this;
		var span = li.find('span:first');
		var folder = span.html();
		
		if(typeof ajax != 'boolean'){
			ajax = true;
		}
		
		var inp = $('<input type="text" name="" value="'+folder+'" class="textbox" />');
			
		inp.css({
			width: span.width() - 2  
		});
		
		inp.bind('blur', function(){
			var val = dirify($(this).val(), true);
			
			if(val.match(/^[0-9]{1,}x[0-9]{1,}$/)){
				val = '_'+val;
			}
			
			var old_path = span.attr('title');
			old_name = old_path.substring(old_path.lastIndexOf('/') + 1);
			
			var new_path = span.attr('title').replace(RegExp('\/'+old_name+'$'), '/'+val);
			
			span.html(val);
			span.attr('title', new_path);
			span.show();
			
			if(ajax){
				$.post(_base+'admin/filemanager/renameFolder/', { folder: old_path.substring(1), new_name: val }, function(response){
				
				}, 'json');
			}
			
			if(typeof cb == 'function'){
				cb.call(_this, val);
			}
			
			$(this).remove();
			
		}).bind('keydown', function(e){
			if(e.which == 13){
				$(this).trigger('blur');
				
			}else if(e.which == 27){
				span.show();
				$(this).remove();
			}
		});
		
		span.hide();
		span.after(inp);
		
		inp.focus();
		
		Event.run('fileManager.onRenameFolder', this, li);
	},
	
	deleteFile: function(li){
		var _this = this;
		var file = $(li).find('.name').attr('title');
		
		$(li).css({
			width: $(li).width(),
			height: $(li).height()
		});
		
		$(li).css({
			'background': 'transparent'
		}).find('*').fadeOut();
		
		$.each(_this.files, function(i, f){
			if(f == file){
				delete _this.files[i];
				return false;
			}
		});
		
		Event.run('fileManager.onDeleteFile', this, li);
	},
	
	deleteFolder: function(li){
		$(li).fadeOut('normal', function(){
			$(this).remove();
		});
		
		Event.run('fileManager.onDeleteFolder', this, li);
	},
	
	getElPos: function(li, el){
		var viewportOffset = el.offset();
		var viewportHeight = el.height();
		var viewportWidth = el.width();
		var viewportScrollTop = el.scrollTop();
		
		var liOffset = li.offset();
		var liHeight = li.height();
		var liWidth = li.width();
		
		var _top = liOffset.top - viewportOffset.top;
		var _left = liOffset.left - viewportOffset.left;
		
		return {
			top: _top,
			left: _left,
			right: viewportWidth - liWidth - _left,
			bottom: viewportHeight - liHeight - _top
		};
	}

}