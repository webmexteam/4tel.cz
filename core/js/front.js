$(function(){
	jQuery('.js-hidden').hide();



	/********************* Registrace *********************/



	// Plna / jednoducha registrace
	$('.j-full-registration').hide();
	$(document).on('click', '.j-full-registration-btn', function(){
		if($(this).data('registrationtype') == "simple") {
			$(this).text($(this).data('simpleregistertext'));
			$(this).data('registrationtype', 'full');
			$('.j-simple-register').fadeOut(function(){
				$('.j-full-registration').fadeIn();
			});
			$('input[name="registration_type"]').val('full');
		}else{
			$(this).text($(this).data('fullregistertext'));
			$(this).data('registrationtype', 'simple');
			$('.j-full-registration').fadeOut(function(){
				$('.j-simple-register').fadeIn();
				
			});
			$('input[name="registration_type"]').val('simple');
		}
		return false;
	});

	// iCheck
	// $('.checkbox input, .radio input').iCheck({
	// 	checkboxClass: 'icheckbox_flat-aero',
	// 	radioClass: 'iradio_flat-aero',
	// 	increaseArea: '20%'
	// });

	// Dodaci adresa
	$('.j-shipping-as-billing').hide();
	$(document).on('change', '.j-shipping-as-billing-btn', function(){
		if($(this).is(':checked')) {
			$('.j-shipping-as-billing').fadeOut();
		}else{
			$('.j-shipping-as-billing').fadeIn();
		}
	});

	// Registrace jako firma
	$('.j-company').hide();
	$(document).on('change', '.j-company-btn', function(){
		if($(this).is(':checked')) {
			$('.j-company').fadeIn();
		}else{
			$('.j-company').fadeOut();
		}
	});

	// Fancybox
	$('a.lightbox').fancybox({
		titlePosition: 'over',
		padding: 0
	});



	/********************* FONT SIZE *********************/	
	


	// fontsize
	var setFontsize = function(size){
		$('body').removeClass('fontsize-small fontsize-big');
		
		if(size == 'small'){
			$('body').addClass('fontsize-small');
			
		}else if(size == 'big'){
			$('body').addClass('fontsize-big');	
		}
	}
	
	$('a.fontsize').bind('click', function(){
		if ($.cookie === undefined) return false;
		
		var size = this.href.match(/#(\w+)$/)[1];
		setFontsize(size);
		$.cookie('fonsize', size, {expires: 7});
		
		return false;
	});
	
	if ($.cookie !== undefined) {
		var fontsize = $.cookie('fonsize');
		if(fontsize){
			setFontsize(fontsize);
		}
	}



	/********************* BASKET EVENTS *********************/



	// ajax basket recalculation
	if($('button[name="update_qty"]').length)
		$('button[name="update_qty"]').hide();


	var timer;
	$('.ajax-qty').bind('keyup change spinchange spin', function(){

		$(this).closest('tr').find('.totaltd .ajax').show();
		$(this).closest('tr').find('.totaltd .total').hide();

		clearInterval(timer);  //clear any interval on key up
		timer = setTimeout(function() { //then give it a second to see if the user is finished
			$.post(_base+'ajax/recalculate_basket', {
				data: $('div.basket form').serialize()
			}, function(response){
				/*if($('.webmex-basket-free-shipping-module').length){
					location.reload();
				}*/

				var data = $.parseJSON(response);

                                //var prekrocenpocet = false;

				$.each(data.products, function(){
					var tr = $('.basket .tablewrap table tr[data-productid="'+this.id+'"]');
					tr.find('.total').show().html(this.total);
					tr.find('.ajax').hide();
                                        tr.find('.ajax-qty').val(this.qty);
					//tr.find('.ajax-qty').val(this.quantity);
                                        
                                        /*if(parseInt(this.qty) > parseInt(this.quantity)) {
                                            prekrocenpocet = true;
                                        }*/
                                        
				});
                                
                                /*if(prekrocenpocet) {
                                    if($('.prekrocenlimit').length) {
                                        $('.prekrocenlimit').remove();
                                    }
                                    $('.basket form').prepend('<div class="alert alert-danger prekrocenlimit">Není možné vložit více kusů daného výrobku.</div>');
                                }*/

				if(data.subtotal){
					$('.basket .tablewrap table tr.subtotal:nth-child(1) td.value').html(data.subtotal);
				}

				if(data.voucher){
					$('.basket .tablewrap table tr.subtotal:nth-child(2) td.value').html(data.voucher);
				}
    
				$('.basket .tablewrap table tr.total td.value').html(data.total);
                                
                                //$('.basket .tablewrap .summary .total .price .value').html(data.total);
                                
                                
                                
			});
		}, 600);
	});
	
	// order
	if(typeof delivery != 'undefined'){
		var estPrice = function(str, t){
			var matches = String(str).match(/^([\+\-]?[\d\.\,]+)\%$/);
			
			if(matches){
				return t / 100 * parseFloatNum(matches[1]);
			}
			
			return str ? parseFloatNum(str) : 0;
		}
		
		var recalcPrice = function(delivery_payment){
			if(typeof _subtotal != 'undefined'){
				var total = parseFloatNum(_subtotal) + price_vat(parseFloatNum(delivery_payment), _vat_delivery);
				var subtotal = parseFloatNum(_subtotal_excl_vat) + price_vat(parseFloatNum(delivery_payment), 0);

				if(typeof _voucher_value != 'undefined'){
					var discount = estPrice(_voucher_value, total);
					$('#discount').html('-'+displayPrice(discount));
					total -= discount;
					subtotal -= discount;
					
					if(total < 0){
						total = 0;
					}
				}

				total = Math.round(total * Math.pow(10, _order_round_decimals)) / Math.pow(10, _order_round_decimals);
				subtotal = Math.round(subtotal * Math.pow(10, _order_round_decimals)) / Math.pow(10, _order_round_decimals);

				$('.j-total-include-vat').html(displayPrice(total));
				$('.j-total-exclude-vat').html(displayPrice(subtotal));

				if($('input[name="total_price"]').length) {
					$('input[name="total_price"]').val(displayPrice(total));
				}
			}
		}
		
		var enablePayments = function(delivery_id){
			$('.j-select-payment').hide()
				.find('input[type=radio]').removeAttr('checked').attr('disabled', true);
			
			if(delivery_id){
				$('.j-remove-after-select-delivery').hide();

				var $delivery = $('.j-select-delivery:has(#delivery_' + delivery_id + ')');
				var price = price_vat(delivery[delivery_id], _vat_delivery);
				$('.j-summary .j-delivery-name').html( $delivery.find('.j-delivery-name-source').text() );
				$('.j-summary .j-delivery-price').html( displayPrice(price) );
			}
			
			$.each(delivery_payments, function(i, dp){
				var paymentPrice = estPrice(payment[dp.payment_id], _subtotal_excl_vat) + estPrice(dp.price, _subtotal_excl_vat);
				var deliveryPaymentPrice = paymentPrice + estPrice(delivery[delivery_id], _subtotal_excl_vat);
				
				if (dp.free_over && parseFloatNum(dp.free_over) <= _subtotal) {
					paymentPrice = 0;  // free delivery
					deliveryPaymentPrice = 0;
				}
				
				if(dp.delivery_id == delivery_id){
					var el = $('#payment_'+dp.payment_id).closest('.j-select-payment');
					
					el.find('input').removeAttr('disabled').data('deliveryPaymentPrice', deliveryPaymentPrice);
					el.show();
					el.find('.j-payment-price')
						.html(paymentPrice? displayPrice(price_vat(paymentPrice, _vat_delivery)): _lang.free_delivery)
						.show();
				}
			});
		}
		
		enablePayments();
		
		$('.j-select-delivery input[type=radio]').bind('change', function(){
			$('.j-delivery-payment-price').html('&mdash;');
			enablePayments(this.value);
			$('.j-delivery-name').html($(this).parent().find('label strong').html());
			
			var price = delivery[this.value];
			if (price) {
				$('.j-delivery-payment-price').html( fprice(price) + ' Kč' );
			} else {
				$('.j-delivery-payment-price').html('0,00 Kč');
			}
			
			recalcPrice(price);
		});
		
		$('.j-select-payment input[type=radio]').bind('change', function(){
			var price = $(this).data('deliveryPaymentPrice');
			
			var $payment = $('.j-select-payment:has(input:checked)');
			var paymentPrice = payment[$payment.find('input').val()];
			var deliveryName = $('.j-select-delivery:has(input:checked) .j-delivery-name-source').text();
			var paymentName = $('.j-select-payment:has(input:checked) .j-payment-name-source').text();

			$.each(delivery_payments, function(i, dp){
				if (dp.payment_id == $payment.find('input').val() && dp.price*1) {
					paymentPrice = dp.price*1;
				}
			});
			
			$('.j-delivery-payment-price').html(displayPrice(price_vat(price, _vat_delivery)));
			recalcPrice(price);
			$('.j-delivery-name').html($(this).parent().find('label strong').html());
			$('.j-delivery-payment-price').html(displayPrice(price_vat(price, _vat_delivery)));
			if($('input[name="dp_price"]').length) {
				$('input[name="dp_price"]').val(displayPrice(price_vat(price, _vat_delivery)));
			}
			if($('input[name="delivery_name"]').length) {
				$('input[name="delivery_name"]').val( deliveryName + ' - ' + paymentName );
			}

			$('.j-summary .j-payment-name').html( $payment.find('.j-payment-name-source').text() );
			$('.j-summary .j-payment-price').html(
				paymentPrice
					? displayPrice( price_vat(paymentPrice, _vat_delivery) )
					: 'ZDARMA'
			);
		});
	}
	
	$('form.j-order-dp-form, form.j-order-customer-form').bind('submit', function(){
		$(this).find('label.error').removeClass('error');

		/** @todo add .required class to template */
		var emptyfields = $(this).find('.required input:not(:disabled)').filter(function(){
			return !$(this).val();
		});

		if(emptyfields.length){
			emptyfields.parent().find('label').addClass('error');
			alert(_lang.fill_in_required_fields);
			return false;
		}
		
		var emailrx = new RegExp(/^((\w+\+*\-*)+\.?)+@((\w+\+*\-*)+\.?)*[\w-]+\.[a-z]{2,6}$/i);
		
		if($(this).find('input[name="email"]').length && ! emailrx.test($(this).find('input[name="email"]').val())){
			alert(_lang.enter_valid_email);
			return false;
		}
		
		if($(this).hasClass('order') && $(this).find('input[name="delivery"]').length && (! $(this).find('input[name="delivery"]:checked').length || ! $(this).find('input[name="payment"]:checked').length)){
			alert(_lang.select_delivery_payment);
			return false;
		}

		//if($(this).hasClass('order') && $(this).find('input[name="souhlas-osobni-udaje"]:not(:checked)').length){
		//	alert(_lang.must_accept_osobni_udaje);
		//	return false;
		//}
		
		if($(this).hasClass('order') && $(this).find('input[name="terms"]:not(:checked)').length){
			alert(_lang.must_accept_terms);
			return false;
		}
		
	});
	
	$('#shipping_as_billing').bind('change', function(){
		if(this.checked){
			$('.jina-dodaci-wrap').hide().find('input, select').attr('disabled', true);
		}else{
			$('.jina-dodaci-wrap').show().find('input, select').removeAttr('disabled');
		}
		
	}).bind('click', function(){
		$(this).trigger('change');
		
	}).trigger('change');
	
	$('.j-lost-password').bind('click', function(){
		$('.j-login-form').fadeOut(function(){
			$('.j-lost-password-form').fadeIn();
		});
		return false;
	});

	$('.j-login-back').bind('click', function(){
		$('.j-lost-password-form').fadeOut(function(){
			$('.j-login-form').fadeIn();
		});
		return false;
	});
	
	$('.producers a.toggle-producers').bind('click', function(){
		var el = $(this).parent().find('.wrap');
		
		if(el.is(':visible')){
			el.slideUp('fast');
			
		}else{
			el.slideDown('fast');
		}
		
		return false;
	});
	
	$('.producers input[type="checkbox"]').bind('click', function(){
		var wrap = $(this).parent().parent().parent();
		var all = wrap.find('input[name=""]');
		
		if(this.name){
			all.removeAttr('checked');
			
		}else{
			$('input[type="checkbox"][name!=""]', wrap).removeAttr('checked');
		}
		
		if(! $('input[type="checkbox"]:checked', wrap).length){
			all.attr('checked', true);
		}
	});
	
	// attribute images
	var swapImage = function(file_id){
		var li = $('.fid-'+file_id);
		
		if(li.length){
			var wrap = li.parents('.files:first');
			var li_current = wrap.find('.picture:first');
			var li_clone = li.clone();
			var li_current_clone = li_current.clone();
			
			if(! li_current || li_current[0] == li[0]){
				return;
			}
			
			if(li_clone.hasClass('smallpic')){
				li_current_clone.addClass('smallpic');
				li_clone.removeClass('smallpic');
			}
			
			var size_match = li_clone.find('img').attr('src').match(/\/_(\d+x\d+)\//);
			
			if(size_match){
				li_current_clone.find('img').attr('src', li_current_clone.find('img').attr('src').replace(/\/_(\d+x\d+)\//, size_match[0]));
			}
			
			size_match = li_current.find('img').attr('src').match(/\/_(\d+x\d+)\//);
			
			if(size_match){
				li_clone.find('img').attr('src', li_clone.find('img').attr('src').replace(/\/_(\d+x\d+)\//, size_match[0]));
			}
			
			li_clone.hide();
			
			li.replaceWith(li_current_clone);
			li_current.replaceWith(li_clone);
			
			li_clone.fadeIn();
			
			$('a.lightbox').fancybox({
				titlePosition: 'over'
			});
		}
	}
	
	var showPrice = function(){
		var total = parseFloat(_product_price);
		var total_no_discount = parseFloat(_product_price_before_discount);
		
		$('input[name^="attributes["]:checked, select[name^="attributes["]').each(function(){
			var attr_id = this.value;
			
			if(product_attr_prices[attr_id]){
				total += product_attr_prices[attr_id];
				total_no_discount += product_attr_prices[attr_id];
			}
		});

		_product_price_final = total;

		$('#product-price')
			.html(displayPrice(price_vat(total, _product_vat)))
			.trigger('change');
		$('#product-price-excl-vat')
			.html(displayPrice(price_unvat(total, _product_vat)))
			.trigger('change');
		
		if(_product_discount){
			$('#product-old-price')
				.html(displayPrice(price_vat(total_no_discount, _product_vat)))
				.trigger('change');
		}
	}
	
	$('input[name^="attributes["]').bind('click', function(){
		var attr_id = this.value;
		
		if(product_attr_files[attr_id]){
			swapImage(product_attr_files[attr_id]);
		}
		
		showPrice();
		
	}).filter(':checked').trigger('click');
	
	$('select[name^="attributes["]').bind('change', function(){
		var attr_id = this.value;
		
		if(product_attr_files[attr_id]){
			swapImage(product_attr_files[attr_id]);
		}
		
		showPrice();
		
	}).trigger('change');



	/********************* PAGE EXPLORER *********************/



	var PageExplorer = {
		init: function(){
			jQuery('.j-pages-container .j-page-link:not(:has(.j-page-expand-link))').each(function(){
				var $pageLink = jQuery(this);
				
				$pageLink.prepend('<span class="j-page-expand-link closed"></span>');
				PageExplorer.rebind();
				PageExplorer.checkLinksState();
			});
		},


		expandLinkClicked: function(){
			var $expandLink = jQuery(this);

			if (PageExplorer.isExpandLinkOpen($expandLink)) {
				PageExplorer.closeExpandLink($expandLink);
			} else {
				PageExplorer.openExpandLink($expandLink);				
			}

			return false;
		},


		openExpandLink: function($expandLink){
			var $pageConteiner = $expandLink.closest('.j-page-container');

			if ($pageConteiner.find('.j-pages-container').length) {
				$pageConteiner.find('.j-pages-container:eq(0)').show(200);				
			} else if(!$expandLink.is('.empty')) {
				jQuery.ajax({
					url: $expandLink.closest('.j-page-link').attr('data-expand-url'),
					beforeSend: function(){
						/** @todo start loading signalisation */
					},
					success: function(data){
						var $subPages = jQuery(data);

						$subPages.hide()
							.appendTo($pageConteiner)
							.show(200);

						if (data) {
							PageExplorer.init();  // init new page links
						} else {
							$expandLink.addClass('empty');
						}
					},
					complete: function(){
						/** @todo stop loading signalisation */
					}
				});
			}

			$expandLink.removeClass('closed')
				.addClass('open');
		},


		closeExpandLink: function($expandLink){
			$expandLink.closest('.j-page-container').find('.j-pages-container:eq(0)').hide(200);

			$expandLink.removeClass('open')
				.addClass('closed');
		},


		rebind: function(){
			jQuery('.j-pages-container .j-page-expand-link').each(function(){
				var $this = jQuery(this);

				$this.unbind()
				$this.bind('click', PageExplorer.expandLinkClicked);
			});
		}, 


		checkLinksState: function(){
			jQuery('.j-pages-container .j-page-expand-link').each(function(){
				var $this = jQuery(this);
				if (PageExplorer.isExpandLinkOpen($this)) {
					$this.removeClass('closed')
						.addClass('open');
				}
			});
		},


		isExpandLinkOpen: function($expandLink){
			return ($expandLink.closest('.j-page-container').find('.j-pages-container:visible').length > 0);
		}
	};

	PageExplorer.init();



	/********************* ANTISPAM FILTER *********************/



	jQuery('.js-hidden input[name=robot]').val('');
	
});



var product_attr_files = {};
var product_attr_prices = {};

// attribute images
function attrFiles(attr_id, file_id)
{
	product_attr_files[attr_id] = file_id;
}

// attribute prices
function attrPrices(attr_id, price)
{
	product_attr_prices[attr_id] = price;
}