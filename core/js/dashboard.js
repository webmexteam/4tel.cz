$(function(){
	// Notes
	$('#save-note').click(function(){
		var text = $(this).parent().find('textarea').val().trim();
		if(text == "" || text == null || !text) {
			alert('Nejdříve vyplňte text zprávy');
			return false;
		}

		$.ajax({
			url: _base+'admin/ajax/save_note',
			type: "POST",
			data: {
				'message': text,
				'color': $('#note-labels label input:checked').val()
			},
			dataType: "json",
			success: renderNotes
		});

		$(this).parent().find('textarea').val("")

		return false;
	});

	$(document).on('click', '.note .note-remove', function(){
		if(!confirm('Opravdu chcete smazat tuto poznámku?'))
			return false;
		$.ajax({
			url: _base+'admin/ajax/remove_note',
			type: "POST",
			data: {
				'id': $(this).closest('.note').attr('data-id')
			},
			dataType: "json",
			success: renderNotes
		});
		return false;
	});

	$(document).on('click', '.note .note-edit', function(){
		$(this).closest('.note').find('.edit').show();
		$(this).closest('.note').find('.message').hide();
		$(this).closest('.note').find('a').hide();
		return false;
	});

	$(document).on('click', '.note button', function(){
		var name = $(this).attr('name');
		if(name == "save") {
			$.ajax({
				url: _base+'admin/ajax/edit_note',
				type: 'POST',
				data: {
					'id': parseInt($(this).closest('.note').attr('data-id')),
					'message': $(this).closest('.note').find('textarea').val()
				},
				dataType: "json",
				success: renderNotes
			});
			return false;
		}

		$.ajax({
			url: _base+'admin/ajax/get_notes',
			type: "GET",
			dataType: "json",
			success: renderNotes
		});

		return false;
	});

	function renderNotes(response) {
		var html = '';
		$.each(response, function(i){

			if(i == 0) {
				html += '<div class="row-fluid">';
			}

			if(i%4==0) {
				html += '</div><div class="row-fluid">';
			}

			html += '<div class="span3">\
						<div class="note" data-id="'+response[i].id+'" style="background:'+response[i].color+'">\
							<div class="row-fluid">\
								<div class="span9">\
									<div class="date">'+response[i].date+'</div>\
								</div>\
								<div class="span3">\
									<a href="#" class="note-remove"></a>\
									<a href="#" class="note-edit"></a>\
								</div>\
								<div class="edit clearfix">\
									<textarea class="span12">'+response[i].message+'</textarea>\
									<button type="submit" name="save">uložit</button>\
									<button type="submit" name="cancel" class="cancel">zrušit</button>\
								</div>\
							</div>\
							<div class="message">'+response[i].message+'</div>\
						</div>\
					</div>';

			if(i == response.length) {
				html += '</div>';
			}
		});
		$('#data-notes').html(html);
	}

	if($('#data-notes').length) {
		$.ajax({
			url: _base+'admin/ajax/get_notes',
			type: "GET",
			dataType: "json",
			success: renderNotes
		});
	}	

	function renderAnalytics(response) {
		renderTable1(response);
		renderTable2(response);
		renderTable3(response);
		renderTable4(response);
		renderGraph1(response);
		renderGraph4(response);
	}

	function renderDatabase(response) {
		renderGraph2(response);
		renderGraph3(response);
	}

	function renderTable1(response) {
		var cls;
		response = response.visitors_table;
		if(response.data.length) {
			$('#table-1 .loading-remove').remove();
			$("#table-1 tbody").append('<tr class="top-tr"><td>&nbsp;</td><td>'+response.total+'</td><td>100%</td></tr>')
			$.each(response.data, function(i){
				cls = 'show-me';
				if(i > 4) {
					cls = 'hide-me';
				}
				$("#table-1 tbody").append('<tr class="'+cls+'"><td>'+response.data[i].website+'</td><td>'+response.data[i].value+'</td><td>'+response.data[i].percentage+'%</td></tr>');
			});
			if(cls == 'hide-me') {
				$("#table-1 tfoot").html('<tr><td colspan="3"><a href="#" class="show-all-data next-show" data-nexttext="Zobrazit méně dat">Zobrazit všechna data</a></td></tr>');
			}
		}
	}

	function renderTable2(response) {
		var cls;
		response = response.view_table;
		if(response.data.length) {
			$('#table-2 .loading-remove').remove();
			$("#table-2 tbody").append('<tr class="top-tr"><td>&nbsp;</td><td>'+response.total+'</td><td>100%</td></tr>')
			$.each(response.data, function(i){
				cls = 'show-me';
				if(i > 4) {
					cls = 'hide-me';
				}
				var date = new Date(response.data[i].day);
				$("#table-2 tbody").append('<tr class="'+cls+'"><td>'+date.getDate()+'. '+(parseInt(date.getMonth())+1)+'. '+date.getFullYear()+'</td><td>'+response.data[i].value+'</td><td>'+response.data[i].percentage+'%</td></tr>');
			});
			if(cls == 'hide-me') {
				$("#table-2 tfoot").html('<tr><td colspan="3"><a href="#" class="show-all-data next-show" data-nexttext="Zobrazit méně dat">Zobrazit všechna data</a></td></tr>');
			}
		}
	}

	function renderTable3(response) {
		var cls;
		response = response.traffic_table;
		if(response.data.length) {
			$('#table-3 .loading-remove').remove();
			$("#table-3 tbody").append('<tr class="top-tr"><td>&nbsp;</td><td>'+response.total+'</td><td>100%</td></tr>')
			$.each(response.data, function(i){
				cls = 'show-me';
				if(i > 4) {
					cls = 'hide-me';
				}
				var date = new Date(response.data[i].day);
				$("#table-3 tbody").append('<tr class="'+cls+'"><td>'+response.data[i].source+'</td><td>'+response.data[i].value+'</td><td>'+response.data[i].percentage+'%</td></tr>');
			});
			if(cls == 'hide-me') {
				$("#table-3 tfoot").html('<tr><td colspan="3"><a href="#" class="show-all-data next-show" data-nexttext="Zobrazit méně dat">Zobrazit všechna data</a></td></tr>');
			}
		}
	}

	function renderTable4(response) {
		var cls;
		response = response.basic_table;
		if(response.data.length) {
			$('#table-4 .loading-remove').remove();
			$.each(response.data, function(i){
				cls = 'show-me';
				if(i > 4) {
					cls = 'hide-me';
				}
				var date = new Date(response.data[i].day);
				$("#table-4 tbody").append('<tr class="'+cls+'"><td>'+response.data[i].name+'</td><td>'+response.data[i].value+'</td></tr>');
			});
			if(cls == 'hide-me') {
				$("#table-4 tfoot").html('<tr><td colspan="3"><a href="#" class="show-all-data next-show" data-nexttext="Zobrazit méně dat">Zobrazit všechna data</a></td></tr>');
			}
		}
	}

	$(document).on('click', '.show-all-data', function(){
		var text = $(this).attr('data-nexttext');

		if($(this).hasClass('next-show')) {
			$(this).closest('table').find('tr.hide-me').show();
			$(this).attr('data-nexttext', $(this).text());
			$(this).text(text);
			$(this).removeClass('next-show').addClass('next-hide');
		}else{
			$(this).closest('table').find('tr.hide-me').hide();
			$(this).attr('data-nexttext', $(this).text());
			$(this).text(text);
			$(this).removeClass('next-hide').addClass('next-show');
		}

		$('html,body').animate({
			scrollTop: $('#'+$(this).closest('table').attr('id')).offset().top
		}, 600);
		return false;
	});


	// Dashboard
	var graphSettings = {
		xaxes: [{
			mode: "time",
			timeformat: "%e.%m.",
			timezone: "browser",
			labelWidth: 0,
      		tickSize: [2, "day"]
		}],
		yaxes: [{
			tickLength: 0,
			axisLabel: ""
			//ticks: []
		}],
   		grid: {
   			hoverable: true,
            clickable: true,
            borderWidth: 0
   		},
   		points: {
			show: true,
			backgroundColor: 'red'
		},
		lines: {
			show: true
		},
   		series: {
	        lines: {
	        	lineWidth: 4,
	        	fill: true,
	        	fillColor: 'e5f3f9',
	        	label: {
	        		show: false
	        	}
	        },
	        shadowSize: 0
	    }
	};

	if($('#new-dashboard').length) {
		var w = $('.tabsWrap .tab.active').width();
		var h = $('.tabsWrap .tab.active').height();
		$('.tabsWrap .tab').each(function(){
			$(this).find('.graph').css({'width':w, 'height':h});
		})
	}

	function renderGraph1(series) {
		series = series.visitors_chart;
		var data = [];
		var obj = {};
		obj.color = '058dc7';
		obj.width = 50;
		obj.data = [];

		if(!series || !series.length) {
			$("#graph-1 .remove").text('Nastala chyba při komunikaci s Google Analytics. Opakujte akci později.');
			return false;
		}
		$.each(series, function(i){
			obj.data.push([series[i].day, series[i].value]);
		});
		data.push(obj);
		if($('#graph-1').length)
			$.plot("#graph-1", data, graphSettings);
	}

	function labelFormatter(label, series) {
		return "<div style='font-size:8pt; text-align:center; padding:2px; color:white;background:#3a3a3a;opacity:0.8'>" + label + "<br/>" + Math.round(series.percent) + "%</div>";
	}
	function renderGraph4(series) {
		series = series.traffic_chart;
		var data = [];
		var obj = {};
		obj.color = '058dc7';
		obj.width = 50;
		obj.data = [];

		if(!series || !series.length) {
			$("#graph-4 .remove").text('Nastala chyba při komunikaci s Google Analytics. Opakujte akci později.');
			return false;
		}
		$.each(series, function(i){
			data[i] = {
				label: series[i].source,
				data: series[i].value
			}
		});

		var s = {
		    series: {
		        pie: {
		            show: true,
		            radius: 1,
		            label: {
		                show: true,
		                radius: 3/4,
		                formatter: labelFormatter,
		                background: {
		                    opacity: 0.5,
		                    color: '#000'
		                }
		            }
		        }
		    },
		    legend: {
		        show: false
	    	}
		};

		if($('#graph-4').length)
			$.plot('#graph-4', data, s);
	}

	function renderGraph2(series) {
		series = series.count;
		var data = [];
		var obj = {};
		obj.color = '058dc7';
		obj.width = 50;
		obj.data = [];

		if(!series || !series.length) {
			$("#graph-2 .remove").text('Nastala chyba při komunikaci s databází. Opakujte akci později.');
			return false;
		}
		$.each(series, function(i){
			obj.data.push([series[i].day, series[i].value]);
		});
		data.push(obj);
		if($('#graph-2').length)
			$.plot("#graph-2", data, graphSettings);
	}

	function renderGraph3(series) {
		series = series.total;
		var data = [];
		var obj = {};
		obj.color = '058dc7';
		obj.width = 50;
		obj.data = [];

		if(!series || !series.length) {
			$("#graph-3 .remove").text('Nastala chyba při komunikaci s databází. Opakujte akci později.');
			return false;
		}
		$.each(series, function(i){
			obj.data.push([series[i].day, series[i].value]);
		});
		data.push(obj);
		if($('#graph-3').length)
			$.plot("#graph-3", data, graphSettings);
	}

	function showTooltip(x, y, contents, z) {
        $('<div id="flot-tooltip">' + contents + '</div>').css({
            top: y - 35,
            left: x + 10
        }).appendTo("body").show();
    }

    $("#graph-1").bind("plothover", function (event, pos, item) {
        if (item) {
            if (previousPoint != item.datapoint) {
                previousPoint = item.datapoint;
                $("#flot-tooltip").remove();
                var date = new Date(item.datapoint[0]);
		        var formattedTime = date.getDate()+'.'+(parseInt(date.getMonth())+1)+'.';
                showTooltip(item.pageX, item.pageY,"<b>datum:</b> "+formattedTime+"<br /><b>návštěv</b>: "+item.datapoint[1]);
            }
        } else {
            $("#flot-tooltip").remove();
            previousPoint = null;
        }
    });

    $("#graph-2").bind("plothover", function (event, pos, item) {
        if (item) {
            if (previousPoint != item.datapoint) {
                previousPoint = item.datapoint;
                $("#flot-tooltip").remove();
                var date = new Date(item.datapoint[0]);
		        var formattedTime = date.getDate()+'.'+(parseInt(date.getMonth())+1)+'.';
                showTooltip(item.pageX, item.pageY,"<b>datum:</b> "+formattedTime+"<br /><b>objednávky</b>: "+item.datapoint[1]);
            }
        } else {
            $("#flot-tooltip").remove();
            previousPoint = null;
        }
    });

    $("#graph-3").bind("plothover", function (event, pos, item) {
        if (item) {
            if (previousPoint != item.datapoint) {
                previousPoint = item.datapoint;
                $("#flot-tooltip").remove();
                var date = new Date(item.datapoint[0]);
		        var formattedTime = date.getDate()+'.'+(parseInt(date.getMonth())+1)+'.';
                showTooltip(item.pageX, item.pageY,"<b>datum:</b> "+formattedTime+"<br /><b>obrat</b>: "+displayPrice(item.datapoint[1]));
            }
        } else {
            $("#flot-tooltip").remove();
            previousPoint = null;
        }
    });

    if($("#graph-2").length || $("#graph-3").length) {
		$.ajax({
			url: _base+'admin/ajax/orders',
			type: "GET",
			dataType: "json",
			success: renderDatabase
		});
	}

    if($("#graph-1").length || $("#graph-4").length || $("#table-1").length) {
    	$("#table-1 tbody").append('<tr class="loading-remove"><td colspan="3">Načítám data z Google Analytics...</td></tr>');
    	$("#table-2 tbody").append('<tr class="loading-remove"><td colspan="3">Načítám data z Google Analytics...</td></tr>');
    	$("#table-3 tbody").append('<tr class="loading-remove"><td colspan="3">Načítám data z Google Analytics...</td></tr>');
    	$("#table-4 tbody").append('<tr class="loading-remove"><td colspan="3">Načítám data z Google Analytics...</td></tr>');
		$.ajax({
			url: _base+'admin/ajax/google_analytics',
			type: "GET",
			dataType: "json",
			success: renderAnalytics
		});
	}

	$('#new-dashboard #refresh-analytics').click(function(){
		var date = $(this).parent().find('input').val();
		var splitted = date.split('až');
		if(splitted.length != 2){
			alert('Datumy nejsou ve správném tvaru.');
			return false;
		}
		var from = $.trim(splitted[0]);
		var to = $.trim(splitted[1]);

		if($('#graph-2').length || $('#graph-3').length) {
			$('#graph-2').html('<div class="remove">Načítám data z databáze...</div>');
			$('#graph-3').html('<div class="remove">Načítám data z databáze...</div>');

			$.ajax({
				url: _base+'admin/ajax/orders?from='+encodeURIComponent(from)+'&to='+encodeURIComponent(to),
				type: "GET",
				dataType: "json",
				success: renderDatabase
			});
		}

		if($('#graph-1').length || $('#graph-4').length || $("#table-1").length) {
			$('#graph-1, #graph-4').html('<div class="remove">Načítám data z Google analytics...</div>');

			$("#table-1 tbody tr, #table-1 tfoot tr").remove();
			$("#table-1 tbody").append('<tr class="loading-remove"><td colspan="3">Načítám data z Google Analytics...</td></tr>');

			$("#table-2 tbody tr, #table-2 tfoot tr").remove();
			$("#table-2 tbody").append('<tr class="loading-remove"><td colspan="3">Načítám data z Google Analytics...</td></tr>');

			$("#table-3 tbody tr, #table-3 tfoot tr").remove();
			$("#table-3 tbody").append('<tr class="loading-remove"><td colspan="3">Načítám data z Google Analytics...</td></tr>');

			$("#table-4 tbody tr, #table-4 tfoot tr").remove();
			$("#table-4 tbody").append('<tr class="loading-remove"><td colspan="3">Načítám data z Google Analytics...</td></tr>');

			$.ajax({
				url: _base+'admin/ajax/google_analytics?from='+encodeURIComponent(from)+'&to='+encodeURIComponent(to),
				type: "GET",
				dataType: "json",
				success: renderAnalytics
			});
		}

		return false;
	});

	// Autosize textarea
	$('.autosize').autosize();
});