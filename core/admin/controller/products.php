<?php

defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */
class Controller_Products extends AdminController
{

	public function __construct()
	{
		parent::__construct();

		Core::$active_tab = 'products';
	}

	public function index()
	{
		gatekeeper('products-edit');

		$order_by = 'name';
		$order_dir = 'ASC';

		sortDir($order_by, $order_dir);

		if (!isSet($_SESSION['bulk'])) {
			$_SESSION['bulk'] = array();
		}

		$_SESSION['bulk'] = array_unique($_SESSION['bulk']);

		if (isSet($_POST['action']) && !empty($_POST['item'])) {
			foreach ($_POST['item'] as $id => $v) {

				if ($_POST['action'] == 'delete') {
					Core::$api->product->delete((int) $id);
				} else if ($_POST['action'] == 'bulk') {
					$_SESSION['bulk'][] = $id;
				}
			}
			flashMsg(__('msg_saved'));
			redirect('admin/products');
		}

		if (isSet($_POST['save']) && !empty($_POST['position'])) {
			foreach ($_POST['position'] as $id => $position) {

				$data = array(
					'price' => !empty($_POST['price'][$id]) ? parseFloat($_POST['price'][$id]) : null,
					'stock' => (isSet($_POST['stock'][$id]) && $_POST['stock'][$id] !== '') ? (int) $_POST['stock'][$id] : null,
					'position' => ($position !== '' ? (int) $position : null),
					'status' => isSet($_POST['status'][$id]) ? 1 : 0
				);

				Core::$api->product->update((int) $id, $data);
			}

			flashMsg(__('msg_saved'));
		}

		if (isSet($_GET['search'])) {
			$q = trim($_GET['search']);

			if (isSet($_GET['cancel_search'])) {
				redirect(url('admin/products', null, true, null));
			}

			if (!empty($q)) {
				$products = Search::findProducts($q);
			}

			if (!$products) {
				$products = Core::$db->product();
			}

			if ($products && !empty($_GET['category'])) {
				if ($page = Core::$db->page[(int) $_GET['category']]) {
					$this->getSubpages($page['id'], $ids);

					$products->where('id IN (' . Core::$db->product_pages()->where('page_id', $ids)->select('product_id') . ') OR id IN (' . $page->product_pages()->select('product_id') . ')');
				}
			}

			if (!empty($_GET['filter_what'])) {
				if ($_GET['filter_what']{0} != '_') {
					// param
					if ($fw = $this->buildFilterWhere($_GET['filter_what'], $_GET['filter_value'])) {
						$products->where($this->buildFilterWhere($_GET['filter_what'], $_GET['filter_value']));
					}
				} else {
					// no image/category
					if ($_GET['filter_what'] == '_no_image') {
						$products->where('id NOT IN (' . Core::$db->product_files()->where('is_image', 1)->select('product_id') . ')');
					} else if ($_GET['filter_what'] == '_no_category') {
						$products->where('id NOT IN (' . Core::$db->product_pages()->select('product_id') . ')');
					}
				}
			}
		} else {
			$products = Core::$db->product();
		}

		if ($products) {
			$products->order($order_by . ' ' . $order_dir)->limit(pgLimit(), pgOffset());

			$total_count = Core::$db->product;

			if ($products->getWhere()) {
				$total_count->where($products->getWhere());
			}

			$total_count = $total_count->group("COUNT(*)");
			$total_count = (int) $total_count[0];
		} else {
			$products = array();
			$total_count = 0;
		}

		$this->content = tpl('products/list.latte', array(
			'order_by' => $order_by,
			'order_dir' => $order_dir,
			'total_count' => $total_count,
			'products' => $products,
			'categories' => '<option value="">' . __('all') . '</option>' . $this->_pageTree(array((int) $_GET['category']))
				));
	}

	public function edit($id)
	{
		gatekeeper('products-edit');

		Event::run('Controller_Products::edit', $id);

		$product = Core::$db->product[(int) $id];

		if ($id > 0 && !$product) {
			flashMsg(__('msg_record_not_found', $id), 'error');
			redirect('admin/products');
		}

		$page_ids = array();
		$feature_values = array();
		$labels = array();
		$related_products = array();

		if ($id) {
			foreach ($product->product_pages() as $p) {
				$page_ids[] = $p['page_id'];
			}

			$attributes = array();
			foreach ($product->product_attributes()->order('id ASC, name ASC') as $attr) {
				$attributes[$attr['name']][] = $attr;
			}

			foreach ($product->product_labels() as $label) {
				$labels[] = $label['label_id'];
			}

			foreach ($product->product_related() as $related) {
				$related_products[] = Core::$db->product[(int) $related['related_id']];
			}
		}

		$availabilities = array("" => '&mdash;');
		foreach (Core::$db->availability() as $av) {
			$availabilities[$av['id']] = $av['name'];
		}

		$vat_rates = array(0 => '&mdash;');
		foreach (Core::$db->vat()->order('rate ASC') as $vat) {
			$vat_rates[$vat['id']] = (float) $vat['rate'] . ' % (' . $vat['name'] . ')';
		}

		if (!empty($_POST)) {
			if (($errors = validate(array('name', "availability_id"))) === true) {
				$data = $_POST;

				if ($product) {
					// push empty toggable values
					$data = array_merge($product->as_array(), prepare_data('product', $data), $data);
				}

				Event::run('Controller_Products::edit.before_save', $product, $data);

				if (!empty($data['features'])) {
					$data['features'] = Core::$api->bucket($data['features'], array(
						'featureset_id' => $data['featureset_id']
							));
				}

				if (empty($data['pages'])) {
					$data['pages'] = array();
				}

				if (empty($data['labels'])) {
					$data['labels'] = array();
				}

				if (empty($data['related'])) {
					$data['related'] = array();
				}

				$product = Core::$api->product->save($data);

				if ($product) {

					if (!empty($data['discounts']) && Core::$is_premium) {
						$discounts = array();

						foreach ($data['discounts']['quantity'] as $discount_id => $qty) {
							$discounts[] = array(
								'quantity' => (int) $qty,
								'value' => (float) $data['discounts']['value'][$discount_id]
							);
						}

						Core::$api->product->setQuantityDiscounts($product, $discounts);
					}

					$files = array();
					$files_map = array();

					if (!empty($_POST['files_filename'])) {
						foreach ($_POST['files_filename'] as $file_id => $filename) {

							if (!empty($_POST['files_delete']) && in_array($file_id, $_POST['files_delete'])) {
								continue;
							}

							$files[] = array(
								'filename' => $filename,
								'description' => $_POST['files_description'][$file_id],
								'position' => $_POST['files_position'][$file_id],
								'size' => (isSet($_POST['files_size'][$file_id])) ? $_POST['files_size'][$file_id] : null,
								'align' => (isSet($_POST['files_align'][$file_id])) ? $_POST['files_align'][$file_id] : null
							);

							$files_map[$file_id] = $filename;
						}
					}

					Core::$api->product->setFiles($product, $files);

					if (!empty($data['attr']) && Core::$is_premium) {
						$attributes = array();

						foreach ($data['attr'] as $attr) {
							$variants = isSet($attr['variants']) ? true : false;
							$default = $attr['is_default'];

							foreach ($attr as $i => $val) {
								if (is_numeric($i)) {
									$attributes[] = array(
										'id'              => $val['id'] ? $val['id'] : null,
										'name'            => $attr['name'],
										'value'           => $val['value'],
										'price'           => $val['price'],
										'weight'          => (float) $val['weight'],
										'is_default'      => ($i == $default ? 1 : 0),
										'sku'             => (!$variants ? null : $val['sku']),
										'ean13'           => (!$variants ? null : $val['ean13']),
										'stock'           => (!$variants ? null : ($val['stock'] !== '' ? $val['stock'] : null)),
										'availability_id' => (!$variants ? null : ($val['availability_id'] !== '' ? $val['availability_id'] : null)),
										'filename'        => ($val['file_id'] ? $files_map[$val['file_id']] : null)
									);
								}
							}
						}

						Core::$api->product->setAttributes($product, $attributes);
					}

					Event::run('Controller_Products::edit.after_save', $product, $data);

					flashMsg(__('msg_saved'));
				} else {
					flashMsg(__('msg_error'), 'error');
				}

				if (isSet($_POST['save_go'])) {
					redirect(url('admin/products', array(),true,true));
				} else {
					redirect(url('admin/products/edit/' . $product['id'], array(),true,true));
				}
			}
		} else if ($id == 0) {
			$product['id'] = 0;
			$product['name'] = __('new_product');
			$product['position'] = 0;
			$product['status'] = 1;
			$product['show_sku'] = 1;

			if(!Core::canAddNewProduct()){
				flashMsg(__('msg_cant_add_new_product'), 'error');
				redirect('admin/products');
			}
		}

		$featuresets = array('0' => '&mdash;');
		foreach (Core::$db->featureset()->order('name ASC') as $fset) {
			$featuresets[$fset['id']] = $fset['name'];
		}

		$attribute_templates = array('0' => '&mdash;');
		foreach (Core::$db->product_attribute_template()->order('name ASC') as $tpl) {
			$attribute_templates[$tpl['id']] = $tpl['name'];
		}

		$this->content = tpl('products/edit.latte', array(
			'product' => $product,
			'availabilities' => $availabilities,
			'pages' => $this->_pageTreeCheckboxes($page_ids),
			'attributes' => $attributes,
			'vat_rates' => $vat_rates,
			'featuresets' => $featuresets,
			'feature_values' => $feature_values,
			'labels' => $labels,
			'related_products' => $related_products,
			'attribute_templates' => $attribute_templates
				));

		Event::run('Controller_Products::edit.render', $product, $this->content);
	}

	private function buildFilterWhere($param, $value)
	{
		$parts = preg_split('/\,|\;|(\s(?=(AND|OR|\&+|\|+)))/i', $value);
		$query = array();

		if ($param == 'availability') {
			$param = 'availability_id';
		}

		foreach ($parts as $part) {
			$part = trim($part);
			$lop = 'OR';
			$op = '=';
			$value = $part;

			if (preg_match('/^(AND|OR|\&+|\|+)(.*)$/i', $part, $m)) {
				$lop = strtoupper($m[1]);
				$part = trim($m[2]);
				$value = $part;

				if ($lop{0} == '&') {
					$lop = 'AND';
				}

				if ($lop{0} == '|') {
					$lop = 'OR';
				}
			}

			if (preg_match('/^([\>\<\=\!]+)(.*)$/', $part, $m)) {
				$op = $m[1];
				$value = trim($m[2]);
			}

			if ($op == '=<') {
				$op = '<=';
			}

			if ($op == '=>') {
				$op = '>=';
			}

			if (strtolower($value) == 'null') {
				$value = null;
			}

			if (strtolower($value) == 'true') {
				$value = 1;
			}

			if (strtolower($value) == 'false') {
				$value = 0;
			}

			if ($value === null) {
				if ($op == '=') {
					$op = 'IS';
				} else {
					$op = 'IS NOT';
				}
			}

			if (strrpos($value, '*') !== false) {
				$op = 'LIKE';
				$value = preg_replace('/\*/', '%', $value);
			}

			if ($param == 'availability_id') {
				if ($av = Core::$db->availability()->where('name LIKE "' . $value . '" OR id = "' . $value . '"')->fetch()) {
					$value = $av['id'];
				} else {
					continue;
				}
			}

			if ($value !== '') {
				$query[] = $lop . ' ' . Core::$db_inst->quote_identifier($param) . ' ' . $op . ' ' . Core::$db_inst->quote($value);
			}
		}

		return count($query) > 0 ? preg_replace('/^\s?(OR|AND)\s?/', '', join(' ', $query)) : null;
	}

	public function bulk($action = null, $product_id = null)
	{
		gatekeeper('products-edit');

		if ($action == 'cancel') {
			unset($_SESSION['bulk']);
			redirect('admin/products');
		} else if ($action == 'remove' && $product_id) {
			unset($_SESSION['bulk'][array_search($product_id, $_SESSION['bulk'])]);
			redirect('admin/products/bulk');
		}

		$params = array(
			'' => __('select_param'),
			'status' => __('status'),
			'price' => __('price'),
			'price_old' => __('price_old'),
			'vat_id' => __('vat'),
			'stock' => __('stock'),
			'availability_id' => __('availability_id'),
			'description' => __('description'),
			'promote' => __('promote'),
			'guarantee' => __('guarantee'),
			'recycling_fee' => __('recycling_fee'),
			'copyright_fee' => __('copyright_fee'),
			'discount' => __('discount'),
			'paid_listing' => __('paid_listing'),
			'remove_page' => __('bulk_remove_page'),
			'remove_page_all' => __('bulk_remove_tree_page'),
			'add_page' => __('bulk_add_page'),
		);

		if (!empty($_POST['param'])) {
			$update_data = array();

			foreach ($_POST['param'] as $param_id => $param) {
				if ($param) {
					$update_data[$param] = trim($_POST['value'][$param_id]);

					if ($param == 'availability_id' && !is_numeric($update_data[$param])) {
						if ($av = Core::$db->availability()->where('name LIKE "' . $update_data[$param] . '%"')->fetch()) {
							$update_data[$param] = $av['id'];
						}
					} else if ($param == 'vat_id') {
						$value = (float) preg_replace('/[^\d]/', '', $update_data[$param]);

						if ($vat = Core::$db->vat()->where('rate', $value)->fetch()) {
							$update_data[$param] = $vat['id'];
						}
					}
				}
			}

			foreach (array_unique($_SESSION['bulk']) as $prodct_id) {
				if ($product = Core::$db->product[$prodct_id]) {
					$data = $update_data;

					foreach ($data as $n => $v) {

						if($n == 'add_page') {
							if(!Core::$db->product_pages()->where('product_id', $prodct_id)->where('page_id', $v)->fetch())
								Core::$db->product_pages(array('product_id' => $prodct_id, 'page_id' => $v));
							continue;
						}
						if($n == 'remove_page') {
							Core::$db->product_pages()->where('product_id', $prodct_id)->where('page_id', $v)->delete();
							continue;
						}
						if($n == 'remove_page_all') {
							Core::$db->product_pages()->where('product_id', $prodct_id)->where('page_id', $v)->delete();

							$parents = Core::$db->page()->where('parent_page', $v);
							foreach($parents as $parent) {
								Core::$db->product_pages()->where('product_id', $prodct_id)->where('page_id', $parent['id'])->delete();

								$parents2 = Core::$db->page()->where('parent_page', $parent['id']);
								foreach($parents2 as $parent2) {
									Core::$db->product_pages()->where('product_id', $prodct_id)->where('page_id', $parent2['id'])->delete();

									$parents3 = Core::$db->page()->where('parent_page', $parent2['id']);
									foreach($parents3 as $parent3) {
										Core::$db->product_pages()->where('product_id', $prodct_id)->where('page_id', $parent3['id'])->delete();

										$parents4 = Core::$db->page()->where('parent_page', $parent3['id']);
										foreach($parents4 as $parent4) {
											Core::$db->product_pages()->where('product_id', $prodct_id)->where('page_id', $parent4['id'])->delete();

											$parents5 = Core::$db->page()->where('parent_page', $parent4['id']);
											foreach($parents5 as $parent5) {
												Core::$db->product_pages()->where('product_id', $prodct_id)->where('page_id', $parent5['id'])->delete();
											}
										}
									}
								}
							}

							continue;
						}

						if (preg_match('/^(\+|\-)(.*)/', $v, $m) && $m[1] && in_array($n, array('price', 'price_old', 'stock', 'recycling_fee', 'copyright_fee'))) {
							$op = $m[1];
							$change = trim($m[2]);

							if (preg_match('/^([\d\,\.]+)\s?\%$/', $change, $m)) {
								$change = $product[$n] / 100 * parseFloat($m[1]);
							}

							$data[$n] = $product[$n] + (parseFloat($change) * ($op == '-' ? -1 : 1));
						}

						if ($v == 'true') {
							$data[$n] = 1;
						}

						if ($v == 'false') {
							$data[$n] = 0;
						}

						if ($v == 'null' || $v === '') {
							$data[$n] = null;
						}
					}

					$data = prepare_data('product', $data, false);

					$product->update($data);
				}
			}

			flashMsg(__('msg_saved'));

			redirect('admin/products');
		}

		$this->content = tpl('products/bulk.latte', array(
			'params' => $params,
			'product_ids' => array_unique($_SESSION['bulk'])
				));
	}

	public function ajax_features($product_id, $featureset_id)
	{

		$this->tpl = null;

		if ((int) $featureset_id) {
			$fset = Core::$db->featureset[$featureset_id];
			$tbl = 'features_' . (int) $featureset_id;
			$values = Core::$db->$tbl()->where('product_id', (int) $product_id)->fetch();

			$this->tpl = tpl('products/features.latte', array(
				'featureset' => $fset,
				'values' => $values
					));
		}
	}

	public function delete($id)
	{
		gatekeeper('products-edit');

		if (Core::$api->product->delete((int) $id)) {
			flashMsg(__('msg_deleted'));
		}

		redirect('admin/products');
	}

	public function copy($id)
	{
		if(!Core::canAddNewProduct()){
			flashMsg(__('msg_cant_add_new_product'), 'error');
			redirect('admin/products');
		}

		gatekeeper('products-edit');

		$product = Core::$db->product[(int) $id];

		if ($product) {
			$product_data = $product->as_array();

			unset($product_data['id']);
			$product_data['status'] = 0;
			$product_data['name'] = 'Copy - ' . $product_data['name'];

			$product_id = Core::$db->product($product_data);

			$data_tbls = array('files', 'pages', 'attributes');

			foreach ($data_tbls as $tbl) {
				$tbl = 'product_' . $tbl;

				$q = $product->$tbl();

				if ($tbl == 'attributes') {
					$q->order('id ASC');
				}

				foreach ($q as $rec) {
					$rec = $rec->as_array();

					unset($rec['id']);
					$rec['product_id'] = $product_id;

					Core::$db->$tbl($rec);
				}
			}

			if ($product['featureset_id']) {
				$features_tbl = 'features_' . $product['featureset_id'];

				if ($row = Core::$db->$features_tbl()->where('product_id', $product['id'])->limit(1)->fetch()) {
					$row = $row->as_array();
					$row['product_id'] = $product_id;

					Core::$db->$features_tbl($row);
				}
			}

			redirect('admin/products/edit/' . $product_id);
		}

		redirect('admin/products');
	}

	public function xml_feeds()
	{
		gatekeeper('products-xml_feeds');

		$services = array();

		if ($f = glob(APPROOT . 'template/system/xml_feeds/*.php')) {
			foreach ($f as $s) {
				$tpl = substr($s, strrpos($s, '/') + 1, -4);
				$services[$tpl] = preg_replace('/_/', '.', $tpl);
			}
		}

		$this->content = tpl('products/xmlfeeds.latte', array(
			'services' => $services,
			'key' => Core::config('access_key')
				));
	}

	private function _pageTree($selected, $parent_id = 0, $level = 1)
	{
		$out = '';

		if ($level == 1) {

			foreach (Core::$def['page_types'] as $type_id => $type) {
				$pages = Core::$db->page()->where('parent_page', $parent_id)->where('menu', $type_id)->order('position ASC, name ASC');

				if (count($pages)) {
					$out .= '<option disabled="disabled">' . $type . '</option>';

					foreach ($pages as $page) {
						$out .= '<option value="' . $page['id'] . '"' . (in_array($page['id'], $selected) ? ' selected="selected"' : '') . ' class="l' . $level . '">' . str_repeat('&nbsp;&nbsp;&nbsp;', $level) . $page['name'] . '</option>';

						$out .= $this->_pageTree($selected, $page['id'], $level + 1);
					}
				}
			}
		} else {
			foreach (Core::$db->page()->where('parent_page', $parent_id) as $page) {
				$out .= '<option value="' . $page['id'] . '"' . (in_array($page['id'], $selected) ? ' selected="selected"' : '') . ' class="l' . $level . '">' . str_repeat('&nbsp;&nbsp;&nbsp;', $level) . $page['name'] . '</option>';

				$out .= $this->_pageTree($selected, $page['id'], $level + 1);
			}
		}

		return $out;
	}

	private function _pageTreeCheckboxes($selected, $parent_id = 0, $level = 1, $renderUl = false)
	{
		$out = '';

		if ($level == 1) {

			foreach (Core::$def['page_types'] as $type_id => $type) {
				$pages = Core::$db->page()->where('parent_page', $parent_id)->where('menu', $type_id)->order('position ASC, name ASC');

				if (count($pages)) {
					$out .= '<li class="check-item title"><strong>' . $type . '</strong><ul>';

					$i = 1;
					foreach ($pages as $page) {
						$customClass = "";
						if(count($pages) == $i) {
							$customClass = " last";
						}
						$out .= '
						<li class="check-item'. $customClass .'">
							<label>' . /*str_repeat('<span class="level">--</span>', $level) . */'
								<input type="checkbox" name="pages[]" value="' . $page['id'] . '"' . (in_array($page['id'], $selected) ? ' checked="checked"' : '') . ' class="l' . $level . '">
								<span>' . $page['name'] . '</span>
							</label>
							'. $this->_pageTreeCheckboxes($selected, $page['id'], $level + 1, true) .'
						</li>';
						$i++;
					}

					$out .= '</ul></li>';
				}
			}
		} else {
			$i = 1;
			$categories = Core::$db->page()->where('parent_page', $parent_id);
			foreach ($categories as $page) {
				if($i == 1){
					$out .= '<ul>';
				}
				$customClass = "";
				if(count($categories) == $i) {
					$customClass = " last";
				}
				$out .= '
					<li class="check-item'. $customClass .'">
						<label>' /*. str_repeat('<span class="level">--</span>', $level)*/ .
							'<input type="checkbox" name="pages[]" value="' . $page['id'] . '"' . (in_array($page['id'], $selected) ? ' checked="checked"' : '') . ' class="l' . $level . '">
							<span>' . $page['name'] . '</span>
						</label>
						'. $this->_pageTreeCheckboxes($selected, $page['id'], $level + 1, true) .'
					</li>';
				if($i == count($categories))
					$out .= '</ul>';
				$i++;
			}
		}

		return $out;
	}

	public function find_sidebar()
	{
		$this->tpl = tpl('iframe.latte');

		$expanded = array();

		if (!empty($_GET['p']) && ($page = Core::$db->page[(int) $_GET['p']])) {
			$parent_id = $page['parent_page'];
			$expanded[] = $page['id'];
			$_page = $page;

			while ($parent_id && $_page) {
				if ($_page = Core::$db->page[$parent_id]) {
					$parent_id = $_page['parent_page'];
					$expanded[] = $_page['id'];
				}
			}
		}

		$this->tpl->content = tpl('products/find/sidebar.latte', array('pages' => $this->find_getPages(), 'expanded' => $expanded));
	}

	private function getSubpages($page_id, & $ids = array())
	{
		foreach (Core::$db->page()->where('parent_page', $page_id)->where('status', 1) as $page) {
			$ids[] = $page['id'];

			$this->getSubpages($page['id'], $ids);
		}
	}

	public function find_main()
	{
		$this->tpl = tpl('iframe.latte');

		gatekeeper('products-edit');

		$order_by = 'name';
		$order_dir = 'ASC';

		if (!empty($_GET['sort'])) {
			$order_by = $_GET['sort'];
		}

		if (!empty($_GET['dir'])) {
			$order_dir = $_GET['dir'];
		}

		if (isSet($_GET['search'])) {
			$q = trim($_GET['search']);

			if (isSet($_GET['cancel_search'])) {
				redirect(url('admin/products/find_main', null, true, null));
			}

			if (!empty($q)) {
				$products = Search::findProducts($q);
			}

			if (!$products) {
				$products = Core::$db->product();
			}

			if (!empty($_GET['filter_what'])) {
				if ($_GET['filter_what']{0} != '_') {
					// param
					if ($fw = $this->buildFilterWhere($_GET['filter_what'], $_GET['filter_value'])) {
						$products->where($this->buildFilterWhere($_GET['filter_what'], $_GET['filter_value']));
					}
				} else {
					// no image/category
					if ($_GET['filter_what'] == '_no_image') {
						$products->where('id NOT IN (' . Core::$db->product_files()->where('is_image', 1)->select('product_id') . ')');
					} else if ($_GET['filter_what'] == '_no_category') {
						$products->where('id NOT IN (' . Core::$db->product_pages()->select('product_id') . ')');
					}
				}
			}
		} else {
			$products = Core::$db->product();
		}

		if (!empty($_GET['category'])) {
			$pages = array((int) $_GET['category']);

			$this->getSubpages($_GET['category'], $pages);

			$products->where('id', Core::$db->product_pages()->where('page_id', $pages)->select('product_id'));
		}

		if ($products) {
			$products->order($order_by . ' ' . $order_dir)->limit(pgLimit(), pgOffset());

			$total_count = Core::$db->product;

			if ($products->getWhere()) {
				$total_count->where($products->getWhere());
			}

			$total_count = $total_count->group("COUNT(*)");
			$total_count = (int) $total_count[0];
		} else {
			$products = array();
			$total_count = 0;
		}

		$this->tpl->content = tpl('products/find/main.latte', array(
			'products' => $products,
			'total_count' => $total_count,
			'order_by' => $order_by,
			'order_dir' => $order_dir,
				));
	}

	public function find()
	{
		$this->tpl = tpl('iframe.latte');

		$this->tpl->content = tpl('products/find/layout.latte');
	}

	public function find_getPages($parent_id = null)
	{
		$pages = Core::$db->page()->order('position ASC');

		if ($parent_id) {
			return $pages->where('parent_page', $parent_id);
		} else {
			return $pages->where('menu', 2);
		}
	}

	public function ajax_attr_template($template_id)
	{
		$this->tpl = null;

		$template_id = (int) $template_id;
		$status = false;
		$groups = array();

		if ($template_id && ($template = Core::$db->product_attribute_template[$template_id])) {
			foreach ($template->product_attribute_template_items()->order('id ASC') as $item) {

				if (!isset($groups[$item['name']])) {
					$groups[$item['name']] = array();
				}

				$groups[$item['name']][] = array(
					'value' => $item['value'],
					'price' => $item['price']
				);
			}

			$status = true;
		}

		echo json_encode(array('attr_groups' => $groups, 'status' => $status));
	}
}
