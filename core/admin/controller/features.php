<?php

defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */
class Controller_Features extends AdminController
{

	public function __construct()
	{
		parent::__construct();

		Core::$active_tab = 'products';
	}

	public function index()
	{
		$order_by = 'name';
		$order_dir = 'ASC';

		sortDir($order_by, $order_dir);

		$featuresets = Core::$db->featureset()->order($order_by . ' ' . $order_dir)->limit(pgLimit(), pgOffset());

		$this->content = tpl('features/list.latte', array(
			'order_by' => $order_by,
			'order_dir' => $order_dir,
			'featuresets' => $featuresets
				));
	}

	public function edit($id)
	{
		$featureset = Core::$db->featureset[(int) $id];

		if ($id > 0 && !$featureset) {
			flashMsg(__('msg_record_not_found', $id), 'error');
			redirect('admin/features');
		}

		if (!empty($_POST)) {
			if (($errors = validate(array('name'))) === true) {
				$data = $_POST;

				if (!$featureset) {
					$featureset_id = Core::$db->featureset(prepare_data('featureset', $data));

					if ($featureset_id) {
						$featureset = Core::$db->featureset[$featureset_id];
						$saved = true;
					}
				} else {
					$saved = (bool) $featureset->update(prepare_data('featureset', $data));
				}

				if ($featureset && $saved !== false) {

					foreach ($_POST['feature'] as $fid => $fdata) {
						if ($fid{0} != '0' && ($feature = Core::$db->feature[$fid])) {
							// update
							if (!trim($fdata['name'])) {
								$feature->delete();
							} else {
								$feature->update(array(
									'name' => $fdata['name'],
									'position' => (int) $fdata['position'],
									'unit' => $fdata['unit'] ? $fdata['unit'] : null,
									//'type' => $fdata['type'],
									'value' => $fdata['value'] ? $fdata['value'] : null,
									'filter' => (Core::$is_premium && isSet($fdata['filter'])) ? 1 : 0
								));
							}
						} else if ($fid{0} == '0' && trim($fdata['name'])) {
							// insert
							Core::$db->feature(array(
								'featureset_id' => $featureset['id'],
								'name' => $fdata['name'],
								'position' => (int) $fdata['position'],
								'unit' => $fdata['unit'] ? $fdata['unit'] : null,
								'type' => $fdata['type'],
								'value' => $fdata['value'] ? $fdata['value'] : null,
								'filter' => (Core::$is_premium && isSet($fdata['filter'])) ? 1 : 0
							));
						}
					}

					$this->_createTable($featureset);

					flashMsg(__('msg_saved'));
				} else {
					flashMsg(__('msg_error'), 'error');
				}

				if (isSet($_POST['save_go'])) {
					redirect('admin/features');
				} else {
					redirect('admin/features/edit/' . $featureset['id']);
				}
			}
		} else if ($id == 0) {
			$featureset['id'] = 0;
			$featureset['name'] = __('new_feature');
		}

		$this->content = tpl('features/edit.latte', array(
			'featureset' => $featureset
				));
	}

	public function delete($id)
	{
		$this->tpl = null;

		$featureset = Core::$db->featureset[(int) $id];

		if ($featureset) {
			$tbl_name = 'features_' . $id;

			$featureset->feature()->delete();
			$featureset->delete();

			Core::$db->product()->where('featureset_id', $id)->update(array('featureset_id' => null));

			Core::$db_inst->schema->dropTable($tbl_name);
		}

		redirect('admin/features');
	}

	public function _createTable($featureset)
	{
		$tbl_name = 'features_' . $featureset['id'];
		$cols = array(
			'product_id' => array(
				'type' => 'integer',
				'primary' => true
			)
		);

		foreach ($featureset->feature() as $feature) {

			if ($feature['type'] == 'string') {
				$s = array(
					'type' => 'varchar',
					'index' => $feature['filter'],
					'index_length' => 10
				);
			} else if ($feature['type'] == 'text') {
				$s = array(
					'type' => 'text'
				);
			} else if ($feature['type'] == 'number') {
				$s = array(
					'type' => 'float',
					'index' => $feature['filter']
				);
			} else if ($feature['type'] == 'yesno') {
				$s = array(
					'type' => 'integer',
					'length' => 1,
					'not_null' => true,
					'index' => $feature['filter'],
					'default' => 0
				);
			} else if ($feature['type'] == 'select') {
				$s = array(
					'type' => 'varchar',
					'index' => $feature['filter'],
					'index_length' => 10
				);
			}

			$cols['f' . $feature['id']] = $s;
		}

		if ($fields = Core::$db_inst->tableFields($tbl_name)) {
			// add cols
			Core::$db_inst->schema->addColumns($tbl_name, $cols);

			foreach ($fields as $col_name => $col_params) {
				if ($col_name{0} == 'f' && !isSet($cols[$col_name])) {
					Core::$db_inst->schema->dropColumn($tbl_name, $col_name);
				}
			}
		} else {
			// create table
			Core::$db_inst->schema->createTable($tbl_name, $cols);
		}
	}

}