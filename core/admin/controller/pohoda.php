<?php

defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */
class Controller_Pohoda extends AdminController
{

	protected $log;
	protected $timestamp;
	protected $tmp_file;
	protected $encoding = 'UTF8';

	public function __construct()
	{
		$this->tpl = null;

		if (!empty($_GET['k']) && strlen(Core::config('sync_key')) >= 6 && $_GET['k'] == Core::config('sync_key')) {

			if (!preg_match('/STORMWARE StwXML/i', $_SERVER['HTTP_USER_AGENT'])) {
				header('Status: 401 Unauthorized');
				die('Access denied.');
			}

			if (!Core::$is_premium) {
				die("You need premium licence");
			}

			parent::__construct(true);
		} else {
			redirect('admin');
		}

		header('Content-Type: text/xml');

		$this->timestamp = time();
		$this->log('Client IP: ' . $_SERVER['REMOTE_ADDR']);

		if ($enc = Registry::get('pohoda_encoding')) {
			$this->encoding = $enc;
		}

		register_shutdown_function(array($this, '_shutdown_handler'));
	}

	public function import($what)
	{
		if ($what == 'stock') {
			$this->_import_stock();
		} else if ($what == 'categories') {
			$this->_import_categories();
		}
	}

	public function export($what)
	{
		if ($what == 'orders') {
			$this->_export_orders();
		} else if ($what == 'addressbook') {
			$this->_export_addressbook();
		}
	}

	private function log($msg)
	{
		$this->log .= date('H:i:s') . "\t" . $msg . "\n";
	}

	private function _import_categories()
	{
		$this->log('Starting import: CATEGORIES');

		$config = Pohoda::config();

		try {
			$xml = new Pohoda($this->_saveInput());
		} catch (Exception $e) {
			$this->log('ERROR: ' . $e->getMessage());
			exit;
		}

		if (!$xml) {
			$this->log('ERROR: Invalid XML import');
			exit;
		}

		$this->_import_subcategories(0, true, $xml);

		$this->log('Finishing import');

		$xml->response();
		exit;
	}

	private function _import_subcategories($parent_id, $element, $xml)
	{
		if ($element === true) {
			$categories = $xml->xpath('//lst:categoryDetail/ctg:category');
		} else {
			$categories = $element->xpath('ctg:subCategories/ctg:category');
		}

		foreach ($categories as $category) {
			$pohoda_id = (int) $xml->val($category->xpath('ctg:id'));
			$category_name = $xml->val($category->xpath('ctg:name'));

			if (!$pohoda_id) {
				continue;
			}

			$data = array(
				'name' => $category_name,
				'status' => (int) ($xml->val($category->xpath('ctg:displayed')) == 'true'),
				'position' => (int) $xml->val($category->xpath('ctg:sequence')),
				'parent_page' => $parent_id,
				'description_short' => $xml->val($category->xpath('ctg:description')),
			);

			if ($page = Core::$db->page()->where('sync_id', $pohoda_id)->fetch()) {
				// update

				if ($page->needUpdate($data)) {
					$page->update($data);

					$this->log('Updating category ' . $page['id'] . ' - ' . $category_name);
				}

				if (count($category->xpath('ctg:subCategories/ctg:category'))) {
					$this->_import_subcategories($page['id'], $category, $xml);
				}
			} else {
				// insert

				$data = array_merge($data, array(
					'sef_url' => dirify($category_name),
					'menu' => $parent_id ? 0 : 2,
					'products' => 1,
					'rss' => 0,
					'product_columns' => 2,
					'authorization' => 0,
					'sync_id' => $pohoda_id
						));

				$page_id = Core::$db->page($data);

				$this->log('Inserting category ' . $page_id . ' - ' . $category_name);

				if (count($category->xpath('ctg:subCategories/ctg:category'))) {
					$this->_import_subcategories($page_id, $category, $xml);
				}
			}
		}
	}

	private function _import_stock()
	{
		$this->log('Starting import: STOCK');

		$config = Pohoda::config();

		try {
			$xml = new Pohoda($this->_saveInput());
		} catch (Exception $e) {
			$this->log('ERROR: ' . $e->getMessage());
			exit;
		}

		if (!$xml) {
			$this->log('ERROR: Invalid XML import');
			exit;
		}

		$best_availability = Core::$db->availability()->where('days >= 0')->order('days ASC')->limit(1)->fetch();

		$priceID = (int) $config['stock_price_id'];

		foreach ($xml->xpath('//lst:stock/stk:stockHeader') as $product) {
			$stock = current($product->xpath('..'));

			$ean = $xml->val($product->xpath('stk:EAN'));
			$sku = $xml->val($product->xpath('stk:code'));
			$is_internet = ($xml->val($product->xpath('stk:isInternet')) == 'true');

			if ($config['stock_internet_only'] && !$is_internet) {
				continue;
			}

			if ($ean || $sku) {
				$_products = Core::$db->product()->where($ean ? 'ean13' : 'sku', $ean ? $ean : $sku);

				if (!$config['stock_price_only']) {
					$_products->limit(1);
				}

				$data = array(
					'ean13' => $ean,
					'sku' => $sku,
					'stock' => (int) $xml->val($product->xpath('stk:count')) - (int) $xml->val($product->xpath('stk:countReceivedOrders')) - (int) $xml->val($product->xpath('stk:reservation'))
				);

				if (is_numeric($data['stock']) && $data['stock'] <= 0) {
					if ($availability = (int) Core::config('stock_out_availability')) {
						$data['availability_id'] = $availability;
					}
				}

				if ($best_availability && !$data['availability_id'] && is_numeric($data['stock']) && $data['stock'] > 0) {
					$data['availability_id'] = $best_availability['id'];
				}

				if ($weight = (float) $xml->val($product->xpath('stk:mass'))) {
					$data['weight'] = $weight;
				}

				if (!$config['stock_price_only']) {
					if ($v = $xml->val($product->xpath('stk:name'))) {
						$data['name'] = $v;
					}

					if ($v = $xml->val($product->xpath('stk:description'))) {
						$data['description_short'] = $v;
					}

					if ($v = $xml->val($product->xpath('stk:description2'))) {
						$data['description'] = $v;
					}

					if ($v = $xml->val($product->xpath('stk:guarantee'))) {
						$unit = 'months';

						if ($xml->val($product->xpath('stk:guaranteeType')) == 'year') {
							$unit = 'years';
						}

						$data['guarantee'] = $v . ' ' . __($unit);
					}
				}

				$count_categories = count($product->xpath('stk:categories/stk:idCategory'));

				if ($config['stock_categories'] && ($category = $xml->val($product->xpath('stk:storage/typ:ids')))) {
					$cats = explode('/', $category);

					if (count($cats) && trim($cats[0])) {
						$data['categories'] = array($cats);
					}
				}

				if ($producer = trim($xml->val($product->xpath('stk:producer')))) {
					$data['manufacturers'] = array(array($producer));
				}

				$saved_ids = array();

				if (count($_products)) {
					// update

					foreach ($_products as $db_product) {
						$data['id'] = $db_product['id'];

						if ($priceID) {
							foreach ($stock->xpath('stk:stockPriceItem/stk:stockPrice') as $p) {
								$pid = (int) $xml->val($p->xpath('typ:id'));

								if ($pid && $pid == $priceID) {
									$data['price'] = price_unvat($xml->val($p->xpath('typ:price')), $db_product['vat'])->price;
									break;
								}
							}
						} else {
							$data['price'] = price_unvat($xml->val($product->xpath('stk:sellingPrice')), $db_product['vat'])->price;
						}

						if ($cost = (float) $xml->val($product->xpath('stk:purchasingPrice'))) {
							$data['cost'] = price_unvat($cost, $db_product['vat'])->price;
						}

						if ($db_product['lock_data']) {
							unset($data['name'], $data['description'], $data['description_short'], $data['sef_url'], $data['title'], $data['meta_description'], $data['meta_keywords']);
						}

						if ($db_product['lock_price']) {
							unset($data['price'], $data['cost'], $data['margin'], $data['vat_id'], $data['recycling_fee'], $data['copyright_fee']);
						}

						if ($db_product['lock_stock']) {
							unset($data['stock'], $data['availability_id']);
						}

						$saved_product = Core::$api->product->save($data);

						$saved_ids[] = $saved_product['id'];

						if ($db_product) {
							$this->log('Updating product ' . $db_product['id'] . ' - [' . ($ean ? $ean : $sku) . '] ' . $db_product['name']);
						} else {
							$this->log('Inserting product ' . $saved_product['id'] . ' - [' . ($ean ? $ean : $sku) . '] ' . $data['name']);
						}
					}
				} else if (!$config['stock_price_only'] && $config['stock_enable_insert']) {
					// insert

					if ($priceID) {
						foreach ($stock->xpath('stk:stockPriceItem/stk:stockPrice') as $p) {
							$pid = (int) $xml->val($p->xpath('typ:id'));

							if ($pid && $pid == $priceID) {
								$data['price'] = price_unvat($xml->val($p->xpath('typ:price')), VAT_DEFAULT)->price;
								break;
							}
						}
					} else {
						$data['price'] = price_unvat($xml->val($product->xpath('stk:sellingPrice')), VAT_DEFAULT)->price;
					}

					if ($cost = (float) $xml->val($product->xpath('stk:purchasingPrice'))) {
						$data['cost'] = price_unvat($cost, VAT_DEFAULT)->price;
					}

					$saved_product = Core::$api->product->save($data);

					$saved_ids[] = $saved_product['id'];

					if ($db_product) {
						$this->log('Updating product ' . $db_product['id'] . ' - [' . ($ean ? $ean : $sku) . '] ' . $db_product['name']);
					} else {
						$this->log('Inserting product ' . $saved_product['id'] . ' - [' . ($ean ? $ean : $sku) . '] ' . $data['name']);
					}
				}


				foreach ($saved_ids as $product_id) {
					if ($count_categories && !$config['stock_categories']) {
						foreach ($product->xpath('stk:categories/stk:idCategory') as $category_id) {
							if ($page = Core::$db->page()->where('sync_id', (int) $xml->val($category_id))->fetch()) {
								if (!Core::$db->product_pages()->where('product_id', $product_id)->where('page_id', $page['id'])->fetch()) {
									Core::$db->product_pages(array(
										'product_id' => $product_id,
										'page_id' => $page['id']
									));
								}
							}
						}
					}

					if ($config['stock_pictures']) {
						foreach ($product->xpath('stk:pictures/stk:picture') as $pic) {
							if ($filepath = $xml->val($pic->xpath('stk:filepath'))) {
								$_path = $config['stock_pictures_root'] . $filepath;

								if (!file_exists($_path)) {
									$this->log('Warning: File ' . $_path . ' not found');
									continue;
								}

								$fileinfo = pathinfo($_path);

								if (!($product_file = Core::$db->product_files()->where('product_id', $product_id)->where('filename', $_path)->limit(1)->fetch())) {
									// insert
									Core::$db->product_files(array(
										'product_id' => $product_id,
										'filename' => $_path,
										'description' => $xml->val($pic->xpath('stk:description')),
										'size' => -1,
										'align' => 2,
										'position' => 0,
										'is_image' => preg_match('/\.(jpe?g|png|gif)$/i', $filepath) ? 1 : 0
									));

									if (in_array(strtolower($fileinfo['extension']), array('gif', 'jpg', 'jpeg', 'png'))) {
										try {
											Image::thumbs(DOCROOT . $_path, (bool) $config['stock_pictures_watermark']);
										} catch (Exception $e) {
											$this->log('Error: ' . $e->getMessage() . ' (' . $_path . ')');
										}
									}
								}
							}
						}
					}
				}
			}
		}

		$this->log('Finishing import');

		$xml->response();
		exit;
	}

	private function _export_orders()
	{
		$this->log('Starting export: ORDERS');
		$date_from = null;
		$date_to = null;

		if ($xml = $this->_getInput()) {
			if ($date = $xml->val($xml->xpath('//lst:dateFrom'))) {
				$date_from = strtotime($date);
				$date_to = strtotime($xml->val($xml->xpath('//lst:dateTill')));
			}
		}

		$orders = Core::$db->order()->where('sync_time IS NULL')->order('id ASC');

		if ($date_from && $date_to) {
			$orders->where('received >= ' . $date_from . ' AND received <= ' . ($date_to + 86399));
		}

		$datapack = new Pohoda_Builder_Datapack();

		if (count($orders) == 0) {
			$datapack->addItem('0');
		}

		foreach ($orders as $order) {
			$item = new Pohoda_Builder_Order($datapack, $order);
			$this->log('Exporting order ' . $order['id']);

			$order->update(array(
				'sync_time' => $this->timestamp
			));
		}

		$this->log('Finishing export');

		echo $datapack->build();

		exit;
	}

	private function _export_addressbook()
	{
		$this->log('Starting export: ADDRESSBOOK');
		$date_from = null;
		$date_to = null;

		$customers = Core::$db->customer()->order('id ASC');

		$datapack = new Pohoda_Builder_Datapack();

		$ids = array();

		foreach ($customers as $customer) {
			$ids[] = $customer['id'];

			$item = new Pohoda_Builder_Address($datapack, $customer);

			$this->log('Exporting address ' . $customer['id']);
		}

		if (count($ids)) {
			$orders = Core::$db->order()->where('customer_id IS NULL OR customer_id NOT IN(' . join(',', $ids) . ')')->order('id ASC');

			foreach ($orders as $order) {
				$order['id'] = 'Order.' . $order['id'];

				$item = new Pohoda_Builder_Address($datapack, $order);
				$this->log('Exporting address ' . $order['id']);
			}
		}

		if (count($orders) == 0 && count($customers) == 0) {
			$datapack->addItem('0');
		}

		$this->log('Finishing export');

		echo $datapack->build();

		exit;
	}

	private function _saveInput()
	{
		$this->tmp_file = DOCROOT . 'etc/tmp/tmpfile_' . uniqid();

		$c = file_get_contents('php://input');

		if ($this->encoding == 'UTF8') {
			$c = iconv('cp1250', 'utf-8', $c);
		}

		if (file_put_contents($this->tmp_file, $c)) {
			return $this->tmp_file;
		}

		return false;
	}

	private function _getInput()
	{
		if ($inp = file_get_contents('php://input')) {

			if (!@simplexml_load_file($inp)) {
				if ($this->encoding == 'UTF8') {
					$inp = iconv('cp1250', 'utf-8', $inp);
				}
			}

			$xml = new Pohoda($inp, true);

			if ($xml) {
				return $xml;
			}
		}

		return false;
	}

	private function _shutdown_handler()
	{
		if (!is_dir(DOCROOT . 'etc/log')) {
			FS::mkdir(DOCROOT . 'etc/log', true);
		}

		@file_put_contents(DOCROOT . 'etc/log/pohoda_' . $this->timestamp . '_' . date('Y-m-d', $this->timestamp) . '.txt', $this->log . "\n\n", FILE_APPEND);
	}

}