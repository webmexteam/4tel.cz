<?php

defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */
class Controller_Dataexport extends AdminController
{

	public function __construct()
	{
		parent::__construct();

		gatekeeper('products-export_import');

		Core::$active_tab = 'products';
	}

	public function index()
	{
		$fields = array(
			'name' => __('name'),
			'nameext' => __('nameext'),
			'status' => __('status'),
			'position' => __('position'),
			'description_short' => __('description_short'),
			'description' => __('description'),
			'price' => __('price'),
			'price_old' => __('price_old'),
			'vat' => __('vat'),
			'stock' => __('stock'),
			'sku' => __('sku'),
			'ean13' => __('ean13'),
			'productno' => __('productno'),
			'availability' => __('availability'),
			'availability_days' => __('availability_days'),
			'guarantee' => __('guarantee'),
			'recycling_fee' => __('recycling_fee'),
			'copyright_fee' => __('copyright_fee'),
			'weight' => __('weight'),
			'cost' => __('cost'),
			'margin' => __('margin'),
			'categories' => __('categories'),
			'manufacturers' => __('manufacturer'),
			'files' => __('files'),
			'attributes' => __('attributes'),
            'features' => __('features'),
			'name_feed' => __('name_feed'),
			'description_fedd' => __('description_feed'),
		);

		$checked = array('id', 'sku', 'name', 'nameext', 'price', 'stock', 'availability_days');

		if (!empty($_COOKIE['dataexport_cols'])) {
			$checked = (array) explode(';', $_COOKIE['dataexport_cols']);
		}

		$this->content = tpl('dataexport/index.latte', array(
			'fields' => $fields,
			'checked' => $checked,
			'categories' => '<option value="">' . __('all') . '</option>' . $this->_pageTree(array((int) $_GET['category']))
				));
	}

	public function pricelist()
	{
		$this->tpl = null;
		@set_time_limit(0);

		$columns = array(
			'sku' => __('sku'),
			'ean13' => __('ean'),
			'name' => __('name'),
			'price' => __(PRICE_NAME) . ' ' . Core::config('currency'),
		);

		$groups = array();

		if (Core::$is_premium) {
			$groups = Core::$db->customer_group();

			if (count($groups)) {
				foreach ($groups as $group) {
					$columns['price_group_' . $group['id']] = __('price_group') . ': ' . $group['name'];
				}
			}
		}

		$export = new Export('ods');

		$export->writer->push_id = false;

		$export->addHeader(array_values($columns));

		$offset = 0;

		while (($products = Core::$db->product()->order('id ASC')->limit(500, $offset)) && count($products)) {
			foreach ($products as $product) {
				foreach ($groups as $group) {
					$product['price_group_' . $group['id']] = $product['price'] * (float) $group['coefficient'];
				}

				$export->addProduct($product, array_keys($columns));
			}

			$offset += 500;
		}

		$name = 'pricelist_' . date('Y-m-d_H:i');

		header("Pragma: public"); // required
		header("Expires: 0");
		header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
		header("Cache-Control: private", false); // required for certain browsers
		header("Content-Type: application/vnd.oasis.opendocument.spreadsheet; charset=UTF-8");
		header("Content-Disposition: attachment; filename=\"" . dirify($name) . ".ods\";");
		header("Content-Transfer-Encoding: binary");

		echo $export->save();
	}

	public function download()
	{
		$this->tpl = null;

		@set_time_limit(0);

		$type = 'xml';
		$category = 0;

		if (!empty($_POST['type'])) {
			$type = trim($_POST['type']);
		}

		if (!empty($_POST['category'])) {
			$category = (int) trim($_POST['category']);
		}

		if ($type) {
			$export = new Export($type);

			$columns = isset($_POST['cols']) ? (array) $_POST['cols'] : null;

			if ($columns) {
				setcookie('dataexport_cols', join(';', $columns), strtotime('+14 days'));
			}

			if ($columns) {
				$export->addHeader(array_values($columns));
			}

			$offset = 0;

			$products = Core::$db->product();

			if ($category > 0) {
				if ($page = Core::$db->page[$category]) {
					$this->getSubpages($page['id'], $ids);

					$products->where('id IN (' . Core::$db->product_pages()->where('page_id', $ids)->select('product_id') . ') OR id IN (' . $page->product_pages()->select('product_id') . ')');
				}
			}

			$where = $products->getWhere();

			while (($_products = $products->order('id ASC')->limit(500, $offset)) && count($_products)) {
				foreach ($_products as $product) {
					$export->addProduct($product, $columns);
				}

				$offset += 500;

				$products = Core::$db->product();

				if ($where) {
					$products->where($where);
				}
			}

			$name = 'export_' . date('Y-m-d_H:i');

			header("Pragma: public"); // required
			header("Expires: 0");
			header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
			header("Cache-Control: private", false); // required for certain browsers

			if ($type == 'xml') {
				header("Content-Type: application/xml; charset=UTF-8");
			} else if ($type == 'xlsx') {
				header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet; charset=UTF-8");
			} else if ($type == 'csv') {
				header("Content-Type: text/x-csv; charset=Windows-1250");
			} else if ($type == 'ods') {
				header("Content-Type: application/vnd.oasis.opendocument.spreadsheet; charset=UTF-8");
			}

			if ($type == 'xlsx') {
				$type = 'xls';
			}

			header("Content-Disposition: attachment; filename=\"" . dirify($name) . "." . $type . "\";");
			header("Content-Transfer-Encoding: binary");

			echo $export->save();
		} else {
			redirect('admin/dataexport');
		}
	}

	private function _pageTree($selected, $parent_id = 0, $level = 1)
	{
		$out = '';

		if ($level == 1) {

			foreach (Core::$def['page_types'] as $type_id => $type) {
				$pages = Core::$db->page()->where('parent_page', $parent_id)->where('menu', $type_id)->order('position ASC, name ASC');

				if (count($pages)) {
					$out .= '<option disabled="disabled">' . $type . '</option>';

					foreach ($pages as $page) {
						$out .= '<option value="' . $page['id'] . '"' . (in_array($page['id'], $selected) ? ' selected="selected"' : '') . ' class="l' . $level . '">' . str_repeat('&nbsp;&nbsp;&nbsp;', $level) . $page['name'] . '</option>';

						$out .= $this->_pageTree($selected, $page['id'], $level + 1);
					}
				}
			}
		} else {
			foreach (Core::$db->page()->where('parent_page', $parent_id) as $page) {
				$out .= '<option value="' . $page['id'] . '"' . (in_array($page['id'], $selected) ? ' selected="selected"' : '') . ' class="l' . $level . '">' . str_repeat('&nbsp;&nbsp;&nbsp;', $level) . $page['name'] . '</option>';

				$out .= $this->_pageTree($selected, $page['id'], $level + 1);
			}
		}

		return $out;
	}

	private function getSubpages($page_id, & $ids = array())
	{
		foreach (Core::$db->page()->where('parent_page', $page_id)->where('status', 1) as $page) {
			$ids[] = $page['id'];

			$this->getSubpages($page['id'], $ids);
		}
	}

}