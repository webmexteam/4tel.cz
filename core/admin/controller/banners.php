<?php

defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */
class Controller_Banners extends AdminController
{

	public function __construct()
	{
		parent::__construct();

		gatekeeper('banners');

		Core::$active_tab = 'banners';
	}

	public function index()
	{
		if (isSet($_POST['save']) && !empty($_POST['banner'])) {
			foreach ($_POST['banner'] as $id => $val) {
				$banner = Core::$db->banner[(int) $id];

				if(isset($_POST['status'][$id])) {
					$status = 1;
				}else{
					$status = 0;
				}

				if ($banner) {
					$banner->update(array('status' => (int) $status));
				}
			}

			flashMsg(__('msg_saved'));
			redirect('admin/banners');
		}


		$this->content = tpl('banners/list.latte', array(
			'banners' => Core::$db->banner()
		));
	}

	public function edit($id)
	{
		$banner = Core::$db->banner[(int) $id];

		if ($id > 0 && !$banner) {
			flashMsg(__('msg_record_not_found', $id), 'error');
			redirect('admin/banners');
		}

		if (!empty($_POST)) {
			if (($errors = validate(array('name'))) === true) {
				$data = $_POST;

				$data['link_new_window'] = isset($_POST['link_new_window']) ? 1:0;

				if (!$banner) {
					$banner_id = Core::$db->banner(prepare_data('banner', $data));

					if ($banner_id) {
						$banner = Core::$db->banner[$banner_id];
						$saved = true;
					}
				} else {
					$saved = (bool) $banner->update(prepare_data('banner', $data));
				}

				if ($banner && $saved !== false) {
					flashMsg(__('msg_saved'));
				} else {
					flashMsg(__('msg_error'), 'error');
				}

				if (isSet($_POST['save_go'])) {
					redirect('admin/banners');
				} else {
					redirect('admin/banners/edit/' . $banner['id']);
				}
			}
		} else if ($id == 0) {
			$banner['id'] = 0;
			$banner['name'] = __('new_banner');
		}

		$this->content = tpl('banners/edit.latte', array(
			'banner' => $banner
		));
	}
}