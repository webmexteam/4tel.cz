<?php

defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */
class Controller_Dataimport extends AdminController
{

	public $fields = array();
	public $schema_dir = 'etc/xml_import/';
	public $is_cli = false;
	public $is_ajax = false;
	public $ajax_total = 0, $ajax_offset = 0;
	public $id_map = array();
	public $last_product_id = 0;
	public $current_schema, $run_id;
	protected $log = '';

	public function __construct()
	{
		$this->fields = Import::$fields;

		// if (defined('CRON') || (preg_match('/^dataimport\/cron\//i', Core::$uri) && !empty($_GET['k']) && $_GET['k'] == Core::config('access_key'))) {
		if (defined('CRON') || (preg_match('/^core\/cron\//i', Core::$uri) && !empty($_GET['k']) && $_GET['k'] == Core::config('access_key'))) {
			parent::__construct(true);
			$this->is_cli = true;
		} else {
			parent::__construct();
			gatekeeper('products-export_import');
		}

		Core::$active_tab = 'products';
	}

	public function __destruct()
	{
		if ($this->log) {
			if (!is_dir(DOCROOT . 'etc/log')) {
				FS::mkdir(DOCROOT . 'etc/log', true);
			}

			@file_put_contents(DOCROOT . 'etc/log/dataimport_' . date('Y-m-d') . '.txt', $this->log . "\n\n", FILE_APPEND);
			$this->log = "";
		}
	}

	public function index()
	{
		$imports = array();

		if ($f = glob(DOCROOT . 'etc/xml_import/*.php')) {
			foreach ($f as $file) {
				$info = pathinfo($file);

				$imports[$info['filename']] = $info['filename'];
			}
		}

		if (isset($_POST['upload_import']) && !empty($_FILES['import_file']) && !$_FILES['import_file']['error']) {
			$f = DOCROOT . 'etc/tmp/di_quick_' . uniqid();

			move_uploaded_file($_FILES['import_file']['tmp_name'], $f);

			$_SESSION['dataimport_quick'] = $f;

			redirect('admin/dataimport/import');
		}

		if (isset($_POST['edit']) && !empty($_POST['schema'])) {
			redirect('admin/dataimport/setup/' . $_POST['schema']);
		}

		if (isset($_POST['do_import']) && !empty($_POST['schema'])) {
			redirect('admin/dataimport/import/' . $_POST['schema']);
		}

		$saved_schema = null;

		if (!empty($_COOKIE['dataimport_schema'])) {
			$saved_schema = $_COOKIE['dataimport_schema'];
		}

		$this->content = tpl('dataimport/index.latte', array(
			'imports' => $imports,
			'key' => Core::config('access_key'),
			'saved_schema' => $saved_schema
				));
	}

	public function create()
	{
		unset($_SESSION['dataimport']);

		if (!empty($_POST['file'])) {
			$file = trim($_POST['file']);
			$error = false;

			if (!$file) {
				$error = __('dataimport_file_path_empty');
			}

			if (!$error) {
				$_SESSION['dataimport'] = array('file' => $file);
				redirect('admin/dataimport/setup');
			}
		}

		$this->content = tpl('dataimport/create.latte', array('error' => $error));
	}

	public function setup($schema = null)
	{
		$fields = array_merge(array(
			'' => '&mdash;',
				), $this->fields);

		$import = null;

		if ($schema) {
			if (empty($_SESSION['dataimport']['edit_schema']) || $_SESSION['dataimport']['edit_schema'] != $schema) {
				unset($_SESSION['dataimport']);
			}

			$import = new Import($schema);

			if ($import) {
				$_SESSION['dataimport'] = array(
					'edit_schema' => $schema,
					'file' => $import->options['datafile'],
					'elements' => $import->options['elements'],
					'options' => $import->options
				);
			}
		}

		if (!empty($_POST) && !empty($_SESSION['dataimport'])) {

			$options = array();

			if ($import) {
				$options = $import->options;
			}

			$file = $_SESSION['dataimport']['file'];

			if (!$import) {

				if ($_SESSION['dataimport']['edit_schema']) {
					$import = new Import($_SESSION['dataimport']['edit_schema']);
				} else {
					$import = new Import(null, $file);
				}
			}

			if ($_SESSION['dataimport']['edit_schema']) {
				$schema_name = $_SESSION['dataimport']['edit_schema'];
				$_SESSION['dataimport']['schema_name'] = $schema_name;
			} else {
				$schema_name = null;

				if (preg_match('/^(http|ftp)s?\:\/\/([^\/]+)/', $file, $m) && $m) {
					$schema_name = preg_replace('/^www\./', '', $m[2]);
				} else {
					$info = pathinfo($file);
					$schema_name = $info['filename'];
				}

				$schema_name = dirify($schema_name);

				if (file_exists(DOCROOT . $this->schema_dir . $schema_name . '.php')) {
					$schema_name .= '_' . uniqid();
				}

				$_SESSION['dataimport']['schema_name'] = $schema_name;
			}

			$_SESSION['saved_schema_name'] = $_SESSION['dataimport']['schema_name'];

			$options = array_merge($options, array(
				'datafile' => $file,
				'datatype' => $options['datatype'] ? $options['datatype'] : $import->getDatatype(),
				'product_element' => !empty($_POST['product_element']) ? $_POST['product_element'] : null,
					));

			if (!empty($_POST['options']) && is_array($_POST['options'])) {
				foreach ($_POST['options'] as $name => $value) {
					$options[$name] = $value;
				}
			}

			$options['elements'] = array();

			foreach ($_POST['custom_element'] as $i => $el) {
				if (isset($_POST['custom_element_value'][$i]) && $el) {
					$value = $_POST['custom_element_value'][$i];

					if (is_array($value)) {
						$key = key($value);

						if ($key == '_static') {
							$value = 'static(' . current($value) . ')';
						} else if ($key == '_function') {
							$value = 'function(' . current($value) . ')';
						} else if ($key == '_expression') {
							$value = 'expression(' . current($value) . ')';
						}
					}

					$options['elements'][$el] = $value;
				}
			}

			$_SESSION['dataimport']['options'] = $options;

			redirect('admin/dataimport/save');
		}

		$this->content = tpl('dataimport/setup.latte', array(
			'fields' => $fields,
			'edit_schema' => $_SESSION['dataimport']['edit_schema'] ? 1 : 0,
			'options' => $import ? $import->options : array()
				));
	}

	public function save()
	{
		$this->tpl = null;

		if (empty($_SESSION['dataimport']['schema_name'])) {
			redirect('admin/dataimport');
		}

		$schema_file = DOCROOT . $this->schema_dir . $_SESSION['dataimport']['schema_name'] . '.php';

		$this->saveSchema($schema_file, $_SESSION['dataimport']['options']);

		redirect('admin/dataimport');
	}

	public function import($schema = null)
	{
		if ($schema) {
			$schema_file = DOCROOT . $this->schema_dir . $schema . '.php';

			if (!$schema || !file_exists($schema_file)) {
				redirect('admin/dataimport');
			}

			setcookie('dataimport_schema', $schema, strtotime('+14 days'), Core::$base);

			include($schema_file);
		} else if ($_SESSION['dataimport_quick']) {
			$schema = '__quick';
		} else {
			redirect('admin/dataimport');
		}

		$_SESSION['dataimport'] = array(
			'file' => $import_options['datafile'],
			'edit_schema' => $schema
		);

		$this->content = tpl('dataimport/import.latte', array(
			'schema' => $schema
				));
	}

	public function cron($schema)
	{
		$this->tpl = null;

		if (!Core::$is_premium) {
			$this->log('You need a premium version to run import');
			return;
		}

		$current_offset = $total_count = 0;

		try {
			$result = $this->doImport($schema, $total_count, $current_offset);

			$this->log(__('dataimport_current_state', $current_offset, $total_count));

			if ($result === -1) {
				$this->log(__('dataimport_proceed_next_step'));
			} else if ($result === true) {
				$this->log(__('dataimport_finished'));
			}
		} catch (Exception $e) {
			$this->log('Error: ' . $e->getMessage());
		}
	}

	public function ajax_download()
	{
		$this->tpl = null;

		@set_time_limit(0);

		$error = null;
		$status = false;

		if (!empty($_SESSION['dataimport']['file']) || !empty($_SESSION['dataimport']['edit_schema'])) {
			$file = $_SESSION['dataimport']['file'];

			try {
				if ($_SESSION['dataimport_quick'] && $_SESSION['dataimport']['edit_schema'] == '__quick') {
					$import = new Import(null, $_SESSION['dataimport_quick']);
				} else if ($_SESSION['dataimport']['edit_schema']) {
					$import = new Import($_SESSION['dataimport']['edit_schema']);
				} else {
					$import = new Import(null, $file);
				}

				$import->download(1);
				$status = true;

				if ($import->tmpfile) {
					$_SESSION['dataimport']['tmpfile'] = $import->tmpfile;
				}
			} catch (Exception $e) {
				$error = $e->getMessage();
			}
		}

		echo json_encode(array('status' => $status, 'error' => (string) $error));
	}

	public function ajax_analyze()
	{
		$this->tpl = null;

		$status = false;
		$error = $data = $result = null;

		if (!empty($_SESSION['dataimport']['file']) || !empty($_SESSION['dataimport']['edit_schema'])) {
			$file = $_SESSION['dataimport']['file'];

			try {
				if ($_SESSION['dataimport_quick'] && $_SESSION['dataimport']['edit_schema'] == '__quick') {
					$import = new Import(null, $_SESSION['dataimport_quick']);
				} else if ($_SESSION['dataimport']['edit_schema']) {
					$import = new Import($_SESSION['dataimport']['edit_schema']);

					$_SESSION['dataimport']['options'] = $import->options;
				} else {
					$import = new Import(null, $file);
				}

				$result = $import->analyze();

				if ($result) {
					$status = true;
					$data = $result;

					$_SESSION['dataimport']['type'] = $result['datatype'];
				} else {
					$error = __('dataimport_invalid_file');
				}
			} catch (Exception $e) {
				$error = $e->getMessage();
			}
		}

		echo json_encode(array('status' => $status, 'error' => (string) $error, 'data' => $data));
	}

	public function ajax_preview()
	{
		$this->tpl = null;

		$status = $schema_class = false;
		$error = $data = $schema_file = null;

		$import_options = $options = array();

		if (!empty($_SESSION['dataimport']['file']) || !empty($_SESSION['dataimport']['edit_schema'])) {
			$file = $_SESSION['dataimport']['file'];

			if ($_SESSION['dataimport_quick'] && $_SESSION['dataimport']['edit_schema'] == '__quick') {
				$import = new Import(null, $_SESSION['dataimport_quick']);

				$opt = $import->analyze();

				$import->options = array(
					'product_element' => $opt['product_element'],
					'elements' => array(),
					'mode' => 0,
					'create_categories' => 1,
					'product_unique' => $opt['unique'],
					'random' => null
				);

				foreach ($opt['elements'] as $i => $el) {
					if (isset($this->fields[$el])) {
						$import->options['elements'][$el] = $i;
					}
				}
			} else if ($_SESSION['dataimport']['edit_schema']) {
				$import = new Import($_SESSION['dataimport']['edit_schema']);
			} else {
				$import = new Import(null, $file);
			}

			if (!empty($_SESSION['dataimport']['edit_schema'])) {
				$options = array_merge(array(
					'product_element' => null,
					'elements' => array(),
					'options' => array(),
					'random' => null
						), (array) $import->options);
			}

			$options = array_merge($options, $_POST);

			if ($options['options']['encoding']) {
				$import->encoding = $options['options']['encoding'];
			}

			if ($random = $_SESSION['dataimport']['randomitem']) {
				$options['random'] = (int) $random;
			}

			if (!empty($_POST['random_product']) && $_POST['random_product']) {
				$options['random'] = true;
			}

			if (is_numeric($options['random'])) {
				$_SESSION['dataimport']['randomitem'] = $options['random'];
			}

			try {
				$random = $options['random'];

				if ($data = $import->getSample($options, $random)) {
					$status = true;
				}

				if ($random) {
					$_SESSION['dataimport']['randomitem'] = $random;
				}
			} catch (Exception $e) {
				$error = $e->getMessage();
			}
		}

		echo json_encode(array('status' => $status, 'error' => (string) $error, 'data' => $data));
	}

	public function ajax_import($schema)
	{
		$this->tpl = null;
		$this->is_ajax = true;

		echo str_pad(' ', 512);
		ob_end_flush();

		if (!Core::$is_premium) {
			$this->logAjax('You need a premium version to run import');
			return;
		}

		if (empty($_SESSION['dataimport_ajax'])) {
			$_SESSION['dataimport_ajax'] = array(
				'total' => 0,
				'offset' => 0
			);
		} else {
			$this->ajax_total = $_SESSION['dataimport_ajax']['total'];
			$this->ajax_offset = $_SESSION['dataimport_ajax']['offset'];
		}

		$current_offset = $total_count = 0;

		try {
			$result = $this->doImport($schema, $total_count, $current_offset);

			$_SESSION['dataimport_ajax']['total'] = $this->ajax_total = $total_count;

			$this->logAjax(__('dataimport_current_state', $current_offset, $total_count));

			if ($result === -1) {
				$this->logAjax(__('dataimport_proceed_next_step'), true);
			} else if ($result === true) {
				$this->logAjax(__('dataimport_finished'), false, true);
			}
		} catch (Exception $e) {
			$this->logAjax('Error: ' . $e->getMessage(), true);
		}

		$_SESSION['dataimport_ajax']['offset'] = $this->ajax_offset = $current_offset;

		if ($this->ajax_total <= $current_offset) {
			unset($_SESSION['dataimport_ajax']);
		}
	}

	private function logAjax($str, $nextStep = false, $finished = false)
	{
		$percent = @round(100 / $this->ajax_total * ($this->ajax_offset + 1), 1);

		$str = trim(htmlspecialchars($str));

		$data = array(
			'progress' => (float) $percent,
			'log' => $str
		);

		if ($nextStep) {
			$data['nextStep'] = 1;
		} else if ($finished) {
			$data['finished'] = 1;
		}

		$this->log .= $str . "\n";

		echo '<script>parent._progress(\'' . json_encode($data) . '\');</script>';
		flush();
	}

	private function log($str)
	{
		if ($this->is_ajax) {
			$this->logAjax($str);
		} else {
			$str = xliterate_utf8($str) . "\n";

			$this->log .= $str;

			echo $str;
		}
	}

	private function doImport($schema, & $total_count = 0, & $current_offset = 0)
	{
		$progress_lifetime = '1 hour';
		$product_fields = Core::$db_inst->tableFields('product');

		if (!Core::$is_premium) {
			return false;
		}

		$this->current_schema = $schema;

		WebmexAPI::$bulk_mode = true;

		$reader = new Import($schema);

		if (!$reader) {
			throw new Exception('Load datafile "' . $options['datafile'] . '" failed');
		}

		$total_count = $reader->count();

		$reader->reader->reset();

		$options = $reader->options;

		$schema_id = $reader->schema_id;
		$schema_class = false;

		$score = $this->getServerScore();
		$stack_limit = min(90, floor($score * 30));

		if (class_exists('Dataimport_schema', false)) {
			$schema_class = true;
		}

		@set_time_limit($this->is_cli ? 0 : 90);
		$time_limit = (int) ini_get('max_execution_time');
		$start_time = time();

		$memory_limit = ini_get('memory_limit');

		if (substr($memory_limit, -1) == 'M') {
			$memory_limit = (float) substr($memory_limit, 0, -1);
		} else if (substr($memory_limit, -1) == 'G') {
			$memory_limit = (float) substr($memory_limit, 0, -1) * 1024;
		}

		$product_index = array();
		$import_run_id = uniqid();

		$progress = Registry::get($schema_id . '_progress');
		$import_data = Registry::get($schema_id . '_data');

		if ($progress && $progress['time'] < strtotime('-' . $progress_lifetime)) {
			$progress = null;
		}

		if ($progress && $progress['run_id']) {
			$import_run_id = $progress['run_id'];
		}

		$this->run_id = $import_run_id;

		$data_stack = array();
		$offset = -1;

		$unique_param = $options['product_unique'];
		$mode = (int) $options['mode'];
		$allow_insert_page = (int) $options['create_categories'];

		if (!isset($options['elements'][$options['product_unique']])) {

			if (!is_numeric($options['product_unique']) && ($options['datatype'] == 'csv' || $options['datatype'] == 'xsl' || $options['datatype'] == 'xlsx' || $options['datatype'] == 'ods')) {
				$options['elements']['_unique'] = $reader->reader->getColumnIndex($options['product_unique']);
			} else {
				$options['elements']['_unique'] = $options['product_unique'];
			}

			$unique_param = '_unique';
		}

		if (isset($options['variant_element'])) {
			$options['elements']['_variant_product_id'] = $options['variant_element'];

			if (!is_numeric($options['variant_element']) && ($options['datatype'] == 'csv' || $options['datatype'] == 'xsl' || $options['datatype'] == 'xlsx' || $options['datatype'] == 'ods')) {
				$options['elements']['_variant_product_id'] = $reader->reader->getColumnIndex($options['variant_element']);
			}

			if (!isset($options['elements']['value'])) {
				$options['elements']['value'] = 'value';
			}
		}

		if ($options['datatype'] == 'csv' || $options['datatype'] == 'xsl' || $options['datatype'] == 'xlsx' || $options['datatype'] == 'ods') {
			// flush first line
			$reader->getElement(null);
		}

		if (!isset($import_data['id_map'])) {
			$import_data['id_map'] = array();
		}

		$this->id_map = $import_data['id_map'];
		$this->last_product_id = $import_data['last_product_id'];

		while (($product_data = $options['elements']) && ($product = $reader->getElement($options['product_element'], $product_data)) !== false) {
			$offset++;
			$is_variant = false;

			if ($progress && $progress['offset'] >= $offset) {
				continue;
			}

			if ($product_data['_variant_product_id']) {
				// product is variant
				$is_variant = true;

				if ($unique_param == 'id') {
					$product_data['_id'] = $product_data[$unique_param];
				}

				$product_data[$unique_param] = uniqid();
			}

			$product_data = $reader->sanitizeData($product, $product_data, $import_data, false, $options);

			$unique = $product_data[$unique_param];

			if (!$is_variant && ($unique === null || $unique === '')) {
				if ($options['product_unique'] == 'id') {
					$unique = uniqid();
				} else {
					$this->log('Warning: empty unique data (offset ' . $offset . ')');
					continue;
				}
			}

			$unique = $this->hash($unique);

			if (!empty($product_data['files'])) {

				if (($options['files_insert_only'] && !empty($product_data['id'])) || !empty($product_data['skip']) || !empty($product_data['delete'])) {
					unset($product_data['files']);
				} else {
					foreach ($product_data['files'] as $i => $file) {
						$source = $target = null;
						$is_image = false;
						$lazy_file = false;

						if (is_string($file)) {
							$source = $file;
						} else if (is_array($file) && isset($file[0])) {
							$source = $file[0];

							if (!empty($file[1])) {
								$target = $file[1];
							}

							if (isset($file[2])) {
								$is_image = $file[2];
							}

							if (isset($file[3])) {
								$lazy_file = $file[3];
							}
						} else {
							$source = !empty($file['source']) ? $file['source'] : $file['filename'];
							$target = $file['filename'];
							$is_image = (isSet($product_data['files']['is_image']) ? $product_data['files']['is_image'] : null);
						}

						if ($target) {
							$tinfo = pathinfo($target);
							$target = $tinfo['basename'];
						}

						if (is_array($file) && !empty($file['filename'])) {

						} else {
							$product_data['files'][$i] = array(
								'source' => $source
							);
						}

						$product_data['files'][$i]['lazy'] = $lazy_file;
						$product_data['files'][$i]['target'] = $target;

						if (!isset($product_data['files'][$i]['directory'])) {
							$product_data['files'][$i]['directory'] = $options['files_target'];
						}

						$already_exists = false;

						$status_file = Core::$api->copyFile($source, array($product_data['files'][$i]['directory'], $target), $is_image, $already_exists, $lazy_file);

						if ($status_file) {
							$product_data['files'][$i]['filename'] = $status_file;
						}

						if ($status_file && !$already_exists) {
							$this->log(__('dataimport_copy_file', $source));
						} else if (!$status_file) {
							$this->log(__('dataimport_copy_file_error', $source));
						}
					}
				}
			}

			if (!empty($product_data['categories']) && isBucket($product_data['categories'])) {
				$product_data['categories']->allow_insert = $allow_insert_page;
			}

			if (!empty($product_data['manufacturers']) && isBucket($product_data['manufacturers'])) {
				$product_data['manufacturers']->allow_insert = $allow_insert_page;
			}

			$data_stack[$unique] = $product_data;

			$time_left = $time_limit - (time() - $start_time);
			$memory_left = $memory_limit - round(memory_get_usage(true) / 1024 / 1024, 1);

			if (count($data_stack) >= $stack_limit || ($time_limit && $time_left <= 5) || ($memory_left <= 1.5)) {
				$_SESSION['dataimport_ajax']['offset'] = $this->ajax_offset = $offset;

				$this->log(__('dataimport_saving_data', count($data_stack)));
				$this->saveProducts($data_stack, $options, $product_index, $mode);
				$data_stack = array();

				$import_data['id_map'] = $this->id_map;
				$import_data['last_product_id'] = $this->last_product_id;

				Registry::set($schema_id . '_progress', array('time' => time(), 'offset' => $offset + 1, 'run_id' => $import_run_id));
				Registry::set($schema_id . '_data', $import_data);

				if (($time_limit && $time_left <= 5) || ($memory_left <= 1.5)) {
					$current_offset = $offset + 1;
					return -1;
				}
			}
		}

		if (!empty($data_stack)) {
			$this->log(__('dataimport_saving_data', count($data_stack)));
			$this->saveProducts($data_stack, $options, $product_index, $mode);
			$data_stack = array();
		}

		$current_offset = $offset + 1;
		$_SESSION['dataimport_ajax']['offset'] = $this->ajax_offset = $offset;

		$import_data['id_map'] = $this->id_map;
		$import_data['last_product_id'] = $this->last_product_id;

		Registry::set($schema_id . '_progress', null);
		Registry::set($schema_id . '_data', $import_data);

		$ids_to_delete = array();

		if ((int) $options['delete_products'] >= 3) {
			$offset = 0;

			while (($_products = Core::$db->product()->select('id')->where('import_uid IS NULL OR import_uid != "' . $this->run_id . '"')->limit(500, $offset)) && count($_products)) {
				foreach ($_products as $_product) {
					$ids_to_delete[] = $_product['id'];
				}

				$offset += 500;
			}
		} else if ((int) $options['delete_products']) {
			$offset = 0;

			while (($_products = Core::$db->product()->select('id')->where('import_schema', $this->current_schema)->where('import_uid != "' . $this->run_id . '"')->limit(500, $offset)) && count($_products)) {
				foreach ($_products as $_product) {
					$ids_to_delete[] = $_product['id'];
				}

				$offset += 500;
			}
		}

		if ($ids_to_delete && is_array($ids_to_delete)) {

			foreach ($ids_to_delete as $id) {
				if ($options['delete_products'] == 1 || $options['delete_products'] == 4) {
					// set status
					if ($product = Core::$api->product->update($id, array('status' => 0))) {
						$this->log(__('dataimport_unset_status', $product['id'], $product['name']));
					}
				} else if ($options['delete_products'] == 2 || $options['delete_products'] == 3) {
					// hard delete

					unset($product_index[array_search($id, $product_index)]);

					if ($product = Core::$api->product->delete($id)) {
						$this->log(__('dataimport_delete', $product['id'], $product['name']));
					}
				}
			}
		}


		return true;
	}

	private function hash($str)
	{
		if (is_numeric($str) || preg_match('/^[a-z\d\-\_]{1,16}$/i', $str)) {
			return $str;
		}

		return md5($str);
	}

	public function _updateProduct($product, $data, $orig_data)
	{
		if ($product['lock_data']) {
			unset($data['name'], $data['description'], $data['description_short'], $data['sef_url'], $data['title'], $data['meta_description'], $data['meta_keywords']);
			unset($orig_data['name'], $orig_data['description'], $orig_data['description_short'], $orig_data['sef_url'], $orig_data['title'], $orig_data['meta_description'], $orig_data['meta_keywords'], $orig_data['categories'], $orig_data['manufacturers']);
		}

		if ($product['lock_price']) {
			unset($data['price'], $data['cost'], $data['margin'], $data['vat_id'], $data['recycling_fee'], $data['copyright_fee']);
			unset($orig_data['price'], $orig_data['cost'], $orig_data['margin'], $orig_data['vat_id'], $orig_data['recycling_fee'], $orig_data['copyright_fee']);
		}

		if ($product['lock_stock']) {
			unset($data['stock'], $data['availability_id']);
			unset($orig_data['stock'], $orig_data['availability_id']);
		}

		unset($data['last_change'], $data['last_change_user'], $data['import_schema']);
		unset($orig_data['last_change'], $orig_data['last_change_user'], $orig_data['import_schema']);

		if (empty($data)) {
			return false;
		}

		$model = $product->model;

		if ($model && is_object($model)) {
			$fields = array_keys($orig_data);

			$current_hash = call_user_func_array(array($model, 'getDataHash'), array($product->as_array(), $fields));

			unset($orig_data['id']);

			$new_hash = call_user_func_array(array($model, 'getDataHash'), array($orig_data, $fields));

			if ($current_hash == $new_hash) {

				$product->update(array('import_uid' => $this->run_id));

				return false;
			}
		}
	}

	private function saveProducts($stack, $options, & $product_index = array(), $mode = 0)
	{
		$unique_param = $options['product_unique'];
		$variant_el = $options['variant_element'];

		$count = array(
			'update' => 0,
			'insert' => 0,
			'skip' => 0
		);

		foreach ($stack as $unique => $product) {

			if (!empty($product['skip']) && $product['skip']) {
				$count['skip']++;
				continue;
			}

			if (isset($variant_el) && $product['_variant_product_id']) {
				$variant = $product;

				if (!$variant['value'] && preg_match('/^(.*)\:([^\:]+)$/', $product['name'], $m) && $m) {
					$variant['name'] = trim($m[1]);
					$variant['value'] = trim($m[2]);
				}

				if ($variant['_id']) {
					$variant['id'] = $variant['_id'];
				}

				$parent_id = $product_index[$product['_variant_product_id']];

				if (!$parent_id && !empty($this->id_map[$product['_variant_product_id']])) {
					$parent_id = $this->id_map[$product['_variant_product_id']];
				} else if (!$parent_id && is_numeric($product['_variant_product_id'])) {
					$parent_id = $product['_variant_product_id'];
				}

				if (!$parent_id || !is_numeric($parent_id)) {
					$parent_id = (int) $this->last_product_id;
				}

				if ($parent_id) {
					$variant = Core::$api->bucket($variant);

					Core::$api->product->setAttributes($parent_id, array($variant), false);

					if ($variant->inserted === true) {
						$this->log(__('dataimport_insert_variant', $product['name'], $product['_variant_product_id']));
						$count['insert']++;
					} else if ($variant->updated === true) {
						$this->log(__('dataimport_update_variant', $product['name'], $product['_variant_product_id']));
						$count['update']++;
					} else {
						$count['skip']++;
					}
				} else {
					$this->log('Warning: invalid variant (' . $product['name'] . ') - parent product not found (' . $product['_variant_product_id'] . ')');
				}

				continue;
			}

			if ($product[$unique_param] !== null) {
				$new_unique = $product[$unique_param];

				if ($unique_param == 'id') {
					$new_unique = null;
				} else if ($new_unique === null || $new_unique === '') {
					$this->log('Warning: empty unique data (current unique: ' . $unique . ')');
					continue;
				}
			}

			if (!empty($product['delete']) && $product['delete'] && $product['id']) {
				unset($product_index[$unique]);
				Core::$api->product->delete('id', $product['id']);
				$this->log(__('dataimport_delete', $product['id'], $product['name']));

				$count['skip']++;
				continue;
			} else if (!empty($product['delete']) && $product['delete']) {
				unset($product_index[$unique]);

				$count['skip']++;
				continue;
			}

			$inserted = false;

			try {
				$product['import_unique'] = $unique;
				$product['import_schema'] = $this->current_schema;
				$product['import_uid'] = $this->run_id;

				$uniq = 'import_unique';

				if (!empty($product['id']) && (int) $product['id']) {
					$uniq = 'id';

					unset($product['import_unique'], $product['import_schema'], $product['import_uid']);
				}

				$_p = Core::$db->product()->where('import_unique', $product['import_unique'])->fetch();
				if($_p['lock_data'] && isset($product['files'])){
					unset($product['files']);
				}

				$result = Core::$api->product->save($product, $uniq, false, array($this, '_updateProduct'), $mode, $inserted);
			} catch (Exception $e) {
				$this->log('ERROR: ' . $e);
				$result = false;
			}

			if ($result && is_numeric($result)) {

				$count[(!$inserted ? 'update' : 'insert')]++;

				$this->last_product_id = $result;

				$this->log(__('dataimport_' . (!$inserted ? 'update' : 'insert'), $result, isset($product['name']) ? $product['name'] : ''));

				if ($unique_param == 'id' && !empty($this->id_map[$unique])) {
					$this->id_map[$unique] = (int) $result;
				}

				$product_index[$unique] = (int) $result;
			} else {
				$count['skip']++;
			}
		}

		$this->log(__('dataimport_saved', $count['insert'], $count['update'], $count['skip']));
	}

	private function getServerScore()
	{
		$score = 0;

		@set_time_limit(100);
		$time_limit = (int) ini_get('max_execution_time');

		$score += $time_limit / 100;

		$memory_limit = ini_get('memory_limit');

		if (substr($memory_limit, -1) == 'M') {
			$memory_limit = (float) substr($memory_limit, 0, -1);
		} else if (substr($memory_limit, -1) == 'G') {
			$memory_limit = (float) substr($memory_limit, 0, -1) * 1024;
		}

		$score += $memory_limit / 64;

		if (version_compare(PHP_VERSION, '5.3', '>=')) {
			$score += 1;
		}

		return round($score, 2);
	}

	private function saveSchema($file, array $options)
	{
		$php = "<?php defined('WEBMEX') or die('No direct access.');\n\n";
		$php .= "/* GENERATED BY WEBMEX " . Core::version . "; " . date('Y-m-d H:i:s') . "; " . Core::config('instid') . " */\n\n";
		$php .= "\$import_options = array(\n";

		$php .= $this->printPhp($options);

		$php .= ");\n\n/* (DO NOT REMOVE THIS LINE) PUT YOUR CONTENT AND FUNCTIONS BELOW */\n\n";

		if (file_exists($file)) {
			$content = file_get_contents($file);

			if (preg_match('/\$import_options/', $content)) {
				$pos = strpos($content, ');', 80) + 3;
			} else {
				$content = preg_replace('/\<\?php( defined\(\'WEBMEX\') or die\(\'No direct access\.\'\)\;)?/miu', '', $content);
				$pos = 0;
			}

			if (($_pos = strpos($content, 'FUNCTIONS BELOW */')) > 0) {
				$pos = $_pos + 20;
			}

			$php .= substr($content, $pos);
		}

		file_put_contents($file, $php);
	}

	private function printPhp($options, $level = 1)
	{
		$php = '';

		foreach ($options as $option => $value) {

			if ($value === null) {
				$value = 'NULL';
			} else if (is_bool($value) || is_numeric($value) || is_object($value)) {
				$value = (string) $value;
			} else if (is_array($value)) {
				$value = "array(\n" . $this->printPhp($value, $level + 1) . str_repeat("\t", $level) . ")";
			} else {
				$value = "'" . $value . "'";
			}

			$php .= "" . (str_repeat("\t", $level)) . "'" . $option . "' => " . $value . ",\n";
		}

		return $php;
	}

}