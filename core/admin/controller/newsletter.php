<?php

defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */
class Controller_Newsletter extends AdminController
{

	protected $csv_delimiter = "\t";

	public function __construct()
	{
		parent::__construct();

		Core::$active_tab = 'customers';
	}

	public function index()
	{
		$order_by = 'email';
		$order_dir = 'ASC';

		sortDir($order_by, $order_dir);

		$emails = Core::$db->newsletter_recipient()->order($order_by . ' ' . $order_dir)->limit(pgLimit(), pgOffset());

		$total_count = Core::$db->newsletter_recipient;

		if ($emails->getWhere()) {
			$total_count->where($emails->getWhere());
		}

		$total_count = $total_count->group("COUNT(*)");
		$total_count = (int) $total_count[0];

		$this->content = tpl('newsletter/list.latte', array(
			'order_by' => $order_by,
			'order_dir' => $order_dir,
			'emails' => $emails,
			'total_count' => $total_count
				));
	}

	public function delete($id)
	{
		$this->tpl = null;

		$email = Core::$db->newsletter_recipient[(int) $id];

		if ($email) {
			$email->delete();
		}

		redirect('admin/newsletter');
	}

	public function export_csv()
	{
		$this->tpl = null;

		header('Content-Type: text/csv');
		header('Content-Disposition: attachment; filename="newsletter_' . date('Y-m-d') . '.csv"');
		header('Content-Transfer-Encoding: binary');
		header('Expires: 0');
		header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
		header('Pragma: no-cache');

		$tmp = tmpfile();

		foreach (Core::$db->newsletter_recipient() as $email) {
			fputcsv($tmp, array($email['email']), $this->csv_delimiter);
		}

		rewind($tmp);
		fpassthru($tmp);

		fclose($tmp);
	}

	public function send()
	{
		if (!empty($_POST)) {
			$subject = trim($_POST['subject']);
			$text = trim($_POST['text']);

			$mailer = mailerConnect();
			$mailer->From = Core::config('email_sender');
			$mailer->FromName = Core::config('store_name');

			$replacements['store_name'] = Core::config('store_name');
			$replacements['store_url'] = url(null, null, true);
			$replacements['admin_url'] = url('admin', null, true);

			$mailer->Subject = $subject;

			$text = preg_replace('/(src|href)\=\"files/miu', '$1="' . Core::$url . 'files', $text);

			$mailer->MsgHTML($text);

			if (isset($_POST['test']) && !empty($_POST['testmail'])) {
				$mailer->AddAddress($_POST['testmail']);
				$mailer->send();

				flashMsg(__('msg_sent'));
			} else if (isset($_POST['send']) && $text && $subject) {
				$errors = array();
				$success = 0;

				foreach (Core::$db->newsletter_recipient()->order('id ASC') as $email) {
					$mailer->AddAddress($email['email']);

					if (!$mailer->send()) {
						$errors[] = $email['email'];
					} else {
						$success++;
					}

					$mailer->ClearAddresses();
				}

				$_SESSION['newsletter_status'] = array(
					'success' => $success,
					'errors' => $errors
				);

				flashMsg(__('msg_sent'));

				redirect('admin/newsletter/status');
			}
		}

		$this->content = tpl('newsletter/send.latte', array(
				));
	}

	public function status()
	{
		if (empty($_SESSION['newsletter_status'])) {
			redirect('admin/newsletter');
		}

		$this->content = tpl('newsletter/status.latte', array(
			'success' => $_SESSION['newsletter_status']['success'],
			'errors' => $_SESSION['newsletter_status']['errors']
				));
	}

}