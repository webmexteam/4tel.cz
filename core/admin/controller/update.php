<?php

defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */
class Controller_Update extends AdminController
{

	public function __construct()
	{
		parent::__construct();

		gatekeeper('update');

		Core::$active_tab = 'system';
	}

	public function index()
	{
		$ftp_required = !is_writable(DOCROOT) || !is_writable(APPROOT . 'include');
		$ftp = array();
		$msg = null;

		$beta = Registry::get('update_beta') ? 1 : 0;
		;

		unset($_SESSION['webmex_update']);

		if (isSet($_GET['beta']) && $_GET['beta'] != $beta) {
			$beta = !empty($_GET['beta']) ? 1 : 0;

			Registry::set('update_beta', $beta);
		}

		$install = null;
		if (!empty($_POST['install'])) {
			$install = trim($_POST['install']);
		}

		$update = Update::check($beta, $install);

		if ($update && $update['items']) {
			$sps_installed = Registry::get('servicepacks_installed');

			if (!$sps_installed) {
				$sps_installed = array();
			}

			$sps_installed = array_merge($sps_installed, Core::$servicepacks);

			foreach ($update['items'] as $item_name => $data) {

				$module = null;
				$is_service_pack = (bool) preg_match('/^SP_[\d\-\.]+$/', $item_name);

				if ($item_name != 'system' && !$is_service_pack && !($module = Core::module($item_name)) && !$data['install']) {
					unset($update['items'][$item_name]);
				} else if ($module) {
					$update['items'][$item_name]['module'] = $module;
				}

				if ($is_service_pack && ($spname = strtoupper(preg_replace('/[\-\.]/', '', $item_name))) && in_array($spname, $sps_installed)) {
					unset($update['items'][$item_name]);
				}
			}
		}



		if ($ftp_required) {
			if (!empty($_COOKIE['update_ftp'])) {
				$data = @base64_decode($_COOKIE['update_ftp']);
				$ftp = @unserialize($data ^ str_repeat(Core::config('instid'), ceil(strlen($data) / strlen(Core::config('instid')))));
			}

			if (!$ftp || empty($ftp['ftp_server'])) {
				$ftp = array(
					'ftp_server' => $_SERVER['HTTP_HOST'],
					'ftp_port' => '21',
					'ftp_dir' => Core::$base,
					'ftp_user' => '',
					'ftp_password' => '',
				);
			}
		}

		$has_install = false;

		if (!empty($update['items'])) {
			foreach ($update['items'] as $item) {
				if ($item['install']) {
					$has_install = true;
				}
			}
		}
    
    $dbver = Core::config('dbversion');
    if (!$dbver) {
        $dbver = '1.0.0';
    }              
    

		$this->content = tpl('update.latte', array(
			'ftp_required' => $ftp_required,
			'ftp' => $ftp,
			'msg' => $msg,
			'update' => $update,
			'beta' => $beta,
			'has_install' => $has_install,
      'dbver' => $dbver,
      'ver' => Core::version,
				));
	}

	private function saveCookie($data)
	{
		if (isSet($data['ftp_save_cookie'])) {
			$str = serialize(array(
				'ftp_server' => $data['ftp_server'],
				'ftp_port' => $$data['ftp_port'],
				'ftp_dir' => $data['ftp_dir'],
				'ftp_user' => $data['ftp_user'],
				'ftp_password' => $data['ftp_password'],
					));

			$str = base64_encode($str ^ str_repeat(Core::config('instid'), ceil(strlen($str) / strlen(Core::config('instid')))));

			setcookie('update_ftp', $str, strtotime('+1 year'), url('admin/update'));
		} else {
			setcookie('update_ftp', '', time() - 1);
		}
	}

	public function ajax_run()
	{
		$this->tpl = null;

		if (!empty($_GET['ftp_user'])) {
			Update::$wrapper = 'ftp://' . $_GET['ftp_user'] . ':' . $_GET['ftp_password'] . '@' . $_GET['ftp_server'] . ':' . $_GET['ftp_port'] . $_GET['ftp_dir'];

			if (isSet($_GET['ftp_save_cookie'])) {
				$this->saveCookie($_GET);
			}
		}

		echo str_pad(' ', 512);
		ob_end_flush();

		if (!isSet($_SESSION['webmex_update'])) {
			echo "Error: Session is missing.";
		} else {
			if (FS::$wrapper) {
				Update::$wrapper = FS::$wrapper;
			}

			if (!empty($_GET['update'])) {
				$this->content = Update::run($_GET['update']);
			}

			unset($_SESSION['webmex_update']);
		}

		flush();
		exit;
	}
  
  public function updatedb() {

      $dbver = Core::config('dbversion');
      if (!$dbver) {
          $dbver = '1.0.0';
      }  

			require_once(DOCROOT . 'core/update.php');
			$script = new Updatescript();      
			$script->exec_individually($dbver);      
      
      redirect(true, 303);        
  }  
  
  
  public function update_force109() {

			require_once(DOCROOT . 'core/update.php');

			$script = new Updatescript();
			$script->exec('1.0.9');  
  }
  

	public function ajax_run_db()
	{
		$this->tpl = null;
		$out = '';

		echo str_pad(' ', 512);
		ob_end_flush();

		echo '<script>parent._progress(\'' . json_encode(array('progress' => 100, 'log' => trim(__('update_running_db_update')))) . '\');</script>';
		flush();

		$v = Core::config('version');

		if (!$v) {
			$v = '1.1.6';
		}

		if (Update::getVersions($v, Core::version)) {
			ob_start();

			require_once(DOCROOT . 'core/update.php');

			$script = new Updatescript();
			$script->exec();

			$result = ob_get_clean();


			echo '<script>parent._progress(\'' . json_encode(array('progress' => 100, 'log' => trim(__('update_db_finished', $result)))) . '\');</script>';
			flush();
		}

		foreach (Core::$modules as $module_name => $module) {
			if ($module->installed) {
				if (Update::getVersions($module->registry['version'], $module->getInfo('version'))) {
					$module->update();
				}
			}
		}

		if ($sps = glob(DOCROOT . 'core/servicepacks/*')) {
			$installed = Registry::get('servicepacks_installed');

			if (!$installed) {
				$installed = array();
			}

			foreach ($sps as $sp) {
				if (is_file($sp) && preg_match('/\/sp_\d+/', $sp)) {
					$classname = strtoupper(substr($sp, strrpos($sp, '/') + 1, -4));

					if (!in_array($classname, $installed)) {
						require_once($sp);

						if (class_exists($classname, false)) {
							$sp_inst = new $classname;
							$sp_inst->install();
							echo '<script>parent._progress(\'' . json_encode(array('progress' => 100, 'log' => trim(__('update_service_pack_db', $classname)))) . '\');</script>';
							flush();
						}
					}
				}
			}
		}

		exit;
	}

	public function ajax_testConnection()
	{
		$this->tpl = null;

		$_POST['ftp_dir'] = '/' . trim($_POST['ftp_dir'], ' /') . '/';

		if (!@ftp_connect($_POST['ftp_server'], $_POST['ftp_port'], 3)) {
			echo 'connection_failed';
		} else if (@file_exists('ftp://' . $_POST['ftp_user'] . ':' . $_POST['ftp_password'] . '@' . $_POST['ftp_server'] . ':' . $_POST['ftp_port'] . $_POST['ftp_dir'] . 'core/include/core.php')) {
			echo 'ok';
		} else {
			echo 'invalid_dir';
		}
	}

	public function updatescript()
	{
		// do not remove. is used after manual update

		$this->tpl = 'result.latte';

		ob_start();

		$updated = false;

		echo __('update_running_db_update') . "\n";

		$v = Core::config('version');

		if (!$v) {
			$v = '1.1.6';
		}

		if (Update::getVersions($v, Core::version)) {
			require_once(DOCROOT . 'core/update.php');

			echo __('update_running_system_update') . "\n";
			$script = new Updatescript();
			$script->exec();

			$updated = true;
		}

		foreach (Core::$modules as $module) {
			if ($module->installed) {
				if (Update::getVersions($module->registry['version'], $module->getInfo('version'))) {
					echo __('update_running_module_update', $module->getInfo('name')) . "\n";
					$module->update();
					$updated = true;
				}
			}
		}

		echo __('update_db_finished', '') . "\n";

		if (!$updated) {
			redirect('admin');
		}

		$this->content = ob_get_clean();
	}

}