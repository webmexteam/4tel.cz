<?php

defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */
class Controller_Install extends AdminController
{

	public function __construct()
	{
		require_once(APPROOT . 'include/adminhelpers.php');

		if (Core::$db && Core::config('instid') && Core::$uri != 'install/deactivate') {
			redirect('admin');
		}

		$this->tpl = tpl('install.latte');

		$license = array('instid' => '', 'license_key' => '', 'is_license' => FALSE);

		if(in_array($_SERVER['SERVER_ADDR'], array('127.0.0.1', '::1')) || $_SERVER['HTTP_HOST'] == 'localhost' || $_SERVER['SERVER_NAME'] == 'localhost') {
			$license = array(
				'instid'      => 'localhost', 
				'license_key' => 'VQgGWxpQClULRANXWxEDFFlfDggPFQICCENVAQpHDFAOAAoIWwgDXAQCDFJVCgANCwwHAkdcAQsQDQtSUglfVkdGE1oIAwReQw0AXENfXVUDWlxWRxIaCltbUQpAXwYKDhBmBANUVR0QRBcUT1wcFFlfDg8PFQIABFtGCAZTAw0DAgUDWAkFBgAEC0RaTg==',
				'is_license'  => TRUE
			);
		}

		$this->tpl->license = $license;
	}

	public function index()
	{
		$this->tpl = null;

		redirect('admin/install/step/0');
	}

	public function step($step = 0)
	{
		$this->tpl->step = $step;

		if ($step != 4 && file_exists(DOCROOT . 'etc/data.sqlite.php')) {
			redirect('admin');
		}

		if (!empty($_POST)) {
			$_SESSION['install_data'] = array_merge((array) $_SESSION['install_data'], $_POST);
		}

		if (!empty($_SESSION['install_data']['language'])) {
			Core::$language = $_SESSION['install_data']['language'];
			Core::loadI18N();
		}

		if ($_SERVER['SERVER_ADMIN'] == 'root@endora.cz') {
			$this->tpl = null;
			die('<p><b>Tento hosting neni podporovan. Pouzijte prosim jiny hosting, ktery nevklada do kodu reklamu.</b></p>');
		}

		if ($step == 0) {

			$languages = array();
			foreach ((array) glob(APPROOT . 'i18n/*', GLOB_ONLYDIR) as $lang) {
				$l = substr($lang, strrpos($lang, '/') + 1);

				$languages[$l] = __('language_' . $l);
			}

			$this->tpl->languages = $languages;
			$errors = array();

			if (!class_exists('PDO')) {
				$errors[] = '- ' . __('install_missing_pdo') . ' (http://www.php.net/manual/en/book.pdo.php)';
			}

			if (class_exists('PDO') && !in_array('sqlite', PDO::getAvailableDrivers()) && !in_array('mysql', PDO::getAvailableDrivers())) {
				$errors[] = '- ' . __('install_missing_pdo_driver');
			}

			$this->tpl->errors = $errors;
		} else if ($step == 1) {
			$ftp_required = false;

			try {
				@rmdir(DOCROOT . 'etc/tmp');
				$er = error_reporting(0);

				FS::mkdir(DOCROOT . 'etc/tmp', true);

				if (!@file_put_contents(DOCROOT . 'etc/tmp/testfile.txt', 'WEBMEX')) {
					$ftp_required = true;
				}
			} catch (Exception $e) {
				$ftp_required = true;
			}

			error_reporting($er);

			if ($ftp_required) {
				$this->tpl->ftp = array(
					'server' => $_SERVER['HTTP_HOST'],
					'port' => '21',
					'dir' => Core::$base,
					'username' => '',
					'password' => ''
				);
			}

			$this->tpl->ftp_required = $ftp_required;
		} else if ($step == 2) {
			$drivers = PDO::getAvailableDrivers();

			if (!empty($_POST['server']) && !empty($_POST['username'])) {
				$_POST['dir'] = '/' . trim($_POST['dir'], '/') . '/';

				$this->_write2def("Core::\$def['fs_wrapper'] = 'ftp://" . $_POST['username'] . ":" . $_POST['password'] . "@" . $_POST['server'] . ":" . $_POST['port'] . $_POST['dir'] . "';");
			}

			if (in_array('sqlite', $drivers)) {
				$this->tpl->has_sqlite = true;
			}

			if (in_array('mysql', $drivers)) {
				$this->tpl->has_mysql = true;
			}

			$this->tpl->mysql = array(
				'server' => 'localhost',
				'port' => '',
				'dbname' => '',
				'username' => '',
				'password' => ''
			);
		} else if ($step == 3) {
			if ($_POST['db'] == 'mysql' && !empty($_POST['server']) && !empty($_POST['dbname'])) {

				$this->_write2def(
"Core::\$def['db'] = array(
	'dsn' => 'mysql:host=" . $_POST['server'] . ";dbname=" . $_POST['dbname'] . (!empty($_POST['port']) ? ';port=' . $_POST['port'] : '') . "',
	'user' => '" . $_POST['username'] . "',
	'password' => '" . $_POST['password'] . "'
);"
				);
			}
		} else if ($step == 4) {
			// Prepare files & database
			$this->_setup();

			// Insert test shop data
			$sampleDataFolder = DOCROOT . 'sample_data/';
			$sampleDataFile = $sampleDataFolder . 'sample_data.sql';
			//**/print('$sampleDataFolder'); dump($sampleDataFolder);
			//**/print('$sampleDataFile'); dump($sampleDataFile);

			if ($_POST['insert_sample_data'] && is_dir($sampleDataFolder) && file_exists($sampleDataFile)) {
				$tres = Core::$db_conn->exec( file_get_contents($sampleDataFile) );
				//**/print('$tres'); dump($tres);

				//**/print('copy'); dump("FS::copydir($sampleDataFolder . 'files', DOCROOT . 'files');");
				$tres = FS::copydir($sampleDataFolder . 'files', DOCROOT . 'files');
				//**/print('$tres'); dump($tres);
			}
			//**/print('rmdir'); dump("FS::rmdir($sampleDataFolder);");
			$tres = FS::rmdir($sampleDataFolder);
			//**/print('$tres'); dump($tres);
			//**/die;

			// Clear
			unset($_SESSION['install_data']);
		} else if ($step > 4) {
			redirect('admin');
		}
	}

	public function ajax_testFtpConnection()
	{
		$this->tpl = null;

		if (empty($_POST['server'])) {
			die('Unknown server');
		}

		$_POST['dir'] = '/' . trim($_POST['dir'], ' /') . '/';

		if (!@ftp_connect($_POST['server'], $_POST['port'], 3)) {
			$e = error_get_last();

			if ($e) {
				echo $e['message'];
			}
		} else if ($fp = @fopen('ftp://' . $_POST['username'] . ':' . $_POST['password'] . '@' . $_POST['server'] . ':' . $_POST['port'] . $_POST['dir'] . 'core/include/core.php', 'r')) {
			@fclose($fp);
			echo 'OK';
		} else {
			echo 'INVALID USERNAME/PASSWORD OR INVALID DIRECTORY';
		}
	}

	public function ajax_testConnection()
	{
		$this->tpl = null;

		if (empty($_POST['server'])) {
			die('Unknown server');
		}

		if (empty($_POST['dbname'])) {
			die('Unknown database');
		}

		try {
			$conn = new PDO('mysql:host=' . $_POST['server'] . ';dbname=' . $_POST['dbname'] . (!empty($_POST['port']) ? ';port=' . $_POST['port'] : ''), $_POST['username'], $_POST['password']);
			echo 'OK';
		} catch (PDOException $e) {
			echo $e->getMessage();
		}
	}

	public function _write2def($lines)
	{
		if (!file_exists(DOCROOT . 'etc/def.php')) {
			$lines = "<?php defined('WEBMEX') or die('No direct access.');	\n\n" . $lines;
		}

		file_put_contents(DOCROOT . 'etc/def.php', $lines . "\n\n", FILE_APPEND);
	}

	public function _clearCache()
	{
		if ($f = @glob(DOCROOT . 'etc/tmp/*')) {
			foreach ($f as $file) {
				FS::writable($file);
				FS::remove($file);
			}
		}
	}

	public function _setup()
	{
		if (!is_dir(DOCROOT . 'etc')) {
			FS::mkdir(DOCROOT . 'etc', true);
		}

		if (FS::$wrapper) {
			@rmdir(DOCROOT . 'etc/tmp');
			@rmdir(DOCROOT . 'etc/theme');
			@rmdir(DOCROOT . 'etc/backup');
			@rmdir(DOCROOT . 'etc/log');
		}

		FS::mkdir(DOCROOT . 'etc/backup', true);
		FS::mkdir(DOCROOT . 'etc/theme', true);
		FS::mkdir(DOCROOT . 'etc/tmp', true);
		FS::mkdir(DOCROOT . 'etc/log', true);
		FS::mkdir(DOCROOT . 'etc/xml_import', true);
		FS::mkdir(DOCROOT . 'etc/modules', true);
		FS::mkdir(DOCROOT . 'etc/feed', true);

		$this->_clearCache();

		if (!Core::$db) {
			Core::$db_conn = Core::$db_inst->connect(true);

			if (!Core::$db_conn) {
				die('Database connection error.');
			}

			include(APPROOT . 'db_schema.php');

			if (Core::$db_inst->_type == 'sqlite') {
				Db::$connection->query('CREATE TABLE IF NOT EXISTS `<?php exit; ?>` (`id` integer PRIMARY KEY)');
			}

			if (!empty($db_tables)) {
				foreach ($db_tables as $tbl_name => $tbl_schema) {
					if ($res = Core::$db_inst->schema->createTable($tbl_name, $tbl_schema)) {

					}
				}
			}

			$sql = '';

			if (!empty($db_extras)) {
				$sql .= $db_extras . "\n";
			}

			if (Core::$locale['language'] != $_SESSION['install_data']['language']) {
				include(APPROOT . 'i18n/' . $_SESSION['install_data']['language'] . '/locale.php');
			}

			$sql .= '
				INSERT INTO `config` (name, value) VALUES("version", "' . Core::version . '");
				INSERT INTO `config` (name, value) VALUES("language", "' . $_SESSION['install_data']['language'] . '");
				INSERT INTO `config` (name, value) VALUES("language_front", "' . $_SESSION['install_data']['language'] . '");
				INSERT INTO `config` (name, value) VALUES("store_name", "' . $_SESSION['install_data']['store_name'] . '");
				INSERT INTO `config` (name, value) VALUES("email_sender", "' . $_SESSION['install_data']['email'] . '");
				INSERT INTO `config` (name, value) VALUES("email_notify", "' . $_SESSION['install_data']['email'] . '");
				INSERT INTO `config` (name, value) VALUES("access_key", "' . random(9) . '");
				INSERT INTO `config` (name, value) VALUES("sync_key", "' . random(mt_rand(10, 14)) . '");
				INSERT INTO `config` (name, value) VALUES("currency", "' . Core::$locale['currency'] . '");
				INSERT INTO `config` (name, value) VALUES("price_format", "' . Core::$locale['price_format'] . '");
				INSERT INTO `config` (name, value) VALUES("vat", "' . Core::$locale['vat'] . '");
				
				INSERT INTO `user` (username, email, password, name, enable_login) VALUES("webmex", "info@webmex.cz", "' . sha1(sha1('eB!Pv1mX=EZ8<[O]3$+l@omHS')) . '", "Webmex PLUS s.r.o.", 1);
				INSERT INTO `user` (username, email, password, enable_login) VALUES("admin", "' . $_SESSION['install_data']['email'] . '", "' . sha1(sha1('admin')) . '", 1);
				INSERT INTO `user_permissions` (user_id, permission_id) VALUES(2, ' . array_search('all', Core::$def['permissions']) . ');
			' . "\n";

			if (!empty($_SESSION['install_data']['instid']) && !empty($_SESSION['install_data']['license_key']) && Core::checklkey($_SESSION['install_data']['instid'], $_SESSION['install_data']['license_key'])) {

				$instid = $_SESSION['install_data']['instid'];

				$sql .= '
					INSERT INTO `config` (name, value) VALUES("instid", "' . $instid . '");
					INSERT INTO `config` (name, value) VALUES("license_key", "' . $_SESSION['install_data']['license_key'] . '");
					INSERT INTO `config` (name, value) VALUES("activated", "1");
				' . "\n";
			} else {
				$instid = random(24);

				$sql .= '
					INSERT INTO `config` (name, value) VALUES("instid", "' . $instid . '");
					INSERT INTO `config` (name, value) VALUES("license_key", "");
					INSERT INTO `config` (name, value) VALUES("activated", "0");
				' . "\n";
			}

			unset($db_extras, $db_tables);

			if(isset($_POST['test']) && $_POST['test'] == 1) {
				if(file_exists(APPROOT . 'i18n/' . $_SESSION['install_data']['language'] . '/db_sample_data.php')){
					include(APPROOT . 'i18n/' . $_SESSION['install_data']['language'] . '/db_sample_data.php');
					$sql .= $db_sample_extras;
				}
			}

			include(APPROOT . 'i18n/' . $_SESSION['install_data']['language'] . '/db_data.php');

			if (!empty($db_extras)) {
				$sql .= $db_extras;
			}

			Core::$db_conn->exec($sql);
		}
	}

	public function deactivate()
	{
		$this->tpl = null;

		if (Core::$is_premium && !empty($_POST['instid']) && !empty($_POST['skey'])) {
			$l = Core::decodelKey();

			if ($l && $_POST['instid'] == Core::config('instid') && $_POST['skey'] == md5($l[6])) {
				Core::$db->config()->where('name', 'activated')->update(array('value' => 0));
				echo 'DEACTIVATED';
			} else {
				echo 'INVALID_DATA';
			}
		} else {
			echo 'ERROR';
		}
	}

}