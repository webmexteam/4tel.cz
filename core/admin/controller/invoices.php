<?php

defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */
class Controller_Invoices extends AdminController
{

	public function __construct()
	{
		parent::__construct();

		gatekeeper('orders-invoices');

		Core::$active_tab = 'orders';
	}

	public function index()
	{
		$order_by = 'invoice_num';
		$order_dir = 'DESC';

		sortDir($order_by, $order_dir);

		if (isSet($_POST['save']) && !empty($_POST['invoices'])) {
			$status = (int) $_POST['new_status'];

			foreach ($_POST['invoices'] as $id) {
				$invoice = Core::$db->invoice[(int) $id];

				if ($invoice) {
					$data = array('status' => $status);
					$invoice->update($data);
				}
			}

			flashMsg(__('msg_saved'));
		}

		$invoices = Core::$db->invoice()->order($order_by . ' ' . $order_dir)->limit(pgLimit(), pgOffset());

		if (!empty($_GET['search'])) {
			$q = trim($_GET['search']);

			if (empty($_GET['where']) || $_GET['where'] == 'all') {
				$invoices->where('invoice_num LIKE "' . $q . '" OR order_id LIKE "' . $q . '" OR customer_name LIKE "%' . $q . '%" OR company_id LIKE "%' . $q . '%" OR company_vat LIKE "%' . $q . '%"');
			} else if ($_GET['where'] == 'invoice_num') {
				$invoices->where('invoice_num', $q);
			} else if ($_GET['where'] == 'order_id') {
				$invoices->where('order_id', $q);
			} else if ($_GET['where'] == 'customer_name') {
				$invoices->where('customer_name LIKE "%' . $q . '%"');
			} else if ($_GET['where'] == 'company_id') {
				$invoices->where('company_id LIKE "%' . $q . '%"');
			}
		}

		$total_count = Core::$db->invoice;

		if ($invoices->getWhere()) {
			$total_count->where($invoices->getWhere());
		}

		$total_count = $total_count->group("COUNT(*)");
		$total_count = (int) $total_count[0];

		$status_options = '<option value="0" disabled="disabled">' . __('change_selected') . '</option>';

		for ($i = 1; $i <= 3; $i++) {
			$status_options .= '<option value="' . $i . '">' . __('invoice_status_' . $i) . '</option>';
		}

		$export_options = array(
			'' => __('export'),
			'this_month' => __('this_month'),
			'previous_month' => __('previous_month'),
		);

		$min_y = Core::$db->invoice()->order('date_issue ASC')->limit(1)->fetch();
		$min_y = date('Y', $min_y['date_issue']);

		for ($i = $min_y; $i <= date('Y'); $i++) {
			$export_options['year_' . $i] = __('year') . ' ' . $i;
		}

		$this->content = tpl('invoices/list.latte', array(
			'order_by' => $order_by,
			'order_dir' => $order_dir,
			'invoices' => $invoices,
			'total_count' => $total_count,
			'status_options' => $status_options,
			'export_options' => $export_options
				));
	}

	public function view($id, $download = false)
	{
		ob_clean();
		
		if ($id == 0) {
			redirect('admin/invoices');
		}

		$invoice = Core::$db->invoice[(int) $id];

		if ($id > 0 && !$invoice) {
			flashMsg(__('msg_record_not_found', $id), 'error');
			redirect('admin/invoices');
		}

		$inv = new Invoice();

		$inv->status = $invoice['status'];
		$inv->type = $invoice['type'];
		$inv->vat_payer = $invoice['vat_payer'];
		$inv->invoice_num = $invoice['invoice_num'];
		$inv->payment = $invoice['payment'];
		$inv->delivery = $invoice['delivery'];
		$inv->symbol = $invoice['symbol'];
		$inv->date_issue = $invoice['date_issue'];
		$inv->date_due = $invoice['date_due'];
		$inv->date_vat = $invoice['date_vat'];
		$inv->date_create = $invoice['date_create'];

		if (!$invoice['customer_name']) {
			$inv->customer = $invoice['customer']; // for backward compatibility (version 1.2)
		} else {
			$inv->customer = $invoice['customer_name'] . "\n" . $invoice['address']
					. "\n\n" . __('company_id') . ': ' . $invoice['company_id']
					. "     " . __('company_vat') . ': ' . $invoice['company_vat'];
		}

		$inv->shipping_address = $invoice['shipping_address'];
		$inv->note = $invoice['note'];
		$inv->voucher_id = $invoice['voucher_id'];

		// logo image
		$logo = DOCROOT . 'etc/invoice_logo.jpg';
		$inv->logo = file_exists($logo) ? $logo : null;

		// signature image
		$signature = DOCROOT . 'etc/invoice_signature.jpg';
		$inv->signature = file_exists($signature) ? $signature : null;

		$inv->issuer = Core::config('invoice_issuer');
		$inv->account_num = Core::config('account_num');
		$inv->footer = Core::config('invoice_footer');

		foreach ($invoice->invoice_items() as $item) {
			$inv->add($item['sku'], $item['name'], $item['quantity'], $item['price'], $item['vat']);
		}

		if ($download) {
			$inv->save($invoice['invoice_num'] . '.pdf', 'D');
		} else {
			$inv->save($invoice['invoice_num'] . '.pdf');
		}

		exit;
	}

	public function edit($id)
	{
		$invoice = Core::$db->invoice[(int) $id];

		if ($id > 0 && !$invoice) {
			flashMsg(__('msg_record_not_found', $id), 'error');
			redirect('admin/invoices');
		}

		if (!Core::$is_premium) {
			redirect('admin/invoices');
		}

		if (!empty($_POST)) {

			if (($errors = validate(array('invoice_num'))) === true) {
				$data = $_POST;
				$data['last_change'] = time();
				$data['last_change_user'] = User::get('id');
				$data['customer'] = '';

				$data['date_issue'] = strtotime($data['date_issue']);
				$data['date_due'] = strtotime($data['date_due']);
				$data['date_vat'] = strtotime($data['date_vat']);

				if (!$invoice) {
					$data['date_create'] = time();
					$data['vat_payer'] = (int) Core::config('vat_payer');

					$invoice_id = Core::$db->invoice(prepare_data('invoice', $data));

					if ($invoice_id) {
						Invoice::inc();
						$invoice = Core::$db->invoice[$invoice_id];
						$saved = true;
					}
				} else {
					$saved = (bool) $invoice->update(prepare_data('invoice', $data));
				}

				if ($invoice && $saved !== false) {

					foreach ($invoice->invoice_items() as $product) {
						if (!empty($_POST['product'][$product['id']])) {
							$product->update(prepare_data('invoice_items', $_POST['product'][$product['id']]));
						} else {
							$product->delete();
						}
					}

					foreach ($_POST['product'] as $id => $product) {
						if (preg_match('/new_/', $id)) {
							$product['invoice_id'] = $invoice['id'];

							if ($product['vat'] === '' && VAT_DEFAULT) {
								$product['vat'] = VAT_DEFAULT;
							}

							Core::$db->invoice_items(prepare_data('invoice_items', $product));
						}
					}

					flashMsg(__('msg_saved'));
				} else {
					flashMsg(__('msg_error'), 'error');
				}

				if (isSet($_POST['save_go'])) {
					redirect('admin/invoices');
				} else {
					redirect('admin/invoices/edit/' . $invoice['id']);
				}
			}
		} else if ($id == 0) {
			$invoice['id'] = 0;
			$invoice['invoice_num'] = Core::config('invoice_num');
			$invoice['date_issue'] = time();
			$invoice['date_vat'] = time();
			$invoice['date_due'] = strtotime('+14 days');
		}

		$this->content = tpl('invoices/edit.latte', array(
			'invoice' => $invoice
				));
	}

	public function download($id)
	{
		$this->view($id, true);
	}

	public function delete($id)
	{
		$this->tpl = null;

		$invoice = Core::$db->invoice[(int) $id];

		if ($invoice) {
			$invoice->invoice_items()->delete();
			$invoice->delete();
		}

		redirect('admin/invoices');
	}

	public function ares()
	{
		$this->tpl = null;
		$response = array('status' => false, 'msg' => '');

		if (!empty($_POST['company_id'])) {
			if (($info = Ares::getInfo($_POST['company_id'])) && $info['status'] == 'ok') {
				$response['status'] = true;
				$response['info'] = $info;
			} else if ($info) {
				$response['msg'] = $info['status'];
			}
		}

		echo json_encode($response);
	}

	public function export($what)
	{
		$this->tpl = null;
		@set_time_limit(0);

		$export = new Export('ods');

		$export->writer->createLine(array(
			__('invoice_num'),
			__('invoice_issue_date'),
			__('invoice_due_date'),
			__('invoice_symbol'),
			__('customer'),
			__('company_id'),
			__('company_vat'),
			__('address'),
			__('total_to_pay'),
			__('status')
		));

		$export->writer->head = true;

		$invoices = Core::$db->invoice()->order('id ASC');

		if ($what == 'this_month') {
			$invoices->where('date_issue >= ' . strtotime(date('Y-m-01')))->where('date_issue <= ' . time());
		} else if ($what == 'previous_month') {
			$month = date('m', strtotime('-1 month'));
			$day = date('t', strtotime(date('Y-' . $month . '-01')));

			$invoices->where('date_issue >= ' . strtotime(date('Y-' . $month . '-01')))->where('date_issue <= ' . strtotime(date('Y-' . $month . '-' . $day . ' 23:59')));
		} else {
			$year = (int) substr($what, 5);

			$invoices->where('date_issue >= ' . strtotime(date($year . '-01-01')))->where('date_issue <= ' . strtotime(date($year . '-12-31 23:59')));
		}

		foreach ($invoices as $invoice) {
			if (!$invoice['customer_name']) {
				$customer = $invoice['customer']; // for backward compatibility (version 1.2)
			} else {
				$customer = $invoice['customer_name'];
			}

			$export->writer->createLine(array(
				$invoice['invoice_num'],
				fdate($invoice['date_issue']),
				fdate($invoice['date_due']),
				$invoice['symbol'] ? $invoice['symbol'] : $invoice['invoice_num'],
				$customer,
				$invoice['company_id'],
				$invoice['company_vat'],
				$invoice['address'],
				$invoice['total_incl_vat'],
				__('invoice_status_' . $invoice['status'])
			));
		}

		$name = 'invoices_' . date('Y-m-d_H:i');

		header("Pragma: public"); // required
		header("Expires: 0");
		header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
		header("Cache-Control: private", false); // required for certain browsers
		header("Content-Type: application/vnd.oasis.opendocument.spreadsheet; charset=UTF-8");
		header("Content-Disposition: attachment; filename=\"" . dirify($name) . ".ods\";");
		header("Content-Transfer-Encoding: binary");

		echo $export->save();
	}

}