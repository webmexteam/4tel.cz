<?php

defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */
class Controller_Emails extends AdminController
{

	public $templates;

	public function __construct()
	{
		parent::__construct();

		gatekeeper('settings');

		Core::$active_tab = 'system';

		$this->templates = array();

		foreach (Core::glob('template/system/email/*') as $tpl) {
			$info = pathinfo($tpl);

			if ($info['filename'] !== 'layout') {
				$name = (string) __('email_template_' . $info['filename']);

				if ($name{0} == '_') {
					$name = $info['filename'];
				}

				$this->templates[$info['filename']] = $name;
			}
		}
	}

	public function index()
	{
		$order_by = 'id';
		$order_dir = 'ASC';

		sortDir($order_by, $order_dir);

		$emails = Core::$db->email_message()->order('`' . $order_by . '` ' . $order_dir);

		$this->content = tpl('emails/list.latte', array(
			'order_by' => $order_by,
			'order_dir' => $order_dir,
			'emails' => $emails,
			'templates' => $this->templates
				));
	}

	public function edit($id)
	{
		$email = Core::$db->email_message[(int) $id];

		if ($id > 0 && !$email) {
			flashMsg(__('msg_record_not_found', $id), 'error');
			redirect('admin/emails');
		}

		if (!empty($_POST)) {
			if (($errors = validate(array('subject'))) === true) {
				$data = $_POST;

				$data['key'] = $data['event'];

				if (!$email) {
					$email_id = Core::$db->email_message(prepare_data('email_message', $data));

					if ($email_id) {
						$email = Core::$db->email_message[$block_id];
						$saved = true;
					}
				} else {
					$saved = (bool) $email->update(prepare_data('email_message', $data));
				}

				if ($email && $saved !== false) {
					flashMsg(__('msg_saved'));
				} else {
					flashMsg(__('msg_error'), 'error');
				}

				if (isSet($_POST['save_go'])) {
					redirect('admin/emails');
				} else {
					redirect('admin/emails/edit/' . $email['id']);
				}
			}
		}

		$events = array();
		foreach (Email::$events as $event) {
			$events[$event] = __('email_event_' . $event);
		}

		$this->content = tpl('emails/edit.latte', array(
			'events' => $events,
			'templates' => $this->templates,
			'email' => $email
				));
	}

	public function delete($id)
	{
		$this->tpl = null;

		if ($email = Core::$db->email_message[(int) $id]) {
			$email->delete();

			flashMsg(__('msg_deleted'));
		}

		redirect('admin/emails');
	}

}