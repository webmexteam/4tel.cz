<?php

defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */
class Controller_Maintenance extends AdminController
{

	public function __construct()
	{
		parent::__construct();

		gatekeeper('settings');

		Core::$active_tab = 'system';
	}

	public function index()
	{
		if (!empty($_POST)) {
			if (isSet($_POST['clear_cache'])) {
				FS::cleanCache();

				flashMsg(__('msg_saved'));
			}

			if (isSet($_POST['reindex'])) {
				@set_time_limit(90);

				Search::index();

				flashMsg(__('msg_saved'));
			}

			if (isset($_POST['maintenance_on']) || isset($_POST['maintenance_off'])) {
				Core::$config['maintenance'] = isset($_POST['maintenance_on']) ? 1 : 0;

				Core::$db->config()->where('name', 'maintenance')->update(array(
					'value' => isset($_POST['maintenance_on']) ? 1 : 0
				));

				flashMsg(__('msg_saved'));
			}

			if(isset($_POST['regenerate_images'])) {
				foreach (Core::$db->product_files()->where('is_image', 1) as $file) {
					try {
						Image::thumbs(DOCROOT . $file['filename']);
					} catch (Exception $e) {

					}
				}
				flashMsg(__('images_regenerated'));
				redirect('admin/maintenance');
			}
		}

		$this->content = tpl('maintenance.latte', array(
				));
	}

	public function flush()
	{
		$this->tpl = null;

		Core::$db_inst->truncateTable('product');
		Core::$db_inst->truncateTable('product_attributes');
		Core::$db_inst->truncateTable('product_files');
		Core::$db_inst->truncateTable('product_pages');
		Core::$db_inst->truncateTable('product_discounts');
		Core::$db_inst->truncateTable('product_labels');
		Core::$db_inst->truncateTable('product_related');
		Core::$db_inst->truncateTable('search_index');
		Core::$db_inst->truncateTable('search_word');
		Core::$db_inst->truncateTable('basket');
		Core::$db_inst->truncateTable('basket_products');
		Core::$db_inst->truncateTable('redirect');
		Core::$db_inst->truncateTable('static_file');

		Core::$db->registry()->where('name LIKE "di_%" OR name LIKE "pagemap_di%" OR name LIKE "pagemap_mdi%"')->delete();

		Core::$db->page()->where('menu = 2 OR ((menu IS NULL OR menu = 0) AND parent_page != 0) OR menu = 3')->delete();

		foreach (Core::$db->featureset() as $fset) {
			$tbl = 'features_' . $fset['id'];

			try {
				Core::$db_inst->truncateTable($tbl);
			} catch (PDOException $e) {

			}
		}

		flashMsg(__('msg_saved'));

		redirect('admin/maintenance');
	}

	public function ajax_reindex()
	{
		$this->tpl = null;

		@set_time_limit(0);

		echo str_pad(' ', 512);
		ob_end_flush();

		Search::index(true);

		flush();
		exit;
	}
}