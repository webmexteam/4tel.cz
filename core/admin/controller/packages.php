<?php

defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */
class Controller_Packages extends AdminController
{

	public function __construct()
	{
		parent::__construct();

		Core::$active_tab = 'orders';
	}

	public function index()
	{
		$order_by = 'id';
		$order_dir = 'DESC';

		sortDir($order_by, $order_dir);

		if (!empty($_POST)) {
			if (isSet($_POST['change_status']) && !empty($_SESSION['post_data']['order'])) {
				foreach ($_SESSION['post_data']['order'] as $order_id => $v) {
					if ($order = Core::$db->order[(int) $order_id]) {
						$order->update(array('status' => 3));
					}
				}
			}
		}

		$orders = Core::$db->order()->where('status', 5)->order($order_by . ' ' . $order_dir);

		$total_count = Core::$db->order;

		if ($orders->getWhere()) {
			$total_count->where($orders->getWhere());
		}

		$total_count = $total_count->group("COUNT(*)");
		$total_count = (int) $total_count[0];

		$this->content = tpl('packages/list.latte', array(
			'order_by' => $order_by,
			'order_dir' => $order_dir,
			'orders' => $orders,
			'total_count' => $total_count
				));
	}

	public function print_labels()
	{
		if (!empty($_POST)) {
			$_SESSION['post_data'] = $_POST;

			$cpost = array();
			foreach ($_SESSION['post_data']['order'] as $order_id => $v) {
				if (($order = Core::$db->order[(int) $order_id]) && $order->delivery['driver'] == 'cpost') {
					$cpost[] = $order['id'];
				}
			}

			$_SESSION['post_data']['cpost'] = $cpost;
		}

		$this->content = tpl('packages/print.latte', array(
			'cpost' => count($cpost),
			'labels' => count($_SESSION['post_data']['order'])
				));
	}

	public function labels()
	{
		$this->tpl = null;

		if (!empty($_SESSION['post_data']['order'])) {
			$sheet = new Labels_Address();

			foreach ($_SESSION['post_data']['order'] as $order_id => $v) {
				if (($order = Core::$db->order[(int) $order_id])) {
					$sheet->addOrder($order);
				}
			}

			$sheet->save();
		}
	}

	public function cp_sheet()
	{
		$this->tpl = null;

		if (!empty($_SESSION['post_data']['cpost'])) {
			$sheet = new Labels_Cpsheet();

			foreach ($_SESSION['post_data']['cpost'] as $order_id) {
				if (($order = Core::$db->order[(int) $order_id]) && $order->delivery['driver'] == 'cpost') {
					$sheet->addOrder($order);
				}
			}

			$sheet->save();
		}
	}

}