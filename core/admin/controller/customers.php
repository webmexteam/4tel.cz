<?php

defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */
class Controller_Customers extends AdminController
{

	public function __construct()
	{
		parent::__construct();

		gatekeeper('customers');

		Core::$active_tab = 'customers';
	}

	public function index()
	{
		$order_by = 'last_name';
		$order_dir = 'ASC';

		sortDir($order_by, $order_dir);

		if (isSet($_POST['action']) && !empty($_POST['item'])) {
			foreach ($_POST['item'] as $id => $v) {
				$customer = Core::$db->customer[$id];

				if ($customer) {
					if ($_POST['action'] == 'delete') {
						$customer->delete();
					}
				}
			}
			flashMsg(__('msg_saved'));
			redirect('admin/customers');
		}

		$customers = Core::$db->customer()->order($order_by . ' ' . $order_dir)->limit(pgLimit(), pgOffset());

		if (!empty($_GET['search']) || !empty($_GET['category'])) {
			$q = trim($_GET['search']);

			if (valid_email($q)) {
				$customers->where('email', $q);
			} else {
				$customers->where('company LIKE "%' . $q . '%" OR first_name LIKE "%' . $q . '%" OR last_name LIKE "%' . $q . '%" OR street LIKE "%' . $q . '%" OR email LIKE "%' . $q . '%"');
			}

			if (!empty($_GET['category'])) {

				if ($_GET['category'] == Core::config('customer_group_default')) {
					$customers->where('customer_group_id = ' . (int) $_GET['category'] . ' OR customer_group_id IS NULL');
				} else {
					$customers->where('customer_group_id', (int) $_GET['category']);
				}
			}
		}

		$total_count = Core::$db->customer;

		if ($customers->getWhere()) {
			$total_count->where($customers->getWhere());
		}

		$total_count = $total_count->group("COUNT(*)");
		$total_count = (int) $total_count[0];

		$groups = array(0 => __('all'));
		foreach (Core::$db->customer_group() as $group) {
			$groups[$group['id']] = $group['name'];
		}

		$this->content = tpl('customers/list.latte', array(
			'order_by' => $order_by,
			'order_dir' => $order_dir,
			'customers' => $customers,
			'groups' => $groups,
			'total_count' => $total_count
				));
	}

	public function edit($id)
	{
		$customer = Core::$db->customer[(int) $id];

		if ($id > 0 && !$customer) {
			flashMsg(__('msg_record_not_found', $id), 'error');
			redirect('admin/customers');
		}

		if (!empty($_POST)) {
			if (($errors = validate(array('email'))) === true) {
				$data = $_POST;

				if (!empty($data['password1']) && !empty($data['password2'])) {
					if (strlen($data['password1']) >= 3 && $data['password1'] == $data['password2']) {
						$data['password'] = sha1(sha1($data['password1']));
					}
				}

				if (!$customer) {
					$customer_id = Core::$db->customer(prepare_data('customer', $data));

					if ($customer_id) {
						$customer = Core::$db->customer[$customer_id];
						$saved = true;
					}

				} else {
					if (isSet($data['active']) && !(int) $customer['active'] && (int) Core::config('customer_confirmation')) {

                        if(!isset($data['password']) && empty($customer['password'])) {
                            $customer_data = $data;
                            
                            $password = strtoupper(random());
                            $data['password'] = sha1(sha1($password));
                            $customer_data['password'] = $password;
                        }
					}

					$saved = (bool) $customer->update(prepare_data('customer', $data));

					if ($saved && isSet($customer_data)) {
						Email::event('new_account', $data['email'], null, $customer_data);
					}
				}

                $this->removeDiscountPage($customer['id']);

                foreach($data as $key => $value) {
                    if(preg_match('/discount_page_([0-9]*)/', $key, $matches)) {
                        $keyDpp = 'dpp_';
                        if(isset($matches[1])) {
                            $keyDpp = 'dpp_'.$matches[1];
                        }

                        if($value > 0) {
                            $discountPage = array('customer_id' => $customer['id'], 'page_id' => $value, 'discount' => $data[$keyDpp]);
                            Core::$db->customer_discount_pages(prepare_data('customer_discount_pages', $discountPage));
                        }
                    }
                }

                if(!empty($data['discount_page'])) {

                }


				if ($customer && $saved !== false) {
					flashMsg(__('msg_saved'));
				} else {
					flashMsg(__('msg_error'), 'error');
				}

				if (isSet($_POST['save_go'])) {
					redirect('admin/customers');
				} else {
					redirect('admin/customers/edit/' . $customer['id']);
				}
			}
		} else if ($id == 0) {
			$customer['id'] = 0;
		}

        // Create customer category discounts
        if($customer['id'] > 0) {
            $categoryDiscounts = array();
            $catDiscounts = Core::$db->customer_discount_pages()->where('customer_id', $customer['id']);

            foreach($catDiscounts as $cd) {
                $categoryDiscounts[] = array('page_id' => $cd['page_id'], 'discount' => $cd['discount'], 'tree' => $this->_pageTree($cd['page_id']));
            }
        }

		$groups = array('0' => __('automatic'));

		foreach (Core::$db->customer_group() as $group) {
			$groups[$group['id']] = $group['name'];
		}

		$this->content = tpl('customers/edit.latte', array(
			'customer' => $customer,
			'groups' => $groups,
            'discount_pages' => $categoryDiscounts,
            'pagetree' => $this->_pageTree($page)
				));
	}

	public function delete($id)
	{
		$this->tpl = null;

		$customer = Core::$db->customer[(int) $id];

		if ($customer) {
			$customer->delete();
		}

		redirect('admin/customers');
	}

	public function removeDiscountPage($id)
	{
		$discount = Core::$db->customer_discount_pages->where('customer_id', $id)->delete();
	}

	private function _pageTree($pageId, $parent_id = 0, $level = 1)
	{
		$out = '';
		$order = 'position ASC';

		if (!empty($_GET['sort'])) {
			$order = $_GET['sort'] . ' ASC';
		}

		if ($level == 1) {
			$out .= '<option value="0">' . __('none') . '</option>';

			//foreach (Core::$def['page_types'] as $type_id => $type) {
                $type_id = 2; // Category page
				$pages = Core::$db->page()->where('parent_page', $parent_id)->where('menu', $type_id)->order($order);

				if (count($pages)) {
					$out .= '<option disabled="disabled">' . $type . '</option>';

					foreach ($pages as $page) {
						$out .= '<option value="' . $page['id'] . '"' . ($pageId == $page['id'] ? ' selected="selected"' : '') . '>' . str_repeat('&nbsp;&nbsp;&nbsp;', $level) . $page['name'] . '</option>';

						$out .= $this->_pageTree($pageId, $page['id'], $level + 1);
					}
				}
			//}
		} else {
			foreach (Core::$db->page()->where('parent_page', $parent_id)->order($order) as $page) {
				$out .= '<option value="' . $page['id'] . '"' . ($pageId == $page['id'] ? ' selected="selected"' : '') . '>' . str_repeat('&nbsp;&nbsp;&nbsp;', $level) . $page['name'] . '</option>';

				$out .= $this->_pageTree($pageId, $page['id'], $level + 1);
			}
		}

		return $out;
	}
}