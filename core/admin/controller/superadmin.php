<?php

defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */
class Controller_Superadmin extends AdminController
{

	public function __construct()
	{
		parent::__construct();

		gatekeeper('settings');

		Core::$active_tab = 'system';
	}

	public function index()
	{

		if (!empty($_POST)) {
			$parts = explode('/', $_POST['template']);

			Core::$db->config()->where('name', 'webmex_product_limit')->update(array(
				'value' => trim($_POST['webmex_product_limit'])
			));

			Core::$db->config()->where('name', 'webmex_disk_space')->update(array(
				'value' => trim($_POST['webmex_disk_space'])
			));

			Core::$db->config()->where('name', 'webmex_plan_name')->update(array(
				'value' => trim($_POST['webmex_plan_name'])
			));

			Core::$db->config()->where('name', 'webmex_license_expiration')->update(array(
				'value' => trim(date('Y-m-d', strtotime($_POST['webmex_license_expiration'])))
			));

			flashMsg(__('msg_saved'));

			redirect(true);
		}

		$config = Core::config();

		$this->content = tpl('superadmin.latte', array(
			'config' => $config,
		));
	}
}