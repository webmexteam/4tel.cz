<?php

defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */
class Controller_Import extends AdminController
{

	protected $apps = array(
		'qc' => 'Quick.Cart',
		'panavis' => 'Panavis',
		'osc' => 'OS Commerce',
		'prestashop' => 'Prestashop'
	);

	public function __construct()
	{
		parent::__construct();

		gatekeeper('import');
	}

	public function index()
	{
		if (!empty($_POST['import_app'])) {
			$_SESSION['import_app'] = $_POST['import_app'];

			if (in_array($_POST['import_app'], array('qc', 'osc', 'panavis', 'prestashop'))) {
				redirect('admin/import/import_path');
			} else {
				redirect('admin/import/import_database');
			}
		}

		unset($_SESSION['import_app']);

		$this->content = tpl('import/index.latte', array(
			'apps' => $this->apps
				));
	}

	public function import_database()
	{
		$error = null;
		$conn = false;
		$mysql = array(
			'server' => 'localhost'
		);

		if (!empty($_POST['mysql'])) {
			$mysql = array_merge($mysql, $_POST['mysql']);

			if (empty($mysql['server'])) {
				$error = 'Unknown server';
			}

			if (empty($mysql['dbname'])) {
				$error = 'Unknown database';
			}

			if (!$error) {
				try {
					$conn = new PDO('mysql:host=' . $mysql['server'] . ';dbname=' . $mysql['dbname'] . (!empty($mysql['port']) ? ';port=' . $mysql['port'] : ''), $mysql['username'], $mysql['password']);
				} catch (PDOException $e) {
					$error = $e->getMessage();
				}
			}

			if ($conn) {
				$_SESSION['import_database'] = $mysql;
				redirect('admin/import/import_' . $_SESSION['import_app']);
			}
		}

		$this->content = tpl('import/database.latte', array(
			'error' => $error,
			'mysql' => $mysql
				));
	}

	public function import_path()
	{
		$default_path = '/';
		$error = null;

		if ($_SESSION['import_app'] == 'qc') {
			$default_path = '/qc/';
		}

		if ($_SESSION['import_app'] == 'panavis') {
			$default_path = '/panavis/';
		}

		if ($_SESSION['import_app'] == 'prestashop') {
			$default_path = '/prestashop/';
		}

		if ($_SESSION['import_app'] == 'osc') {
			$default_path = '/osc/';
		}

		if (!empty($_POST['path'])) {
			$path = DOCROOT . ltrim(trim($_POST['path'], '/') . '/', '/');

			if ($_SESSION['import_app'] == 'qc' || $_SESSION['import_app'] == 'panavis') {
				$data = $this->detect_qc($path, $error);
			} else if ($_SESSION['import_app'] == 'osc') {
				$data = $this->detect_osc($path, $error);
			} else if ($_SESSION['import_app'] == 'prestashop') {
				$data = $this->detect_prestashop($path, $error);
			}

			if ($data && !$error) {
				$_SESSION['import_path'] = $path;
				redirect('admin/import/import_' . $_SESSION['import_app']);
			} else if (!$error) {
				$error = __('import_app_not_found') . ' (' . $path . ')';
			}
		}

		$this->content = tpl('import/path.latte', array(
			'default_path' => $default_path,
			'error' => $error
				));
	}

	private function detect_prestashop($path, & $error = null)
	{
		if (is_dir($path . 'config')) {
			@include($path . 'config/settings.inc.php');

			if (!defined('_PS_VERSION_')) {
				return null;
			}

			$data = array(
				'config' => array(
					'path_images' => $path . 'img/p'
				),
				'db' => array(
					'server' => _DB_SERVER_,
					'username' => _DB_USER_,
					'password' => _DB_PASSWD_,
					'dbname' => _DB_NAME_,
					'prefix' => _DB_PREFIX_
				),
				'version' => _PS_VERSION_
			);

			try {
				$conn = new PDO('mysql:host=' . _DB_SERVER_ . ';dbname=' . _DB_NAME_, _DB_USER_, _DB_PASSWD_);
				$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
				$conn->query('SET NAMES UTF8');

				$db = new NotORM($conn);

				$db->table_prefix = _DB_PREFIX_;

				if ($configuration = $db->configuration()) {
					foreach ($configuration as $cfg) {
						$data['config'][$cfg['name']] = $cfg['value'];
					}
				}
			} catch (PDOException $e) {
				$error = $e->getMessage();
			}

			return $data;
		}

		return null;
	}

	private function detect_osc($path, & $error = null)
	{
		if (is_dir($path . 'includes')) {
			@include($path . 'includes/configure.php');

			if (!defined('DB_SERVER')) {
				return null;
			}

			preg_match('/osCommerce Online Merchant v([\d\.]+)/miu', file_get_contents($path . 'includes/application_top.php'), $m);

			if (empty($m)) {
				return null;
			}

			$data = array(
				'config' => array(
					'path_images' => $path . ltrim(DIR_WS_IMAGES, '/')
				),
				'db' => array(
					'server' => DB_SERVER,
					'username' => DB_SERVER_USERNAME,
					'password' => DB_SERVER_PASSWORD,
					'dbname' => DB_DATABASE
				),
				'version' => $m[1]
			);

			try {
				$conn = new PDO('mysql:host=' . DB_SERVER . ';dbname=' . DB_DATABASE, DB_SERVER_USERNAME, DB_SERVER_PASSWORD);
				$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
				$conn->query('SET NAMES UTF8');

				$db = new NotORM($conn);

				if ($configuration = $db->configuration()) {
					foreach ($configuration as $cfg) {
						$data['config'][$cfg['configuration_key']] = $cfg['configuration_value'];
					}
				}
			} catch (PDOException $e) {
				$error = $e->getMessage();
			}

			return $data;
		}

		return null;
	}

	private function detect_qc($path, & $error = null)
	{
		if (is_dir($path . 'config')) {
			$old_path = set_include_path(DOCROOT . 'core/');

			@include($path . 'config/general.php');

			set_include_path($old_path);

			if (empty($config) || empty($config['version'])) {
				return null;
			}

			$data = array(
				'config' => $config,
				'version' => $config['version'],
				'version_spec' => $config['version']
			);

			if (isSet($config['base'])) {
				$data['version_spec'] = 'h2_' . $config['version'];
			}

			if (file_exists($path . 'config/database.php')) {
				$data['version_spec'] = 'db_' . $data['version_spec'];
			}

			return $data;
		}

		return null;
	}

	////////////////////////////

	public function import_qc()
	{
		if (empty($_SESSION['import_path'])) {
			redirect('admin/import');
		}

		$data = $this->detect_qc($_SESSION['import_path']);

		if ($data && !empty($_POST)) {
			$this->tpl = 'result.latte';

			$this->do_import_qc(array(
				'path' => $_SESSION['import_path'],
				'version' => $data['version_spec'],
				'language' => $data['config']['default_lang'],
				'import_config' => isSet($_POST['import_config']),
				'import_pages' => isSet($_POST['import_pages']),
				'import_products' => isSet($_POST['import_products']),
				'import_orders' => isSet($_POST['import_orders']),
				'import_files' => isSet($_POST['import_files']),
				'import_invoices' => isSet($_POST['import_invoices']),
				'delete_data' => isSet($_POST['delete_data'])
			));
		} else {
			$this->content = tpl('import/qc.latte', array(
				'data' => $data
					));
		}
	}

	public function import_panavis()
	{
		if (empty($_SESSION['import_path'])) {
			redirect('admin/import');
		}

		$data = $this->detect_qc($_SESSION['import_path']);

		if ($data && !empty($_POST)) {
			$this->tpl = 'result.latte';

			$this->do_import_panavis(array(
				'path' => $_SESSION['import_path'],
				'version' => $data['version_spec'],
				'import_config' => isSet($_POST['import_config']),
				'import_pages' => isSet($_POST['import_pages']),
				'import_products' => isSet($_POST['import_products']),
				'import_orders' => isSet($_POST['import_orders']),
				'import_files' => isSet($_POST['import_files']),
				'import_invoices' => isSet($_POST['import_invoices']),
				'delete_data' => isSet($_POST['delete_data'])
			));
		} else {
			$this->content = tpl('import/qc.latte', array(
				'data' => $data,
				'panavis' => 1
					));
		}
	}

	public function import_osc()
	{
		if (empty($_SESSION['import_path'])) {
			redirect('admin/import');
		}

		$data = $this->detect_osc($_SESSION['import_path']);

		if ($data && !empty($_POST)) {
			$this->tpl = 'result.latte';

			$conn = new PDO('mysql:host=' . $data['db']['server'] . ';dbname=' . $data['db']['dbname'], $data['db']['username'], $data['db']['password']);
			$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$conn->query('SET NAMES UTF8');
			$db = new NotORM($conn);

			$this->do_import_osc(array(
				'path' => $_SESSION['import_path'],
				'connection' => $conn,
				'db' => $db,
				'version' => $data['version'],
				'config' => $data['config'],
				'import_config' => isSet($_POST['import_config']),
				'import_pages' => isSet($_POST['import_pages']),
				'import_products' => isSet($_POST['import_products']),
				'import_orders' => isSet($_POST['import_orders']),
				'import_files' => isSet($_POST['import_files']),
				'delete_data' => isSet($_POST['delete_data'])
			));
		} else {
			$this->content = tpl('import/osc.latte', array(
				'data' => $data
					));
		}
	}

	public function import_prestashop()
	{
		if (empty($_SESSION['import_path'])) {
			redirect('admin/import');
		}

		$data = $this->detect_prestashop($_SESSION['import_path']);

		if ($data && !empty($_POST)) {
			$this->tpl = 'result.latte';

			$conn = new PDO('mysql:host=' . $data['db']['server'] . ';dbname=' . $data['db']['dbname'], $data['db']['username'], $data['db']['password']);
			$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$conn->query('SET NAMES UTF8');
			$db = new NotORM($conn);

			$db->table_prefix = $data['db']['prefix'];

			$this->do_import_prestashop(array(
				'path' => $_SESSION['import_path'],
				'connection' => $conn,
				'db' => $db,
				'version' => $data['version'],
				'config' => $data['config'],
				'import_config' => isSet($_POST['import_config']),
				'import_pages' => isSet($_POST['import_pages']),
				'import_products' => isSet($_POST['import_products']),
				'import_orders' => isSet($_POST['import_orders']),
				'import_files' => isSet($_POST['import_files']),
				'delete_data' => isSet($_POST['delete_data'])
			));
		} else {
			$this->content = tpl('import/prestashop.latte', array(
				'data' => $data
					));
		}
	}

	private function deleteData()
	{
		Core::$db_conn->query('DELETE FROM `page`; DELETE FROM `page_files`');
		Core::$db_conn->query('DELETE FROM `product`; DELETE FROM `product_files`; DELETE FROM `product_attributes`; DELETE FROM `product_discounts`; DELETE FROM `product_pages`');
		Core::$db_conn->query('DELETE FROM `order`; DELETE FROM `order_products`; DELETE FROM `order_comments`;');
		Core::$db_conn->query('DELETE FROM `redirect`');
	}

	private function do_import_prestashop(array $opt)
	{
		$options = array_merge(array(
			'path' => null,
			'connection' => null,
			'db' => null,
			'version' => null,
			'config' => null,
			'import_config' => false,
			'import_pages' => false,
			'import_products' => false,
			'import_orders' => false,
			'import_files' => false,
			'delete_data' => false
				), $opt);

		extract($options);

		@set_time_limit(0);
		$max_time = (int) ini_get('max_execution_time');

		$log = '';

		if ($delete_data) {
			$this->deleteData();
		}

		$lang_id = $config['PS_LANG_DEFAULT'];

		if ($import_config) {
			$log .= 'IMPORT CONFIG... ';

			Core::$db->config()->where('name', 'store_name')->update(array('value' => $config['PS_SHOP_NAME']));
			Core::$db->config()->where('name', 'email_sender')->update(array('value' => $config['PS_SHOP_EMAIL']));
			Core::$db->config()->where('name', 'email_notify')->update(array('value' => $config['PS_SHOP_EMAIL']));
		}

		// categories
		$pages_map = array();

		if ($import_pages) {
			$parents = array();

			$log .= "IMPORT PAGES... \n";
			if ($categories = $db->category()) {
				foreach ($categories as $category) {
					if ($locale = $db->category_lang()->where('id_category', $category['id_category'])->where('id_lang', $lang_id)->fetch()) {
						$page_id = Core::$db->page(array(
							'parent_page' => 0,
							'status' => $category['active'],
							'position' => 0,
							'menu' => 2,
							'sef_url' => dirify($locale['link_rewrite']),
							'name' => $locale['name'],
							'description_short' => '',
							'description' => $locale['description'],
							'products' => (int) 1,
							'subpages' => 'list',
							'title' => $locale['name']
								));

						$pages_map[(int) $category['id_category']] = $page_id;

						if ($page_id && (int) $category['id_parent']) {
							$parents[$page_id] = $category['id_parent'];
						}

						$log .= "PAGE " . $locale['name'] . " IMPORTED\n";
					}
				}

				foreach ($parents as $page_id => $parent_id) {
					Core::$db->page()->where('id', $page_id)->update(array(
						'parent_page' => $pages_map[(int) $parent_id],
						'menu' => null
					));
				}
			}
		}

		// products
		$products_map = array();
		$products_vat = array();
		$availabilities = array();
		$vat_rates = array();

		if ($import_products) {
			$log .= "IMPORT PRODUCTS... \n";

			foreach (Core::$db->availability() as $av) {
				$availabilities[$av['name']] = $av['id'];
			}

			foreach (Core::$db->vat() as $vat) {
				$vat_rates[(float) $vat['rate']] = $vat['id'];
			}

			if ($products = $db->product()) {
				foreach ($products as $product) {

					if ($locale = $db->product_lang()->where('id_product', $product['id_product'])->where('id_lang', $lang_id)->fetch()) {
						$odata = array(
							'name' => $locale['name'],
							'sku' => $product['reference'],
							'ean13' => $product['ean13'],
							'productno' => $product['supplier_reference'],
							'status' => (int) $product['active'],
							'position' => 0,
							'description_short' => $locale['description_short'],
							'description' => $locale['description'],
							'price' => (float) $product['price'],
							'cost' => $product['wholesale_price'] ? (float) $product['wholesale_price'] : null,
							'discount' => $product['discount'] ? (float) $product['discount'] : null,
							'discount_date' => $product['reduction_to'] ? strtotime($product['reduction_to']) : null,
							'weight' => $product['weight'] ? (float) $product['weight'] : null,
							'sef_url' => dirify($locale['link_rewrite']),
							'stock' => !empty($product['quantity']) ? (float) $product['quantity'] : null,
							'default_page' => $pages_map[$product['id_category_default']]
						);

						if (isset($availabilities[$locale['available_now']])) {
							$odata['availability_id'] = $availabilities[$locale['available_now']];
						} else if ($locale['available_now']) {
							$odata['availability_id'] = Core::$db->availability(array(
								'name' => $locale['available_now']
									));

							$availabilities[$locale['available_now']] = $odata['availability_id'];
						}

						$rate = $db->tax()->where('id_tax', $product['id_tax'])->fetch();

						$rate = (float) $rate['rate'];

						if (isset($vat_rates[$rate])) {
							$odata['vat_id'] = $vat_rates[$rate];
						} else if ($product['id_tax']) {
							$odata['vat_id'] = Core::$db->vat(array(
								'rate' => $rate
									));

							$vat_rates[$rate] = $odata['vat_id'];
						}

						$product_id = Core::$db->product($odata);

						$log .= "PRODUCT " . $locale['name'] . " IMPORTED\n";

						$products_map[(int) $product['id_product']] = $product_id;

						if ($import_files && ($files = $db->image()->where('id_product', $product['id_product']))) {
							foreach ($files as $file) {
								$filename = $file['id_product'] . '-' . $file['id_image'] . '.jpg';

								$locale = $db->image_lang()->where('id_image', $file['id_image'])->where('id_lang', $lang_id)->fetch();

								Core::$db->product_files(array(
									'product_id' => $product_id,
									'filename' => 'files/' . $filename,
									'description' => $locale['legend'],
									'position' => $file['cover'] ? 0 : 1,
									'size' => -1,
									'is_image' => 1,
									'align' => 2
								));
							}
						}
					}
				}

				foreach ($db->category_product() as $pc) {
					Core::$db->product_pages(array(
						'page_id' => $pages_map[(int) $pc['id_category']],
						'product_id' => $products_map[(int) $pc['id_product']]
					));
				}
			}
		}

		$orders_map = array();

		$states = array(
			1 => 1,
			2 => 1,
			3 => 2,
			4 => 3,
			5 => 3,
			6 => 4,
			7 => 4,
			8 => 1,
			9 => 1,
			10 => 1,
			11 => 1,
		);

		if ($import_orders) {
			$log .= "IMPORT ORDERS... \n";

			if ($orders = $db->orders()) {
				$payments = array();

				foreach ($orders as $order) {

					$state = $db->order_history()->where('id_order', $order['id_order'])->order('date_add DESC')->fetch();
					$customer = $db->customer()->where('id_customer', $order['id_customer'])->fetch();
					$address = $db->address()->where('id_address', $order['id_address_invoice'])->fetch();
					$ship_address = null;
					$comment = $db->message()->where('id_order', $order['id_order'])->fetch();
					$delivery = $db->carrier()->where('id_carrier', $order['id_carrier'])->fetch();

					if ($order['id_address_delivery'] != $order['id_address_invoice']) {
						$ship_address = $db->address()->where('id_address', $order['id_address_delivery'])->fetch();
					}

					$name = explode(' ', $order['customers_name'], 2);

					if (!in_array($order['payment_method'], $payments)) {
						$payments[] = $order['payment_method'];
					}

					$odata = array(
						'received' => strtotime($order['date_add']),
						'ip' => '',
						'status' => $states[(int) $state['id_order_state']],
						'first_name' => $address['firstname'],
						'last_name' => $address['lastname'],
						'company' => $address['company'],
						'street' => trim($address['address1'] . ' ' . $address['address2']),
						'zip' => $address['postcode'],
						'city' => $address['city'],
						'country' => '',
						'phone' => $address['phone_mobile'] ? $address['phone_mobile'] : $address['phone'],
						'email' => $customer['email'],
						'comment' => $comment ? $comment['message'] : ''
					);

					if ($ship_address) {
						$odata = array_merge($odata, array(
							'ship_first_name' => $ship_address['firstname'],
							'ship_last_name' => $ship_address['lastname'],
							'ship_company' => $ship_address['company'],
							'ship_street' => trim($ship_address['address1'] . ' ' . $ship_address['address2']),
							'ship_zip' => $ship_address['postcode'],
							'ship_city' => $ship_address['city'],
							'ship_country' => ''
								));
					}

					$order_id = Core::$db->order($odata);

					$log .= "ORDER " . $order['id_order'] . " (" . $address['firstname'] . ' ' . $address['lastname'] . ") IMPORTED\n";

					$orders_map[(int) $order['id_order']] = $order_id;

					Core::$db->order_products(array(
						'order_id' => $order_id,
						'product_id' => null,
						'quantity' => 1,
						'price' => (float) $order['total_shipping'],
						'vat' => null,
						'name' => __('delivery') . ' [' . $order['payment'] . ', ' . $delivery['name'] . ']'
					));
				}

				foreach ($db->order_detail() as $product) {
					Core::$db->order_products(array(
						'order_id' => $orders_map[(int) $product['id_order']],
						'product_id' => @$products_map[(int) $product['product_id']],
						'quantity' => (int) $product['product_quantity'],
						'price' => (float) $product['product_price'],
						'vat' => (float) $product['tax_rate'],
						'name' => $product['product_name'],
						'sku' => $product['product_reference']
					));
				}
			}
		}

		if ($import_files) {
			$this->tpl = null;

			$_SESSION['import_files_path'] = $config['path_images'];

			$_SESSION['import_data'] = array(
				'app' => 'prestashop',
				'log' => $log
			);

			redirect('admin/import/copyfiles');
		}

		$this->content = $log;
	}

	private function do_import_osc(array $opt)
	{
		$options = array_merge(array(
			'path' => null,
			'connection' => null,
			'db' => null,
			'version' => null,
			'config' => null,
			'import_config' => false,
			'import_pages' => false,
			'import_products' => false,
			'import_orders' => false,
			'import_files' => false,
			'delete_data' => false
				), $opt);

		extract($options);

		@set_time_limit(0);
		$max_time = (int) ini_get('max_execution_time');

		$log = '';

		if ($delete_data) {
			$this->deleteData();
		}

		$lang_id = null;

		if ($lang = $db->languages()->order('sort_order ASC')->fetch()) {
			$lang_id = $lang['languages_id'];
		}

		$vat_map = array();

		if ($import_products || $import_orders) {
			foreach ($db->tax_rates() as $rate) {
				if ($vat = Core::$db->vat()->where('rate', $rate['tax_rate'])->fetch()) {
					$vat_id = $vat['id'];
				} else {
					$vat_id = Core::$db->vat(array(
						'name' => $rate['tax_description'],
						'rate' => (float) $rate['tax_rate']
							));
				}

				$vat_map[(int) $rate['tax_rates_id']] = $vat_id;
			}
		}

		if ($import_config) {
			$log .= 'IMPORT CONFIG... ';

			Core::$db->config()->where('name', 'store_name')->update(array('value' => $config['STORE_NAME']));
			Core::$db->config()->where('name', 'email_sender')->update(array('value' => $config['EMAIL_FROM']));
			Core::$db->config()->where('name', 'email_notify')->update(array('value' => $config['SEND_EXTRA_ORDER_EMAILS_TO']));
		}

		// categories
		$pages_map = array();

		if ($import_pages) {
			$parents = array();

			$log .= "IMPORT PAGES... \n";
			if ($categories = $db->categories()) {
				foreach ($categories as $category) {
					if ($data = $db->categories_description()->where('categories_id', $category['categories_id'])->where('language_id', $lang_id)->fetch()) {

						$page_id = Core::$db->page(array(
							'parent_page' => 0,
							'status' => 1,
							'position' => (int) $category['sort_order'],
							'menu' => 2,
							'sef_url' => dirify($data['categories_name']),
							'name' => $data['categories_name'],
							'description_short' => '',
							'products' => (int) 1,
							'subpages' => 'list',
							'title' => $data['categories_name']
								));

						$pages_map[(int) $category['categories_id']] = $page_id;

						if ($page_id && (int) $category['parent_id']) {
							$parents[$page_id] = $category['parent_id'];
						}

						$log .= "PAGE " . $data['categories_name'] . " IMPORTED\n";
					}
				}

				foreach ($parents as $page_id => $parent_id) {
					Core::$db->page()->where('id', $page_id)->update(array(
						'parent_page' => $pages_map[(int) $parent_id],
						'menu' => null
					));
				}
			}
		}

		// products
		$products_map = array();
		$products_vat = array();

		if ($import_products) {
			$log .= "IMPORT PRODUCTS... \n";

			if ($products = $db->products()) {
				foreach ($products as $product) {
					if ($data = $db->products_description()->where('products_id', $product['products_id'])->where('language_id', $lang_id)->fetch()) {
						$desc = $data['products_description'];

						$odata = array(
							'name' => $data['products_name'],
							'sku' => $product['products_model'],
							'status' => (int) $product['products_status'],
							'position' => 0,
							'description_short' => $this->limitWords(strip_tags($desc), 10),
							'description' => $desc,
							'price' => (float) $product['products_price'],
							'weight' => $product['products_weight'] ? (float) $product['products_weight'] : null,
							'sef_url' => dirify($data['products_name']),
							'availability_id' => 1,
							'stock' => !empty($product['products_quantity']) ? (float) $product['products_quantity'] : null,
							'vat_id' => @$vat_map[(int) $product['products_tax_class_id']]
						);

						$product_id = Core::$db->product($odata);

						$products_vat[$product_id] = @$vat_map[(int) $product['products_tax_class_id']];

						$log .= "PRODUCT " . $data['products_name'] . " IMPORTED\n";

						$products_map[(int) $product['products_id']] = $product_id;

						if ($import_files && $product['products_image']) {
							$img = $config['path_images'] . $product['products_image'];

							if (file_exists($img)) {
								Core::$db->product_files(array(
									'product_id' => $product_id,
									'filename' => 'files/' . $product['products_image'],
									'description' => '',
									'position' => 0,
									'size' => -1,
									'is_image' => 1,
									'align' => 2
								));
							}
						}
					}
				}

				foreach ($db->products_to_categories() as $pc) {
					Core::$db->product_pages(array(
						'page_id' => $pages_map[(int) $pc['categories_id']],
						'product_id' => $products_map[(int) $pc['products_id']]
					));
				}
			}
		}

		if ($import_orders) {
			// payments
			$payment_map = array();

			$log .= "IMPORT PAYMENTS... \n";

			if ($payments = $db->orders()->select('DISTINCT payment_method')) {
				foreach ($payments as $payment) {
					if ($p = Core::$db->payment()->where('name', $payment['payment_method'])->fetch()) {
						$payment_id = $p['id'];
					} else {
						$payment_id = Core::$db->payment(array(
							'name' => $payment['payment_method']
								));

						$log .= "PAYMENT " . $payment['payment_method'] . " IMPORTED\n";
					}

					$payment_map[$payment['payment_method']] = $payment_id;
				}
			}
		}

		// orders
		$orders_map = array();

		if ($import_orders) {
			$log .= "IMPORT ORDERS... \n";

			if ($orders = $db->orders()) {
				$payments = array();

				foreach ($orders as $order) {

					$name = explode(' ', $order['customers_name'], 2);

					if (!in_array($order['payment_method'], $payments)) {
						$payments[] = $order['payment_method'];
					}

					$odata = array(
						'received' => strtotime($order['date_purchased']),
						'ip' => '',
						'status' => (int) $order['orders_status'],
						'first_name' => $name[0],
						'last_name' => $name[1],
						'company' => $order['customers_company'],
						'street' => $order['customers_street_address'],
						'zip' => $order['customers_postcode'],
						'city' => $order['customers_city'],
						'country' => $order['customers_state'],
						'phone' => $order['customers_telephone'],
						'email' => $order['customers_email_address'],
						'payment_id' => $payment_map[$order['payment_method']]
					);

					if (!empty($order['delivery_name'])) {
						$name = explode(' ', $order['delivery_name'], 2);

						$odata = array_merge($odata, array(
							'ship_first_name' => $name[0],
							'ship_last_name' => $name[1],
							'ship_company' => $order['delivery_company'],
							'ship_street' => $order['delivery_street_address'],
							'ship_zip' => $order['delivery_postcode'],
							'ship_city' => $order['delivery_city'],
							'ship_country' => $order['delivery_state']
								));
					}

					$order_id = Core::$db->order($odata);

					$log .= "ORDER " . $order['orders_id'] . " (" . $order['customers_name'] . ") IMPORTED\n";

					$orders_map[(int) $order['orders_id']] = $order_id;
				}

				foreach ($db->orders_products() as $product) {
					Core::$db->order_products(array(
						'order_id' => $orders_map[(int) $product['orders_id']],
						'product_id' => @$products_map[(int) $product['products_id']],
						'quantity' => (int) $product['products_quantity'],
						'price' => (float) $product['products_price'],
						'vat' => $products_vat[$orders_map[(int) $product['orders_id']]],
						'name' => $product['products_name']
					));
				}
			}
		}

		if ($import_files) {
			$this->tpl = null;

			$_SESSION['import_files_path'] = $config['path_images'];

			$_SESSION['import_data'] = array(
				'log' => $log
			);

			redirect('admin/import/copyfiles');
		}

		$this->content = $log;
	}

	private function do_import_panavis(array $opt)
	{
		$options = array_merge(array(
			'path' => null,
			'version' => null,
			'import_config' => false,
			'import_pages' => false,
			'import_products' => false,
			'import_orders' => false,
			'import_files' => false,
			'import_invoices' => false,
			'delete_data' => false
				), $opt);

		extract($options);

		@set_time_limit(0);
		$max_time = (int) ini_get('max_execution_time');

		$log = '';

		$old_path = set_include_path(DOCROOT . 'core/');
		include($path . 'config/general.php');
		set_include_path($old_path);

		if ($delete_data) {
			$this->deleteData();
		}

		if ($import_config) {
			$log .= 'IMPORT CONFIG... ';
			// import config
			Core::$db->config()->where('name', 'title')->update(array('value' => '%s | ' . $config['title']));
			Core::$db->config()->where('name', 'description')->update(array('value' => $config['description']));
			Core::$db->config()->where('name', 'keywords')->update(array('value' => $config['keywords']));

			Core::$db->config()->where('name', 'email_sender')->update(array('value' => $config['email']));
			Core::$db->config()->where('name', 'email_notify')->update(array('value' => $config['email']));

			Core::$db->config()->where('name', 'currency')->update(array('value' => $config['currency_symbol']));

			$log .= "OK\n";
		}

		// import data
		if ($import_pages) {
			$log .= "IMPORT PAGES... \n";

			$data = file($path . 'db/categories.php');

			$parents = array();
			$pages_map = array();

			foreach ($data as $line) {
				$exp = explode('$', iconv('windows-1250', 'utf-8', $line));
				$page = array(
					'iCategory' => $exp[2],
					'iPosition' => $exp[0],
					'sName' => $exp[4],
					'iType' => $exp[1],
					'iParent' => $exp[3],
					'sShortDescription' => $exp[5],
					'sDescription' => $exp[6]
				);

				if ($page) {
					$desc = preg_replace('/\|n\|/miu', "\n", $page['sShortDescription']);

					if (trim($desc) == '<p>&#160;</p>') {
						$desc = '';
					}

					$desclong = preg_replace('/\|n\|/miu', "\n", $page['sDescription']);

					if (trim($desclong) == '<p>&#160;</p>') {
						$desclong = '';
					}

					$page_id = Core::$db->page(array(
						'parent_page' => 0,
						'status' => 1,
						'position' => (int) $page['iPosition'],
						'menu' => ($page['iType'] == 1 ? 2 : 5),
						'sef_url' => dirify($page['sName']),
						'name' => $page['sName'],
						'description' => $desclong,
						'description_short' => $desc,
						'products' => ($page['iType'] == 1 ? 1 : 0),
						'subpages' => 'list',
						'title' => $page['sName']
							));

					$log .= "PAGE " . $page['sName'] . " IMPORTED\n";

					$pages_map[$page['iCategory']] = $page_id;

					if ($page_id && (int) $page['iParent']) {
						$parents[$page_id] = $page['iParent'];
					}
				}
			}

			foreach ($parents as $page_id => $parent_id) {
				Core::$db->page()->where('id', $page_id)->update(array(
					'parent_page' => $pages_map[(int) $parent_id],
					'menu' => null
				));
			}

			unset($data);

			// pages FILES
			$data = file($path . 'db/categories_files.php');

			foreach ($data as $line) {
				$exp = explode('$', iconv('windows-1250', 'utf-8', $line));
				$file = array();

				list($file['id'], $file['iCategory'], $file['file'], $file['description'], $file['type']) = $exp;

				if ($file) {
					if (!empty($pages_map[(int) $file['iCategory']])) {
						Core::$db->page_files(array(
							'page_id' => $pages_map[(int) $file['iCategory']],
							'filename' => 'files/categories/' . $file['file'],
							'description' => $file['description'],
							'position' => 0,
							'size' => -1,
							'is_image' => (int) $file['type'],
							'align' => 2
						));
					}
				}
			}

			$log .= "IMPORTED " . count($pages_map) . " PAGES\n";
			$log .= "OK\n";
		}

		if ($import_products) {
			$log .= "IMPORT PRODUCTS... \n";
			// products

			$data = file($path . 'db/products.php');

			$products_map = array();

			foreach ($data as $line) {
				$exp = explode('$', iconv('windows-1250', 'utf-8', $line));
				$product = array(
					'name' => trim($exp[1]),
					'id' => $exp[2],
					'price' => $exp[4],
					'description_short' => $exp[5],
					'price_old' => $exp[6],
					'stock' => $exp[8],
					'category_id' => $exp[3],
					'status' => $exp[7],
				);

				if (!$product || !$product['name']) {
					continue;
				}

				$odata = array(
					'name' => $product['name'],
					'status' => (int) $product['status'],
					'position' => 0,
					'description_short' => $product['description_short'],
					'price' => (float) $product['price'],
					'vat_id' => (int) Core::config('vat_rate_default'),
					'price_old' => !empty($product['price_old']) ? (float) $product['price_old'] : null,
					'sef_url' => dirify($product['name']),
					'availability_id' => 1
				);

				if (!preg_match('/\>/', $product['stock']) && (int) $product['stock'] >= 0) {
					$odata['stock'] = (int) $product['stock'];
				}

				$product_id = Core::$db->product($odata);
				$products_map[$product['id']] = $product_id;

				$log .= "PRODUCT " . $product['name'] . " IMPORTED\n";

				if ($product['category_id'] && !empty($pages_map[$product['category_id']])) {
					Core::$db->product_pages(array(
						'product_id' => $product_id,
						'page_id' => $pages_map[$product['category_id']]
					));
				}

				unset($data);

				$data = file($path . 'db/products_content.php');

				foreach ($data as $line) {
					$exp = explode('$', iconv('windows-1250', 'utf-8', $line));

					if ($products_map[$exp[0]]) {
						Core::$db->product()->where('id', $products_map[$exp[0]])->update(array(
							'description' => $exp[1]
						));
					}
				}

				unset($data);

				$data = file($path . 'db/products_files.php');

				foreach ($data as $line) {
					$exp = explode('$', iconv('windows-1250', 'utf-8', $line));
					$file = array();

					list($file['id'], $file['iProduct'], $file['file'], $file['description'], $file['type']) = $exp;

					if ($file) {
						if (!empty($products_map[(int) $file['iProduct']])) {
							if (!($f = Core::$db->product_files()->where('product_id', $products_map[(int) $file['iProduct']])->where('filename', 'files/products/' . $file['file'])->fetch())) {
								Core::$db->product_files(array(
									'product_id' => $products_map[(int) $file['iProduct']],
									'filename' => 'files/products/' . $file['file'],
									'description' => $file['description'],
									'position' => 0,
									'size' => -1,
									'is_image' => (int) $file['type'],
									'align' => 2
								));
							} else {
								$f->releaseMem();
							}
						}
					}
				}
			}

			if ($import_files) {
				$this->tpl = null;

				$_SESSION['import_files_path'] = $path . 'files/';

				$_SESSION['import_data'] = array(
					'log' => $log
				);

				redirect('admin/import/copyfiles');
			}
		}

		$this->content = $log;
	}

	private function do_import_qc(array $opt)
	{
		$options = array_merge(array(
			'path' => null,
			'version' => null,
			'language' => null,
			'import_config' => false,
			'import_pages' => false,
			'import_products' => false,
			'import_orders' => false,
			'import_files' => false,
			'import_invoices' => false,
			'delete_data' => false
				), $opt);

		extract($options);

		@set_time_limit(0);
		$max_time = (int) ini_get('max_execution_time');

		$log = '';
		$is_h2 = (bool) preg_match('/h2_/', $version);
		$is_h2_db = (bool) preg_match('/^db_h2_/', $version);

		$old_path = set_include_path(DOCROOT . 'core/');
		include($path . 'config/general.php');
		include($path . 'config/lang_' . $language . '.php');

		if ($is_h2_db) {
			include($path . 'config/database.php');

			if (!($db = new PDO('mysql:host=' . $database['server'] . ';dbname=' . $database['db_name'], $database['user'], $database['password']))) {
				$is_h2_db = false;
			} else {
				$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
				$db->query('SET NAMES UTF8');
			}
		}

		set_include_path($old_path);

		if ($delete_data) {
			$this->deleteData();
		}

		if ($import_config) {
			$log .= 'IMPORT CONFIG... ';
			// import config
			Core::$db->config()->where('name', 'title')->update(array('value' => '%s | ' . $config['title']));
			Core::$db->config()->where('name', 'description')->update(array('value' => $config['description']));
			Core::$db->config()->where('name', 'keywords')->update(array('value' => $config['keywords']));

			Core::$db->config()->where('name', 'email_sender')->update(array('value' => $config['orders_email']));
			Core::$db->config()->where('name', 'email_notify')->update(array('value' => $config['orders_email']));

			Core::$db->config()->where('name', 'currency')->update(array('value' => $config['currency_symbol']));

			if ($is_h2) {
				Core::$db->config()->where('name', 'store_name')->update(array('value' => $config['shop_name']));

				if ($import_invoices) {
					Core::$db->config()->where('name', 'invoice_num')->update(array('value' => $config['invoice_num']));
				}
			}

			$log .= "OK\n";
		}

		// import data
		if (!$is_h2_db) {
			include($path . 'db/' . $language . '_pages.def.php');
			include($path . 'db/' . $language . '_pages_ext.def.php');
			include($path . 'db/' . $language . '_pages_files.def.php');
			include($path . 'db/' . $language . '_products.def.php');
			include($path . 'db/' . $language . '_products_ext.def.php');
			include($path . 'db/' . $language . '_products_files.def.php');
			include($path . 'db/' . $language . '_products_pages.def.php');
			include($path . 'db/' . $language . '_carriers.def.php');
			include($path . 'db/' . $language . '_payments.def.php');
			include($path . 'db/' . $language . '_carriers_payments.def.php');
			include($path . 'db/orders.def.php');
			include($path . 'db/orders_products.def.php');
			include($path . 'db/orders_status.def.php');
			include($path . 'db/orders_comments.def.php');
		}

		if ($is_h2 && !$is_h2_db) {
			include($path . 'db/invoices.def.php');
			include($path . 'db/invoices_items.def.php');
		}

		if ($import_pages) {
			$log .= "IMPORT PAGES... \n";
			// pages
			if (!$is_h2_db) {
				$data = file($path . 'db/' . $language . '_pages.php');
				$fn = $language . '_pages';
			} else {
				$data = $db->query('SELECT * FROM `pages`');
				$data->setFetchMode(PDO::FETCH_ASSOC);
			}

			$parents = array();
			$pages_map = array();

			foreach ($data as $line) {
				if (!$is_h2_db) {
					$exp = explode('$', $line);
				}

				if ($is_h2_db || count($exp) > 1) {
					if (!$is_h2_db) {
						$page = $fn($exp);
					} else {
						$page = $line;
					}

					$desc = preg_replace('/\|n\|/miu', "\n", $page['sDescriptionShort']);

					if (trim($desc) == '<p>&#160;</p>') {
						$desc = '';
					}

					$page_id = Core::$db->page(array(
						'parent_page' => 0,
						'status' => (int) $page['iStatus'],
						'position' => (int) $page['iPosition'],
						'menu' => $this->qc_getMenu($page['iType']),
						'sef_url' => $is_h2 ? $page['sLink'] : dirify($page['sName']),
						'name' => $page['sName'],
						'description_short' => $desc,
						'products' => (int) $page['iProducts'],
						'subpages' => (int) $page['iSubpagesShow'] == 2 ? 'list_photo' : 'list',
						'title' => $page['sNameTitle']
							));

					$url = '?' . ($config['language_in_url'] ? $language . '_' : '') . $this->qc_dirify($page['sName']) . ',' . $page['iPage'];

					if ($is_h2) {
						$url = $page['sLink'] . $config['url_postfix'];
					}

					Core::$db->redirect(array(
						'url' => $url,
						'object_type' => 'page',
						'object_id' => $page_id
					));

					$log .= "PAGE " . $page['sName'] . " IMPORTED\n";

					$pages_map[(int) $page['iPage']] = $page_id;

					if ($page_id && (int) $page['iPageParent']) {
						$parents[$page_id] = $page['iPageParent'];
					}
				}
			}

			if ($delete_data && isSet($config['start_page'])) {
				Core::$db->config()->where('name', 'page_index')->update(array('value' => $pages_map[$config['start_page']]));
				Core::$db->config()->where('name', 'page_basket')->update(array('value' => $pages_map[$config['basket_page']]));
				Core::$db->config()->where('name', 'page_order_step1')->update(array('value' => $pages_map[$config['page_order_step1']]));
				Core::$db->config()->where('name', 'page_order_step2')->update(array('value' => $pages_map[$config['page_order_step2']]));
				Core::$db->config()->where('name', 'page_terms')->update(array('value' => $pages_map[$config['rules_page']]));
				Core::$db->config()->where('name', 'page_search')->update(array('value' => $pages_map[$config['page_search']]));
			}

			foreach ($parents as $page_id => $parent_id) {
				Core::$db->page()->where('id', $page_id)->update(array(
					'parent_page' => $pages_map[(int) $parent_id],
					'menu' => null
				));
			}

			unset($data);

			// pages EXT
			if (!$is_h2_db) {
				$data = file($path . 'db/' . $language . '_pages_ext.php');
				$fn = $language . '_pages_ext';
			} else {
				$data = $db->query('SELECT * FROM `pages_ext`');
				$data->setFetchMode(PDO::FETCH_ASSOC);
			}

			foreach ($data as $line) {
				if (!$is_h2_db) {
					$exp = explode('$', $line);
				}

				if ($is_h2_db || count($exp) > 1) {
					if (!$is_h2_db) {
						$page = $fn($exp);
					} else {
						$page = $line;
					}

					if ($db_page = Core::$db->page[$pages_map[(int) $page['iPage']]]) {
						$desc = preg_replace('/\|n\|/miu', "\n", $page['sDescriptionFull']);

						if (trim($desc) == '<p>&#160;</p>') {
							$desc = '';
						}

						$db_page->update(array(
							'description' => $desc,
							'meta_description' => $page['sMetaDescription'],
							'meta_keywords' => $page['sMetaKeywords'],
							'external_url' => $page['sUrl']
						));
					}
				}
			}

			unset($data);

			// pages FILES
			if (!$is_h2_db) {
				$data = file($path . 'db/' . $language . '_pages_files.php');
				$fn = $language . '_pages_files';
			} else {
				$data = $db->query('SELECT * FROM `pages_files`');
				$data->setFetchMode(PDO::FETCH_ASSOC);
			}

			foreach ($data as $line) {
				if (!$is_h2_db) {
					$exp = explode('$', $line);
				}

				if ($is_h2_db || count($exp) > 1) {
					if (!$is_h2_db) {
						$file = $fn($exp);
					} else {
						$file = $line;
					}

					if (!empty($pages_map[(int) $file['iPage']])) {
						Core::$db->page_files(array(
							'page_id' => $pages_map[(int) $file['iPage']],
							'filename' => 'files/' . $file['sFileName'],
							'description' => $file['sDescription'],
							'position' => (int) $file['iPosition'],
							'size' => -1,
							'is_image' => (int) $file['iPhoto'],
							'align' => (int) $file['iType'] == 1 ? 2 : 3
						));
					}
				}
			}

			$log .= "IMPORTED " . count($pages_map) . " PAGES\n";
			$log .= "OK\n";
		}

		unset($data);

		if ($import_products) {
			$log .= "IMPORT PRODUCTS... \n";
			// products
			if (!$is_h2_db) {
				$data = file($path . 'db/' . $language . '_products.php');
				$fn = $language . '_products';
			} else {
				$data = $db->query('SELECT * FROM `products`');
				$data->setFetchMode(PDO::FETCH_ASSOC);
			}

			$products_map = array();
			$availability_map = array();
			$availability = $availability_h2 = array();

			if ($is_h2) {
				$availability = $availability_h2 = explode(';', $config['availability']);
			} else {
				foreach ($data as $line) {
					if (!$is_h2_db) {
						$exp = explode('$', $line);
					}

					if ($is_h2_db || count($exp) > 1) {
						if (!$is_h2_db) {
							$product = $fn($exp);
						} else {
							$product = $line;
						}

						if ($product['sAvailable'] && !in_array($product['sAvailable'], $availability)) {
							$availability[] = $product['sAvailable'];
						}
					}
				}
			}

			foreach ($availability as $name) {
				if ($name && !($av = Core::$db->availability()->where('name', $name)->fetch())) {
					$availability_map[$name] = Core::$db->availability(array(
						'name' => $name
							));

					$log .= "AVAILABILITY " . $name . " IMPORTED\n";
				} else if ($name) {
					$availability_map[$name] = $av['id'];
				}
			}

			if ($is_h2_db) {
				unset($data);

				$data = $db->query('SELECT * FROM `products`');
				$data->setFetchMode(PDO::FETCH_ASSOC);
			}

			$ext_producers = false;
			$ext_producers_map = array();

			if ($import_pages && file_exists($path . 'db/' . $language . '_producers.def.php')) {
				$ext_producers = true;
			}

			foreach ($data as $line) {
				if (!$is_h2_db) {
					$exp = explode('$', $line);
				}

				if ($is_h2_db || count($exp) > 1) {
					if (!$is_h2_db) {
						$product = $fn($exp);
					} else {
						$product = $line;
					}

					$desc = preg_replace('/\|n\|/miu', "\n", $product['sDescriptionShort']);

					if (trim($desc) == '<p>&#160;</p>') {
						$desc = '';
					}

					if ($ext_producers && (int) $product['iProducer']) {
						$ext_producers_map[$product['iProduct']] = $product['iProducer'];
					}

					$odata = array(
						'name' => $product['sName'],
						'status' => (int) $product['iStatus'],
						'position' => (int) $product['iPosition'],
						'description_short' => $desc,
						'price' => (float) $product['fPrice'],
						'vat_id' => (int) Core::config('vat_rate_default'),
						'price_old' => !empty($product['fPriceOld']) ? (float) $product['fPriceOld'] : null,
						'sef_url' => $is_h2 ? $product['sLink'] : dirify($product['sName']),
						'availability_id' => @$availability_map[$is_h2 ? $availability_h2[(int) $product['sAvailable']] : $product['sAvailable']]
					);

					if ($is_h2 && notEmpty($product['aStock'])) {
						$odata['stock'] = (int) $product['aStock'];
					}

					if ($is_h2_db && notEmpty($product['sCode'])) {
						$odata['sku'] = $product['sCode'];
					}

					if ($product['pRecycle']) {
						$odata['recycling_fee'] = $product['pRecycle'];
					}

					if ($product['pAuthor']) {
						$odata['copyright_fee'] = $product['pAuthor'];
					}

					if (notEmpty($product['iWarranty'])) {
						$odata['guarantee'] = $product['iWarranty'];
					}

					if (notEmpty($product['rPrice'])) {
						$odata['price_old'] = $product['rPrice'];
					}

					$product_id = Core::$db->product($odata);

					$log .= "PRODUCT " . $product['sName'] . " IMPORTED\n";

					$url = '?' . $product['iProduct'] . ',' . ($config['language_in_url'] ? $language . '_' : '') . $this->qc_dirify($product['sName']);

					if ($is_h2) {
						$url = $product['sLink'] . $config['url_postfix'];
					}

					Core::$db->redirect(array(
						'url' => trim($url, '/'),
						'object_type' => 'product',
						'object_id' => $product_id
					));

					$products_map[(int) $product['iProduct']] = $product_id;

					if ($is_h2 && !empty($product['sVariant']) && !empty($product['sVariantValues'])) {

						foreach (explode(',', $product['sVariantValues']) as $i => $val) {
							$val = trim($val);

							if ($val) {
								Core::$db->product_attributes(array(
									'product_id' => $product_id,
									'name' => $product['sVariant'],
									'value' => $val,
									'is_default' => $i == 0 ? 1 : 0
								));
							}
						}
					}
				}
			}

			unset($data);


			// === EXT version ===
			// products attributes
			if ($import_products && file_exists($path . 'db/' . $language . '_products_attributes.def.php')) {
				include($path . 'db/' . $language . '_products_attributes.def.php');

				$data = file($path . 'db/' . $language . '_products_attributes.php');
				$fn = $language . '_products_attributes';

				foreach ($data as $line) {
					$exp = explode('$', $line);

					if (count($exp) > 1) {
						$attr = $fn($exp);

						Core::$db->product_attributes(array(
							'product_id' => $products_map[(int) $attr['iProduct']],
							'name' => $attr['sGroup'],
							'value' => $attr['sAttribute'],
							'price' => $attr['sPrice'],
							'is_default' => $attr['iDefault'] == 1 ? 1 : 0
						));
					}
				}

				unset($data);
			}

			// products stock
			if ($import_products && file_exists($path . 'db/' . $language . '_products_stock.def.php')) {
				include($path . 'db/' . $language . '_products_stock.def.php');

				$data = file($path . 'db/' . $language . '_products_stock.php');
				$fn = $language . '_products_stock';

				foreach ($data as $line) {
					$exp = explode('$', $line);

					if (count($exp) > 1) {
						$stock = $fn($exp);

						if ($product = Core::$db->product[$products_map[(int) $stock['iProduct']]]) {
							$product->update(array(
								'stock' => (int) $stock['fStock']
							));
						}
					}
				}

				unset($data);
			}

			// customers
			if ($import_orders && file_exists($path . 'db/users.def.php')) {
				include($path . 'db/users.def.php');

				$data = file($path . 'db/users.php');
				$fn = 'users';

				foreach ($data as $line) {
					$exp = explode('$', $line);

					if (count($exp) > 1) {
						$customer = $fn($exp);

						Core::$db->customer(array(
							'email' => $customer['sEmail'],
							'password' => sha1(sha1($customer['sPass'])),
							'first_name' => $customer['sFirstName'],
							'last_name' => $customer['sLastName'],
							'company' => $customer['sCompanyName'],
							'street' => $customer['sStreet'],
							'zip' => $customer['sZipCode'],
							'city' => $customer['sCity'],
							'phone' => $customer['sPhone'],
							'company_vat' => $customer['sNip'],
						));
					}
				}

				unset($data);
			}

			// producers
			if ($import_pages && !empty($ext_producers_map)) {
				include($path . 'db/' . $language . '_producers.def.php');

				$data = file($path . 'db/' . $language . '_producers.php');
				$fn = $language . '_producers';

				foreach ($data as $line) {
					$exp = explode('$', $line);

					if (count($exp) > 1) {
						$producer = $fn($exp);

						if ($page = Core::$db->page()->where('name', $producer['sName'])->where('menu', 3)->fetch()) {
							$producer_id = $page['id'];
						} else {
							$producer_id = Core::$db->page(array(
								'name' => $producer['sName'],
								'status' => 1,
								'menu' => 3,
								'parent_page' => 0,
								'sef_url' => dirify($producer['sName']),
								'products' => 1,
								'position' => 0
									));
						}

						if ($producer_id) {
							foreach ($ext_producers_map as $_product_id => $_producer_id) {
								if ($_producer_id == $producer['iProducer'] && !($product_page = Core::$db->product_pages()->where('product_id', $products_map[(int) $_product_id])->where('page_id', $producer_id)->fetch())) {
									Core::$db->product_pages(array(
										'product_id' => $products_map[(int) $_product_id],
										'page_id' => $producer_id
									));
								}
							}
						}
					}
				}

				unset($data);
			}

			// END EXT
			// products EXT
			if (!$is_h2_db) {
				$data = file($path . 'db/' . $language . '_products_ext.php');
				$fn = $language . '_products_ext';
			} else {
				$data = $db->query('SELECT * FROM `products_ext`');
				$data->setFetchMode(PDO::FETCH_ASSOC);
			}

			foreach ($data as $line) {
				if (!$is_h2_db) {
					$exp = explode('$', $line);
				}

				if ($is_h2_db || count($exp) > 1) {
					if (!$is_h2_db) {
						$ext = $fn($exp);
					} else {
						$ext = $line;
					}

					if ($product = Core::$db->product[$products_map[(int) $ext['iProduct']]]) {
						$desc = preg_replace('/\|n\|/miu', "\n", $ext['sDescriptionFull']);

						if (trim($desc) == '<p>&#160;</p>') {
							$desc = '';
						}

						$product->update(array(
							'description' => $desc,
							'title' => $ext['sNameTitle'],
							'meta_description' => $ext['sMetaDescription'],
							'meta_keywords' => $ext['sMetaKeywords']
						));
					}
				}
			}

			unset($data);

			// products FILES
			if (!$is_h2_db) {
				$data = file($path . 'db/' . $language . '_products_files.php');
				$fn = $language . '_products_files';
			} else {
				$data = $db->query('SELECT * FROM `products_files`');
				$data->setFetchMode(PDO::FETCH_ASSOC);
			}

			foreach ($data as $line) {
				if (!$is_h2_db) {
					$exp = explode('$', $line);
				}

				if ($is_h2_db || count($exp) > 1) {
					if (!$is_h2_db) {
						$file = $fn($exp);
					} else {
						$file = $line;
					}

					if (!empty($products_map[(int) $file['iProduct']])) {
						Core::$db->product_files(array(
							'product_id' => $products_map[(int) $file['iProduct']],
							'filename' => 'files/' . $file['sFileName'],
							'description' => $file['sDescription'],
							'position' => (int) $file['iPosition'],
							'size' => -1,
							'is_image' => (int) $file['iPhoto'],
							'align' => (int) $file['iType'] == 1 ? 2 : 3
						));
					}
				}
			}

			unset($data);

			// product PAGES
			if (!$is_h2_db) {
				$data = file($path . 'db/' . $language . '_products_pages.php');
				$fn = $language . '_products_pages';
			} else {
				$data = $db->query('SELECT * FROM `products_pages`');
				$data->setFetchMode(PDO::FETCH_ASSOC);
			}

			foreach ($data as $line) {
				if (!$is_h2_db) {
					$exp = explode('$', $line);
				}

				if ($is_h2_db || count($exp) > 1) {
					if (!$is_h2_db) {
						$product_page = $fn($exp);
					} else {
						$product_page = $line;
					}

					if (!empty($pages_map[(int) $product_page['iPage']]) && !empty($products_map[(int) $product_page['iProduct']])) {
						Core::$db->product_pages(array(
							'product_id' => $products_map[(int) $product_page['iProduct']],
							'page_id' => $pages_map[(int) $product_page['iPage']]
						));
					}
				}
			}

			$log .= "IMPORTED " . count($products_map) . " PRODUCTS\n";
			$log .= "OK\n";
		}

		unset($data);

		if ($import_config || $import_orders) {
			$log .= "IMPORT DELIVERIES & PAYMENTS... \n";
			// carriers
			if (!$is_h2_db) {
				$data = file($path . 'db/' . $language . '_carriers.php');
				$fn = $language . '_carriers';
			} else {
				$data = $db->query('SELECT * FROM `carriers`');
				$data->setFetchMode(PDO::FETCH_ASSOC);
			}

			$carriers_map = array();

			foreach ($data as $line) {
				if (!$is_h2_db) {
					$exp = explode('$', $line);
				}

				if ($is_h2_db || count($exp) > 1) {
					if (!$is_h2_db) {
						$carrier = $fn($exp);
					} else {
						$carrier = $line;
					}

					if (!($delivery = Core::$db->delivery()->where('name', $carrier['sName'])->fetch())) {
						$delivery_id = Core::$db->delivery(array(
							'name' => $carrier['sName'],
							'price' => (float) $carrier['fPrice']
								));

						$log .= "DELIVERY " . $carrier['sName'] . " IMPORTED\n";

						$carriers_map[(int) $carrier['iCarrier']] = $delivery_id;
					} else {
						$carriers_map[(int) $carrier['iCarrier']] = $delivery['id'];
					}
				}
			}

			unset($data);

			// payments
			if (!$is_h2_db) {
				$data = file($path . 'db/' . $language . '_payments.php');
				$fn = $language . '_payments';
			} else {
				$data = $db->query('SELECT * FROM `payments`');
				$data->setFetchMode(PDO::FETCH_ASSOC);
			}

			$payments_map = array();

			foreach ($data as $line) {
				if (!$is_h2_db) {
					$exp = explode('$', $line);
				}

				if ($is_h2_db || count($exp) > 1) {
					if (!$is_h2_db) {
						$payment = $fn($exp);
					} else {
						$payment = $line;
					}

					if (!($db_payment = Core::$db->payment()->where('name', $payment['sName'])->fetch())) {
						$payment_id = Core::$db->payment(array(
							'name' => $payment['sName']
								));

						$log .= "PAYMENT " . $payment['sName'] . " IMPORTED\n";

						$payments_map[(int) $payment['iPayment']] = $payment_id;
					} else {
						$payments_map[(int) $payment['iPayment']] = $db_payment['id'];
					}
				}
			}

			unset($data);

			// carriers - payments
			if (!$is_h2_db) {
				$data = file($path . 'db/' . $language . '_carriers_payments.php');
				$fn = $language . '_carriers_payments';
			} else {
				$data = $db->query('SELECT * FROM `carriers_payments`');
				$data->setFetchMode(PDO::FETCH_ASSOC);
			}

			foreach ($data as $line) {
				if (!$is_h2_db) {
					$exp = explode('$', $line);
				}

				if ($is_h2_db || count($exp) > 1) {
					if (!$is_h2_db) {
						$cp = $fn($exp);
					} else {
						$cp = $line;
					}

					$delivery_id = $carriers_map[(int) $cp['iCarrier']];
					$payment_id = $payments_map[(int) $cp['iPayment']];

					if ($delivery_id && $payment_id && !($db_cp = Core::$db->delivery_payments()->where('delivery_id', $delivery_id)->where('payment_id', $payment_id)->fetch())) {
						$odata = array(
							'delivery_id' => $delivery_id,
							'payment_id' => $payment_id,
							'price' => $cp['sPrice']
						);

						if ($is_h2) {
							$odata['free_over'] = (float) $config['shipping_free'];
						}

						Core::$db->delivery_payments($odata);
					}
				}
			}

			$log .= "OK\n";
		}

		unset($data);

		if ($import_orders) {
			$log .= "IMPORT ORDERS... \n";
			// orders
			if (!$is_h2_db) {
				$data = file($path . 'db/orders.php');
				$fn = 'orders';

				usort($data, array($this, 'qc_sortOrders'));
			} else {
				$data = $db->query('SELECT * FROM `orders`');
				$data->setFetchMode(PDO::FETCH_ASSOC);
			}

			$orders_map = array();
			$invoices_map = array();

			foreach ($data as $line) {
				if (!$is_h2_db) {
					$exp = explode('$', $line);
				}

				if ($is_h2_db || count($exp) > 1) {
					if (!$is_h2_db) {
						$order = $fn($exp);
					} else {
						$order = $line;
					}

					$odata = array(
						'received' => (int) $order['iTime'],
						'ip' => $order['sIp'],
						'status' => (int) $order['iStatus'],
						'first_name' => $order['sFirstName'],
						'last_name' => $order['sLastName'],
						'company' => $order['sCompanyName'],
						'street' => $order['sStreet'],
						'zip' => $order['sZipCode'],
						'city' => $order['sCity'],
						'phone' => $order['sPhone'],
						'email' => $order['sEmail'],
						'delivery_id' => @$carriers_map[(int) $order['iCarrier']],
						'payment_id' => @$payments_map[(int) $order['iPayment']],
						'delivery_payment' => (float) $order['fCarrierPrice'] + estPrice($order['sPaymentPrice'], (float) $order['fCarrierPrice'])
					);

					if ($is_h2) {
						$odata['company_id'] = $order['sIC'];
						$odata['company_vat'] = $order['sDIC'];
						$odata['ship_first_name'] = $order['shFisrtName'];
						$odata['ship_last_name'] = $order['shLastName'];
						$odata['ship_company'] = $order['shCompanyName'];
						$odata['ship_street'] = $order['shStreet'];
						$odata['ship_zip'] = $order['shZipCode'];
						$odata['ship_city'] = $order['shCity'];
					}

					$order_id = Core::$db->order($odata);

					if ($is_h2 && $order['sInvoice']) {
						$invoices_map[$order['sInvoice']] = $order_id;
					}

					$log .= "ORDER (" . $order['iOrder'] . ") " . $order['sFirstName'] . " " . $order['sLastName'] . " IMPORTED\n";

					$orders_map[(int) $order['iOrder']] = $order_id;
				}
			}

			unset($data);

			// orders PRODUCTS
			if (!$is_h2_db) {
				$data = file($path . 'db/orders_products.php');
				$fn = 'orders_products';
			} else {
				$data = $db->query('SELECT * FROM `orders_products`');
				$data->setFetchMode(PDO::FETCH_ASSOC);
			}

			foreach ($data as $line) {
				if (!$is_h2_db) {
					$exp = explode('$', $line);
				}

				if ($is_h2_db || count($exp) > 1) {
					if (!$is_h2_db) {
						$product = $fn($exp);
					} else {
						$product = $line;
					}

					if (!empty($orders_map[(int) $product['iOrder']])) {
						$odata = array(
							'order_id' => $orders_map[(int) $product['iOrder']],
							'product_id' => @$products_map[(int) $product['iProduct']],
							'quantity' => (int) $product['iQuantity'],
							'price' => (float) $product['fPrice'],
							'vat' => (float) VAT_DEFAULT,
							'name' => $product['sName']
						);

						if ($is_h2) {
							$odata['vat'] = (float) $product['sVat'];
						}

						Core::$db->order_products($odata);
					}
				}
			}

			foreach (Core::$db->order() as $order) {
				$total = 0;
				foreach ($order->order_products() as $product) {
					$total += $product['price'] * $product['quantity'];
				}

				$order->update(array('total_price' => $total));
			}

			unset($data);

			// orders STATUS
			if (!$is_h2_db) {
				$data = file($path . 'db/orders_status.php');
				$fn = 'orders_status';
			} else {
				$data = $db->query('SELECT * FROM `orders_status`');
				$data->setFetchMode(PDO::FETCH_ASSOC);
			}

			foreach ($data as $line) {
				if (!$is_h2_db) {
					$exp = explode('$', $line);
				}

				if ($is_h2_db || count($exp) > 1) {
					if (!$is_h2_db) {
						$status = $fn($exp);
					} else {
						$status = $line;
					}

					if (!empty($orders_map[(int) $status['iOrder']]) && $order = Core::$db->order[$orders_map[(int) $status['iOrder']]]) {
						$order->update(array(
							'status_log' => trim($order['status_log'] . "\n" . $status['iTime'] . ";" . $status['iStatus'] . ";" . User::get('id'))
						));
					}
				}
			}

			unset($data);

			// orders COMMENTS
			if (!$is_h2_db) {
				$data = file($path . 'db/orders_comments.php');
				$fn = 'orders_comments';
			} else {
				$data = $db->query('SELECT * FROM `orders_comments`');
				$data->setFetchMode(PDO::FETCH_ASSOC);
			}

			foreach ($data as $line) {
				if (!$is_h2_db) {
					$exp = explode('$', $line);
				}

				if ($is_h2_db || count($exp) > 1) {
					if (!$is_h2_db) {
						$comment = $fn($exp);
					} else {
						$comment = $line;
					}

					if (!empty($orders_map[(int) $comment['iOrder']]) && $order = Core::$db->order[$orders_map[(int) $comment['iOrder']]]) {
						$order->update(array(
							'comment' => $comment['sComment']
						));
					}
				}
			}

			$log .= "IMPORTED " . count($orders_map) . " ORDERS\n";
			$log .= "OK\n";
		}

		unset($data);

		if ($is_h2 && $import_invoices) {
			$log .= "IMPORT INVOICES... \n";
			// invoices
			if (!$is_h2_db) {
				$data = file($path . 'db/invoices.php');
				$fn = 'invoices';
			} else {
				$data = $db->query('SELECT * FROM `invoices`');
				$data->setFetchMode(PDO::FETCH_ASSOC);
			}

			$invoices2_map = array();

			foreach ($data as $line) {
				if (!$is_h2_db) {
					$exp = explode('$', $line);
				}

				if ($is_h2_db || count($exp) > 1) {
					if (!$is_h2_db) {
						$invoice = $fn($exp);
					} else {
						$invoice = $line;
					}

					$customer = $invoice['sCompanyName'] . "\n"
							. $invoice['sName'] . "\n"
							. $invoice['sStreet'] . "\n"
							. $invoice['sZipCode'] . " " . $invoice['sCity'] . "\n\n"
							. __('company_id') . ": " . $invoice['sIC'] . "  " . __('company_vat') . ": " . $invoice['sDIC'];

					$invoice_id = Core::$db->invoice(array(
						'invoice_num' => $invoice['sNum'],
						'symbol' => $invoice['sVar'],
						'symbol' => $invoice['sVar'],
						'date_issue' => (int) $invoice['iDate'],
						'date_vat' => (int) $invoice['iDateTax'],
						'date_due' => (int) $invoice['iDateDue'],
						'date_create' => (int) $invoice['iDate'],
						'payment' => $invoice['sPayment'],
						'order_id' => @$invoices_map[$invoice['sNum']],
						'customer' => trim($customer),
						'status' => 2
							));

					$log .= "INVOICE " . $invoice['sNum'] . " IMPORTED\n";

					$invoices2_map[$invoice['sNum']] = $invoice_id;
				}
			}

			unset($data);

			// invoices PRODUCTS
			if (!$is_h2_db) {
				$data = file($path . 'db/invoices_items.php');
				$fn = 'invoices_items';
			} else {
				$data = $db->query('SELECT * FROM `invoices_items`');
				$data->setFetchMode(PDO::FETCH_ASSOC);
			}

			foreach ($data as $line) {
				if (!$is_h2_db) {
					$exp = explode('$', $line);
				}

				if ($is_h2_db || count($exp) > 1) {
					if (!$is_h2_db) {
						$item = $fn($exp);
					} else {
						$item = $line;
					}

					if (!empty($invoices2_map[$item['iInvoice']])) { // sNum
						$item_id = Core::$db->invoice_items(array(
							'invoice_id' => $invoices2_map[$item['iInvoice']],
							'name' => $item['sName'],
							'quantity' => $item['iQuantity'],
							'price' => (float) $item['fPrice']
								));
					}
				}
			}

			$log .= "IMPORTED " . count($invoices2_map) . " INVOICES\n";
			$log .= "OK\n";
		}

		if ($import_files) {
			$this->tpl = null;

			$_SESSION['import_files_path'] = $path . 'files/';

			$_SESSION['import_data'] = array(
				'log' => $log
			);

			redirect('admin/import/copyfiles');
		}

		$this->content = $log;
	}

	public function copyfiles($offset = 0)
	{
		$this->tpl = 'result.latte';

		$log = '';
		$offset = (int) $offset;

		if (!empty($_SESSION['import_data'])) {
			$log = $_SESSION['import_data']['log'];

			@set_time_limit(0);

			$log .= "COPY FILES (offset " . $offset . ")... \n";

			$result = $this->_copyFiles($offset, $log);

			$_SESSION['import_data']['log'] = $log;

			if ($result === false) {
				redirect('admin/import/copyfiles/' . $offset);
			}
		}

		$this->content = $log;
	}

	private function _copyFiles(& $offset, & $log)
	{
		$path = $_SESSION['import_files_path'];
		$files = array();
		$max_time = (int) ini_get('max_execution_time');
		$start = time();

		$this->_listFiles('/', $files);

		if (empty($files)) {
			return true;
		}

		$i = -1;
		foreach ($files as $file) {
			$i++;

			if ($i < $offset) {
				continue;
			}

			$offset = $i;

			if ($max_time && (time() - $start) >= ($max_time - 2)) {
				return false;
			}

			$info = pathinfo($file);
			$target = DOCROOT . 'files' . $file;

			if (!is_dir(DOCROOT . 'files' . $info['dirname'])) {
				FS::mkdir(DOCROOT . 'files' . $info['dirname'], true);
			}

			if (copy($path . $file, DOCROOT . 'files' . $file)) {
				$log .= "FILE " . $file . " COPIED\n";

				if (preg_match('/^jpe?g|gif|png$/i', $info['extension'])) {
					Image::thumbs(DOCROOT . 'files' . $file);
				}
			}
		}

		return true;
	}

	private function _listFiles($dir, & $files = array())
	{
		$path = $_SESSION['import_files_path'];
		$dirlen = strlen($path);
		$is_prestashop = (!empty($_SESSION['import_data']['app']) && $_SESSION['import_data']['app'] == 'prestashop');

		if ($f = glob($path . $dir . '*')) {
			foreach ($f as $file) {
				if (is_dir($file)) {
					if (!preg_match('/\/(\d{3}|ext|themes)$/', $file)) {
						$d = substr($file, $dirlen);

						$this->_listFiles($d . '/', $files);
					}
				} else {
					$info = pathinfo($file);

					if ($is_prestashop && !preg_match('/^\d+\-\d+\.jpg$/', $info['basename'])) {
						continue;
					}

					$files[] = substr($file, $dirlen);
				}
			}
		}

		return $files;
	}

	private function qc_getMenu($iType)
	{
		switch ((int) $iType) {
			case 1:
				return 4;
			case 2:
				return 1;
			case 3:
				return 2;
			case 4:
				return 3;
			default:
				return 5;
		}
	}

	private function qc_dirify($str)
	{
		$str = str_replace(
				array('ś', 'ą', 'ź', 'ż', 'ę', 'ł', 'ó', 'ć', 'ń', 'Ś', 'Ą', 'Ź', 'Ż', 'Ę', 'Ł', 'Ó', 'Ć', 'Ń', 'á', 'č', 'ď', 'é', 'ě', 'í', 'ň', 'ř', 'š', 'ť', 'ú', 'ů', 'ý', 'ž', 'Á', 'Č', 'Ď', 'É', 'Ě', 'Í', 'Ň', 'Ř', 'Š', 'Ť', 'Ú', 'Ů', 'Ý', 'Ž', 'ä', 'ľ', 'ĺ', 'ŕ', 'Ä', 'Ľ', 'Ĺ', 'Ŕ', 'ö', 'ü', 'ß', 'Ö', 'Ü'), array('s', 'a', 'z', 'z', 'e', 'l', 'o', 'c', 'n', 'S', 'A', 'Z', 'Z', 'E', 'L', 'O', 'C', 'N', 'a', 'c', 'd', 'e', 'e', 'i', 'n', 'r', 's', 't', 'u', 'u', 'y', 'z', 'A', 'C', 'D', 'E', 'E', 'I', 'N', 'R', 'S', 'T', 'U', 'U', 'Y', 'Z', 'a', 'l', 'l', 'r', 'A', 'L', 'L', 'R', 'o', 'u', 'S', 'O', 'U'), $str
		);

		$str = str_replace(
				array(' ', '&raquo;', '/', '$', '\'', '"', '~', '\\', '?', '#', '%', '+', '^', '*', '>', '<', '@', '|', '&quot;', '%', ':', '&', ',', '=', '--', '--', '[', ']'), array('-', '', '-', '-', '', '', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '', '-', '-', '-', '-', '(', ')'), trim($str)
		);

		return strtolower($str);
	}

	private function qc_sortOrders($a, $b)
	{
		$a = explode('$', $a);
		$b = explode('$', $b);

		if (count($a) <= 1 || count($b) <= 1) {
			return 0;
		}

		if ((int) $a[0] == (int) $b[0]) {
			return 0;
		}

		return ((int) $a[0] < (int) $b[0]) ? -1 : 1;
	}

	private function limitWords($str, $limit, $end_char = null)
	{
		$limit = (int) $limit;
		$end_char = ($end_char === null) ? '&#8230;' : $end_char;

		if (trim($str) === '')
			return $str;

		if ($limit <= 0)
			return $end_char;

		preg_match('/^\s*+(?:\S++\s*+){1,' . $limit . '}/u', $str, $matches);

		return rtrim($matches[0]) . (strlen($matches[0]) === strlen($str) ? '' : $end_char);
	}

}