<?php

defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 * @author Tomas Nikl <tomasnikl.cz@gmail.com>
 */
class Controller_Ajax extends AdminController
{

	public function __construct() {
		$this->tpl = null;
	}

	public function edit_note(){
		if(isset($_POST['id'])){
			$note = Core::$db->notes[(int) $_POST['id']];
		}
		$note->update(array(
			'message' => $_POST['message']
		));

		$this->get_notes();
	}

	public function remove_note(){
		if(isset($_POST['id'])){
			Core::$db->notes[(int) $_POST['id']]->delete();
		}

		$this->get_notes();
	}

	public function get_notes() {
		$notes = array();
		foreach(Core::$db->notes()->order('id DESC') as $note) {
			$notes[] = array(
				'id' => $note['id'],
				'message' => $note['message'],
				'color' => $note['color'],
				'date' => date('j. n. Y H:i:s', $note['date'])
			);
		}
		echo json_encode($notes);
		exit;
	}

	public function save_note() {
		if(isset($_POST))
			Core::$db->notes(array(
				'message' => $_POST['message'],
				'color' => $_POST['color'],
				'date' => time()
			));

		$this->get_notes();
	}

	private static function getTmpFilename($from, $to, $prepend) {
		return 'ajax_stats_'.$prepend.'_'.md5($from.$to.Core::$config['instid'].Core::$config['access_key']).'.txt';
	}

	public function google_analytics() {
		try {
			$from = isset($_GET['from']) ? date('Y-m-d', strtotime(str_replace(' ', '', urldecode($_GET['from'])))) : date('Y-m-d', strtotime('-14 days'));
			$to = isset($_GET['to']) ? date('Y-m-d', strtotime(str_replace(' ', '', urldecode($_GET['to'])))) : date('Y-m-d');
			$tmp_file = DOCROOT . 'etc/tmp/'.$this->getTmpFilename($from, $to, 'ga');
			if(file_exists($tmp_file) && @filemtime($tmp_file) <= strtotime('+4 hours')) {
				echo base64_decode(file_get_contents($tmp_file));
				exit;
			}

			require_once(APPROOT . 'vendor/gapi/class.analytics.php');
			$start_date = $from;
			$end_date = $to;
			$analytics = new fetchAnalytics(Core::$config['ga_username'], Core::$config['ga_password'], Core::$config['ga_profile'], $start_date, $end_date);
			
			$return = array();

			// Tabulka navstevniku
				$referralTraffic = $analytics->referralCount();
				$count = 0;
				foreach ($referralTraffic as $key => $value) {
					$count += (int) $value;
				}
				$_vt = array();
				foreach ($referralTraffic as $key => $value) {
					$_vt[] = array(
						'website' => $key,
						'value' => $value,
						'percentage' => round($value / $count * 100 * 100) / 100
					);
				}
				$_vt = array_reverse($this->orderBy($_vt, 'value'));
				$visitors_table = array(
					'total' => $count,
					'data' => $_vt
				);
				$return['visitors_table'] = $visitors_table;

			// Graf navstevnosti
				$perDayCount = $analytics->perDayCount();
				$visitors_chart = array();
				foreach ($perDayCount as $key => $value) {
					$visitors_chart[] = array(
						'day' => ((int) strtotime($key) ) * 1000,
						'value' => (int) $value
					);
				}
				$return['visitors_chart'] = $visitors_chart;

			// Traffic
				$trafficCountNum = $analytics->sourceCountNum();
				$traffic_chart = array();
				foreach ($trafficCountNum as $key => $value) {
					$traffic_chart[] = array(
						'source' => $key,
						'value' => (int) $value
					);
				}
				$return['traffic_chart'] = $traffic_chart;

			// Zobrazeni stranek za den
				$perDayCount = $analytics->perDayCount();
				$count = 0;
				foreach ($perDayCount as $key => $value) {
					$count += (int) $value;
				}
				$_pd = array();
				foreach ($perDayCount as $key => $value) {
					$_pd[] = array(
						'day' => ((int) strtotime($key) ) * 1000,
						'value' => $value,
						'percentage' => round($value / $count * 100 * 100) / 100
					);
				}
				//$_pd = array_reverse($this->orderBy($_pd, 'value'));
				$view_table = array(
					'total' => $count,
					'data' => $_pd
				);
				$return['view_table'] = $view_table;

			// Traffic tabulka
				$count = 0;
				foreach ($trafficCountNum as $key => $value) {
					$count += (int) $value;
				}
				$_pd = array();
				foreach ($trafficCountNum as $key => $value) {
					$_pd[] = array(
						'source' => $key,
						'value' => $value,
						'percentage' => round($value / $count * 100 * 100) / 100
					);
				}
				$_pd = array_reverse($this->orderBy($_pd, 'value'));
				$traffic_table = array(
					'total' => $count,
					'data' => $_pd
				);
				$return['traffic_table'] = $traffic_table;

			// Basic tabulka
				$basic = $analytics->trafficCount();
				$return['basic_table'] = array('data'=>array());
				$return['basic_table']['data'][] = array(
					'name' => 'Celkem návštěv',
					'value' => number_format($basic[0]['visits'], 0, '', ' '),
				);
				$return['basic_table']['data'][] = array(
					'name' => 'Nových návštěv',
					'value' => number_format($basic[0]['newVisits'], 0, '', ' '),
				);
				$return['basic_table']['data'][] = array(
					'name' => 'Zobrazení stránek',
					'value' => number_format($basic[0]['pageView'], 0, '', ' '),
				);
				$return['basic_table']['data'][] = array(
					'name' => 'Míra opuštění',
					'value' => number_format((round($basic[0]['bounceRate']*100)/100), 0, '', ' ').'%'
				);
				$return['basic_table']['data'][] = array(
					'name' => 'Průměrný čas na stránce',
					'value' => (round($basic[0]['timeOnSite']*100)/100).' sekund'
				);


			fwrite(fopen($tmp_file, 'w'), base64_encode(json_encode($return)));
			echo json_encode($return);
			exit;
		}catch(Exeption $e) {
		}
		echo json_encode(array());
		exit;
	}

	public function orders() {
		$from = isset($_GET['from']) ? date('Y-m-d', strtotime(str_replace(' ', '', urldecode($_GET['from'])))) : date('Y-m-d', strtotime('-14 days'));
		$to = isset($_GET['to']) ? date('Y-m-d', strtotime(str_replace(' ', '', urldecode($_GET['to'])))) : date('Y-m-d');
		$tmp_file = DOCROOT . 'etc/tmp/'.$this->getTmpFilename($from, $to, 'db');
		if(file_exists($tmp_file) && @filemtime($tmp_file) <= strtotime('5 minutes')) {
			echo base64_decode(file_get_contents($tmp_file));
			exit;
		}

		$start_date = $from;
		$end_date = $to;
		$orders = Core::$db->order()->where('status != 4')->where('received >= ' . strtotime($start_date) . ' AND received <= ' . (strtotime($end_date) + 86399));

		$return = array();

		// Pocet objednavek
			$count = array();
			for ($i = strtotime($start_date); $i <= strtotime($end_date); $i = $i + 86400) {
				$orders = Core::$db->order()->select('COUNT(id) as count')->where('status != 4')->where('received >= ' . $i . ' AND received <= ' . ($i + 86399))->fetch();
				$count[] = array(
					'day' => ((int) $i) * 1000,
					'value' => (int) $orders['count'],
					'total' => $orders['total']
				);
			}
			$return['count'] = $count;

		// Hodnota objednavek
			$total = array();
			for ($i = strtotime($start_date); $i <= strtotime($end_date); $i = $i + 86400) {
				$orders = Core::$db->order()->select('SUM(total_incl_vat) as total')->where('status != 4')->where('received >= ' . $i . ' AND received <= ' . ($i + 86399))->fetch();
				$total[] = array(
					'day' => ((int) $i) * 1000,
					'value' => (int) $orders['total'],
				);
			}
			$return['total'] = $total;

		fwrite(fopen($tmp_file, 'w'), base64_encode(json_encode($return)));
		echo json_encode($return);
		exit;
	}

	private function orderBy($data, $field)
	{
		$code = "return strnatcmp(\$a['$field'], \$b['$field']);";
		usort($data, create_function('$a,$b', $code));
		return $data;
	}

}