<?php

defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */
class Controller_Users extends AdminController
{

	public function __construct()
	{
		parent::__construct();

		gatekeeper('users');

		Core::$active_tab = 'system';
	}

	public function index()
	{
		$order_by = 'username';
		$order_dir = 'ASC';

		sortDir($order_by, $order_dir);

		$users = Core::$db->user()->where('username != ?', 'webmex')->order($order_by . ' ' . $order_dir);

		$this->content = tpl('users/list.latte', array(
			'order_by' => $order_by,
			'order_dir' => $order_dir,
			'users' => $users
				));
	}

	public function edit($id)
	{
		$user = Core::$db->user[(int) $id];

		if($user['username'] == "webmex") {
			redirect('admin/users/');
		}

		if ($id > 0 && !$user) {
			flashMsg(__('msg_record_not_found', $id), 'error');
			redirect('admin/users');
		}

		$user_permissions = array();

		if ($user) {
			foreach ($user->user_permissions() as $perm) {
				$user_permissions[] = $perm['permission_id'];
			}
		}

		if (!empty($_POST)) {
			if (($errors = validate(array('username', 'email'))) === true) {
				$data = $_POST;
				$new_pass = null;

				if (!empty($data['password1']) && $data['password1'] == $data['password2']) {
					$new_pass = $data['password1'];
					$data['password'] = sha1(sha1($new_pass));
				} else if (empty($data['password1']) && !$user) {
					$new_pass = strtoupper(random());
					$data['password'] = sha1(sha1($new_pass));
				}

				if (in_array(10000, (array) $data['permissions'])) {
					$data['permissions'] = array(10000);
				}

				if ($id == 1) {
					$data['enable_login'] = 1;
				}

				if (!$user) {
					$user_id = Core::$db->user(prepare_data('user', $data));

					if ($user_id) {
						$user = Core::$db->user[$user_id];
						$saved = true;
					}
				} else {
					$saved = (bool) $user->update(prepare_data('user', $data));
				}

				if ($saved !== false && $user) {

					foreach ((array) $data['permissions'] as $pid) {
						if (!in_array($pid, $user_permissions)) {
							Core::$db->user_permissions(array(
								'user_id' => $user['id'],
								'permission_id' => $pid
							));
						}
					}

					if (!empty($data['permissions'])) {
						Core::$db->user_permissions()->where('user_id', $user['id'])->where('permission_id NOT IN (' . join(',', (array) $data['permissions']) . ')')->delete();
					}

					$_data = $user->as_array();
					$_data['password'] = $new_pass;


					if ($id == 0 && $new_pass) {
						// new user
						sendmail($user['email'], array(__('new_user_account'), __('new_user_account_text')), $_data);
					} else if ($new_pass) {
						// password change
						sendmail($user['email'], array(__('new_user_password'), __('new_user_password_text')), $_data);
					}

					flashMsg(__('msg_saved'));
				} else {
					flashMsg(__('msg_error'), 'error');
				}

				if (isSet($_POST['save_go'])) {
					redirect('admin/users');
				} else {
					redirect('admin/users/edit/' . $user['id']);
				}
			} else {
				flashMsg(__('msg_error'), 'error');
			}
		} else if ($id == 0) {
			$user_permissions[] = 10000;
			$user['id'] = 0;
			$user['enable_login'] = 1;
		}

		$permissions = '';

		foreach (Core::$def['permissions'] as $pid => $n) {
			$permissions .= '<option value="' . $pid . '"' . (in_array($pid, $user_permissions) ? ' selected="selected"' : '') . '>' . (substr($pid, -2) == '00' ? '' : '&nbsp;&nbsp;&nbsp;')
					. join(' &rsaquo; ', array_map('__', preg_split('/-/', $n)))
					. '</option>';
		}

		$this->content = tpl('users/edit.latte', array(
			'user' => $user,
			'permissions' => $permissions
				));
	}

	public function delete($id)
	{
		$this->tpl = null;

		if ($id != 1 && $user = Core::$db->user[(int) $id]) {
			if($user['username'] == 'webmex') {
				redirect('admin/users');
			}
			$user->user_permissions()->delete();
			$user->delete();

			flashMsg(__('msg_deleted'));
		}

		redirect('admin/users');
	}

}