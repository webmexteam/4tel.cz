<?php

defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */
class Controller_Vouchers extends AdminController
{

	public function __construct()
	{
		parent::__construct();

		gatekeeper('vouchers');

		Core::$active_tab = 'customers';
	}

	public function index()
	{
		$order_by = 'id';
		$order_dir = 'DESC';

		sortDir($order_by, $order_dir);

		if (isSet($_POST['action']) && !empty($_POST['item'])) {
			foreach ($_POST['item'] as $id => $v) {
				$voucher = Core::$db->voucher[$id];

				if ($voucher) {
					if ($_POST['action'] == 'delete') {
						$voucher->delete();
					}
				}
			}
			flashMsg(__('msg_saved'));
			redirect('admin/vouchers');
		}

		$vouchers = Core::$db->voucher()->order($order_by . ' ' . $order_dir)->limit(pgLimit(), pgOffset());

		if (!empty($_GET['search']) || !empty($_GET['category'])) {
			$q = trim($_GET['search']);

			$vouchers->where('code LIKE "%' . $q . '%" OR name LIKE "%' . $q . '%"');
		}

		$total_count = Core::$db->voucher;

		if ($vouchers->getWhere()) {
			$total_count->where($vouchers->getWhere());
		}

		$total_count = $total_count->group("COUNT(*)");
		$total_count = (int) $total_count[0];

		$this->content = tpl('vouchers/list.latte', array(
			'order_by' => $order_by,
			'order_dir' => $order_dir,
			'vouchers' => $vouchers,
			'total_count' => $total_count
				));
	}

	public function edit($id)
	{
		$voucher = Core::$db->voucher[(int) $id];

		if ($id > 0 && !$voucher) {
			flashMsg(__('msg_record_not_found', $id), 'error');
			redirect('admin/vouchers');
		}

		if (!Core::$is_premium) {
			redirect('admin/vouchers');
		}

		if (!empty($_POST)) {
			$unique_check = Core::$db->voucher()->where('code', $_POST['code']);

			if ($voucher['id']) {
				$unique_check->where('id != ' . $voucher['id']);
			}

			if (!empty($_POST['code']) && $unique_check->fetch()) {
				flashMsg(__('msg_voucher_not_unique'), 'error');

				foreach ($_POST as $k => $v) {
					$voucher[$k] = $v;
				}
			} else if (($errors = validate(array('code', 'value'))) === true) {
				$data = $_POST;
				$data['last_change'] = time();
				$data['last_change_user'] = User::get('id');

				if (!empty($data['valid_from'])) {
					$data['valid_from'] = strtotime($data['valid_from']);
				}

				if (!empty($data['valid_to'])) {
					$data['valid_to'] = strtotime($data['valid_to']);
				}

				$data['value'] = preg_replace('/^\-|\+/', '', $data['value']);

				if (!$voucher['id']) {
					$voucher_id = Core::$db->voucher(prepare_data('voucher', $data));

					if ($voucher_id) {
						$voucher = Core::$db->voucher[$voucher_id];
						$saved = true;
					}
				} else {
					$saved = (bool) $voucher->update(prepare_data('voucher', $data));
				}

				if ($voucher && $saved !== false) {
					flashMsg(__('msg_saved'));
				} else {
					flashMsg(__('msg_error'), 'error');
				}

				if (isSet($_POST['save_go']) || !$voucher['id']) {
					redirect('admin/vouchers');
				} else {
					redirect('admin/vouchers/edit/' . $voucher['id']);
				}
			}
		} else if ($id == 0) {
			$voucher['id'] = 0;
		}

		$this->content = tpl('vouchers/edit.latte', array(
			'voucher' => $voucher
				));
	}

	public function delete($id)
	{
		$this->tpl = null;

		$voucher = Core::$db->voucher[(int) $id];

		if ($voucher) {
			$voucher->delete();
		}

		redirect('admin/vouchers');
	}

	public function generators()
	{
		$order_by = 'id';
		$order_dir = 'DESC';

		if (!empty($_GET['sort'])) {
			$order_by = $_GET['sort'];
		}

		if (!empty($_GET['dir'])) {
			$order_dir = $_GET['dir'];
		}

		if (isSet($_POST['action']) && !empty($_POST['item'])) {
			foreach ($_POST['item'] as $id => $v) {
				$generator = Core::$db->voucher_generator[$id];

				if ($generator) {
					if ($_POST['action'] == 'delete') {
						$generator->delete();
					}
				}
			}
			flashMsg(__('msg_saved'));
			redirect('admin/vouchers/generators');
		}

		$generators = Core::$db->voucher_generator()->order($order_by . ' ' . $order_dir)->limit(pgLimit(), pgOffset());

		$total_count = Core::$db->voucher_generator;

		if ($generators->getWhere()) {
			$total_count->where($generators->getWhere());
		}

		$total_count = $total_count->group("COUNT(*)");
		$total_count = (int) $total_count[0];

		$this->content = tpl('vouchers/generators.latte', array(
			'order_by' => $order_by,
			'order_dir' => $order_dir,
			'generators' => $generators,
			'total_count' => $total_count
				));
	}

	public function generator_edit($id)
	{
		$generator = Core::$db->voucher_generator[(int) $id];

		if ($id > 0 && !$generator) {
			flashMsg(__('msg_record_not_found', $id), 'error');
			redirect('admin/vouchers/generators');
		}

		if (!Core::$is_premium) {
			redirect('admin/vouchers/generators');
		}

		if (!empty($_POST)) {

			if (($errors = validate(array('value'))) === true) {
				$data = $_POST;
				$data['last_change'] = time();
				$data['last_change_user'] = User::get('id');

				if (!empty($data['valid_from'])) {
					$data['valid_from'] = strtotime($data['valid_from']);
				}

				if (!empty($data['valid_to'])) {
					$data['valid_to'] = strtotime($data['valid_to']);
				}

				$data['value'] = preg_replace('/^\-|\+/', '', $data['value']);

				if (!$generator['id']) {
					$generator_id = Core::$db->voucher_generator(prepare_data('voucher_generator', $data));

					if ($generator_id) {
						$generator = Core::$db->voucher_generator[$generator_id];
						$saved = true;
					}
				} else {
					$saved = (bool) $generator->update(prepare_data('voucher_generator', $data));
				}

				if ($generator && $saved !== false) {
					flashMsg(__('msg_saved'));
				} else {
					flashMsg(__('msg_error'), 'error');
				}

				if (isSet($_POST['save_go']) || !$generator['id']) {
					redirect('admin/vouchers/generators');
				} else {
					redirect('admin/vouchers/generator_edit/' . $generator['id']);
				}
			}
		} else if ($id == 0) {
			$generator['id'] = 0;
		}

		$vouchers = Core::$db->voucher()->where('generator_id', $generator['id'])->order('id DESC');
		$groups = array(0 => __('all'));

		foreach (Core::$db->customer_group() as $group) {
			$groups[$group['id']] = $group['name'];
		}

		$this->content = tpl('vouchers/generator_edit.latte', array(
			'generator' => $generator,
			'vouchers' => $vouchers,
			'groups' => $groups
				));
	}

	public function generator_delete($id)
	{
		$this->tpl = null;

		$generator = Core::$db->voucher_generator[(int) $id];

		if ($generator) {
			$generator->delete();
		}

		redirect('admin/vouchers/generators');
	}

}