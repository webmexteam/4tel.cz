<?php

defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */
class Controller_Settings extends AdminController
{

	public function __construct()
	{
		parent::__construct();

		gatekeeper('settings');

		Core::$active_tab = 'system';
	}

	public function index()
	{
		$templates = '';
		$current_tpl = Core::config('template') . '/' . Core::config('template_theme');

		foreach ((array) glob(APPROOT . 'template/*', GLOB_ONLYDIR) as $tpl) {
			$tpl_name = substr($tpl, strrpos($tpl, '/') + 1);

			if ($tpl_name == 'system' || !file_exists($tpl . '/template_info.php')) {
				continue;
			}

			include($tpl . '/template_info.php');

			$themes = array();

			if ($f = glob($tpl . '/css/theme/*.less.css')) {
				foreach ($f as $theme) {
					$themes[] = substr($theme, strrpos($theme, '/') + 1, -9);
				}
			}

			if ($f = glob(DOCROOT . 'etc/theme/*.less.css')) {
				foreach ($f as $theme) {
					$n = substr($theme, strrpos($theme, '/') + 1, -9);

					if (preg_match('/^' . $tpl_name . '_([^_]*)/', $n, $m)) {
						$themes[] = $m[1];
					}
				}
			}

			if (!empty($themes)) {
				$templates .= '<option value="' . $tpl_name . '" disabled="disabled">' . __('template') . ': ' . $template['name'] . '</option>';
				foreach ($themes as $theme) {
					$templates .= '<option value="' . $tpl_name . '/' . $theme . '"' . ($current_tpl == $tpl_name . '/' . $theme ? ' selected="selected"' : '') . '>&nbsp;&nbsp;&nbsp;- ' . $theme . '</option>';
				}
			} else {
				$templates .= '<option value="' . $tpl_name . '/"' . (Core::config('template') == $tpl_name ? ' selected="selected"' : '') . '>' . __('template') . ': ' . $template['name'] . '</option>';
			}
		}

		if (!empty($_POST)) {

			$ignore = array('activated', 'first_login', 'template', 'phone_required', 'url_full_path', 'sitemap_products');

			if (!isSet($_POST['vat_payer'])) {
				$_POST['vat_payer'] = 0;
			}

			// Prevod barvy
			$_POST['watermark_font_color'] = implode(',', $this->color_unpack($_POST['watermark_font_color']));

			if(isset($_POST['ga_password']) && $_POST['ga_password'] == "") {
				unset($_POST['ga_password']);
			}

			foreach (Core::$db->config() as $config) {
				if (isSet($_POST[$config['name']]) && is_array($_POST[$config['name']])) {
					Core::$db->config()->where('name', $config['name'])->update(array(
						'value' => json_encode($this->_saveArray($config['name'], $_POST[$config['name']]))
					));
				} else if (isSet($_POST[$config['name']])) {
					Core::$db->config()->where('name', $config['name'])->update(array(
						'value' => trim($_POST[$config['name']])
					));
				} else if (is_numeric($config['value']) && $config['value'] == 1 && !in_array($config['name'], $ignore)) {
					Core::$db->config()->where('name', $config['name'])->update(array(
						'value' => 0
					));
				}
			}

			if (!empty($_POST['template'])) {
				$parts = explode('/', $_POST['template']);

				Core::$db->config()->where('name', 'template')->update(array(
					'value' => trim($parts[0])
				));

				Core::$db->config()->where('name', 'template_theme')->update(array(
					'value' => trim($parts[1])
				));
			}

			if (empty($_POST['instid']) || empty($_POST['license_key'])) {
				Core::$db->config()->where('name', 'activated')->update(array(
					'value' => 0
				));
			} else {
				Core::$config['instid'] = $_POST['instid'];
				Core::$config['license_key'] = trim($_POST['license_key']);

				if (Core::validatelkey() && Core::checklkey()) {
					Core::$db->config()->where('name', 'activated')->update(array(
						'value' => 1
					));
				}
			}

			if (!empty($_POST['group'])) {

				foreach ($_POST['group'] as $group_id => $data) {
					if ((int) $group_id && ($group = Core::$db->customer_group[(int) $group_id])) {

						$data['unique_sale'] = (isset($data['unique_sale'])) ? 1 : 0;

						if (trim($data['name'])) {
							// update
							$group->update(prepare_data('customer_group', $data));
						} else {
							// delete
							$group->delete();
						}
					} else if (trim($data['name']) && $data['coefficient']) {
						// insert
						$group_id = Core::$db->customer_group(prepare_data('customer_group', $data));
					}
				}
			}

			if (!empty($_POST['vat_rate'])) {

				foreach ($_POST['vat_rate'] as $vat_id => $data) {
					if ((int) $vat_id && ($vat = Core::$db->vat[(int) $vat_id])) {

						if (trim($data['name'])) {
							// update
							$vat->update(prepare_data('vat', $data));
						} else {
							// delete
							$vat->delete();
						}
					} else if (trim($data['name']) && $data['rate']) {
						// insert
						$vat_id = Core::$db->vat(prepare_data('vat', $data));
					}
				}
			}

			// Razitko na fakture
			if(!empty($_FILES['invoice_signature']) && !empty($_FILES['invoice_signature']['tmp_name']))
			{
				if($_FILES['invoice_signature']['errors'] > 0)
				{
					flashMsg(__('msg_error'));
					redirect(true);
				}

				if(!empty($_FILES['invoice_signature']['type']) && $_FILES['invoice_signature']['type'] != "image/jpg" && $_FILES['invoice_signature']['type'] != "image/jpeg")
				{
					flashMsg(__('msg_insufficient_file'), 'error');
					redirect(true);
				}

				$stamp_row = Core::$db->config()->where('name', 'invoice_signature')->fetch();


				move_uploaded_file($_FILES['invoice_signature']['tmp_name'], DOCROOT . "etc/invoice_signature.jpg");


				if($stamp_row !== false)
				{
					Core::$db->config()->where('name', 'invoice_signature')->update(array('value' => $_FILES['invoice_signature']['name']));
				} else {
					Core::$db->config()->insert(array('name' => 'invoice_signature', 'value' => $_FILES['invoice_signature']['name']));
				}
			}

			// Logo na fakture
			if(!empty($_FILES['invoice_logo']) && !empty($_FILES['invoice_logo']['tmp_name']))
			{
				if($_FILES['invoice_logo']['errors'] > 0)
				{
					flashMsg(__('msg_error'));
					redirect(true);
				}

				if(!empty($_FILES['invoice_logo']['type']) && $_FILES['invoice_logo']['type'] != "image/jpg" && $_FILES['invoice_logo']['type'] != "image/jpeg")
				{
					flashMsg(__('msg_insufficient_file'), 'error');
					redirect(true);
				}

				$logo_row = Core::$db->config()->where('name', 'invoice_logo')->fetch();


				move_uploaded_file($_FILES['invoice_logo']['tmp_name'], DOCROOT . "etc/invoice_logo.jpg");


				if($logo_row !== false)
				{
					Core::$db->config()->where('name', 'invoice_logo')->update(array('value' => $_FILES['invoice_logo']['name']));
				} else {
					Core::$db->config()->insert(array('name' => 'invoice_logo', 'value' => $_FILES['invoice_logo']['name']));
				}
			}

			// Obchodni podminky PDF
			if(!empty($_FILES['business_conditions_pdf']) && !empty($_FILES['business_conditions_pdf']['tmp_name']))
			{
				if($_FILES['business_conditions_pdf']['errors'] > 0)
				{
					flashMsg(__('msg_error'));
					redirect(true);
				}

				if(!empty($_FILES['business_conditions_pdf']['type']) && $_FILES['business_conditions_pdf']['type'] != "application/pdf")
				{
					flashMsg(__('msg_insufficient_file'), 'error');
					redirect(true);
				}

				$bu_row = Core::$db->config()->where('name', 'business_conditions_pdf')->fetch();

				move_uploaded_file($_FILES['business_conditions_pdf']['tmp_name'], DOCROOT . "etc/obchodni_podminky.pdf");

				if($bu_row !== false)
				{
					Core::$db->config()->where('name', 'business_conditions_pdf')->update(array('value' => $_FILES['business_conditions_pdf']['name']));
				} else {
					Core::$db->config()->insert(array('name' => 'business_conditions_pdf', 'value' => $_FILES['business_conditions_pdf']['name']));
				}
			}

			// Obchodni podminky TXT
			if(!empty($_FILES['business_conditions_txt']) && !empty($_FILES['business_conditions_txt']['tmp_name']))
			{
				if($_FILES['business_conditions_txt']['errors'] > 0)
				{
					flashMsg(__('msg_error'));
					redirect(true);
				}

				if(!empty($_FILES['business_conditions_txt']['type']) && $_FILES['business_conditions_txt']['type'] != "text/plain")
				{
					flashMsg(__('msg_insufficient_file'), 'error');
					redirect(true);
				}

				$bu_row = Core::$db->config()->where('name', 'business_conditions_txt')->fetch();

				move_uploaded_file($_FILES['business_conditions_txt']['tmp_name'], DOCROOT . "etc/obchodni_podminky.txt");

				if($bu_row !== false)
				{
					Core::$db->config()->where('name', 'business_conditions_txt')->update(array('value' => $_FILES['business_conditions_txt']['name']));
				} else {
					Core::$db->config()->insert(array('name' => 'business_conditions_txt', 'value' => $_FILES['business_conditions_txt']['name']));
				}
			}

			// Dalsi soubor objednavky PDF
			if(!empty($_FILES['next_email_att']) && !empty($_FILES['next_email_att']['tmp_name']))
			{
				if($_FILES['next_email_att']['errors'] > 0)
				{
					flashMsg(__('msg_error'));
					redirect(true);
				}

				if(!empty($_FILES['next_email_att']['type']) && $_FILES['next_email_att']['type'] != "application/pdf")
				{
					flashMsg(__('msg_insufficient_file'), 'error');
					redirect(true);
				}

				$bu_row = Core::$db->config()->where('name', 'next_email_att')->fetch();

				move_uploaded_file($_FILES['next_email_att']['tmp_name'], DOCROOT . "etc/objednavka_priloha.pdf");

				if($bu_row !== false)
				{
					Core::$db->config()->where('name', 'next_email_att')->update(array('value' => $_FILES['next_email_att']['name']));
				} else {
					Core::$db->config()->insert(array('name' => 'next_email_att', 'value' => $_FILES['next_email_att']['name']));
				}
			}

			if (!empty($_POST['command'])) {
				$this->_execCommand($_POST['command']);
			}

			unset($_SESSION['first_login']);

			flashMsg(__('msg_saved'));

			redirect(true);
		}

		$languages = array();
		foreach ((array) glob(APPROOT . 'i18n/*', GLOB_ONLYDIR) as $lang) {
			$l = substr($lang, strrpos($lang, '/') + 1);

			$languages[$l] = __('language_' . $l);
		}

		$availabilities = array('0' => __('select_option'));
		if ($avs = Core::$db->availability()) {
			foreach ($avs as $av) {
				$availabilities[$av['id']] = $av['name'];
			}
		}

		$config = Core::config();
		$config['watermark_font_color'] = $this->color_pack(explode(',', $config['watermark_font_color']));

		$this->content = tpl('settings.latte', array(
			'config' => $config,
			'templates' => $templates,
			'languages' => $languages,
			'availabilities' => $availabilities,
			'pages' => $this->_pageTree()
				));
	}

	private function _execCommand($command)
	{
		if (preg_match('/^(\w+)(.*)/i', $command, $m) && $m) {
			$cmd = strtolower($m[1]);

			if ($cmd == 'config') {
				// config limit_records_admin = 22
				preg_match('/(\w+)\s?\=\s?(.*)$/', $m[2], $_m);

				if ($_m[1]) {

					if ($row = Core::$db->config()->where('name', trim($_m[1]))->fetch()) {
						Core::$db->config()->where('name', trim($_m[1]))->update(array(
							'value' => trim($_m[2])
						));
					} else {
						Core::$db->config(array(
							'name' => trim($_m[1]),
							'value' => trim($_m[2])
						));
					}
				}
			} else if ($cmd == 'registry') {
				// registry recordname = value
				preg_match('/(\w+)\s?\=\s?(.*)$/', $m[2], $_m);

				if ($_m[1]) {
					Registry::set(trim($_m[1]), trim($_m[2]));
				}
			} else if ($cmd == 'set') {
				// set order autoincrement = 1000
				preg_match('/(\w+)\s+(\w+)\s?\=\s?(.*)$/', $m[2], $_m);

				if ($_m[1] && trim($_m[2]) == 'autoincrement') {
					Core::$db_inst->setAI(trim($_m[1]), (int) $_m[3]);
				}
			} else if ($cmd == 'update') {
				// update product show_ean13 = 1
				preg_match('/(\w+)\s+(\w+)\s?(\=\s?(.*))?$/', $m[2], $_m);

				if ($_m[1] && $_m[2]) {
					$tbl = trim($_m[1]);

					if ($_m[4] === 'NULL' || $_m[4] === '') {
						$_m[4] = NULL;
					}

					if (trim($_m[2]) == 'sef_url' && empty($_m[3])) {
						Core::$api->product->updateURLs();
					} else if (trim($_m[2]) == 'margin') {
						$c = (float) $_m[4] / 100 + 1;

						Db::$connection->query('UPDATE `' . @Core::$def['db']['table_prefix'] . 'product` SET margin = ' . (float) $_m[4] . ' WHERE cost IS  NULL');
						Db::$connection->query('UPDATE `' . @Core::$def['db']['table_prefix'] . 'product` SET margin = ' . (float) $_m[4] . ', price = cost * ' . $c . ' WHERE cost IS NOT NULL');
					} else {
						Core::$db->$tbl()->update(array(
							trim($_m[2]) => $_m[4]
						));
					}
				}
			}
		}
	}

	private function _saveArray($name, $data)
	{
		$output = array();

		if ($arr = json_decode(Core::config($name), true)) {
			foreach ($arr as $n => $v) {
				if (isSet($data[$n])) {
					$output[$n] = $data[$n];
				} else if ($v == 1) {
					$output[$n] = 0;
				} else {
					$output[$n] = $v;
				}
			}
		}

		foreach ($data as $k => $v) {
			if (!isSet($output[$k])) {
				$output[$k] = $v;
			}
		}

		return $output;
	}

	private function _pageTree($parent_id = 0, $level = 0)
	{
		$out = array(0 => __('none'));

		$pages = Core::$db->page()->where('parent_page', $parent_id)->order('position ASC');

		if ($parent_id == 0) {
			$pages->where('menu != 2')->where('menu != 3'); // not categories
		}

		foreach ($pages as $page) {
			$out[$page['id']] = str_repeat('&nbsp;&nbsp;&nbsp;', $level) . $page['name'];

			$out += $this->_pageTree($page['id'], $level + 1);
		}

		return $out;
	}

	public function delivery_payment()
	{
		$deliveries = Core::$db->delivery()->order('ord ASC');
		$payments = Core::$db->payment()->order('ord ASC');

		$this->content = tpl('delivery_payment/list.latte', array(
			'deliveries' => $deliveries,
			'payments' => $payments
				));
	}

	public function delivery($id)
	{
		$delivery = Core::$db->delivery[(int) $id];

		if ($id > 0 && !$delivery) {
			flashMsg(__('msg_record_not_found', $id), 'error');
			redirect('admin/settings/delivery_payment');
		}

		$drivers = array('' => __('none'));
		foreach (Delivery::$drivers as $driver) {
			$driverclass = 'Delivery_' . ucfirst($driver);
			$inst = new $driverclass();
			$drivers[$driver] = $inst->name;
		}

		if (!empty($_POST)) {
			if (($errors = validate(array('name'))) === true) {
				$data = $_POST;

                // logo pro dopravu
                if(!empty($_FILES['img']) && !empty($_FILES['img']['tmp_name']))
                {
                    $logoName = 'etc/'.$_FILES['img']['name'];

                    $data['img'] = $logoName;

                    if($_FILES['img']['errors'] > 0)
                    {
                        flashMsg(__('msg_error'));
                        redirect(true);
                    }

                    if(!empty($_FILES['img']['type']) && $_FILES['img']['type'] != "image/jpg" && $_FILES['img']['type'] != "image/jpeg" && $_FILES['img']['type'] != "image/png")
                    {
                        flashMsg(__('msg_insufficient_file'), 'error');
                        redirect(true);
                    }


                    move_uploaded_file($_FILES['img']['tmp_name'], DOCROOT . $logoName);

                }

				if (!$delivery) {
					$delivery_id = Core::$db->delivery(prepare_data('delivery', $data));

					if ($delivery_id) {
						$delivery = Core::$db->delivery[$delivery_id];
						$saved = true;
					}
				} else {
					$saved = (bool) $delivery->update(prepare_data('delivery', $data));
				}

				if ($delivery && $saved !== false) {
					flashMsg(__('msg_saved'));
				} else {
					flashMsg(__('msg_error'), 'error');
				}

				if (isSet($_POST['save_go'])) {
					redirect('admin/settings/delivery_payment');
				} else {
					redirect('admin/settings/delivery/' . $delivery['id']);
				}
			}
		} else if ($id == 0) {
			$delivery['id'] = 0;
			$delivery['name'] = __('new_delivery');
		}

		$groups = array('0' => __('all'));

		foreach (Core::$db->customer_group() as $group) {
			$groups[$group['id']] = $group['name'];
		}

		$this->content = tpl('delivery_payment/delivery_edit.latte', array(
			'delivery' => $delivery,
			'drivers' => $drivers,
			'groups' => $groups
				));
	}

	public function delivery_delete($id)
	{
		$this->tpl = null;

		$delivery = Core::$db->delivery[(int) $id];

		if ($delivery) {
			$delivery->delivery_payments()->delete();
			$delivery->delete();
		}

		redirect('admin/settings/delivery_payment');
	}

	public function payment($id)
	{
		$payment = Core::$db->payment[(int) $id];

		if ($id > 0 && !$payment) {
			flashMsg(__('msg_record_not_found', $id), 'error');
			redirect('admin/settings/delivery_payment');
		}

		$drivers = array('' => __('none'));
		foreach (Payment::$drivers as $driver) {
			$driverclass = 'Payment_' . ucfirst($driver);
			$inst = new $driverclass();
			$drivers[$driver] = $inst->name;
		}

		$deliveries = Core::$db->delivery();
		$delivery_ids = array();

		if ($payment) {
			foreach ($payment->delivery_payments() as $delivery) {
				$delivery_ids[] = $delivery['delivery_id'];
			}
		}

		if (!empty($_POST)) {
			if (($errors = validate(array('name'))) === true) {
				$data = $_POST;

                // logo pro platbu
                if(!empty($_FILES['img']) && !empty($_FILES['img']['tmp_name']))
                {
                    $logoName = 'etc/'.$_FILES['img']['name'];

                    $data['img'] = $logoName;

                    if($_FILES['img']['errors'] > 0)
                    {
                        flashMsg(__('msg_error'));
                        redirect(true);
                    }

                    if(!empty($_FILES['img']['type']) && $_FILES['img']['type'] != "image/jpg" && $_FILES['img']['type'] != "image/jpeg" && $_FILES['img']['type'] != "image/png")
                    {
                        flashMsg(__('msg_insufficient_file'), 'error');
                        redirect(true);
                    }

                    move_uploaded_file($_FILES['img']['tmp_name'], DOCROOT . $logoName);

                }

				$data['config'] = json_encode($data['payment_config']);

				if (!$payment) {
					$payment_id = Core::$db->payment(prepare_data('payment', $data));

					if ($payment_id) {
						$payment = Core::$db->payment[$payment_id];
						$saved = true;
					}
				} else {
					$saved = $payment->update(prepare_data('payment', $data));
				}

				if ($payment && $saved !== false) {
					if (!empty($_POST['delivery'])) {
						foreach ($_POST['delivery'] as $delivery_id => $on) {
							$fover = $_POST['delivery_free_over'][$delivery_id];
							$price = $_POST['delivery_price'][$delivery_id];

							if (in_array($delivery_id, $delivery_ids)) {
								// update

								Core::$db->delivery_payments()->where('delivery_id', (int) $delivery_id)->where('payment_id', $payment['id'])->update(array(
									'price' => $price == '' ? null : parseFloat($price),
									'free_over' => $fover == '' ? null : parseFloat($fover),
								));
							} else {
								Core::$db->delivery_payments(array(
									'payment_id' => $payment['id'],
									'delivery_id' => (int) $delivery_id,
									'price' => $price == '' ? null : parseFloat($price),
									'free_over' => $fover == '' ? null : parseFloat($fover),
								));
							}
						}
					}

					$del = Core::$db->delivery_payments()->where('payment_id', $payment['id']);

					if (!empty($_POST['delivery'])) {
						$del->where('delivery_id NOT IN (' . join(',', array_keys($_POST['delivery'])) . ')');
					}

					$del->delete();

					flashMsg(__('msg_saved'));
				} else {
					flashMsg(__('msg_error'), 'error');
				}

				if (isSet($_POST['save_go'])) {
					redirect('admin/settings/delivery_payment');
				} else {
					redirect('admin/settings/payment/' . $payment['id']);
				}
			}
		} else if ($id == 0) {
			$payment['id'] = 0;
			$payment['name'] = __('new_payment');
		}

		$groups = array('0' => __('all'));

		foreach (Core::$db->customer_group() as $group) {
			$groups[$group['id']] = $group['name'];
		}

		$this->content = tpl('delivery_payment/payment_edit.latte', array(
			'payment' => $payment,
			'drivers' => $drivers,
			'deliveries' => $deliveries,
			'groups' => $groups
				));
	}

	public function payment_delete($id)
	{
		$this->tpl = null;

		$payment = Core::$db->payment[(int) $id];

		if ($payment) {
			$payment->delivery_payments()->delete();
			$payment->delete();
		}

		redirect('admin/settings/delivery_payment');
	}

	public function availabilities()
	{
		$order_by = 'days';
		$order_dir = 'ASC';

		if (!empty($_GET['sort'])) {
			$order_by = $_GET['sort'];
		}

		if (!empty($_GET['dir'])) {
			$order_dir = $_GET['dir'];
		}

		$availabilities = Core::$db->availability()->order($order_by . ' ' . $order_dir);

		if (isSet($_POST['save']) && !empty($_POST['days'])) {
			foreach ($_POST['days'] as $id => $days) {
				$availability = Core::$db->availability[$id];

				if ($availability) {
					$availability->update(array('days' => (int) $days));
				}
			}

			foreach ($_POST['hex_color'] as $id => $hex_color) {
				$availability = Core::$db->availability[$id];

				if ($availability) {
					$availability->update(array('hex_color' => $hex_color));
				}
			}

			flashMsg(__('msg_saved'));
		}

		$this->content = tpl('availabilities/list.latte', array(
			'order_by' => $order_by,
			'order_dir' => $order_dir,
			'availabilities' => $availabilities
				));
	}

	public function availability($id)
	{
		$availability = Core::$db->availability[(int) $id];

		if ($id > 0 && !$availability) {
			flashMsg(__('msg_record_not_found', $id), 'error');
			redirect('admin/settings/availabilities');
		}

		if (!empty($_POST)) {
			if (($errors = validate(array('name'))) === true) {
				$data = $_POST;

				if (!$availability) {
					$availability_id = Core::$db->availability(prepare_data('availability', $data));

					if ($availability_id) {
						$availability = Core::$db->availability[$availability_id];
						$saved = true;
					}
				} else {
					$saved = (bool) $availability->update(prepare_data('availability', $data));
				}

				if ($availability && $saved !== false) {
					flashMsg(__('msg_saved'));
				} else {
					flashMsg(__('msg_error'), 'error');
				}

				if (isSet($_POST['save_go'])) {
					redirect('admin/settings/availabilities');
				} else {
					redirect('admin/settings/availability/' . $availability['id']);
				}
			}
		} else if ($id == 0) {
			$availability['id'] = 0;
			$availability['name'] = __('new_availability');
			$availability['days'] = 0;
		}

		$this->content = tpl('availabilities/edit.latte', array(
			'availability' => $availability
				));
	}

	public function availability_delete($id)
	{
		$this->tpl = null;

		$availability = Core::$db->availability[(int) $id];

		if ($availability) {
			$availability->delete();
		}

		redirect('admin/settings/availabilities');
	}

	public function countries()
	{
		$order_by = 'name';
		$order_dir = 'ASC';

		if (!empty($_GET['sort'])) {
			$order_by = $_GET['sort'];
		}

		if (!empty($_GET['dir'])) {
			$order_dir = $_GET['dir'];
		}

		$countries = Core::$db->country()->order($order_by . ' ' . $order_dir);

		$this->content = tpl('countries/list.latte', array(
			'order_by' => $order_by,
			'order_dir' => $order_dir,
			'countries' => $countries
				));
	}

	public function country($id)
	{
		$country = Core::$db->country[(int) $id];

		if ($id > 0 && !$country) {
			flashMsg(__('msg_record_not_found', $id), 'error');
			redirect('admin/settings/countries');
		}

		if (!empty($_POST)) {
			if (($errors = validate(array('name'))) === true) {
				$data = $_POST;

				if (!$country) {
					$country_id = Core::$db->country(prepare_data('country', $data));

					if ($country_id) {
						$country = Core::$db->country[$country_id];
						$saved = true;
					}
				} else {
					$saved = (bool) $country->update(prepare_data('country', $data));
				}

				if ($country && $saved !== false) {
					flashMsg(__('msg_saved'));
				} else {
					flashMsg(__('msg_error'), 'error');
				}

				if (isSet($_POST['save_go'])) {
					redirect('admin/settings/countries');
				} else {
					redirect('admin/settings/country/' . $country['id']);
				}
			}
		} else if ($id == 0) {
			$country['id'] = 0;
			$country['name'] = __('new_country');
			$country['position'] = 0;
		}

		$this->content = tpl('countries/edit.latte', array(
			'country' => $country
				));
	}

	public function country_delete($id)
	{
		$this->tpl = null;

		$country = Core::$db->country[(int) $id];

		if ($country) {
			$country->delete();
		}

		redirect('admin/settings/countries');
	}

	public function emails()
	{
		redirect('admin/emails');
	}

	public function email($id)
	{
		redirect('admin/emails');
	}

	public function ajax_getPaymentConfig($driver, $payment_id = null)
	{
		$this->tpl = null;

		$driver_name = 'Payment_' . ucfirst($driver);

		if ($payment_id) {
			$payment = Core::$db->payment[(int) $payment_id];
		} else {
			$payment = null;
		}

		$inst = new $driver_name($payment);

		echo $inst->getConfigForm();
	}

	private function color_unpack($hex, $normalize = false) {
		if (strlen($hex) == 4) {
			$hex = $hex[1] . $hex[1] . $hex[2] . $hex[2] . $hex[3] . $hex[3];
		}

		$c = hexdec($hex);

		for ($i = 16; $i >= 0; $i -= 8) {
			$out[] = (($c >> $i) & 0xFF) / ($normalize ? 255 : 1);
		}

		return $out;
	}

	private function color_pack($rgb, $normalize = false) {
		foreach ($rgb as $k => $v) {
		$out |= (($v * ($normalize ? 255 : 1)) << (16 - $k * 8));
		}
		return '#'. str_pad(dechex($out), 6, 0, STR_PAD_LEFT);
	}

	public function deleteInvoiceSignature()
	{
		$row = Core::$db->config()->where('name', 'invoice_signature')->fetch();

		if($row && $row !== null)
		{
			Core::$db->config()->where('name', 'invoice_signature')->delete();
			$fs = new FS();
			$fs->remove(DOCROOT . 'etc/invoice_signature.jpg');
		}

		flashMsg(__('msg_saved'));
		redirect('admin/settings#jq-tab-invoices');
	}

	public function deleteInvoiceLogo()
	{
		$row = Core::$db->config()->where('name', 'invoice_logo')->fetch();

		if($row && $row !== null)
		{
			Core::$db->config()->where('name', 'invoice_logo')->delete();
			$fs = new FS();
			$fs->remove(DOCROOT . 'etc/invoice_logo.jpg');
		}

		flashMsg(__('msg_saved'));
		redirect('admin/settings#jq-tab-invoices');
	}

	public function seleteBusinessConditionsPdf()
	{
		$row = Core::$db->config()->where('name', 'business_conditions_pdf')->fetch();

		if($row && $row !== null)
		{
			Core::$db->config()->where('name', 'business_conditions_pdf')->delete();
			$fs = new FS();
			$fs->remove(DOCROOT . 'etc/obchodni_podminky.pdf');
		}

		flashMsg(__('msg_saved'));
		redirect('admin/settings#jq-tab-general');
	}

	public function seleteBusinessConditionsTxt()
	{
		$row = Core::$db->config()->where('name', 'business_conditions_txt')->fetch();

		if($row && $row !== null)
		{
			Core::$db->config()->where('name', 'business_conditions_txt')->delete();
			$fs = new FS();
			$fs->remove(DOCROOT . 'etc/obchodni_podminky.txt');
		}

		flashMsg(__('msg_saved'));
		redirect('admin/settings#jq-tab-general');
	}

	public function deleteNextEmailAtt()
	{
		$row = Core::$db->config()->where('name', 'next_email_att')->fetch();

		if($row && $row !== null)
		{
			Core::$db->config()->where('name', 'next_email_att')->delete();
			$fs = new FS();
			$fs->remove(DOCROOT . 'etc/objednavka_priloha.pdf');
		}

		flashMsg(__('msg_saved'));
		redirect('admin/settings#jq-tab-general');
	}

	public function deleteDeliveryImg()
	{
        $param = '';

        $id = $_GET['id'];
        if(isset($id) && $id > 0) {
            $param = $id;

            $delivery = Core::$db->delivery()->where('id', $id)->fetch();

            Core::$db->delivery()->where('id', $id)->update(array('img' => ''));

            if(isset($delivery['img'])) {
                $fs = new FS();
                $fs->remove(DOCROOT . $delivery['img']);
            }
        }

		flashMsg(__('msg_saved'));
		redirect('admin/settings/delivery/'.$param);
	}

	public function deletePaymentImg()
	{
        $param = '';

        $id = $_GET['id'];
        if(isset($id) && $id > 0) {
            $param = $id;

            $payment = Core::$db->payment()->where('id', $id)->fetch();

            Core::$db->payment()->where('id', $id)->update(array('img' => ''));

            if(isset($payment['img'])) {
                $fs = new FS();
                $fs->remove(DOCROOT . $payment['img']);
            }
        }

		flashMsg(__('msg_saved'));
		redirect('admin/settings/payment/'.$param);
	}
}