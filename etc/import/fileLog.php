<?php

$GLOBALS['fileLogUsed'] = FALSE;
$GLOBALS['fileLogDepth'] = 0;
function fileLog($msg = '', $filename = 'main.log', $depthIncrement = 0)
{
	if ($depthIncrement < 0) {
		$GLOBALS['fileLogDepth'] += $depthIncrement;
	}

	$filepath = __DIR__ . '/logs/' . $filename;

	//~~~~~~ Format message
	// Base message
	$msg = '[' . date('Y-m-d H:i:s') . '] [' . $_SERVER['REQUEST_URI'] . "] [" . $_SERVER['REMOTE_ADDR'] . "] \n" . $msg . "\n";

	// Indentation
	$indent = str_repeat("\t", $GLOBALS['fileLogDepth']);
	$msg = $indent . $msg;
	$msg = str_replace("\n", "\n$indent", $msg);
	$msg .= "\n";

	// First entry prepend with blank lines
	if (!$GLOBALS['fileLogUsed']) {
		$msg = "\n\n#####  #####  #####  #####  #####  ##### \n\n\n\n" . $msg;

		$GLOBALS['fileLogUsed'] = TRUE;
	}
	//~~~~~~/

	if ($depthIncrement > 0) {
		$GLOBALS['fileLogDepth'] += $depthIncrement;
	}
	return file_put_contents($filepath, $msg, FILE_APPEND);
}