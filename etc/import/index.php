<?php
/**/ini_set('display_errors', 1); error_reporting(E_ALL);
/**/echo "<pre>START<br>";

header('Content-Type: text/html; charset=utf-8');
ini_set('memory_limit', '3072M');

$timeLimit = 580 + 20;  // X sekund + 20 sekund rezerva
//ignore_user_abort(1);
set_time_limit($timeLimit);

include_once __DIR__ . '/fileLog.php';
include_once 'Db.php';

$db = new Db();

$startTime = time();

$url = 'http://hurtownia.partnertele.com/export/37214.xml';
$xml = 'xml/4tel.xml';

$user = 'info@4tel.cz';
$password = 'kdK339';

$productCounter = 0;
$importSchema = 'hurtownia.partnertele.com';
$impID = existsTodayImport($db);

$logFile = "import.".date('Y-m-d').".".substr(uniqid(),0,4).".log";

if(!$impID) {
    // Create import
    $sql = "INSERT INTO imports (start, status, type, ip) VALUES (NOW(), 2, 0, '".$_SERVER['REMOTE_ADDR']."')";
    $importID = $db->insertAndGetID($sql);

    // Download data file
    if($content = getUrlContent($url, $user, $password)) {
        createFile($xml, $content);
    }
} else {
    // Not neccessary download data file
    $content = file_get_contents($xml);
}

if($content) {

    try {
        $parser = new SimpleXMLElement($content);
    } catch(Exception $e) {
        $sql = "UPDATE imports SET end = NOW(), status = 3 WHERE id = ".$importID;
        $db->query($sql);

        addToLogFile($e->getMessage());
        /**/echo "EXIT(53)<br>";
        exit(0);
    }

    // get all product in db
    //$dbProductNos = getProductNos($db);

    //echo "<br> Produktu ".count($parser->produkt);
    $importProducts = array('products' => count($parser->produkt), 'updated' => 0, 'new' => 0);

    // Get currency list from CNB
    $cnbUrl = 'https://www.cnb.cz/cs/financni_trhy/devizovy_trh/kurzy_devizoveho_trhu/denni_kurz.txt?date='.date('d.m.Y');

    // Get currency content from CNB
    $cnb = getUrlContent($cnbUrl);

    if ($parser != null && count($parser->produkt) > 0) {


        //      ####                   #                    #             ###
        //      #   #   ####   ####          ###                 # ##    #      ###
        //      ####   #   #  ###      #    #               #    ##  #  ####   #   #
        //      #   #  #  ##    ###    #    #               #    #   #   #     #   #
        //      ####    ## #  ####     #     ###            #    #   #   #      ###
        //
        if(!$impID) {
            foreach($parser->produkt as $prd) {

                $product = array();
                $page = array();
                $currency = 'EUR';

                $id = xmlAttribute($prd, 'id');
                $product['productno'] = trim($id);
                $product['import_schema'] = $importSchema;
                $product['import_uid'] = $importID;
                $product['stock'] = 10; // default stock items

                if($prd->nazwy) {
                    if($prd->nazwy->nazwa) {
                        //echo "<br> $id, ".$prd->nazwy->nazwa;
                        $product['name'] = trim(addslashes($prd->nazwy->nazwa));
                        $product['sef_url'] = str2url($product['name']);
                    }
                }

                if($prd->opisy) {
                    if($prd->opisy->opis) {
                        $product['description'] = trim(addslashes($prd->opisy->opis));
                    }
                }

                if($prd->cena) {
                    if($prd->cena->cena_netto) {
                        $product['price'] = xmlAttribute($prd->cena->cena_netto, 'wartosc');
                        $currency = xmlAttribute($prd->cena->cena_netto, 'waluta');
                    }
                }

                $product['availability_id'] = 3; // Avilability Sell ended - default value

                if($prd->dostepnosc) {
                    //$product['status'] = 1;
                    $product['availability_id'] = 1; // Avilability In stock
                    if($prd->dostepnosc == 0) {
                        //$product['status'] = 0;
                        $product['availability_id'] = 3; // Avilability Sell ended
                    }
                }

                if($prd->ean) {
                    $product['ean13'] = trim($prd->ean);
                }

                $page['menu'] = 2; // menu = 2, page is category

                if($prd->kategoria) {
                    $page['name'] = trim(addslashes($prd->kategoria));
                    $page['sef_url'] = str2url($page['name']);
                }

                if($prd->kategoria_tree) {
                    $page['origin_tree'] = trim($prd->kategoria_tree);
                }

                $originPrice = array('price' => $product['price'], 'currency' => $currency);
                $price = getCZPriceFromCNB($cnb, $originPrice);
                $product['cost'] = $price; // Purchase price
                $product['price'] = $price; // Selling price

                //**/print("saveProduct... "); var_dump($product);
                $importProducts = saveProduct($db, $product, $page, $importProducts);

                sleep(0.1);
            }

            hideEmptyCategories($db);

            $sql = "UPDATE imports SET  end = NOW(), status = 1, products = ".$importProducts['products'].", updated = ".$importProducts['updated'].", new = ".$importProducts['new']."
                    WHERE id = ".$importID;
            $db->query($sql);
        }


        //       ###    ##        #           #                                 #     ###
        //      #   #    #     ####                ## #   ####    ###   # ##   ####      #
        //      #   #    #    #   #           #    # # #  #   #  #   #  ##      #      ##
        //      #   #    #    #   #           #    # # #  #   #  #   #  #       #
        //       ###    ###    ####           #    #   #  ####    ###   #        ##    #
        //                                                #
        $import = existsInterruptedImport($db);

        $next = 0;
        if ($import['id'] > 0) {
            $importID = $import['id'];
            $sql = "UPDATE imports SET status = 2 WHERE id = ".$importID;
            $db->query($sql);
            $next = $import['next'];
        } elseif (!existsTodayImport($db, 1)) {
            // Create import
            $sql = "INSERT INTO imports (start, status, type) VALUES (NOW(), 2, 1)";
            $importID = $db->insertAndGetID($sql);
        } else {
            /**/echo "EXIT(162)<br>";
            exit(0);
        }

        
        //      #####    #     ##
        //      #               #     ###    ####
        //      ####     #      #    #####  ###
        //      #        #      #    #        ###
        //      #        #     ###    ###   ####
        $i = 0;
        foreach($parser->produkt as $prd) {
            $id = xmlAttribute($prd, 'id');

            if($i==0)fileLog(json_encode($prd), $logFile);
            //**/if($id==51917)fileLog("Product #$prd[productno]:", $logFile, +1);

            if(isset($next) && $i < $next)  {
                $i++;
                //**/if($id==51917)fileLog("skipping...", $logFile, -1);
                continue;
            }
            //**/if($id==51917)fileLog(json_encode($prd), $logFile);

            $product_file = array();


            // Get product id
            $query = "SELECT id FROM product WHERE productno = '".$id."' LIMIT 1";
            $prdDb = $db->select($query);

            if ($prdDb[0]['id'] > 0) {

                if ($prd->zdjecia) {

                    foreach($prd->zdjecia->zdjecie as $file) {

                        if($file) {
                            $originId = xmlAttribute($file, 'id');
                            //**/if($id==51917)fileLog("File #$originId:", $logFile, +1);

                            $originFileUrl = xmlAttribute($file, 'url');
                            $originFileArray = explode('/', $originFileUrl);
                            $fileNameArray = explode('.', $originFileArray[count($originFileArray)-1]);

                            $extension = $fileNameArray[count($fileNameArray)-1];

                            $fileUrl = 'http://partnertele.com/images/600/'.$originId.'-600.'.$extension;

                            $fileArray = explode('/', $fileUrl);

                            $product_file['product_id'] = $prdDb[0]['id'];
                            $product_file['filename'] = 'files/'.$fileArray[count($fileArray)-1];
                            $product_file['origin_id'] = $originId;
                            $product_file['is_image'] = 1;
                            $product_file['main'] = 0;

                            $main = xmlAttribute($file, 'glowne');

                            $filenameMain = 'files/_640x480/'.$fileArray[count($fileArray)-1];
                            $filenameMiddle = 'files/_200x200/'.$fileArray[count($fileArray)-1];
                            $filenameSmall = 'files/_120x120/'.$fileArray[count($fileArray)-1];

                            if($main > 0) {
                               $product_file['main'] = 1;
                            }
                            //**/if($id==51917)fileLog(json_encode($product_file), $logFile);

                            $fileID = existsFileDb($db, $product_file);

                            if(!$fileID || !existsFile($db, $product_file)) {
                                //**/if($id==51917)fileLog("file not exist, downloading...", $logFile);

                                $filename = '../../'.$product_file['filename'];
                                $filenameMain = '../../'.$filenameMain;
                                $filenameMiddle = '../../'.$filenameMiddle;
                                $filenameSmall = '../../'.$filenameSmall;

                                if(file_put_contents($filename, file_get_contents($fileUrl))) {
                                    file_put_contents($filenameMain, file_get_contents($fileUrl));
                                    file_put_contents($filenameMiddle, file_get_contents($fileUrl));
                                    file_put_contents($filenameSmall, file_get_contents($fileUrl));

                                    $product_file['size'] = -1;
                                    $product_file['align'] = 2;

                                    $tableColumns = implode(", ", array_keys($product_file));
                                    $tableValues  = implode("', '", array_values($product_file));

                                    $sql = "INSERT INTO product_files ($tableColumns) VALUES ('$tableValues')";
                                    $fileID = $db->insertAndGetID($sql);
                                    //**/if($id==51917)fileLog("download succeeded.", $logFile);
                                } else {
                                    //**/if($id==51917)fileLog("download failed.", $logFile);
                                }
                            } else {
                                //**/if($id==51917)fileLog("file exists, updating 'main' attribute...", $logFile);
                                $sql = "UPDATE product_files SET main = ".$product_file['main']." WHERE id = ".$fileID;
                                $db->query($sql);
                            }

                            //**/if($id==51917)fileLog("------", $logFile, -1);
                        }
                    }

                }
            }

            //if($i > 9) break;
            $i++;

            $sql = "UPDATE imports SET status = 2, next = $i WHERE id = ".$importID;
            $db->query($sql);

            $duration = time() - $startTime; // Doba trvani
            if($duration > $timeLimit - 20) { // Ukonci skript kdyz se blizi konec time limitu, rezerva 20 sekund
                $sql = "UPDATE imports SET end = NOW(), status = 4, next = $i WHERE id = ".$importID;
                $db->query($sql);

                addToLogFile("<br>Vynuceny konec! ".$duration."s ".date('Y-m-d H:i:s'));

                // Remove duplicated rows
                $sql = "
                    DELETE pf1
                    FROM product_files pf1, product_files pf2
                    WHERE pf1.id < pf2.id
                        AND pf1.product_id = pf2.product_id
                        AND pf1.filename = pf2.filename
                ";
                $db->query($sql);
                //echo date('Y-m-d G:i:s')."<br>EXIT(266) - refreshing...<br><script>window.location.reload(false);</script>";

                //**/if($id==51917)fileLog("Ending because of time limit...", $logFile, -1);
                exit(0);
            }
        }


        //       ###    ##                                ####   ####
        //      #   #    #     ###    ####  # ##          #   #  #   #
        //      #        #    #####  #   #  ##            #   #  ####
        //      #   #    #    #      #  ##  #             #   #  #   #
        //       ###    ###    ###    ## #  #             ####   ####

        // Discard products, that aren't in last import
        //$sql = "UPDATE product SET status = 0 WHERE import_uid != ".$importID;
        $sql = "DELETE FROM product WHERE import_uid != " . ($importID - 1);  // -1: two-way import process
        if ($productCounter > 999) {  // don't delete anything if suspiciously count of products were updated
            $deleteResult = $db->query($sql);
            /**/print("\n\n$sql\n");
            /**/print("Deleted rows: " . $db::$connection->affected_rows . "\n\n");
        }

        // Remove duplicated rows
        $sql = "
            DELETE pf1
            FROM product_files pf1, product_files pf2
            WHERE pf1.id < pf2.id
                AND pf1.product_id = pf2.product_id
                AND pf1.filename = pf2.filename
        ";
        $db->query($sql);

        // Create import
        $sql = "UPDATE imports SET end = NOW(), status = 1 WHERE id = ".$importID;
        $db->query($sql);

        //**/if($id==51917)fileLog("========================", $logFile, -1);
    }




} else {
    $sql = "UPDATE imports SET end = NOW(), status = 0 WHERE id = ".$importID;
    $db->query($sql);

}


//      #####                        #       #
//      #      #   #  # ##    ###   ####           ###   # ##    ####
//      ####   #   #  ##  #  #       #       #    #   #  ##  #  ###
//      #      #  ##  #   #  #       #       #    #   #  #   #    ###
//      #       ## #  #   #   ###     ##     #     ###   #   #  ####
function saveProduct($db, $product, $page, $importProducts) {
    global $importID, $productCounter;

    $query = "SELECT id, status, availability_id, cost  FROM product WHERE productno = '".$product['productno']."' LIMIT 1";
    $prd = $db->select($query);

    if(count($prd) > 0) {
        //**/print("UPDATE product ..."); var_dump($product); 

        $productID = $prd[0]['id'];

        // delete product from all categories, don't delete product from New items category(109)
        $query = "DELETE FROM product_pages WHERE product_id = ".$productID." AND page_id != 109";
        $db->query($query);

        $date = new DateTime(date('Y-m-d H:i:s'));
        $date->format("U");

        $costCond = "";
        if(!empty($prd[0]['cost']) && $prd[0]['cost'] == 0) {
            $costCond = "cost = ".$product['cost'].", ";
        }

        // Don't update the prices
        $sql = "UPDATE product SET import_schema = '".$product['import_schema']."',
                                    import_uid = ".$importID /*$product['import_uid']*/ .",
                                    status = 1,
                                    availability_id = ".$product['availability_id'].",
                                    stock = stock+".$product['stock'].",
                                    ean13 = stock+".$product['ean13'].",
                                    $costCond
                                    last_change = '".$date->getTimestamp()."'
                                    WHERE id = ".$productID;
        $db->query($sql);
        $productCounter++;
        //**/print($sql); die();

        $importProducts['updated'] = $importProducts['updated'] + 1;

        //exists page ?
        $pageID = existsCategory($db, $page);

        if(!$pageID) {
            $date = new DateTime(date('Y-m-d H:i:s'));
            $date->format("U");

            $page['last_change'] = $date->getTimestamp();

            // Create page if page is not exists
            $tableColumns = implode(", ",array_keys($page));
            $tableValues  = implode("', '", array_values($page));

            $sql = "INSERT INTO page ($tableColumns) VALUES ('$tableValues')";
            $pageID = $db->insertAndGetID($sql);
        }

        if($productID > 0 && $pageID > 0) {
            $sql = "INSERT INTO product_pages (product_id, page_id) VALUES ($productID, $pageID)";
            $db->insertAndGetID($sql);

            // if db product was sell ended status, we class product to category New items
            if($prd[0]['availability_id'] == 3 && $product['availability_id'] == 1) {
                // Save product to New items category
                $sql = "INSERT INTO product_pages (product_id, page_id) VALUES ($productID, 109)";
                $db->insertAndGetID($sql);
            }

            // Disable update sell price at updated product, except cost = 0
            if(!empty($prd[0]['cost']) && $prd[0]['cost'] == 0) {
                marginCalculate($db, $pageID, $productID);
            }
        }

    } else {
        $date = new DateTime(date('Y-m-d H:i:s'));
        $date->format("U");

        $product['last_change'] = $date->getTimestamp();

        // Class to new items category and status = 0
        $tableColumns = implode(", ",array_keys($product));
        $tableValues  = implode("', '", array_values($product));

        $sql = "INSERT INTO product ($tableColumns) VALUES ('$tableValues')";
        $productID = $db->insertAndGetID($sql);
        $productCounter++;
        //**/print("<br>\n$sql<br>\n");

        $importProducts['new'] = $importProducts['new'] + 1;

        $pageID = existsCategory($db, $page);

        if(!$pageID) {
            $date = new DateTime(date('Y-m-d H:i:s'));
            $date->format("U");

            $page['last_change'] = $date->getTimestamp();

            $tableColumns = implode(", ",array_keys($page));
            $tableValues  = implode("', '", array_values($page));

            $sql = "INSERT INTO page ($tableColumns) VALUES ('$tableValues')";
            $pageID = $db->insertAndGetID($sql);
        }

        if($productID > 0 && $pageID > 0) {
            $sql = "INSERT INTO product_pages (product_id, page_id) VALUES ($productID, $pageID)";
            $db->insertAndGetID($sql);

            // Save product to New items category
            $sql = "INSERT INTO product_pages (product_id, page_id) VALUES ($productID, 109)";
            $db->insertAndGetID($sql);

            marginCalculate($db, $pageID, $productID);

        }

    }

    return $importProducts;
}


function marginCalculate($db, $pageID, $productID) {

    if($pageID > 0 && $productID > 0) {

        $query = "SELECT id, margin FROM page WHERE id = $pageID LIMIT 1";
        $p = $db->select($query);

        // Margin application for product in category
        $margin = 0;
        if(!empty($p[0]['margin']) && is_numeric($p[0]['margin'])) {
            $margin = $p[0]['margin'];
        }

        $query = "SELECT id, cost, lock_price FROM product WHERE id = $productID LIMIT 1";
        $prd = $db->select($query);

        if($prd && $prd[0]['lock_price'] == 0) {
            $price = (($prd[0]['cost'] / 100) * $margin) + $prd[0]['cost'];

            $sql = "UPDATE product SET price = $price WHERE id = ".$productID;
            $db->query($sql);
        }

    }
}


function hideEmptyCategories($db) {

    $query = "SELECT id FROM page WHERE menu = 2";
    $pages = $db->select($query);

    foreach($pages as $page) {
        $query = "SELECT COUNT(*) AS count FROM product_pages WHERE page_id = ".$page['id'];
        $count = $db->select($query);

        if($count[0]['count'] == 0 && !hasSubcategory($db, $page['id'])) {
            $sql = "UPDATE page SET status = 0 WHERE id = ".$page['id'];
            $db->query($sql);
        }

    }

}


function hasSubcategory($db, $id) {

    $has = false;

    $query = "SELECT id FROM page WHERE parent_page = $id LIMIT 1";
    $pages = $db->select($query);

    if(count($pages) > 0) {
        $has = true;
    }

    return $has;
}


function existsCategory($db, $page) {
    $pageID = false;

    $query = "SELECT id FROM page WHERE origin_tree = '".$page['origin_tree']."' LIMIT 1";
    $page = $db->select($query);

    if(count($page) > 0) {
        $pageID = $page[0]['id'];
    }

    return $pageID;
}


function existsFileDb($db, $productFile) {
    // Check DB
    $dbExists = false;

    $query = "SELECT id FROM product_files WHERE origin_id = '".$productFile['origin_id']."' AND product_id = ".$productFile['product_id']." LIMIT 1";
    $pf = $db->select($query);

    if(count($pf) > 0) {
        $dbExists = $pf[0]['id'];
    }

    return ($dbExists);
}


function existsFile($db, $productFile) {
    // Check filesystem
    $fileExists = file_exists("../../".$productFile['filename']);

    return $fileExists;
}


function existsTodayImport($db, $type = 0) {

    $importID = false;

    $sql = "SELECT * FROM imports WHERE DATE(end) = '".date('Y-m-d')."' AND status = 1 AND type = $type LIMIT 1";
    $imp = $db->select($sql);

    if(count($imp) > 0) {
        $importID = $imp[0]['id'];
    }

    return $importID;
}


function existsInterruptedImport($db) {

    $import = array('id' => false, 'next' => 0);

    $sql = "SELECT * FROM imports WHERE DATE(end) = '".date('Y-m-d')."' AND status = 4 LIMIT 1";
    $imp = $db->select($sql);

    if(count($imp) > 0) {
        $import['id'] = $imp[0]['id'];
        $import['next'] = $imp[0]['next'];
    }

    return $import;
}


function getProductNos($db) {

    $nos = array();

    $query = "SELECT id FROM product";
    $products = $db->select($query);

    foreach($products as $p) {
        $nos[] = $p['id'];
    }

    return $nos;
}


function getUrlContent($url, $user = null, $password = null) {

    $curl_handle=curl_init();
    curl_setopt($curl_handle, CURLOPT_HEADER, 0);
    curl_setopt($curl_handle, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, 1);
    //curl_setopt($curl_handle, CURLOPT_FOLLOWLOCATION, 1);
    curl_setopt($curl_handle, CURLOPT_FILETIME, 1);
    curl_setopt($curl_handle, CURLOPT_URL, trim($url));
    if($user != null && $password != null) {
        curl_setopt($curl_handle, CURLOPT_USERPWD, "$user:$password");
        curl_setopt($curl_handle, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    }
    $content = curl_exec($curl_handle);
    $http_status = curl_getinfo($curl_handle, CURLINFO_HTTP_CODE);
    $redirectURL = curl_getinfo($curl_handle, CURLINFO_EFFECTIVE_URL);
    $lastModified = curl_getinfo($curl_handle,CURLINFO_FILETIME);

    curl_close($curl_handle);

    if(($http_status >= 400 && $http_status <= 600) || $http_status == 0) {
        addToLogFile($http_status."|".$url."|Nepodařilo se získat obsah souboru!");
    } else {
        addToLogFile("OK|".$url);
    }

    return $content;
}


function createFile($fileName, $content = null) {
    $fp = fopen($fileName, 'wb');
    if($content != null) {
        fwrite($fp, $content);
    }
    fclose($fp);
}


function addToLogFile($content) {

    $dirLog = 'logs';
    $log = 'logs/log-'.date('Ymd').'.txt';

    if(!file_exists($log)) {

        if(!is_dir($dirLog)) {
            $old = umask(0000);
            mkdir($dirLog, '0777');
            umask($old);
        }

        createFile($log);
    }

    $fileContent = file_get_contents($log);
    $fileContent .= date('Y-m-d H:i:s')."\t".$content."\n";

    file_put_contents($log, $fileContent);
}


function xmlAttribute($object, $attribute)
{
    if(isset($object[$attribute]))
        return trim((string) $object[$attribute]);
}


function getCZPriceFromCNB($cnb, $p) {

    // Currency
    $price = $p['price'];
    $rate = 27.5;
    if(preg_match('/(.*)\|'.$p['currency'].'\|(.*)/', $cnb, $currency)) {
        if(isset($currency[2])) {
            $rate = floatval(preg_replace('/\,/', '.', $currency[2]));
            $price = round(($price * $rate), 2);
        }
    }

    return $price;
}


function str2url($s, $ll = 0, $lw = 0 ) {

    $s = str_replace( array( '&amp;', '&quot;' ), ' ', $s );

    $s = str_replace( array(
            'Á','À','Ä','Å','Č','Ď','É','È','Ë','Ě','Í','Ì','Ï','Ĺ','Ľ','Ň'
            ,'Ó','Ò','Ö','Ô','Ø','Ŕ','Ř','Š','Ť','Ú','Ù','Ü','Ů','Ý','Ž'
            ,'á','à','ä','å','č','ď','é','è','ë','ě','í','ì','ï','ĺ','ľ','ň'
            ,'ó','ò','ö','ô','ø','ŕ','ř','š','ť','ú','ù','ü','ů','ý','ž'
            ,'µ','Ÿ','Œ','œ'
    ), array(
            'a','a','a','a','c','d','e','e','e','e','i','i','i','l','l','n'
            ,'o','o','o','o','o','r','r','s','t','u','u','u','u','y','z'
            ,'a','a','a','a','c','d','e','e','e','e','i','i','i','l','l','n'
            ,'o','o','o','o','o','r','r','s','t','u','u','u','u','y','z'
            , 'u', 'y', 'oe', 'oe'
    ), $s );

    $s = strtolower( $s );
    $s = preg_replace( '/[^a-z0-9_]/', ' ', $s );
    $s = trim( $s );
    $s = preg_replace( '/ +/', '-', $s );

    if ( $ll && ( strlen( $s ) > $ll ) ) $s = substr( $s, 0, $ll );
    if ( $lw )
    {
        $m = explode( '-', $s );
        if ( count($m) > $lw ) $s = implode('-',array_slice($m,0,$lw));
    }

    return $s;
}


/**/echo "END.";