<?php defined('WEBMEX') or die('No direct access.');

$view->override = false;

$view->slot_start(); ?>

	<div class="inputwrap clearfix">
		<?php
			 function _pageTree($parent_id = 0, $level = 0)
			{
				$out = array(0 => __('none'));

				$pages = Core::$db->page()->where('parent_page', $parent_id)->order('position ASC');

				if($parent_id == 0){
					$pages->where('menu != 2')->where('menu != 3'); // not categories
				}

				foreach($pages as $page){
					$out[$page['id']] = str_repeat('&nbsp;&nbsp;&nbsp;', $level).$page['name'];

					$out += _pageTree($page['id'], $level + 1);
				}

				return $out;
			}
			$pages = _pageTree();
		?>
		<?php echo forminput('select', 'page_webmex_consulting_room', $config['page_webmex_consulting_room'], array('options' => $pages)); ?>
	</div>

<?php $question = $view->slot_stop();  ?>


<?php 
	$description = $view->find('#tab-pages .tabwrap .inputwrap:last');
	$description->after($question);
?>


