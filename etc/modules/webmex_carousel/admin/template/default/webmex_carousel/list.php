<?php defined('WEBMEX') or die('No direct access.');

?>

<h2><?php echo __('webmex_carousel_list_carousel')?></h2>

<table class="datagrid">
	<thead>
		<tr>
			<td>&nbsp;</td>
			<td class="<?php echo ($order_by == 'id' ? ' sort-'.$order_dir : '')?>"><a href="<?php echo url(true, array('sort' => 'id', 'dir' => ($order_dir == 'ASC' ? 'DESC' : 'ASC')))?>"><?php echo __('id')?></a></td>
			<td class="<?php echo ($order_by == 'name' ? ' sort-'.$order_dir : '')?>"><a href="<?php echo url(true, array('sort' => 'name', 'dir' => ($order_dir == 'ASC' ? 'DESC' : 'ASC')))?>"><?php echo __('webmex_carousel_carousel_name')?></a></td>
			<td><?php echo __('webmex_carousel_number_of_items')?></td>
			<td class="actions">&nbsp;</td>
		</tr>
	</thead>

	<tbody>
		<?php if(! count($carousels)): ?>
			<tr class="norecords">
				<td colspan="5"><?php echo __('no_records')?></td>
			</tr>
		<?php endif; ?>

		<?php foreach($carousels as $item): ?>
			<?php 
				$items = Core::$db->webmex_carousel_item()->where('carousel_id', $item['id']);
			?>
			<tr>
				<td width="20"><i class="icon-webmex-folder"></i></td>
				<td><?php echo $item['id']?></td>
				<td><a href="<?php echo url('admin/webmex_carousel/edit_carousel/'.$item['id'])?>" title="<?php echo __('edit')?>"><?php echo $item['name']?></a></td>
				<td><?php echo $items->count()?></td>
				<td class="actions">
					<a href="<?php echo url('admin/webmex_carousel/edit_carousel/'.$item['id'])?>" class="edit" title="<?php echo __('edit')?>">&nbsp;</a>
					<a href="<?php echo url('admin/webmex_carousel/delete_carousel/'.$item['id'])?>" class="delete" title="<?php echo __('delete')?>">&nbsp;</a>
				</td>
			</tr>
		<?php endforeach; ?>
	</tbody>
</table>
