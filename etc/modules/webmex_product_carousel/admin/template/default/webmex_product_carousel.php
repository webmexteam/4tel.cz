<?php defined('WEBMEX') or die('No direct access.');?>


<?php

	$storage = Core::module('webmex_product_carousel')->getStorage();
	$products = unserialize($storage['products']);

?>

<div class="inputwrap related-products">
	<label>Zboží v carouselu:</label>
	<div class="products">
		<ul>
			<?php foreach($products as $product): ?>
			<?php $product = Core::$db->product[(int) $product] ?>
			<li class="clearfix">
				<a href="#" class="delete">&nbsp;</a>
				<input type="hidden" name="products[]" value="<?php echo $product['id']?>" />

				<?php echo $product['name']?><br />
				<span class="price"><?php echo price($product['price'])?></span>
				<span class="id">(<?php echo $product['id']?>)</span>
			</li>
			<?php endforeach; ?>
		</ul>
		<div class="addproduct">
			<a href="#">Přidat zboží</a>
		</div>
	</div>
</div>


<script>

$(function(){
	$('.addproduct').bind('click', function(){
		productfinder({
			callback: function(products){

				var wrap = $('.related-products .products ul');

				if(products.length){
					$('.related-products .products li.empty').remove();
				}

				$.each(products, function(i, product){
					var li = $('<li class="clearfix" />');
					li.append('<a href="#" class="delete">&nbsp;</a>');
					li.append('<input type="hidden" name="products[]" value="'+product['product_id']+'" />');
					li.append(product['name']+'<br />');
					li.append('<span class="price">'+displayPrice(product['price'])+'</span>');
					li.append('<span class="id">('+product['product_id']+')</span>');

					wrap.append(li);
				});
			}
		});
		return false;
	});

	$('.related-products .products a.delete').live('click', function(){
		$(this).parent().remove();
		return false;
	});

	var relatedCount = $('.related-products .products li').length;

	if(! relatedCount){
		$('.related-products .products ul').append('<li class="empty">Žádné položky</li>');
	}
});
</script>

<style>
.related-products .products {
		display: block;
		border: 1px solid #ddd;
		padding: 0;
		margin: 10px 50% 5px 0;
		background: #fff;
	}
	.related-products .products ul {
		margin: 0;
		padding: 0;
		list-style: none;
	}
	.related-products .products ul li {
		padding: 3px;
		border-bottom: 1px solid #eee;
	}
	.related-products .products ul li:last-child {
		border-bottom: none;
	}
	.related-products .products ul li .price,
	.related-products .products ul li .id {
		font-size: 90%;
		color: #777;
		margin-right: 5px;
	}
	.related-products .products ul li .pic {
		float: left;
		width: 40px;
		margin-right: 5px;
	}
	.related-products .products ul li img {
		height: 40px;
		max-height: 40px;
		max-width: 40px;
	}
	.related-products .products ul li a.delete {
		float: right;
		width: 16px;
		height: 16px;
		background: url(./core/admin/template/default/icons/delete.png) no-repeat top right;
		text-decoration: none;
	}
	.related-products .products .addproduct {
		background: #eee;
		padding: 4px;
	}
</style>