<?php

defined('WEBMEX') or die('No direct access.');

class Module_Webmex_Product_Carousel extends Module {

	public $has_settings = true;

	protected $options = array(
        'products' => array()
        );

    public $module_info = array(
	'name' => array(
	    'cs' => 'Carousel z produktů',
	),          
	'description' => array(
	    'cs' => 'Vytvoří carousel z produktů',
	),        
	'version' => '1.0.0',
	'author' => 'Tomas Nikl &copy; Webmex.cz',
	'www' => '<a href="http://www.webmex.cz">www.webmex.cz</a>',
    );

    /**
     * Run after install module.
     */
    public function install($module) {

    	$module->update(array('storage' => serialize($this->options)));
    }

    public function saveSettingForm() {
        if(!empty($_POST)){
            $_POST['products'] = serialize($_POST['products']);
    		$data = (array) $this->getStorage();
    		$data = Arr::overwrite($this->options, $data, $_POST);
    		$this->saveStorage($data);
    		flashMsg(__('msg_saved'));
    	}
    }
    
    public function getSettingForm() {
    	return tpl('webmex_product_carousel.php', array(
    			'module' => $this, 
    			'storage' => $this->getStorage())
    		);
    }
    
}
