<?php defined('WEBMEX') or die('No direct access.');

include APPROOT . 'vendor/phpexcel/PHPExcel.php';


class Controller_Order_export extends AdminController 
{

	protected function generatePhpExcel($year, $month)
	{
		$this->tpl = NULL;

		$date = new DateTime("$year-$month-01");


		// 1) Get data
		$dateFrom = clone $date;
		$dateTo = clone $date; $dateTo->modify('+1 month')->modify('-1 day');
		$orders = Core::$db->order()
					->where('received >= ?', $this->getDateTimeTimestamp($dateFrom))
					->where('received <= ?', $this->getDateTimeTimestamp($dateTo))
					->order('received DESC');


		// 2) Create XLSX file
		$phpExcel = new PHPExcel();
		$sheet = $phpExcel->getActiveSheet();
		$sheet->getStyle("A1:Z1")->getFont()->setBold(true);
		$sheet->getRowDimension(1)->setRowHeight(-1);

		// Header row
		$sheet->setCellValue('A1', 'ID objednávky')
			->setCellValue('B1', 'Číslo faktury')
			->setCellValue('C1', 'Fakturační jméno zákazníka (+firmy)')
			->setCellValue('D1', 'Celková částka objednávky')
			->setCellValue('E1', 'Datum úhrady')
			->setCellValue('F1', 'Způsob úhrady');

		// Set data
		$i = 1;
		$sum = 0;
		foreach ($orders as $order) {
			$i++;
			$sum += $order['total_incl_vat'];

			$invoice = $order->invoice()? $order->invoice()->fetch(): NULL;
			$sheet->setCellValue("A$i", $order['id'])
				->setCellValue("B$i", $invoice? $invoice['invoice_num']: '')
				->setCellValue("C$i", "$order[first_name] $order[last_name]" . ($order['company']? " ($order[ship_company])": ''))
				->setCellValue("D$i", $order['total_incl_vat'])
				->setCellValue("E$i", $order['payment_realized']? PHPExcel_Shared_Date::PHPToExcel( $order['payment_realized'] ): '')
				->setCellValue("F$i", $order->payment['name']);

			$sheet->getStyle("E$i")
				->getNumberFormat()
				->setFormatCode('dd.mm.yyyy');
		}

		$i += 3;
		$sheet->setCellValue("A$i", 'Celkový počet objednávek:');
		$sheet->setCellValue("B$i", count($orders));

		$i += 1;
		$sheet->setCellValue("A$i", 'Celková částka:');
		$sheet->setCellValue("B$i", $sum);

		$sheet->getStyle('A' . ($i-1) . ':A' . $i . '')->getFont()->setBold(true);

		// 3) Return
		return $phpExcel;
	}



	public function excel()
	{
		$this->tpl = NULL;

		$date = (@$_GET['year'] && @$_GET['month'])? new DateTime("$_GET[year]-$_GET[month]-01"): NULL;
		$filetype = @$_GET['filetype']? $_GET['filetype']: 'xlsx';
		$writerName = @$_GET['filetype'] == 'xls'? 'Excel5': 'Excel2007';


		// 1) Check
		if (!$date) {
			flashMsg('Chyba: Nebylo vybráno datum.');
			redirect('admin/orders');
		}


		// 2) Get data
		$phpExcel = $this->generatePhpExcel($date->format('Y'), $date->format('n'));


		// 3) Download
		header('Content-Type: application/other');
		header('Content-Disposition: attachment; filename="objednavky-' . $date->format('Y-m') . '.' . $filetype . '"');
		$objWriter = PHPExcel_IOFactory::createWriter($phpExcel, $writerName);
		$objWriter->save("php://output");
	}



	private function getDateTimeTimestamp($dateTime)
	{
		return strtotime( $dateTime->format('Y-m-d G:i:s') );
	}

}