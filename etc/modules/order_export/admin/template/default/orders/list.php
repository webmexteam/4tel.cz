<?php defined('WEBMEX') or die('No direct access.');
$view->override = false; 
?>



<?php /********************* GET DATA *********************/ ?>
<?php
$orderStats = Core::$db->order()
				->select('MIN(received) AS min_received, MAX(received) AS max_received')
				->order('received ASC')
				->fetch();
$minDate = new DateTime( date('Y-m-d', $orderStats['min_received']) );
$maxDate = new DateTime( date('Y-m-d', $orderStats['max_received']) );
?>



<?php /********************* RENDER EXPORT FORM *********************/ ?>
<?php
$view->slot_start();
?>


<form method="get" action="<?php echo url('admin/order_export/excel/'); ?>" class="form">
	<div class="gridsearch">
		<fieldset class="clearfix label-left">
			<div class="inputwrap clearfix">
				<label>Exportovat objednávky do excelu:</label>
			</div>

			<div class="inputwrap clearfix">
				<label for="oe-year">Rok:</label>
				<select class="input-select" name="year" id="oe-year">
					<?php for($i = $minDate->format('Y'); $i <= $maxDate->format('Y'); $i++): ?>
					<option value="<?php echo $i; ?>"><?php echo $i; ?></option>
					<?php endfor; ?>
				</select>
			</div>

			<div class="inputwrap clearfix">
				<label for="oe-month">Měsíc:</label>
				<select class="input-select" name="month" id="oe-month">
					<?php for($i = 1; $i <= 12; $i++): ?>
					<option value="<?php echo $i; ?>"><?php echo __("webmex_module_order_export_month$i"); ?></option>
					<?php endfor; ?>
				</select>
			</div>

			<div class="inputwrap clearfix">
				<label for="inp-search">Formát:</label>
				<select class="input-select short" name="filetype" id="oe-filetype">
					<option value="xlsx">xlsx</option>
					<option value="xls">xls</option>
				</select>
			</div>

			<button class="button" type="submit">Exportovat</button>
		</fieldset>
	</div>
</form>


<?php
$content = $view->slot_stop();
//**/dump( $view->find("div")->html() ); die;
$view->find(".gridsearch")->parent()->after($content);
?>