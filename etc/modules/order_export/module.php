<?php
defined('WEBMEX') or die('No direct access.');


/**
 * Class Module_Order_export
 *
 * @todo translates
 * @author  Michal Mikoláš <nanuqcz@gmail.com>
 */
class Module_Order_export extends Module 
{
	public $has_settings = false;

	public $module_info = array(
		'name' => array(
			'cs' => 'XLS Export objednávek',
		),
		'description' => array(
			'cs' => 'Umožní exportovat vybrané objednávky pro Excel.',
		),
		'version' => '1.0.0',
		'author' => 'Michal Mikoláš &copy; Webmex.cz',
		'www' => '<a href="http://www.webmex.cz">www.webmex.cz</a>',
	);



	/**
	 * Run after install module.
	 *
	 * @param $module
	 */
	public function install($module) 
	{
	}



	/**
	 * Run at every request
	 */
	public function setup() 
	{
		
	}



	/********************* INTERNAL *********************/



	public function renderTpl($filename, $data, $encoding = 'UTF-8')
	{
		// Discad previous content (white spaces etc.)
		while (ob_get_level()) {
			ob_end_clean();
		}

		// Get template content
		ob_start();
		echo tpl($filename, $data, 'default');
		$content = ob_get_clean();

		// Change encoding
		if ($encoding != 'UTF-8') {
			$content = iconv('UTF-8', $encoding, $content);
		}

		// Render
		echo $content;
		die;
	}
	
}