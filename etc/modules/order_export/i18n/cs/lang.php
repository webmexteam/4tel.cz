<?php defined('SQC') or die('No direct access.');

Core::$lang += array(
	'webmex_module_order_export_month1'	=> 'Leden',
	'webmex_module_order_export_month2'	=> 'Únor',
	'webmex_module_order_export_month3'	=> 'Březen',
	'webmex_module_order_export_month4'	=> 'Duben',
	'webmex_module_order_export_month5'	=> 'Květen',
	'webmex_module_order_export_month6'	=> 'Červen',
	'webmex_module_order_export_month7'	=> 'Červenec',
	'webmex_module_order_export_month8'	=> 'Srpen',
	'webmex_module_order_export_month9'	=> 'Září',
	'webmex_module_order_export_month10'	=> 'Říjen',
	'webmex_module_order_export_month11'	=> 'Listopad',
	'webmex_module_order_export_month12'	=> 'Prosinec',
);
