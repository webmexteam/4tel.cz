<?php defined('WEBMEX') or die('No direct access.'); ?>

<div class="form">
	<div class="clearfix">
		<fieldset style="width:300px">
			<?php
			echo forminput('text', 'label', $storage['label'], array(
				'label' => __('webmex_module_stock_info_label'),
			));
			echo forminput('text', 'limits', $storage['limits'], array(
				'label' => __('webmex_module_stock_info_limits'),
			));
			echo forminput('select', 'availabilities[]', NULL/*$storage['availabilities']*/, array(
				'label' => __('webmex_module_stock_info_availabilities'),
				'options' => $availabilityPairs,
				'multiple' => 'multiple',
			));
			?>
		</fieldset>

		<script>
		<?php /* hotfix for forminput multiselect default value bug */ ?>
		jQuery(function(){
			<?php if(is_array($storage['availabilities'])): ?>
				<?php foreach($storage['availabilities'] as $key => $paymentId): ?>
				jQuery("select[name='availabilities[]'] option[value=<?php echo $paymentId; ?>]").attr('selected', 'selected');
				<?php endforeach; ?>
			<?php endif; ?>
		});
		</script>
	</div>
</div>