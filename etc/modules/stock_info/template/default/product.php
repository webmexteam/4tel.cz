<?php defined('WEBMEX') or die('No direct access.');
$view->override = false; 
?>



<?php /********************* GET DATA *********************/ ?>
<?php
$storage = Core::module('stock_info')->getStorage();

$availabilities = $storage['availabilities'];
$limits = preg_split('/, ?/', $storage['limits']);
?>



<?php /********************* RENDER *********************/ ?>
<?php if ($product['stock'] && $limits): ?>
	<?php if (!$availabilities || in_array($product['availability_id'], $availabilities)): ?>
		<?php
		$view->slot_start();

		$stockInfo = '&lt; ' . $limits[0];
		foreach ($limits as $limit) {
			if ((int)$product['stock'] > (int)$limit) {
				$stockInfo = '&gt; ' . $limit;
			}
		}
		?>

		<div class="stock-info">
			<span><?php echo $storage['label'] . ' ' . $stockInfo . ' ' . __('webmex_module_stock_info_pieces'); ?></span>
		</div>

		<?php
		$content = $view->slot_stop();
		$view->find(".productdetail .productinfo form.basket")->before($content);  // classic default template
		?>
	<?php endif; ?>
<?php endif; ?>
