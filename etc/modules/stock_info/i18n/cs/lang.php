<?php defined('SQC') or die('No direct access.');

Core::$lang += array(
	'webmex_module_stock_info_label' => 'Text před počtem kusů',
	'webmex_module_stock_info_limits' => 'Limity počtu kusů',
	'webmex_module_stock_info_availabilities' => 'Zobrazovat pouze u dostupnosti',
	'webmex_module_stock_info_pieces' => 'ks',
);
