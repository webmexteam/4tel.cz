<?php defined('WEBMEX') or die('No direct access.');


/**
 * Class Module_Stock_info
 * 
 * @author  Michal Mikoláš <nanuqcz@gmail.com>
 * @copyright  Webmex s.r.o.
 */
class Module_Stock_info extends Module 
{
	public $has_settings = true;

	protected $options = array(
		'label' => 'Skladem',
		'limits' => '5,10,20,50,100,200,500,1000',
		'availabilities' => array(),
	);

	public $module_info = array(
		'name' => array(
			'cs' => 'Stav skladu',
		),
		'description' => array(
			'cs' => 'Informace o počtu kusů skladem v detailu produktu.',
		),
		'version' => '1.0.0',
		'author' => 'Michal Mikoláš &copy; Webmex.cz',
		'www' => '<a href="http://www.webmex.cz">www.webmex.cz</a>',
	);



	/**
	 * Run after install module.
	 */
	public function install($module) 
	{
	}



	public function setup() 
	{
		View::addCSS(
			'front', 
			DOCROOT . 'etc/modules/stock_info/template/' . Core::config('template') . '/css/stock-info.css'
		);
	}



	/**
	 * Save settings form
	 */
	public function saveSettingForm() {
		if(!empty($_POST)){
			$data = (array) $this->getStorage();
			$data = Arr::overwrite($this->options, $data, $_POST);
			$data['availabilities'] = $_POST['availabilities'];
			$this->saveStorage($data);
			flashMsg(__('msg_saved'));
		}
	}



	/**
	 * @return View
	 */
	public function getSettingForm() {
		$availabilityPairs = array();
		foreach (Core::$db->availability() as $availability) {
			$availabilityPairs[$availability['id']] = $availability['name'];
		}

		return tpl('module_settings_stock_info.php', array(
			'module' => $this, 
			'storage' => $this->getStorage(),
			'availabilityPairs' => $availabilityPairs,
		));
	}



	/********************* Internal *********************/



	public function flashMsg($msg)
	{
		if (!is_array($_SESSION['flash_msg'])) {
			$_SESSION['flash_msg'] = array(
				'type' => 'ok',
				'msg' => is_object($msg) ? $msg->__toString() : $msg,
			);

		} else {
			$_SESSION['flash_msg']['msg'] .= '<p>' . $msg;
		}
	}
	
}
