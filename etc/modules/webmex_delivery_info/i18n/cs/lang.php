<?php defined('SQC') or die('No direct access.');

Core::$lang += array(
	'web_module_delivery_order_to_hour'	=> 'Příjem objednávek do',
	'web_module_delivery_delay' => 'Prodlení ve dnech (např. v období Vánoc, kdy pošta nestíhá)',
	'web_module_delivery_today_text' => 'Zobrazovaný text pro dopravu doručenou ještě dnes. Nechte prázdné pro zobrazení datumu',
	'web_module_delivery_tommorow_text' => 'Zobrazovaný text pro dopravu doručenou zítra. Nechte prázdné pro zobrazení datumu',
	'web_module_delivery_today_color' => 'Barva pro text dopravy dnes',
	'web_module_delivery_tommorow_color' => 'Barva pro text dopravy zítra',
	'web_module_delivery_title' => 'Kdy bude zboží u Vás?',
	'web_module_delivery_info_text' => 'Pokud zboží objednáte dnes do',
);
