<?php defined('SQC') or die('No direct access.');

Core::$lang += array(
	'web_module_delivery_order_to_hour'	=> 'Príjem objednávok do',
	'web_module_delivery_delay' => 'Omeškanie v dňoch (napr. v období Vianoc , kedy pošta nestíha)',
	'web_module_delivery_today_text' => 'Zobrazovaný text pre dopravu doručenú ešte dnes. Nechajte prázdne pre zobrazenie dátumu',
	'web_module_delivery_tommorow_text' => 'Zobrazovaný text pre dopravu doručenú zajtra. Nechajte prázdne pre zobrazenie dátumu',
	'web_module_delivery_today_color' => 'Farba pre text dopravy dnes',
	'web_module_delivery_tommorow_color' => 'Farba pre text dopravy zajtra',
	'web_module_delivery_title' => 'Kedy bude tovar u Vás?',
	'web_module_delivery_info_text' => 'Pokiaľ tovar objednáte dnes do',
);
