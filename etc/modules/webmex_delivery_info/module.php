<?php
defined('WEBMEX') or die('No direct access.');

/**
 * Class Module_Webmex_Delivery_Info
 */
class Module_Webmex_Delivery_Info extends Module {

	public $has_settings = true;

	protected $options = array(
		'order_to_hour' 		=> '16:00',
		'delay'					=> 0,
		'today_text'			=> 'Dnes',
		'tommorow_text'			=> 'Zítra',
		'today_color'			=> '#398000',
		'tommorow_color'		=> '#398000'
	);

    public $module_info = array(
		'name' => array(
			'cs' => 'Kdy bude zboží u zákazníka',
		),
		'description' => array(
			'cs' => 'Kalkulace doby doručení zboží',
		),
		'version' => '1.0.0',
		'author' => 'Tomas Nikl &copy; Webmex.cz',
		'www' => '<a href="http://www.webmex.cz">www.webmex.cz</a>',
    );

	/**
	 * Run after install module.
	 *
	 * @param $module
	 */
	public function install($module) {
    	$module->update(array('storage' => serialize($this->options)));

		try {
			Core::$db_inst->schema->addColumns('delivery',array('delivery_days' => array(
				'type' => 'integer',
				'not_null' => true,
				'default' => 0
			)));
			Core::$db_inst->schema->addColumns('delivery',array('delivery_on_weekend' => array(
				'type' => 'integer',
				'length' => 1,
				'not_null' => true,
				'default' => 0
			)));
			Core::$db_inst->schema->addColumns('delivery',array('delivery_on_holiday' => array(
				'type' => 'integer',
				'length' => 1,
				'not_null' => true,
				'default' => 0
			)));
			Core::$db_inst->schema->addColumns('delivery',array('delivery_included' => array(
				'type' => 'integer',
				'length' => 1,
				'not_null' => true,
				'default' => 1
			)));
			Core::$db_inst->schema->addColumns('availability',array('delivery_included' => array(
				'type' => 'integer',
				'length' => 1,
				'not_null' => true,
				'default' => 1
			)));
		} catch (Exception $e) {}
    }

	public function setup() {
		View::addCSS('front', DOCROOT.'etc/modules/webmex_delivery_info/template/'.Core::config('template').'/resources/css/screen.less.css');
	}

	/**
	 * Save settings form
	 */
	public function saveSettingForm() {
    	if(!empty($_POST)){
    		$data = (array) $this->getStorage();
    		$data = Arr::overwrite($this->options, $data, $_POST);
    		$this->saveStorage($data);
    		flashMsg(__('msg_saved'));
    	}
    }

	/**
	 * @return View
	 */
	public function getSettingForm() {
    	return tpl('webmex_delivery_info_settings.php', array(
    			'module' => $this, 
    			'storage' => $this->getStorage())
    		);
    }
    
}
