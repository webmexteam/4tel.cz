<?php

function webmex_delivery_info_get_deliveries($customer = null, $total_price = null, $total_weight = null, $storage, $availability)
{
	$group_ids = array(0, (int) Core::config('customer_group_default'));

	if ($customer) {
		$group_ids[] = (int) Core::config('customer_group_registered');

		if (($gid = (int) $customer['customer_group_id'])) {
			$group_ids[] = $gid;
		}
	}

	$deliveries = array();

	$next_day = (strtotime(date('Y-m-d-H:i')) < strtotime(date('Y-m-d').'-'.$storage['order_to_hour'])) ? TRUE : FALSE;

	foreach (Core::$db->delivery()->where('customer_group_id', $group_ids)->where('delivery_included', 1)->order('ord') as $del) {
		$st = true;

		if ($del['weight_min'] !== null && $del['weight_min'] > $total_weight) {
			$st = false;
		}

		if ($del['weight_max'] !== null && $del['weight_max'] < $total_weight) {
			$st = false;
		}

		if ($del['price_min'] !== null && $del['price_min'] > $total_price) {
			$st = false;
		}

		if ($del['price_max'] !== null && $del['price_max'] < $total_price) {
			$st = false;
		}

		if ($st) {
			$plus_days = $storage['delay'] + $availability['days'] + $del['delivery_days'];


			if(!$next_day) {
				$plus_days += 1;
			}

			$date = date('j.n.Y', strtotime('+'.$plus_days.' days'));

			if(!$del['delivery_on_weekend'] && $del['delivery_on_holiday']) {
				$start = strtotime(date('j.n.Y'));
				$end = strtotime($date);
				$date = webmex_delivery_info_calculate_only_weekend($start, $end);
			}

			if(!$del['delivery_on_weekend'] && !$del['delivery_on_holiday']) {
				$start = strtotime(date('j.n.Y'));
				$end = strtotime($date);
				$date = webmex_delivery_info_calculate_weekend_and_holiday($start, $end);
			}

			if($del['delivery_on_weekend'] && !$del['delivery_on_holiday']) {
				$start = strtotime(date('j.n.Y'));
				$end = strtotime($date);
				$date = webmex_delivery_info_calculate_only_holiday($start, $end);
			}

			if(strtotime($date) == strtotime(date('j.n.Y')) && !empty($storage['today_text'])) {
				$date = '<strong style="color:'.$storage['today_color'].'">'.$storage['today_text'].'</strong>';
			}

			if(strtotime('-1 day', strtotime($date)) == strtotime(date('j.n.Y')) && !empty($storage['tommorow_text'])) {
				$date = '<strong style="color:'.$storage['tommorow_color'].'">'.$storage['tommorow_text'].'</strong>';
			}

			$deliveries[strtotime($date).rand(0,999999)] = array(
				'name' => $del['name'],
				'delivery_to' => $date
			);
		}
	}

	return array(
		'deliveries' => $deliveries,
		'order_to_info' => $next_day
	);
}

function webmex_delivery_info_calculate_only_holiday($start, $end) {
	$dStart = new DateTime(date('j-n-Y',$start));
	$dEnd  = new DateTime(date('j-n-Y',$end));
	$dDiff = $dStart->diff($dEnd);
	$days_diff = $dDiff->days;

	for ($i=0; $i < $days_diff; $i++) {
		$d = date('j.n.Y', strtotime('+'.$i.' days', $start));
		if(webmex_delivery_info_is_holiday($d)) {
			$days_diff = $days_diff+1;
		}
	}
	if(webmex_delivery_info_is_holiday(date('j.n.Y', strtotime('+'.$days_diff.' days', $start)))) {
		if(webmex_delivery_info_is_holiday(date('j.n.Y', strtotime('+'.($days_diff+1).' days', $start)))) {
			if(webmex_delivery_info_is_holiday(date('j.n.Y', strtotime('+'.($days_diff+2).' days', $start)))) {
				if(webmex_delivery_info_is_holiday(date('j.n.Y', strtotime('+'.($days_diff+3).' days', $start)))) {
					if(webmex_delivery_info_is_holiday(date('j.n.Y', strtotime('+'.($days_diff+4).' days', $start)))) {
						return date('j.n.Y', strtotime('+'.($days_diff+5).' days', $start));
					}else{
						return date('j.n.Y', strtotime('+'.($days_diff+4).' days', $start));
					}
				}else{
					return date('j.n.Y', strtotime('+'.($days_diff+3).' days', $start));
				}
			}else{
				return date('j.n.Y', strtotime('+'.($days_diff+2).' days', $start));
			}
		}else {
			return date('j.n.Y', strtotime('+'.($days_diff+1).' days', $start));
		}
	}else{
		return date('j.n.Y', strtotime('+'.$days_diff.' days', $start));
	}
}

function webmex_delivery_info_calculate_weekend_and_holiday($start, $end) {
	$dStart = new DateTime(date('j-n-Y',$start));
	$dEnd  = new DateTime(date('j-n-Y',$end));
	$dDiff = $dStart->diff($dEnd);
	$days_diff = $dDiff->days;

	for ($i=0; $i < $days_diff; $i++) {
		$d = date('j.n.Y', strtotime('+'.$i.' days', $start));
		if(webmex_delivery_info_is_weekend($d) || webmex_delivery_info_is_holiday($d)) {
			$days_diff = $days_diff+1;
		}
	}
	if(webmex_delivery_info_is_weekend(date('j.n.Y', strtotime('+'.$days_diff.' days', $start))) || webmex_delivery_info_is_holiday(date('j.n.Y', strtotime('+'.$days_diff.' days', $start)))) {
		if(webmex_delivery_info_is_weekend(date('j.n.Y', strtotime('+'.($days_diff+1).' days', $start))) || webmex_delivery_info_is_holiday(date('j.n.Y', strtotime('+'.($days_diff+1).' days', $start)))) {
			if(webmex_delivery_info_is_holiday(date('j.n.Y', strtotime('+'.($days_diff+2).' days', $start)))) {
				if(webmex_delivery_info_is_holiday(date('j.n.Y', strtotime('+'.($days_diff+3).' days', $start)))) {
					if(webmex_delivery_info_is_holiday(date('j.n.Y', strtotime('+'.($days_diff+4).' days', $start)))) {
						return date('j.n.Y', strtotime('+'.($days_diff+5).' days', $start));
					}else{
						return date('j.n.Y', strtotime('+'.($days_diff+4).' days', $start));
					}
				}else{
					return date('j.n.Y', strtotime('+'.($days_diff+3).' days', $start));
				}
			}else{
				return date('j.n.Y', strtotime('+'.($days_diff+2).' days', $start));
			}

		}else {
			return date('j.n.Y', strtotime('+'.($days_diff+1).' days', $start));
		}
	}else{
		return date('j.n.Y', strtotime('+'.$days_diff.' days', $start));
	}
}

function webmex_delivery_info_calculate_only_weekend($start, $end) {
	$dStart = new DateTime(date('j-n-Y',$start));
	$dEnd  = new DateTime(date('j-n-Y',$end));
	$dDiff = $dStart->diff($dEnd);
	$days_diff = $dDiff->days;

	for ($i=0; $i < $days_diff; $i++) {
		$d = date('j.n.Y', strtotime('+'.$i.' days', $start));
		if(webmex_delivery_info_is_weekend($d)) {
			$days_diff = $days_diff+1;
		}
	}
	if(webmex_delivery_info_is_weekend(date('j.n.Y', strtotime('+'.$days_diff.' days', $start)))) {
		if(webmex_delivery_info_is_weekend(date('j.n.Y', strtotime('+'.($days_diff+1).' days', $start)))) {
			return date('j.n.Y', strtotime('+'.($days_diff+2).' days', $start));
		}else {
			return date('j.n.Y', strtotime('+'.($days_diff+1).' days', $start));
		}
	}else{
		return date('j.n.Y', strtotime('+'.$days_diff.' days', $start));
	}
}

function webmex_delivery_info_is_holiday($date) {
	$years = array(date('Y'), date('Y', strtotime('+1 year')));
	$days = array(
		'1.1.',
		'1.5.',
		'8.5.',
		'5.6.',
		'6.6.',
		'28.9.',
		'28.10.',
		'17.11.',
		'24.12.',
		'25.12.',
		'26.12.',
		date('j.n', strtotime('+1 day', easter_date(2015))), //velikonocni pondeli
	);

	foreach($years as $year) {
		foreach ($days as $day) {
			if(strtotime($day.$year) == strtotime($date)) {
				return true;
			}
		}
	}
	return false;
}

function webmex_delivery_info_is_weekend($date) {
	return (date('N', strtotime($date)) >= 6);
}

?>