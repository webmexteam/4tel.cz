<?php 

class Controller_Webmex_Delivery_Info extends Controller_Default 
{
	private $storage;



	public function table($id, $attribute = NULL) 
	{
		$this->storage = Core::module('area_units')->getStorage();

		$product = Core::$db->product[$id];

		$this->tpl = 'empty_layout.latte';
		$this->content = tpl(
			'table.latte', 
			array(
				'product' => $product,
				'attribute' => $attribute,
			)
		);
	}

}