<?php defined('WEBMEX') or die('No direct access.'); ?>

<div class="form">
	<div class="clearfix">
		<fieldset style="width:300px">
			<?php
			echo forminput('text', 'order_to_hour', $storage['order_to_hour'], array('label' => __('web_module_delivery_order_to_hour')))
				.forminput('text', 'delay', $storage['delay'], array('label' => __('web_module_delivery_delay')))
				.forminput('text', 'today_text', $storage['today_text'], array('label' => __('web_module_delivery_today_text')))
				.forminput('text', 'tommorow_text', $storage['tommorow_text'], array('label' => __('web_module_delivery_tommorow_text')))
				.forminput('text', 'today_color', $storage['today_color'], array('label' => __('web_module_delivery_today_color')))
				.forminput('text', 'tommorow_color', $storage['tommorow_color'], array('label' => __('web_module_delivery_tommorow_color')))
			?>
		</fieldset>
	</div>
</div>