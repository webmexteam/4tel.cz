<?php defined('SQC') or die('No direct access.');

Core::$lang += array(
	'area_units_tab_title'	=> 'Plošné jednotky',
	'area_units_allowed' => 'Povolit plošné jednotky',
	'area_units_unit_1' => 'Měrná jednotka 1',
	'area_units_unit_2' => 'Měrná jednotka 2',
	'area_units_lock_unit_1' => 'Měrná jednotka 1 je uzamčena',
	'area_units_lock_unit_2' => 'Měrná jednotka 2 je uzamčena',
	'area_units_default_unit_1' => 'Výchozí hodnota měrné jednotky 1',
	'area_units_default_unit_2' => 'Výchozí hodnota měrné jednotky 2',
);
