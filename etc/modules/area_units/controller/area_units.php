<?php defined('WEBMEX') or die('No direct access.');

class Controller_Area_Units extends Controller_Default {

	private $storage;

	// pages
	public function a($id, $sef_url = null)
	{
		$page = Core::$db->page[(int) $id];
		$products = $basket_products = $product_tpl = $file_content = $producers = $features = null;

		$is_basket = (Core::config('page_basket') == $page['id']);
		$is_order_step1 = (Core::config('page_order_step1') == $page['id']);
		$is_order_step2 = (Core::config('page_order_step2') == $page['id']);
		$is_order_finish = (Core::config('page_order_finish') == $page['id']);
		$is_search = (Core::config('page_search') == $page['id']);
		$is_sitemap = (Core::config('page_sitemap') == $page['id']);

		$canonical = null;

		if (!empty($_GET)) {
			$d = $_GET;

			unset($d['page'], $d['uri'], $d['q']);

			if (!empty($d)) {
				$canonical = url(true, false, true);
			}
		}

		View::$global_data['canonical'] = $canonical;

		if ($page && $page['status'] == 1 && ($sef_url === null || $page['sef_url'] == $sef_url)) {

			if ($page['external_url']) {
				redirect(ltrim($page['external_url'], '/'));
			}

			setMeta($page);

			Core::$current_page = $page;

			$expanded = array();

			if (Core::$current_page && !Core::$current_page['parent_page']) {
				$expanded[] = Core::$current_page['id'];
			} else if (Core::$current_page) {
				$parent_id = Core::$current_page['parent_page'];
				$_page = null;

				$expanded[] = Core::$current_page['id'];

				while ($parent_id) {
					$_page = Core::$db->page[$parent_id];

					if (!$_page) {
						break;
					}

					$expanded[] = $parent_id;

					$parent_id = $_page['parent_page'];
				}
			}

			Core::$active_pages = $expanded;

			if ((int) $page['authorization'] && !Customer::$logged_in) {
				$this->content->content = tpl('page_login.php', array(
					'page' => $page
				));
				return;
			}

			if (!empty($page['content_file']) && file_exists(DOCROOT . 'etc/pages/' . $page['content_file'])) {
				$file_content = $this->getPageContent(DOCROOT . 'etc/pages/' . $page['content_file']);
			}

			if ($is_basket) {
				$this->_basket();

				$basket_products = Area_Units_Basket::products();
			}

			if ($is_order_step1 && empty($_GET['payment'])) {

				$basket_products = Area_Units_Basket::products();

				if (!$basket_products) {
					redirect(PAGE_BASKET);
				}

				$this->_order_step1();

			} else if ($is_order_step2 && empty($_GET['payment'])) {

				$basket_products = Area_Units_Basket::products();

				if (!$basket_products) {
					redirect(PAGE_BASKET);
				}

				$this->_order_step2();

			} else if ($is_order_finish && !empty($_GET['finish'])) {
				$basket_products = Area_Units_Basket::products();
				$order = Core::$db->order[(int) $_SESSION['order_id']];

				if ($order && $order['received'] == $_GET['finish']) {
					View::$global_data['order'] = $order;
				} else {
					redirect(PAGE_ORDER);
				}
			}

			$order_by = 'default';
			$order_dir = 'ASC';

			if ($page['product_columns'] == -1) {
				$view = 'list';
			} else {
				$view = 'pictures';
			}

			if ($page['product_columns'] == -1) {
				$product_tpl = 'product_list.php';
			} else {
				$product_tpl = 'product_' . ($page['product_columns'] ? $page['product_columns'] : 2) . 'columns.php';
			}

			if ((int) $page['enable_filter']) {

				foreach ($_GET as $k => $v) {
					if ($k == 'view-list') {
						$view = 'list';
					} else if ($k == 'view-pictures') {
						$view = 'pictures';
					} else if ($k == 'dir-asc') {
						$order_dir = 'ASC';
					} else if ($k == 'dir-desc') {
						$order_dir = 'DESC';
					}
				}

				$view_tpl = $view == 'pictures' ? ($page['product_columns'] > 0 ? $page['product_columns'] : 2) . 'columns' : 'list';

				$product_tpl = 'product_' . $view_tpl . '.php';

				if (isSet($_GET['order'])) {
					$order_by = $_GET['order'];
					if (!in_array($order_by, array('name', 'price', 'default'))) {
						$order_by = 'default';
					}
				}
			}

			View::$global_data['_view'] = $view;
			View::$global_data['_order'] = $order_by;
			View::$global_data['_order_dir'] = $order_dir;

			if ($page['products'] == 1 && !$is_search) {

				if ((int) $page['inherit_products']) {
					$this->getSubpages($page['id'], $ids);

					if(Core::config('hide_no_stock_products')) {
						$products = Core::$db->product()->where('stock > 0 AND hide_product_on_zero_stock = 1 OR hide_product_on_zero_stock != 1')->where('id IN (' . Core::$db->product_pages()->where('page_id', $ids)->select('product_id') . ') OR id IN (' . $page->product_pages()->select('product_id') . ')');
					}else{
						$products = Core::$db->product()->where('id IN (' . Core::$db->product_pages()->where('page_id', $ids)->select('product_id') . ') OR id IN (' . $page->product_pages()->select('product_id') . ')');
					}
				} else {
					if(Core::config('hide_no_stock_products')) {
						$products = Core::$db->product()->where('stock > 0 AND hide_product_on_zero_stock = 1 OR hide_product_on_zero_stock != 1')->where('id', $page->product_pages()->select('product_id'));
					}else{
						$products = Core::$db->product()->where('id', $page->product_pages()->select('product_id'));
					}
				}

				$products->where('status', 1);

				if ((int) $page['enable_filter']) {
					$_products = clone $products;
				}

				if ((int) $page['enable_filter'] && !empty($_GET['p'])) {
					$products->where('id', Core::$db->product_pages()->where('page_id', $_GET['p'])->select('product_id'));
				}

				if ($page['enable_filter'] && !empty($_GET['f']) && ($featureset = $page->featureset) && $featureset['id']) {
					$tbl_name = 'features_' . $featureset['id'];

					$_ids = Core::$db->$tbl_name();

					foreach ($_GET['f'] as $fid => $fvalue) {
						if ($fvalue !== '') {
							$_ids->where('f' . $fid, $fvalue);
						}
					}

					if ($_ids->getWhere()) {
						$products->where('id', $_ids->select('product_id'));
					}
				}

				if ($page['enable_filter'] && isSet($_products)) {
					$product_ids = $page_ids = array();

					foreach ($_products->select('id') as $priduct_id) {
						$product_ids[] = $priduct_id['id'];
					}

					$producers = Core::$db->page()->where('menu', 3)->where('status', 1)->where('id', Core::$db->product_pages()->where('product_id', $product_ids)->select('page_id'))->order('position ASC');
				}

				if (Core::$is_premium && $page['enable_filter'] && $page->featureset['id']) {
					foreach ($page->featureset->feature()->order('position ASC') as $feature) {

						if ((int) $feature['filter']) {
							$features[$feature['id']] = array(
								'name' => $feature['name'],
								'options' => $this->getFeatureOptions($page->featureset, $feature, $product_ids)
							);
						}
					}
				}

				Event::run('Controller_Default.page::products', $products);

				if (!(int) $page['enable_filter']) {
					$products->order('position ASC, promote DESC');
				} else {

					$orderby = $order_by . ' ' . $order_dir;

					if ($order_by == 'default') {
						$orderby = 'position ' . $order_dir;

						$deforder = Core::config('default_order');

						if (!is_null($deforder) && preg_match('#^(.+)(ASC|DESC)$#', $deforder, $m)) {
							$odir = 'ASC';
							if ($m[2] == 'ASC' && $order_dir == 'DESC') $odir = 'DESC';
							else if ($m[2] == 'DESC' && $order_dir == 'ASC') $odir = 'DESC';

							if ($m[1] == 'position ') $orderby = 'position ' . $odir;
							else if ($m[1] == 'price ') $orderby = 'price ' . $odir;
							else if ($m[1] == 'name ') $orderby = 'name ' . $odir;
							else if ($m[1] == 'position price ') $orderby = 'position ' . $odir . ', price ' . $odir;
						}
					}

					$products->order($orderby . ', promote DESC');
				}

				$products->limit(pgLimit(), pgOffset());

				$products_count = Core::$db->product;

				if ($products->getWhere()) {
					$products_count->where($products->getWhere());
				}

				$products_count = $products_count->select('COUNT(id) as total_count')->fetch();
				$products_count = (int) $products_count['total_count'];
			}

			if ($is_search && !empty($_GET['q'])) {
				$q = trim($_GET['q']);

				$products = Search::findProducts($q, $order_by, $order_dir);

				if ($products) {
					if ($page['enable_filter']) {
						$_products = clone $products;
					}

					if ($page['enable_filter'] && !empty($_GET['p'])) {
						$products->where('id', Core::$db->product_pages()->where('page_id', $_GET['p'])->select('product_id'));
					}

					if ($page['enable_filter'] && isSet($_products)) {
						$page_ids = array();

						foreach (Core::$db->product_pages()->where('product_id', $_products->select('id'))->select('page_id') as $pid) {
							$page_ids[] = $pid['page_id'];
						}

						$producers = Core::$db->page()->where('menu', 3)->where('status', 1)->where('id', $page_ids);
					}

					Event::run('Controller_Default.page::products', $products);

					$products->limit(pgLimit(), pgOffset());

					$products_count = Core::$db->product;

					if ($products->getWhere()) {
						$products_count->where($products->getWhere());
					}

					$products_count = $products_count->select('COUNT(id) as total_count')->fetch();
					$products_count = (int) $products_count['total_count'];
				} else {
					$products = array();
					$products_count = 0;
				}
			}

			// set layout (columns)
			$this->content = tpl('default_' . ($page['layout_columns'] ? $page['layout_columns'] : 3) . 'columns.php');

			$subpages = Core::$db->page()->where('parent_page', $page['id'])->where('status', 1)->order('position ASC');

			if ($page['subpages'] == 'news') {
				$subpages->order('pubdate DESC');
			}

			$page_action = $page_action_top = null;

			if ($is_basket) {
				$page_action = tpl('basket.php', array('basket_products' => $basket_products));
			} else if ($is_order_finish) {
				$page_action = tpl('order_finish.php', array('basket_products' => $basket_products));
			} else if ($is_order_step1 && !empty($_GET['payment'])) {
				$page_action = $this->_order_payment($_GET['id'], $_GET['hash']);
			} else if ($is_order_step1) {
				$page_action = tpl('order_step1.php', array('basket_products' => $basket_products));
			} else if ($is_order_step2) {
				$page_action = tpl('order_step2.php', array('basket_products' => $basket_products));
			} else if ($is_sitemap) {
				$page_action = tpl('sitemap.php');
			} else if ($is_search) {
				$page_action_top = tpl('search_results.php', array('products' => $products, 'total_products' => $products_count));
			}

			// set content
			$this->content->content = tpl('page.php', array(
				'page' => $page,
				'page_file_content' => $file_content,
				'products' => $products,
				'products_count' => $products_count,
				'product_tpl' => $product_tpl,
				'subpages' => $subpages,
				'is_basket' => $is_basket,
				'is_order_step1' => $is_order_step1,
				'is_order_step2' => $is_order_step2,
				'is_order_finish' => $is_order_finish,
				'is_search' => $is_search,
				'is_sitemap' => $is_sitemap,
				'basket_products' => $basket_products,
				'producers' => $producers,
				'features' => $features,
				'page_action_top' => $page_action_top,
				'page_action' => $page_action
			));
		} else {
			Core::show_404();
		}
	}

	public function buy() {

		$this->storage = Core::module('area_units')->getStorage();

		Event::add('Controller::construct', array($this, 'init'));
		$controller = new Controller_Default;

		if (!empty($_POST['product_id']) && is_numeric($_POST['product_id'])) {

			$result = Area_Units_Basket::add((int) $_POST['product_id'], !empty($_POST['attributes']) ? $_POST['attributes'] : null, !empty($_POST['qty']) && is_numeric($_POST['qty']) ? (int) $_POST['qty'] : 1);

			if ($result !== true && is_numeric($result)) {
				View::$global_data['basket_error'] = __($result > 0 ? 'max_quantity' : 'basket_no_stock', $result);
			}

			redirect(PAGE_BASKET);
		}
	}

	public function recalculate_basket() {

		$this->tpl = null;

		// Rozparsujeme $_POST data
		$post = array();
		parse_str($_POST['data'], $post);

		// Aktualizujeme polozky v kosiku
		$product_data = array();
		foreach($post['qty'] as $_id => $_qty) {
			if((int) $_qty <= 0){
				$_qty = 1;
			}
			$product_data[$_id] = $_qty;
		}

		Area_Units_Basket::update($product_data);

		$total = Area_Units_Basket::total();
		$data = array();

		foreach(Area_Units_Basket::products() as $product){

			ob_start();
			echo price_vat(array($product['product'], ($product['price'] * $product['qty'])));
			$product_total_price = ob_get_contents();
			ob_clean();

			$data['products'][$product['id']] = array(
				'id'       => $product['id'],
				'price'    => price_vat(array($product['product'], $product['price'])),
				'quantity' => $product['qty'],
				'total'    => $product_total_price,
				'qty'	   => $product_data[$product['id']]
			);
		}

		if((int) Core::config('vat_payer')){
			ob_start();
			echo price($total->subtotal);
			$subtotal = ob_get_contents();
			ob_clean();

			$data['subtotal'] = $subtotal;
		}

		if(Basket::voucher()){
			ob_start();
			echo '-'.price($total->discount);
			$voucher = ob_get_contents();
			ob_clean();

			$data['voucher'] = $voucher;
		}

		ob_start();
		echo price($total->total);
		$total_price = ob_get_contents();
		ob_clean();

		$data['total'] = $total_price;

		echo json_encode($data);
	}
}



