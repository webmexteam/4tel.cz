<?php
$storage = Core::module('area_units')->getStorage();
$view->override = false;

if($basket_products !== null && count($basket_products))
	$total = Area_Units_Basket::total();

$view->find('tfoot .subtotal .value')->html(price($total->subtotal));
$view->find('tfoot .voucher .value')->html('-'.price($total->discount));
$view->find('tfoot .total .value')->html(price($total->total));
$view->find('.ajax-qty')->removeClass('ajax-qty')->addClass('ajax-qty-new');

$i=0;
foreach($basket_products as $product){

	$product_name = $product['product']['name'].' '.$product['product']['nameext'];
//		$produc_price = price_vat(array($product['product'], $product['price']));
//		$product_total = price_vat(array($product['product'], ($product['price'] * $product['qty'])));

	if($product['basket_product']['area_units_unit_1'] && $product['basket_product']['area_units_unit_2']){
		$product_name .= ' ('.$product['basket_product']['area_units_unit_1'].$product['product']['area_units_unit_1'] . ' x '.$product['basket_product']['area_units_unit_2'].$product['product']['area_units_unit_2'].')';
//			$product_price = price_vat(array($product['product'], $product['price'] * (float) $product['basket_product']['area_units_unit_2'] * (float) $product['basket_product']['area_units_unit_1']));
//			$product_total = price_vat(array($product['product'], ($product['price'] * $product['qty'] * (float) $product['basket_product']['area_units_unit_2'] * (float) $product['basket_product']['area_units_unit_1'])));
	}

	$view->find('.basket .tablewrap table tbody tr:nth-child('.$i.') .name .inner-name')->html($product_name);
//		$view->find('.basket .tablewrap table tbody tr:nth-child('.$i.') .price')->html($product_price);
//		$view->find('.basket .tablewrap table tbody tr:nth-child('.$i.') .total')->html($product_total);
	$i++;
}
?>