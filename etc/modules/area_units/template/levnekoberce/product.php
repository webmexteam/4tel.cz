<?php
	$storage = Core::module('area_units')->getStorage();
	$view->override = false;

	$view->find('form.basket')->attr('action', url('area_units/buy'));


	$view->slot_start();

	// Pokud u produktu neni cena, nezobrazujeme
	if(!SHOW_PRICES || $product['price'] == null || (float) $product['price'] == 0 || !$product['area_units_allowed'])
		return false;
?>

	<?php if($product['area_units_lock_unit_1']):?>
		<input type="hidden" name="area_units_unit_1" value="<?php echo $product['area_units_default_unit_1']?>">
		<input type="text" autocomplete="off" disabled="disabled" name="nothing_1" value="<?php echo $product['area_units_default_unit_1']?>" class="text">
	<?php else:?>
		<input type="text" autocomplete="off" name="area_units_unit_1" value="<?php echo $product['area_units_default_unit_1']?>" class="text">
	<?php endif?>
	<span class="unit" style="margin:0 6px 0 -8px;font-size:11px;"><?php echo $product['area_units_unit_1']?></span>
	<span>x</span>

	<?php if($product['area_units_lock_unit_2']):?>
		<input type="hidden" name="area_units_unit_2" value="<?php echo $product['area_units_default_unit_2']?>">
		<input type="text" autocomplete="off" disabled="disabled" name="nothing_2" value="<?php echo $product['area_units_default_unit_2']?>" class="text">
	<?php else:?>
		<input type="text" autocomplete="off" name="area_units_unit_2" value="<?php echo $product['area_units_default_unit_1']?>" class="text">
	<?php endif?>
	<span class="unit" style="margin:0 0 0 -8px;font-size:11px;"><?php echo $product['area_units_unit_2']?></span>
	<span>=&nbsp;&nbsp;<span id="area-units-result">1</span> <?php echo $product['area_units_unit_1']?></span>

	<script>
		$(document).ready(function(){
			<?php if(SHOW_PRICES && $product['price'] !== null && $product['vat']): ?>
				// Cena s DPH
				var au_price_vat = <?php echo parseFloat(str_replace(',','.', price_vat(array($product, $product['price']+$product['recycling_fee']))))?>;
				var au_price = <?php echo parseFloat(str_replace(',','.', price_unvat(array($product, $product['price']+$product['recycling_fee']))))?>;
			<?php elseif(SHOW_PRICES && $product['price'] !== null): ?>
				// Cena bez DPH
				var au_price_vat = null;
				var au_price = <?php echo parseFloat(str_replace(',','.', price($product['price'] + $product['recycling_fee'])))?>;
			<?php endif?>

			areaUnitsRecalc();

			$('input[name="area_units_unit_1"], input[name="qty"], input[name="area_units_unit_2"]').change(function(){
				areaUnitsRecalc();
			})

			function areaUnitsRecalc() {
				var u1 = 1,
					u2 = 1,
					quantity = parseInt($('input[name="qty"]').val()),
					final_price_vat = au_price, // S DPH jen kdyz je nastavene
					final_price = au_price; // Bez DPH nebo konecna

				if(quantity <= 0)
					quantity = 1;

				if(parseFloat($('input[name="area_units_unit_1"]').val().replace(',','.')) > 0){
					u1 = parseFloat($('input[name="area_units_unit_1"]').val().replace(',','.'))
				}else{
					$('input[name="area_units_unit_1"]').val(u1)
				}

				if(parseFloat($('input[name="area_units_unit_2"]').val().replace(',','.')) > 0) {
					u2 = parseFloat($('input[name="area_units_unit_2"]').val().replace(',','.'))
				}else{
					$('input[name="area_units_unit_2"]').val(u2)
				}

				$('#area-units-result').html(u1*u2);

				if(au_price_vat) {
					final_price_vat = quantity * u1 * u2 * au_price_vat;
					final_price = quantity * u1 * u2 * final_price;
					$('#product-price').html(displayPrice(final_price_vat));
					$('#product-price-excl-vat').html(displayPrice(final_price));
				}else{
					final_price = quantity * u1 * u2 * final_price;
					$('#product-price').html(displayPrice(final_price));
				}
			}
		})
	</script>

<?php
	$html = $view->slot_stop();
	$view->find('#append-calculator')->append($html);
?>