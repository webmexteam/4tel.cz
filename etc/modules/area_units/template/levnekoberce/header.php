<?php
$storage = Core::module('area_units')->getStorage();
$view->override = false;

if(Area_Units_Basket::count()){
	$view->find('#basket .price strong')->html(price(Area_Units_Basket::total()->total));
}
?>