<?php
defined('WEBMEX') or die('No direct access.');

/**
 * Class Module_Area_Units
 */
class Module_Area_Units extends Module {

	public $has_settings = true;

	protected $options = array(
	);

    public $module_info = array(
		'name' => array(
			'cs' => 'Výpočet ceny na základě plošných jednotek',
		),
		'description' => array(
			'cs' => 'Umožní cenu vypočítávat na základě plošných jednotek',
		),
		'version' => '1.0.0',
		'author' => 'Tomas Nikl &copy; Webmex.cz',
		'www' => '<a href="http://www.webmex.cz">www.webmex.cz</a>',
    );

	/**
	 * Run after install module.
	 *
	 * @param $module
	 */
	public function install($module) {
    	$module->update(array('storage' => serialize($this->options)));

		try {
			Core::$db_inst->schema->addColumns('product',
				array(
					'area_units_unit_1' => array(
						'type' => 'varchar'
					)
				)
			);

			Core::$db_inst->schema->addColumns('product',
				array(
					'area_units_unit_2' => array(
						'type' => 'varchar'
					)
				)
			);

			Core::$db_inst->schema->addColumns('product',
				array(
					'area_units_lock_unit_1' => array(
						'type' => 'integer',
						'not_null' => true,
						'default' => 0
					)
				)
			);

			Core::$db_inst->schema->addColumns('product',
				array(
					'area_units_lock_unit_2' => array(
						'type' => 'integer',
						'not_null' => true,
						'default' => 0
					)
				)
			);

			Core::$db_inst->schema->addColumns('product',
				array(
					'area_units_default_unit_1' => array(
						'type' => 'float'
					)
				)
			);

			Core::$db_inst->schema->addColumns('product',
				array(
					'area_units_default_unit_2' => array(
						'type' => 'float'
					)
				)
			);

			Core::$db_inst->schema->addColumns('product',
				array(
					'area_units_allowed' => array(
						'type' => 'integer',
						'not_null' => true,
						'default' => 0
					)
				)
			);

			Core::$db_inst->schema->addColumns('basket_products',
				array(
					'area_units_unit_1' => array(
						'type' => 'float'
					)
				)
			);

			Core::$db_inst->schema->addColumns('basket_products',
				array(
					'area_units_unit_2' => array(
						'type' => 'float'
					)
				)
			);

		} catch (Exception $e) {}
    }

	public function setup() {

	}

	public static function setEnv() {
		if(Core::$segments[0] == "default" && !Core::$is_admin) {
			Core::$segments[0] = "area_units";
		}
	}

	/**
	 * Save settings form
	 */
	public function saveSettingForm() {
    	if(!empty($_POST)){
    		$data = (array) $this->getStorage();
    		$data = Arr::overwrite($this->options, $data, $_POST);
    		$this->saveStorage($data);
    		flashMsg(__('msg_saved'));
    	}
    }

	/**
	 * @return View
	 */
	public function getSettingForm() {
    	return tpl('module_area_units.php', array(
    			'module' => $this, 
    			'storage' => $this->getStorage())
    		);
    }
    
}

class Area_Units_Basket extends Basket
{
	public static function add($product_id, $attributes = null, $qty = 1)
	{
		if ($attributes) {
			$attributes = join(';', $attributes);
		}

		$qty = abs($qty);
		$product = Core::$db->product[(int) $product_id];

		if (!$product || !$product['status']) {
			return false;
		}

		if ((int) Core::config('suspend_no_stock') && ($stock = self::isInStock($product_id, $attributes, $qty)) !== true) {
			return (int) $stock;
		}

		$r = Core::$db->basket_products()
			->where('basket_id', self::id())
			->where('product_id', $product_id)
			->where('attributes', $attributes);

		if(isset($_POST['area_units_unit_1']) && isset($_POST['area_units_unit_2'])) {
			$r->where('area_units_unit_1', $_POST['area_units_unit_1']);
			$r->where('area_units_unit_2', $_POST['area_units_unit_2']);
		}

		$r = $r->fetch();

		if ($r) {
			Event::run('Area_Units_Basket::add', $product_id, $attributes, $qty);

			return self::update($r['id'], $r['qty'] + $qty);
		} else {
			$data = array(
				'basket_id' => self::id(),
				'product_id' => (int) $product_id,
				'attributes' => $attributes,
				'qty' => $qty
			);

			if(isset($_POST['area_units_unit_1']) && isset($_POST['area_units_unit_2'])) {
				$data['area_units_unit_1'] = $_POST['area_units_unit_1'];
				$data['area_units_unit_2'] = $_POST['area_units_unit_2'];
			}

			Core::$db->basket_products($data);
		}

		Event::run('Area_Units_Basket::add', $product_id, $attributes, $qty);

		return true;
	}

	public static function total()
	{
		$subtotal = 0;
		$total = 0;
		$weight = 0;
		$discount = 0;

		$products = self::products();

		if (!$products || !count($products)) {
			return null;
		}

		foreach ($products as $product) {
			$subtotal += price_unvat($product['price'], $product['product']['vat'])->price * $product['qty'];
			$total += price_vat($product['price'], $product['product']['vat'])->price * $product['qty'];

			if ($product['weight']) {
				$weight += ($product['weight'] * $product['qty']);
			}
		}

		$tbd = $total;

		if ($voucher = self::voucher()) {
			$discount = estPrice($voucher['value'], $total);

			$total = $total - $discount;
		}

		if ($total < 0) {
			$total = 0;
		}

		$obj = new Basket_Total;
		$obj->subtotal = $subtotal;
		$obj->total = round($total, (int) Core::config('order_round_decimals'));
		$obj->_total = $total;
		$obj->_total_before_discount = $tbd;
		$obj->discount = $discount;
		$obj->weight = $weight;

		return $obj;
	}

	public static function products()
	{
		$basket_id = self::id();
		$products = array();

		if (!$basket_id) {
			return null;
		}

		foreach (self::$basket->basket_products() as $product) {
			if (!$product->product['id']) {
				continue;
			}

			$price = $product->product['price'];
			$sku = $product->product['sku'];
			$ean = $product->product['ean13'];
			$stock = $product->product['stock'];
			$weight = (float) $product->product['weight'];

			if($product['area_units_unit_1']){
				$price = $price * $product['area_units_unit_1'];
			}

			if($product['area_units_unit_2']){
				$price = $price * $product['area_units_unit_2'];
			}

			$attributes = '';

			Event::run('Area_Units_Basket::products.product_attributes', $product, $attributes, $price, $sku, $ean, $stock);

			$attribute_file_id = null;

			if ($attr_ids = explode(';', $product['attributes'])) {
				foreach ($attr_ids as $attr_id) {

					if (!is_numeric($attr_id)) {
						continue;
					}

					$attr = Core::$db->product_attributes[(int) $attr_id];

					if ($attr) {
						$attributes .= $attr['name'] . ': ' . $attr['value'] . ', ';

						if (preg_match('/^([\d\-\+\.\,]+)\%$/', trim($attr['price']), $matches)) {
							$price += parseFloat($matches[1]) / 100 * $product->product['price'];
						} else if (preg_match('/^([\d\-\+\.\,]+)$/', trim($attr['price']), $matches)) {
							$price += parseFloat($matches[1]);
						}

						if($attr['weight']) {
							$weight = (float) $attr['weight'];
						}

						if($attr['file_id']) {
							$attribute_file_id = (int) $attr['file_id'];
						}

						if ($attr['sku']) {
							if (strrpos($attr['sku'], '*') !== false) {
								$sku = preg_replace('/\*/', $sku, $attr['sku']);
							} else {
								$sku = $attr['sku'];
							}
						}

						if ($attr['ean13']) {
							if (strrpos($attr['ean13'], '*') !== false) {
								$ean = preg_replace('/\*/', $ean, $attr['ean13']);
							} else {
								$ean = $attr['ean13'];
							}
						}

						if ($attr['stock'] !== null) {
							$stock = $attr['stock'];
						}
					}
				}
			}

			if (count($discounts = $product->product->product_discounts()->order('quantity DESC'))) {
				foreach ($discounts as $discount) {
					if ($product['qty'] >= $discount['quantity']) {
						$price = $price - ((float) $discount['value'] / 100 * $price);
						break;
					}
				}
			}

			if ($product->product['recycling_fee']) {
				$price += (float) $product->product['recycling_fee'];
			}

			if ($product->product['copyright_fee']) {
				$price += (float) $product->product['copyright_fee'];
			}

			$products[] = array(
				'basket_product' => $product,
				'id' => $product['id'],
				'qty' => $product['qty'],
				'product' => $product->product,
				'price' => $price,
				'sku' => $sku,
				'ean13' => $ean,
				'stock' => $stock,
				'attributes' => trim($attributes, ', '),
				'attributes_ids' => $product['attributes'],
				'weight' => $weight,
				'attribute_file_id' => $attribute_file_id,
			);
		}

		return $products;
	}
}