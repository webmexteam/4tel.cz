﻿<?php defined('WEBMEX') or die('No direct access.');

class Module_Webmex_New_Order extends Module {
	
	public $has_settings = false;
	
	public $module_info = array(
		'name'		=> array(
			'cs' => 'Nová objednávka v administraci'
		),
		'description' => array(
			'cs' => 'Přidá možnost vytvořit v administraci novou objednávku.'
		),
		
		'version'	=> '1.0.0',
		'license'	=> 'Webmex.cz',
		'author'	=> 'Webmex.cz; (c) 2013',
		'www'		=> 'http://www.webmex.cz/'
	);

	public function setup()
	{
		Event::add('Controller::construct', array($this, 'init'));
	}
	
	public function init()
	{
		Menu::add(array(
			'position' => 2500,
			'text' => __('webmex_new_order_add_new'),
			'url' => url('admin/webmex_new_order/new_order'),
		), 'comments', 'orders');
	}
}