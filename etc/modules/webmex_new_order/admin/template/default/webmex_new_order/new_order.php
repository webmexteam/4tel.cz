<?php defined('WEBMEX') or die('No direct access.');

/**
 * Webmex - http://www.webmex.cz.
 */

?><style>
	h3 label {
		font-weight: normal;
	}
	h3 label input {
		margin: 0 3px 0 15px !important;
	}
	.form .inputwrap {
		width: 100%;
	}
	.form .inline .inputwrap {
		width: 46% !important;
	}
	.form .inline .inputwrap:nth-child(2n) {
		float: right;
		margin-right: 5px;
	}
	.orderinfo {
		padding: 0 0 10px 0;
		margin: 5px 0 40px 0;
		border-bottom: 1px solid #d6d6d6;
	}
	.orderinfo .cell {
		width: 200px;
		float: left;
		padding-right: 10px;
		margin-right: 10px;
		border-right: 1px solid #d6d6d6;
		margin-bottom: 0 !important;
	}
	.orderinfo .cell-last {
		float: none;
		width: auto;
		border-right: none;
	}
	.orderinfo .inputwrap {
		width: 200px;
	}
	.orderinfo .inputwrap .input-text {
		width: 94%;
		margin-bottom: 0 !important;
	}
	.orderinfo div.row {
		margin-top: 5px;
	}
	.orderinfo div.row strong {
		width: 90px;
		float: left;
		text-align: right;
		margin-right: 10px;
	}
	.orderinfo .inputwrap label {
		font-weight: bold;
	}
	.orderinfo div.row .input-checkbox {
		margin: 0 !important;
	}
	.orderproducts {
		margin-top: 30px;
	}
	.orderproducts .input .name {
		width: 99%;
	}
	.orderproducts td {
		vertical-align: top !important;
	}
	.orderproducts tfoot {
		border: 1px solid #d6d6d6;
		border-bottom-color: #999;
		background: #f0f0f0;
	}
	.orderproducts tfoot td {
		padding: 5px 15px 5px 5px;
	}
	.orderproducts tfoot .total td {
		font-size: 120%;
		font-weight: bold;
	}
	.orderproducts .actions {
		width: 30px !important;
	}
	.orderproducts #newrow {
		display: none;
	}
	.orderproducts .stock {
		text-align: right;
		padding-right: 30px;
	}
	.ordercomment {
		margin-top: 15px;
	}
	.ordercomment p {
		background: #fbfbe7;
		border-left: 3px solid #f0e9c1;
		padding: 10px;
		margin: 0 0 10px 0;
	}
	.ordercomment p.nocomment {
		background-color: #f0f0f0;
		border: none;
	}
	.ordercomment p.admincomment {
		border-left: 3px solid #d6d6d6;
		background: #f6f6f6;
	}
	.ordercomment p.admincomment span {
		display: block;
		color: #666;
		font-size: 90%;
		margin-bottom: 5px;
	}
	.statuslog {
		margin: -40px 0 20px 0;
		background: #f0f0f0;
		padding: 5px;
		color: #666;
		line-height: 150%;
		font-size: 90%;
	}
	.invoice {
		background: #f0f0f0;
		border-top: 3px solid #d0d0d0;
		padding: 10px;
		margin: 5px 0 20px 0;
	}
	.invoice #invoiceform {
		margin: 0;
		display: none;
	}
	.ordersidebar {
		background: #f0f0f0;
		border-top: 3px solid #d0d0d0;
		width: 230px;
		padding: 0 10px 10px 10px;
		float: right;
	}
	.ordersidebar .title {
		display: block;
		margin-top: 10px;
	}
	.ordersidebar span.title {
		margin-bottom: 3px;
	}
	.ordersidebar .title input.input-checkbox {
		margin: 0 5px 0 0;
		vertical-align: bottom;
	}
	.ordersidebar .paid,
	.ordersidebar .sync_time {
		padding: 5px;
		background: #fff;
	}
	.ordersidebar .inputwrap {
		margin: 10px 0 0 0;
		width: 100%;
	}
	.ordersidebar .inputwrap .input-text {
		width: 95%;
	}
	.ordersidebar .payment_details {
		background: #fff;
		padding: 5px;
		line-height: 140%;
	}
	.ordersidebar .toggleset {
		margin-top: 15px;
	}
</style>
<?php $vat_payer = ($order['vat_payer'] !== null ? (int) $order['vat_payer'] : (int) Core::config('vat_payer'))?>

<form class="form" action="<?php echo url('admin/webmex_new_order/new_order_save')?>" method="post">

	<h2><a href="<?php echo url('admin/orders')?>"><?php echo __('orders')?></a> &rsaquo; <?php echo __('webmex_new_order_add_new')?></h2>

	<div class="buttons tbar">
		<button type="submit" class="button" name="save_go"><?php echo __('save_go')?> &rsaquo;</button>
		<button type="submit" class="button" name="save"><?php echo __('save')?> &rsaquo;</button>
	</div>

	<div class="orderinfo clearfix">
		<div class="cell">
			<?php echo forminput('select', 'status', $order['status'], array('options' => $status_options))?>
		</div>
	</div>

	<?php if(($statuslog = explode("\n", $order['status_log'])) && $statuslog[0]): ?>
	<div class="statuslog">
		<ul>
			<?php foreach($statuslog as $log): if($log): $log = explode(';', $log); ?>
			<li><?php echo __('status_'.Core::$def['order_status'][$log[1]])?> - <?php echo fdate((int) $log[0], true)?>, <?php echo Core::$db->user[(int) $log[2]]['username']?></li>
			<?php endif; endforeach; ?>
		</ul>
	</div>
	<?php endif; ?>

	<div class="ordersidebar">
		<span class="title"><?php echo __('payment_type')?>:</span>
		<strong><?php echo $order->payment['name']?></strong>

		<label class="title paid"><input type="checkbox" name="payment_realized" value="1" class="input-checkbox"<?php echo ($order['payment_realized'] ? ' checked="checked"' : '')?> /> <?php echo __('payment_realized')?></label>
		<?php if($order['payment_realized']): ?>
		<div class="payment_details">
			<?php echo __('date').': '.fdate($order['payment_realized'], true)?>

			<?php if($order['payment_session']): ?>
			<br /><?php echo __('payment_session').': '.$order['payment_session']?>
			<?php endif; ?>
		</div>
		<?php elseif($payment && $link = $payment->getPaymentLink($order)): ?>
		<span class="title"><a href="<?php echo $link?>" target="_blank"><?php echo __('online_payment')?> &raquo;</a></span>
		<?php endif; ?>

		<?php if($order['payment_status']): ?>
		<br /><em style="color:#555"><?php echo __('payment_status_'.strtolower($order['payment_status']))?></em>
		<?php endif; ?>

		<hr />

		<?php if((int) Core::config('confirm_orders')): ?>
			<span class="title"><?php echo __('order_confirmation')?>:</span>
			<?php if($order['confirmed']): ?>
			<strong><?php echo fdate($order['confirmed'], true)?></strong>
			<?php else: ?>
			<label class="title paid"><input type="checkbox" name="confirmed" value="1" class="input-checkbox" /> <?php echo __('confirmed')?></label>
			<?php endif; ?>
			<hr />
		<?php endif; ?>

		<span class="title"><?php echo __('delivery_type')?>:</span>
		<strong><?php echo $order->delivery['name']?></strong>

		<?php echo forminput('text', 'track_num', $order['track_num'], array(
			'label' => __('track_num'),
			'premium' => true
		))?>

		<?php if($delivery && $order['track_num'] && ($link = $delivery->getTrackLink())): ?>
		<span class="title"><a href="<?php echo $link?>" target="_blank"><?php echo __('track_package')?> &raquo;</a></span>
		<?php endif; ?>

		<hr />

		<?php if($voucher): ?>
		<span class="title"><?php echo __('voucher')?>:</span>
		<strong><?php echo $voucher['code']?></strong> (<a href="<?php echo url(true, array('removevoucher' => 1))?>" title="<?php echo __('remove')?>">&times;</a>)<br />
		<?php echo __('value')?>: <?php echo $voucher['value']?><br />
		<span class="title"><a href="<?php echo url('admin/vouchers/edit/'.$voucher['id'])?>"><?php echo __('detail')?> &raquo;</a></span>

		<?php else: ?>
		<?php echo forminput('text', 'voucher', '', array(
			'label' => __('voucher_code'),
			'premium' => true
		))?>
		<?php endif; ?>

		<fieldset class="toggleset">
			<legend><?php echo __('advanced_options')?></legend>

			<div class="wrap">
				<label class="title sync_time"><input type="checkbox" name="sync_time" value="1" class="input-checkbox"<?php echo ($order['sync_time'] ? ' checked="checked"' : '')?> /> <?php echo __('synced')?></label>
				<?php if($order['sync_time']): ?>
				<div class="payment_details">
					<?php echo __('date').': '.fdate($order['sync_time'], true)?>
				</div>
				<?php endif; ?>
			</div>
		</fieldset>
	</div>

	<div style="margin-right:260px;">
		<div class="clearfix">

			<div style="width:48%;float:left;margin-right:30px;">
				<h3><?php echo __('billing_address')?></h3>

				<fieldset class="inline">
					<?php echo forminput('text', 'first_name', $order['first_name'])
					.forminput('text', 'last_name', $order['last_name'])?>
				</fieldset>

				<fieldset class="inline">
					<?php echo forminput('text', 'company', $order['company'])
					.forminput('text', 'company_id', $order['company_id'])
					.forminput('text', 'company_vat', $order['company_vat'])?>
				</fieldset>

				<hr class="hline" />

				<fieldset class="inline">
					<?php echo forminput('text', 'email', $order['email'])
					.forminput('text', 'phone', $order['phone'])?>
				</fieldset>

				<hr class="hline" />

				<fieldset>
					<?php echo forminput('text', 'street', $order['street'])?>
				</fieldset>

				<fieldset class="inline">
					<?php echo forminput('text', 'city', $order['city'])
					.forminput('text', 'zip', $order['zip'])
					.forminput('text', 'country', $order['country'])?>
				</fieldset>

				<div class="ordercomment">
					<h3><?php echo __('comment')?></h3>

					<?php if($order['comment']): ?>
						<p><?php echo nl2br($order['comment'])?></p>
					<?php else: ?>
						<p class="nocomment"><?php echo __('no_comment')?></p>
					<?php endif; ?>

					<?php if($order): foreach($order->order_comments() as $comment): ?>
					<p class="admincomment">
						<span><?php echo fdate($comment['time'], true)?>, <?php echo $comment->user['username']?> - <?php echo ($comment['is_private'] ? __('private_comment') : __('public_comment'))?></span>
						<?php echo nl2br($comment['comment'])?>
					</p>
					<?php endforeach; endif; ?>

					<?php echo forminput('textarea', 'admin_comment', '')
					.forminput('checkbox', 'admin_comment_send', '')?>
				</div>

			</div>

			<fieldset style="width:48%;float:left;">
				<h3>
					<?php echo __('shipping_address')?>
					<label><input type="checkbox" name="shipping_as_billing" value="1" class="input-checkbox"<?php echo (! $order['ship_street'] ? ' checked="checked"' : '')?> /> <?php echo __('shipping_as_billing')?></label>
				</h3>

				<div id="shippingaddr">
					<fieldset class="inline">
						<?php echo forminput('text', 'ship_first_name', $order['ship_first_name'], array('label' => __('first_name')))
						.forminput('text', 'ship_last_name', $order['ship_last_name'], array('label' => __('last_name')))?>
					</fieldset>

					<fieldset>
						<?php echo forminput('text', 'ship_street', $order['ship_street'], array('label' => __('street')))?>
					</fieldset>

					<fieldset class="inline">
						<?php echo forminput('text', 'ship_city', $order['ship_city'], array('label' => __('city')))
						.forminput('text', 'ship_zip', $order['ship_zip'], array('label' => __('zip')))
						.forminput('text', 'ship_country', $order['ship_country'], array('label' => __('country')))?>
					</fieldset>
				</div>
			</fieldset>
		</div>

	</div>

	<div class="orderproducts">
		<h3><?php echo __('products')?></h3>

		<table class="datagrid">
		<thead>
			<tr>
				<td class="right" width="60"><?php echo __('id')?></td>
				<td width="140"><?php echo __('sku')?></td>
				<td><?php echo __('name')?></td>
				<td class="right" width="100"><?php echo __(PRICE_NAME)?></td>
				<?php if($vat_payer): ?>
					<td class="right" width="60"><?php echo __('vat')?> %</td>
				<?php endif; ?>
				<td class="center" width="60"><?php echo __('quantity')?></td>
				<td class="right" width="100"><?php echo __($vat_payer ? 'total_incl_vat' : 'total')?></td>
				<td class="actions">&nbsp;</td>
			</tr>
		</thead>

		<tbody>
			<tr id="newrow">
				<td class="input">
					<input type="text" name="product[0][product_id]" class="input-text right" value="" />
				</td>
				<td class="input">
					<input type="text" name="product[0][sku]" class="input-text" value="" />
				</td>
				<td class="input">
					<input type="text" name="product[0][name]" class="input-text name" value="" />
				</td>
				<td class="input">
					<input type="text" name="product[0][price]" class="input-text right" value="" />
				</td>
				<?php if($vat_payer): ?>
					<td class="input">
						<input type="text" name="product[0][vat]" class="input-text right" value="" />
					</td>
				<?php endif; ?>
				<td class="input">
					<input type="text" name="product[0][quantity]" class="input-text center" value="1" />
				</td>
				<td class="right">
					<span class="subtotal"></span>
				</td>
				<td class="actions">
					<a href="#" class="deleterow" title="<?php echo __('delete')?>">&nbsp;</a>
				</td>
			</tr>
		</tbody>

		<tfoot>
			<?php if($vat_payer): ?>
			<tr>
				<td colspan="3" rowspan="2">
					<button class="button" name="add_row"><?php echo __('add_row')?></button>
					<button class="button" name="add_product"><?php echo __('add_product')?></button>
				</td>
				<td class="right" colspan="3"><?php echo __('subtotal')?>:</td>
				<td class="right"><span id="subtotal"></span></td>
				<td rowspan="<?php echo ($voucher ? 4 : 3)?>" class="stock">

				</td>
			</tr>
			<tr>
				<td class="right" colspan="3"><?php echo __('vat')?>:</td>
				<td class="right"><span id="vat"></span></td>
			</tr>

			<?php if($voucher): ?>
			<tr class="voucher">
				<td colspan="6" class="right"><?php echo __('voucher')?>:</td>
				<td class="right"><span id="discount"></span></td>
			</tr>
			<?php endif; ?>

			<tr class="total">
				<td colspan="6" class="right"><?php echo __('total')?>:</td>
				<td class="right"><span id="total"></span></td>
			</tr>

			<?php if(! (int) Core::config('stock_auto') && ! (int) $order['stock_recalc']): ?>
			<tr>
				<td class="right" colspan="<?php echo ($vat_payer ? 6 : 5)?>"><button class="button" name="stock_recalc"><?php echo __('stock_recalc')?></button></td>
				<td class="right" colspan="2">&nbsp;</td>
			</tr>
			<?php endif; ?>

			<?php else: ?>

			<?php if($voucher): ?>
			<tr class="voucher">
				<td colspan="5" class="right"><?php echo __('voucher')?>:</td>
				<td class="right"><span id="discount"></span></td>
				<td>&nbsp;</td>
			</tr>
			<?php endif; ?>

			<tr class="total">
				<td colspan="3">
					<button class="button" name="add_row"><?php echo __('add_row')?></button>
					<button class="button" name="add_product"><?php echo __('add_product')?></button>
				</td>
				<td class="right" colspan="2"><?php echo __('total')?>:</td>
				<td class="right"><span id="total"></span></td>
				<td>&nbsp;</td>
			</tr>

			<?php if(! (int) Core::config('stock_auto') && ! (int) $order['stock_recalc']): ?>
			<tr>
				<td class="right" colspan="5"><button class="button" name="stock_recalc"><?php echo __('stock_recalc')?></button></td>
				<td class="right" colspan="2">&nbsp;</td>
			</tr>
			<?php endif; ?>

			<?php endif; ?>

		</tfoot>
	</table>
	</div>

	<div class="buttons bbar">
		<button type="submit" class="button" name="save_go"><?php echo __('save_go')?> &rsaquo;</button>
		<button type="submit" class="button" name="save"><?php echo __('save')?> &rsaquo;</button>
	</div>


</form>

<script>
	$(function(){
		var vat_payer = <?php echo $vat_payer?>;
		var voucher_value = '<?php echo ($voucher ? $voucher['value'] : '')?>';

		var toggleShipForm = function(enable){
			var f = $('#shippingaddr');

			if(! enable){
				f.find('input').attr('disabled', true);
				f.css('opacity', 0.5);

			}else{
				f.find('input').removeAttr('disabled');
				f.css('opacity', 1);
			}
		}

		var estPrice = function(str, total){
			var matches = String(str).match(/^([\+\-]?[\d\.\,]+)\%$/);

			if(matches){
				return total / 100 * parseFloatNum(matches[1]);
			}

			return str ? parseFloatNum(str) : 0;
		}

		var inp = $('input[name=shipping_as_billing]');

		inp.bind('click', function(){
			toggleShipForm(! this.checked);
		});

		toggleShipForm(! inp.is(':checked'));

		var recalc = function(){
			var subtotal = 0;
			var total = 0;
			var vat = 0;
			var discount = 0;

			$('.orderproducts tbody tr:visible').each(function(){
				var psubtotal = 0, pvat = 0;
				var p = parseFloatNum($(this).find('input[name*="price"]').val() || 0);
				var v = parseFloatNum($(this).find('input[name*="vat"]').val() || 0) / 100;
				var q = parseFloatNum($(this).find('input[name*="quantity"]').val() || 1);

				if(vat_payer){
					if(_vat_mode == 'exclude'){
						psubtotal = (p * q);
						pvat = (p * q * v);

					}else{
						var p_excl = p / (v + 1);
						psubtotal = (p_excl * q);
						pvat = (p - p_excl) * q;
					}

				}else{
					psubtotal = (p * q);
				}

				subtotal += psubtotal;
				vat += pvat;

				$(this).find('span.subtotal').html(displayPrice(psubtotal + pvat));
			});

			if(vat_payer){
				$('#subtotal').html(displayPrice(subtotal));
				$('#vat').html(displayPrice(vat));
			}

			total = subtotal + vat;

			if(voucher_value){
				discount = estPrice(voucher_value, total);
				total = total - discount;
			}

			if(total < 0){
				total = 0;
			}

			total = Math.round(total * Math.pow(10, _order_round_decimals)) / Math.pow(10, _order_round_decimals);

			$('#discount').html('-'+displayPrice(discount));

			$('#total').html(displayPrice(total));
		}

		$('.orderproducts input').live('change', recalc);

		recalc();

		var newid = 0;

		$('button[name="add_row"]').bind('click', function(){
			var row = $('#newrow').clone();

			newid ++;

			row.find('input[name^="product[0]"]').each(function(){
				$(this).attr('name', $(this).attr('name').replace(/product\[0\]/, 'product[new_'+newid+']'));
			});

			$('#newrow').before(row);

			row.removeAttr('id').show();

			return false;
		});

		$('button[name="add_product"]').bind('click', function(){
			productfinder({
				callback: function(products){

					$.each(products, function(i, product){
						product.quantity = 1;

						var row = $('#newrow').clone();

						newid ++;

						row.find('input[name^="product[0]"]').each(function(){
							var name = this.name.match(/\[(\w+)\]$/)[1];

							$(this).attr('name', $(this).attr('name').replace(/product\[0\]/, 'product[new_'+newid+']'));
							$(this).val(product[name] || '');
						});

						$('#newrow').before(row);

						row.removeAttr('id').show();
					});

					recalc();
				}
			});
			return false;
		});

		$('.orderproducts a.deleterow').live('click', function(){
			if(confirm(_lang.confirm_delete)){
				$(this).parents('tr:first').remove();
				recalc();
			}
			return false;
		});

		$('button[name="create_invoice"]').bind('click', function(){
			var ret = $('#invoiceform').is(':visible');

			$('#invoiceform').show();

			return ret;
		});
	});
</script>