<?php defined('WEBMEX') or die('No direct access.');

class Controller_Webmex_New_Order extends AdminController {

	public function __construct()
	{
		parent::__construct();
				
		gatekeeper('webmex_new_order');
		
		Core::$active_tab = 'orders';
	}

	public function new_order() {

		/**
		 * Stavy objednavek
		 */
		$status_options = array();
		foreach (Core::$def['order_status'] as $sid => $n) {
			$status_options[$sid] = __('status_' . $n);
		}


		$this->content = tpl('webmex_new_order/new_order.php', array(
			'status_options' => $status_options,
		));
	}

	public function new_order_save() {
		$id = Core::$db->order->insert(array());

		$order = Core::$db->order[(int) $id];

		Event::run('Controller_Orders::edit', $order);

		if ($id > 0 && !$order) {
			flashMsg(__('msg_record_not_found', $id), 'error');
			redirect('admin/orders');
		}

		if ($order['currency'] && $order['currency'] != Core::config('currency')) {
			Core::$config['currency'] = $order['currency'];
		}

		$voucher = $order->voucher;

		if (!$voucher['id']) {
			$voucher = null;
		}

		if ($voucher && !empty($_GET['removevoucher'])) {
			$total_vat = $order->model->getTotal_incl_vat(true);

			$order->update(array(
				'voucher_id' => null,
				'total_incl_vat' => $total_vat
			));

			$order['voucher_id'] = null;

			$voucher->update(array('used' => $voucher['used'] - 1));
			Core::$db->voucher_orders()->where('order_id', $order['id'])->where('voucher_id', $voucher['id'])->delete();

			redirect('admin/orders/edit/' . $order['id']);
		}

		if (!empty($_POST)) {
			if (isSet($_POST['stock_recalc'])) {
				foreach ($order->order_products() as $product) {
					if ($product['product_id'] && $product->product['stock'] !== null && $product->product['stock'] !== '') {
						$new_qty = (int) $product->product['stock'] - (int) $product['quantity'];

						$product->product->update(array(
							'stock' => $new_qty
						));

						if (Core::config('stock_notify') !== '' && $new_qty < Core::config('stock_notify')) {
							Email::event('low_stock', Core::config('email_notify'), null, $product->product);
						}
					}
				}

				$order->update(array('stock_recalc' => 1));

				redirect(true);
			}

			if (($errors = validate(array('status'))) === true) {
				$data = $_POST;
				$data['last_change'] = time();
				$data['received'] = time();
				$data['last_change_user'] = User::get('id');

				if (isSet($data['shipping_as_billing'])) {
					$data['ship_first_name'] = '';
					$data['ship_last_name'] = '';
					$data['ship_street'] = '';
					$data['ship_city'] = '';
					$data['ship_zip'] = '';
					$data['ship_country'] = '';
				}

				if ($order && $data['status'] != $order['status']) {
					$data['status_log'] = trim($order['status_log'] . "\n" . time() . ";" . $data['status'] . ";" . User::get('id'));
				}

				if ($order && !(int) $order['payment_realized'] && isSet($data['payment_realized'])) {
					$data['payment_realized'] = time();
					$data['payment_session'] = null;
				} else if ($order && (int) $order['payment_realized'] && isSet($data['payment_realized'])) {
					$data['payment_realized'] = $order['payment_realized'];
				} else {
					$data['payment_realized'] = 0;
					$data['payment_session'] = null;
				}

				if ($order && !(int) $order['confirmed'] && isSet($data['confirmed'])) {
					$data['confirmed'] = time();
				} else if ($order && (int) $order['confirmed'] && isSet($data['confirmed'])) {
					$data['confirmed'] = $order['confirmed'];
				}

				if ($order && !(int) $order['sync_time'] && isSet($data['sync_time'])) {
					$data['sync_time'] = time();
				} else if ($order && (int) $order['sync_time'] && isSet($data['sync_time'])) {
					$data['sync_time'] = $order['sync_time'];
				} else {
					$data['sync_time'] = null;
				}

				Event::run('Controller_Orders::edit.before_save', $order, $data);

				if (!empty($_POST['voucher'])) {
					if ($voucher = Core::$db->voucher()->where('code', $_POST['voucher'])->fetch()) {

						if ($voucher->isValid()) {
							$data['voucher_id'] = $voucher['id'];

							$voucher->update(array('used' => $voucher['used'] + 1));

							Core::$db->voucher_orders(array(
								'order_id' => $order['id'],
								'voucher_id' => $voucher['id'],
								'date' => time()
							));
						}
					} else {
						$voucher_error = true;
						flashMsg(__('msg_voucher_invalid_code'), 'error');
					}
				}

				if ($order->update(prepare_data('order', $data))) {

					if (isSet($_POST['create_invoice'])) {
						$o = Core::$db->order[$order['id']];

						$invoice_id = Invoice::createFromOrder($o, $_POST['invoice_num'], strtotime($_POST['invoice_issue_date']));
					}

					foreach ($order->order_products() as $product) {
						if (!empty($_POST['product'][$product['id']])) {
							$product->update(prepare_data('order_products', $_POST['product'][$product['id']]));
						} else {
							$product->delete();
						}
					}

					foreach ($_POST['product'] as $id => $product) {
						if (preg_match('/new_/', $id)) {
							$product['order_id'] = $order['id'];

							if (!empty($product['product_id']) && (empty($product['name']) || empty($product['price']))) {
								if ($db_product = Core::$db->product[(int) $product['product_id']]) {
									foreach ($product as $k => $v) {
										if (empty($v) && isSet($db_product[$k])) {
											$product[$k] = $db_product[$k];
										}
									}
								}
							}

							if ($product['vat'] == '' && Core::config('vat')) {
								$product['vat'] = Core::config('vat');
							}

							Core::$db->order_products(prepare_data('order_products', $product));
						}
					}

					$subtotal = 0;
					$total_vat = 0;

					$items = '';

					foreach (Core::$db->order_products()->where('order_id', $order['id']) as $product) {
						if ($product['product_id']) {
							$subtotal += $product['price'] * $product['quantity'];
						}

						$total_vat += price_vat($product['price'] * $product['quantity'], $product['vat'])->price;

						$items .= ($product['sku'] ? '' . $product['sku'] . ' - ' : '') . $product['name'] . ' -  ' .
								preg_replace('/\&nbsp;/', ' ', price_vat($product['price'], $product['vat'])) . ' * ' . $product['quantity'] . ' = ' . preg_replace('/\&nbsp;/', ' ', price_vat($product['price'] * $product['quantity'], $product['vat'])) . "\n";
					}

					if ($voucher) {
						$discount = estPrice($voucher['value'], $total_vat);
						$total_vat = round($total_vat - $discount, (int) Core::config('order_round_decimals'));
					}

					$total_vat = round($total_vat, (int) Core::config('order_round_decimals'));

					$order->update(array(
						'total_incl_vat' => $total_vat,
						'total_price' => $subtotal
					));

					if (!empty($_POST['admin_comment'])) {
						Core::$db->order_comments(array(
							'order_id' => $order['id'],
							'user_id' => User::get('id'),
							'time' => time(),
							'comment' => $_POST['admin_comment'],
							'is_private' => isSet($_POST['admin_comment_send']) ? 0 : 1
						));

						if (isSet($_POST['admin_comment_send'])) {
							Email::event('order_comment', $order['email'], null, array('order' => $order, 'comment' => Email::html($_POST['admin_comment'])));
						}
					}

					Event::run('Controller_Orders::edit.after_save', $order, $data);

					if (!isset($voucher_error)) {
						flashMsg(__('msg_saved'));
					}
				} else {
					flashMsg(__('msg_error'), 'error');
				}

				if (isSet($_POST['save_go'])) {
					redirect('admin/orders');
				} else {
					redirect('admin/orders/edit/' . $order['id']);
				}
			}
		} else if ($id == 0) {
			$page['name'] = __('new_page');
			$page['menu'] = 5; // hidden page
			$page['position'] = 0;
			$page['status'] = 1;
		}

		$status_options = array();
		foreach (Core::$def['order_status'] as $sid => $n) {
			$status_options[$sid] = __('status_' . $n);
		}

		if ($order['delivery_id']) {
			$delivery_driver = 'Delivery_' . ucfirst($order->delivery['driver']);
		}

		if ($order['payment_id']) {
			$payment_driver = 'Payment_' . ucfirst($order->payment['driver']);
		}

		$delivery = null;
		if (isSet($delivery_driver) && class_exists($delivery_driver)) {
			$delivery = new $delivery_driver($order);
		}

		$payment = null;
		if (isSet($payment_driver) && class_exists($payment_driver)) {
			$payment = new $payment_driver($order);
		}
	}
}