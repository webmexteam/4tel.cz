<?php defined('WEBMEX') or die('No direct access.');
$view->override = false; 
?>



<?php /********************* GET DATA *********************/ ?>
<?php
$orderData = $_SESSION['order'];

$storage = Core::module('ulozenka')->getStorage();
$ulozenka = new UlozenkaApi($storage['shopId'], $storage['apiKey']);
$branch = $ulozenka->getBranch( $orderData['ulozenka_branch_id'] );
?>



<?php /********************* RENDER *********************/ ?>
<?php
if (@$orderData['ulozenka_branch_id'] && @$orderData['delivery_name']) { 
	$deliveryTd = $view->find("table table table td:contains('" . $orderData['delivery_name'] . "')");

	$oldContent = $deliveryTd->html();
	$newContent = str_replace(
		$orderData['delivery_name'], 
		$orderData['delivery_name'] . ' - ' . $branch->name, 
		$oldContent
	);

	$deliveryTd->html($newContent);
}
?>
