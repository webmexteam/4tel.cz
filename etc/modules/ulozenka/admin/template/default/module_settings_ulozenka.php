<?php defined('WEBMEX') or die('No direct access.'); ?>

<div class="form">
	<div class="clearfix">
		<fieldset style="width:300px">
			<?php
			echo forminput('text', 'shopId', $storage['shopId'], array(
				'label' => __('webmex_module_ulozenka_shopId'),
			));
			echo forminput('text', 'apiKey', $storage['apiKey'], array(
				'label' => __('webmex_module_ulozenka_apiKey'),
			));
			echo forminput('select', 'registerBranchId', $storage['registerBranchId'], array(
				'label' => __('webmex_module_ulozenka_registerBranchId'),
			 	'options' => $branchPairs,
			));
			echo forminput('select', 'cashOnDeliveryPayments[]', NULL, array(
				'label' => __('webmex_module_ulozenka_cashOnDelivery'),
				'options' => $paymentPairs,
				'multiple' => 'multiple',
			));
			echo forminput('select', 'currency', $storage['currency'], array(
				'label' => __('webmex_module_ulozenka_currency'),
				'options' => $currencyPairs,
			));
			echo forminput('select', 'czkCountries[]', NULL, array(
				'label' => __('webmex_module_ulozenka_czkCountries'),
				'options' => $countryPairs,
				'multiple' => 'multiple',
			));
			echo forminput('select', 'svkCountries[]', NULL, array(
				'label' => __('webmex_module_ulozenka_svkCountries'),
				'options' => $countryPairs,
				'multiple' => 'multiple',
			));
			?>
		</fieldset>

		<script>
		<?php /* hotfix for forminput multiselect default value bug */ ?>
		jQuery(function(){
			<?php if(is_array($storage['cashOnDeliveryPayments'])): ?>
				<?php foreach($storage['cashOnDeliveryPayments'] as $key => $paymentId): ?>
				jQuery("select[name='cashOnDeliveryPayments[]'] option[value=<?php echo $paymentId; ?>]").attr('selected', 'selected');
				<?php endforeach; ?>
			<?php endif; ?>

			<?php if(is_array($storage['czkCountries'])): ?>
				<?php foreach($storage['czkCountries'] as $key => $countryId): ?>
				jQuery("select[name='czkCountries[]'] option[value='<?php echo $countryId; ?>']").attr('selected', 'selected');
				<?php endforeach; ?>
			<?php endif; ?>

			<?php if(is_array($storage['svkCountries'])): ?>
				<?php foreach($storage['svkCountries'] as $key => $countryId): ?>
				jQuery("select[name='svkCountries[]'] option[value='<?php echo $countryId; ?>']").attr('selected', 'selected');
				<?php endforeach; ?>
			<?php endif; ?>
		});
		</script>
	</div>
</div>


<?php
// Check
$ulozenka = new UlozenkaApi($storage['shopId'], $storage['apiKey']);
try {
	$consignments = $ulozenka->searchConsignments();
	echo 'ID obchodu i API klíč jsou v pořádku. <br />';

} catch (UlozenkaApiException $e) {
	echo $e->getMessage() . '<br />';
}
