<?php defined('WEBMEX') or die('No direct access.');


/**
 * Cache for Ulozenka API
 *
 * @author  Michal Mikoláš <nanuqcz@gmail.com>
 * @copyright  Webmex s.r.o.
 */
class UlozenkaApiCache /*extends Nette\Object*/
{
	/** @var array */
	private $data = array();

	/** @var string */
	private $filename = 'ulozenka-api-cache.txt';

	/** @var int */
	private $lifetime = 86400;  // 24 hours



	public function __construct()
	{
		$this->checkFile();
		$this->loadFile();
	}



	public function save($key, $value)
	{
		$this->data[$key] = $value;

		$this->saveFile();
	}



	public function load($key)
	{
		return isset($this->data[$key])? $this->data[$key]: NULL;
	}



	public function clear()
	{
		$emptyValue = array();

		file_put_contents(
			$this->getFilePath(), 
			serialize($emptyValue)
		);
	}



	private function checkFile()
	{
		$filePath = $this->getFilePath();

		if (!file_exists($filePath) || filectime($filePath) < (time() - $this->lifetime)) {
			$this->clear();
		}
	}



	private function loadFile()
	{
		$dataSerialized = file_get_contents( $this->getFilePath() );

		$this->data = unserialize($dataSerialized);
	}



	private function saveFile()
	{
		$dataSerialized = serialize($this->data);

		file_put_contents($this->getFilePath(), $dataSerialized);
	}



	private function getFilePath()
	{
		return DOCROOT . 'etc/tmp/' . $this->filename;
	}

}