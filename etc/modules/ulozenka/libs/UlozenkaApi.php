<?php defined('WEBMEX') or die('No direct access.');

include_once APPROOT . 'vendor/httpful/bootstrap.php';
include_once __DIR__ . '/UlozenkaApiCache.php';

use Httpful\Request,
	Httpful\Http;


/**
 * Ulozenka API
 *
 * @author  Michal Mikoláš <nanuqcz@gmail.com>
 * @copyright  Webmex s.r.o.
 */
class UlozenkaApi /*extends Nette\Object*/
{
	/** @var string */
	private $apiUrl = 'https://api.ulozenka.cz/v2';

	/** @var int */
	private $shopId;
	
	/** @var string */
	private $apiKey;
	
	/** @var UlozenkaApiCache */
	private $cache;
	


	public function __construct($shopId = NULL, $apiKey = NULL)
	{
		$this->shopId = $shopId;
		$this->apiKey = $apiKey;
		$this->cache = new UlozenkaApiCache();
	}



	public function getBranch($id)
	{
		$response = $this->createRequest('GET')
			->uri($this->apiUrl . '/branches/' . $id)
			->send();

		$this->checkResponse($response);

		return $response->body->data[0];
	}



	public function searchBranches($conditions = array())
	{
		// 1) Get data
		$branches = $this->cache->load('searchBranches');
		if (!$branches) {
			// Get response
			$response = $this->createRequest('GET')
				->uri($this->apiUrl . '/branches')
				->send();

			$this->checkResponse($response);

			// Fetch data
			$branches = $response->body->data;

			$this->cache->save('searchBranches', $branches);
		}

		// 2) Filter by conditions
		if ($conditions) {
			$branches = $this->filter($branches, $conditions);
		}

		// 3) Return
		return $branches;
	}



	public function searchConsignments($conditions = array())
	{
		// 1) Get data
		// Get response
		$response = $this->createRequest('GET')
			->uri($this->apiUrl . '/consignments')
			->send();

		$this->checkResponse($response);

		// Fetch data
		$consignments = $response->body->data;

		// 2) Filter by conditions
		if ($conditions) {
			$consignments = $this->filter($consignments, $conditions);
		}

		// 3) Return
		return $consignments;
	}



	public function createConsignment($values)
	{
		$response = $this->createRequest('POST')
			->uri($this->apiUrl . '/consignments')
			->body( json_encode($values) )
			->send();

		$this->checkResponse($response);

		$consignmentLink = $response->body->_links->self->href;
		$consignmentId = (int) preg_replace('#^.*/([0-9]+)$#', '$1', $consignmentLink);
		
		return $consignmentId;
	}



	public function searchTransportServices($conditions = array())
	{
		// Get response
		$response = $this->createRequest('GET')
			->uri($this->apiUrl . '/transportservices')
			->send();

		$this->checkResponse($response);

		// Fetch data
		$transportServices = $response->body->data;

		if ($conditions) {
			$transportServices = $this->filter($transportServices, $conditions);
		}

		// Return
		return $transportServices;
	}



	public function getBarcodesLink($ulozenkaPackageIds)
	{
		if (!is_array($ulozenkaPackageIds)) $ulozenkaPackageIds = array($ulozenkaPackageIds);

		return 'https://partner.ulozenka.cz/label/label?consignments=[' . join(',', $ulozenkaPackageIds) . ']';
	}



	private function createRequest($method = 'GET')
	{
		$method = strtoupper($method);

		$request = Request::init( constant("Httpful\\Http::$method") )
			->sendsJson();

		if ($this->shopId)
			$request->addHeader('X-Shop', $this->shopId);
		if ($this->apiKey)
			$request->addHeader('X-Key', $this->apiKey);

		return $request;
	}



	private function checkResponse($response)
	{
		if (empty($response) || empty($response->body) || 
			$response->body->code < 200 || $response->body->code > 299) {
			
			$errorMsg = '';
			if (isset($response->body->errors[0])) {
				$errorMsg = $response->body->errors[0]->description;
			}

			throw new UlozenkaApiException($errorMsg);
		}
	}



	private function filter($data, $conditions)
	{
		foreach ($data as $pos => $entry) {
			foreach ($conditions as $key => $value) {
				if (!$this->checkCondition(array($key => $value), $entry)) {
					unset($data[$pos]);
					break;
				}
			}
		}

		return $data;
	}



	private function checkCondition($condition, $data)
	{
		$key = key($condition);
		$value = current($condition);

		if (strpos($key, '.')) {
			$parts = explode('.', $key);

			$nestedData = $data->{$parts[0]};
			$nestedCond = array(
				join('.', array_splice($parts, 1)) => $value,
			);

			return $this->checkCondition($nestedCond, $nestedData);

		} else {
			return (isset($data->$key) && $data->$key == $value);			
		}

	}

}



class UlozenkaApiException extends Exception 
{
}