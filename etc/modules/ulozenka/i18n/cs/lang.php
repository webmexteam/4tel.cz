<?php defined('SQC') or die('No direct access.');

Core::$lang += array(
	'webmex_module_ulozenka_shopId' => 'ID obchodu',
	'webmex_module_ulozenka_apiKey' => 'API klíč',
	'webmex_module_ulozenka_cashOnDelivery' => 'Platby dobírkou',
	'webmex_module_ulozenka_currency' => 'Měna',
	'webmex_module_ulozenka_registerBranchId' => 'Podatelna',
	'webmex_module_ulozenka_vs' => 'VS pro Uloženku',
	'webmex_module_ulozenka_note' => 'Poznámka pro Uloženku',
	'webmex_module_ulozenka_czkCountries' => 'Země CZK',
	'webmex_module_ulozenka_svkCountries' => 'Země SVK',
);
