<?php defined('WEBMEX') or die('No direct access.');

Delivery::$drivers[] = 'ulozenka_svk_parcelshop';


/**
 * Webmex - http://www.webmex.cz.
 */
class Delivery_Ulozenka_svk_parcelshop extends Delivery
{
	public $name = 'Uloženka SK - ParcelShop';



	public function getTrackLink($track_num = '')
	{
		return 'https://tracking.ulozenka.cz/' . $track_num;
	}

}