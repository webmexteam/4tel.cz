<?php defined('WEBMEX') or die('No direct access.');

Delivery::$drivers[] = 'ulozenka_cze_parcelshop';


/**
 * Webmex - http://www.webmex.cz.
 */
class Delivery_Ulozenka_cze_parcelshop extends Delivery
{
	public $name = 'Uloženka ČR - ParcelShop';



	public function getTrackLink($track_num = '')
	{
		return 'https://tracking.ulozenka.cz/' . $track_num;
	}

}