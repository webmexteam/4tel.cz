<?php defined('WEBMEX') or die('No direct access.');

Delivery::$drivers[] = 'ulozenka_svk';


/**
 * Webmex - http://www.webmex.cz.
 */
class Delivery_Ulozenka_svk extends Delivery
{
	public $name = 'Uloženka SK - Kamenné prodejny';



	public function getTrackLink($track_num = '')
	{
		return 'https://tracking.ulozenka.cz/' . $track_num;
	}

}