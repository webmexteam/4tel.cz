<?php defined('WEBMEX') or die('No direct access.');

Delivery::$drivers[] = 'ulozenka_cze';


/**
 * Webmex - http://www.webmex.cz.
 */
class Delivery_Ulozenka_cze extends Delivery
{
	public $name = 'Uloženka ČR - Kamenné prodejny';



	public function getTrackLink($track_num = '')
	{
		return 'https://tracking.ulozenka.cz/' . $track_num;
	}

}