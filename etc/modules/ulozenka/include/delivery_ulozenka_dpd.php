<?php defined('WEBMEX') or die('No direct access.');

Delivery::$drivers[] = 'ulozenka_dpd';


/**
 * Webmex - http://www.webmex.cz.
 */
class Delivery_Ulozenka_dpd extends Delivery
{
	public $name = 'Uloženka - DPD';



	public function getTrackLink($track_num = '')
	{
		return 'https://tracking.ulozenka.cz/' . $track_num;
	}

}