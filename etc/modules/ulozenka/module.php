<?php
defined('WEBMEX') or die('No direct access.');

include_once __DIR__ . '/libs/UlozenkaApi.php';


/**
 * Class Module_Ulozenka
 * 
 * @author  Michal Mikoláš <nanuqcz@gmail.com>
 * @copyright  Webmex s.r.o.
 */
class Module_Ulozenka extends Module 
{
	public $has_settings = true;
	private $ulozenka;

	protected $options = array(
		'shopId' => '',
		'apiKey' => '',
		'registerBranchId' => '',
		'cashOnDeliveryPayments' => array(),
		'currency' => '',
		'czkCountries' => array(),
		'svkCountries' => array(),
	);

	public $module_info = array(
		'name' => array(
			'cs' => 'Doručovací služba Ulozenka.cz',
			'sk' => 'Doručovací služba Ulozenka.cz',
		),
		'description' => array(
			'cs' => 'Přidá propojení s ulozenka.cz',
			'sk' => 'Přidá propojení s ulozenka.cz',
		),
		'version' => '1.0.0',
		'author' => 'Michal Mikoláš &copy; Webmex.cz',
		'www' => '<a href="http://www.webmex.cz">www.webmex.cz</a>',
	);



	/**
	 * Run after install module.
	 */
	public function install($module) 
	{
		// 1) Save module settings
		$module->update(array('storage' => serialize($this->options)));


		// 2) Add delivery to DB
		// Calculate ord value
		$lastDelivery = Core::$db->delivery()
						->order('ord DESC')
						->limit(1)
						->fetch();
		$ord = $lastDelivery? $lastDelivery['ord']: 0;

		// Save
		$types = array(
			array(
				'name' => 'Osobní odběr - pobočky Uloženka ČR - Kamenné prodejny',
				'driver' => 'ulozenka_cze',
			),
			array(
				'name' => 'Osobní odběr - pobočky Uloženka ČR - ParcelShop',
				'driver' => 'ulozenka_cze_parcelshop',
			),
			array(
				'name' => 'Osobní odběr - pobočky Uloženka SK - Kamenné prodejny',
				'driver' => 'ulozenka_svk',
			),
			array(
				'name' => 'Osobní odběr - pobočky Uloženka SK - ParcelShop',
				'driver' => 'ulozenka_svk_parcelshop',
			),
			array(
				'name' => 'Uloženka - DPD',
				'driver' => 'ulozenka_dpd',
			),
		);
		foreach ($types as $type) {
			$ord++;

			$deliveryId = Core::$db->delivery()->insert(array(
				'name' => $type['name'],
				'driver' => $type['driver'],
				'price' => 100,
				'customer_group_id' => 0,
				'weight_min' => NULL,
				'weight_max' => NULL,
				'price_min' => NULL,
				'price_max' => NULL,
				'ord' => $ord,
			));

			// Connect delivery with delivery payments
			$payments = Core::$db->payment();
			foreach ($payments as $payment) {
				Core::$db->delivery_payments()->insert(array(
					'delivery_id' => $deliveryId,
					'payment_id' => $payment['id'],
					'price' => NULL,
					'free_over' => NULL,
				));
			}
		}


		// 3) Update DB schema
		try {
			Core::$db_inst->schema->addColumns('order', array(
				'ulozenka_branch_id' => array(
					'type' => 'integer',
				),
				'ulozenka_vs' => array(
					'type' => 'varchar',
				),
				'ulozenka_note' => array(
					'type' => 'varchar',
				),
			));
		} catch (Exception $e) {}
	}



	public function setup() 
	{
		$storage = $this->getStorage();
		$this->ulozenka = new UlozenkaApi($storage['shopId'], $storage['apiKey']);

		Event::add('Controller_Default::saveOrder', array($this, 'saveOrder'));
		Event::add('Controller_Orders::edit.after_save', array($this, 'adminEditOrder'));


		// Save ulozenka's branch_id after submit
		if (isset($_POST['submit_order']) && !empty($_POST['delivery']) 
			&& !empty($_POST['ulozenka'][ $_POST['delivery'] ])) 
		{
			$_SESSION['order']['ulozenka_branch_id'] = $_POST['ulozenka'][ $_POST['delivery'] ];
		}


		// Admin: Print selected barcodes
		// if (strpos($_SERVER['REQUEST_URI'], 'admin/orders'))
		if (isset($_POST['action']) && $_POST['action'] == 'ulozenka_printBarcodes' 
			&& isset($_POST['item']) && count($_POST['item'])) {
			
			$orderIds = array_keys($_POST['item']);
			$consignmentIds = array();
			foreach ($orderIds as $orderId) {
				$order = Core::$db->order()->where('id', $orderId)->fetch();

				if ($order['ulozenka_branch_id'] && $order['track_num']) {
					$consignmentIds[] = $order['track_num'];
				}
			}

			if (count($consignmentIds)) {
				redirect( $this->ulozenka->getBarcodesLink($consignmentIds) );
			}
		}


		// Admin: Multi-send to zasilkovna
		// if (strpos($_SERVER['REQUEST_URI'], 'admin/orders'))
		if (isset($_POST['action']) && $_POST['action'] == 'ulozenka_createPackets' 
			&& isset($_POST['item']) && count($_POST['item'])) 
		{
			// 1) Get data
			$settings = $this->getStorage();

			// 2) Export order to zasilkovna
			if ($settings['shopId'] && $settings['apiKey'] && $settings['registerBranchId']) {
				$orderIds = array_keys($_POST['item']);
				$successIds = array();
				$alreadyCreatedIds = array();
				$notUlozenkaIds = array();
				foreach ($orderIds as $orderId) {
					$order = Core::$db->order()->where('id', $orderId)->fetch();
					$delivery = $order->delivery;

					// Valid order
					if (!$order['track_num'] && ($order['ulozenka_branch_id'] || ($delivery && $delivery['driver'] == 'ulozenka_dpd'))) {
						// try {
							$consignmentId = $this->createConsignment($order, $order['ulozenka_vs'], $order['ulozenka_note']);

							$order->update(array(
								'track_num' => $consignmentId,
							));
						// } catch (UlozenkaApiException $e) {
						// 	Core::log($e->getMessage(), 'error');
						// }

						$successIds[] = $orderId;

					// Already exported order
					} elseif ($order['ulozenka_branch_id'] && $order['track_num']) {
						$alreadyCreatedIds[] = $orderId;

					// Order with different delivery method
					} elseif (!$order['ulozenka_branch_id']) {
						$notUlozenkaIds[] = $orderId;
					}
				}
				
				// Render
				if (count($successIds)) {
					$this->flashMsg('Následující objednávky byly úspěšně odeslány do služby ulozenka.cz: ' . join(', ', $successIds));
				}
				if (count($alreadyCreatedIds)) {
					$this->flashMsg('Následující objednávky nebyly odeslány, protože již v úložence jsou: ' . join(', ', $alreadyCreatedIds));
				}
				if (count($notUlozenkaIds)) {
					$this->flashMsg('Následující objednávky nebyly odeslány, protože mají nastaven jiný spůsob dopravy, než ulozenka.cz: ' . join(', ', $notUlozenkaIds));
				}
			
			} else {
				$this->flashMsg('Není vyplněno nastavení modulu Ulozenka.cz');
			}

			redirect('admin/orders');
		}


		// Admin: Add delivery drivers
		require_once('include/delivery_ulozenka_cze.php');
		require_once('include/delivery_ulozenka_cze_parcelshop.php');
		require_once('include/delivery_ulozenka_svk.php');
		require_once('include/delivery_ulozenka_svk_parcelshop.php');
		require_once('include/delivery_ulozenka_dpd.php');
	}



	public function saveOrder($order, $customer, $payment, $delivery) 
	{
		if (isset($_SESSION['order']['ulozenka_branch_id']) && $_SESSION['order']['ulozenka_branch_id']) {
			$order->update(array(
				'ulozenka_branch_id' => $_SESSION['order']['ulozenka_branch_id'],
			));
		}
	}



	public function adminEditOrder($order) {
		if (isset($_POST['ulozenka_createPacket']) && $_POST['ulozenka_createPacket']) {
			// try {
				$consignmentId = $this->createConsignment($order, $_POST['ulozenka_vs'], $_POST['ulozenka_note']);

				$order->update(array(
					'track_num' => $consignmentId,
				));
			// } catch (???Exception $e) {
			// 	Core::log($e->getMessage(), 'error');
			// }
		}
	}



	private function createConsignment($order, $vs, $note = '')
	{
		// 1) Init
		$settings = $this->getStorage();
		$delivery = $order->delivery;
		
		// 2) Prepare values
		$cashOnDelivery = 0;
		if ( in_array($order['payment_id'], $settings['cashOnDeliveryPayments']) ) {
			$cashOnDelivery = $order['total_incl_vat'];
		}
		
		$values = array(
			'register_branch_id' => $settings['registerBranchId'],
			'order_number' => $order['id'],
			'parcel_count' => 1,
			'customer_name' => $order['ship_first_name']?: $order['first_name'],
			'customer_surname' => $order['ship_last_name']?: $order['last_name'],
			'customer_email' => $order['email'],
			'customer_phone' => $order['phone'],
			'address_street' => $order['ship_street']?: $order['street'],
			'address_town' => $order['ship_city']?: $order['city'],
			'address_zip' => $order['ship_zip']?: $order['zip'],
			'cash_on_delivery' => $cashOnDelivery,
			'weight' => $this->getOrderWeight($order),
			'currency' => $settings['currency'],
			'variable' => $vs,
			'note' => $note,
		);

		// Driver: Ulozenka CR or Ulozenka SK
		if ($order['ulozenka_branch_id']) {
			$branch = $this->ulozenka->getBranch($order['ulozenka_branch_id']);

			$transportServices = $this->ulozenka->searchTransportServices(array(
				'transport.id' => $branch->transport->id,
			));
			$transportService = array_shift($transportServices);

			$values['destination_branch_id'] = $order['ulozenka_branch_id'];
			$values['transport_service_id'] = $transportService->id;
			$values['address_state'] = $branch->country;

		// Driver: Ulozenka - DPD
		} elseif ($delivery && $delivery['driver'] == 'ulozenka_dpd') {
			$values['transport_service_id'] = 3;  // DPD Classic - DPD
			
			$country = $order['ship_country']?: $order['country'];
			if ($settings['czkCountries'] && in_array($country, $settings['czkCountries'])) {
				$values['address_state'] = 'SVK';
			} elseif ($settings['svkCountries'] && in_array($country, $settings['svkCountries'])) {
				$values['address_state'] = 'SVK';
			}

		// Driver: Invalid driver
		} else {
			throw new Exception('Invalid delivery driver.');
		}

		// 3) Create package
		$consignmentId = $this->ulozenka->createConsignment($values);

		return $consignmentId;
	}



	/**
	 * Save settings form
	 */
	public function saveSettingForm() {
		if(!empty($_POST)){
			$data = (array) $this->getStorage();
			$data = Arr::overwrite($this->options, $data, $_POST);
			$data['cashOnDeliveryPayments'] = $_POST['cashOnDeliveryPayments'];
			$data['czkCountries'] = $_POST['czkCountries'];
			$data['svkCountries'] = $_POST['svkCountries'];
			$this->saveStorage($data);
			flashMsg(__('msg_saved'));
		}
	}



	/**
	 * @return View
	 */
	public function getSettingForm() {
		$paymentPairs = array();
		foreach (Core::$db->payment() as $payment) {
			$paymentPairs[$payment['id']] = $payment['name'];
		}

		$countryPairs = array();
		foreach (Core::$db->country() as $country) {
			$countryPairs[$country['name']] = $country['name'];
		}

		$branchPairs = array();
		foreach ($this->ulozenka->searchBranches() as $branch) {
			$branchPairs[$branch->id] = $branch->name;
		}
		$branchPairs = array('' => '') + $branchPairs;
		
		return tpl('module_settings_ulozenka.php', array(
			'module' => $this, 
			'storage' => $this->getStorage(),
			'branchPairs' => $branchPairs,
			'paymentPairs' => $paymentPairs,
			'countryPairs' => $countryPairs,
			'currencyPairs' => array(
				'CZK' => 'CZK',
				'EUR' => 'EUR',
			),
		));
	}



	/********************* Internal *********************/



	public function flashMsg($msg)
	{
		if (!is_array($_SESSION['flash_msg'])) {
			$_SESSION['flash_msg'] = array(
				'type' => 'ok',
				'msg' => is_object($msg) ? $msg->__toString() : $msg,
			);

		} else {
			$_SESSION['flash_msg']['msg'] .= '<p>' . $msg;
		}
	}



	public function getOrderWeight($order)
	{
		$weight = 0;
		foreach ($order->order_products() as $orderProduct) {
			$weight += $orderProduct->product['weight'];
		}

		return $weight / 1000;  // kg to g conversion
	}
	
}
