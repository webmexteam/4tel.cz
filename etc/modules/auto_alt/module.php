<?php
defined('WEBMEX') or die('No direct access.');

/**
 * Class Module_auto_alt
 */
class Module_auto_alt extends Module {

	public $has_settings = false;

	public $module_info = array(
		'name' => array(
			'cs' => 'Automatický ALT atribut',
		),
		'description' => array(
			'cs' => 'Přidá automaticky atribut ALT u obrázků, kde není vyplněn',
		),
		'version' => '1.0.0',
		'author' => 'Michal Mikoláš &copy; Webmex.cz',
		'www' => '<a href="http://www.webmex.cz">www.webmex.cz</a>',
	);
}
