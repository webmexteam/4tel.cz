<?php
defined('WEBMEX') or die('No direct access.');

require_once('libs/importer.php');
require_once('libs/adapter.php');


/**
 * Class Module_Zasilkovna
 *
 * @todo translates
 */
class Module_Zasilkovna extends Module {

	public $has_settings = true;

	protected $options = array(
		'api_key' => '',
		'api_password' => '',
		'shop_name' => '',
		'currency' => '',
		'max_value' => '',
		'cash_on_delivery_payments' => array(),
	);

    public $module_info = array(
		'name' => array(
			'cs' => 'Doručovací služba Zásilkovna.cz',
		),
		'description' => array(
			'cs' => 'Přidá propojení se zásilkovna.cz',
		),
		'version' => '1.0.0',
		'author' => 'Tomas Nikl &copy; Webmex.cz',
		'www' => '<a href="http://www.webmex.cz">www.webmex.cz</a>',
    );



	/**
	 * Run after install module.
	 *
	 * @param $module
	 */
	public function install($module) {
		// 1) Save module settings
    	$module->update(array('storage' => serialize($this->options)));


    	// 2) Add delivery to DB
    	// Calculate ord value
    	$lastDelivery = Core::$db->delivery()
    					->order('ord DESC')
    					->limit(1)
    					->fetch();
    	$ord = $lastDelivery? $lastDelivery['ord']: 0;

    	// Save
    	$types = array(
    		array(
    			'name' => 'Osobní odběr - pobočky Zásilkovna ČR',
    			'driver' => 'zasilkovna_cz',
    		),
    		array(
    			'name' => 'Osobní odběr - pobočky Zásilkovna SK',
    			'driver' => 'zasilkovna_sk',
    		),
    	);
    	foreach ($types as $type) {
    		$ord++;

	    	$deliveryId = Core::$db->delivery()->insert(array(
				'name' => $type['name'],
				'driver' => $type['driver'],
				'price' => 100,
				'customer_group_id' => 0,
				'weight_min' => NULL,
				'weight_max' => NULL,
				'price_min' => NULL,
				'price_max' => NULL,
				'ord' => $ord,
	    	));

	    	// Connect delivery with delivery payments
	    	$payments = Core::$db->payment();
	    	foreach ($payments as $payment) {
	    		Core::$db->delivery_payments()->insert(array(
					'delivery_id' => $deliveryId,
					'payment_id' => $payment['id'],
					'price' => NULL,
					'free_over' => NULL,
	    		));
	    	}
    	}


    	// 3) Update DB schema
		try {
			Core::$db_inst->schema->addColumns('order', array('zasilkovna_branch_id' => array(
				'type' => 'integer',
			)));

			Core::$db_inst->schema->createTable(
				'zasilkovna_branch', 
				array(
					'id' => array(
						'type' => 'integer',
						'primary' => TRUE,
						'autoincrement' => FALSE,  // `id` is imported from zasilkovna.cz's source
					),
					'name_street' => array(
						'type' => 'varchar',
						'not_null' => TRUE,
					),
				)
			);
		} catch (Exception $e) {}
    }



	public function setup() {
		Event::add('Controller_Default::saveOrder', array($this, 'saveOrder'));
		Event::add('Controller_Orders::edit.after_save', array($this, 'adminEditOrder'));


		// Save zasilkovna's branch_id after submit
		if (isset($_POST['submit_order'])) {
			$delivery = Core::$db->delivery[ $_POST['delivery'] ];

			if (isset($_POST['zasilkovna_sk']) && $_POST['zasilkovna_sk'] && $delivery['driver'] == 'zasilkovna_sk') {
				$_SESSION['order']['zasilkovna_branch_id'] = $_POST['zasilkovna_sk'];
			}
			if (isset($_POST['zasilkovna_cz']) && $_POST['zasilkovna_cz'] && $delivery['driver'] == 'zasilkovna_cz') {
				$_SESSION['order']['zasilkovna_branch_id'] = $_POST['zasilkovna_cz'];
			}
		}


		// Import & show branches
		$importer = new Zasilkovna_Importer();
		if ($importer->hasApiKey()) {
			if ($importer->isOutdated()) {
				$importer->importJs();
				$importer->importJson();
			}
			View::addJS('front', '/' . $importer->getJsFilePath());
		}


		// Admin: Print selected barcodes
		/** @todo Is there better way to extend Admin:Order:index action? */
		// if (strpos($_SERVER['REQUEST_URI'], 'admin/orders'))
		if (isset($_POST['action']) && $_POST['action'] == 'zasilkovna_printBarcodes' 
			&& isset($_POST['item']) && count($_POST['item'])) {
			
			$orderIds = array_keys($_POST['item']);
			redirect('admin/zasilkovna/printBarcodes/' . join($orderIds, ',') );
		}


		// Admin: Multi-send to zasilkovna
		/** @todo Is there better way to extend Admin:Order:index action? */
		// if (strpos($_SERVER['REQUEST_URI'], 'admin/orders'))
		if (isset($_POST['action']) && $_POST['action'] == 'zasilkovna_createPackets' 
			&& isset($_POST['item']) && count($_POST['item'])) {

			// 1) Get data
			$settings = Core::module('zasilkovna')->getStorage();

			// 2) Export order to zasilkovna
			if ($settings['api_key'] && $settings['api_password'] && $settings['shop_name'] && $settings['max_value']) {
				$zasilkovnaAdapter = new Zasilkovna_Adapter($settings['api_key'], $settings['api_password'], $settings['shop_name'], $settings['max_value'], $settings['cash_on_delivery_payments']);
				
				$orderIds = array_keys($_POST['item']);
				$successIds = array();
				$alreadyCreatedIds = array();
				$notZasilkovnaIds = array();
				foreach ($orderIds as $orderId) {
					$order = Core::$db->order()->where('id', $orderId)->fetch();

					// Valid order
					if ($order['zasilkovna_branch_id'] && !$order['track_num']) {
						// try {
							$packet = $zasilkovnaAdapter->createPacket(array(
								'number' => $order['id'],
								'name' => $order['first_name'],
								'surname' => $order['last_name'],
								'email' => $order['email'],
								'phone' => $order['phone'],
								'addressId' => $order['zasilkovna_branch_id'],
								'value' => $order['total_incl_vat'],
								'currency' => $settings['currency'],
								'eshop' => $settings['shop_name'],
							));

							$order->update(array(
								'track_num' => $packet->id,
							));
						// } catch (Zasilkovna_SoapException $e) {
						// 	Core::log($e->getMessage(), 'error');
						// }

						$successIds[] = $orderId;

					// Already exported order
					} elseif ($order['zasilkovna_branch_id'] && $order['track_num']) {
						$alreadyCreatedIds[] = $orderId;

					// Order with different delivery method
					} elseif (!$order['zasilkovna_branch_id']) {
						$notZasilkovnaIds[] = $orderId;
					}
				}
				
				// Render
				if (count($successIds)) {
					$this->flashMsg('Následující objednávky byly úspěšně odeslány do služby zasilkovna.cz: ' . join(', ', $successIds));
				}
				if (count($alreadyCreatedIds)) {
					$this->flashMsg('Následující objednávky nebyly odeslány, protože již v zásilkovně jsou: ' . join(', ', $alreadyCreatedIds));
				}
				if (count($notZasilkovnaIds)) {
					$this->flashMsg('Následující objednávky nebyly odeslány, protože mají nastaven jiný spůsob dopravy, než zasilkovna.cz: ' . join(', ', $notZasilkovnaIds));
				}
			
			} else {
				$this->flashMsg('Není vyplněno nastavení modulu Zasilkovna.cz');
			}

			redirect('admin/orders');
		}


		// Admin: Add delivery drivers
		require_once('include/delivery_zasilkovna_cz.php');
		require_once('include/delivery_zasilkovna_sk.php');
	}



	public function saveOrder($order, $customer, $payment, $delivery) {
		if (isset($_SESSION['order']['zasilkovna_branch_id']) && $_SESSION['order']['zasilkovna_branch_id']) {
			$order->update(array(
				'zasilkovna_branch_id' => $_SESSION['order']['zasilkovna_branch_id'],
			));
		}
	}



	public function adminEditOrder($order) {
		if (isset($_POST['zasilkovna_createPacket']) && $_POST['zasilkovna_createPacket']) {
			$settings = Core::module('zasilkovna')->getStorage();

			// Track number
			$zasilkovnaAdapter = new Zasilkovna_Adapter($settings['api_key'], $settings['api_password'], $settings['shop_name'], $settings['max_value'], $settings['cash_on_delivery_payments']);
			// try {
				$packet = $zasilkovnaAdapter->createPacket(array(
					'number' => $order['id'],
					'name' => $order['first_name'],
					'surname' => $order['last_name'],
					'email' => $order['email'],
					'phone' => $order['phone'],
					'addressId' => $order['zasilkovna_branch_id'],
					'value' => $order['total_incl_vat'],
					'currency' => $settings['currency'],
					'eshop' => $settings['shop_name'],
					'paymentId' => $order['payment_id'],
				));

				$order->update(array(
					'track_num' => $packet->id,
				));
			// } catch (Zasilkovna_SoapException $e) {
			// 	Core::log($e->getMessage(), 'error');
			// }
		}
	}



	/********************* Settings *********************/



	/**
	 * @return View
	 */
	public function getSettingForm() {
		$paymentPairs = array();
		foreach (Core::$db->payment() as $payment) {
			$paymentPairs[$payment['id']] = $payment['name'];
		}

    	return tpl('module_settings_zasilkovna.php', array(
			'module' => $this, 
			'storage' => $this->getStorage(),
    		'paymentPairs' => $paymentPairs,
		));
    }



	/**
	 * Save settings form
	 */
	public function saveSettingForm() {
    	if(!empty($_POST)){
    		// Update
    		$data = (array) $this->getStorage();
    		$data = Arr::overwrite($this->options, $data, $_POST);
    		$data['cash_on_delivery_payments'] = $_POST['cash_on_delivery_payments'];
    		$this->saveStorage($data);

    		// Refresh branch data
			$importer = new Zasilkovna_Importer();
			if ($importer->hasApiKey()) {
				$importer->importJs();
				$importer->importJson();
			}

    		// Render
    		flashMsg(__('msg_saved'));
    	}
    }



    /********************* Internal *********************/



    public function flashMsg($msg)
    {
    	if (!is_array($_SESSION['flash_msg'])) {
			$_SESSION['flash_msg'] = array(
				'type' => 'ok',
				'msg' => is_object($msg) ? $msg->__toString() : $msg,
			);

		} else {
			$_SESSION['flash_msg']['msg'] .= '<p>' . $msg;
		}
    }
    
}