<?php


/**
 * Zasilkovna_Importer
 */
class Zasilkovna_Importer
{
	/** @var array */
	private $settings;

	/** @var array */
	private $jsFilePath = 'etc/tmp/branch.js';



	public function __construct()
	{
		$this->settings = Core::module('zasilkovna')->getStorage();
	}


	
	public function hasApiKey()
	{
		return (isset($this->settings['api_key']) && $this->settings['api_key']);
	}


	
	public function importJs()
	{
		// Get file
		$jsContent = file_get_contents("http://www.zasilkovna.cz/api/v2/" . $this->settings['api_key'] . "/branch.js");

		// Check
		if (strlen($jsContent) < 500) {
			throw new Exception('Invalid zasilkovna.cz response - maybe wrong API key?');
		}

		// Save
		$filePath = DOCROOT . $this->jsFilePath;
		if (!file_put_contents($filePath, $jsContent)) {
			throw new Exception("Cant write to file '$filePath'.");
		}
	}


	
	public function importJson()
	{
		// Get data
		$jsonContent = file_get_contents("http://www.zasilkovna.cz/api/v2/" . $this->settings['api_key'] . "/branch.json");
		$jsonContent = json_decode($jsonContent);

		// Check
		if ($jsonContent === NULL) {
			throw new Exception('Invalid zasilkovna.cz response - maybe wrong API key?');
		}

		// Save
		foreach ($jsonContent->data as $id => $branch) {
			try {
				Core::$db->zasilkovna_branch()->insert(array(
					'id' => $branch->id,
					'name_street' => $branch->nameStreet,
				));
			} catch(PDOException $e) {
				// Branch entry already exists
			} catch (Odb_Exception $e) {
				// Branch entry already exists
			}
		}
	}


	
	public function isOutdated()
	{
		$filePath = DOCROOT . $this->jsFilePath;
		return !file_exists($filePath) || (filemtime($filePath) < strtotime('-24 hours'));
	}



	public function getJsFilePath()
	{
		return $this->jsFilePath;
	}

}