<?php


/**
 * Zasilkovna_Adapter
 */
class Zasilkovna_Adapter
{
	/** @var string */
	private $wsdlPath = 'http://www.zasilkovna.cz/api/soap-php-bugfix.wsdl';

	/** @var SoapClient */
	private $soap;

	/** @var string */
	private $apiKey;
	
	/** @var string */
	private $apiPassword;
	
	/** @var string */
	private $shopName;
	
	/** @var float */
	private $maxValue;
	
	/** @var array */
	private $cashOnDeliveryIds;



	public function __construct($apiKey, $apiPassword, $shopName = NULL, $maxValue = 20000, $cashOnDeliveryIds = NULL)
	{
		$this->apiKey = $apiKey;
		$this->apiPassword = $apiPassword;
		$this->shopName = $shopName;
		$this->maxValue = $maxValue;
		$this->cashOnDeliveryIds = $cashOnDeliveryIds;

		$this->soap = new SoapClient($this->wsdlPath);
	}



	/**
	 * @param array
	 * @return stdClass
	 * @throws Zasilkovna_SoapException
	 */
	public function createPacket($attributes)
	{
		$attributes = $this->prepareAttributes($attributes);

		try {
			$packet = $this->soap->createPacket($this->apiPassword, $attributes);
			return $packet;

		} catch (SoapFault $e) {
			throw $this->createException($e);
		}
	}



	/**
	 * @param array
	 * @return stdClass
	 * @throws Zasilkovna_SoapException
	 */
	public function packetAttributesValid($attributes)
	{
		$attributes = $this->prepareAttributes($attributes);

		try {
			$packet = $this->soap->packetAttributesValid($this->apiPassword, $attributes);
			return $packet;

		} catch (SoapFault $e) {
			throw $this->createException($e);
		}
	}



	public function barcodePng($barcode)
	{
		try {
			$barcode = $this->soap->barcodePng($this->apiPassword, $barcode);
			return $barcode;

		} catch (SoapFault $e) {
			throw $this->createException($e);
		}
	}



	private function prepareAttributes($attributes)
	{
		// 1) Check
		$this->checkRequiredAttributes($attributes);


		// 2) Prepare attributes
		// Required values
		$values = array(
			'number' => $attributes['number'],
			'name' => $attributes['name'],
			'surname' => $attributes['surname'],
			'email' => @$attributes['email'],
			'phone' => @$attributes['phone'],
			'addressId' => $attributes['addressId'],
			'value' => $attributes['value'],
			'eshop' => $attributes['eshop'],
		);

		// Value
		$values['value'] = ($attributes['value'] < $this->maxValue)? $attributes['value']: $this->maxValue;

		// Eshop
		if (@$attributes['eshop'])
			$values['eshop'] = $attributes['eshop'];

		// Cash on delivery
		if (@$attributes['paymentId'] && is_array($this->cashOnDeliveryIds)) {
			if (in_array($attributes['paymentId'], $this->cashOnDeliveryIds)) {
				$values['cod'] = $attributes['value'];
			} else {
				$values['cod'] = 0;
			}
		}

		// Other values
		foreach ($attributes as $key => $value) {
			if (!isset($values[$key])) {
				$values[$key] = $value;
			}
		}


		// 3) Return
		return $values;
	}



	private function checkRequiredAttributes($attributes)
	{
		if (!@$attributes['number'])
			throw new Exception('Zasilkovna: Order number was not setted.');
		if (!@$attributes['name'])
			throw new Exception('Zasilkovna: Customer name was not setted.');
		if (!@$attributes['surname'])
			throw new Exception('Zasilkovna: Customer surname was not setted.');
		if (!@$attributes['email'] && !@$attributes['phone'])
			throw new Exception('Zasilkovna: Customer e-mail or phone must be setted.');
		if (!@$attributes['addressId'])
			throw new Exception('Zasilkovna: No branch was chosen.');
	}



	private function createException(SoapFault $e)
	{
		$message = $e->getMessage();

		// Append SoapFault details to error message
		if (isset($e->detail->PacketAttributesFault->attributes->fault) 
			&& $faults = $e->detail->PacketAttributesFault->attributes->fault) {
			
			if (is_array($faults)) {
				foreach ($faults as $fault) {
					$message .= '; ' . $fault->name . ': ' . $fault->fault;
				}
			} else {
				$fault = $faults;
				$message .= '; ' . $fault->name . ': ' . $fault->fault;
			}
		}

		$zasilkovnaException = new Zasilkovna_SoapException($message);
		return $zasilkovnaException;
	}

}



/**
 * Zasilkovna_SoapException
 */
class Zasilkovna_SoapException extends Exception
{
}