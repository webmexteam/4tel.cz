<?php defined('WEBMEX') or die('No direct access.');


class Controller_Zasilkovna extends AdminController 
{
	/** @var array|NULL */
	private $storage = null;

	/** @var bool */
	private $isAjax = false;



	public function __construct()
	{
		parent::__construct();
		
		/**/
		$this->settings = Core::module('zasilkovna')->getStorage();
		$this->zasilkovnaAdapter = new Zasilkovna_Adapter($this->settings['api_key'], $this->settings['api_password'], $this->settings['shop_name'], $this->settings['max_value'], $settings['cash_on_delivery_payments']);
	}



	public function barcode($id)
	{
		$order = Core::$db->order()->where('id', $id)->fetch();

		// Check
		if (!$order || !$order['track_num']) {
			Core::show_404();
		}

		// Render
		$barcode = $this->zasilkovnaAdapter->barcodePng($order['track_num']);
		
		$this->tpl = tpl('barcode.php', array(
			'barcode' => $barcode,
		));
	}



	public function printBarcodes($id)
	{
		$orderIds = explode(',', $id);
		$orders = Core::$db->order()->where('id', $orderIds);

		// Check
		if (count($orders) == 0) {
			Core::show_404();
		}

		// Render
		$this->tpl = tpl('print-barcodes.php', array(
			'orders' => $orders,
		));
	}

}