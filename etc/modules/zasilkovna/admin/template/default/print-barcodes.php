<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Print barcodes</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	
	<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
	<style>
	.box {
		float: left;
		margin: 0 4em 4em 0;
		width: 245px;
	}
	.box .pair {
		margin-bottom: 0.5em;
	}

	.box label {
		display: block;
		color: #666;
		font-size: 12px;
		font-weight: normal;
		width: 100%;
		margin: 0;
	}
	.box .value {
		font-size: 16px;
		font-weight: bold;
	}

	.box .barcode label {
		color: #333;
		font-size: 12px;
		padding-left: 20px;
	}
	.box .barcode img {
		display: inline-block;
		width: 121px;
		height: 40px;
	}
	</style>
</head>
<body>

	<?php foreach($orders as $order): ?>
		<div class="box">
			<div class="pair">
				<label for="">Příjemce</label>
				<div class="value">
					<?php echo $order['first_name'] ?> <?php echo $order['last_name'] ?>
				</div>
			</div>
			<div class="pair">
				<label for="">Cílová pobočka</label>
				<div class="value">
					<?php if ($order['zasilkovna_branch_id']): ?>
						<?php echo $order->zasilkovna_branch['name_street'] ?>
					<?php else: ?>
						<small><i>Chyba: Neodesláno do zasilkovna.cz</i></small>
					<?php endif; ?>
				</div>
			</div>
			<div class="pair">
				<label for="">Číslo objednávky</label>
				<div class="value"><?php echo $order['id'] ?></div>
			</div>

			<div class="pair barcode">
				<?php if ($order['track_num'] && $order['zasilkovna_branch_id']): ?>
					<label for="">
							Z <?php echo $order['track_num'] ?>
					</label>
					<img src="<?php echo url('admin/zasilkovna/barcode/' . $order['id']); ?>" alt="Z <?php echo $order['track_num'] ?>">
				
				<?php else: ?>
					<label for="">
						Chyba: Neodesláno do zasilkovna.cz
					</label>
					<img src="" alt="Error">
				<?php endif; ?>
			</div>
			
			<!--<div class="pair">
				<label for="">Zpět (odesilatel)</label>
				<div class="value">ČR P901 (Vladislav Brzobohaty)</div>
			</div>-->
		</div>
	<?php endforeach; ?>


	<script>
	window.print();
	</script>
</body>
</html>

