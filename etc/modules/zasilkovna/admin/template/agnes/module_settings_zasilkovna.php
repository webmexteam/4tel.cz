<?php defined('WEBMEX') or die('No direct access.'); ?>

<div class="form">
	<div class="clearfix">
		<fieldset style="width:300px">
			<?php
			echo forminput('text', 'api_key', $storage['api_key'], array('label' => __('webmex_module_zasilkovna_key')));
			echo forminput('text', 'api_password', $storage['api_password'], array('label' => __('webmex_module_zasilkovna_password')));
			echo forminput('text', 'shop_name', $storage['shop_name'], array('label' => __('webmex_module_zasilkovna_shop_name')));
			echo forminput('select', 'currency', $storage['currency'], array(
				'label' => __('webmex_module_zasilkovna_currency'),
				'options' => array(
					'CZK' => 'CZK',
					'EUR' => 'EUR',
					'HUF' => 'HUF',
				),
			));
			echo forminput('text', 'max_value', $storage['max_value'], array('label' => __('webmex_module_zasilkovna_max_value')));
			echo forminput('select', 'cash_on_delivery_payments[]', NULL, array(
				'label' => __('webmex_module_zasilkovna_cash_on_delivery'),
				'options' => $paymentPairs,
				'multiple' => 'multiple',
			));
			?>
		</fieldset>

		<script>
		<?php /* hotfix for forminput multiselect default value bug */ ?>
		jQuery(function(){
			<?php if(is_array($storage['cash_on_delivery_payments'])): ?>
				<?php foreach($storage['cash_on_delivery_payments'] as $key => $paymentId): ?>
				jQuery("select[name='cash_on_delivery_payments[]'] option[value=<?php echo $paymentId; ?>]").attr('selected', 'selected');
				<?php endforeach; ?>
			<?php endif; ?>
		});
		</script>
	</div>
</div>


<?php
// Check
/** @todo Check API key */
$zasilkovnaAdapter = new Zasilkovna_Adapter($storage['api_key'], $storage['api_password'], $storage['shop_name']);
try {
	$packet = $zasilkovnaAdapter->packetAttributesValid(array(
		'number' => rand(1, 999999),
		'name' => "Petr",
		'surname' => "Novák",
		'email' => "petr@novak.cz",
		'phone' => "+420777123456",
		'addressId' => 85,
		'value' => $storage['max_value'],
		'currency' => $storage['currency'],
		'eshop' => $storage['shop_name'],
	));
	echo 'API heslo i název obchodu jsou v pořádku. <br />';

} catch (Exception $e) {
	echo $e->getMessage() . '<br />';
}
