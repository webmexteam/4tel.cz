<?php defined('SQC') or die('No direct access.');

Core::$lang += array(
	'webmex_module_zasilkovna_key'	=> 'API klíč',
	'webmex_module_zasilkovna_password'	=> 'API heslo',
	'webmex_module_zasilkovna_shop_name' => 'Název obchodu',
	'webmex_module_zasilkovna_currency' => 'Měna',
	'webmex_module_zasilkovna_max_value' => 'Maximální hodnota pro účely pojištění balíku',
	'webmex_module_zasilkovna_cash_on_delivery' => 'Platby dobírkou',
);
