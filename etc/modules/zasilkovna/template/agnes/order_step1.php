<?php defined('WEBMEX') or die('No direct access.');
$view->override = false; 
?>



<?php /********************* GET DATA *********************/ ?>
<?php
$zasilkovnaDeliveries = Core::$db->delivery()
	->where('driver=? OR driver=?', 'zasilkovna_cz', 'zasilkovna_sk');
?>



<?php /********************* RENDER *********************/ ?>
<?php 
/**
 * Zasilkovna select boxes 
 * @see http://www.zasilkovna.cz/pristup-k-pobockam 
 */ 
?>
<?php 
$rendered = array();
foreach ($zasilkovnaDeliveries as $delivery): 
	$view->slot_start();
	$country = ($delivery['driver'] == 'zasilkovna_sk')? 'sk': 'cz';
?>

	<div class="packetery-branch-list zasilkovna-select-container list-type=1 country=<?php echo $country ?> field-name=zasilkovna_<?php echo $country ?>" style="border: 1px dotted black;">Načítání: seznam poboček osobního odběru</div>

<?php 
	$select = $view->slot_stop();
	foreach ($view->find("label") as $label) {
		$label = pq($label);

		if (trim($label->text()) == $delivery['name'] && !in_array($label->attr('for'), $rendered)) {
			pq($label)->after($select);

			$rendered[] = $label->attr('for');
		}
	}
endforeach; 
?>



<?php
/*
 * JavaScript
 */
$view->slot_start();
?>

<script>
jQuery(function(){
	var deliveryChanged = function(){
		var $this = jQuery('input[name=delivery]:checked');
		var $selectContainer = $this.parents('td:eq(0)').find('.zasilkovna-select-container');

		jQuery('.zasilkovna-select-container').hide();
		jQuery('.zasilkovna-select-container').find('select').attr('required', null);
		
		$selectContainer.show();
		$selectContainer.find('select').attr('required', 'required');
	};

	jQuery('input[name=delivery]').change(deliveryChanged);
	deliveryChanged();
});
</script>

<?php
$script = $view->slot_stop();
$view->find("form.order")->append($script);
?>