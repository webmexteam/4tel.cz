<?php defined('WEBMEX') or die('No direct access.');

Delivery::$drivers[] = 'zasilkovna_cz';


/**
 * Webmex - http://www.webmex.cz.
 */
class Delivery_Zasilkovna_cz extends Delivery
{
	public $name = 'Zásilkovna ČR';



	public function getTrackLink($track_num = null)
	{
		if (!$track_num) {
			$track_num = $this->order['track_num'];
		}

		return 'http://www.cpost.cz/cz/nastroje/sledovani-zasilky.php?barcode=' . $track_num . '&locale=CZ&go=ok';
	}

}