<?php defined('WEBMEX') or die('No direct access.');

/**
* @author Jan Fišer <cryou.net@gmail.com>
* @copyright  Webmex s.r.o.
*/

use \Nette\Utils\Validators;
use \Nette\Utils\Strings;
use \Nette\UnexpectedValueException;

class Module_Modal_Window_Helper {

	/**
	 * @param NULL|int $first
	 * @param NULL|int $second
	 * @return NULL|boolean
	 */
	static public function isBiggerOrEquivalent($first, $second) {

		return ($first === NULL || $second === NULL) ? NULL : ($first >= $second);
	}

	/**
	 * @param NULL|int $first
	 * @param NULL|int $second
	 * @return NULL|boolean
	 */
	static public function isSmaller($first, $second) {

		return ($first === NULL || $second === NULL) ? NULL : ($first < $second);
	}

	static public function stripTags($string, array $allowed) {}
}

class Module_Modal_Window_Convertor {

	/** @var string */
	protected $error_prefix;

	/** @var array */
	protected $data;

	/**
	 * @param array $data
	 * @param string $error_prefix
	 */
	public function __construct(array $data, $error_prefix = '') {

		$this->data = $data;
		$this->error_prefix = (string) $error_prefix;
	}

	/**
	 * @return array
	 */
	public function getData() {

		return $this->data;
	}

	/**
	 * @param string $key
	 * @return int
	 * @throw UnexpectedValueException
	 */
	public function asUnsignedInt($key) {

		return $this->data[$key] = self::toUnsignedInt($this->data[$key], $this->error_prefix . '_' . $key);
	}

	/**
	 * @param string $key
	 * @return int
	 * @throw UnexpectedValueException
	 */
	public function asPositiveInt($key) {

		return $this->data[$key] = self::toPositiveInt($this->data[$key], $this->error_prefix . '_' . $key);
	}

	/**
	 * @param string $key
	 * @return int|NULL
	 * @throw UnexpectedValueException
	 */
	public function asUnsignedIntOrNull($key) {

		return $this->data[$key] = self::toUnsignedIntOrNull($this->data[$key], $this->error_prefix . '_' . $key);
	}

	/**
	 * @param string $key
	 * @return bool
	 * @throw UnexpectedValueException
	 */
	public function asBoolean($key) {

		return $this->data[$key] = self::toBoolean($this->data[$key], $this->error_prefix . '_' . $key);
	}

	/**
	 * @param string $key
	 * @return string|NULL
	 * @throw UnexpectedValueException
	 */
	public function asPath($key) {

		return $this->data[$key] = self::toPath($this->data[$key], $this->error_prefix . '_' . $key);
	}

	/**
	 * @param mixed $mixed
	 * @param string $errorMsg
	 * @return int
	 * @throw UnexpectedValueException
	 */
	static public function toUnsignedInt($mixed, $errorMsg = '') {

		if(Validators::isNumericInt($mixed) && ($mixed = (int) $mixed) >= 0) {
			return $mixed;
		}
		throw new UnexpectedValueException($errorMsg);
	}

	/**
	 * @param mixed $mixed
	 * @param string $errorMsg
	 * @return int
	 * @throw UnexpectedValueException
	 */
	static public function toPositiveInt($mixed, $errorMsg = '') {

		if(Validators::isNumericInt($mixed) && ($mixed = (int) $mixed) > 0) {
			return $mixed;
		}
		throw new UnexpectedValueException($errorMsg);
	}

	/**
	 * @param mixed $mixed
	 * @param string $errorMsg
	 * @return int|NULL
	 * @throw UnexpectedValueException
	 */
	static public function toUnsignedIntOrNull($mixed, $errorMsg = '') {

		if($mixed === '') {
			return NULL;
		} elseif(Validators::isNumericInt($mixed)) {
			return (int) $mixed;
		}
		throw new UnexpectedValueException($errorMsg);
	}

	/**
	 * @param mixed $mixed
	 * @param string $errorMsg
	 * @return bool
	 * @throw UnexpectedValueException
	 */
	static public function toBoolean($mixed, $errorMsg = '') {

		if(is_bool($mixed) || $mixed === "0" || $mixed === "1") {
			return (bool) $mixed;
		}
		throw new UnexpectedValueException($errorMsg);
	}

	/**
	 * @param mixed $mixed
	 * @param string $errorMsg
	 * @return string|NULL
	 * @throw UnexpectedValueException
	 */
	static public function toPath($mixed, $errorMsg = '') {

		if($mixed === '') {
			return NULL;
		} elseif(is_string($mixed)) {
			if(Validators::isUrl($mixed)) {
				return $mixed;
			}
			$file = (Strings::startsWith($mixed, '/')) ? __DIR__ . '/../../..' .$mixed : __DIR__ . '/../../../' .$mixed; 
			if(file_exists($file)) {
				return (Strings::startsWith($mixed, '/')) ? $mixed : '/'. $mixed;
			}
			throw new UnexpectedValueException($errorMsg);
		}
		throw new UnexpectedValueException($errorMsg);
	}
}

class Module_Modal_Window extends Module {

	public $has_settings = TRUE;

	public $module_info = array(
		'name'		=> array(
			'cs' => 'Modální okno',
			'sk' => 'Modální okno'
		),
		'description' => array(
			'cs' => 'Přidá možnost vytvářet vyskakovací okno',
			'sk' => 'Přidá možnost vytvářet vyskakovací okno'
		),
		'version'	=> '0.1.0',
		'author' => 'Jan Fišer &copy; Webmex.cz',
		'www' => '<a href="http://www.webmex.cz">www.webmex.cz</a>',
	);

	protected $options = array(

		'box_activate' => FALSE,

		/** time */
		'delay_time' => 0,

		'max_running_time' => 10,
		'endless_running' => FALSE,

		'redisplay' => FALSE,
		'redisplay_time' => 10,
		/** end */

		'title'	=> NULL,
		'body' => '',

		'show_header' => FALSE,
		'show_footer' => FALSE,

		'box_height' => NULL,
		'box_width'	=> NULL,
		'box_max_height' => NULL,
		'box_max_width'	=> NULL,
		'box_min_height' => NULL,
		'box_min_width'	=> NULL,

		'box_background_image' => NULL,
		'box_background_color' => NULL,
	);

	public function saveSettingForm() {

    	if(empty($_POST)) {
			return;
		}
    	$data = Arr::overwrite($this->options, $_POST);

		try{

			$convertor = new Module_Modal_Window_Convertor($data, 'modal_window_module_error');

			$convertor->asBoolean('box_activate');

			/** time */
			$convertor->asUnsignedInt('delay_time');

			$convertor->asPositiveInt('max_running_time');
			$convertor->asBoolean('endless_running');

			$convertor->asBoolean('redisplay');
			$convertor->asPositiveInt('redisplay_time');
			/** end */


			/** size */
			$height = $convertor->asUnsignedIntOrNull('box_height');
			$width = $convertor->asUnsignedIntOrNull('box_width');

			$maxHeight = $convertor->asUnsignedIntOrNull('box_max_height');
			$maxWidth = $convertor->asUnsignedIntOrNull('box_max_width');

			$minHeight = $convertor->asUnsignedIntOrNull('box_min_height');
			$minWidth = $convertor->asUnsignedIntOrNull('box_min_width');

			if (Module_Modal_Window_Helper::isSmaller($height, $minHeight)) {
				throw new UnexpectedValueException('modal_window_module_error_box_height');
			}
			if (Module_Modal_Window_Helper::isSmaller($maxHeight, $height)) {
				throw new UnexpectedValueException('modal_window_module_error_box_max_height');
			}
			if (Module_Modal_Window_Helper::isSmaller($maxHeight, $minHeight)) {
				throw new UnexpectedValueException('modal_window_module_error_box_max_height');
			}
			if (Module_Modal_Window_Helper::isSmaller($width, $minWidth)) {
				throw new UnexpectedValueException('modal_window_module_error_box_width');
			}
			if (Module_Modal_Window_Helper::isSmaller($maxWidth, $width)) {
				throw new UnexpectedValueException('modal_window_module_error_box_max_width');
			}
			if (Module_Modal_Window_Helper::isSmaller($maxWidth, $minWidth)) {
				throw new UnexpectedValueException('modal_window_module_error_box_max_width');
			}
			/** end */

			$convertor->asBoolean('show_header');
			$convertor->asBoolean('show_footer');

			$convertor->asPath('box_background_image');

			$this->saveStorage($convertor->getData());
			flashMsg(__('msg_saved'));
		} catch(UnexpectedValueException $e) {
			flashMsg(__($e->getMessage()), 'error');
		}
	}

	/**
	 * @return View
	 */
	public function getSettingForm() {

    	return tpl('modal_window_settings.latte', array(
    		'module' => $this, 
    		'storage' => $this->getStorage()
		));
	}

	public function install($module) {

    	$module->update(array('storage' => serialize($this->options)));
	}
}