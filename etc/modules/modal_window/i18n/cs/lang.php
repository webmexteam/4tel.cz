<?php defined('WEBMEX') or die('No direct access.');

Core::$lang += array(

	'modal_window_module_box_activate' => 'Aktivní',


	/** time */
	'modal_window_module_tab_box_time' => 'Časování',

	'modal_window_module_delay_time' => 'Zobrazit za',
	'modal_window_module_error_delay_time' => 'Položku "Zobrazit za" nastavte jako nezáporné číslo.',

	'modal_window_module_max_running_time' => 'Maximální čas zobrazení',
	'modal_window_module_error_max_running_time' => 'Položku "Maximální čas zobrazení" nastavte jako kladné číslo.',

	'modal_window_module_endless_running' => 'Nekonečný čas zobrazení',
	'modal_window_module_error__endless_running' => 'Položka "Nekonečný čas zobrazení" musí být zaškrtnutá/nezaškrtnutá.',

	'modal_window_module_redisplay' => 'Opětovně zobrazit',
	'modal_window_module_error__redisplay' => 'Položka "Opětovně zobrazit" musí být zaškrtnutá/nezaškrtnutá.',

	'modal_window_module_redisplay_time' => 'Opětovně zobrazit za',
	'modal_window_module_error_redisplay_time' => 'Položku "Opětovně zobrazit za" nastavte jako kladné číslo.',
	/** end */



	'modal_window_module_title' => 'Titulek okna',
	'modal_window_module_body' => 'Tělo okna',

	'modal_window_module_show_header' => 'Zobrazit hlavičku',
	'modal_window_module_show_footer' => 'Zobrazit patičku',

	'modal_window_module_tab_box_size' => 'Velikost okna',
	'modal_window_module_box_max_height' => 'Maximální výška',
	'modal_window_module_box_max_width' => 'Maximální šířka',
	'modal_window_module_box_min_height' => 'Minimální výška',
	'modal_window_module_box_min_width' => 'Minimální šířka',
	'modal_window_module_box_height' => 'Výška',
	'modal_window_module_box_width' => 'Šířka',

	'modal_window_module_tab_background' => 'Nastavení pozadí',
	'modal_window_module_box_background_image' => 'Obrazové pozadí',
	'modal_window_module_box_background_color' => 'Barva pozadí',

	'modal_window_module_error_box_activate' => 'Položka "Aktivní" musí být zaškrtnutá/nezaškrtnutá.',

	'modal_window_module_error_show_header' => 'Položka "Zobrazit hlavičku" musí být zaškrtnutá/nezaškrtnutá.',
	'modal_window_module_error_show_footer' => 'Položka "Zobrazit patičku" musí být zaškrtnutá/nezaškrtnutá.',

	'modal_window_module_error_box_max_height' =>  'Položku "Maximální výška" nastavte jako nezáporné číslo nebo nechte nevyplněnou.',
	'modal_window_module_error_box_max_width' =>  'Položku "Maximální šířka" nastavte jako nezáporné číslo nebo nechte nevyplněnou.',

	'modal_window_module_error_box_min_height' =>  'Položku "Minimální výška" nastavte jako nezáporné číslo nebo nechte nevyplněnou.',
	'modal_window_module_error_box_min_width' =>  'Položku "Minimální šířka" nastavte jako nezáporné číslo nebo nechte nevyplněnou.',

	'modal_window_module_error_box_height' => 'Položku "Výška" nastavte jako nezáporné číslo nebo nechte nevyplněnou.',
	'modal_window_module_error_box_width' => 'Položku "Šířka" nastavte jako nezáporné číslo nebo nechte nevyplněnou.',

	'modal_window_module_error_box_background_image' => 'Položka "Obrazové pozadí" musí obsahovat platnou url, adresu souboru nebo musí být prázdná.',
	
	'Close' => 'Zavřít'
);