function setupModal(){
	var json = this.getAttribute('data-storage');
	var storage = $.parseJSON(json);

	var box = $(this);
	setTimeout(function(){
		box.modal('show');
	}, storage.delay_time * 1000);

	box.on('show.bs.modal', function(){
		var content = $(this).find('.modal-content');
		var style = {}
		if(storage.box_width !== null){
			style.width = storage.box_width + 'px';
		}
		if(storage.box_min_width !== null){
			style['min-width'] = storage.box_min_width + 'px';
		}
		if(storage.box_max_width !== null){
			style['max-width'] = storage.box_max_width + 'px';
		}

		if(storage.box_height !== null){
			style.height = storage.box_height + 'px';
		}
		if(storage.box_min_height !== null){
			style['min-height'] = storage.box_min_height + 'px';
		}
		if(storage.box_max_height !== null){
			style['max-height'] = storage.box_max_height + 'px';
		}

		content.css(style);
		
		setTimeout(function(){
			var body = content.find('.modal-body');
			var header = content.find('.modal-header');
			var footer = content.find('.modal-footer');
			var reduce = parseInt(header.css('padding-bottom'))
				+ parseInt(body.css('padding-top'))
				+ parseInt(body.css('padding-bottom'))
				+ parseInt(footer.css('padding-top'));
			var style = {};

			if(storage.show_header){
				reduce += content.find('.modal-header').height();
			}
			if(storage.show_footer){
				reduce += content.find('.modal-footer').height();
			}

			if(storage.box_height !== null){
				style.height = (content.height() - reduce) + 'px';
			}
			if(storage.box_min_height !== null){
				style['min-height'] = (parseInt(content.css('min-height')) - reduce) + 'px';
			}
			if(storage.box_max_height !== null){
				style['max-height'] = (parseInt(content.css('max-height')) - reduce) + 'px';
			}

			body.css(style);
		}, 500);
	});

	if(storage.endless_running === false){
		box.on('show.bs.modal', function(){
			var box = $(this);
			setTimeout(function(){
				box.modal('hide');
			}, storage.max_running_time * 1000);
		});
	}

	if(storage.redisplay === true){
		box.on('hide.bs.modal', function(){
			var box = $(this);
			setTimeout(function(){
				box.modal('show');
			}, storage.redisplay_time * 1000);
		});
	}
};