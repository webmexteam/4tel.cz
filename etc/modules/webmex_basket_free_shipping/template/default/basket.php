<?php defined('WEBMEX') or die('No direct access.');

/**
* @author Tomas Nikl, www.webmex.cz
* @copyright webmex.cz 2013
*/

	$view->override = false;
	$view->slot_start();

    $_ds = Core::$db->delivery_payments()->where('delivery_id')->where('payment_id')->where('free_over != ?', 'NULL');

    if($_ds->count()){
        echo '<div class="webmex-basket-free-shipping-module"></div>';
        $smallestFreePrice = NULL;

        foreach($_ds as $_d) {
            if(isset($smallestFreePrice) && (float) $_d['free_over'] < $smallestFreePrice || $smallestFreePrice == NULL) {
                $smallestFreePrice = (float) $_d['free_over'];
            }
        }

        if($total->total < $smallestFreePrice){
            echo '<div class="reset"></div><div class="webmex-basket-free-shipping-wrapper">' . __("webmex_basket_free_shipping") . ' <strong>' . price($smallestFreePrice-$total->total) . '</strong>';
        }
    }
?>

<?php 
    $data = $view->slot_stop();
	$dom = $view->find('.basket form');
	$dom->before($data);
?>