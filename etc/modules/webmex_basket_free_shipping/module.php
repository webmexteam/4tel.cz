<?php defined('WEBMEX') or die('No direct access.');

/**
* @author Tomas Nikl, www.webmex.cz
* @copyright webmex.cz 2013
*/

class Module_Webmex_basket_free_shipping extends Module {

	public $module_info = array(
		'name'		=> array(
			'cs' => 'Zobrazení zbývající hodnoty nákupu pro dopravu zdarma'
		),
		'description' => array(
			'cs' => 'Zobrazí v košíku, za kolik zbývá nakoupit pro dosažení dopravy zdarma'
		),
		
		'version'	=> '1.0.0',
		'author'	=> 'Tomáš Nikl; (c) 2013',
		'www'		=> 'http://www.webmex.cz/'
	);

	public function setup() {
		View::addCSS('front', 'etc/modules/webmex_basket_free_shipping/template/default/resources/css/screen.css');
	}
}