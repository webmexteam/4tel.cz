<?php defined('WEBMEX') or die('No direct access.');

/**
* @author Tomas Nikl, www.webmex.cz
* @copyright webmex.cz 2013
*/

Core::$lang += array(
	'webmex_basket_free_shipping' => 'Pro získání dopravy zdarma zbývá nakoupit za',
	'webmex_basket_free_shipping_done' => 'Doprava zdarma'
);