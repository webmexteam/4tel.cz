<?php defined('WEBMEX') or die('No direct access.');

class Controller_Mezikrok extends Controller {

	public function get_related_products($id) {
		$this->tpl = null;

		$product = Core::$db->product[(int) $id];
		$related_products = array();

		$mezikrokProducts = Core::$db->product()->where('mezikrok', 1);
		foreach ($mezikrokProducts->order('RAND()')->limit(3) as $related_product) {
			if ($related_product['status']) {
				$availability_name = '';
				$availability_color = '';

				if ((($availability = $related_product->availability) && $availability['name']) || ($availability = $related_product['bestAvailability'])) {
					$availability_name = $availability['name'];
					$availability_color = $availability['hex_color'];
				}

				$related_products[] = array(
					'id' => $related_product['id'],
					'url' => url($related_product),
					'name' => $related_product['name'],
					'image' => imgsrc($related_product, 3),
					'availability_name' => $availability_name,
					'availability_color' => $availability_color,
					'price_vat' => ''.price(price_vat($related_product)->price),
					'price_unvat' => 'Bez DPH '.price(price_unvat($related_product)->price),
					'buy_url' => url('mezikrok/second_buy', array('buy' => $related_product['id']))
				);
			}
		}

		$return = array();
		$return['related'] = $related_products;

		echo json_encode($return);
		exit;
	}

	public function ajaxbasket()
	{
		$this->tpl = null;
		$response = array('status' => false, 'msg' => '');

		if (!empty($_POST)) {
			if (!empty($_POST['product_id'])) {
				$product = Core::$db->product[(int) $_POST['product_id']];

				if (Core::$is_premium && $product && empty($_POST['attributes']) && count($product->product_attributes())) {
					$response['msg'] = 'Product has attributes that must be selected';
					$response['has_attributes'] = true;
					$response['redirect'] = url($product, null, true);
				} else {
					$attrs = !empty($_POST['attributes']) ? (array) $_POST['attributes'] : null;

					$result = Basket::add((int) $_POST['product_id'], $attrs, max((int) $_POST['qty'], 1));

					if ($result !== true && is_numeric($result)) {
						$response['msg'] = (string) __($result > 0 ? 'max_quantity' : 'basket_no_stock', $result);
					} else if ($result === true) {
						$response['status'] = true;
						$response['msg'] = (string) __('ajaxbasket_product_added');
					}
				}
			}
		}

		$response['products'] = array();

		if ($products = Basket::products()) {
			foreach ($products as $product) {
				$response['products'][] = array(
					'id' => $product['id'],
					'qty' => $product['qty'],
					'name' => $product['product']['name'],
					'url' => url($product['product'], null, true),
					'price' => $product['price'],
					'sku' => $product['sku'],
					'ean13' => $product['ean13'],
					'stock' => $product['stock'],
					'attributes' => $product['attributes'],
					'attributes_ids' => $product['attributes_ids'],
					'weight' => $product['weight']
				);
			}
		}

		$response['items'] = count($response['products']);
		$response['total'] = Basket::total()->total;

		$response['page_basket'] = url(PAGE_BASKET, null, true);

		$_ds = Core::$db->delivery_payments()->where('delivery_id')->where('payment_id')->where('free_over IS NOT NULL');
		$smallestFreePrice = NULL;
		$smallestFreeDelivery = NULL;
		foreach($_ds as $_d) {
			if ((isset($smallestFreePrice) && (float) $_d['free_over'] < $smallestFreePrice) || $smallestFreePrice == NULL) {
				$smallestFreePrice = (float) $_d['free_over'];
				$smallestFreeDelivery = $_d->delivery['name'];
			}
		}
		$response['free_delivery_from'] = ''.price($smallestFreePrice);
		$response['free_delivery_from_name'] = $smallestFreeDelivery;
		$response['you_buy_percent'] = 100;
		$response['free_delivery_remains'] = 0;

		if(Basket::total()->total < $smallestFreePrice){
			$response['you_buy_percent'] = round(Basket::total()->total / $smallestFreePrice * 100);
			$response['free_delivery_remains'] = ''.price($smallestFreePrice-Basket::total()->total);
		}
		echo json_encode($response);
	}

	public function second_buy() {
		if (!empty($_GET['buy'])) {
			$product = Core::$db->product[(int) $_GET['buy']];

			if (Core::$is_premium && $product && count($product->product_attributes())) {
				redirect($product);
			}

			$result = Basket::add((int) $_GET['buy'], null, (isset($_GET['qty'])) ? (int) $_GET['qty'] : 1);

			if ($result !== true && is_numeric($result)) {
				View::$global_data['basket_error'] = __($result > 0 ? 'max_quantity' : 'basket_no_stock', $result);
			}

			redirect(PAGE_BASKET);
		}
	}
}