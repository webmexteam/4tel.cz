<?php
defined('WEBMEX') or die('No direct access.');

/**
 * Class Module_mezikrok
 */
class Module_mezikrok extends Module {

	public $has_settings = false;

    public $module_info = array(
		'name' => array(
			'cs' => 'Mezikrok',
		),
		'description' => array(
			'cs' => 'Přidá mezikrok mezi nákupem a košíkem',
		),
		'version' => '1.0.0',
		'author' => 'Tomas Nikl &copy; Webmex.cz',
		'www' => '<a href="http://www.webmex.cz">www.webmex.cz</a>',
    );

	public function install() {
		Core::$db_inst->schema->addColumns('product', array('mezikrok' => array(
			'type' => 'integer',
			'length' => 1,
			'not_null' => true,
			'default' => 0,
		)));
	}

	public function setup() {
		View::addCSS('front', DOCROOT.'etc/modules/mezikrok/template/'.Core::config('template').'/resources/css/screen.less.css');
		View::addCSS('front', DOCROOT.'etc/modules/mezikrok/template/'.Core::config('template').'/resources/css/modal.css');
		View::addJs('front', 'etc/modules/mezikrok/template/'.Core::config('template').'/resources/js/basket.js');
		View::addJs('front', 'etc/modules/mezikrok/template/'.Core::config('template').'/resources/js/modal.js');
	}
}
