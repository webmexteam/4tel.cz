<?php
$storage = Core::module('mezikrok')->getStorage();
$view->override = false;
$view->slot_start(); 
?>




<div id="mezikrok-modal" class="modal hide fade">
	<div class="modal-header">
		<div class="has-added">
			<span><?php echo __('module_mezikrok_product_added');?></span>
			<strong id="mezikrok-buy-name"></strong>
		</div>

		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	</div>
	<div class="modal-body">
		<div class="doprava-zdarma">
			<span class="title"><?php echo __('module_mezikrok_delivery_free_left');?></span>
			<div class="bar">
				<div class="line" style="width:50%"></div>
			</div>
			<div class="reset"></div>
		</div>

		<div class="want-to-buy">
			<h3><?php echo __('module_mezikrok_dont_miss');?></h3>
			<div class="row-fluid related-products">
				<?php for ($i=0; $i < 3; $i++):?>
					<div class="span4">
						<div class="item item-<?php echo $i?>">
							<a href="#" class="image">
								<img src="" alt="" width="145">
							</a>
							<h4 class="name"></h4>
							<div class="prices">
								<div class="price_vat"></div>
							</div>
							<div class="buy-btn">
								<a href="" class="buy second-buy"><?php echo __('module_mezikrok_buy');?></a>
							</div>
						</div>
					</div>
				<?php endfor?>
			</div>
		</div>
	</div>
	<div class="modal-footer">
		<div class="row-fluid">
			<div class="span6">
				<a href="#" class="continue close" data-dismiss="modal" aria-hidden="true"><?php echo __('module_mezikrok_continue');?></a>
			</div>
			<div class="span6">
				<a href="<?php echo url(PAGE_BASKET)?>" class="go-basket"><span><?php echo __('module_mezikrok_shopping_cart');?></span></a>
			</div>
		</div>
	</div>
</div>

<?php
$html = $view->slot_stop();
$view->find('.products')->after($html);

$view->find('.products .product .prices a')->addClass('mezikrok-btn');
?>