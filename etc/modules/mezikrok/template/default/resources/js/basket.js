var getProductIdByButton = function(element) {
	var $this = $(element);

	if ($this.is('input, button')) {
		return $this.parent().find('input[name="product_id"]').val();
	
	} else if ($this.is('a')) {
		return $this.attr('href').match(/buy\=([\d]+)/)[1];
	}

	return null;
}

var Basket = {
	
	updatePage: function(response){
		var basket = $('#basket');
		
		if(basket && basket.length){
			basket.find('.value:eq(0)').html(response.items ? response.items+' ks' : '0 ks');
			basket.find('.value:eq(1)').html(response.total ? displayPrice(response.total) : '0 Kč');
		}
	},
	
	showBubble: function(target_el, response){
		// Doprava zdarma
		if(response.free_delivery_remains == 0) {
			$('#mezikrok-modal .doprava-zdarma').hide();
		}else{
			$('#mezikrok-modal .doprava-zdarma').show();
			$('#mezikrok-modal #free-delivery-remains').html(response.free_delivery_remains);						
			$('#mezikrok-modal .bar .line').css('width', response.you_buy_percent+'%');
			$('#mezikrok-modal .full-doprava').html(response.free_delivery_from);
			$('#mezikrok-modal .doprava-zdarma').show();
		}

		// Souvisejici a zbrazeni modal okna
		$('#mezikrok-buy-name').html($('.main h1.name').text());

		var product_id = getProductIdByButton(target_el);
		$.get(_base+'mezikrok/get_related_products/'+product_id, function(r) {
			$('#mezikrok-modal .related-products .item').hide();
			$.each(r.related, function(i){
				var item = $('#mezikrok-modal .related-products .item-'+i);
				item.show();

				item.find('.name').html(r.related[i].name);
				item.find('.image img').attr('src', r.related[i].image);
				item.find('.image').attr('href', r.related[i].url);
				item.find('.availability').html(r.related[i].availability_name);
				item.find('.availability').css('color', '#'+r.related[i].availability_color);
				item.find('.price_vat').html(r.related[i].price_vat);
				item.find('.price_unvat').html(r.related[i].price_unvat);
				item.find('.buy').attr('href', r.related[i].buy_url);
			});
			if(r.related.length) {
				$('#mezikrok-modal .want-to-buy').show();
			}
			$('#mezikrok-modal').modal('show');
		}, 'json');
		
		Basket.updatePage(response);
	},
	
	addToBasket: function(data, btn, allow_redirect){
		if(btn){
			$(btn).attr('disabled', true);
		}
	
		$.post(_base+'mezikrok/ajaxbasket', data, function(response){
			if(response){
				if(btn){
					if(! response.status && response.has_attributes && allow_redirect){
						window.location = response.redirect;
						return;
					}
					
					$(btn).removeAttr('disabled');
					Basket.showBubble(btn, response);
				}
				
			}else{
				alert('Unknown error');
			}
			
		}, 'json');
	}
	
}

$(function(){
	
	$('.mezikrok-btn').bind('click', function(){
		var data = $(this).parents('form:first').serializeArray();
		Basket.addToBasket(data, this, false);
		return false;
	});
	
	$('a.buy.mezikrok-btn').bind('click', function(){
		var product_id = $(this).attr('href').match(/buy\=([\d]+)/)[1];
		
		if(product_id){
			Basket.addToBasket({
				product_id: product_id,
				qty: 1
			}, this, true);
			
			return false;
		}
	});

	$('#mezikrok-modal .second-buy').bind('click', function(){
		var qty = $(this).parent().parent().find('input[name="qty"]').val();
		qty = qty? qty: 1;
		
		$(this).attr('href', $(this).attr('href')+'&qty='+qty);
	});
});