<?php defined('SQC') or die('No direct access.');

Core::$lang += array(
	'module_mezikrok_product_added'	=> 'Produkt byl přidán do košíku',
	'module_mezikrok_delivery_free_left'	=> 'Do dopravy zdarma Vám chybí nakoupit ještě za <em id="free-delivery-remains"></em>',
	'module_mezikrok_dont_miss'	=> 'Mohlo by Vás ještě zajímat',
	'module_mezikrok_buy'	=> 'koupit',
	'module_mezikrok_continue'	=> 'Zpět do e-shopu',
	'module_mezikrok_shopping_cart'	=> 'Nákupní košík',
	'module_mezikrok_delivery_free_done'	=> 'Dopravu máte zdarma!',
);
