<?php defined('SQC') or die('No direct access.');

Core::$lang += array(
	'module_mezikrok_product_added'	=> 'Produkt bol pridaný do košíka',
	'module_mezikrok_delivery_free_left'	=> 'Do dopravy zdarma Vám chýba nakúpiť ešte za <em id="free-delivery-remains"></em>',
	'module_mezikrok_dont_miss'	=> 'Mohlo by Vás ešte zaujímať',
	'module_mezikrok_buy'	=> 'kúpiť',
	'module_mezikrok_continue'	=> 'Pokračovať v nákupe',
	'module_mezikrok_shopping_cart'	=> 'Nákupný košík',
);
