<?php
$view->override = false;
$view->slot_start(); 
?>


<div class="inputwrap clearfix">
	<label for="inp-promote">Zobrazit v mezikroku:</label>
	<input type="checkbox" class="input-checkbox" id="inp-mezikrok" value="1" name="mezikrok" <?php echo $product['mezikrok']? 'checked="checked"': ''; ?>>
</div>


<?php
$html = $view->slot_stop();
$view->find('#tab-options')->prepend($html);
?>